<?php
 class SupriScmCTR implements IControle{

 	public function SupriScmCTR(){
 	}

 	public function preparaLista(){

 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriScm = $oFachada->recuperarTodosSupriScmPorUnidade($_SESSION['Perfil']['CodUnidade']);
 		$_REQUEST['voSupriScm'] = $voSupriScm;
 		include_once("view/Suprimento/supri_scm/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaSys = new FachadaSysBD();
        $nIdPessoa = $_REQUEST['fPesCodigo'];

 		$oSupriScm = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriScm = ($_POST['fIdSupriScm'][0]) ? $_POST['fIdSupriScm'][0] : $_GET['nIdSupriScm'];

 			if($nIdSupriScm){

                $vIdSupriScm = explode("||",$nIdSupriScm);

                $oSupriScm = $oFachada->recuperarUmSupriScm($vIdSupriScm[0]);
                $_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItemPorSupriScm2($oSupriScm->getCodScm());

               if($_REQUEST['sOP'] == 'Alterar'){

                    ///print_r($oSupriScm);
                    //die();

                    $oConta = $oFachadaFinanceiro->recuperarUmMnyPlanoContas($oSupriScm->getConCodigo());
                    $nCodigo = substr($oConta->getCodigo(),0,-2);
                    $_REQUEST['voMnyConta'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($nCodigo);
                    //die();
               }

 			}
 		}

        //print_r($_REQUEST['voMnyConta'] );
        //die('aqui');

 		$_REQUEST['oSupriScm'] = ($_SESSION['oSupriScm']) ? $_SESSION['oSupriScm'] : $oSupriScm;
 		unset($_SESSION['oSupriScm']);


        if($_REQUEST['sOP'] == "Cadastrar"){
            $oSupriScm = $oFachada->recuperarUmSupriScmPorMaiorNumeroScm();
            if($oSupriScm){
                $nNumeroScm = $oSupriScm->getNumeroScm();
            }else{
                $nNumeroScm = 0;
            }

            $_REQUEST['sNumeroSolicitacao'] = "SUP.".date('Y').".".($nNumeroScm + 1);
            $_REQUEST['nNumeroScm'] = ($nNumeroScm + 1);


        }

        $sGrupo = '4'; // Tipo de doc
		$_REQUEST['voMnyTipoDoc'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '7'; // Centro de Negocio
		$_REQUEST['voMnyCentroNegocio'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		//$sGrupo = '8'; // Unidade
		//$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		$_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());

		$sGrupo = '10'; // Setor
		$_REQUEST['voMnySetor'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '6'; // Custo
		$_REQUEST['voMnyCusto'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = 2; // Centro de Custo
		$_REQUEST['voMnyCentroCusto'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasCabeca($sGrupo);

		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();

        $_REQUEST['voSupriProduto'] = $oFachada->recuperarTodosSupriProduto();

        $_REQUEST['voMnyContratoDocTipo'] = $oFachadaFinanceiro->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial(13,0);

 		if($_REQUEST['sOP'] == "Detalhar"){

            // FLUXO DA TRAMITAÇÂO...
            $_REQUEST['voTramitacao'] = $oFachada->recuperarTodosSupriScmProtocoloCodScm($oSupriScm->getCodScm());

            $oSTatusWizardBreadcrumb =  $oFachada->recuperarSupriScmProtocoloPorCodScmWizardBreadcrumb($oSupriScm->getCodScm());
            $_REQUEST['sSTatusWizardBreadcrumb'] = $oSTatusWizardBreadcrumb->para_cod_tipo_protocolo;

            $_REQUEST['voTramitacaoStatus'] = $oFachada->recuperarTodosSupriProtocoloEncaminhamentoPorTramite();
            // TRAMITA PARA...
            $_REQUEST['voSupriProtocoloEncaminhamento'] = $oFachada->recuperarTodosSysProtocoloEncaminhamentoPorOrigem($_SESSION['Perfil']['CodGrupoUsuario']);

            include_once("view/Suprimento/supri_scm/detalhe.php");

        }else{

 			include_once("view/Suprimento/supri_scm/insere_altera.php");

        }
 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];


 		if($sOP != "Excluir"){

           // print_r($_POST);
           // die();


            if($_POST['fEntrega'] == 1){
                $_POST['fQtdeEntrega'] =1;
            }


  //        $oSupriScm = $oFachada->inicializarSupriScm($_POST['fCodScm'],$_POST['fCodPessoaSolicitante'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataPrevEntrega'],mb_convert_case($_POST['fEntrega'],MB_CASE_UPPER, "UTF-8"),$_POST['fQtdeEntrega'],mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodTipoCusto'],$_POST['fCodNegocio'],$_POST['fCodUnidade'],$_POST['fCodCusto'],$_POST['fCodSetor'],$_POST['fCodConta'],$_POST['fEmpCodigo'],mb_convert_case($_POST['fObservacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataSolicitacao']);
            $oSupriScm = $oFachada->inicializarSupriScm($_POST['fCodScm'],$_POST['fCodPessoaSolicitante'],$_POST['fNumeroScm'],$_POST['fNumeroSolicitacao'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fUniCodigo'],$_POST['fCenCodigo'],$_POST['fSetCodigo'],$_POST['fConCodigo'],$_POST['fEmpCodigo'],mb_convert_case($_POST['fObservacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataSolicitacao']);


            $_SESSION['oSupriScm'] = $oSupriScm;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;


 			$oValidate->add_number_field("CodPessoaSolicitante", $oSupriScm->getCodPessoaSolicitante(), "number", "y");
			$oValidate->add_text_field("Descricao", $oSupriScm->getDescricao(), "text", "y");
			//$oValidate->add_date_field("DataPrevEntrega", $oSupriScm->getDataPrevEntrega(), "date", "y");
			//$oValidate->add_text_field("Entrega", $oSupriScm->getEntrega(), "text", "y");
            $oValidate->add_text_field("Aplicacao", $oSupriScm->getAplicacao(), "text", "y");
            /*
            if($oSupriScm->getEntrega() ==2){
                $oValidate->add_number_field("Qtde", $oSupriScm->getQtdeEntrega(), "number", "y");

            }
            */
            $oValidate->add_number_field("Custo", $oSupriScm->getCusCodigo(), "number", "y");
            $oValidate->add_number_field("C.Negocio", $oSupriScm->getNegCodigo(), "number", "y");
			$oValidate->add_number_field("Unidade", $oSupriScm->getUniCodigo(), "number", "y");
            $oValidate->add_number_field("C.Custo", $oSupriScm->getCenCodigo(), "number", "y");
			$oValidate->add_number_field("Conta", $oSupriScm->getConCodigo(), "number", "y");
			$oValidate->add_number_field("Empresa", $oSupriScm->getEmpCodigo(), "number", "y");
            $oValidate->add_number_field("Setor", $oSupriScm->getSetCodigo(), "number", "y");


            if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();

 				$sHeader = "?bErro=1&action=SupriScm.preparaFormulario&sOP=".$sOP."&nIdSupriScm=".$_POST['fCodScm'];
 				//header("Location: ".$sHeader);

 			}


 		}

 		switch($sOP){
 			case "Cadastrar":

                $nErro=0;
                $nProdutos = count($_POST['fCodProduto']);
                $j = $nProdutos -1;

                          //print_r($_POST['fDataPrevEntrega'] );
                          //die();
                if($nProdutos >= 1){
                    if($nCodSolicitacao = $oFachada->inserirSupriScm($oSupriScm)){
                        $i=0;
                       while ($i <=$j){
                           $nItem = $i+1;
                            $oSupriScmItem = $oFachada->inicializarSupriScmItem($nCodSolicitacao,$nItem,$_POST['fCodProduto'][$i],$_POST['fQtde'][$i],mb_convert_case($_POST['fProjeto'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fRevisao'][$i],$_POST['fConsumoMensal'][$i],mb_convert_case($_POST['fAplicacaoItem'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fDataPrevEntrega'][$i],mb_convert_case($_POST['fEntrega'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fQtdeAtendida']);

                           if(!$oFachada->inserirSupriScmItem($oSupriScmItem)){
                               $nErro++;
                               $oFachada->excluirSupriScmItemPorSolicitacao($nCodSolicitacao);
                               $oFachada->excluirSupriScm($nCodSolicitacao);
                               break;
                           }
                           $i++;
                       }
                    if($nErro >0){
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir Solicita&ccedil;&atilde;o de Compra[ITEM]!";
                        $sHeader = "?bErro=1&action=SupriScm.preparaFormulario&sOP=".$sOP;
                    }else{

                       if($_FILES['arquivo']){
                           $fCodContratoDocTipo = $_REQUEST['fCodContratoDocTipo'];
                           $arquivos = $_FILES['arquivo'];
                       //  Continuar aqui...

                               for($i = 0 ; count($arquivos['name']) > $i  ;$i++){
                                    if($_FILES["arquivo"]["tmp_name"][$i]){
                                        $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                        $Tipo = $_FILES["arquivo"]["type"][$i];
                                        $Tamanho = $_FILES["arquivo"]["size"][$i];

                                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                        $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                        $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                       // unlink("c:\\wamp\\tmp\\arquivo.pdf");

                                        $sNome = "SOLICITACAO_".$nCodSolicitacao;
                                        $sArquivo = $Anexo;
                                        $nCodScm = $nCodSolicitacao;
                                        $nCodMapa = "NULL";
                                        $nCodFornecedor = "NULL";

                                        $sExtensao = $Tipo;
                                        $nCodTipo = $fCodContratoDocTipo[$i];
                                        $nTamanho = $Tamanho;
                                        $dDataHora= date('d/m/Y H:i:s');
                                        $sRealizadoPor = $_SESSION['Perfil']['Login'];
                                        $nAtivo = 1;

                                        $oSupriArquivo = $oFachada->inicializarSupriArquivo($nCodArquivo[$i],$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nCodScm,$nCodMapa,$nCodFornecedor);
                                        $oSupriArquivo->setArquivo($sArquivo);
                                        $oFachada->inserirSupriArquivo($oSupriArquivo);

                                    }
                               }

                             $_POST['fCodScm'] = $nCodSolicitacao;
                             $_POST['fResponsavel'] = $_SESSION['Perfil']['Login'];
                             $_POST['fDeCodTipoProtocolo'] = $_SESSION['Perfil']['CodGrupoUsuario'];
                             $_POST['fParaCodTipoProtocolo'] = 20; // *20 -> Almoxarifado   $_REQUEST['fParaCodTipoProtocolo'];
                             $_POST['fDescricao'] = "SOLICITAÇÃO REALIZADA !"; //$_REQUEST['fResponsavel'];
                             $_POST['fData'] = date("d/m/Y H:i:s");

                             $oSupriScmProtocolo = $oFachada->inicializarSupriScmProtocolo($_POST['fCodProtocoloScm'],$_POST['fCodScm'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                             $oFachada->inserirSupriScmProtocolo($oSupriScmProtocolo);

                            //$_POST['fContratoCodigo'] = $_REQUEST['fCodContratoArquivo'];
                            //$oUpload = UploadMultiplo::UploadBanco($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$nCodSolicitacao,$_REQUEST['fContratoTipo']);
                        }

                         //INICIO TRAMITAÇÃO ...
                         $_POST['fCodScm'] = $nCodSolicitacao;
                         $_POST['fResponsavel'] = $_SESSION['Perfil']['Login'];
                         $_POST['fData'] = date('d/m/Y');
                         $_POST['fDeCodTipoProtocolo'] = $_SESSION['Perfil']['CodGrupoUsuario'];
                         $_POST['fParaCodTipoProtocolo'] = 20; // *20 -> Almoxarifado   $_REQUEST['fParaCodTipoProtocolo'];
                         $_POST['fDescricao'] = "SOLICITAÇÃO DE COMPRA REALIZADA !"; //$_REQUEST['fResponsavel'];

                         $oSupriScmProtocolo = $oFachada->inicializarSupriScmProtocolo($_POST['fCodProtocoloScm'],$_POST['fCodScm'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);

                         $oFachada->inserirSupriScmProtocolo($oSupriScmProtocolo);

                        //fim arquivos
                        unset($_SESSION['oSupriScm']);
                        $_SESSION['sMsg'] = "Solicita&ccedil;&atilde;o de Compra inserida com sucesso!";
                        $sHeader = "?bErro=0&action=SupriScm.preparaLista";
                    }
                }else{
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a solicita&ccedil;&atilde;o de Compra!";
                    $sHeader = "?bErro=1&action=SupriScm.preparaFormulario&sOP=".$sOP;
                }
                }else{
                   $_SESSION['sMsg'] = "Nenhum Produto foi adicionado a Solicita&ccedil;&atilde;o de Compra!";
                    $sHeader = "?bErro=1&action=SupriScm.preparaFormulario&sOP=".$sOP;
                }

 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriScm($oSupriScm)){
 					unset($_SESSION['oSupriScm']);
 					$_SESSION['sMsg'] = "Solicitação alterada com sucesso!";

                    $nScm = 1;

 				} else {
 					$nScm = 2;
 				}


                $nItem = 0;
                $nProdutos = count($_POST['fCodProduto']);
                $j = $nProdutos -1;
                $i=0;
                $UltimoItem = $oFachada->recuperarUmSupriScmItemPorMaxItem($oSupriScm->getCodScm());

                if($nProdutos >= 1){
                     $nItem = ($UltimoItem->getCodScmItem() +1);
                     for($i=0; $nProdutos > $i; $i++){
                        $oSupriScmItem = $oFachada->inicializarSupriScmItem($oSupriScm->getCodScm(),$nItem,$_POST['fCodProduto'][$i],$_POST['fQtde'][$i],mb_convert_case($_POST['fProjeto'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fRevisao'][$i],$_POST['fConsumoMensal'][$i],mb_convert_case($_POST['fAplicacaoItem'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fDataPrevEntrega'][$i],mb_convert_case($_POST['fEntrega'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fQtdeAtendida']);
                         //print_r($oSupriScmItem );die();
                         if(!$oFachada->inserirSupriScmItem($oSupriScmItem)){
                           $nItem=1;
                          // $oFachada->excluirSupriScmItemPorSolicitacao($nCodSolicitacao);
                          // $oFachada->excluirSupriScm($nCodSolicitacao);
                           break;
                       }
                       $nItem++;
                    }
                }

                //print_r($_FILES['arquivo']);
               // print_r($_POST);
               // die();

                if($_FILES['arquivo']){
                    $fCodContratoDocTipo = $_REQUEST['fCodContratoDocTipo'];
                    $arquivos = $_FILES['arquivo'];
               //  Continuar aqui...


                       for($i = 0 ; count($arquivos['name']) > $i  ;$i++){
                            if($_FILES["arquivo"]["tmp_name"][$i]){
                                $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                $Tipo = $_FILES["arquivo"]["type"][$i];
                                $Tamanho = $_FILES["arquivo"]["size"][$i];

                                move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                               // unlink("c:\\wamp\\tmp\\arquivo.pdf");

                                $sNome = "SOLICITACAO_".$oSupriScm->getCodScm();
                                $sArquivo = $Anexo;
                                $nCodScm = $oSupriScm->getCodScm();
                                $nCodMapa = "NULL";
                                $nCodFornecedor = "NULL";

                                $sExtensao = $Tipo;
                                $nCodTipo = $fCodContratoDocTipo[$i];
                                $nTamanho = $Tamanho;
                                $dDataHora= date('d/m/Y H:i:s');
                                $sRealizadoPor = $_SESSION['Perfil']['Login'];
                                $nAtivo = 1;

                                if( $arquivos['name'][$i] != "" && ($_POST['fCodArquivo'][$i] != "") ){
                                //if($nCodArquivo[$i]){
                                    $oSupriArquivo = $oFachada->recuperarUmSupriArquivo($_POST['fCodArquivo'][$i]);
                                    $oSupriArquivo->setArquivo($sArquivo);
                                    $oSupriArquivo->setTamanho($Tamanho);
                                    $oSupriArquivo->setRealizadoPor($sRealizadoPor);
                                    $oSupriArquivo->setDataHoraBanco($dDataHora);

                                    //print_r($oSupriArquivo);
                                    //die();

                                    $oFachada->alterarSupriArquivo($oSupriArquivo);

                                }else{
                                    $oSupriArquivo = $oFachada->inicializarSupriArquivo($nCodArquivo[$i],$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nCodScm,$nCodMapa,$nCodFornecedor);
                                    $oSupriArquivo->setArquivo($sArquivo);
                                    $oFachada->inserirSupriArquivo($oSupriArquivo);
                                }

                            }
                       }





                    $_POST['fContratoCodigo'] = $_REQUEST['fCodContratoArquivo'];
                   // $oUpload = UploadMultiplo::UploadBanco($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$nCodSolicitacao,$_REQUEST['fContratoTipo']);
                }
                $sHeader = "?action=SupriScm.preparaFormulario&sOP=Alterar&nIdSupriScm=".$oSupriScm->getCodScm();
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriScm']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriScm($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_scm(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScm.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_scm!";
 					$sHeader = "?bErro=1&action=SupriScm.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

   function insereProduto(){
			$oFachadaSuprimento   = new FachadaSuprimentoBD();
            $_REQUEST['idCampo'] = $_GET['idCampo'];
            $_REQUEST['oSupriProduto'];
            $_REQUEST['nCodProduto'] = $_GET['nCodProduto'];
            $_REQUEST['oSupriProduto'] = $oFachadaSuprimento->recuperarUmSupriProduto($_GET['nCodProduto']);
            $_REQUEST['nQtde'] = $_GET['Qtde'];
            $_REQUEST['sProjeto'] = $_GET['sProjeto'];
            $_REQUEST['nRev'] = $_GET['nRev'];
            $_REQUEST['nConsumoMensal'] = $_GET['nConsumoMensal'];
            $_REQUEST['sAplicacao'] = $_GET['sAplicacao'];
            $_REQUEST['dDataPrevEntrega'] = $_GET['dDataPrevEntrega'];
            $_REQUEST['sTipo'] = $_GET['sTipo'];

			include_once("view/suprimento/supri_scm/produto_ajax.php");
			exit();
	}


    function excluiItem(){

         $oFachada = new FachadaSuprimentoBD();

         $vCodScmItem = explode('||',$_REQUEST['nCodScmItem']);
         $oSupriScmItem = $oFachada->recuperarUmSupriScmItem($vCodScmItem[0],$vCodScmItem[1]);

         if($oFachada->excluirSupriScmItem($vCodScmItem[0],$vCodScmItem[1])){
          echo "
                <div class='col-sm-1'><span class='label label-danger' style='font-weight:bold;'><i class='glyphicon glyphicon-trash'></i>Excluido</span></div>
                <div class='col-sm-4'><strike>".$oSupriScmItem->getSupriProduto()->getDescricao()."</strike></div>
                <div class='col-sm-1' style='padding-left: 20px;'><strike>". $oSupriScmItem->getQtde()."(".$oSupriScmItem->getSupriProduto()->getSupriUnidade()->getDescricao().")</strike></div>
                <div class='col-sm-2' style='padding-left: 30px;'><strike>". $oSupriScmItem->getProjeto() ."</strike></div>
                <div class='col-sm-1' style='padding-left: 30px;'><strike>". $oSupriScmItem->getRevisao() ."</strike></div>
                <div class='col-sm-1' style='text-align:center;'><strike>". $oSupriScmItem->getConsumoMensal() ."</strike></div>
                <div class='col-sm-2' style='padding-left: 100px;'><strike>". $oSupriScmItem->getAplicacao() ."</strike></div>
            ";
         }else{
             echo "
                <div class='col-sm-1'><span class='label label-danger' style='font-weight:bold;'><i class='glyphicon glyphicon-trash'></i>Excluido</span></div>
                <div class='col-sm-4'><strike>".$oSupriScmItem->getSupriProduto()->getDescricao()."</strike></div>
                <div class='col-sm-1' style='padding-left: 20px;'><strike>". $oSupriScmItem->getQtde()."(".$oSupriScmItem->getSupriProduto()->getSupriUnidade()->getDescricao().")</strike></div>
                <div class='col-sm-2' style='padding-left: 30px;'><strike>". $oSupriScmItem->getProjeto() ."</strike></div>
                <div class='col-sm-1' style='padding-left: 30px;'><strike>". $oSupriScmItem->getRevisao() ."</strike></div>
                <div class='col-sm-1' style='text-align:center;'><strike>". $oSupriScmItem->getConsumoMensal() ."</strike></div>
                <div class='col-sm-2' style='padding-left: 100px;'><strike>". $oSupriScmItem->getAplicacao() ."</strike></div>
            ";
         }

         exit;

    }


     function atualizaNumeroSolicitacao(){
           $oFachadaSuprimento = new FachadaSuprimentoBD();
           $oFachadaFinanceiro = new FachadaFinanceiroBD();

           $sSetor = substr($oFachadaFinanceiro->recuperarUmMnyPlanoContas($_GET['nSetCodigo'])->getDescricao(),0,3);


           $oSupriScm = $oFachadaSuprimento->recuperarUmSupriScmPorMaiorNumeroScm();
           if($oSupriScm){
                $nNumeroScm = $oSupriScm->getNumeroScm();
           }else{
                $nNumeroScm = 0;
           }

           $_REQUEST['sNumeroSolicitacao'] = 'SUP.'.$sSetor.'.'.date('Y').'.'.($nNumeroScm + 1);
           $sNovoNumero = "SUP.".$sSetor.".".date('Y').".".($nNumeroScm + 1);

           echo "<span class = 'label label-primary' style='font-size:20px;'>SUP.".$sSetor.".".date('Y').".".($nNumeroScm + 1)."</span><br>
           <input type='hidden' value='".$sNovoNumero."' name='fNumeroSolicitacao'>

           ";

           exit();

     }

 }


 ?>
