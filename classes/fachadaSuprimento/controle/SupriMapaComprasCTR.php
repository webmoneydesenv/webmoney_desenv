<?php
 class SupriMapaComprasCTR implements IControle{

 	public function SupriMapaComprasCTR(){

 	}

 	public function preparaFornecedor(){
        $oFachadaView  = new FachadaViewBD;

        if($_GET['nome']){
            $oVPessoaGeralFormatada = $oFachadaView->recuperarUmVPessoaGeralFormatadaPorNome($_GET['nome']);

            if($oVPessoaGeralFormatada){
                echo "<input type='text'  readonly name='tab_title' id='tab_title' value='".$oVPessoaGeralFormatada->getNome()."' class='fornecedor'>";
                echo "<input type='hidden' id='tab_end' value='".$oVPessoaGeralFormatada->getPesgEndLogra().", ".$oVPessoaGeralFormatada->getPesgEndBairro().", ".$oVPessoaGeralFormatada->getCidade()."-".$oVPessoaGeralFormatada->getEstado()."'>";
                echo "<input type='hidden' id='tab_fone' value='".$oVPessoaGeralFormatada->getPesFones()."'>";
                echo "<div id='contato'>".$oVPessoaGeralFormatada->getPesgEmail()."</div>";
            }else{
                echo "Nenhum registro foi encontrato!";
            }
        }else{
            echo "";
        }


       // print_r($oVPessoaGeralFormatada);
       // die();





        exit();
    }
 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriMapaCompras = $oFachada->recuperarTodosSupriMapaCompras();

 		$_REQUEST['voSupriMapaCompras'] = $voSupriMapaCompras;
 		$_REQUEST['voSupriOm'] = $oFachada->recuperarTodosSupriOm();


 		include_once("view/Suprimento/supri_mapa_compras/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();
        $oFachadaView = new FachadaViewBD();
 		$oSupriMapaCompras = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriMapaCompras = ($_POST['fIdSupriMapaCompras'][0]) ? $_POST['fIdSupriMapaCompras'][0] : $_GET['nIdSupriMapaCompras'];

 			if($nIdSupriMapaCompras){
 				$vIdSupriMapaCompras = explode("||",$nIdSupriMapaCompras);
 				$oSupriMapaCompras = $oFachada->recuperarUmSupriMapaCompras($vIdSupriMapaCompras[0]);
 			}
 		}else{
            $_REQUEST['sOP'] = "Cadastrar";
        }

 		$_REQUEST['oSupriMapaCompras'] = ($_SESSION['oSupriMapaCompras']) ? $_SESSION['oSupriMapaCompras'] : $oSupriMapaCompras;
 		unset($_SESSION['oSupriMapaCompras']);

 		$_REQUEST['voSupriOm'] = $oFachada->recuperarTodosSupriOm();
        $_REQUEST['voVPessoaGeralFormatada'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();


          $_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItemPorOrcamento($_GET['codOm']);


        $_REQUEST['oSupriOm'] = $_REQUEST['oSupriOm'];
        $_REQUEST['voSupriOmItem'] = $_REQUEST['voSupriOmItem'];

        if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_mapa_compras/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_mapa_compras/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriMapaCompras = $oFachada->inicializarSupriMapaCompras($_POST['fCodMapa'],$_POST['fCodOm'],$_POST['fDataFechamento'],mb_convert_case($_POST['fAprovadorPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fValorAprovado']);
 			$_SESSION['oSupriMapaCompras'] = $oSupriMapaCompras;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodOm", $oSupriMapaCompras->getCodOm(), "number", "y");
			//$oValidate->add_date_field("DataFechamento", $oSupriMapaCompras->getDataFechamento(), "date", "y");
			//$oValidate->add_text_field("AprovadorPor", $oSupriMapaCompras->getAprovadorPor(), "text", "y");
			//$oValidate->add_number_field("ValorAprovado", $oSupriMapaCompras->getValorAprovado(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriMapaCompras.preparaFormulario&sOP=".$sOP."&nIdSupriMapaCompras=".$_POST['fCodMapa'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriMapaCompras($oSupriMapaCompras)){
 					unset($_SESSION['oSupriMapaCompras']);
 					$_SESSION['sMsg'] = "supri_mapa_compras inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaCompras.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_mapa_compras!";
 					$sHeader = "?bErro=1&action=SupriMapaCompras.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriMapaCompras($oSupriMapaCompras)){
 					unset($_SESSION['oSupriMapaCompras']);
 					$_SESSION['sMsg'] = "supri_mapa_compras alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaCompras.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_mapa_compras!";
 										$sHeader = "?bErro=1&action=SupriMapaCompras.preparaFormulario&sOP=".$sOP."&nIdSupriMapaCompras=".$_POST['fCodMapa'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriMapaCompras']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriMapaCompras($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_mapa_compras(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaCompras.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_mapa_compras!";
 					$sHeader = "?bErro=1&action=SupriMapaCompras.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}


    public function preparaOm(){

        $oFachadaSuprimento = new FachadaSuprimentoBD();
        $nCodOm = $_REQUEST['codOm'];
        $_REQUEST['oSupriOm'] = $oFachadaSuprimento->recuperarUmSupriOm($nCodOm);
        $_REQUEST['voSupriOmItem'] = $oFachadaSuprimento->recuperarTodosSupriOmItemPorOrcamento($nCodOm);

        //include_once('view/Suprimento/supri_mapa_compras/produtos_om_ajax.php');
       $sHeader = "?action=SupriMapaCompras.preparaFormulario&sOP=Cadastrar&codOm=".$nCodOm;
        header("Location: ".$sHeader);

        //$this->preparaFormulario();

        exit();
    }

 }


 ?>
