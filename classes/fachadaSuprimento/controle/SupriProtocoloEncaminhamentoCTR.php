<?php
 class SupriProtocoloEncaminhamentoCTR implements IControle{

 	public function SupriProtocoloEncaminhamentoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriProtocoloEncaminhamento = $oFachada->recuperarTodosSupriProtocoloEncaminhamento();

 		$_REQUEST['voSupriProtocoloEncaminhamento'] = $voSupriProtocoloEncaminhamento;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();


 		include_once("view/Suprimento/supri_protocolo_encaminhamento/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriProtocoloEncaminhamento = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriProtocoloEncaminhamento = ($_POST['fIdSupriProtocoloEncaminhamento'][0]) ? $_POST['fIdSupriProtocoloEncaminhamento'][0] : $_GET['nIdSupriProtocoloEncaminhamento'];

 			if($nIdSupriProtocoloEncaminhamento){
 				$vIdSupriProtocoloEncaminhamento = explode("||",$nIdSupriProtocoloEncaminhamento);
 				$oSupriProtocoloEncaminhamento = $oFachada->recuperarUmSupriProtocoloEncaminhamento($vIdSupriProtocoloEncaminhamento[0]);
 			}
 		}

 		$_REQUEST['oSupriProtocoloEncaminhamento'] = ($_SESSION['oSupriProtocoloEncaminhamento']) ? $_SESSION['oSupriProtocoloEncaminhamento'] : $oSupriProtocoloEncaminhamento;
 		unset($_SESSION['oSupriProtocoloEncaminhamento']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_protocolo_encaminhamento/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_protocolo_encaminhamento/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriProtocoloEncaminhamento = $oFachada->inicializarSupriProtocoloEncaminhamento($_POST['fCodProtocoloEncaminhamento'],$_POST['fDe'],$_POST['fPara'],$_POST['fInterno'],$_POST['fOrdem'],$_POST['fAtivo']);
 			$_SESSION['oSupriProtocoloEncaminhamento'] = $oSupriProtocoloEncaminhamento;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("De", $oSupriProtocoloEncaminhamento->getDe(), "number", "y");
			$oValidate->add_number_field("Para", $oSupriProtocoloEncaminhamento->getPara(), "number", "y");
			//$oValidate->add_number_field("Interno", $oSupriProtocoloEncaminhamento->getInterno(), "number", "y");
			//$oValidate->add_number_field("Ordem", $oSupriProtocoloEncaminhamento->getOrdem(), "number", "y");
			$oValidate->add_number_field("Ativo", $oSupriProtocoloEncaminhamento->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP."&nIdSupriProtocoloEncaminhamento=".$_POST['fCodProtocoloEncaminhamento'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriProtocoloEncaminhamento($oSupriProtocoloEncaminhamento)){
 					unset($_SESSION['oSupriProtocoloEncaminhamento']);
 					$_SESSION['sMsg'] = "Encaminhamentos inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProtocoloEncaminhamento.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Encaminhamentos!";
 					$sHeader = "?bErro=1&action=SupriProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriProtocoloEncaminhamento($oSupriProtocoloEncaminhamento)){
 					unset($_SESSION['oSupriProtocoloEncaminhamento']);
 					$_SESSION['sMsg'] = "Encaminhamentos alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProtocoloEncaminhamento.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Encaminhamentos!";
 										$sHeader = "?bErro=1&action=SupriProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP."&nIdSupriProtocoloEncaminhamento=".$_POST['fCodProtocoloEncaminhamento'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriProtocoloEncaminhamento']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriProtocoloEncaminhamento($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "Encaminhamentos(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProtocoloEncaminhamento.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Encaminhamentos!";
 					$sHeader = "?bErro=1&action=SupriProtocoloEncaminhamento.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
