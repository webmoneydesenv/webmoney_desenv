<?php
 class SupriOmCTR implements IControle{

 	public function SupriOmCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriOm = $oFachada->recuperarTodosSupriOm();

 		$_REQUEST['voSupriOm'] = $voSupriOm;
 		//$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();

		//$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();

		//$_REQUEST['voSysEmpresa'] = $oFachada->recuperarTodosSysEmpresa();


 		include_once("view/Suprimento/supri_om/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();
 		$oFachadaView = new FachadaViewBD();

 		$oSupriOm = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriOm = ($_POST['fIdSupriOm'][0]) ? $_POST['fIdSupriOm'][0] : $_GET['nIdSupriOm'];

 			if($nIdSupriOm){
 				$vIdSupriOm = explode("||",$nIdSupriOm);
 				$oSupriOm = $oFachada->recuperarUmSupriOm($vIdSupriOm[0]);
 			}
 		}

 		$_REQUEST['oSupriOm'] = ($_SESSION['oSupriOm']) ? $_SESSION['oSupriOm'] : $oSupriOm;
 		unset($_SESSION['oSupriOm']);

 		//$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();

		$_REQUEST['voVPessoaGeralFormatada'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();

		//$_REQUEST['voSysEmpresa'] = $oFachada->recuperarTodosSysEmpresa();

        $_REQUEST['voSupriScm'] = $oFachada->recuperarTodosSupriScm();



        $_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItemPorSupriScm2($_REQUEST['nCodProduto']);




        $_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItemPorOrcamento($nIdSupriOm);

 		if($_REQUEST['sOP'] == "Detalhar"){

            $_REQUEST['oSupriMapaCompras'] = $oFachada->recuperarUmSupriMapaComprasPorOrcamento($oSupriOm->getCodOm());

 			include_once("view/Suprimento/supri_om/detalhe.php");
        }else{

            if($_GET['nCodScm']){
                $_REQUEST['oSupriScm'] = $oFachada->recuperarUmSupriScm($_GET['nCodScm']);
                $_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItemPorSupriScm2($_GET['nCodScm']);

            }



            //print_r($_REQUEST['voSupriOmItem']);
            //die();

 			include_once("view/Suprimento/supri_om/insere_altera.php");
        }
 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();


 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriOm = $oFachada->inicializarSupriOm($_POST['fCodOm'],$_POST['fEmpCodigo'],$_POST['fCodPessoaComprador'],mb_convert_case($_POST['fEntregaEndereco'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEntregaContato'],MB_CASE_UPPER, "UTF-8"),$_POST['fEntregaFone'],$_POST['fCodFornecedor'],$_POST['fResposta'],$_POST['fFechamento'],$_POST['fPrevisao'],mb_convert_case($_POST['fObservacao'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oSupriOm'] = $oSupriOm;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("EmpCodigo", $oSupriOm->getEmpCodigo(), "number", "y");
			//$oValidate->add_number_field("CodPessoaComprador", $oSupriOm->getCodPessoaComprador(), "number", "y");
			//$oValidate->add_text_field("EntregaEndereco", $oSupriOm->getEntregaEndereco(), "text", "y");
			//$oValidate->add_text_field("EntregaContato", $oSupriOm->getEntregaContato(), "text", "y");
			//$oValidate->add_number_field("EntregaFone", $oSupriOm->getEntregaFone(), "number", "y");
			//$oValidate->add_number_field("CodFornecedor", $oSupriOm->getCodFornecedor(), "number", "y");
			//$oValidate->add_date_field("Resposta", $oSupriOm->getResposta(), "date", "y");
			//$oValidate->add_date_field("Fechamento", $oSupriOm->getFechamento(), "date", "y");
			//$oValidate->add_date_field("Previsao", $oSupriOm->getPrevisao(), "date", "y");
			//$oValidate->add_text_field("Observacao", $oSupriOm->getObservacao(), "text", "y");



            //print_r($oSupriOm);
            //die();


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriOm.preparaFormulario&sOP=".$sOP."&nIdSupriOm=".$_POST['fCodOm'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($nCodOm = $oFachada->inserirSupriOm($oSupriOm)){
 					unset($_SESSION['oSupriOm']);
 					$_SESSION['sMsg'] = "supri_om inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOm.preparaLista";

                    //$oSupriOm = $oFachada->recuperarUmSupriOm();

                   if($_POST['fCodProduto']){
                        $nItem = $oFachada->recuperarUmSupriOmItemPorMaiorItem($nCodOm);

                        //print_r($nItem );die();

                        $_POST['fCodOmItem']  = ($nItem && $nItem->getCodOmItem())?  ($nItem->getCodOmItem() + 1) : 1;

                        for($i = 0; count($_POST['fQtdeOrc']) > $i; $i++){

                            if($_POST['fQtdeOrc'][$i]>0){
                               $v1[] = $_POST['fCodScmItem'][$i];
                            }
                        }

                        /*
                        print_r($v1);
                        echo "<br>";
                        print_r($v2);
                        die();
                        */


                        foreach(array_filter($_POST['fQtdeOrc']) as $key=>$value){
                           $QtdeOc[] = $value;
                        }

                        foreach(array_filter($v1) as $key=>$value){
                           $vCodScmItem[] = $value;
                        }

                        //print_r(count($_POST['fQtdeOrc']));
                        //print_r($QtdeOc);
                        //echo "<br>";
                        //print_r($vScmITem);

                       // print_r(count($_POST['fCodProduto']));
                       // die();

                        $oSupriScm = $oFachada->recuperarUmSupriScm($_REQUEST['fCodScm']);
                        $nCodScm = $oSupriScm->getCodScm();


                        //print_r($vCodScmItem);
                        //die();


                        for($i=0;$i<=count($_POST['fCodProduto']);$i++){
                            //$vItemSolicitado = explode("||",$vScmITem[$i]);
                            $oSupriScmItem = $oFachada->recuperarUmSupriScmItem($nCodScm ,$vCodScmItem[$i]);

                           // print_r($oSupriScmItem->getCodScmItem() );
                           // die();

                            $_POST['fQtde']  = $QtdeOc[$i];
                            $teste[]  = $QtdeOc[$i];
                            $_POST['fCodScm'] = $nCodScm ;
                            $_POST['CodScmItem'] = $oSupriScmItem->getCodScmItem();
                            $_POST['fAplicacao'] = $oSupriScmItem->getAplicacao();
                            $_POST['fObeservacao'] = $oSupriScm->getObservacao();
                            $_POST['fCodProduto'] = $oSupriScmItem->getCodProduto();
                            $_POST['fTipoMaterial'] =  "null";
                            $oSupriOmItem = $oFachada->inicializarSupriOmItem($_POST['fCodOm'],($_POST['fCodOmItem']++),$_POST['fCodScm'],$_POST['CodScmItem'],$_POST['fTipoMaterial'],mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fObeservacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodProduto'],$_POST['fQtde']);


                            //$oSupriScmItem = $oFachada->recuperarUmSupriScmItem($vItemSolicitado[0],$vItemSolicitado[1]);
                            // print_r($oSupriScmItem);
                           // echo "<br>";


                            if($oFachada->inserirSupriOmItem($oSupriOmItem)){

                                $oSupriScmItem->setQtdeAtendida($_POST['fQtde']);
                                $oSupriScmItem = $oFachada->alterarSupriScmItem($oSupriScmItem);
                                //print_r($oSupriScmItem);
                            }

                            //print_r($oSupriOmItem);
                        }

                        //print_r($teste);
                       // die('aqui');

                    }






 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Orçamento!";
 					$sHeader = "?bErro=1&action=SupriOm.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriOm($oSupriOm)){
 					unset($_SESSION['oSupriOm']);

                    if($_POST['fCodProduto']){
                        $nItem = $oFachada->recuperarUmSupriOmItemPorMaiorItem($oSupriOm->getCodOm());

                        $_POST['fCodOmItem']  = ($nItem && $nItem->getCodOmItem())?  ($nItem->getCodOmItem() + 1) : 1;

                        $oSupriScm = $oFachada->recuperarUmSupriScm($_REQUEST['fCodScm']);
                        $nCodScm = $oSupriScm->getCodScm();

                        $j =count($_POST['fCodProduto']) ;
                        for($i=0;$i<$j;$i++){
                            if($_POST['fQtdeOrc'][$i] > 0 ){
                                $QtdeOc =$_POST['fQtdeOrc'][$i];
                                $oSupriScmItemOrc = $oFachada->recuperarUmSupriScmItem($nCodScm ,$_POST['fCodScmItem'][$i]);

                                $_POST['fQtde']  = $QtdeOc;
                                $teste[]  = $QtdeOc[$i];
                                $_POST['fCodScm'] = $nCodScm;
                                $_POST['CodScmItem'] = $oSupriScmItemOrc->getCodScmItem();
                                $_POST['fAplicacao'] = $oSupriScmItemOrc->getAplicacao();
                                $_POST['fObeservacao'] = $oSupriScm->getObservacao();
                                $_POST['fCodProduto'] = $oSupriScmItemOrc->getCodProduto();
                                $_POST['fTipoMaterial'] =  "null";

                                $oSupriOmItem = $oFachada->inicializarSupriOmItem($_POST['fCodOm'],($_POST['fCodOmItem']++),$_POST['fCodScm'],$_POST['CodScmItem'],$_POST['fTipoMaterial'],mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fObeservacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodProduto'],$_POST['fQtde']);

                                if(!$oFachada->inserirSupriOmItem($oSupriOmItem)){
                                    die('erro inserirSupriOmItem');
                                }
                            }

                        }

                    }

 					$_SESSION['sMsg'] = "Orçamento alterado com sucesso!";
 					$sHeader = "?bErro=1&action=SupriOm.preparaFormulario&sOP=".$sOP."&nIdSupriOm=".$_POST['fCodOm'];

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Orçamento!";
 										$sHeader = "?bErro=1&action=SupriOm.preparaFormulario&sOP=".$sOP."&nIdSupriOm=".$_POST['fCodOm'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriOm']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriOm($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_om(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOm.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_om!";
 					$sHeader = "?bErro=1&action=SupriOm.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}


    public function preparaProdutosSolicitacao(){
        $oFachada = new FachadaSuprimentoBD();
        $_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItemPorSupriScm3($_REQUEST['nCodScm']);

        $_REQUEST['voSupriScmItem'];

        include('view/Suprimento/supri_om/produtos_ajax.php');

    }


 }


 ?>
