<?php
 class SupriScmProtocoloCTR implements IControle{

 	public function SupriScmProtocoloCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriScmProtocolo = $oFachada->recuperarTodosSupriScmProtocolo();

 		$_REQUEST['voSupriScmProtocolo'] = $voSupriScmProtocolo;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();



 		include_once("view/Suprimento/supri_scm_protocolo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriScmProtocolo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriScmProtocolo = ($_POST['fIdSupriScmProtocolo'][0]) ? $_POST['fIdSupriScmProtocolo'][0] : $_GET['nIdSupriScmProtocolo'];

 			if($nIdSupriScmProtocolo){
 				$vIdSupriScmProtocolo = explode("||",$nIdSupriScmProtocolo);
 				$oSupriScmProtocolo = $oFachada->recuperarUmSupriScmProtocolo($vIdSupriScmProtocolo[0]);
 			}
 		}

 		$_REQUEST['oSupriScmProtocolo'] = ($_SESSION['oSupriScmProtocolo']) ? $_SESSION['oSupriScmProtocolo'] : $oSupriScmProtocolo;
 		unset($_SESSION['oSupriScmProtocolo']);

 		//$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

		//$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_scm_protocolo/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_scm_protocolo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriScmProtocolo = $oFachada->inicializarSupriScmProtocolo($_POST['fCodProtocoloScm'],$_POST['fCodScm'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);

           // print_r($oSupriScmProtocolo);
            //die();



 			$_SESSION['oSupriScmProtocolo'] = $oSupriScmProtocolo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodScm", $oSupriScmProtocolo->getCodScm(), "number", "y");
			//$oValidate->add_number_field("DeCodTipoProtocolo", $oSupriScmProtocolo->getDeCodTipoProtocolo(), "number", "y");
			//$oValidate->add_number_field("ParaCodTipoProtocolo", $oSupriScmProtocolo->getParaCodTipoProtocolo(), "number", "y");
			//$oValidate->add_text_field("Responsavel", $oSupriScmProtocolo->getResponsavel(), "text", "y");
			//$oValidate->add_date_field("Data", $oSupriScmProtocolo->getData(), "date", "y");
			//$oValidate->add_text_field("Descricao", $oSupriScmProtocolo->getDescricao(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oSupriScmProtocolo->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriScmProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriScmProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriScmProtocolo($oSupriScmProtocolo)){
 					unset($_SESSION['oSupriScmProtocolo']);
 					$_SESSION['sMsg'] = "supri_scm_protocolo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScm.preparaFormulario&sOP=Detalhar&nIdSupriScm=".$_POST['fCodScm'];

 				}else{
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_scm_protocolo!";
 					$sHeader = "?bErro=1&action=SupriScm.preparaFormulario&sOP=Detalhar&nIdSupriScm=".$_POST['fCodScm'];
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriScmProtocolo($oSupriScmProtocolo)){
 					unset($_SESSION['oSupriScmProtocolo']);
 					$_SESSION['sMsg'] = "supri_scm_protocolo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmProtocolo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_scm_protocolo!";
 										$sHeader = "?bErro=1&action=SupriScmProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriScmProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriScmProtocolo']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriScmProtocolo($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_scm_protocolo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmProtocolo.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_scm_protocolo!";
 					$sHeader = "?bErro=1&action=SupriScmProtocolo.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
