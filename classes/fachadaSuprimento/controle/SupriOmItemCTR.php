<?php
 class SupriOmItemCTR implements IControle{

 	public function SupriOmItemCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriOmItem = $oFachada->recuperarTodosSupriOmItem();

 		$_REQUEST['voSupriOmItem'] = $voSupriOmItem;
 		$_REQUEST['voSupriOm'] = $oFachada->recuperarTodosSupriOm();

		$_REQUEST['voSupriProduto'] = $oFachada->recuperarTodosSupriProduto();


 		include_once("view/Suprimento/supri_om_item/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriOmItem = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriOmItem = ($_POST['fIdSupriOmItem'][0]) ? $_POST['fIdSupriOmItem'][0] : $_GET['nIdSupriOmItem'];

 			if($nIdSupriOmItem){
 				$vIdSupriOmItem = explode("||",$nIdSupriOmItem);
                $oSupriOmItem = $oFachada->recuperarUmSupriOmItem2($vIdSupriOmItem[0],$vIdSupriOmItem[1]);

                //print_r($_REQUEST['oSupriOmItem']);
                //die();

 				//$oSupriOmItem = $oFachada->recuperarUmSupriOmItem($vIdSupriOmItem[0],$vIdSupriOmItem[1]);
 			}
 		}

 		$_REQUEST['oSupriOmItem'] = ($_SESSION['oSupriOmItem']) ? $_SESSION['oSupriOmItem'] : $oSupriOmItem;
 		unset($_SESSION['oSupriOmItem']);

 		//$_REQUEST['voSupriOm'] = $oFachada->recuperarTodosSupriOm();

		//$_REQUEST['voSupriProduto'] = $oFachada->recuperarTodosSupriProduto();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_om_item/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_om_item/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriOmItem = $oFachada->inicializarSupriOmItem($_POST['fCodOm'],$_POST['fCodOmItem'],$_POST['fCodScm'],$_POST['fCodScmItem'],$_POST['fTipoMaterial'],mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fObeservacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodProduto'],$_POST['fQtde']);
 			$_SESSION['oSupriOmItem'] = $oSupriOmItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodOmItem", $oSupriOmItem->getCodOmItem(), "number", "y");
			//$oValidate->add_number_field("TipoMaterial", $oSupriOmItem->getTipoMaterial(), "number", "y");
			//$oValidate->add_text_field("Aplicacao", $oSupriOmItem->getAplicacao(), "text", "y");
			//$oValidate->add_text_field("Obeservacao", $oSupriOmItem->getObeservacao(), "text", "y");
			//$oValidate->add_number_field("CodProduto", $oSupriOmItem->getCodProduto(), "number", "y");
			//$oValidate->add_number_field("Qtde", $oSupriOmItem->getQtde(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriOmItem.preparaFormulario&sOP=".$sOP."&nIdSupriOmItem=".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriOmItem($oSupriOmItem)){
 					unset($_SESSION['oSupriOmItem']);
 					$_SESSION['sMsg'] = "supri_om_item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_om_item!";
 					$sHeader = "?bErro=1&action=SupriOmItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriOmItem($oSupriOmItem)){
 					unset($_SESSION['oSupriOmItem']);
 					$_SESSION['sMsg'] = "supri_om_item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_om_item!";
 										$sHeader = "?bErro=1&action=SupriOmItem.preparaFormulario&sOP=".$sOP."&nIdSupriOmItem=".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriOmItem']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriOmItem($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_om_item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItem.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_om_item!";
 					$sHeader = "?bErro=1&action=SupriOmItem.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

    public function preparaItem(){

        $oFachada = new FachadaSuprimentoBD();

        $a = explode(',',$_GET['Teste']);

        foreach($a as $key=>$value){
            $a = explode('.',$value);

             $a[0] . "||" . $a[1];
               $Prod = $oFachada->recuperarUmSupriScmItemOmItemAjax($a[0] ,$a[1] );
            echo "
            <input type='hidden' value='".$a[0] . "||" . $a[1]."'>
            <div class='form-group' >
                <div class='col-sm-1'><button  type='button' class='btn btn-danger removeButton' ><i class='glyphicon glyphicon-minus'></i></button></div>
                <div class='col-sm-3'>".$Prod->numero_solicitacao."</div>
                 <div class='col-sm-3'>".$Prod->descricao."</div>
                <div class='col-sm-1'>".$Prod->qtde."</div>
                <div class='col-sm-1'><input type='text' value='".$Prod->qtde."' name='fNovaQtde'></div>
            </div>
        ";
        }




        die();

    }



 }


 ?>
