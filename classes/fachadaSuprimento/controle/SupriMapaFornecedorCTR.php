<?php
 class SupriMapaFornecedorCTR implements IControle{

 	public function SupriMapaFornecedorCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriMapaFornecedor = $oFachada->recuperarTodosSupriMapaFornecedor();

 		$_REQUEST['voSupriMapaFornecedor'] = $voSupriMapaFornecedor;
 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();


 		include_once("view/Suprimento/supri_mapa_fornecedor/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriMapaFornecedor = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriMapaFornecedor = ($_POST['fIdSupriMapaFornecedor'][0]) ? $_POST['fIdSupriMapaFornecedor'][0] : $_GET['nIdSupriMapaFornecedor'];

 			if($nIdSupriMapaFornecedor){
 				$vIdSupriMapaFornecedor = explode("||",$nIdSupriMapaFornecedor);
 				$oSupriMapaFornecedor = $oFachada->recuperarUmSupriMapaFornecedor($vIdSupriMapaFornecedor[0],$vIdSupriMapaFornecedor[1]);
 			}
 		}

 		$_REQUEST['oSupriMapaFornecedor'] = ($_SESSION['oSupriMapaFornecedor']) ? $_SESSION['oSupriMapaFornecedor'] : $oSupriMapaFornecedor;
 		unset($_SESSION['oSupriMapaFornecedor']);

 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_mapa_fornecedor/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_mapa_fornecedor/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriMapaFornecedor = $oFachada->inicializarSupriMapaFornecedor($_POST['fCodMapa'],$_POST['fCodFornecedor'],mb_convert_case($_POST['fContato'],MB_CASE_UPPER, "UTF-8"),$_POST['fFone'],$_POST['fCondicaoPagamento'],$_POST['fDescPercentual'],$_POST['fDescValor'],$_POST['fFreteCifFob'],mb_convert_case($_POST['fObs'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oSupriMapaFornecedor'] = $oSupriMapaFornecedor;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodFornecedor", $oSupriMapaFornecedor->getCodFornecedor(), "number", "y");
			//$oValidate->add_text_field("Contato", $oSupriMapaFornecedor->getContato(), "text", "y");
			//$oValidate->add_number_field("Fone", $oSupriMapaFornecedor->getFone(), "number", "y");
			//$oValidate->add_number_field("CondicaoPagamento", $oSupriMapaFornecedor->getCondicaoPagamento(), "number", "y");
			//$oValidate->add_number_field("DescPercentual", $oSupriMapaFornecedor->getDescPercentual(), "number", "y");
			//$oValidate->add_number_field("DescValor", $oSupriMapaFornecedor->getDescValor(), "number", "y");
			//$oValidate->add_number_field("FreteCifFob", $oSupriMapaFornecedor->getFreteCifFob(), "number", "y");
			//$oValidate->add_text_field("Obs", $oSupriMapaFornecedor->getObs(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriMapaFornecedor.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedor=".$_POST['fCodMapa']."||".$_POST['fCodFornecedor'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriMapaFornecedor($oSupriMapaFornecedor)){
 					unset($_SESSION['oSupriMapaFornecedor']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedor.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_mapa_fornecedor!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedor.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriMapaFornecedor($oSupriMapaFornecedor)){
 					unset($_SESSION['oSupriMapaFornecedor']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedor.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_mapa_fornecedor!";
 										$sHeader = "?bErro=1&action=SupriMapaFornecedor.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedor=".$_POST['fCodMapa']."||".$_POST['fCodFornecedor'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriMapaFornecedor']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriMapaFornecedor($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedor.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_mapa_fornecedor!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedor.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
