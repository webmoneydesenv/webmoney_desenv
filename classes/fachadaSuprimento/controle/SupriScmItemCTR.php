<?php
 class SupriScmItemCTR implements IControle{

 	public function SupriScmItemCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriScmItem = $oFachada->recuperarTodosSupriScmItem();

 		$_REQUEST['voSupriScmItem'] = $voSupriScmItem;
 		$_REQUEST['voSupriScm'] = $oFachada->recuperarTodosSupriScm();


 		include_once("view/Suprimento/supri_scm_item/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriScmItem = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriScmItem = ($_POST['fIdSupriScmItem'][0]) ? $_POST['fIdSupriScmItem'][0] : $_GET['nIdSupriScmItem'];

 			if($nIdSupriScmItem){
 				$vIdSupriScmItem = explode("||",$nIdSupriScmItem);
                $oSupriScmItem = $oFachada->recuperarUmSupriScmItem($vIdSupriScmItem[0],$vIdSupriScmItem[1]);
 			}else{

                $oSupriScmItem = $oFachada->recuperarUmSupriScmItem2($_GET['nCodScm'],$_GET['nItem']);
            }
 		}

        //print_r($_REQUEST['oSupriScmItem']);
        //die('die');

 		$_REQUEST['oSupriScmItem'] = ($_SESSION['oSupriScmItem']) ? $_SESSION['oSupriScmItem'] : $oSupriScmItem;
 		unset($_SESSION['oSupriScmItem']);

 		$_REQUEST['voSupriScm'] = $oFachada->recuperarTodosSupriScm();


        $_REQUEST['voSupriProtocoloEncaminhamento'] = $oFachada->recuperarTodosSupriProtocoloEncaminhamentoPorOrigem(15);


 		if($_REQUEST['sOP'] == "Detalhar"){

            $_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItemPorItemSolicitacao($oSupriScmItem->cod_scm,$oSupriScmItem->cod_produto);

 			include_once("view/Suprimento/supri_scm_item/detalhe.php");
        }else{
 			include_once("view/Suprimento/supri_scm_item/insere_altera.php");
        }
 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriScmItem = $oFachada->inicializarSupriScmItem($_POST['fCodScm'],$_POST['fCodScmItem'],$_POST['fCodProduto'],$_POST['fQtde'],mb_convert_case($_POST['fProjeto'],MB_CASE_UPPER, "UTF-8"),$_POST['fRevisao'],$_POST['fConsumoMensal'],mb_convert_case($_POST['fAplicacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataPrevEntrega'],mb_convert_case($_POST['fEntrega'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fQtdeAtendida'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oSupriScmItem'] = $oSupriScmItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodScm", $oSupriScmItem->getCodScm(), "number", "y");
			$oValidate->add_number_field("CodScmItem", $oSupriScmItem->getCodScmItem(), "number", "y");
			//$oValidate->add_number_field("CodProduto", $oSupriScmItem->getCodProduto(), "number", "y");
			//$oValidate->add_number_field("Qtde", $oSupriScmItem->getQtde(), "number", "y");
			//$oValidate->add_text_field("Projeto", $oSupriScmItem->getProjeto(), "text", "y");
			//$oValidate->add_number_field("Revisao", $oSupriScmItem->getRevisao(), "number", "y");
			//$oValidate->add_number_field("ConsumoMensal", $oSupriScmItem->getConsumoMensal(), "number", "y");
			//$oValidate->add_text_field("Aplicacao", $oSupriScmItem->getAplicacao(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriScmItem.preparaFormulario&sOP=".$sOP."&nIdSupriScmItem=".$_POST['fCodScm']."||".$_POST['fCodScmItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriScmItem($oSupriScmItem)){
 					unset($_SESSION['oSupriScmItem']);
 					$_SESSION['sMsg'] = "supri_scm_item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_scm_item!";
 					$sHeader = "?bErro=1&action=SupriScmItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriScmItem($oSupriScmItem)){
 					unset($_SESSION['oSupriScmItem']);
 					$_SESSION['sMsg'] = "supri_scm_item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_scm_item!";
 										$sHeader = "?bErro=1&action=SupriScmItem.preparaFormulario&sOP=".$sOP."&nIdSupriScmItem=".$_POST['fCodScm']."||".$_POST['fCodScmItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriScmItem']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriScmItem($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_scm_item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItem.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_scm_item!";
 					$sHeader = "?bErro=1&action=SupriScmItem.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
