<?php
 class SupriMapaFornecedorItemCTR implements IControle{

 	public function SupriMapaFornecedorItemCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriMapaFornecedorItem = $oFachada->recuperarTodosSupriMapaFornecedorItem();

 		$_REQUEST['voSupriMapaFornecedorItem'] = $voSupriMapaFornecedorItem;
 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();


 		include_once("view/Suprimento/supri_mapa_fornecedor_item/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriMapaFornecedorItem = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriMapaFornecedorItem = ($_POST['fIdSupriMapaFornecedorItem'][0]) ? $_POST['fIdSupriMapaFornecedorItem'][0] : $_GET['nIdSupriMapaFornecedorItem'];

 			if($nIdSupriMapaFornecedorItem){
 				$vIdSupriMapaFornecedorItem = explode("||",$nIdSupriMapaFornecedorItem);
 				$oSupriMapaFornecedorItem = $oFachada->recuperarUmSupriMapaFornecedorItem($vIdSupriMapaFornecedorItem[0],$vIdSupriMapaFornecedorItem[1],$vIdSupriMapaFornecedorItem[2],$vIdSupriMapaFornecedorItem[3]);
 			}
 		}

 		$_REQUEST['oSupriMapaFornecedorItem'] = ($_SESSION['oSupriMapaFornecedorItem']) ? $_SESSION['oSupriMapaFornecedorItem'] : $oSupriMapaFornecedorItem;
 		unset($_SESSION['oSupriMapaFornecedorItem']);

 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_mapa_fornecedor_item/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_mapa_fornecedor_item/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriMapaFornecedorItem = $oFachada->inicializarSupriMapaFornecedorItem($_POST['fCodMapa'],$_POST['fCodFornecedor'],$_POST['fCodOm'],$_POST['fCodOmItem'],$_POST['fValorOrigem'],$_POST['fDifAliquota'],$_POST['fDifValor'],$_POST['fValorFinal'],mb_convert_case($_POST['fObs'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oSupriMapaFornecedorItem'] = $oSupriMapaFornecedorItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodMapa", $oSupriMapaFornecedorItem->getCodMapa(), "number", "y");
			$oValidate->add_number_field("CodFornecedor", $oSupriMapaFornecedorItem->getCodFornecedor(), "number", "y");
			$oValidate->add_number_field("CodOm", $oSupriMapaFornecedorItem->getCodOm(), "number", "y");
			$oValidate->add_number_field("CodOmItem", $oSupriMapaFornecedorItem->getCodOmItem(), "number", "y");
			//$oValidate->add_number_field("ValorOrigem", $oSupriMapaFornecedorItem->getValorOrigem(), "number", "y");
			//$oValidate->add_number_field("DifAliquota", $oSupriMapaFornecedorItem->getDifAliquota(), "number", "y");
			//$oValidate->add_number_field("DifValor", $oSupriMapaFornecedorItem->getDifValor(), "number", "y");
			//$oValidate->add_number_field("ValorFinal", $oSupriMapaFornecedorItem->getValorFinal(), "number", "y");
			//$oValidate->add_text_field("Obs", $oSupriMapaFornecedorItem->getObs(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriMapaFornecedorItem.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedorItem=".$_POST['fCodMapa']."||".$_POST['fCodFornecedor']."||".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriMapaFornecedorItem($oSupriMapaFornecedorItem)){
 					unset($_SESSION['oSupriMapaFornecedorItem']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_mapa_fornecedor_item!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedorItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriMapaFornecedorItem($oSupriMapaFornecedorItem)){
 					unset($_SESSION['oSupriMapaFornecedorItem']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_mapa_fornecedor_item!";
 										$sHeader = "?bErro=1&action=SupriMapaFornecedorItem.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedorItem=".$_POST['fCodMapa']."||".$_POST['fCodFornecedor']."||".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriMapaFornecedorItem']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriMapaFornecedorItem($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorItem.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_mapa_fornecedor_item!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedorItem.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
