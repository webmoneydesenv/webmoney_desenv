<?php
 class SupriItemProtocoloCTR implements IControle{

 	public function SupriItemProtocoloCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriItemProtocolo = $oFachada->recuperarTodosSupriItemProtocolo();

 		$_REQUEST['voSupriItemProtocolo'] = $voSupriItemProtocolo;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();


 		include_once("view/Suprimento/supri_item_protocolo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriItemProtocolo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriItemProtocolo = ($_POST['fIdSupriItemProtocolo'][0]) ? $_POST['fIdSupriItemProtocolo'][0] : $_GET['nIdSupriItemProtocolo'];

 			if($nIdSupriItemProtocolo){
 				$vIdSupriItemProtocolo = explode("||",$nIdSupriItemProtocolo);
 				$oSupriItemProtocolo = $oFachada->recuperarUmSupriItemProtocolo($vIdSupriItemProtocolo[0]);
 			}
 		}

 		$_REQUEST['oSupriItemProtocolo'] = ($_SESSION['oSupriItemProtocolo']) ? $_SESSION['oSupriItemProtocolo'] : $oSupriItemProtocolo;
 		unset($_SESSION['oSupriItemProtocolo']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_item_protocolo/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_item_protocolo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriItemProtocolo = $oFachada->inicializarSupriItemProtocolo($_POST['fCodProtocoloSuprimentoItem'],$_POST['fCodScm'],$_POST['fCodScmItem'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
 			$_SESSION['oSupriItemProtocolo'] = $oSupriItemProtocolo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodScm", $oSupriItemProtocolo->getCodScm(), "number", "y");
			$oValidate->add_number_field("CodScmItem", $oSupriItemProtocolo->getCodScmItem(), "number", "y");
			$oValidate->add_number_field("DeCodTipoProtocolo", $oSupriItemProtocolo->getDeCodTipoProtocolo(), "number", "y");
			$oValidate->add_number_field("ParaCodTipoProtocolo", $oSupriItemProtocolo->getParaCodTipoProtocolo(), "number", "y");
			//$oValidate->add_text_field("Responsavel", $oSupriItemProtocolo->getResponsavel(), "text", "y");
			$oValidate->add_date_field("Data", $oSupriItemProtocolo->getData(), "date", "y");
			//$oValidate->add_text_field("Descricao", $oSupriItemProtocolo->getDescricao(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oSupriItemProtocolo->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriItemProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriItemProtocolo($oSupriItemProtocolo)){
 					unset($_SESSION['oSupriItemProtocolo']);
 					$_SESSION['sMsg'] = "supri_item_protocolo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriItemProtocolo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_item_protocolo!";
 					$sHeader = "?bErro=1&action=SupriItemProtocolo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriItemProtocolo($oSupriItemProtocolo)){
 					unset($_SESSION['oSupriItemProtocolo']);
 					$_SESSION['sMsg'] = "supri_item_protocolo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriItemProtocolo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_item_protocolo!";
 										$sHeader = "?bErro=1&action=SupriItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriItemProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriItemProtocolo']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriItemProtocolo($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_item_protocolo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriItemProtocolo.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_item_protocolo!";
 					$sHeader = "?bErro=1&action=SupriItemProtocolo.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
