<?php
 class SupriProdutoCTR implements IControle{

 	public function SupriProdutoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriProduto = $oFachada->recuperarTodosSupriProduto();

 		$_REQUEST['voSupriProduto'] = $voSupriProduto;
 		$_REQUEST['voSupriUnidade'] = $oFachada->recuperarTodosSupriUnidade();

 		include_once("view/Suprimento/supri_produto/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriProduto = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriProduto = ($_POST['fIdSupriProduto'][0]) ? $_POST['fIdSupriProduto'][0] : $_GET['nIdSupriProduto'];

 			if($nIdSupriProduto){
 				$vIdSupriProduto = explode("||",$nIdSupriProduto);
 				$oSupriProduto = $oFachada->recuperarUmSupriProduto($vIdSupriProduto[0]);
 			}
 		}

 		$_REQUEST['oSupriProduto'] = ($_SESSION['oSupriProduto']) ? $_SESSION['oSupriProduto'] : $oSupriProduto;
 		unset($_SESSION['oSupriProduto']);

 		$_REQUEST['voSupriUnidade'] = $oFachada->recuperarTodosSupriUnidade();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_produto/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_produto/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriProduto = $oFachada->inicializarSupriProduto($_POST['fCodProduto'],$_POST['fCodUnidade'],mb_convert_case($_POST['fClassificacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
 			$_SESSION['oSupriProduto'] = $oSupriProduto;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

			//$oValidate->add_number_field("CodUnidade", $oSupriProduto->getCodUnidade(), "number", "y");
			//$oValidate->add_text_field("Classificacao", $oSupriProduto->getClassificacao(), "text", "y");
			//$oValidate->add_text_field("Descricao", $oSupriProduto->getDescricao(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriProduto.preparaFormulario&sOP=".$sOP."&nIdSupriProduto=".$_POST['fCodProduto'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriProduto($oSupriProduto)){
 					unset($_SESSION['oSupriProduto']);
 					$_SESSION['sMsg'] = "Produto inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProduto.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Produto!";
 					$sHeader = "?bErro=1&action=SupriProduto.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriProduto($oSupriProduto)){
 					unset($_SESSION['oSupriProduto']);
 					$_SESSION['sMsg'] = "Produto alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProduto.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Produto!";
 										$sHeader = "?bErro=1&action=SupriProduto.preparaFormulario&sOP=".$sOP."&nIdSupriProduto=".$_POST['fCodProduto'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriProduto']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriProduto($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "Produto(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriProduto.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Produto!";
 					$sHeader = "?bErro=1&action=SupriProduto.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}


    public function exibeUnidade(){

        $oFachada = new FachadaSuprimentoBD();
        echo $oFachada->recuperarUmSupriProduto($_REQUEST['nCodProduto'])->getSupriUnidade()->getDescricao();
        exit();


    }

 }


 ?>
