<?php
 class SupriUnidadeCTR implements IControle{

 	public function SupriUnidadeCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriUnidade = $oFachada->recuperarTodosSupriUnidade();

 		$_REQUEST['voSupriUnidade'] = $voSupriUnidade;


 		include_once("view/Suprimento/supri_unidade/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriUnidade = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriUnidade = ($_POST['fIdSupriUnidade'][0]) ? $_POST['fIdSupriUnidade'][0] : $_GET['nIdSupriUnidade'];

 			if($nIdSupriUnidade){
 				$vIdSupriUnidade = explode("||",$nIdSupriUnidade);
 				$oSupriUnidade = $oFachada->recuperarUmSupriUnidade($vIdSupriUnidade[0]);
 			}
 		}

 		$_REQUEST['oSupriUnidade'] = ($_SESSION['oSupriUnidade']) ? $_SESSION['oSupriUnidade'] : $oSupriUnidade;
 		unset($_SESSION['oSupriUnidade']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_unidade/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_unidade/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriUnidade = $oFachada->inicializarSupriUnidade($_POST['fCodUnidade'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
 			$_SESSION['oSupriUnidade'] = $oSupriUnidade;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_text_field("Descricao", $oSupriUnidade->getDescricao(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriUnidade.preparaFormulario&sOP=".$sOP."&nIdSupriUnidade=".$_POST['fCodUnidade'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriUnidade($oSupriUnidade)){
 					unset($_SESSION['oSupriUnidade']);
 					$_SESSION['sMsg'] = "Unidade inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Unidade!";
 					$sHeader = "?bErro=1&action=SupriUnidade.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriUnidade($oSupriUnidade)){
 					unset($_SESSION['oSupriUnidade']);
 					$_SESSION['sMsg'] = "Unidade alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Unidade!";
 										$sHeader = "?bErro=1&action=SupriUnidade.preparaFormulario&sOP=".$sOP."&nIdSupriUnidade=".$_POST['fCodUnidade'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriUnidade']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriUnidade($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "Unidade(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriUnidade.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Unidade!";
 					$sHeader = "?bErro=1&action=SupriUnidade.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
