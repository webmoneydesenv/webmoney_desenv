<?php
 class SupriMapaFornecedorFreteCTR implements IControle{

 	public function SupriMapaFornecedorFreteCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriMapaFornecedorFrete = $oFachada->recuperarTodosSupriMapaFornecedorFrete();

 		$_REQUEST['voSupriMapaFornecedorFrete'] = $voSupriMapaFornecedorFrete;
 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();
		$_REQUEST['voMnyPessoaJuridica'] = $oFachada->recuperarTodosMnyPessoaJuridica();


 		include_once("view/Suprimento/supri_mapa_fornecedor_frete/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriMapaFornecedorFrete = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriMapaFornecedorFrete = ($_POST['fIdSupriMapaFornecedorFrete'][0]) ? $_POST['fIdSupriMapaFornecedorFrete'][0] : $_GET['nIdSupriMapaFornecedorFrete'];

 			if($nIdSupriMapaFornecedorFrete){
 				$vIdSupriMapaFornecedorFrete = explode("||",$nIdSupriMapaFornecedorFrete);
 				$oSupriMapaFornecedorFrete = $oFachada->recuperarUmSupriMapaFornecedorFrete($vIdSupriMapaFornecedorFrete[0]);
 			}
 		}

 		$_REQUEST['oSupriMapaFornecedorFrete'] = ($_SESSION['oSupriMapaFornecedorFrete']) ? $_SESSION['oSupriMapaFornecedorFrete'] : $oSupriMapaFornecedorFrete;
 		unset($_SESSION['oSupriMapaFornecedorFrete']);

 		$_REQUEST['voSupriMapaCompras'] = $oFachada->recuperarTodosSupriMapaCompras();
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();
		$_REQUEST['voMnyPessoaJuridica'] = $oFachada->recuperarTodosMnyPessoaJuridica();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_mapa_fornecedor_frete/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_mapa_fornecedor_frete/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriMapaFornecedorFrete = $oFachada->inicializarSupriMapaFornecedorFrete($_POST['fCodMapa'],$_POST['fCodFornecedor'],$_POST['fCodForma'],$_POST['fCodTransportadora'],$_POST['fPrazoEntrega'],mb_convert_case($_POST['fObs'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fContato'],MB_CASE_UPPER, "UTF-8"),$_POST['fFone']);
 			$_SESSION['oSupriMapaFornecedorFrete'] = $oSupriMapaFornecedorFrete;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodFornecedor", $oSupriMapaFornecedorFrete->getCodFornecedor(), "number", "y");
			//$oValidate->add_number_field("CodForma", $oSupriMapaFornecedorFrete->getCodForma(), "number", "y");
			//$oValidate->add_number_field("CodTransportadora", $oSupriMapaFornecedorFrete->getCodTransportadora(), "number", "y");
			//$oValidate->add_date_field("PrazoEntrega", $oSupriMapaFornecedorFrete->getPrazoEntrega(), "date", "y");
			//$oValidate->add_text_field("Obs", $oSupriMapaFornecedorFrete->getObs(), "text", "y");
			//$oValidate->add_text_field("Contato", $oSupriMapaFornecedorFrete->getContato(), "text", "y");
			//$oValidate->add_date_field("Fone", $oSupriMapaFornecedorFrete->getFone(), "date", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriMapaFornecedorFrete.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedorFrete=".$_POST['fCodMapa'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriMapaFornecedorFrete($oSupriMapaFornecedorFrete)){
 					unset($_SESSION['oSupriMapaFornecedorFrete']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_frete inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorFrete.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_mapa_fornecedor_frete!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedorFrete.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriMapaFornecedorFrete($oSupriMapaFornecedorFrete)){
 					unset($_SESSION['oSupriMapaFornecedorFrete']);
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_frete alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorFrete.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_mapa_fornecedor_frete!";
 										$sHeader = "?bErro=1&action=SupriMapaFornecedorFrete.preparaFormulario&sOP=".$sOP."&nIdSupriMapaFornecedorFrete=".$_POST['fCodMapa'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriMapaFornecedorFrete']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriMapaFornecedorFrete($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_mapa_fornecedor_frete(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriMapaFornecedorFrete.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_mapa_fornecedor_frete!";
 					$sHeader = "?bErro=1&action=SupriMapaFornecedorFrete.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
