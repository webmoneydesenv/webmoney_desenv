<?php
 class SupriScmItemOmItemCTR implements IControle{

 	public function SupriScmItemOmItemCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriScmItemOmItem = $oFachada->recuperarTodosSupriScmItemOmItem();

 		$_REQUEST['voSupriScmItemOmItem'] = $voSupriScmItemOmItem;
 		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();


 		include_once("view/Suprimento/supri_scm_item_om_item/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriScmItemOmItem = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriScmItemOmItem = ($_POST['fIdSupriScmItemOmItem'][0]) ? $_POST['fIdSupriScmItemOmItem'][0] : $_GET['nIdSupriScmItemOmItem'];

 			if($nIdSupriScmItemOmItem){
 				$vIdSupriScmItemOmItem = explode("||",$nIdSupriScmItemOmItem);
 				$oSupriScmItemOmItem = $oFachada->recuperarUmSupriScmItemOmItem($vIdSupriScmItemOmItem[0],$vIdSupriScmItemOmItem[1],$vIdSupriScmItemOmItem[2],$vIdSupriScmItemOmItem[3]);
 			}
 		}

 		$_REQUEST['oSupriScmItemOmItem'] = ($_SESSION['oSupriScmItemOmItem']) ? $_SESSION['oSupriScmItemOmItem'] : $oSupriScmItemOmItem;
 		unset($_SESSION['oSupriScmItemOmItem']);

 		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();
		$_REQUEST['voSupriOmItem'] = $oFachada->recuperarTodosSupriOmItem();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_scm_item_om_item/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_scm_item_om_item/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriScmItemOmItem = $oFachada->inicializarSupriScmItemOmItem($_POST['fCodScm'],$_POST['fCodScmItem'],$_POST['fCodOm'],$_POST['fCodOmItem']);
 			$_SESSION['oSupriScmItemOmItem'] = $oSupriScmItemOmItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodScm", $oSupriScmItemOmItem->getCodScm(), "number", "y");
			$oValidate->add_number_field("CodScmItem", $oSupriScmItemOmItem->getCodScmItem(), "number", "y");
			$oValidate->add_number_field("CodOm", $oSupriScmItemOmItem->getCodOm(), "number", "y");
			$oValidate->add_number_field("CodOmItem", $oSupriScmItemOmItem->getCodOmItem(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriScmItemOmItem.preparaFormulario&sOP=".$sOP."&nIdSupriScmItemOmItem=".$_POST['fCodScm']."||".$_POST['fCodScmItem']."||".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriScmItemOmItem($oSupriScmItemOmItem)){
 					unset($_SESSION['oSupriScmItemOmItem']);
 					$_SESSION['sMsg'] = "supri_scm_item_om_item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItemOmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_scm_item_om_item!";
 					$sHeader = "?bErro=1&action=SupriScmItemOmItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriScmItemOmItem($oSupriScmItemOmItem)){
 					unset($_SESSION['oSupriScmItemOmItem']);
 					$_SESSION['sMsg'] = "supri_scm_item_om_item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItemOmItem.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_scm_item_om_item!";
 										$sHeader = "?bErro=1&action=SupriScmItemOmItem.preparaFormulario&sOP=".$sOP."&nIdSupriScmItemOmItem=".$_POST['fCodScm']."||".$_POST['fCodScmItem']."||".$_POST['fCodOm']."||".$_POST['fCodOmItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriScmItemOmItem']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriScmItemOmItem($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_scm_item_om_item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriScmItemOmItem.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_scm_item_om_item!";
 					$sHeader = "?bErro=1&action=SupriScmItemOmItem.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
