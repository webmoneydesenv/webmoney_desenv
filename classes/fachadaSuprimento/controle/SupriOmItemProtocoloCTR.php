<?php
 class SupriOmItemProtocoloCTR implements IControle{

 	public function SupriOmItemProtocoloCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriOmItemProtocolo = $oFachada->recuperarTodosSupriOmItemProtocolo();

 		$_REQUEST['voSupriOmItemProtocolo'] = $voSupriOmItemProtocolo;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();


 		include_once("view/Suprimento/supri_om_item_protocolo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriOmItemProtocolo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriOmItemProtocolo = ($_POST['fIdSupriOmItemProtocolo'][0]) ? $_POST['fIdSupriOmItemProtocolo'][0] : $_GET['nIdSupriOmItemProtocolo'];

 			if($nIdSupriOmItemProtocolo){
 				$vIdSupriOmItemProtocolo = explode("||",$nIdSupriOmItemProtocolo);
 				$oSupriOmItemProtocolo = $oFachada->recuperarUmSupriOmItemProtocolo($vIdSupriOmItemProtocolo[0]);
 			}
 		}

 		$_REQUEST['oSupriOmItemProtocolo'] = ($_SESSION['oSupriOmItemProtocolo']) ? $_SESSION['oSupriOmItemProtocolo'] : $oSupriOmItemProtocolo;
 		unset($_SESSION['oSupriOmItemProtocolo']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();
		$_REQUEST['voSupriScmItem'] = $oFachada->recuperarTodosSupriScmItem();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_om_item_protocolo/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_om_item_protocolo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriOmItemProtocolo = $oFachada->inicializarSupriOmItemProtocolo($_POST['fCodProtocoloSuprimentoItem'],$_POST['fCodOm'],$_POST['fCodOmItem'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
 			$_SESSION['oSupriOmItemProtocolo'] = $oSupriOmItemProtocolo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodOm", $oSupriOmItemProtocolo->getCodOm(), "number", "y");
			$oValidate->add_number_field("CodOmItem", $oSupriOmItemProtocolo->getCodOmItem(), "number", "y");
			$oValidate->add_number_field("DeCodTipoProtocolo", $oSupriOmItemProtocolo->getDeCodTipoProtocolo(), "number", "y");
			$oValidate->add_number_field("ParaCodTipoProtocolo", $oSupriOmItemProtocolo->getParaCodTipoProtocolo(), "number", "y");
			//$oValidate->add_text_field("Responsavel", $oSupriOmItemProtocolo->getResponsavel(), "text", "y");
			$oValidate->add_date_field("Data", $oSupriOmItemProtocolo->getData(), "date", "y");
			//$oValidate->add_text_field("Descricao", $oSupriOmItemProtocolo->getDescricao(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oSupriOmItemProtocolo->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriOmItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriOmItemProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriOmItemProtocolo($oSupriOmItemProtocolo)){
 					unset($_SESSION['oSupriOmItemProtocolo']);
 					$_SESSION['sMsg'] = "supri_om_item_protocolo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItemProtocolo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_om_item_protocolo!";
 					$sHeader = "?bErro=1&action=SupriOmItemProtocolo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriOmItemProtocolo($oSupriOmItemProtocolo)){
 					unset($_SESSION['oSupriOmItemProtocolo']);
 					$_SESSION['sMsg'] = "supri_om_item_protocolo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItemProtocolo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_om_item_protocolo!";
 										$sHeader = "?bErro=1&action=SupriOmItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdSupriOmItemProtocolo=".$_POST['fCodProtocoloSuprimentoItem'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdSupriOmItemProtocolo']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirSupriOmItemProtocolo($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "supri_om_item_protocolo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupriOmItemProtocolo.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) supri_om_item_protocolo!";
 					$sHeader = "?bErro=1&action=SupriOmItemProtocolo.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
