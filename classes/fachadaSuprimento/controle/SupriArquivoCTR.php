<?php
 class SupriArquivoCTR implements IControle{

 	public function SupriArquivoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuprimentoBD();

 		$voSupriArquivo = $oFachada->recuperarTodosSupriArquivo();

 		$_REQUEST['voSupriArquivo'] = $voSupriArquivo;
 		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();

		$_REQUEST['voSupriScm'] = $oFachada->recuperarTodosSupriScm();

		$_REQUEST['voSupriMapaFornecedor'] = $oFachada->recuperarTodosSupriMapaFornecedor();

		$_REQUEST['voSupriMapaFornecedor'] = $oFachada->recuperarTodosSupriMapaFornecedor();


 		include_once("view/Suprimento/supri_arquivo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$oSupriArquivo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupriArquivo = ($_POST['fIdSupriArquivo'][0]) ? $_POST['fIdSupriArquivo'][0] : $_GET['nIdSupriArquivo'];

 			if($nIdSupriArquivo){
 				$vIdSupriArquivo = explode("||",$nIdSupriArquivo);
 				$oSupriArquivo = $oFachada->recuperarUmSupriArquivo($vIdSupriArquivo[0]);
 			}
 		}

 		$_REQUEST['oSupriArquivo'] = ($_SESSION['oSupriArquivo']) ? $_SESSION['oSupriArquivo'] : $oSupriArquivo;
 		unset($_SESSION['oSupriArquivo']);

 		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();

		$_REQUEST['voSupriScm'] = $oFachada->recuperarTodosSupriScm();

		$_REQUEST['voSupriMapaFornecedor'] = $oFachada->recuperarTodosSupriMapaFornecedor();

		$_REQUEST['voSupriMapaFornecedor'] = $oFachada->recuperarTodosSupriMapaFornecedor();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Suprimento/supri_arquivo/detalhe.php");
 		else
 			include_once("view/Suprimento/supri_arquivo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSuprimentoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSupriArquivo = $oFachada->inicializarSupriArquivo($_POST['fCodArquivo'],mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fArquivo'],mb_convert_case($_POST['fExtensao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodTipo'],mb_convert_case($_POST['fTamanho'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataHora'],mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo'],$_POST['fCodScm'],$_POST['fCodMapa'],$_POST['fCodFornecedor']);
 			$_SESSION['oSupriArquivo'] = $oSupriArquivo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_text_field("Nome", $oSupriArquivo->getNome(), "text", "y");
			//$oValidate->add_text_field("Arquivo", $oSupriArquivo->getArquivo(), "text", "y");
			//$oValidate->add_text_field("Extensao", $oSupriArquivo->getExtensao(), "text", "y");
			//$oValidate->add_number_field("CodTipo", $oSupriArquivo->getCodTipo(), "number", "y");
			//$oValidate->add_text_field("Tamanho", $oSupriArquivo->getTamanho(), "text", "y");
			//$oValidate->add_date_field("DataHora", $oSupriArquivo->getDataHora(), "date", "y");
			//$oValidate->add_text_field("RealizadoPor", $oSupriArquivo->getRealizadoPor(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oSupriArquivo->getAtivo(), "number", "y");
			//$oValidate->add_number_field("CodScm", $oSupriArquivo->getCodScm(), "number", "y");
			//$oValidate->add_number_field("CodMapa", $oSupriArquivo->getCodMapa(), "number", "y");
			//$oValidate->add_number_field("CodFornecedor", $oSupriArquivo->getCodFornecedor(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupriArquivo.preparaFormulario&sOP=".$sOP."&nIdSupriArquivo=".$_POST['fCodArquivo'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSupriArquivo($oSupriArquivo)){
 					unset($_SESSION['oSupriArquivo']);
 					$_SESSION['sMsg'] = "supri_arquivo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SupriArquivo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o supri_arquivo!";
 					$sHeader = "?bErro=1&action=SupriArquivo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupriArquivo($oSupriArquivo)){
 					unset($_SESSION['oSupriArquivo']);
 					$_SESSION['sMsg'] = "supri_arquivo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SupriArquivo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o supri_arquivo!";
 										$sHeader = "?bErro=1&action=SupriArquivo.preparaFormulario&sOP=".$sOP."&nIdSupriArquivo=".$_POST['fCodArquivo'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
                $nCodArquivo = $_REQUEST['fCodArquivo'];

                if($oFachada->presenteSupriArquivo($nCodArquivo)){
                    if($oFachada->excluirSupriArquivo($nCodArquivo)){
                        $_SESSION['sMsg'] = "Arquivo exclu&iacute;do com sucesso!";
                    } else {
                        $_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o arquivo!";
                    }
                }else{
                     $_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel encontrar o arquivo!";
                }

                $sHeader = "?action=SupriScm.preparaFormulario&sOP=Alterar&nIdSupriScm=".$_REQUEST['fCodScm'];
 			break;

 		}

 		header("Location: ".$sHeader);

 	}


    function preparaArquivo(){
         $nCodArquivo = $_GET['fCodArquivo'];
         $oFachadaSuprimento = new FachadaSuprimentoBD();
        if($_REQUEST['oArquivo'] = $oFachadaSuprimento->recuperarUmSupriArquivo($nCodArquivo)){
            $_REQUEST['oArquivo'] = $_REQUEST['oArquivo']->getArquivo();
            include_once('view/suprimento/supri_arquivo/anexo.php');
        }else{
            $_SESSION['sMsg'] = "Arquivo não encontrado!";
 			$sHeader = "?bErro=1";
            header("Location: ".$sHeader);
        }
         exit();
    }

 }


 ?>
