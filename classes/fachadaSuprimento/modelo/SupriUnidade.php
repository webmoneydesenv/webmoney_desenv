<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_unidade
  */
 class SupriUnidade{
 	/**
	* @campo cod_unidade
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodUnidade;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
    /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;

 	public function __construct(){

 	}

 	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}

	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
 }
 ?>
