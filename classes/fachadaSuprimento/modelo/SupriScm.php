<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_scm
  */
 class SupriScm{
 	/**
	* @campo cod_scm
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodScm;
	/**
	* @campo cod_pessoa_solicitante
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodPessoaSolicitante;
    /**
	* @campo numero_scm
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nNumeroScm;
	/**
	* @campo numero_solicitacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNumeroSolicitacao;

	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;

	/**
	* @campo aplicacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAplicacao;
	/**
	* @campo cus_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCusCodigo;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObservacao;

	/**
	* @campo data_solicitacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataSolicitacao;


	private $oAcessoUsuario;
	private $oMnyPlanoContasCusto;
	private $oMnyPlanoContasConta;
	private $oMnyPlanoContasNegocio;
	private $oMnyPlanoContasUnidade;
	private $oMnyPlanoContasCentro;
	private $oMnyPlanoContasSetor;
	private $oSysEmpresa;


 	public function __construct(){

 	}

	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setCodPessoaSolicitante($nCodPessoaSolicitante){
		$this->nCodPessoaSolicitante = $nCodPessoaSolicitante;
	}
	public function getCodPessoaSolicitante(){
		return $this->nCodPessoaSolicitante;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}


public function setAplicacao($sAplicacao){
		$this->sAplicacao = $sAplicacao;
	}
	public function getAplicacao(){
		return $this->sAplicacao;
	}
	public function setCusCodigo($nCusCodigo){
		$this->nCusCodigo = $nCusCodigo;
	}
	public function getCusCodigo(){
		return $this->nCusCodigo;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setCenCodigo($nCenCodigo){
		$this->nCenCodigo = $nCenCodigo;
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}
	public function setConCodigo($nConCodigo){
		$this->nConCodigo = $nConCodigo;
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}

	public function setDataSolicitacao($dDataSolicitacao){
		$this->dDataSolicitacao = $dDataSolicitacao;
	}
	public function getDataSolicitacao(){
		return $this->dDataSolicitacao;
	}
	public function getDataSolicitacaoFormatado(){
		$oData = new DateTime($this->dDataSolicitacao);
		 return $oData->format("d/m/Y H:i:s");
	}
	public function setDataSolicitacaoBanco($dDataSolicitacao){
		 if($dDataSolicitacao){
			 $oData = DateTime::createFromFormat('d/m/Y H:i:s', $dDataSolicitacao);
			 $this->dDataSolicitacao = $oData->format('Y-m-d H:i:s') ;
	}
		 }

     	public function setNumeroScm($nNumeroScm){
		$this->nNumeroScm = $nNumeroScm;
	}
	public function getNumeroScm(){
		return $this->nNumeroScm;
	}
	 	public function setNumeroSolicitacao($sNumeroSolicitacao){
		$this->sNumeroSolicitacao = $sNumeroSolicitacao;
	}
	public function getNumeroSolicitacao(){
		return $this->sNumeroSolicitacao;
	}



	public function setAcessoUsuario($oAcessoUsuario){
		$this->oAcessoUsuario = $oAcessoUsuario;
	}
	public function getAcessoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($this->getCodPessoaSolicitante());
		return $this->oAcessoUsuario;
	}
	public function getMnyPlanoContasCusto(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCusto = $oFachada->recuperarUmMnyPlanoContas($this->getCusCodigo());
		return $this->oMnyPlanoContasCusto;
	}
	public function setMnyPlanoContasCusto($oMnyPlanoContasCusto){
		$this->oMnyPlanoContasCusto = $oMnyPlanoContasCusto;
	}
	public function getMnyPlanoContasNegocio(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasNegocio = $oFachada->recuperarUmMnyPlanoContas($this->getNegCodigo());
		return $this->oMnyPlanoContasNegocio;
	}
	public function setMnyPlanoContasNegocio($oMnyPlanoContasNegocio){
		$this->oMnyPlanoContasNegocio = $oMnyPlanoContasNegocio;
	}
	public function getMnyPlanoContasCentro(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCentro = $oFachada->recuperarUmMnyPlanoContas($this->getCenCodigo());
		return $this->oMnyPlanoContasCentro;
	}
	public function setMnyPlanoContasCentro($oMnyPlanoContasCentro){
		$this->oMnyPlanoContasCentro = $oMnyPlanoContasCentro;
	}
	public function getMnyPlanoContasUnidade(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasUnidade = $oFachada->recuperarUmMnyPlanoContas($this->getUniCodigo());
		return $this->oMnyPlanoContasUnidade;
	}
	public function setMnyPlanoContasUnidade($oMnyPlanoContasUnidade){
		$this->oMnyPlanoContasUnidade = $oMnyPlanoContasUnidade;
	}

	public function getMnyPlanoContasConta(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasConta = $oFachada->recuperarUmMnyPlanoContas($this->getConCodigo());
		return $this->oMnyPlanoContasConta;
	}
	public function setMnyPlanoContasConta($oMnyPlanoContasConta){
		$this->oMnyPlanoContasConta = $oMnyPlanoContasConta;
	}

	public function getMnyPlanoContasSetor(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasSetor = $oFachada->recuperarUmMnyPlanoContas($this->getSetCodigo());
		return $this->oMnyPlanoContasSetor;
	}
	public function setMnyPlanoContasSetor($oMnyPlanoContasSetor){
		$this->oMnyPlanoContasSetor = $oMnyPlanoContasSetor;
	}

	public function setSysEmpresa($oSysEmpresa){
		$this->oSysEmpresa = $oSysEmpresa;
	}
	public function getSysEmpresa(){
		$oFachada = new FachadaSysBD();
		$this->oSysEmpresa = $oFachada->recuperarUmSysEmpresa($this->getEmpCodigo());
		return $this->oSysEmpresa;
	}

 }
 ?>
