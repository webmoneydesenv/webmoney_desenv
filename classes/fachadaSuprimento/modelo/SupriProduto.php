<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_produto
  */
 class SupriProduto{
 	/**
	* @campo cod_produto
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProduto;

	/**
	* @campo cod_unidade
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo classificacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sClassificacao;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oSupriUnidade;


 	public function __construct(){

 	}

 	public function setCodProduto($nCodProduto){
		$this->nCodProduto = $nCodProduto;
	}
	public function getCodProduto(){
		return $this->nCodProduto;
	}

	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setClassificacao($sClassificacao){
		$this->sClassificacao = $sClassificacao;
	}
	public function getClassificacao(){
		return $this->sClassificacao;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}

	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setSupriUnidade($oSupriUnidade){
		$this->oSupriUnidade = $oSupriUnidade;
	}
	public function getSupriUnidade(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriUnidade = $oFachada->recuperarUmSupriUnidade($this->getCodUnidade());
		return $this->oSupriUnidade;
	}

 }
 ?>
