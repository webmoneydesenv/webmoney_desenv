<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_arquivo
  */
 class SupriArquivo{
 	/**
	* @campo cod_arquivo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodArquivo;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo arquivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sArquivo;
	/**
	* @campo extensao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sExtensao;
	/**
	* @campo cod_tipo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodTipo;
	/**
	* @campo tamanho
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTamanho;
	/**
	* @campo data_hora
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataHora;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo cod_scm
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo cod_mapa
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodMapa;
	/**
	* @campo cod_fornecedor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodFornecedor;
	private $oMnyContratoDocTipo;
	private $oSupriScm;
	private $oSupriMapaFornecedor;


 	public function __construct(){

 	}

 	public function setCodArquivo($nCodArquivo){
		$this->nCodArquivo = $nCodArquivo;
	}
	public function getCodArquivo(){
		return $this->nCodArquivo;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setArquivo($sArquivo){
		$this->sArquivo = $sArquivo;
	}
	public function getArquivo(){
		return $this->sArquivo;
	}
	public function setExtensao($sExtensao){
		$this->sExtensao = $sExtensao;
	}
	public function getExtensao(){
		return $this->sExtensao;
	}
	public function setCodTipo($nCodTipo){
		$this->nCodTipo = $nCodTipo;
	}
	public function getCodTipo(){
		return $this->nCodTipo;
	}
	public function setTamanho($sTamanho){
		$this->sTamanho = $sTamanho;
	}
	public function getTamanho(){
		return $this->sTamanho;
	}
	public function setDataHora($dDataHora){
		$this->dDataHora = $dDataHora;
	}
	public function getDataHora(){
		return $this->dDataHora;
	}

    public function setDataHoraBanco($dDataHora){
         if($dDataHora){
             $oData = DateTime::createFromFormat('d/m/Y H:i:s', $dDataHora);
             $this->dDataHora = $oData->format('Y-m-d H:i:s') ;
         }
    }
	public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setCodMapa($nCodMapa){
		$this->nCodMapa = $nCodMapa;
	}
	public function getCodMapa(){
		return $this->nCodMapa;
	}
	public function setCodFornecedor($nCodFornecedor){
		$this->nCodFornecedor = $nCodFornecedor;
	}
	public function getCodFornecedor(){
		return $this->nCodFornecedor;
	}
	public function setMnyContratoDocTipo($oMnyContratoDocTipo){
		$this->oMnyContratoDocTipo = $oMnyContratoDocTipo;
	}
	public function getMnyContratoDocTipo(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oMnyContratoDocTipo = $oFachada->recuperarUmMnyContratoDocTipo($this->getCodTipo());
		return $this->oMnyContratoDocTipo;
	}
	public function setSupriScm($oSupriScm){
		$this->oSupriScm = $oSupriScm;
	}
	public function getSupriScm(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriScm = $oFachada->recuperarUmSupriScm($this->getCodScm());
		return $this->oSupriScm;
	}
	public function setSupriMapaFornecedor($oSupriMapaFornecedor){
		$this->oSupriMapaFornecedor = $oSupriMapaFornecedor;
	}
	public function getSupriMapaFornecedor(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriMapaFornecedor = $oFachada->recuperarUmSupriMapaFornecedor($this->getCodMapa());
		return $this->oSupriMapaFornecedor;
	}

 }
 ?>
