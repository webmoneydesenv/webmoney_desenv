<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_mapa_fornecedor_frete
  */
 class SupriMapaFornecedorFrete{
 	/**
	* @campo cod_mapa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodMapa;
	/**
	* @campo cod_fornecedor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodFornecedor;
	/**
	* @campo cod_forma
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodForma;
	/**
	* @campo cod_transportadora
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodTransportadora;
	/**
	* @campo prazo_entrega
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dPrazoEntrega;
	/**
	* @campo obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObs;
	/**
	* @campo contato
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sContato;
	/**
	* @campo fone
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dFone;
	private $oSupriMapaCompras;
	private $oMnyPessoaGeral;
	private $oMnyPessoaJuridica;


 	public function __construct(){

 	}

 	public function setCodMapa($nCodMapa){
		$this->nCodMapa = $nCodMapa;
	}
	public function getCodMapa(){
		return $this->nCodMapa;
	}
	public function setCodFornecedor($nCodFornecedor){
		$this->nCodFornecedor = $nCodFornecedor;
	}
	public function getCodFornecedor(){
		return $this->nCodFornecedor;
	}
	public function setCodForma($nCodForma){
		$this->nCodForma = $nCodForma;
	}
	public function getCodForma(){
		return $this->nCodForma;
	}
	public function setCodTransportadora($nCodTransportadora){
		$this->nCodTransportadora = $nCodTransportadora;
	}
	public function getCodTransportadora(){
		return $this->nCodTransportadora;
	}
	public function setPrazoEntrega($dPrazoEntrega){
		$this->dPrazoEntrega = $dPrazoEntrega;
	}
	public function getPrazoEntrega(){
		return $this->dPrazoEntrega;
	}
	public function getPrazoEntregaFormatado(){
		$oData = new DateTime($this->dPrazoEntrega);		 return $oData->format("d/m/Y");
	}
	public function setPrazoEntregaBanco($dPrazoEntrega){
		 if($dPrazoEntrega){
			 $oData = DateTime::createFromFormat('d/m/Y', $dPrazoEntrega);
			 $this->dPrazoEntrega = $oData->format('Y-m-d') ;
	}
		 }
	public function setObs($sObs){
		$this->sObs = $sObs;
	}
	public function getObs(){
		return $this->sObs;
	}
	public function setContato($sContato){
		$this->sContato = $sContato;
	}
	public function getContato(){
		return $this->sContato;
	}
	public function setFone($dFone){
		$this->dFone = $dFone;
	}
	public function getFone(){
		return $this->dFone;
	}
	public function getFoneFormatado(){
		$oData = new DateTime($this->dFone);		 return $oData->format("d/m/Y");
	}
	public function setFoneBanco($dFone){
		 if($dFone){
			 $oData = DateTime::createFromFormat('d/m/Y', $dFone);
			 $this->dFone = $oData->format('Y-m-d') ;
	}
		 }
	public function setSupriMapaCompras($oSupriMapaCompras){
		$this->oSupriMapaCompras = $oSupriMapaCompras;
	}
	public function getSupriMapaCompras(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriMapaCompras = $oFachada->recuperarUmSupriMapaCompras($this->getCodMapa());
		return $this->oSupriMapaCompras;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($this->getCodFornecedor());
		return $this->oMnyPessoaGeral;
	}
	public function setMnyPessoaJuridica($oMnyPessoaJuridica){
		$this->oMnyPessoaJuridica = $oMnyPessoaJuridica;
	}
	public function getMnyPessoaJuridica(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oMnyPessoaJuridica = $oFachada->recuperarUmMnyPessoaJuridica($this->getCodTransportadora());
		return $this->oMnyPessoaJuridica;
	}

 }
 ?>
