<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_mapa_fornecedor
  */
 class SupriMapaFornecedor{
 	/**
	* @campo cod_mapa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodMapa;
	/**
	* @campo cod_fornecedor
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodFornecedor;
	/**
	* @campo contato
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sContato;
	/**
	* @campo fone
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nFone;
	/**
	* @campo condicao_pagamento
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCondicaoPagamento;
	/**
	* @campo desc_percentual
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDescPercentual;
	/**
	* @campo desc_valor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDescValor;
	/**
	* @campo frete_cif_fob
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nFreteCifFob;
	/**
	* @campo obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObs;
	private $oSupriMapaCompras;
	private $oMnyPessoaGeral;


 	public function __construct(){

 	}

 	public function setCodMapa($nCodMapa){
		$this->nCodMapa = $nCodMapa;
	}
	public function getCodMapa(){
		return $this->nCodMapa;
	}
	public function setCodFornecedor($nCodFornecedor){
		$this->nCodFornecedor = $nCodFornecedor;
	}
	public function getCodFornecedor(){
		return $this->nCodFornecedor;
	}
	public function setContato($sContato){
		$this->sContato = $sContato;
	}
	public function getContato(){
		return $this->sContato;
	}
	public function setFone($nFone){
		$this->nFone = $nFone;
	}
	public function getFone(){
		return $this->nFone;
	}
	public function setCondicaoPagamento($nCondicaoPagamento){
		$this->nCondicaoPagamento = $nCondicaoPagamento;
	}
	public function getCondicaoPagamento(){
		return $this->nCondicaoPagamento;
	}
	public function setDescPercentual($nDescPercentual){
		$this->nDescPercentual = $nDescPercentual;
	}
	public function getDescPercentual(){
		return $this->nDescPercentual;
	}
	public function getDescPercentualFormatado(){
		 $vRetorno = number_format($this->nDescPercentual , 2, ',', '.');		 return $vRetorno;
	}
	public function setDescPercentualBanco($nDescPercentual){
		if($nDescPercentual){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDescPercentual = str_replace($sOrigem, $sDestino, $nDescPercentual);

		}else{
		$this->nDescPercentual = 'null';
			}
		}
public function setDescValor($nDescValor){
		$this->nDescValor = $nDescValor;
	}
	public function getDescValor(){
		return $this->nDescValor;
	}
	public function getDescValorFormatado(){
		 $vRetorno = number_format($this->nDescValor , 2, ',', '.');		 return $vRetorno;
	}
	public function setDescValorBanco($nDescValor){
		if($nDescValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDescValor = str_replace($sOrigem, $sDestino, $nDescValor);

		}else{
		$this->nDescValor = 'null';
			}
		}
public function setFreteCifFob($nFreteCifFob){
		$this->nFreteCifFob = $nFreteCifFob;
	}
	public function getFreteCifFob(){
		return $this->nFreteCifFob;
	}
	public function getFreteCifFobFormatado(){
		 $vRetorno = number_format($this->nFreteCifFob , 2, ',', '.');		 return $vRetorno;
	}
	public function setFreteCifFobBanco($nFreteCifFob){
		if($nFreteCifFob){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nFreteCifFob = str_replace($sOrigem, $sDestino, $nFreteCifFob);

		}else{
		$this->nFreteCifFob = 'null';
			}
		}
public function setObs($sObs){
		$this->sObs = $sObs;
	}
	public function getObs(){
		return $this->sObs;
	}
	public function setSupriMapaCompras($oSupriMapaCompras){
		$this->oSupriMapaCompras = $oSupriMapaCompras;
	}
	public function getSupriMapaCompras(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriMapaCompras = $oFachada->recuperarUmSupriMapaCompras($this->getCodMapa());
		return $this->oSupriMapaCompras;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($this->getCodFornecedor());
		return $this->oMnyPessoaGeral;
	}

 }
 ?>
