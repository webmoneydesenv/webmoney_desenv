<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_om_item_protocolo
  */
 class SupriOmItemProtocolo{
 	/**
	* @campo cod_protocolo_suprimento_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProtocoloSuprimentoItem;
	/**
	* @campo cod_om
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOm;
	/**
	* @campo cod_om_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOmItem;
	/**
	* @campo de_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDeCodTipoProtocolo;
	/**
	* @campo para_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nParaCodTipoProtocolo;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoGrupoUsuario;
	private $oAcessoGrupoUsuario;
	private $oSupriScmItem;
	private $oSupriScmItem;


 	public function __construct(){

 	}

 	public function setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem){
		$this->nCodProtocoloSuprimentoItem = $nCodProtocoloSuprimentoItem;
	}
	public function getCodProtocoloSuprimentoItem(){
		return $this->nCodProtocoloSuprimentoItem;
	}
	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setCodOmItem($nCodOmItem){
		$this->nCodOmItem = $nCodOmItem;
	}
	public function getCodOmItem(){
		return $this->nCodOmItem;
	}
	public function setDeCodTipoProtocolo($nDeCodTipoProtocolo){
		$this->nDeCodTipoProtocolo = $nDeCodTipoProtocolo;
	}
	public function getDeCodTipoProtocolo(){
		return $this->nDeCodTipoProtocolo;
	}
	public function setParaCodTipoProtocolo($nParaCodTipoProtocolo){
		$this->nParaCodTipoProtocolo = $nParaCodTipoProtocolo;
	}
	public function getParaCodTipoProtocolo(){
		return $this->nParaCodTipoProtocolo;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		 if($dData){
			 $oData = DateTime::createFromFormat('d/m/Y', $dData);
			 $this->dData = $oData->format('Y-m-d') ;
	}
		 }
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDeCodTipoProtocolo());
		return $this->oAcessoGrupoUsuario;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getParaCodTipoProtocolo());
		return $this->oAcessoGrupoUsuario;
	}
	public function setSupriScmItem($oSupriScmItem){
		$this->oSupriScmItem = $oSupriScmItem;
	}
	public function getSupriScmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriScmItem = $oFachada->recuperarUmSupriScmItem($this->getCodOm());
		return $this->oSupriScmItem;
	}
	public function setSupriScmItem($oSupriScmItem){
		$this->oSupriScmItem = $oSupriScmItem;
	}
	public function getSupriScmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriScmItem = $oFachada->recuperarUmSupriScmItem($this->getCodOmItem());
		return $this->oSupriScmItem;
	}

 }
 ?>
