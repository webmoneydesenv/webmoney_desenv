<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_scm_item_om_item
  */
 class SupriScmItemOmItem{
 	/**
	* @campo cod_scm
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo cod_scm_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScmItem;
	/**
	* @campo cod_om
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOm;
	/**
	* @campo cod_om_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOmItem;
	private $oSupriScmItem;
	private $oSupriOmItem;


 	public function __construct(){

 	}

 	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setCodScmItem($nCodScmItem){
		$this->nCodScmItem = $nCodScmItem;
	}
	public function getCodScmItem(){
		return $this->nCodScmItem;
	}
	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setCodOmItem($nCodOmItem){
		$this->nCodOmItem = $nCodOmItem;
	}
	public function getCodOmItem(){
		return $this->nCodOmItem;
	}
	public function setSupriScmItem($oSupriScmItem){
		$this->oSupriScmItem = $oSupriScmItem;
	}
	public function getSupriScmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriScmItem = $oFachada->recuperarUmSupriScmItem($this->getCodScm());
		return $this->oSupriScmItem;
	}

	public function setSupriOmItem($oSupriOmItem){
		$this->oSupriOmItem = $oSupriOmItem;
	}
	public function getSupriOmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriOmItem = $oFachada->recuperarUmSupriOmItem($this->getCodOm());
		return $this->oSupriOmItem;
	}


 }
 ?>
