<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_scm_item
  */
 class SupriScmItem{
 	/**
	* @campo cod_scm
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo cod_scm_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScmItem;
	/**
	* @campo cod_produto
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodProduto;
	/**
	* @campo qtde
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nQtde;
	/**
	* @campo projeto
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sProjeto;
	/**
	* @campo revisao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nRevisao;
	/**
	* @campo consumo_mensal
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConsumoMensal;
	/**
	* @campo aplicacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAplicacao;
    /**
	* @campo data_prev_entrega
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataPrevEntrega;
	/**
	* @campo entrega
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEntrega;
     /**
	* @campo qtde_atendida
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nQtdeAtendida;
	private $oSupriScm;
	private $oSupriProduto;


 	public function __construct(){

 	}

 	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setCodScmItem($nCodScmItem){
		$this->nCodScmItem = $nCodScmItem;
	}
	public function getCodScmItem(){
		return $this->nCodScmItem;
	}
	public function setCodProduto($nCodProduto){
		$this->nCodProduto = $nCodProduto;
	}
	public function getCodProduto(){
		return $this->nCodProduto;
	}
	public function setQtde($nQtde){
		$this->nQtde = $nQtde;
	}
	public function getQtde(){
		return $this->nQtde;
	}
	public function getQtdeFormatado(){
		 $vRetorno = number_format($this->nQtde , 2, ',', '.');
		 return $vRetorno;
	}
	public function setQtdeBanco($nQtde){
		if($nQtde){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nQtde = str_replace($sOrigem, $sDestino, $nQtde);

		}else{
		$this->nQtde = 'null';
			}
		}
public function setProjeto($sProjeto){
		$this->sProjeto = $sProjeto;
	}
	public function getProjeto(){
		return $this->sProjeto;
	}
	public function setRevisao($nRevisao){
		$this->nRevisao = $nRevisao;
	}
	public function getRevisao(){
		return $this->nRevisao;
	}
	public function setConsumoMensal($nConsumoMensal){
		$this->nConsumoMensal = $nConsumoMensal;
	}
	public function getConsumoMensal(){
		return $this->nConsumoMensal;
	}
	public function setAplicacao($sAplicacao){
		$this->sAplicacao = $sAplicacao;
	}
	public function getAplicacao(){
		return $this->sAplicacao;
	}
	public function setSupriScm($oSupriScm){
		$this->oSupriScm = $oSupriScm;
	}
	public function getSupriScm(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriScm = $oFachada->recuperarUmSupriScm($this->getCodScm());
		return $this->oSupriScm;
	}

     public function getSupriProduto(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriProduto = $oFachada->recuperarUmSupriProduto($this->getCodProduto());
		return $this->oSupriProduto;
	}

    	public function setDataPrevEntrega($dDataPrevEntrega){
		$this->dDataPrevEntrega = $dDataPrevEntrega;
	}
	public function getDataPrevEntrega(){
		return $this->dDataPrevEntrega;
	}
	public function getDataPrevEntregaFormatado(){
		$oData = new DateTime($this->dDataPrevEntrega);
		 return $oData->format("d/m/Y");
	}
	public function setDataPrevEntregaBanco($dDataPrevEntrega){

		 if($dDataPrevEntrega){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataPrevEntrega);
			 $this->dDataPrevEntrega = $oData->format('Y-m-d') ;
	}
		 }
	public function setEntrega($nEntrega){
		$this->nEntrega = $nEntrega;
	}
	public function getEntrega(){
		return $this->nEntrega;
	}

	public function setQtdeAtendida($nQtdeAtendida){
		$this->nQtdeAtendida = $nQtdeAtendida;
	}
	public function getQtdeAtendida(){
		return $this->nQtdeAtendida;
	}

 }
 ?>
