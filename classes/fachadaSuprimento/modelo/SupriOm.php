<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_om
  */
 class SupriOm{
 	/**
	* @campo cod_om
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodOm;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo cod_pessoa_comprador
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodPessoaComprador;
	/**
	* @campo entrega_endereco
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEntregaEndereco;
	/**
	* @campo entrega_contato
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEntregaContato;
	/**
	* @campo entrega_fone
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEntregaFone;
	/**
	* @campo cod_fornecedor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodFornecedor;
	/**
	* @campo resposta
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dResposta;
	/**
	* @campo fechamento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dFechamento;
	/**
	* @campo previsao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dPrevisao;
	/**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObservacao;
	private $oAcessoUsuario;
	private $oMnyPessoaGeral;
	private $oSysEmpresa;


 	public function __construct(){

 	}

 	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setCodPessoaComprador($nCodPessoaComprador){
		$this->nCodPessoaComprador = $nCodPessoaComprador;
	}
	public function getCodPessoaComprador(){
		return $this->nCodPessoaComprador;
	}
	public function setEntregaEndereco($sEntregaEndereco){
		$this->sEntregaEndereco = $sEntregaEndereco;
	}
	public function getEntregaEndereco(){
		return $this->sEntregaEndereco;
	}
	public function setEntregaContato($sEntregaContato){
		$this->sEntregaContato = $sEntregaContato;
	}
	public function getEntregaContato(){
		return $this->sEntregaContato;
	}
	public function setEntregaFone($nEntregaFone){
		$this->nEntregaFone = $nEntregaFone;
	}
	public function getEntregaFone(){
		return $this->nEntregaFone;
	}
	public function setCodFornecedor($nCodFornecedor){
		$this->nCodFornecedor = $nCodFornecedor;
	}
	public function getCodFornecedor(){
		return $this->nCodFornecedor;
	}
	public function setResposta($dResposta){
		$this->dResposta = $dResposta;
	}
	public function getResposta(){
		return $this->dResposta;
	}
	public function getRespostaFormatado(){
		$oData = new DateTime($this->dResposta);
		 return $oData->format("d/m/Y");
	}
	public function setRespostaBanco($dResposta){
		 if($dResposta){
			 $oData = DateTime::createFromFormat('d/m/Y', $dResposta);
			 $this->dResposta = $oData->format('Y-m-d') ;
	}
		 }
	public function setFechamento($dFechamento){
		$this->dFechamento = $dFechamento;
	}
	public function getFechamento(){
		return $this->dFechamento;
	}
	public function getFechamentoFormatado(){
		$oData = new DateTime($this->dFechamento);
		return $oData->format("d/m/Y");
	}
	public function setFechamentoBanco($dFechamento){
		 if($dFechamento){
			 $oData = DateTime::createFromFormat('d/m/Y', $dFechamento);
			 $this->dFechamento = $oData->format('Y-m-d') ;
	}
		 }
	public function setPrevisao($dPrevisao){
		$this->dPrevisao = $dPrevisao;
	}
	public function getPrevisao(){
		return $this->dPrevisao;
	}
	public function getPrevisaoFormatado(){
		$oData = new DateTime($this->dPrevisao);
		 return $oData->format("d/m/Y");
	}
	public function setPrevisaoBanco($dPrevisao){
		 if($dPrevisao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dPrevisao);
			 $this->dPrevisao = $oData->format('Y-m-d') ;
	}
		 }
	public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}
	public function setAcessoUsuario($oAcessoUsuario){
		$this->oAcessoUsuario = $oAcessoUsuario;
	}
	public function getAcessoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($this->getCodPessoaComprador());
		return $this->oAcessoUsuario;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($this->getCodFornecedor());
		return $this->oMnyPessoaGeral;
	}
	public function setSysEmpresa($oSysEmpresa){
		$this->oSysEmpresa = $oSysEmpresa;
	}
	public function getSysEmpresa(){
		$oFachada = new FachadaSysBD();
		$this->oSysEmpresa = $oFachada->recuperarUmSysEmpresa($this->getEmpCodigo());
		return $this->oSysEmpresa;
	}

 }
 ?>
