<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_om_item
  */
 class SupriOmItem{
 	/**
	* @campo cod_om
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOm;
	/**
	* @campo cod_om_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOmItem;
      	/**
	* @campo cod_scm
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo cod_scm_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScmItem;
	/**
	* @campo tipo_material
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTipoMaterial;
	/**
	* @campo aplicacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAplicacao;
	/**
	* @campo obeservacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObeservacao;
	/**
	* @campo cod_produto
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodProduto;
	/**
	* @campo qtde
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nQtde;
	private $oSupriOm;
	private $oSupriProduto;


 	public function __construct(){

 	}

 	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setCodOmItem($nCodOmItem){
		$this->nCodOmItem = $nCodOmItem;
	}
	public function getCodOmItem(){
		return $this->nCodOmItem;
	}
     public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setCodScmItem($nCodScmItem){
		$this->nCodScmItem = $nCodScmItem;
	}
	public function getCodScmItem(){
		return $this->nCodScmItem;
	}
	public function setTipoMaterial($nTipoMaterial){
		$this->nTipoMaterial = $nTipoMaterial;
	}
	public function getTipoMaterial(){
		return $this->nTipoMaterial;
	}
	public function setAplicacao($sAplicacao){
		$this->sAplicacao = $sAplicacao;
	}
	public function getAplicacao(){
		return $this->sAplicacao;
	}
	public function setObeservacao($sObeservacao){
		$this->sObeservacao = $sObeservacao;
	}
	public function getObeservacao(){
		return $this->sObeservacao;
	}
	public function setCodProduto($nCodProduto){
		$this->nCodProduto = $nCodProduto;
	}
	public function getCodProduto(){
		return $this->nCodProduto;
	}
	public function setQtde($nQtde){
		$this->nQtde = $nQtde;
	}
	public function getQtde(){
		return $this->nQtde;
	}
	public function setSupriOm($oSupriOm){
		$this->oSupriOm = $oSupriOm;
	}
	public function getSupriOm(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriOm = $oFachada->recuperarUmSupriOm($this->getCodOm());
		return $this->oSupriOm;
	}
	public function setSupriProduto($oSupriProduto){
		$this->oSupriProduto = $oSupriProduto;
	}
	public function getSupriProduto(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriProduto = $oFachada->recuperarUmSupriProduto($this->getCodProduto());
		return $this->oSupriProduto;
	}

 }
 ?>
