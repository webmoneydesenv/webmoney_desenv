<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_mapa_fornecedor_item
  */
 class SupriMapaFornecedorItem{
 	/**
	* @campo cod_mapa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodMapa;
	/**
	* @campo cod_fornecedor
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodFornecedor;
	/**
	* @campo cod_om
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOm;
	/**
	* @campo cod_om_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodOmItem;
	/**
	* @campo valor_origem
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorOrigem;
	/**
	* @campo dif_aliquota
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDifAliquota;
	/**
	* @campo dif_valor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDifValor;
	/**
	* @campo valor_final
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorFinal;
	/**
	* @campo obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObs;
	private $oSupriMapaCompras;
	private $oMnyPessoaGeral;
	private $oSupriOmItem;
	private $oSupriOmItem;


 	public function __construct(){

 	}

 	public function setCodMapa($nCodMapa){
		$this->nCodMapa = $nCodMapa;
	}
	public function getCodMapa(){
		return $this->nCodMapa;
	}
	public function setCodFornecedor($nCodFornecedor){
		$this->nCodFornecedor = $nCodFornecedor;
	}
	public function getCodFornecedor(){
		return $this->nCodFornecedor;
	}
	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setCodOmItem($nCodOmItem){
		$this->nCodOmItem = $nCodOmItem;
	}
	public function getCodOmItem(){
		return $this->nCodOmItem;
	}
	public function setValorOrigem($nValorOrigem){
		$this->nValorOrigem = $nValorOrigem;
	}
	public function getValorOrigem(){
		return $this->nValorOrigem;
	}
	public function getValorOrigemFormatado(){
		 $vRetorno = number_format($this->nValorOrigem , 2, ',', '.');		 return $vRetorno;
	}
	public function setValorOrigemBanco($nValorOrigem){
		if($nValorOrigem){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorOrigem = str_replace($sOrigem, $sDestino, $nValorOrigem);

		}else{
		$this->nValorOrigem = 'null';
			}
		}
public function setDifAliquota($nDifAliquota){
		$this->nDifAliquota = $nDifAliquota;
	}
	public function getDifAliquota(){
		return $this->nDifAliquota;
	}
	public function getDifAliquotaFormatado(){
		 $vRetorno = number_format($this->nDifAliquota , 2, ',', '.');		 return $vRetorno;
	}
	public function setDifAliquotaBanco($nDifAliquota){
		if($nDifAliquota){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDifAliquota = str_replace($sOrigem, $sDestino, $nDifAliquota);

		}else{
		$this->nDifAliquota = 'null';
			}
		}
public function setDifValor($nDifValor){
		$this->nDifValor = $nDifValor;
	}
	public function getDifValor(){
		return $this->nDifValor;
	}
	public function getDifValorFormatado(){
		 $vRetorno = number_format($this->nDifValor , 2, ',', '.');		 return $vRetorno;
	}
	public function setDifValorBanco($nDifValor){
		if($nDifValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDifValor = str_replace($sOrigem, $sDestino, $nDifValor);

		}else{
		$this->nDifValor = 'null';
			}
		}
public function setValorFinal($nValorFinal){
		$this->nValorFinal = $nValorFinal;
	}
	public function getValorFinal(){
		return $this->nValorFinal;
	}
	public function getValorFinalFormatado(){
		 $vRetorno = number_format($this->nValorFinal , 2, ',', '.');		 return $vRetorno;
	}
	public function setValorFinalBanco($nValorFinal){
		if($nValorFinal){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorFinal = str_replace($sOrigem, $sDestino, $nValorFinal);

		}else{
		$this->nValorFinal = 'null';
			}
		}
public function setObs($sObs){
		$this->sObs = $sObs;
	}
	public function getObs(){
		return $this->sObs;
	}
	public function setSupriMapaCompras($oSupriMapaCompras){
		$this->oSupriMapaCompras = $oSupriMapaCompras;
	}
	public function getSupriMapaCompras(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriMapaCompras = $oFachada->recuperarUmSupriMapaCompras($this->getCodMapa());
		return $this->oSupriMapaCompras;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($this->getCodFornecedor());
		return $this->oMnyPessoaGeral;
	}
	public function setSupriOmItem($oSupriOmItem){
		$this->oSupriOmItem = $oSupriOmItem;
	}
	public function getSupriOmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriOmItem = $oFachada->recuperarUmSupriOmItem($this->getCodOm());
		return $this->oSupriOmItem;
	}
	public function setSupriOmItem($oSupriOmItem){
		$this->oSupriOmItem = $oSupriOmItem;
	}
	public function getSupriOmItem(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriOmItem = $oFachada->recuperarUmSupriOmItem($this->getCodOmItem());
		return $this->oSupriOmItem;
	}

 }
 ?>
