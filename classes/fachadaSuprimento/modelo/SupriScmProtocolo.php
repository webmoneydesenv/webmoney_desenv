<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_scm_protocolo
  */
 class SupriScmProtocolo{
 	/**
	* @campo cod_protocolo_scm
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProtocoloScm;
	/**
	* @campo cod_scm
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo de_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDeCodTipoProtocolo;
	/**
	* @campo para_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nParaCodTipoProtocolo;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoGrupoUsuario;
    private $oSysTipoProtocoloDe;
	private $oSysTipoProtocoloPara;

 	public function __construct(){

 	}

 	public function setCodProtocoloScm($nCodProtocoloScm){
		$this->nCodProtocoloScm = $nCodProtocoloScm;
	}
	public function getCodProtocoloScm(){
		return $this->nCodProtocoloScm;
	}
	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setDeCodTipoProtocolo($nDeCodTipoProtocolo){
		$this->nDeCodTipoProtocolo = $nDeCodTipoProtocolo;
	}
	public function getDeCodTipoProtocolo(){
		return $this->nDeCodTipoProtocolo;
	}
	public function setParaCodTipoProtocolo($nParaCodTipoProtocolo){
		$this->nParaCodTipoProtocolo = $nParaCodTipoProtocolo;
	}
	public function getParaCodTipoProtocolo(){
		return $this->nParaCodTipoProtocolo;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		 if($dData){
		 $oData = DateTime::createFromFormat('d/m/Y H:i:s', $dData);
		 $this->dData = $oData->format('Y-m-d H:i:s') ;
	}
		 }
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDeCodTipoProtocolo());
		return $this->oAcessoGrupoUsuario;
	}


    public function getDeSysTipoProtocolo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oSysTipoProtocoloDe = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDeCodTipoProtocolo());
		return $this->oSysTipoProtocoloDe;
	}
	public function setParaSysTipoProtocolo($oSysTipoProtocoloPara){
		$this->oSysTipoProtocoloPara = $oSysTipoProtocoloPara;
	}
	public function getParaSysTipoProtocolo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oSysTipoProtocoloPara = $oFachada->recuperarUmAcessoGrupoUsuario($this->getParaCodTipoProtocolo());
		return $this->oSysTipoProtocoloPara;
	}

 }
 ?>
