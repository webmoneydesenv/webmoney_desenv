<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_mapa_compras
  */
 class SupriMapaCompras{
 	/**
	* @campo cod_mapa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodMapa;
	/**
	* @campo cod_om
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodOm;
	/**
	* @campo data_fechamento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataFechamento;
	/**
	* @campo aprovador_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAprovadorPor;
	/**
	* @campo valor_aprovado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorAprovado;
	private $oSupriOm;


 	public function __construct(){

 	}

 	public function setCodMapa($nCodMapa){
		$this->nCodMapa = $nCodMapa;
	}
	public function getCodMapa(){
		return $this->nCodMapa;
	}
	public function setCodOm($nCodOm){
		$this->nCodOm = $nCodOm;
	}
	public function getCodOm(){
		return $this->nCodOm;
	}
	public function setDataFechamento($dDataFechamento){
		$this->dDataFechamento = $dDataFechamento;
	}
	public function getDataFechamento(){
		return $this->dDataFechamento;
	}
	public function getDataFechamentoFormatado(){
		$oData = new DateTime($this->dDataFechamento);		 return $oData->format("d/m/Y");
	}
	public function setDataFechamentoBanco($dDataFechamento){
		 if($dDataFechamento){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataFechamento);
			 $this->dDataFechamento = $oData->format('Y-m-d') ;
	}
		 }
	public function setAprovadorPor($sAprovadorPor){
		$this->sAprovadorPor = $sAprovadorPor;
	}
	public function getAprovadorPor(){
		return $this->sAprovadorPor;
	}
	public function setValorAprovado($nValorAprovado){
		$this->nValorAprovado = $nValorAprovado;
	}
	public function getValorAprovado(){
		return $this->nValorAprovado;
	}
	public function getValorAprovadoFormatado(){
		 $vRetorno = number_format($this->nValorAprovado , 2, ',', '.');		 return $vRetorno;
	}
	public function setValorAprovadoBanco($nValorAprovado){
		if($nValorAprovado){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorAprovado = str_replace($sOrigem, $sDestino, $nValorAprovado);

		}else{
		$this->nValorAprovado = 'null';
			}
		}
public function setSupriOm($oSupriOm){
		$this->oSupriOm = $oSupriOm;
	}
	public function getSupriOm(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oSupriOm = $oFachada->recuperarUmSupriOm($this->getCodOm());
		return $this->oSupriOm;
	}

 }
 ?>
