<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuprimento
  * @SGBD mysql
  * @tabela supri_protocolo_encaminhamento
  */
 class SupriProtocoloEncaminhamento{
 	/**
	* @campo cod_protocolo_encaminhamento
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProtocoloEncaminhamento;
	/**
	* @campo de
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDe;
	/**
	* @campo para
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPara;
	/**
	* @campo interno
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nInterno;
	/**
	* @campo ordem
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nOrdem;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoGrupoUsuario;



 	public function __construct(){

 	}

 	public function setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
		$this->nCodProtocoloEncaminhamento = $nCodProtocoloEncaminhamento;
	}
	public function getCodProtocoloEncaminhamento(){
		return $this->nCodProtocoloEncaminhamento;
	}
	public function setDe($nDe){
		$this->nDe = $nDe;
	}
	public function getDe(){
		return $this->nDe;
	}
	public function setPara($nPara){
		$this->nPara = $nPara;
	}
	public function getPara(){
		return $this->nPara;
	}
	public function setInterno($nInterno){
		$this->nInterno = $nInterno;
	}
	public function getInterno(){
		return $this->nInterno;
	}
	public function setOrdem($nOrdem){
		$this->nOrdem = $nOrdem;
	}
	public function getOrdem(){
		return $this->nOrdem;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaSuprimentoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDe());
		return $this->oAcessoGrupoUsuario;
	}

 }
 ?>
