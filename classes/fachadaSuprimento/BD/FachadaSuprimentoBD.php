<?php


class FachadaSuprimentoBD {
	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriMapaCompras
 	 *
 	 * @return SupriMapaCompras
 	 */
 	public function inicializarSupriMapaCompras($nCodMapa,$nCodOm,$dDataFechamento,$sAprovadorPor,$nValorAprovado){
 		$oSupriMapaCompras = new SupriMapaCompras();

		$oSupriMapaCompras->setCodMapa($nCodMapa);
		$oSupriMapaCompras->setCodOm($nCodOm);
		$oSupriMapaCompras->setDataFechamentoBanco($dDataFechamento);
		$oSupriMapaCompras->setAprovadorPor($sAprovadorPor);
		$oSupriMapaCompras->setValorAprovadoBanco($nValorAprovado);
 		return $oSupriMapaCompras;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriMapaCompras
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriMapaCompras($oSupriMapaCompras){
 		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriMapaCompras
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriMapaCompras($oSupriMapaCompras){
 		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriMapaCompras
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriMapaCompras($nCodMapa){
 		$oSupriMapaCompras = new SupriMapaCompras();

		$oSupriMapaCompras->setCodMapa($nCodMapa);
  		$oPersistencia = new Persistencia($oSupriMapaCompras);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriMapaCompras na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriMapaCompras($nCodMapa){
 		$oSupriMapaCompras = new SupriMapaCompras();

		$oSupriMapaCompras->setCodMapa($nCodMapa);
  		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriMapaCompras da base de dados
 	 *
 	 * @return SupriMapaCompras
 	 */
 	public function recuperarUmSupriMapaCompras($nCodMapa){
 		$oSupriMapaCompras = new SupriMapaCompras();
  		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		$sTabelas = "supri_mapa_compras";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_mapa = $nCodMapa";
  		$voSupriMapaCompras = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaCompras)
  			return $voSupriMapaCompras[0];
  		return false;
 	}

    /**
 	 *
 	 * M�todo para recuperar um objeto SupriMapaCompras da base de dados
 	 *
 	 * @return SupriMapaCompras
 	 */
 	public function recuperarUmSupriMapaComprasPorOrcamento($nCodOm){
 		$oSupriMapaCompras = new SupriMapaCompras();
  		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		$sTabelas = "supri_mapa_compras";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_om = $nCodOm";
  		$voSupriMapaCompras = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaCompras)
  			return $voSupriMapaCompras[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriMapaCompras da base de dados
 	 *
 	 * @return SupriMapaCompras[]
 	 */
 	public function recuperarTodosSupriMapaCompras(){
 		$oSupriMapaCompras = new SupriMapaCompras();
  		$oPersistencia = new Persistencia($oSupriMapaCompras);
  		$sTabelas = "supri_mapa_compras";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriMapaCompras = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaCompras)
  			return $voSupriMapaCompras;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriMapaFornecedor
 	 *
 	 * @return SupriMapaFornecedor
 	 */
 	public function inicializarSupriMapaFornecedor($nCodMapa,$nCodFornecedor,$sContato,$nFone,$nCondicaoPagamento,$nDescPercentual,$nDescValor,$nFreteCifFob,$sObs){
 		$oSupriMapaFornecedor = new SupriMapaFornecedor();

		$oSupriMapaFornecedor->setCodMapa($nCodMapa);
		$oSupriMapaFornecedor->setCodFornecedor($nCodFornecedor);
		$oSupriMapaFornecedor->setContato($sContato);
		$oSupriMapaFornecedor->setFone($nFone);
		$oSupriMapaFornecedor->setCondicaoPagamento($nCondicaoPagamento);
		$oSupriMapaFornecedor->setDescPercentualBanco($nDescPercentual);
		$oSupriMapaFornecedor->setDescValorBanco($nDescValor);
		$oSupriMapaFornecedor->setFreteCifFobBanco($nFreteCifFob);
		$oSupriMapaFornecedor->setObs($sObs);
 		return $oSupriMapaFornecedor;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriMapaFornecedor
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriMapaFornecedor($oSupriMapaFornecedor){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriMapaFornecedor
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriMapaFornecedor($oSupriMapaFornecedor){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriMapaFornecedor
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriMapaFornecedor($nCodMapa,$nCodFornecedor){
 		$oSupriMapaFornecedor = new SupriMapaFornecedor();

		$oSupriMapaFornecedor->setCodMapa($nCodMapa);
		$oSupriMapaFornecedor->setCodFornecedor($nCodFornecedor);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriMapaFornecedor na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriMapaFornecedor($nCodMapa,$nCodFornecedor){
 		$oSupriMapaFornecedor = new SupriMapaFornecedor();

		$oSupriMapaFornecedor->setCodMapa($nCodMapa);
		$oSupriMapaFornecedor->setCodFornecedor($nCodFornecedor);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriMapaFornecedor da base de dados
 	 *
 	 * @return SupriMapaFornecedor
 	 */
 	public function recuperarUmSupriMapaFornecedor($nCodMapa,$nCodFornecedor){
 		$oSupriMapaFornecedor = new SupriMapaFornecedor();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
  		$sTabelas = "supri_mapa_fornecedor";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_mapa = $nCodMapa AND cod_fornecedor = $nCodFornecedor";
  		$voSupriMapaFornecedor = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedor)
  			return $voSupriMapaFornecedor[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriMapaFornecedor da base de dados
 	 *
 	 * @return SupriMapaFornecedor[]
 	 */
 	public function recuperarTodosSupriMapaFornecedor(){
 		$oSupriMapaFornecedor = new SupriMapaFornecedor();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedor);
  		$sTabelas = "supri_mapa_fornecedor";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriMapaFornecedor = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedor)
  			return $voSupriMapaFornecedor;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriMapaFornecedorFrete
 	 *
 	 * @return SupriMapaFornecedorFrete
 	 */
 	public function inicializarSupriMapaFornecedorFrete($nCodMapa,$nCodFornecedor,$nCodForma,$nCodTransportadora,$dPrazoEntrega,$sObs,$sContato,$dFone){
 		$oSupriMapaFornecedorFrete = new SupriMapaFornecedorFrete();

		$oSupriMapaFornecedorFrete->setCodMapa($nCodMapa);
		$oSupriMapaFornecedorFrete->setCodFornecedor($nCodFornecedor);
		$oSupriMapaFornecedorFrete->setCodForma($nCodForma);
		$oSupriMapaFornecedorFrete->setCodTransportadora($nCodTransportadora);
		$oSupriMapaFornecedorFrete->setPrazoEntregaBanco($dPrazoEntrega);
		$oSupriMapaFornecedorFrete->setObs($sObs);
		$oSupriMapaFornecedorFrete->setContato($sContato);
		$oSupriMapaFornecedorFrete->setFoneBanco($dFone);
 		return $oSupriMapaFornecedorFrete;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriMapaFornecedorFrete
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriMapaFornecedorFrete($oSupriMapaFornecedorFrete){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriMapaFornecedorFrete
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriMapaFornecedorFrete($oSupriMapaFornecedorFrete){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriMapaFornecedorFrete
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriMapaFornecedorFrete($nCodMapa){
 		$oSupriMapaFornecedorFrete = new SupriMapaFornecedorFrete();

		$oSupriMapaFornecedorFrete->setCodMapa($nCodMapa);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriMapaFornecedorFrete na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriMapaFornecedorFrete($nCodMapa){
 		$oSupriMapaFornecedorFrete = new SupriMapaFornecedorFrete();

		$oSupriMapaFornecedorFrete->setCodMapa($nCodMapa);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriMapaFornecedorFrete da base de dados
 	 *
 	 * @return SupriMapaFornecedorFrete
 	 */
 	public function recuperarUmSupriMapaFornecedorFrete($nCodMapa){
 		$oSupriMapaFornecedorFrete = new SupriMapaFornecedorFrete();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
  		$sTabelas = "supri_mapa_fornecedor_frete";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_mapa = $nCodMapa";
  		$voSupriMapaFornecedorFrete = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedorFrete)
  			return $voSupriMapaFornecedorFrete[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriMapaFornecedorFrete da base de dados
 	 *
 	 * @return SupriMapaFornecedorFrete[]
 	 */
 	public function recuperarTodosSupriMapaFornecedorFrete(){
 		$oSupriMapaFornecedorFrete = new SupriMapaFornecedorFrete();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorFrete);
  		$sTabelas = "supri_mapa_fornecedor_frete";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriMapaFornecedorFrete = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedorFrete)
  			return $voSupriMapaFornecedorFrete;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriMapaFornecedorItem
 	 *
 	 * @return SupriMapaFornecedorItem
 	 */
 	public function inicializarSupriMapaFornecedorItem($nCodMapa,$nCodFornecedor,$nCodOm,$nCodOmItem,$nValorOrigem,$nDifAliquota,$nDifValor,$nValorFinal,$sObs){
 		$oSupriMapaFornecedorItem = new SupriMapaFornecedorItem();

		$oSupriMapaFornecedorItem->setCodMapa($nCodMapa);
		$oSupriMapaFornecedorItem->setCodFornecedor($nCodFornecedor);
		$oSupriMapaFornecedorItem->setCodOm($nCodOm);
		$oSupriMapaFornecedorItem->setCodOmItem($nCodOmItem);
		$oSupriMapaFornecedorItem->setValorOrigemBanco($nValorOrigem);
		$oSupriMapaFornecedorItem->setDifAliquotaBanco($nDifAliquota);
		$oSupriMapaFornecedorItem->setDifValorBanco($nDifValor);
		$oSupriMapaFornecedorItem->setValorFinalBanco($nValorFinal);
		$oSupriMapaFornecedorItem->setObs($sObs);
 		return $oSupriMapaFornecedorItem;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriMapaFornecedorItem
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriMapaFornecedorItem($oSupriMapaFornecedorItem){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriMapaFornecedorItem
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriMapaFornecedorItem($oSupriMapaFornecedorItem){
 		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriMapaFornecedorItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriMapaFornecedorItem($nCodMapa,$nCodFornecedor,$nCodOm,$nCodOmItem){
 		$oSupriMapaFornecedorItem = new SupriMapaFornecedorItem();

		$oSupriMapaFornecedorItem->setCodMapa($nCodMapa);
		$oSupriMapaFornecedorItem->setCodFornecedor($nCodFornecedor);
		$oSupriMapaFornecedorItem->setCodOm($nCodOm);
		$oSupriMapaFornecedorItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriMapaFornecedorItem na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriMapaFornecedorItem($nCodMapa,$nCodFornecedor,$nCodOm,$nCodOmItem){
 		$oSupriMapaFornecedorItem = new SupriMapaFornecedorItem();

		$oSupriMapaFornecedorItem->setCodMapa($nCodMapa);
		$oSupriMapaFornecedorItem->setCodFornecedor($nCodFornecedor);
		$oSupriMapaFornecedorItem->setCodOm($nCodOm);
		$oSupriMapaFornecedorItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriMapaFornecedorItem da base de dados
 	 *
 	 * @return SupriMapaFornecedorItem
 	 */
 	public function recuperarUmSupriMapaFornecedorItem($nCodMapa,$nCodFornecedor,$nCodOm,$nCodOmItem){
 		$oSupriMapaFornecedorItem = new SupriMapaFornecedorItem();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
  		$sTabelas = "supri_mapa_fornecedor_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_mapa = $nCodMapa AND cod_fornecedor = $nCodFornecedor AND cod_om = $nCodOm AND cod_om_item = $nCodOmItem";
  		$voSupriMapaFornecedorItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedorItem)
  			return $voSupriMapaFornecedorItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriMapaFornecedorItem da base de dados
 	 *
 	 * @return SupriMapaFornecedorItem[]
 	 */
 	public function recuperarTodosSupriMapaFornecedorItem(){
 		$oSupriMapaFornecedorItem = new SupriMapaFornecedorItem();
  		$oPersistencia = new Persistencia($oSupriMapaFornecedorItem);
  		$sTabelas = "supri_mapa_fornecedor_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriMapaFornecedorItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriMapaFornecedorItem)
  			return $voSupriMapaFornecedorItem;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriOm
 	 *
 	 * @return SupriOm
 	 */
 	public function inicializarSupriOm($nCodOm,$nEmpCodigo,$nCodPessoaComprador,$sEntregaEndereco,$sEntregaContato,$nEntregaFone,$nCodFornecedor,$dResposta,$dFechamento,$dPrevisao,$sObservacao){
 		$oSupriOm = new SupriOm();

		$oSupriOm->setCodOm($nCodOm);
		$oSupriOm->setEmpCodigo($nEmpCodigo);
		$oSupriOm->setCodPessoaComprador($nCodPessoaComprador);
		$oSupriOm->setEntregaEndereco($sEntregaEndereco);
		$oSupriOm->setEntregaContato($sEntregaContato);
		$oSupriOm->setEntregaFone($nEntregaFone);
		$oSupriOm->setCodFornecedor($nCodFornecedor);
		$oSupriOm->setRespostaBanco($dResposta);
		$oSupriOm->setFechamentoBanco($dFechamento);
		$oSupriOm->setPrevisaoBanco($dPrevisao);
		$oSupriOm->setObservacao($sObservacao);
 		return $oSupriOm;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriOm
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriOm($oSupriOm){
 		$oPersistencia = new Persistencia($oSupriOm);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriOm
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriOm($oSupriOm){
 		$oPersistencia = new Persistencia($oSupriOm);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriOm
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriOm($nCodOm){
 		$oSupriOm = new SupriOm();

		$oSupriOm->setCodOm($nCodOm);
  		$oPersistencia = new Persistencia($oSupriOm);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriOm na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriOm($nCodOm){
 		$oSupriOm = new SupriOm();

		$oSupriOm->setCodOm($nCodOm);
  		$oPersistencia = new Persistencia($oSupriOm);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriOm da base de dados
 	 *
 	 * @return SupriOm
 	 */
 	public function recuperarUmSupriOm($nCodOm){
 		$oSupriOm = new SupriOm();
  		$oPersistencia = new Persistencia($oSupriOm);
  		$sTabelas = "supri_om";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_om = $nCodOm";
  		$voSupriOm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOm)
  			return $voSupriOm[0];
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriOm da base de dados
 	 *
 	 * @return SupriOm[]
 	 */
 	public function recuperarTodosSupriOm(){
 		$oSupriOm = new SupriOm();
  		$oPersistencia = new Persistencia($oSupriOm);
  		$sTabelas = "supri_om";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriOm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOm)
  			return $voSupriOm;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriOmItem
 	 *
 	 * @return SupriOmItem
 	 */
 	public function inicializarSupriOmItem($nCodOm,$nCodOmItem,$nCodScm,$nCodScmItem,$nTipoMaterial,$sAplicacao,$sObeservacao,$nCodProduto,$nQtde){
 		$oSupriOmItem = new SupriOmItem();

		$oSupriOmItem->setCodOm($nCodOm);
		$oSupriOmItem->setCodOmItem($nCodOmItem);
        $oSupriOmItem->setCodScm($nCodScm);
		$oSupriOmItem->setCodScmItem($nCodScmItem);
		$oSupriOmItem->setTipoMaterial($nTipoMaterial);
		$oSupriOmItem->setAplicacao($sAplicacao);
		$oSupriOmItem->setObeservacao($sObeservacao);
		$oSupriOmItem->setCodProduto($nCodProduto);
		$oSupriOmItem->setQtde($nQtde);
 		return $oSupriOmItem;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriOmItem
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriOmItem($oSupriOmItem){
 		$oPersistencia = new Persistencia($oSupriOmItem);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriOmItem
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriOmItem($oSupriOmItem){
 		$oPersistencia = new Persistencia($oSupriOmItem);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriOmItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriOmItem($nCodOm,$nCodOmItem){
 		$oSupriOmItem = new SupriOmItem();

		$oSupriOmItem->setCodOm($nCodOm);
		$oSupriOmItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriOmItem);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriOmItem na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriOmItem($nCodOm,$nCodOmItem){
 		$oSupriOmItem = new SupriOmItem();

		$oSupriOmItem->setCodOm($nCodOm);
		$oSupriOmItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriOmItem da base de dados
 	 *
 	 * @return SupriOmItem
 	 */
 	public function recuperarUmSupriOmItem($nCodOm,$nCodOmItem){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_om = $nCodOm AND cod_om_item = $nCodOmItem";
  		$voSupriOmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItem)
  			return $voSupriOmItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriOmItem da base de dados
 	 *
 	 * @return SupriOmItem
 	 */
 	public function recuperarUmSupriOmItem2($nCodOm,$nCodOmItem){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item oi";
  		$sCampos = "sup.numero_solicitacao AS numero_solicitacao,i.cod_scm,i.cod_scm_item,p.descricao,p.cod_produto,concat(i.qtde, ' (', u.descricao, ')') AS qtde_solicitada,
                    concat(oi.qtde, ' (', u.descricao, ')') AS qtde_orcada,concat(qtde_atendida,' (',u.descricao,')') AS qtde_atendida,
                    projeto,revisao,consumo_mensal,i.aplicacao,i.data_prev_entrega,DATE_FORMAT(i.data_prev_entrega,'%d/%m/%Y') AS data_entrega_formatada,
                    i.entrega,(
                        CASE i.entrega
                        WHEN 1 THEN
                            'Unica'
                        WHEN 2 THEN
                            'Fracionada'
                        END
                    ) AS entrega_formatada";
  		$sComplemento ="INNER JOIN supri_scm_item i ON i.cod_scm =oi.cod_scm And i.cod_scm_item = oi.cod_scm_item
                        INNER JOIN supri_om om ON om.cod_om =oi.cod_om
                        INNER JOIN supri_produto p ON p.cod_produto = i.cod_produto
                        INNER JOIN supri_unidade u ON u.cod_unidade = p.cod_unidade
                        INNER JOIN supri_scm sup ON sup.cod_scm = i.cod_scm
                        WHERE oi.cod_om = $nCodOm And oi.cod_om_item=$nCodOmItem";
        //echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
  		$voSupriOmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItem)
  			return $voSupriOmItem[0];
  		return false;
 	}




     /**
 	 *
 	 * M�todo para recuperar um objeto SupriOm da base de dados
 	 *
 	 * @return SupriOm
 	 */
 	public function recuperarUmSupriOmItemPorMaiorItem($nCodOm){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item";
  		$sCampos = "max(cod_om_item) As cod_om_item";
  		$sComplemento = " WHERE cod_om = $nCodOm";
  		$voSupriOm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        if($voSupriOm)
  			return $voSupriOm[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriOmItem da base de dados
 	 *
 	 * @return SupriOmItem[]
 	 */
 	public function recuperarTodosSupriOmItem(){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriOmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItem)
  			return $voSupriOmItem;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriOmItem da base de dados
 	 *
 	 * @return SupriOmItem[]
 	 */
 	public function recuperarTodosSupriOmItemPorOrcamento($nCodOm){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item i";
  		$sCampos = "  sol.numero_solicitacao,i.cod_scm,	i.cod_scm_item,i.cod_om,i.cod_om_item as om_item,p.descricao,p.cod_produto,concat(qtde,' (',u.descricao,')') AS qtde";
  		$sComplemento = "INNER JOIN supri_produto p ON p.cod_produto = i.cod_produto
		                 INNER JOIN supri_unidade u ON u.cod_unidade = p.cod_unidade
                         INNER JOIN supri_scm sol ON sol.cod_scm = i.cod_scm
		                 WHERE i.cod_om =".$nCodOm;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voSupriOmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItem)
  			return $voSupriOmItem;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriOmItem da base de dados
 	 *
 	 * @return SupriOmItem[]
 	 */
 	public function recuperarTodosSupriOmItemPorItemSolicitacao($nCodSm,$nCodProduto){
 		$oSupriOmItem = new SupriOmItem();
  		$oPersistencia = new Persistencia($oSupriOmItem);
  		$sTabelas = "supri_om_item";
  		$sCampos = "supri_om.cod_om,supri_om_item.qtde";
  		$sComplemento = "INNER JOIN supri_om on supri_om.cod_om = supri_om_item.cod_om
                         Where cod_scm = ".$nCodSm." And cod_produto=".$nCodProduto;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voSupriOmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItem)
  			return $voSupriOmItem;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriProduto
 	 *
 	 * @return SupriProduto
 	 */
 	public function inicializarSupriProduto($nCodProduto,$nCodUnidade,$sClassificacao,$sDescricao,$nAtivo){
 		$oSupriProduto = new SupriProduto();

		$oSupriProduto->setCodProduto($nCodProduto);
		$oSupriProduto->setCodUnidade($nCodUnidade);
		$oSupriProduto->setClassificacao($sClassificacao);
		$oSupriProduto->setDescricao($sDescricao);
		$oSupriProduto->setAtivo($nAtivo);
 		return $oSupriProduto;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriProduto
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriProduto($oSupriProduto){
 		$oPersistencia = new Persistencia($oSupriProduto);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriProduto
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriProduto($oSupriProduto){
 		$oPersistencia = new Persistencia($oSupriProduto);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriProduto
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriProduto($nCodProduto){
 		$oSupriProduto = new SupriProduto();

		$oSupriProduto->setCodProduto($nCodProduto);
  		$oPersistencia = new Persistencia($oSupriProduto);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriProduto na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriProduto($nCodProduto){
 		$oSupriProduto = new SupriProduto();

		$oSupriProduto->setCodProduto($nCodProduto);
  		$oPersistencia = new Persistencia($oSupriProduto);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriProduto da base de dados
 	 *
 	 * @return SupriProduto
 	 */
 	public function recuperarUmSupriProduto($nCodProduto){
 		$oSupriProduto = new SupriProduto();
  		$oPersistencia = new Persistencia($oSupriProduto);
  		$sTabelas = "supri_produto";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_produto = $nCodProduto";
  		$voSupriProduto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProduto)
  			return $voSupriProduto[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriProduto da base de dados
 	 *
 	 * @return SupriProduto[]
 	 */
 	public function recuperarTodosSupriProduto(){
 		$oSupriProduto = new SupriProduto();
  		$oPersistencia = new Persistencia($oSupriProduto);
  		$sTabelas = "supri_produto";
  		$sCampos = "cod_produto,und.descricao as cod_unidade,classificacao,supri_produto.descricao,supri_produto.ativo";
  		$sComplemento = "INNER JOIN supri_unidade und ON und.cod_unidade=supri_produto.cod_unidade
                         WHERE  supri_produto.ativo=1";
  		$voSupriProduto = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProduto)
  			return $voSupriProduto;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriScm
 	 *
 	 * @return SupriScm
 	 */
    public function inicializarSupriScm($nCodScm,$nCodPessoaSolicitante,$nNumeroScm,$sNumeroSolicitacao,$sDescricao,$sAplicacao,$nCusCodigo,$nNegCodigo,$nUniCodigo,$nCenCodigo,$nSetCodigo,$nConCodigo,$nEmpCodigo,$sObservacao,$dDataSolicitacao){

        $oSupriScm = new SupriScm();

		$oSupriScm->setCodScm($nCodScm);
		$oSupriScm->setCodPessoaSolicitante($nCodPessoaSolicitante);
        $oSupriScm->setNumeroScm($nNumeroScm);
        $oSupriScm->setNumeroSolicitacao($sNumeroSolicitacao);
		$oSupriScm->setDescricao($sDescricao);

		$oSupriScm->setAplicacao($sAplicacao);
		$oSupriScm->setCusCodigo($nCusCodigo);
		$oSupriScm->setNegCodigo($nNegCodigo);
		$oSupriScm->setUniCodigo($nUniCodigo);
		$oSupriScm->setCenCodigo($nCenCodigo);
		$oSupriScm->setSetCodigo($nSetCodigo);
		$oSupriScm->setConCodigo($nConCodigo);
		$oSupriScm->setEmpCodigo($nEmpCodigo);
		$oSupriScm->setObservacao($sObservacao);
		$oSupriScm->setDataSolicitacaoBanco($dDataSolicitacao);
 		return $oSupriScm;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriScm
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriScm($oSupriScm){
 		$oPersistencia = new Persistencia($oSupriScm);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriScm
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriScm($oSupriScm){
 		$oPersistencia = new Persistencia($oSupriScm);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriScm
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriScm($nCodScm){
 		$oSupriScm = new SupriScm();

		$oSupriScm->setCodScm($nCodScm);
  		$oPersistencia = new Persistencia($oSupriScm);
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriScm na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriScm($nCodScm){
 		$oSupriScm = new SupriScm();

		$oSupriScm->setCodScm($nCodScm);
  		$oPersistencia = new Persistencia($oSupriScm);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScm da base de dados
 	 *
 	 * @return SupriScm
 	 */
 	public function recuperarUmSupriScm($nCodScm){
 		$oSupriScm = new SupriScm();
  		$oPersistencia = new Persistencia($oSupriScm);
  		$sTabelas = "supri_scm";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_scm = $nCodScm";
       // echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
  		$voSupriScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScm)
  			return $voSupriScm[0];
  		return false;
 	}

    /**
 	 *
 	 * M�todo para recuperar um objeto SupriScm da base de dados
 	 *
 	 * @return SupriScm
 	 */
 	public function recuperarUmSupriScmPorMaiorNumeroScm(){
 		$oSupriScm = new SupriScm();
  		$oPersistencia = new Persistencia($oSupriScm);
  		$sTabelas = "supri_scm";
  		$sCampos = "*";
  		$sComplemento = "where numero_scm = (Select max(numero_scm) From supri_scm) ";
        //echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
  		$voSupriScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScm)
  			return $voSupriScm[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScm da base de dados
 	 *
 	 * @return SupriScm[]
 	 */
 	public function recuperarTodosSupriScm(){
 		$oSupriScm = new SupriScm();
  		$oPersistencia = new Persistencia($oSupriScm);
  		$sTabelas = "supri_scm";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScm)
  			return $voSupriScm;
  		return false;
 	}
    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScm da base de dados
 	 *
 	 * @return SupriScm[]
 	 */
 	public function recuperarTodosSupriScmPorUnidade($nCodUnidade){
 		$oSupriScm = new SupriScm();
  		$oPersistencia = new Persistencia($oSupriScm);
  		$sTabelas = "supri_scm s";
  		$sCampos = "cod_scm,p.nome AS cod_pessoa_solicitante,numero_scm,numero_solicitacao,descricao,data_prev_entrega,entrega,qtde_entrega,aplicacao,cus_codigo,neg_codigo,uni_codigo,cen_codigo,set_codigo,con_codigo,emp_codigo,observacao,data_solicitacao";
  		$sComplemento = "
        INNER JOIN acesso_usuario u ON u.cod_usuario = s.cod_pessoa_solicitante
        INNER JOIN v_pessoa_geral_formatada p ON p.pesg_codigo = u.pesg_codigo
        WHERE s.uni_codigo=".$nCodUnidade;
        //echo "Select $sCampos From $sTabelas $sComplemento";
  		$voSupriScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScm)
  			return $voSupriScm;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriScmItem
 	 *
 	 * @return SupriScmItem
 	 */
 	public function inicializarSupriScmItem($nCodScm,$nCodScmItem,$nCodProduto,$nQtde,$sProjeto,$nRevisao,$nConsumoMensal,$sAplicacao,$dDataPrevEntrega,$nEntrega,$nQtdeAtendida){
 		$oSupriScmItem = new SupriScmItem();

		$oSupriScmItem->setCodScm($nCodScm);
		$oSupriScmItem->setCodScmItem($nCodScmItem);
		$oSupriScmItem->setCodProduto($nCodProduto);
		$oSupriScmItem->setQtdeBanco($nQtde);
		$oSupriScmItem->setProjeto($sProjeto);
		$oSupriScmItem->setRevisao($nRevisao);
		$oSupriScmItem->setConsumoMensal($nConsumoMensal);
		$oSupriScmItem->setAplicacao($sAplicacao);
		$oSupriScmItem->setDataPrevEntregaBanco($dDataPrevEntrega);
		$oSupriScmItem->setEntrega($nEntrega);
		$oSupriScmItem->setQtdeAtendida($nQtdeAtendida);
 		return $oSupriScmItem;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriScmItem
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriScmItem($oSupriScmItem){
 		$oPersistencia = new Persistencia($oSupriScmItem);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriScmItem
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriScmItem($oSupriScmItem){
 		$oPersistencia = new Persistencia($oSupriScmItem);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriScmItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriScmItem($nCodScm,$nCodScmItem){
 		$oSupriScmItem = new SupriScmItem();

		$oSupriScmItem->setCodScm($nCodScm);
		$oSupriScmItem->setCodScmItem($nCodScmItem);
  		$oPersistencia = new Persistencia($oSupriScmItem);
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriScmItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriScmItemPorSolicitacao($nCodScm){

        $oSupriScmItem = new SupriScmItem();
        $sComplemento = " WHERE cod_scm=".$nCodScm;
            $oPersistencia = new Persistencia($oSupriScmItem);
            $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;
 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriScmItem na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriScmItem($nCodScm,$nCodScmItem){
 		$oSupriScmItem = new SupriScmItem();

		$oSupriScmItem->setCodScm($nCodScm);
		$oSupriScmItem->setCodScmItem($nCodScmItem);
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem
 	 */
 	public function recuperarUmSupriScmItem($nCodScm,$nCodScmItem){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas = "supri_scm_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_scm = $nCodScm AND cod_scm_item = $nCodScmItem";
  		$voSupriScmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem[0];
  		return false;
 	}



 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem
 	 */
 	public function recuperarUmSupriScmItem2($nCodScm,$nCodScmItem){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas  = "supri_scm_item i";
  		$sCampos = "sup.numero_solicitacao As numero_solicitacao,i.cod_scm,cod_scm_item,p.descricao, p.cod_produto,concat(qtde,' (',u.descricao,')') AS qtde,concat(qtde_atendida,' (',u.descricao,')') AS qtde_atendida,projeto,revisao,consumo_mensal,i.aplicacao,i.data_prev_entrega,DATE_FORMAT(i.data_prev_entrega,'%d/%m/%Y') as data_entrega_formatada,i.entrega,(
                        CASE i.entrega
                        WHEN 1 THEN
                        'Unica'
                        WHEN 2 THEN
                             'Fracionada'
                        END
                    ) AS entrega_formatada";
  		$sComplemento = "INNER JOIN supri_produto p ON p.cod_produto=i.cod_produto
                         INNER JOIN supri_unidade u ON u.cod_unidade =p.cod_unidade
                         INNER JOIN supri_scm sup  ON sup.cod_scm=i.cod_scm
                         WHERE i.cod_scm=".$nCodScm." AND i.cod_scm_item=".$nCodScmItem;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $oSupriScmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($oSupriScmItem)
  			return $oSupriScmItem[0];
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem
 	 */
 	public function recuperarUmSupriScmItemPorMaxItem($nCodScm){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas = "supri_scm_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_scm = $nCodScm AND cod_scm_item = (SELECT max(cod_scm_item) FROM supri_scm_item WHERE cod_scm =".$nCodScm.")";
  		$voSupriScmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem[]
 	 */
 	public function recuperarTodosSupriScmItem(){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas = "supri_scm_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriScmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem[]
 	 */
 	public function recuperarTodosSupriScmItemPorSupriScm($nCodScm){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas = "supri_scm_item i";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_scm=".$nCodScm;
  		$voSupriScmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem[]
 	 */
 	public function recuperarTodosSupriScmItemPorSupriScm2($nCodScm){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas  = "supri_scm_item i";
  		$sCampos = "sup.numero_solicitacao As numero_solicitacao,i.cod_scm,cod_scm_item,p.descricao, p.cod_produto,qtde,concat(qtde,' (',u.descricao,')') AS qtde_formatada,qtde_atendida,concat(qtde_atendida,' (',u.descricao,')') AS qtde_atendida_formatada,projeto,revisao,consumo_mensal,i.aplicacao,i.data_prev_entrega,DATE_FORMAT(i.data_prev_entrega,'%d/%m/%Y') as data_entrega_formatada,i.entrega,(
                        CASE i.entrega
                        WHEN 1 THEN
                        'Unica'
                        WHEN 2 THEN
                             'Fracionada'
                        END
                    ) AS entrega_formatada";
  		$sComplemento = "INNER JOIN supri_produto p ON p.cod_produto=i.cod_produto
                         INNER JOIN supri_unidade u ON u.cod_unidade =p.cod_unidade
                         INNER JOIN supri_scm sup  ON sup.cod_scm=i.cod_scm
                         WHERE i.cod_scm=".$nCodScm;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $voSupriScmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem;
  		return false;
 	}

    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItem da base de dados
 	 * USADO NA TELA DE OR�AMENTO PARA CARREGAR APENAS PRODUTOS QUE AINDA N�O FORAM COMPRADOS
 	 * @return SupriScmItem[]
 	 */
 	public function recuperarTodosSupriScmItemPorSupriScm3($nCodScm){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas  = "supri_scm_item i";
  		$sCampos = "sup.numero_solicitacao As numero_solicitacao,i.cod_scm,cod_scm_item,p.descricao, p.cod_produto,qtde,concat(qtde,' (',u.descricao,')') AS qtde_formatada,qtde_atendida,concat(qtde_atendida,' (',u.descricao,')') AS qtde_atendida_formatada,projeto,revisao,consumo_mensal,i.aplicacao,i.data_prev_entrega,DATE_FORMAT(i.data_prev_entrega,'%d/%m/%Y') as data_entrega_formatada,i.entrega,(
                        CASE i.entrega
                        WHEN 1 THEN
                        'Unica'
                        WHEN 2 THEN
                             'Fracionada'
                        END
                    ) AS entrega_formatada";
  		$sComplemento = "INNER JOIN supri_produto p ON p.cod_produto=i.cod_produto
                         INNER JOIN supri_unidade u ON u.cod_unidade =p.cod_unidade
                         INNER JOIN supri_scm sup  ON sup.cod_scm=i.cod_scm
                         WHERE i.cod_scm=".$nCodScm." And i.qtde > i.qtde_atendida";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $voSupriScmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItem da base de dados
 	 *
 	 * @return SupriScmItem[]
 	 */
 	public function recuperarTodosSupriScmItemDetalhe($nCodScm,$nCodScmItem){
 		$oSupriScmItem = new SupriScmItem();
  		$oPersistencia = new Persistencia($oSupriScmItem);
  		$sTabelas  = "supri_scm_item item";
  		$sCampos = "	sol.numero_solicitacao As cod_scm,item.cod_scm_item,prod.descricao As cod_produto,item.qtde,item.qtde_atendida,item.projeto,item.revisao,item.consumo_mensal,item.aplicacao,item.data_prev_entrega,item.entrega";
  		$sComplemento = "INNER JOIN supri_scm sol ON sol.cod_scm = item.cod_scm
                         INNER JOIN supri_produto prod ON prod.cod_produto = item.cod_produto
                         WHERE
                          item.cod_scm=".$nCodScm." And item.cod_scm_item=1".$nCodScmItem;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $voSupriScmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItem)
  			return $voSupriScmItem;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriScmItemOmItem
 	 *
 	 * @return SupriScmItemOmItem
 	 */
 	public function inicializarSupriScmItemOmItem($nCodScm,$nCodScmItem,$nCodOm,$nCodOmItem){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();

		$oSupriScmItemOmItem->setCodScm($nCodScm);
		$oSupriScmItemOmItem->setCodScmItem($nCodScmItem);
		$oSupriScmItemOmItem->setCodOm($nCodOm);
		$oSupriScmItemOmItem->setCodOmItem($nCodOmItem);
 		return $oSupriScmItemOmItem;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriScmItemOmItem
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriScmItemOmItem($oSupriScmItemOmItem){
 		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriScmItemOmItem
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriScmItemOmItem($oSupriScmItemOmItem){
 		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriScmItemOmItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriScmItemOmItem($nCodScm,$nCodScmItem,$nCodOm,$nCodOmItem){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();

		$oSupriScmItemOmItem->setCodScm($nCodScm);
		$oSupriScmItemOmItem->setCodScmItem($nCodScmItem);
		$oSupriScmItemOmItem->setCodOm($nCodOm);
		$oSupriScmItemOmItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriScmItemOmItem na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriScmItemOmItem($nCodScm,$nCodScmItem,$nCodOm,$nCodOmItem){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();

		$oSupriScmItemOmItem->setCodScm($nCodScm);
		$oSupriScmItemOmItem->setCodScmItem($nCodScmItem);
		$oSupriScmItemOmItem->setCodOm($nCodOm);
		$oSupriScmItemOmItem->setCodOmItem($nCodOmItem);
  		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmItemOmItem da base de dados
 	 *
 	 * @return SupriScmItemOmItem
 	 */
 	public function recuperarUmSupriScmItemOmItem($nCodScm,$nCodScmItem,$nCodOm,$nCodOmItem){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();
  		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		$sTabelas = "supri_scm_item_om_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_scm = $nCodScm AND cod_scm_item = $nCodScmItem AND cod_om = $nCodOm AND cod_om_item = $nCodOmItem";
  		$voSupriScmItemOmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItemOmItem)
  			return $voSupriScmItemOmItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmItemOmItem da base de dados
 	 *
 	 * @return SupriScmItemOmItem
 	 */
 	public function recuperarUmSupriScmItemOmItemAjax($nCodScm,$nCodScmItem){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();
  		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		$sTabelas = "supri_scm_item i";
  		$sCampos = "s.numero_solicitacao, p.descricao,i.qtde";
  		$sComplemento = "INNER JOIN supri_produto p ON p.cod_produto = i.cod_produto
                         INNER JOIN supri_scm s ON s.cod_scm = 16
                         WHERE i.cod_scm = 16 AND cod_scm_item = 1";
  		$voSupriScmItemOmItem = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItemOmItem)
  			return $voSupriScmItemOmItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmItemOmItem da base de dados
 	 *
 	 * @return SupriScmItemOmItem[]
 	 */
 	public function recuperarTodosSupriScmItemOmItem(){
 		$oSupriScmItemOmItem = new SupriScmItemOmItem();
  		$oPersistencia = new Persistencia($oSupriScmItemOmItem);
  		$sTabelas = "supri_scm_item_om_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriScmItemOmItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmItemOmItem)
  			return $voSupriScmItemOmItem;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriUnidade
 	 *
 	 * @return SupriUnidade
 	 */
 	public function inicializarSupriUnidade($nCodUnidade,$sDescricao,$nAtivo){
 		$oSupriUnidade = new SupriUnidade();

		$oSupriUnidade->setCodUnidade($nCodUnidade);
		$oSupriUnidade->setDescricao($sDescricao);
		$oSupriUnidade->setAtivo($nAtivo);
 		return $oSupriUnidade;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriUnidade
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriUnidade($oSupriUnidade){
 		$oPersistencia = new Persistencia($oSupriUnidade);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriUnidade
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriUnidade($oSupriUnidade){
 		$oPersistencia = new Persistencia($oSupriUnidade);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriUnidade
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriUnidade($nCodUnidade){
 		$oSupriUnidade = new SupriUnidade();

		$oSupriUnidade->setCodUnidade($nCodUnidade);
  		$oPersistencia = new Persistencia($oSupriUnidade);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriUnidade na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriUnidade($nCodUnidade){
 		$oSupriUnidade = new SupriUnidade();

		$oSupriUnidade->setCodUnidade($nCodUnidade);
  		$oPersistencia = new Persistencia($oSupriUnidade);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriUnidade da base de dados
 	 *
 	 * @return SupriUnidade
 	 */
 	public function recuperarUmSupriUnidade($nCodUnidade){
 		$oSupriUnidade = new SupriUnidade();
  		$oPersistencia = new Persistencia($oSupriUnidade);
  		$sTabelas = "supri_unidade";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_unidade = $nCodUnidade";
  		$voSupriUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriUnidade)
  			return $voSupriUnidade[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriUnidade da base de dados
 	 *
 	 * @return SupriUnidade[]
 	 */
 	public function recuperarTodosSupriUnidade(){
 		$oSupriUnidade = new SupriUnidade();
  		$oPersistencia = new Persistencia($oSupriUnidade);
  		$sTabelas = "supri_unidade";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1";
  		$voSupriUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriUnidade)
  			return $voSupriUnidade;
  		return false;
 	}

	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriArquivo
 	 *
 	 * @return SupriArquivo
 	 */
 	public function inicializarSupriArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$sTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nCodScm,$nCodMapa,$nCodFornecedor){
 		$oSupriArquivo = new SupriArquivo();

		$oSupriArquivo->setCodArquivo($nCodArquivo);
		$oSupriArquivo->setNome($sNome);
		$oSupriArquivo->setArquivo($sArquivo);
		$oSupriArquivo->setExtensao($sExtensao);
		$oSupriArquivo->setCodTipo($nCodTipo);
		$oSupriArquivo->setTamanho($sTamanho);
		$oSupriArquivo->setDataHoraBanco($dDataHora);
		$oSupriArquivo->setRealizadoPor($sRealizadoPor);
		$oSupriArquivo->setAtivo($nAtivo);
		$oSupriArquivo->setCodScm($nCodScm);
		$oSupriArquivo->setCodMapa($nCodMapa);
		$oSupriArquivo->setCodFornecedor($nCodFornecedor);
 		return $oSupriArquivo;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriArquivo
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriArquivo($oSupriArquivo){
 		$oPersistencia = new Persistencia($oSupriArquivo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriArquivo
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriArquivo($oSupriArquivo){
 		$oPersistencia = new Persistencia($oSupriArquivo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriArquivo
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriArquivo($nCodArquivo){
 		$oSupriArquivo = new SupriArquivo();

		$oSupriArquivo->setCodArquivo($nCodArquivo);
  		$oPersistencia = new Persistencia($oSupriArquivo);
    		$bExcluir =  $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriArquivo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriArquivo($nCodArquivo){
 		$oSupriArquivo = new SupriArquivo();

		$oSupriArquivo->setCodArquivo($nCodArquivo);
  		$oPersistencia = new Persistencia($oSupriArquivo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriArquivo da base de dados
 	 *
 	 * @return SupriArquivo
 	 */
 	public function recuperarUmSupriArquivo($nCodArquivo){
 		$oSupriArquivo = new SupriArquivo();
  		$oPersistencia = new Persistencia($oSupriArquivo);
  		$sTabelas = "supri_arquivo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_arquivo = $nCodArquivo";
  		$voSupriArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriArquivo)
  			return $voSupriArquivo[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriArquivo da base de dados
 	 *
 	 * @return SupriArquivo[]
 	 */
 	public function recuperarTodosSupriArquivo(){
 		$oSupriArquivo = new SupriArquivo();
  		$oPersistencia = new Persistencia($oSupriArquivo);
  		$sTabelas = "supri_arquivo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriArquivo)
  			return $voSupriArquivo;
  		return false;
 	}


    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos MnyContratoArquivo da base de dados
 	 *
 	 * @return MnyContratoArquivo[]
 	 */
 	public function recuperarTodosSupriArquivoPorContratoTipoArquivo($nCodScm, $nContratoDocTipo){
 		$oSysArquivo = new SupriArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "supri_arquivo";
  		$sCampos = "cod_arquivo,mny_contrato_doc_tipo.cod_contrato_doc_tipo AS cod_contrato_doc_tipo,nome,realizado_por,item AS ativo";
  		$sComplemento = "INNER JOIN mny_contrato_doc_tipo ON mny_contrato_doc_tipo.cod_contrato_doc_tipo = supri_arquivo.cod_tipo WHERE supri_arquivo.ativo=1 and supri_arquivo.cod_scm=$nCodScm  AND  cod_tipo=$nContratoDocTipo";
        //echo "SELECT " . $sCampos . " FROM " .$sTabelas . " ". $sComplemento;
        //die();
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}

    /**
 	 *
 	 * M�todo para inicializar um Objeto SupriItemProtocolo
 	 *
 	 * @return SupriItemProtocolo
 	 */
 	public function inicializarSupriItemProtocolo($nCodProtocoloSuprimentoItem,$nCodScm,$nCodScmItem,$nDeCodTipoProtocolo,$nParaCodTipoProtocolo,$sResponsavel,$dData,$sDescricao,$nAtivo){
 		$oSupriItemProtocolo = new SupriItemProtocolo();

		$oSupriItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
		$oSupriItemProtocolo->setCodScm($nCodScm);
		$oSupriItemProtocolo->setCodScmItem($nCodScmItem);
		$oSupriItemProtocolo->setDeCodTipoProtocolo($nDeCodTipoProtocolo);
		$oSupriItemProtocolo->setParaCodTipoProtocolo($nParaCodTipoProtocolo);
		$oSupriItemProtocolo->setResponsavel($sResponsavel);
		$oSupriItemProtocolo->setDataBanco($dData);
		$oSupriItemProtocolo->setDescricao($sDescricao);
		$oSupriItemProtocolo->setAtivo($nAtivo);
 		return $oSupriItemProtocolo;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriItemProtocolo($oSupriItemProtocolo){
 		$oPersistencia = new Persistencia($oSupriItemProtocolo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriItemProtocolo($oSupriItemProtocolo){
 		$oPersistencia = new Persistencia($oSupriItemProtocolo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriItemProtocolo = new SupriItemProtocolo();

		$oSupriItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriItemProtocolo);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriItemProtocolo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriItemProtocolo = new SupriItemProtocolo();

		$oSupriItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriItemProtocolo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriItemProtocolo da base de dados
 	 *
 	 * @return SupriItemProtocolo
 	 */
 	public function recuperarUmSupriItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriItemProtocolo = new SupriItemProtocolo();
  		$oPersistencia = new Persistencia($oSupriItemProtocolo);
  		$sTabelas = "supri_item_protocolo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_protocolo_suprimento_item = $nCodProtocoloSuprimentoItem";
  		$voSupriItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriItemProtocolo)
  			return $voSupriItemProtocolo[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriItemProtocolo da base de dados
 	 *
 	 * @return SupriItemProtocolo[]
 	 */
 	public function recuperarTodosSupriItemProtocolo(){
 		$oSupriItemProtocolo = new SupriItemProtocolo();
  		$oPersistencia = new Persistencia($oSupriItemProtocolo);
  		$sTabelas = "supri_item_protocolo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriItemProtocolo)
  			return $voSupriItemProtocolo;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriProtocoloEncaminhamento
 	 *
 	 * @return SupriProtocoloEncaminhamento
 	 */
 	public function inicializarSupriProtocoloEncaminhamento($nCodProtocoloEncaminhamento,$nDe,$nPara,$nInterno,$nOrdem,$nAtivo){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();

		$oSupriProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
		$oSupriProtocoloEncaminhamento->setDe($nDe);
		$oSupriProtocoloEncaminhamento->setPara($nPara);
		$oSupriProtocoloEncaminhamento->setInterno($nInterno);
		$oSupriProtocoloEncaminhamento->setOrdem($nOrdem);
		$oSupriProtocoloEncaminhamento->setAtivo($nAtivo);
 		return $oSupriProtocoloEncaminhamento;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriProtocoloEncaminhamento($oSupriProtocoloEncaminhamento){
 		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriProtocoloEncaminhamento($oSupriProtocoloEncaminhamento){
 		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();

		$oSupriProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriProtocoloEncaminhamento na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();

		$oSupriProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SupriProtocoloEncaminhamento
 	 */
 	public function recuperarUmSupriProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		$sTabelas = "supri_protocolo_encaminhamento";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_protocolo_encaminhamento = $nCodProtocoloEncaminhamento";
  		$voSupriProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProtocoloEncaminhamento)
  			return $voSupriProtocoloEncaminhamento[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SupriProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSupriProtocoloEncaminhamento(){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		$sTabelas = "supri_protocolo_encaminhamento";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProtocoloEncaminhamento)
  			return $voSupriProtocoloEncaminhamento;
  		return false;
 	}
 	 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SupriProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSupriProtocoloEncaminhamentoPorOrigem($nDe){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		$sTabelas = "supri_protocolo_encaminhamento";
  		$sCampos = "cod_protocolo_encaminhamento,de,para,acesso_grupo_usuario.descricao as ativo";
  		$sComplemento = "INNER JOIN acesso_grupo_usuario ON acesso_grupo_usuario.cod_grupo_usuario = supri_protocolo_encaminhamento.para
                         WHERE de<>para AND de =$nDe And interno=0 ";
  		$voSupriProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProtocoloEncaminhamento)
  			return $voSupriProtocoloEncaminhamento;
  		return false;
 	}

    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SupriProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSupriProtocoloEncaminhamentoPorTramite(){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		$sTabelas = "supri_protocolo_encaminhamento";
   		$sCampos = "DISTINCT de,descricao as ativo,ordem";
  		$sComplemento = " INNER JOIN acesso_grupo_usuario ON supri_protocolo_encaminhamento.de = cod_grupo_usuario and  cod_grupo_usuario in (select DISTINCT de from supri_protocolo_encaminhamento) WHERE NOT ISNULL(ordem) ORDER BY 3"  ;
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $voSupriProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProtocoloEncaminhamento)
  			return $voSupriProtocoloEncaminhamento;
  		return false;
 	}
    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SupriProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSysProtocoloEncaminhamentoPorOrigem($nDe){
 		$oSupriProtocoloEncaminhamento = new SupriProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSupriProtocoloEncaminhamento);
  		$sTabelas = "supri_protocolo_encaminhamento";
   		$sCampos = "cod_protocolo_encaminhamento,de,para,acesso_grupo_usuario.descricao as ativo";
  		$sComplemento = "INNER JOIN acesso_grupo_usuario ON acesso_grupo_usuario.cod_grupo_usuario = supri_protocolo_encaminhamento.para
						 WHERE de<>para AND de =" .$nDe . " And interno=0 ";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
        $voSupriProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriProtocoloEncaminhamento)
  			return $voSupriProtocoloEncaminhamento;
  		return false;
 	}

    /**
 	 *
 	 * M�todo para inicializar um Objeto SupriOmItemProtocolo
 	 *
 	 * @return SupriOmItemProtocolo
 	 */
 	public function inicializarSupriOmItemProtocolo($nCodProtocoloSuprimentoItem,$nCodOm,$nCodOmItem,$nDeCodTipoProtocolo,$nParaCodTipoProtocolo,$sResponsavel,$dData,$sDescricao,$nAtivo){
 		$oSupriOmItemProtocolo = new SupriOmItemProtocolo();

		$oSupriOmItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
		$oSupriOmItemProtocolo->setCodOm($nCodOm);
		$oSupriOmItemProtocolo->setCodOmItem($nCodOmItem);
		$oSupriOmItemProtocolo->setDeCodTipoProtocolo($nDeCodTipoProtocolo);
		$oSupriOmItemProtocolo->setParaCodTipoProtocolo($nParaCodTipoProtocolo);
		$oSupriOmItemProtocolo->setResponsavel($sResponsavel);
		$oSupriOmItemProtocolo->setDataBanco($dData);
		$oSupriOmItemProtocolo->setDescricao($sDescricao);
		$oSupriOmItemProtocolo->setAtivo($nAtivo);
 		return $oSupriOmItemProtocolo;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriOmItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriOmItemProtocolo($oSupriOmItemProtocolo){
 		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriOmItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriOmItemProtocolo($oSupriOmItemProtocolo){
 		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriOmItemProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriOmItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriOmItemProtocolo = new SupriOmItemProtocolo();

		$oSupriOmItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriOmItemProtocolo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriOmItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriOmItemProtocolo = new SupriOmItemProtocolo();

		$oSupriOmItemProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriOmItemProtocolo da base de dados
 	 *
 	 * @return SupriOmItemProtocolo
 	 */
 	public function recuperarUmSupriOmItemProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriOmItemProtocolo = new SupriOmItemProtocolo();
  		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
  		$sTabelas = "supri_om_item_protocolo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_protocolo_suprimento_item = $nCodProtocoloSuprimentoItem";
  		$voSupriOmItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItemProtocolo)
  			return $voSupriOmItemProtocolo[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriOmItemProtocolo da base de dados
 	 *
 	 * @return SupriOmItemProtocolo[]
 	 */
 	public function recuperarTodosSupriOmItemProtocolo(){
 		$oSupriOmItemProtocolo = new SupriOmItemProtocolo();
  		$oPersistencia = new Persistencia($oSupriOmItemProtocolo);
  		$sTabelas = "supri_om_item_protocolo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriOmItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriOmItemProtocolo)
  			return $voSupriOmItemProtocolo;
  		return false;
 	}



	/**
 	 *
 	 * M�todo para inicializar um Objeto SupriScmProtocolo
 	 *
 	 * @return SupriScmProtocolo
 	 */
 	public function inicializarSupriScmProtocolo($nCodProtocoloScm,$nCodScm,$nDeCodTipoProtocolo,$nParaCodTipoProtocolo,$sResponsavel,$dData,$sDescricao,$nAtivo){
 		$oSupriScmProtocolo = new SupriScmProtocolo();

		$oSupriScmProtocolo->setCodProtocoloScm($nCodProtocoloScm);
		$oSupriScmProtocolo->setCodScm($nCodScm);
		$oSupriScmProtocolo->setDeCodTipoProtocolo($nDeCodTipoProtocolo);
		$oSupriScmProtocolo->setParaCodTipoProtocolo($nParaCodTipoProtocolo);
		$oSupriScmProtocolo->setResponsavel($sResponsavel);
		$oSupriScmProtocolo->setDataBanco($dData);
		$oSupriScmProtocolo->setDescricao($sDescricao);
		$oSupriScmProtocolo->setAtivo($nAtivo);
 		return $oSupriScmProtocolo;
 	}

 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto SupriScmProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function inserirSupriScmProtocolo($oSupriScmProtocolo){
 		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto SupriScmProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function alterarSupriScmProtocolo($oSupriScmProtocolo){
 		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto SupriScmProtocolo
 	 *
 	 * @return boolean
 	 */
 	public function excluirSupriScmProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriScmProtocolo = new SupriScmProtocolo();

		$oSupriScmProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto SupriScmProtocolo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSupriScmProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriScmProtocolo = new SupriScmProtocolo();

		$oSupriScmProtocolo->setCodProtocoloSuprimentoItem($nCodProtocoloSuprimentoItem);
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto SupriScmProtocolo da base de dados
 	 *
 	 * @return SupriScmProtocolo
 	 */
 	public function recuperarUmSupriScmProtocolo($nCodProtocoloSuprimentoItem){
 		$oSupriScmProtocolo = new SupriScmProtocolo();
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		$sTabelas = "supri_scm_protocolo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_protocolo_suprimento_item = $nCodProtocoloSuprimentoItem";
  		$voSupriScmProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmProtocolo)
  			return $voSupriScmProtocolo[0];
  		return false;
 	}

    /**
 	 *
 	 * M�todo para recuperar um objeto SupriScmProtocolo da base de dados
 	 *
 	 * @return SupriScmProtocolo
 	 */
 	public function recuperarSupriScmProtocoloPorCodScmWizardBreadcrumb($nCodScm){
 		$oSupriScmProtocolo = new SupriScmProtocolo();
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		$sTabelas = "v_supri_tramitacao_scm";
  		$sCampos = "*";
  		$sComplemento = "Where cod_scm = ".$nCodScm." ORDER BY data DESC LIMIT 1";
        //echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
  		$voSupriScmProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmProtocolo)
  			return $voSupriScmProtocolo[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmProtocolo da base de dados
 	 *
 	 * @return SupriScmProtocolo[]
 	 */
 	public function recuperarTodosSupriScmProtocolo(){
 		$oSupriScmProtocolo = new SupriScmProtocolo();
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		$sTabelas = "supri_scm_protocolo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSupriScmProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmProtocolo)
  			return $voSupriScmProtocolo;
  		return false;
 	}


    /**
 	 *
 	 * M�todo para recuperar um vetor de objetos SupriScmProtocolo da base de dados
 	 *
 	 * @return SupriScmProtocolo[]
 	 */
 	public function recuperarTodosSupriScmProtocoloCodScm($nCodScm){
 		$oSupriScmProtocolo = new SupriScmProtocolo();
  		$oPersistencia = new Persistencia($oSupriScmProtocolo);
  		$sTabelas = "supri_scm_protocolo";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1 AND cod_scm=".$nCodScm." ORDER BY cod_protocolo_scm,data ASC";
  		$voSupriScmProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSupriScmProtocolo)
  			return $voSupriScmProtocolo;
  		return false;
 	}






}
?>
