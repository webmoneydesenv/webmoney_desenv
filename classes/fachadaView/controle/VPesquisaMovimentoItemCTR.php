<?php
 class VPesquisaMovimentoItemCTR implements IControle{
 
 	public function VPesquisaMovimentoItemCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVPesquisaMovimentoItem = $oFachada->recuperarTodosVPesquisaMovimentoItem();
 
 		$_REQUEST['voVPesquisaMovimentoItem'] = $voVPesquisaMovimentoItem;
 		
 		
 		include_once("view/view/v_pesquisa_movimento_item/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVPesquisaMovimentoItem = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVPesquisaMovimentoItem = ($_POST['fIdVPesquisaMovimentoItem'][0]) ? $_POST['fIdVPesquisaMovimentoItem'][0] : $_GET['nIdVPesquisaMovimentoItem'];
 	
 			if($nIdVPesquisaMovimentoItem){
 				$vIdVPesquisaMovimentoItem = explode("||",$nIdVPesquisaMovimentoItem);
 				$oVPesquisaMovimentoItem = $oFachada->recuperarUmVPesquisaMovimentoItem();
 			}
 		}
 		
 		$_REQUEST['oVPesquisaMovimentoItem'] = ($_SESSION['oVPesquisaMovimentoItem']) ? $_SESSION['oVPesquisaMovimentoItem'] : $oVPesquisaMovimentoItem;
		
	
		
		//$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplemento($sComplemento); 
		
 		unset($_SESSION['oVPesquisaMovimentoItem']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
			
 			include_once("view/view/v_pesquisa_movimento_item/detalhe.php");
 		else
 			include_once("view/view/v_pesquisa_movimento_item/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVPesquisaMovimentoItem = $oFachada->inicializarVPesquisaMovimentoItem($_POST['fCompetencia'],$_POST['fEmpRazaoSocial'],$_POST['fEmpFantasia'],$_POST['fEmpCnpj'],$_POST['fEmpImagem'],$_POST['fMovDataEmissao'],$_POST['fTipNome'],$_POST['fPesgEmail'],$_POST['fPesFones'],$_POST['fTipoconCod'],$_POST['fPesgBcoAgencia'],$_POST['fPesgBcoConta'],$_POST['fBcoNome'],$_POST['fNome'],$_POST['fIdentificacao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fPesCodigo'],$_POST['fConCodigo'],$_POST['fSetCodigo'],$_POST['fMovObs'],$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],$_POST['fMovDocumento'],$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovValorGlob'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],$_POST['fAtivo'],$_POST['fMovOutrosDesc'],$_POST['fComCodigo'],$_POST['fCustoCodigo'],$_POST['fCustoDescricao'],$_POST['fNegocioCodigo'],$_POST['fNegocioDescricao'],$_POST['fCentroCodigo'],$_POST['fCentroDescricao'],$_POST['fUnidadeCodigo'],$_POST['fUnidadeDescricao'],$_POST['fAceiteCodigo'],$_POST['fAceiteDescricao'],$_POST['fSetorCodigo'],$_POST['fSetorDescricao'],$_POST['fTipoDocumentoCodigo'],$_POST['fTipoDocumentoDescricao'],$_POST['fTipoMovimentoCodigo'],$_POST['fTipoMovimentoDescricao'],$_POST['fContaCodigo'],$_POST['fContaDescricao'],$_POST['fFormaPagamentoCodigo'],$_POST['fFormaPagamentoDescricao'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],$_POST['fTipAceCodigo']);
 			$_SESSION['oVPesquisaMovimentoItem'] = $oVPesquisaMovimentoItem;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Competencia", $oVPesquisaMovimentoItem->getCompetencia(), "text", "y");
			$oValidate->add_text_field("EmpRazaoSocial", $oVPesquisaMovimentoItem->getEmpRazaoSocial(), "text", "y");
			$oValidate->add_text_field("EmpFantasia", $oVPesquisaMovimentoItem->getEmpFantasia(), "text", "y");
			$oValidate->add_text_field("EmpCnpj", $oVPesquisaMovimentoItem->getEmpCnpj(), "text", "y");
			//$oValidate->add_text_field("EmpImagem", $oVPesquisaMovimentoItem->getEmpImagem(), "text", "y");
			$oValidate->add_date_field("MovDataEmissao", $oVPesquisaMovimentoItem->getMovDataEmissao(), "date", "y");
			$oValidate->add_text_field("TipNome", $oVPesquisaMovimentoItem->getTipNome(), "text", "y");
			$oValidate->add_text_field("PesgEmail", $oVPesquisaMovimentoItem->getPesgEmail(), "text", "y");
			$oValidate->add_text_field("PesFones", $oVPesquisaMovimentoItem->getPesFones(), "text", "y");
			//$oValidate->add_number_field("TipoconCod", $oVPesquisaMovimentoItem->getTipoconCod(), "number", "y");
			$oValidate->add_text_field("PesgBcoAgencia", $oVPesquisaMovimentoItem->getPesgBcoAgencia(), "text", "y");
			$oValidate->add_text_field("PesgBcoConta", $oVPesquisaMovimentoItem->getPesgBcoConta(), "text", "y");
			$oValidate->add_text_field("BcoNome", $oVPesquisaMovimentoItem->getBcoNome(), "text", "y");
			$oValidate->add_text_field("Nome", $oVPesquisaMovimentoItem->getNome(), "text", "y");
			$oValidate->add_text_field("Identificacao", $oVPesquisaMovimentoItem->getIdentificacao(), "text", "y");
			//$oValidate->add_number_field("CusCodigo", $oVPesquisaMovimentoItem->getCusCodigo(), "number", "y");
			$oValidate->add_number_field("NegCodigo", $oVPesquisaMovimentoItem->getNegCodigo(), "number", "y");
			//$oValidate->add_number_field("CenCodigo", $oVPesquisaMovimentoItem->getCenCodigo(), "number", "y");
			$oValidate->add_number_field("UniCodigo", $oVPesquisaMovimentoItem->getUniCodigo(), "number", "y");
			$oValidate->add_number_field("PesCodigo", $oVPesquisaMovimentoItem->getPesCodigo(), "number", "y");
			//$oValidate->add_number_field("ConCodigo", $oVPesquisaMovimentoItem->getConCodigo(), "number", "y");
			$oValidate->add_number_field("SetCodigo", $oVPesquisaMovimentoItem->getSetCodigo(), "number", "y");
			//$oValidate->add_text_field("MovObs", $oVPesquisaMovimentoItem->getMovObs(), "text", "y");
			$oValidate->add_text_field("MovInc", $oVPesquisaMovimentoItem->getMovInc(), "text", "y");
			//$oValidate->add_text_field("MovAlt", $oVPesquisaMovimentoItem->getMovAlt(), "text", "y");
			//$oValidate->add_number_field("MovContrato", $oVPesquisaMovimentoItem->getMovContrato(), "number", "y");
			$oValidate->add_text_field("MovDocumento", $oVPesquisaMovimentoItem->getMovDocumento(), "text", "y");
			$oValidate->add_number_field("MovParcelas", $oVPesquisaMovimentoItem->getMovParcelas(), "number", "y");
			$oValidate->add_number_field("MovTipo", $oVPesquisaMovimentoItem->getMovTipo(), "number", "y");
			$oValidate->add_number_field("EmpCodigo", $oVPesquisaMovimentoItem->getEmpCodigo(), "number", "y");
			$oValidate->add_number_field("MovIcmsAliq", $oVPesquisaMovimentoItem->getMovIcmsAliq(), "number", "y");
			$oValidate->add_number_field("MovValorGlob", $oVPesquisaMovimentoItem->getMovValorGlob(), "number", "y");
			//$oValidate->add_number_field("MovPis", $oVPesquisaMovimentoItem->getMovPis(), "number", "y");
			//$oValidate->add_number_field("MovConfins", $oVPesquisaMovimentoItem->getMovConfins(), "number", "y");
			//$oValidate->add_number_field("MovCsll", $oVPesquisaMovimentoItem->getMovCsll(), "number", "y");
			//$oValidate->add_number_field("MovIss", $oVPesquisaMovimentoItem->getMovIss(), "number", "y");
			//$oValidate->add_number_field("MovIr", $oVPesquisaMovimentoItem->getMovIr(), "number", "y");
			//$oValidate->add_number_field("MovIrrf", $oVPesquisaMovimentoItem->getMovIrrf(), "number", "y");
			//$oValidate->add_number_field("MovInss", $oVPesquisaMovimentoItem->getMovInss(), "number", "y");
			//$oValidate->add_number_field("MovOutros", $oVPesquisaMovimentoItem->getMovOutros(), "number", "y");
			//$oValidate->add_number_field("MovDevolucao", $oVPesquisaMovimentoItem->getMovDevolucao(), "number", "y");
			$oValidate->add_number_field("Ativo", $oVPesquisaMovimentoItem->getAtivo(), "number", "y");
			//$oValidate->add_number_field("MovOutrosDesc", $oVPesquisaMovimentoItem->getMovOutrosDesc(), "number", "y");
			//$oValidate->add_number_field("ComCodigo", $oVPesquisaMovimentoItem->getComCodigo(), "number", "y");
			//$oValidate->add_text_field("CustoCodigo", $oVPesquisaMovimentoItem->getCustoCodigo(), "text", "y");
			//$oValidate->add_text_field("CustoDescricao", $oVPesquisaMovimentoItem->getCustoDescricao(), "text", "y");
			//$oValidate->add_text_field("NegocioCodigo", $oVPesquisaMovimentoItem->getNegocioCodigo(), "text", "y");
			//$oValidate->add_text_field("NegocioDescricao", $oVPesquisaMovimentoItem->getNegocioDescricao(), "text", "y");
			//$oValidate->add_text_field("CentroCodigo", $oVPesquisaMovimentoItem->getCentroCodigo(), "text", "y");
			//$oValidate->add_text_field("CentroDescricao", $oVPesquisaMovimentoItem->getCentroDescricao(), "text", "y");
			//$oValidate->add_text_field("UnidadeCodigo", $oVPesquisaMovimentoItem->getUnidadeCodigo(), "text", "y");
			//$oValidate->add_text_field("UnidadeDescricao", $oVPesquisaMovimentoItem->getUnidadeDescricao(), "text", "y");
			//$oValidate->add_text_field("AceiteCodigo", $oVPesquisaMovimentoItem->getAceiteCodigo(), "text", "y");
			//$oValidate->add_text_field("AceiteDescricao", $oVPesquisaMovimentoItem->getAceiteDescricao(), "text", "y");
			//$oValidate->add_text_field("SetorCodigo", $oVPesquisaMovimentoItem->getSetorCodigo(), "text", "y");
			//$oValidate->add_text_field("SetorDescricao", $oVPesquisaMovimentoItem->getSetorDescricao(), "text", "y");
			//$oValidate->add_text_field("TipoDocumentoCodigo", $oVPesquisaMovimentoItem->getTipoDocumentoCodigo(), "text", "y");
			//$oValidate->add_text_field("TipoDocumentoDescricao", $oVPesquisaMovimentoItem->getTipoDocumentoDescricao(), "text", "y");
			//$oValidate->add_text_field("TipoMovimentoCodigo", $oVPesquisaMovimentoItem->getTipoMovimentoCodigo(), "text", "y");
			//$oValidate->add_text_field("TipoMovimentoDescricao", $oVPesquisaMovimentoItem->getTipoMovimentoDescricao(), "text", "y");
			//$oValidate->add_text_field("ContaCodigo", $oVPesquisaMovimentoItem->getContaCodigo(), "text", "y");
			//$oValidate->add_text_field("ContaDescricao", $oVPesquisaMovimentoItem->getContaDescricao(), "text", "y");
			//$oValidate->add_text_field("FormaPagamentoCodigo", $oVPesquisaMovimentoItem->getFormaPagamentoCodigo(), "text", "y");
			//$oValidate->add_text_field("FormaPagamentoDescricao", $oVPesquisaMovimentoItem->getFormaPagamentoDescricao(), "text", "y");
			$oValidate->add_number_field("MovCodigo", $oVPesquisaMovimentoItem->getMovCodigo(), "number", "y");
			$oValidate->add_number_field("MovItem", $oVPesquisaMovimentoItem->getMovItem(), "number", "y");
			$oValidate->add_date_field("MovDataVencto", $oVPesquisaMovimentoItem->getMovDataVencto(), "date", "y");
			//$oValidate->add_date_field("MovDataPrev", $oVPesquisaMovimentoItem->getMovDataPrev(), "date", "y");
			$oValidate->add_number_field("MovValor", $oVPesquisaMovimentoItem->getMovValor(), "number", "y");
			//$oValidate->add_number_field("MovJuros", $oVPesquisaMovimentoItem->getMovJuros(), "number", "y");
			//$oValidate->add_number_field("MovValorPagar", $oVPesquisaMovimentoItem->getMovValorPagar(), "number", "y");
			$oValidate->add_number_field("FpgCodigo", $oVPesquisaMovimentoItem->getFpgCodigo(), "number", "y");
			$oValidate->add_number_field("TipDocCodigo", $oVPesquisaMovimentoItem->getTipDocCodigo(), "number", "y");
			//$oValidate->add_number_field("MovRetencao", $oVPesquisaMovimentoItem->getMovRetencao(), "number", "y");
			$oValidate->add_date_field("MovDataInclusao", $oVPesquisaMovimentoItem->getMovDataInclusao(), "date", "y");
			//$oValidate->add_number_field("MovValorPago", $oVPesquisaMovimentoItem->getMovValorPago(), "number", "y");
			$oValidate->add_number_field("TipAceCodigo", $oVPesquisaMovimentoItem->getTipAceCodigo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VPesquisaMovimentoItem.preparaFormulario&sOP=".$sOP."&nIdVPesquisaMovimentoItem="";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVPesquisaMovimentoItem($oVPesquisaMovimentoItem)){
 					unset($_SESSION['oVPesquisaMovimentoItem']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimentoItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimentoItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVPesquisaMovimentoItem($oVPesquisaMovimentoItem)){
 					unset($_SESSION['oVPesquisaMovimentoItem']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimentoItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimentoItem.preparaFormulario&sOP=".$sOP."&nIdVPesquisaMovimentoItem="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVPesquisaMovimentoItem = explode("____",$_REQUEST['fIdVPesquisaMovimentoItem']);
   				foreach($vIdPaiVPesquisaMovimentoItem as $vIdFilhoVPesquisaMovimentoItem){
  					$vIdVPesquisaMovimentoItem = explode("||",$vIdFilhoVPesquisaMovimentoItem);
 					foreach($vIdVPesquisaMovimentoItem as $nIdVPesquisaMovimentoItem){
  						$bResultado &= $oFachada->excluirVPesquisaMovimentoItem($vIdVPesquisaMovimentoItem[0],$vIdVPesquisaMovimentoItem[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimentoItem.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimentoItem.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
