<?php
 class VRelatorioPorEmpresaCTR implements IControle{

 	public function VRelatorioPorEmpresaCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();

 		$voVRelatorioPorEmpresa = $oFachada->recuperarTodosVRelatorioPorEmpresa();

 		$_REQUEST['voVRelatorioPorEmpresa'] = $voVRelatorioPorEmpresa;


 		include_once("view/view/v_relatorio_por_empresa/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$oVRelatorioPorEmpresa = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVRelatorioPorEmpresa = ($_POST['fIdVRelatorioPorEmpresa'][0]) ? $_POST['fIdVRelatorioPorEmpresa'][0] : $_GET['nIdVRelatorioPorEmpresa'];

 			if($nIdVRelatorioPorEmpresa){
 				$vIdVRelatorioPorEmpresa = explode("||",$nIdVRelatorioPorEmpresa);
 				$oVRelatorioPorEmpresa = $oFachada->recuperarUmVRelatorioPorEmpresa();
 			}
 		}

 		$_REQUEST['oVRelatorioPorEmpresa'] = ($_SESSION['oVRelatorioPorEmpresa']) ? $_SESSION['oVRelatorioPorEmpresa'] : $oVRelatorioPorEmpresa;
 		unset($_SESSION['oVRelatorioPorEmpresa']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_relatorio_por_empresa/detalhe.php");
 		else
 			include_once("view/view/v_relatorio_por_empresa/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oVRelatorioPorEmpresa = $oFachada->inicializarVRelatorioPorEmpresa(mb_convert_case($_POST['fFa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fCredor'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpresa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fUnidade'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDescricaoDespesa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAporte'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8"),$_POST['fParcela'],mb_convert_case($_POST['fCompetencia'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fVencimento'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fFormaPagamento'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovValorPagar'],$_POST['fAcrescimo'],$_POST['fMovJuros'],$_POST['fDesconto'],mb_convert_case($_POST['fReprogramado'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPago'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPagar'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVRelatorioPorEmpresa'] = $oVRelatorioPorEmpresa;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_text_field("Fa", $oVRelatorioPorEmpresa->getFa(), "text", "y");
			$oValidate->add_text_field("Credor", $oVRelatorioPorEmpresa->getCredor(), "text", "y");
			$oValidate->add_text_field("Empresa", $oVRelatorioPorEmpresa->getEmpresa(), "text", "y");
			//$oValidate->add_text_field("Unidade", $oVRelatorioPorEmpresa->getUnidade(), "text", "y");
			//$oValidate->add_text_field("DescricaoDespesa", $oVRelatorioPorEmpresa->getDescricaoDespesa(), "text", "y");
			//$oValidate->add_text_field("Aporte", $oVRelatorioPorEmpresa->getAporte(), "text", "y");
			$oValidate->add_text_field("MovDocumento", $oVRelatorioPorEmpresa->getMovDocumento(), "text", "y");
			//$oValidate->add_text_field("Parcela", $oVRelatorioPorEmpresa->getParcela(), "text", "y");
			//$oValidate->add_text_field("Competencia", $oVRelatorioPorEmpresa->getCompetencia(), "text", "y");
			//$oValidate->add_text_field("Vencimento", $oVRelatorioPorEmpresa->getVencimento(), "text", "y");
			$oValidate->add_text_field("FormaPagamento", $oVRelatorioPorEmpresa->getFormaPagamento(), "text", "y");
			//$oValidate->add_number_field("MovValorPagar", $oVRelatorioPorEmpresa->getMovValorPagar(), "number", "y");
			//$oValidate->add_number_field("Acrescimo", $oVRelatorioPorEmpresa->getAcrescimo(), "number", "y");
			//$oValidate->add_number_field("MovJuros", $oVRelatorioPorEmpresa->getMovJuros(), "number", "y");
			//$oValidate->add_number_field("Desconto", $oVRelatorioPorEmpresa->getDesconto(), "number", "y");
			$oValidate->add_text_field("Reprogramado", $oVRelatorioPorEmpresa->getReprogramado(), "text", "y");
			$oValidate->add_text_field("Pago", $oVRelatorioPorEmpresa->getPago(), "text", "y");
			$oValidate->add_text_field("Pagar", $oVRelatorioPorEmpresa->getPagar(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VRelatorioPorEmpresa.preparaFormulario&sOP=".$sOP."&nIdVRelatorioPorEmpresa="";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVRelatorioPorEmpresa($oVRelatorioPorEmpresa)){
 					unset($_SESSION['oVRelatorioPorEmpresa']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VRelatorioPorEmpresa.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VRelatorioPorEmpresa.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVRelatorioPorEmpresa($oVRelatorioPorEmpresa)){
 					unset($_SESSION['oVRelatorioPorEmpresa']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VRelatorioPorEmpresa.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 										$sHeader = "?bErro=1&action=VRelatorioPorEmpresa.preparaFormulario&sOP=".$sOP."&nIdVRelatorioPorEmpresa=""";
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdVRelatorioPorEmpresa']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirVRelatorioPorEmpresa($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VRelatorioPorEmpresa.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VRelatorioPorEmpresa.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
