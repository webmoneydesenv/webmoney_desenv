<?php
 class VPessoaGeralFormatadaCTR implements IControle{
 
 	public function VPessoaGeralFormatadaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVPessoaGeralFormatada = $oFachada->recuperarTodosVPessoaGeralFormatada();
 
 		$_REQUEST['voVPessoaGeralFormatada'] = $voVPessoaGeralFormatada;
 	
 		include_once("view/view/v_pessoa_geral_formatada/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVPessoaGeralFormatada = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVPessoaGeralFormatada = ($_POST['fIdVPessoaGeralFormatada'][0]) ? $_POST['fIdVPessoaGeralFormatada'][0] : $_GET['nIdVPessoaGeralFormatada'];
 	
 			if($nIdVPessoaGeralFormatada){
 				$vIdVPessoaGeralFormatada = explode("||",$nIdVPessoaGeralFormatada);
 				$oVPessoaGeralFormatada = $oFachada->recuperarUmVPessoaGeralFormatada();
 			}
 		}
 		
 		$_REQUEST['oVPessoaGeralFormatada'] = ($_SESSION['oVPessoaGeralFormatada']) ? $_SESSION['oVPessoaGeralFormatada'] : $oVPessoaGeralFormatada;
 		unset($_SESSION['oVPessoaGeralFormatada']);
 
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_pessoa_geral_formatada/detalhe.php");
 		else{
 			
 			include_once("view/view/v_pessoa_geral_formatada/insere_altera.php");
 		}
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVPessoaGeralFormatada = $oFachada->inicializarVPessoaGeralFormatada(mb_convert_case($_POST['fTipNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesgCodigo'],$_POST['fTipCodigo'],$_POST['fCodStatus'],mb_convert_case($_POST['fPesgEndLogra'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEndBairro'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEndCep'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fCidade'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEstado'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEmail'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesFones'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgInc'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgAlt'],MB_CASE_UPPER, "UTF-8"),$_POST['fBcoCodigo'],$_POST['fTipoconCod'],mb_convert_case($_POST['fPesgBcoAgencia'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgBcoConta'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo'],mb_convert_case($_POST['fBcoSigla'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fBcoNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDescStatus'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fTipoconInc'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fIdentificacao'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVPessoaGeralFormatada'] = $oVPessoaGeralFormatada;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("TipNome", $oVPessoaGeralFormatada->getTipNome(), "text", "y");
			$oValidate->add_number_field("PesgCodigo", $oVPessoaGeralFormatada->getPesgCodigo(), "number", "y");
			$oValidate->add_number_field("TipCodigo", $oVPessoaGeralFormatada->getTipCodigo(), "number", "y");
			$oValidate->add_number_field("CodStatus", $oVPessoaGeralFormatada->getCodStatus(), "number", "y");
			$oValidate->add_text_field("PesgEndLogra", $oVPessoaGeralFormatada->getPesgEndLogra(), "text", "y");
			$oValidate->add_text_field("PesgEndBairro", $oVPessoaGeralFormatada->getPesgEndBairro(), "text", "y");
			$oValidate->add_text_field("PesgEndCep", $oVPessoaGeralFormatada->getPesgEndCep(), "text", "y");
			$oValidate->add_text_field("Cidade", $oVPessoaGeralFormatada->getCidade(), "text", "y");
			$oValidate->add_text_field("Estado", $oVPessoaGeralFormatada->getEstado(), "text", "y");
			$oValidate->add_text_field("PesgEmail", $oVPessoaGeralFormatada->getPesgEmail(), "text", "y");
			$oValidate->add_text_field("PesFones", $oVPessoaGeralFormatada->getPesFones(), "text", "y");
			$oValidate->add_text_field("PesgInc", $oVPessoaGeralFormatada->getPesgInc(), "text", "y");
			$oValidate->add_text_field("PesgAlt", $oVPessoaGeralFormatada->getPesgAlt(), "text", "y");
			$oValidate->add_number_field("BcoCodigo", $oVPessoaGeralFormatada->getBcoCodigo(), "number", "y");
			$oValidate->add_number_field("TipoconCod", $oVPessoaGeralFormatada->getTipoconCod(), "number", "y");
			$oValidate->add_text_field("PesgBcoAgencia", $oVPessoaGeralFormatada->getPesgBcoAgencia(), "text", "y");
			$oValidate->add_text_field("PesgBcoConta", $oVPessoaGeralFormatada->getPesgBcoConta(), "text", "y");
			$oValidate->add_number_field("Ativo", $oVPessoaGeralFormatada->getAtivo(), "number", "y");
			$oValidate->add_text_field("BcoSigla", $oVPessoaGeralFormatada->getBcoSigla(), "text", "y");
			$oValidate->add_text_field("BcoNome", $oVPessoaGeralFormatada->getBcoNome(), "text", "y");
			$oValidate->add_text_field("DescStatus", $oVPessoaGeralFormatada->getDescStatus(), "text", "y");
			$oValidate->add_text_field("TipoconInc", $oVPessoaGeralFormatada->getTipoconInc(), "text", "y");
			$oValidate->add_text_field("Nome", $oVPessoaGeralFormatada->getNome(), "text", "y");
			$oValidate->add_text_field("Identificacao", $oVPessoaGeralFormatada->getIdentificacao(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VPessoaGeralFormatada.preparaFormulario&sOP=".$sOP."&nIdVPessoaGeralFormatada="";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVPessoaGeralFormatada($oVPessoaGeralFormatada)){
 					unset($_SESSION['oVPessoaGeralFormatada']);
 					$_SESSION['sMsg'] = "VPessoaGeralFormatada inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaGeralFormatada.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VPessoaGeralFormatada!";
 					$sHeader = "?bErro=1&action=VPessoaGeralFormatada.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVPessoaGeralFormatada($oVPessoaGeralFormatada)){
 					unset($_SESSION['oVPessoaGeralFormatada']);
 					$_SESSION['sMsg'] = "VPessoaGeralFormatada alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaGeralFormatada.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VPessoaGeralFormatada!";
 					$sHeader = "?bErro=1&action=VPessoaGeralFormatada.preparaFormulario&sOP=".$sOP."&nIdVPessoaGeralFormatada="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fIdVPessoaGeralFormatada'] as $nIdVPessoaGeralFormatada){
 					$vIdVPessoaGeralFormatada = explode("||",$nIdVPessoaGeralFormatada);
 					$bResultado &= $oFachada->excluirVPessoaGeralFormatada();
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "VPessoaGeralFormatada(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaGeralFormatada.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) VPessoaGeralFormatada!";
 					$sHeader = "?bErro=1&action=VPessoaGeralFormatada.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
