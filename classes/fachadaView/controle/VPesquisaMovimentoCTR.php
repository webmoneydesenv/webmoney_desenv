<?php
 class VPesquisaMovimentoCTR implements IControle{
 
 	public function VPesquisaMovimentoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVPesquisaMovimento = $oFachada->recuperarTodosVPesquisaMovimento();
 
 		$_REQUEST['voVPesquisaMovimento'] = $voVPesquisaMovimento;
 		
 		
 		include_once("view/view/v_pesquisa_movimento/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVPesquisaMovimento = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVPesquisaMovimento = ($_POST['fIdVPesquisaMovimento'][0]) ? $_POST['fIdVPesquisaMovimento'][0] : $_GET['nIdVPesquisaMovimento'];
 	
 			if($nIdVPesquisaMovimento){
 				$vIdVPesquisaMovimento = explode("||",$nIdVPesquisaMovimento);
 				$oVPesquisaMovimento = $oFachada->recuperarUmVPesquisaMovimento();
 			}
 		}
 		
 		$_REQUEST['oVPesquisaMovimento'] = ($_SESSION['oVPesquisaMovimento']) ? $_SESSION['oVPesquisaMovimento'] : $oVPesquisaMovimento;
 		unset($_SESSION['oVPesquisaMovimento']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_pesquisa_movimento/detalhe.php");
 		else
 			include_once("view/view/v_pesquisa_movimento/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVPesquisaMovimento = $oFachada->inicializarVPesquisaMovimento($_POST['fNome'],$_POST['fIdentificacao'],$_POST['fMovDataEmissao'],$_POST['fMovContrato'],$_POST['fMovDocumento'],$_POST['fMovObs'],$_POST['fMovTipo'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$_POST['fCompetencia'],$_POST['fPesgCodigo']);
 			$_SESSION['oVPesquisaMovimento'] = $oVPesquisaMovimento;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Nome", $oVPesquisaMovimento->getNome(), "text", "y");
			$oValidate->add_text_field("Identificacao", $oVPesquisaMovimento->getIdentificacao(), "text", "y");
			$oValidate->add_date_field("MovDataEmissao", $oVPesquisaMovimento->getMovDataEmissao(), "date", "y");
			//$oValidate->add_number_field("MovContrato", $oVPesquisaMovimento->getMovContrato(), "number", "y");
			$oValidate->add_text_field("MovDocumento", $oVPesquisaMovimento->getMovDocumento(), "text", "y");
			$oValidate->add_number_field("MovTipo", $oVPesquisaMovimento->getMovTipo(), "number", "y");
			$oValidate->add_number_field("MovCodigo", $oVPesquisaMovimento->getMovCodigo(), "number", "y");
			$oValidate->add_number_field("MovItem", $oVPesquisaMovimento->getMovItem(), "number", "y");
			$oValidate->add_date_field("MovDataVencto", $oVPesquisaMovimento->getMovDataVencto(), "date", "y");
			$oValidate->add_text_field("Competencia", $oVPesquisaMovimento->getCompetencia(), "text", "y");
			$oValidate->add_number_field("PesgCodigo", $oVPesquisaMovimento->getPesgCodigo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VPesquisaMovimento.preparaFormulario&sOP=".$sOP."&nIdVPesquisaMovimento=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVPesquisaMovimento($oVPesquisaMovimento)){
 					unset($_SESSION['oVPesquisaMovimento']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimento.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVPesquisaMovimento($oVPesquisaMovimento)){
 					unset($_SESSION['oVPesquisaMovimento']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimento.preparaFormulario&sOP=".$sOP."&nIdVPesquisaMovimento="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVPesquisaMovimento = explode("____",$_REQUEST['fIdVPesquisaMovimento']);
   				foreach($vIdPaiVPesquisaMovimento as $vIdFilhoVPesquisaMovimento){
  					$vIdVPesquisaMovimento = explode("||",$vIdFilhoVPesquisaMovimento);
 					foreach($vIdVPesquisaMovimento as $nIdVPesquisaMovimento){
  						$bResultado &= $oFachada->excluirVPesquisaMovimento($vIdVPesquisaMovimento[0],$vIdVPesquisaMovimento[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisaMovimento.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisaMovimento.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
