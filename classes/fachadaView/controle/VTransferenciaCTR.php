<?php
 class VTransferenciaCTR implements IControle{
 
 	public function VTransferenciaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVTransferencia = $oFachada->recuperarTodosVTransferencia();
 
 		$_REQUEST['voVTransferencia'] = $voVTransferencia;
 		
 		
 		include_once("view/view/v_transferencia/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVTransferencia = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVTransferencia = ($_POST['fIdVTransferencia'][0]) ? $_POST['fIdVTransferencia'][0] : $_GET['nIdVTransferencia'];
 	
 			if($nIdVTransferencia){
 				$vIdVTransferencia = explode("||",$nIdVTransferencia);
 				$oVTransferencia = $oFachada->recuperarUmVTransferencia();
 			}
 		}
 		
 		$_REQUEST['oVTransferencia'] = ($_SESSION['oVTransferencia']) ? $_SESSION['oVTransferencia'] : $oVTransferencia;
 		unset($_SESSION['oVTransferencia']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_transferencia/detalhe.php");
 		else
 			include_once("view/view/v_transferencia/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVTransferencia = $oFachada->inicializarVTransferencia($_POST['fMovDataEmissao'],$_POST['fValor'],$_POST['fCodTransferencia'],$_POST['fMovCodigoOrigem'],$_POST['fMovItemOrigem'],$_POST['fOrigem'],mb_convert_case($_POST['fContaOrigem'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAgenciaOrigem'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fBancoNomeOrigem'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodEmpresaOrigem'],mb_convert_case($_POST['fEmpresaOrigem'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovCodigoDestino'],$_POST['fMovItemDestino'],$_POST['fDestino'],mb_convert_case($_POST['fContaDestino'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAgenciaDestino'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fBancoNomeDestino'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodEmpresaDestino'],mb_convert_case($_POST['fEmpresaDestino'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVTransferencia'] = $oVTransferencia;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_date_field("MovDataEmissao", $oVTransferencia->getMovDataEmissao(), "date", "y");
			$oValidate->add_number_field("CodTransferencia", $oVTransferencia->getCodTransferencia(), "number", "y");
			$oValidate->add_number_field("MovCodigoOrigem", $oVTransferencia->getMovCodigoOrigem(), "number", "y");
			$oValidate->add_number_field("MovItemOrigem", $oVTransferencia->getMovItemOrigem(), "number", "y");
			$oValidate->add_number_field("Origem", $oVTransferencia->getOrigem(), "number", "y");
			$oValidate->add_text_field("ContaOrigem", $oVTransferencia->getContaOrigem(), "text", "y");
			$oValidate->add_text_field("AgenciaOrigem", $oVTransferencia->getAgenciaOrigem(), "text", "y");
			$oValidate->add_text_field("BancoNomeOrigem", $oVTransferencia->getBancoNomeOrigem(), "text", "y");
			$oValidate->add_number_field("CodEmpresaOrigem", $oVTransferencia->getCodEmpresaOrigem(), "number", "y");
			$oValidate->add_text_field("EmpresaOrigem", $oVTransferencia->getEmpresaOrigem(), "text", "y");
			$oValidate->add_number_field("MovCodigoDestino", $oVTransferencia->getMovCodigoDestino(), "number", "y");
			$oValidate->add_number_field("MovItemDestino", $oVTransferencia->getMovItemDestino(), "number", "y");
			$oValidate->add_number_field("Destino", $oVTransferencia->getDestino(), "number", "y");
			$oValidate->add_text_field("ContaDestino", $oVTransferencia->getContaDestino(), "text", "y");
			$oValidate->add_text_field("AgenciaDestino", $oVTransferencia->getAgenciaDestino(), "text", "y");
			$oValidate->add_text_field("BancoNomeDestino", $oVTransferencia->getBancoNomeDestino(), "text", "y");
			$oValidate->add_number_field("CodEmpresaDestino", $oVTransferencia->getCodEmpresaDestino(), "number", "y");
			$oValidate->add_text_field("EmpresaDestino", $oVTransferencia->getEmpresaDestino(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VTransferencia.preparaFormulario&sOP=".$sOP."&nIdVTransferencia=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVTransferencia($oVTransferencia)){
 					unset($_SESSION['oVTransferencia']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VTransferencia.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VTransferencia.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVTransferencia($oVTransferencia)){
 					unset($_SESSION['oVTransferencia']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VTransferencia.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VTransferencia.preparaFormulario&sOP=".$sOP."&nIdVTransferencia=";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVTransferencia = explode("____",$_REQUEST['fIdVTransferencia']);
   				foreach($vIdPaiVTransferencia as $vIdFilhoVTransferencia){
  					$vIdVTransferencia = explode("||",$vIdFilhoVTransferencia);
 					foreach($vIdVTransferencia as $nIdVTransferencia){
  						$bResultado &= $oFachada->excluirVTransferencia($vIdVTransferencia[0],$vIdVTransferencia[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VTransferencia.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VTransferencia.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
