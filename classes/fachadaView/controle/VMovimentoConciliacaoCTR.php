<?php
 class VMovimentoConciliacaoCTR implements IControle{
 
 	public function VMovimentoConciliacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVMovimentoConciliacao = $oFachada->recuperarTodosVMovimentoConciliacao();
 
 		$_REQUEST['voVMovimentoConciliacao'] = $voVMovimentoConciliacao;
 		
 		
 		include_once("view/view/v_movimento_conciliacao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVMovimentoConciliacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVMovimentoConciliacao = ($_POST['fIdVMovimentoConciliacao'][0]) ? $_POST['fIdVMovimentoConciliacao'][0] : $_GET['nIdVMovimentoConciliacao'];
 	
 			if($nIdVMovimentoConciliacao){
 				$vIdVMovimentoConciliacao = explode("||",$nIdVMovimentoConciliacao);
 				$oVMovimentoConciliacao = $oFachada->recuperarUmVMovimentoConciliacao();
 			}
 		}
 		
 		$_REQUEST['oVMovimentoConciliacao'] = ($_SESSION['oVMovimentoConciliacao']) ? $_SESSION['oVMovimentoConciliacao'] : $oVMovimentoConciliacao;
 		unset($_SESSION['oVMovimentoConciliacao']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_movimento_conciliacao/detalhe.php");
 		else
 			include_once("view/view/v_movimento_conciliacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVMovimentoConciliacao = $oFachada->inicializarVMovimentoConciliacao($_POST['fCodConciliacao'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fCodUnidade'],$_POST['fTipoCaixa'], $_POST['fDataConciliacao'], $_POST['fConsolidado'], $_POST['fOrdem'],$_POST['fMovValor'],mb_convert_case($_POST['fHistorico'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovTipo'],$_POST['fContaCodigoConc'],$_POST['fUnidadeDescricao'],$_POST['fBcoNome'],$_POST['fContaCorrenteConc'],$_POST['fAgenciaContaConc'],$_POST['fContaCodigo'],mb_convert_case($_POST['fAgencia'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fConta'],MB_CASE_UPPER, "UTF-8"), $_POST['fCaminhoArquivo'],$_POST['fCodArquivo']);
 			$_SESSION['oVMovimentoConciliacao'] = $oVMovimentoConciliacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("MovCodigo", $oVMovimentoConciliacao->getMovCodigo(), "number", "y");
			$oValidate->add_number_field("MovItem", $oVMovimentoConciliacao->getMovItem(), "number", "y");
			$oValidate->add_number_field("CodUnidade", $oVMovimentoConciliacao->getCodUnidade(), "number", "y");
			$oValidate->add_number_field("TipoCaixa", $oVMovimentoConciliacao->getTipoCaixa(), "number", "y");
			$oValidate->add_number_field("MovValor", $oVMovimentoConciliacao->getMovValor(), "number", "y");
			//$oValidate->add_text_field("Historico", $oVMovimentoConciliacao->getHistorico(), "text", "y");
			//$oValidate->add_number_field("ContaCodigo", $oVMovimentoConciliacao->getContaCodigo(), "number", "y");
			$oValidate->add_text_field("Agencia", $oVMovimentoConciliacao->getAgencia(), "text", "y");
			$oValidate->add_text_field("Conta", $oVMovimentoConciliacao->getConta(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VMovimentoConciliacao.preparaFormulario&sOP=".$sOP."&nIdVMovimentoConciliacao=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVMovimentoConciliacao($oVMovimentoConciliacao)){
 					unset($_SESSION['oVMovimentoConciliacao']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VMovimentoConciliacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VMovimentoConciliacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVMovimentoConciliacao($oVMovimentoConciliacao)){
 					unset($_SESSION['oVMovimentoConciliacao']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VMovimentoConciliacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VMovimentoConciliacao.preparaFormulario&sOP=".$sOP."&nIdVMovimentoConciliacao=";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
				
 				$vIdPaiVMovimentoConciliacao = explode("____",$_REQUEST['fIdVMovimentoConciliacao']);
   				foreach($vIdPaiVMovimentoConciliacao as $vIdFilhoVMovimentoConciliacao){
  					$vIdVMovimentoConciliacao = explode("||",$vIdFilhoVMovimentoConciliacao);
 					foreach($vIdVMovimentoConciliacao as $nIdVMovimentoConciliacao){		
  						$bResultado &= $oFachada->excluirVMovimentoConciliacao($vIdVMovimentoConciliacao[0],$vIdVMovimentoConciliacao[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Concilia&ccedil;&atilde;o(s) exclu&iacute;do(s) com sucesso!";
					if($_REQUEST['req'] == 1){
						$sHeader = "?action=MnyConciliacao.preparaFormualario&sOP=Cadastrar";
					}else{
						$sHeader = "?bErro=0&action=MnyConciliacao.preparaLista";	
					}
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Item(s)!";
 					$sHeader = "?action=MnyConciliacao.preparaFormualario&sOP=Cadastrar";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
