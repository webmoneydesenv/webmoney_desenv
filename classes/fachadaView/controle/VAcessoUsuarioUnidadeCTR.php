<?php
 class VAcessoUsuarioUnidadeCTR implements IControle{

 	public function VAcessoUsuarioUnidadeCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();

 		$voVAcessoUsuarioUnidade = $oFachada->recuperarTodosVAcessoUsuarioUnidade();

 		$_REQUEST['voVAcessoUsuarioUnidade'] = $voVAcessoUsuarioUnidade;


 		include_once("view/view/v_acesso_usuario_empresa/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$oVAcessoUsuarioUnidade = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVAcessoUsuarioUnidade = ($_POST['fIdVAcessoUsuarioUnidade'][0]) ? $_POST['fIdVAcessoUsuarioUnidade'][0] : $_GET['nIdVAcessoUsuarioUnidade'];

 			if($nIdVAcessoUsuarioUnidade){
 				$vIdVAcessoUsuarioUnidade = explode("||",$nIdVAcessoUsuarioUnidade);
 				$oVAcessoUsuarioUnidade = $oFachada->recuperarUmVAcessoUsuarioUnidade();
 			}
 		}

 		$_REQUEST['oVAcessoUsuarioUnidade'] = ($_SESSION['oVAcessoUsuarioUnidade']) ? $_SESSION['oVAcessoUsuarioUnidade'] : $oVAcessoUsuarioUnidade;
 		unset($_SESSION['oVAcessoUsuarioUnidade']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_acesso_usuario_empresa/detalhe.php");
 		else
 			include_once("view/view/v_acesso_usuario_empresa/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oVAcessoUsuarioUnidade = $oFachada->inicializarVAcessoUsuarioUnidade($_POST['fCodUsuario'],$_POST['fEmpCodigo'],$_POST['fCodGrupoUsuario'],$_POST['fCodPlanoContas'],mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpFantasia'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fUnidade'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPerfil'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVAcessoUsuarioUnidade'] = $oVAcessoUsuarioUnidade;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodUsuario", $oVAcessoUsuarioUnidade->getCodUsuario(), "number", "y");
			$oValidate->add_number_field("EmpCodigo", $oVAcessoUsuarioUnidade->getEmpCodigo(), "number", "y");
			$oValidate->add_number_field("CodGrupoUsuario", $oVAcessoUsuarioUnidade->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_number_field("CodPlanoContas", $oVAcessoUsuarioUnidade->getCodPlanoContas(), "number", "y");
			$oValidate->add_text_field("Nome", $oVAcessoUsuarioUnidade->getNome(), "text", "y");
			$oValidate->add_text_field("EmpFantasia", $oVAcessoUsuarioUnidade->getEmpFantasia(), "text", "y");
			$oValidate->add_text_field("Unidade", $oVAcessoUsuarioUnidade->getUnidade(), "text", "y");
			//$oValidate->add_text_field("Perfil", $oVAcessoUsuarioUnidade->getPerfil(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				//$sHeader = "?";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVAcessoUsuarioUnidade($oVAcessoUsuarioUnidade)){
 					unset($_SESSION['oVAcessoUsuarioUnidade']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VAcessoUsuarioUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VAcessoUsuarioUnidade.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVAcessoUsuarioUnidade($oVAcessoUsuarioUnidade)){
 					unset($_SESSION['oVAcessoUsuarioUnidade']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VAcessoUsuarioUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VAcessoUsuarioUnidade.preparaFormulario&sOP=".$sOP."&nIdVAcessoUsuarioUnidade="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				$vIdPaiVAcessoUsuarioUnidade = explode("____",$_REQUEST['fIdVAcessoUsuarioUnidade']);
   				foreach($vIdPaiVAcessoUsuarioUnidade as $vIdFilhoVAcessoUsuarioUnidade){
  					$vIdVAcessoUsuarioUnidade = explode("||",$vIdFilhoVAcessoUsuarioUnidade);
 					foreach($vIdVAcessoUsuarioUnidade as $nIdVAcessoUsuarioUnidade){
  						$bResultado &= $oFachada->excluirVAcessoUsuarioUnidade($vIdVAcessoUsuarioUnidade[0],$vIdVAcessoUsuarioUnidade[1]);
  					}
   				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VAcessoUsuarioUnidade.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VAcessoUsuarioUnidade.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
