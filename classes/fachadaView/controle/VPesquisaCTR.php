<?php
 class VPesquisaCTR implements IControle{
 
 	public function VPesquisaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVPesquisa = $oFachada->recuperarTodosVPesquisa();
 
 		$_REQUEST['voVPesquisa'] = $voVPesquisa;
 		
 		
 		include_once("view/view/v_pesquisa/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVPesquisa = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVPesquisa = ($_POST['fIdVPesquisa'][0]) ? $_POST['fIdVPesquisa'][0] : $_GET['nIdVPesquisa'];
 	
 			if($nIdVPesquisa){
 				$vIdVPesquisa = explode("||",$nIdVPesquisa);
 				$oVPesquisa = $oFachada->recuperarUmVPesquisa();
 			}
 		}
 		
 		$_REQUEST['oVPesquisa'] = ($_SESSION['oVPesquisa']) ? $_SESSION['oVPesquisa'] : $oVPesquisa;
 		unset($_SESSION['oVPesquisa']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_pesquisa/detalhe.php");
 		else
 			include_once("view/view/v_pesquisa/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVPesquisa = $oFachada->inicializarVPesquisa($_POST['fMovDataVencto'],mb_convert_case($_POST['fTipo'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovContrato'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fCompetencia'],$_POST['fMovValor'],$_POST['fMovTipo'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fTipNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesgCodigo'],mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fIdentificacao'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovDataInclusao'],$_POST['fMovDataPrev'],$_POST['fMovValorPago'],$_POST['fMovDataEmissao'],$_POST['fEmpCodigo'],mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVPesquisa'] = $oVPesquisa;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_date_field("MovDataVencto", $oVPesquisa->getMovDataVencto(), "date", "y");
			//$oValidate->add_text_field("Tipo", $oVPesquisa->getTipo(), "text", "y");
			//$oValidate->add_text_field("MovContrato", $oVPesquisa->getMovContrato(), "text", "y");
			$oValidate->add_number_field("MovCodigo", $oVPesquisa->getMovCodigo(), "number", "y");
			$oValidate->add_number_field("MovItem", $oVPesquisa->getMovItem(), "number", "y");
			$oValidate->add_date_field("Competencia", $oVPesquisa->getCompetencia(), "date", "y");
			$oValidate->add_number_field("MovValor", $oVPesquisa->getMovValor(), "number", "y");
			$oValidate->add_number_field("MovTipo", $oVPesquisa->getMovTipo(), "number", "y");
			$oValidate->add_text_field("Descricao", $oVPesquisa->getDescricao(), "text", "y");
			$oValidate->add_text_field("TipNome", $oVPesquisa->getTipNome(), "text", "y");
			$oValidate->add_number_field("PesgCodigo", $oVPesquisa->getPesgCodigo(), "number", "y");
			$oValidate->add_text_field("Nome", $oVPesquisa->getNome(), "text", "y");
			$oValidate->add_text_field("Identificacao", $oVPesquisa->getIdentificacao(), "text", "y");
			$oValidate->add_date_field("MovDataInclusao", $oVPesquisa->getMovDataInclusao(), "date", "y");
			//$oValidate->add_date_field("MovDataPrev", $oVPesquisa->getMovDataPrev(), "date", "y");
			//$oValidate->add_number_field("MovValorPago", $oVPesquisa->getMovValorPago(), "number", "y");
			$oValidate->add_date_field("MovDataEmissao", $oVPesquisa->getMovDataEmissao(), "date", "y");
			$oValidate->add_text_field("MovDocumento", $oVPesquisa->getMovDocumento(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VPesquisa.preparaFormulario&sOP=".$sOP."&nIdVPesquisa=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVPesquisa($oVPesquisa)){
 					unset($_SESSION['oVPesquisa']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisa.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVPesquisa($oVPesquisa)){
 					unset($_SESSION['oVPesquisa']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisa.preparaFormulario&sOP=".$sOP."&nIdVPesquisa=";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVPesquisa = explode("____",$_REQUEST['fIdVPesquisa']);
   				foreach($vIdPaiVPesquisa as $vIdFilhoVPesquisa){
  					$vIdVPesquisa = explode("||",$vIdFilhoVPesquisa);
 					foreach($vIdVPesquisa as $nIdVPesquisa){
  						$bResultado &= $oFachada->excluirVPesquisa($vIdVPesquisa[0],$vIdVPesquisa[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VPesquisa.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VPesquisa.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
