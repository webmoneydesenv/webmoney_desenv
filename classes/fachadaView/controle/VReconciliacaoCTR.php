<?php
 class VReconciliacaoCTR implements IControle{
 
 	public function VReconciliacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 		$voVReconciliacao = $oFachada->recuperarTodosVReconciliacao();
 
 		$_REQUEST['voVReconciliacao'] = $voVReconciliacao;
 		
		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
 		 
 		$sGrupo = '8';//Tipo de Unidade
		$_REQUEST['voMnyTipoUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
 		
 		include_once("view/view/v_reconciliacao/pesquisa_reconciliacao.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVReconciliacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVReconciliacao = ($_POST['fIdVReconciliacao'][0]) ? $_POST['fIdVReconciliacao'][0] : $_GET['nIdVReconciliacao'];
 	
 			if($nIdVReconciliacao){
 				$vIdVReconciliacao = explode("||",$nIdVReconciliacao);
 				$oVReconciliacao = $oFachada->recuperarUmVReconciliacao();
 			}
 		}
 		
 		$_REQUEST['oVReconciliacao'] = ($_SESSION['oVReconciliacao']) ? $_SESSION['oVReconciliacao'] : $oVReconciliacao;
 		unset($_SESSION['oVReconciliacao']);
 
 		$sGrupo = '8';//Tipo de Unidade
		$_REQUEST['voMnyTipoUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_reconciliacao/detalhe.php");
 		else
 			include_once("view/view/v_reconciliacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVReconciliacao = $oFachada->inicializarVReconciliacao(mb_convert_case($_POST['fFa'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovValorPagar'],$_POST['fFpgCodigo'],mb_convert_case($_POST['fFormaDePagamento'],MB_CASE_UPPER, "UTF-8"),$_POST['fTipDocCodigo'],mb_convert_case($_POST['fTipoDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],$_POST['fEmpCodigo'],$_POST['fMovTipo'],mb_convert_case($_POST['fMovimentoTipo'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8"),$_POST['fConCodigo'],mb_convert_case($_POST['fContaDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fUniCodigo'],mb_convert_case($_POST['fUnidadeDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCenCodigo'],mb_convert_case($_POST['fCentroDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fNegCodigo'],mb_convert_case($_POST['fUnidadeNegocioDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fCcrConta'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpresa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVReconciliacao'] = $oVReconciliacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Fa", $oVReconciliacao->getFa(), "text", "y");
			$oValidate->add_number_field("MovValorPagar", $oVReconciliacao->getMovValorPagar(), "number", "y");
			$oValidate->add_number_field("FpgCodigo", $oVReconciliacao->getFpgCodigo(), "number", "y");
			$oValidate->add_text_field("FormaDePagamento", $oVReconciliacao->getFormaDePagamento(), "text", "y");
			$oValidate->add_number_field("TipDocCodigo", $oVReconciliacao->getTipDocCodigo(), "number", "y");
			$oValidate->add_text_field("TipoDescricao", $oVReconciliacao->getTipoDescricao(), "text", "y");
			//$oValidate->add_date_field("Data", $oVReconciliacao->getData(), "date", "y");
			$oValidate->add_number_field("EmpCodigo", $oVReconciliacao->getEmpCodigo(), "number", "y");
			$oValidate->add_number_field("MovTipo", $oVReconciliacao->getMovTipo(), "number", "y");
			$oValidate->add_text_field("MovimentoTipo", $oVReconciliacao->getMovimentoTipo(), "text", "y");
			$oValidate->add_text_field("MovDocumento", $oVReconciliacao->getMovDocumento(), "text", "y");
			//$oValidate->add_number_field("ConCodigo", $oVReconciliacao->getConCodigo(), "number", "y");
			$oValidate->add_text_field("ContaDescricao", $oVReconciliacao->getContaDescricao(), "text", "y");
			$oValidate->add_number_field("UniCodigo", $oVReconciliacao->getUniCodigo(), "number", "y");
			$oValidate->add_text_field("UnidadeDescricao", $oVReconciliacao->getUnidadeDescricao(), "text", "y");
			//$oValidate->add_number_field("CenCodigo", $oVReconciliacao->getCenCodigo(), "number", "y");
			$oValidate->add_text_field("CentroDescricao", $oVReconciliacao->getCentroDescricao(), "text", "y");
			$oValidate->add_number_field("NegCodigo", $oVReconciliacao->getNegCodigo(), "number", "y");
			$oValidate->add_text_field("UnidadeNegocioDescricao", $oVReconciliacao->getUnidadeNegocioDescricao(), "text", "y");
			$oValidate->add_text_field("CcrConta", $oVReconciliacao->getCcrConta(), "text", "y");
			$oValidate->add_text_field("Empresa", $oVReconciliacao->getEmpresa(), "text", "y");
			//$oValidate->add_text_field("MovObs", $oVReconciliacao->getMovObs(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VReconciliacao.preparaFormulario&sOP=".$sOP."&nIdVReconciliacao=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVReconciliacao($oVReconciliacao)){
 					unset($_SESSION['oVReconciliacao']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VReconciliacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VReconciliacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
			case "GerarRelarotio":
				if($_REQUEST['fTipoRelatorio'] == "extrato"){
					header("Location:http://200.98.201.88/webmoney/relatorios/?rel=extrato_conciliacao&cod_unidade=212&conta=196&data= ");					
					exit();
				}elseif($_REQUEST['fTipoRelatorio'] == "reconciliacao"){
					header("Location:http:");					
					exit();
				}
			break;
 			case "Alterar":
 				if($oFachada->alterarVReconciliacao($oVReconciliacao)){
 					unset($_SESSION['oVReconciliacao']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VReconciliacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VReconciliacao.preparaFormulario&sOP=".$sOP."&nIdVReconciliacao=";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVReconciliacao = explode("____",$_REQUEST['fIdVReconciliacao']);
   				foreach($vIdPaiVReconciliacao as $vIdFilhoVReconciliacao){
  					$vIdVReconciliacao = explode("||",$vIdFilhoVReconciliacao);
 					foreach($vIdVReconciliacao as $nIdVReconciliacao){
  						$bResultado &= $oFachada->excluirVReconciliacao($vIdVReconciliacao[0],$vIdVReconciliacao[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VReconciliacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VReconciliacao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
	
	function recuperaContaCorrente(){
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$nCodUnidade = $_REQUEST['nUniCodigo'];
		$_REQUEST['voMnyContaCorrente'] = $oFachadaFinanceiro->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);		
		include_once("view/view/v_reconciliacao/conta_ajax.php");
		exit();
	}
 
 }
 
 
 ?>
