<?php
 class VPessoaFormatadaCTR implements IControle{
 
 	public function VPessoaFormatadaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVPessoaFormatada = $oFachada->recuperarTodosVPessoaFormatada();
 
 		$_REQUEST['voVPessoaFormatada'] = $voVPessoaFormatada;
 		
 		
 		include_once("view/view/v_pessoa_formatada/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVPessoaFormatada = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVPessoaFormatada = ($_POST['fIdVPessoaFormatada'][0]) ? $_POST['fIdVPessoaFormatada'][0] : $_GET['nIdVPessoaFormatada'];
 	
 			if($nIdVPessoaFormatada){
 				$vIdVPessoaFormatada = explode("||",$nIdVPessoaFormatada);
 				$oVPessoaFormatada = $oFachada->recuperarUmVPessoaFormatada();
 			}
 		}
 		
 		$_REQUEST['oVPessoaFormatada'] = ($_SESSION['oVPessoaFormatada']) ? $_SESSION['oVPessoaFormatada'] : $oVPessoaFormatada;
 		unset($_SESSION['oVPessoaFormatada']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_pessoa_formatada/detalhe.php");
 		else
 			include_once("view/view/v_pessoa_formatada/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVPessoaFormatada = $oFachada->inicializarVPessoaFormatada($_POST['fPesgCodigo'],$_POST['fPesgInc'],$_POST['fPesgAlt'],$_POST['fTipCodigo'],$_POST['fCodStatus'],$_POST['fPesFones']);
 			$_SESSION['oVPessoaFormatada'] = $oVPessoaFormatada;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("PesgCodigo", $oVPessoaFormatada->getPesgCodigo(), "number", "y");
			$oValidate->add_text_field("PesgInc", $oVPessoaFormatada->getPesgInc(), "text", "y");
			$oValidate->add_text_field("PesgAlt", $oVPessoaFormatada->getPesgAlt(), "text", "y");
			$oValidate->add_number_field("TipCodigo", $oVPessoaFormatada->getTipCodigo(), "number", "y");
			$oValidate->add_number_field("CodStatus", $oVPessoaFormatada->getCodStatus(), "number", "y");
			$oValidate->add_text_field("PesFones", $oVPessoaFormatada->getPesFones(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VPessoaFormatada.preparaFormulario&sOP=".$sOP."&nIdVPessoaFormatada="";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVPessoaFormatada($oVPessoaFormatada)){
 					unset($_SESSION['oVPessoaFormatada']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaFormatada.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VPessoaFormatada.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVPessoaFormatada($oVPessoaFormatada)){
 					unset($_SESSION['oVPessoaFormatada']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaFormatada.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VPessoaFormatada.preparaFormulario&sOP=".$sOP."&nIdVPessoaFormatada="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVPessoaFormatada = explode("____",$_REQUEST['fIdVPessoaFormatada']);
   				foreach($vIdPaiVPessoaFormatada as $vIdFilhoVPessoaFormatada){
  					$vIdVPessoaFormatada = explode("||",$vIdFilhoVPessoaFormatada);
 					foreach($vIdVPessoaFormatada as $nIdVPessoaFormatada){
  						$bResultado &= $oFachada->excluirVPessoaFormatada($vIdVPessoaFormatada[0],$vIdVPessoaFormatada[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VPessoaFormatada.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VPessoaFormatada.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
