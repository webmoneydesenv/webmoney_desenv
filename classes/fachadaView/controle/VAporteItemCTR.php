<?php
 class VAporteItemCTR implements IControle{
 
 	public function VAporteItemCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVAporteItem = $oFachada->recuperarTodosVAporteItem();
 
 		$_REQUEST['voVAporteItem'] = $voVAporteItem;
 		
 		
 		include_once("view/view/v_aporte_item/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVAporteItem = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVAporteItem = ($_POST['fIdVAporteItem'][0]) ? $_POST['fIdVAporteItem'][0] : $_GET['nIdVAporteItem'];
 	
 			if($nIdVAporteItem){
 				$vIdVAporteItem = explode("||",$nIdVAporteItem);
 				$oVAporteItem = $oFachada->recuperarUmVAporteItem();
 			}
 		}
 		
 		$_REQUEST['oVAporteItem'] = ($_SESSION['oVAporteItem']) ? $_SESSION['oVAporteItem'] : $oVAporteItem;
 		unset($_SESSION['oVAporteItem']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_aporte_item/detalhe.php");
 		else
 			include_once("view/view/v_aporte_item/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVAporteItem = $oFachada->inicializarVAporteItem($_POST['fCodSolicitacaoAporte'],$_POST['fMovCodigo'],$_POST['fMovItem'],mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesCodigo'],mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fMovValor']);
 			$_SESSION['oVAporteItem'] = $oVAporteItem;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("CodSolicitacaoAporte", $oVAporteItem->getCodSolicitacaoAporte(), "number", "y");
			//$oValidate->add_number_field("MovCodigo", $oVAporteItem->getMovCodigo(), "number", "y");
			//$oValidate->add_number_field("MovItem", $oVAporteItem->getMovItem(), "number", "y");
			$oValidate->add_text_field("MovDocumento", $oVAporteItem->getMovDocumento(), "text", "y");
			//$oValidate->add_text_field("MovObs", $oVAporteItem->getMovObs(), "text", "y");
			$oValidate->add_number_field("PesCodigo", $oVAporteItem->getPesCodigo(), "number", "y");
			$oValidate->add_text_field("Nome", $oVAporteItem->getNome(), "text", "y");
			$oValidate->add_number_field("MovValor", $oVAporteItem->getMovValor(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VAporteItem.preparaFormulario&sOP=".$sOP."&nIdVAporteItem="";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVAporteItem($oVAporteItem)){
 					unset($_SESSION['oVAporteItem']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VAporteItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VAporteItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVAporteItem($oVAporteItem)){
 					unset($_SESSION['oVAporteItem']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VAporteItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VAporteItem.preparaFormulario&sOP=".$sOP."&nIdVAporteItem="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVAporteItem = explode("____",$_REQUEST['fIdVAporteItem']);
   				foreach($vIdPaiVAporteItem as $vIdFilhoVAporteItem){
  					$vIdVAporteItem = explode("||",$vIdFilhoVAporteItem);
 					foreach($vIdVAporteItem as $nIdVAporteItem){
  						$bResultado &= $oFachada->excluirVAporteItem($vIdVAporteItem[0],$vIdVAporteItem[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VAporteItem.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VAporteItem.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
