<?php
 class VMovimentoCTR implements IControle{
 
 	public function VMovimentoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		if($_REQUEST['sTipoMovimentoCodigo'])
	 		$voVMovimento = $oFachada->recuperarTodosVMovimentoPorTipo($_REQUEST['sTipoMovimentoCodigo']);
 
 		$_REQUEST['voVMovimento'] = $voVMovimento;
 		
 		
 		include_once("view/view/v_movimento/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVMovimento = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVMovimento = ($_POST['fIdVMovimento'][0]) ? $_POST['fIdVMovimento'][0] : $_GET['nIdVMovimento'];
 	
 			if($nIdVMovimento){
 				$vIdVMovimento = explode("||",$nIdVMovimento);
 				$oVMovimento = $oFachada->recuperarUmVMovimento();
 			}
 		}
 		
 		$_REQUEST['oVMovimento'] = ($_SESSION['oVMovimento']) ? $_SESSION['oVMovimento'] : $oVMovimento;
 		unset($_SESSION['oVMovimento']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_movimento/detalhe.php");
 		else
 			include_once("view/view/v_movimento/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 	}
 
 }
 
 
 ?>
