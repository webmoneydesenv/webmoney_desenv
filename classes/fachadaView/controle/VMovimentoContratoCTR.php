<?php
 class VMovimentoContratoCTR implements IControle{
 
 	public function VMovimentoContratoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVMovimentoContrato = $oFachada->recuperarTodosVMovimentoContratoSemContrato($_SESSION['oEmpresa']->getEmpCodigo());
 		$_REQUEST['voVMovimentoContrato'] = $voVMovimentoContrato;
 		
 		
 		include_once("view/view/v_movimento_contrato/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVMovimentoContrato = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVMovimentoContrato = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdVMovimentoContrato'];
 			
 			if($nIdVMovimentoContrato){
 				$fIdMnyContratoPessoa = explode("||",$nIdVMovimentoContrato);
 				$oVMovimentoContrato = $oFachada->recuperarUmVMovimentoContratoPorMovimento($fIdMnyContratoPessoa[1]);				
 			}
 		}
 		
		$nMovCodigo = $fIdMnyContratoPessoa[1];
	
		$_REQUEST['voVPesquisaMovimentoItem'] = $oFachada->recuperarTodosPesquisaContratoMovimentoItemPorComplemento($nMovCodigo); 
		/*	print_r($_REQUEST['voVPesquisaMovimentoItem']);
		die();
		*/
		
 		$_REQUEST['oVMovimentoContrato'] = ($_SESSION['oVMovimentoContrato']) ? $_SESSION['oVMovimentoContrato'] : $oVMovimentoContrato;
 		unset($_SESSION['oVMovimentoContrato']);
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_movimento_contrato/detalhe.php");
 		else
 			include_once("view/view/v_movimento_contrato/insere_altera.php");
 		exit();
 	}
 
 	public function processaFormulario(){
 		
 	}
 
 }
 
 
 ?>
