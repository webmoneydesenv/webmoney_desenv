<?php
 class VSupriTramitacaoScmCTR implements IControle{

 	public function VSupriTramitacaoScmCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();

 		$voVSupriTramitacaoScm = $oFachada->recuperarTodosVSupriTramitacaoScm();

 		$_REQUEST['voVSupriTramitacaoScm'] = $voVSupriTramitacaoScm;


 		include_once("view/View/v_supri_tramitacao_scm/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$oVSupriTramitacaoScm = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVSupriTramitacaoScm = ($_POST['fIdVSupriTramitacaoScm'][0]) ? $_POST['fIdVSupriTramitacaoScm'][0] : $_GET['nIdVSupriTramitacaoScm'];

 			if($nIdVSupriTramitacaoScm){
 				$vIdVSupriTramitacaoScm = explode("||",$nIdVSupriTramitacaoScm);
 				$oVSupriTramitacaoScm = $oFachada->recuperarUmVSupriTramitacaoScm();
 			}
 		}

 		$_REQUEST['oVSupriTramitacaoScm'] = ($_SESSION['oVSupriTramitacaoScm']) ? $_SESSION['oVSupriTramitacaoScm'] : $oVSupriTramitacaoScm;
 		unset($_SESSION['oVSupriTramitacaoScm']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/View/v_supri_tramitacao_scm/detalhe.php");
 		else
 			include_once("view/View/v_supri_tramitacao_scm/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oVSupriTramitacaoScm = $oFachada->inicializarVSupriTramitacaoScm($_POST['fCodProtocoloScm'],$_POST['fCodScm'],mb_convert_case($_POST['fTramDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fUsuario'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],$_POST['fAtivo'],$_POST['fCodDe'],mb_convert_case($_POST['fDe'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodPara'],mb_convert_case($_POST['fPara'],MB_CASE_UPPER, "UTF-8"),$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo']);
 			$_SESSION['oVSupriTramitacaoScm'] = $oVSupriTramitacaoScm;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodProtocoloScm", $oVSupriTramitacaoScm->getCodProtocoloScm(), "number", "y");
			$oValidate->add_number_field("CodScm", $oVSupriTramitacaoScm->getCodScm(), "number", "y");
			//$oValidate->add_text_field("TramDescricao", $oVSupriTramitacaoScm->getTramDescricao(), "text", "y");
			//$oValidate->add_text_field("Usuario", $oVSupriTramitacaoScm->getUsuario(), "text", "y");
			$oValidate->add_date_field("Data", $oVSupriTramitacaoScm->getData(), "date", "y");
			//$oValidate->add_number_field("Ativo", $oVSupriTramitacaoScm->getAtivo(), "number", "y");
			$oValidate->add_number_field("CodDe", $oVSupriTramitacaoScm->getCodDe(), "number", "y");
			//$oValidate->add_text_field("De", $oVSupriTramitacaoScm->getDe(), "text", "y");
			$oValidate->add_number_field("CodPara", $oVSupriTramitacaoScm->getCodPara(), "number", "y");
			//$oValidate->add_text_field("Para", $oVSupriTramitacaoScm->getPara(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VSupriTramitacaoScm.preparaFormulario&sOP=".$sOP."&nIdVSupriTramitacaoScm=".;
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVSupriTramitacaoScm($oVSupriTramitacaoScm)){
 					unset($_SESSION['oVSupriTramitacaoScm']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VSupriTramitacaoScm.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VSupriTramitacaoScm.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVSupriTramitacaoScm($oVSupriTramitacaoScm)){
 					unset($_SESSION['oVSupriTramitacaoScm']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VSupriTramitacaoScm.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 										$sHeader = "?bErro=1&action=VSupriTramitacaoScm.preparaFormulario&sOP=".$sOP."&nIdVSupriTramitacaoScm=".;
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdVSupriTramitacaoScm']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirVSupriTramitacaoScm($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VSupriTramitacaoScm.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VSupriTramitacaoScm.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
