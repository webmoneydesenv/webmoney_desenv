<?php
 class VContratoAditivoCTR implements IControle{

 	public function VContratoAditivoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();

 		$voVContratoAditivo = $oFachada->recuperarTodosVContratoAditivo();

 		$_REQUEST['voVContratoAditivo'] = $voVContratoAditivo;


 		include_once("view/view/v_contrato_aditivo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$oVContratoAditivo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVContratoAditivo = ($_POST['fIdVContratoAditivo'][0]) ? $_POST['fIdVContratoAditivo'][0] : $_GET['nIdVContratoAditivo'];

 			if($nIdVContratoAditivo){
 				$vIdVContratoAditivo = explode("||",$nIdVContratoAditivo);
 				$oVContratoAditivo = $oFachada->recuperarUmVContratoAditivo();
 			}
 		}

 		$_REQUEST['oVContratoAditivo'] = ($_SESSION['oVContratoAditivo']) ? $_SESSION['oVContratoAditivo'] : $oVContratoAditivo;
 		unset($_SESSION['oVContratoAditivo']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_contrato_aditivo/detalhe.php");
 		else
 			include_once("view/view/v_contrato_aditivo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oVContratoAditivo = $oFachada->inicializarVContratoAditivo($_POST['fContratoCodigo'],mb_convert_case($_POST['fNumero'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataInicio'],$_POST['fDataValidade'],$_POST['fValorContrato'],mb_convert_case($_POST['fTipoContrato'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVContratoAditivo'] = $oVContratoAditivo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("ContratoCodigo", $oVContratoAditivo->getContratoCodigo(), "number", "y");
			//$oValidate->add_text_field("Numero", $oVContratoAditivo->getNumero(), "text", "y");
			//$oValidate->add_text_field("Descricao", $oVContratoAditivo->getDescricao(), "text", "y");
			//$oValidate->add_date_field("DataInicio", $oVContratoAditivo->getDataInicio(), "date", "y");
			//$oValidate->add_date_field("DataValidade", $oVContratoAditivo->getDataValidade(), "date", "y");
			//$oValidate->add_number_field("ValorContrato", $oVContratoAditivo->getValorContrato(), "number", "y");
			$oValidate->add_text_field("TipoContrato", $oVContratoAditivo->getTipoContrato(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VContratoAditivo.preparaFormulario&sOP=".$sOP."&nIdVContratoAditivo="";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVContratoAditivo($oVContratoAditivo)){
 					unset($_SESSION['oVContratoAditivo']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VContratoAditivo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VContratoAditivo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVContratoAditivo($oVContratoAditivo)){
 					unset($_SESSION['oVContratoAditivo']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VContratoAditivo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 										$sHeader = "?bErro=1&action=VContratoAditivo.preparaFormulario&sOP=".$sOP."&nIdVContratoAditivo=""";
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdVContratoAditivo']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirVContratoAditivo($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VContratoAditivo.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VContratoAditivo.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
