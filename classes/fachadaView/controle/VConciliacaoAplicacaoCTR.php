<?php
 class VConciliacaoAplicacaoCTR implements IControle{
 
 	public function VConciliacaoAplicacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVConciliacaoAplicacao = $oFachada->recuperarTodosVConciliacaoAplicacao();
 
 		$_REQUEST['voVConciliacaoAplicacao'] = $voVConciliacaoAplicacao;
 		
 		
 		include_once("view/view/v_conciliacao_aplicacao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVConciliacaoAplicacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVConciliacaoAplicacao = ($_POST['fIdVConciliacaoAplicacao'][0]) ? $_POST['fIdVConciliacaoAplicacao'][0] : $_GET['nIdVConciliacaoAplicacao'];
 	
 			if($nIdVConciliacaoAplicacao){
 				$vIdVConciliacaoAplicacao = explode("||",$nIdVConciliacaoAplicacao);
 				$oVConciliacaoAplicacao = $oFachada->recuperarUmVConciliacaoAplicacao();
 			}
 		}
 		
 		$_REQUEST['oVConciliacaoAplicacao'] = ($_SESSION['oVConciliacaoAplicacao']) ? $_SESSION['oVConciliacaoAplicacao'] : $oVConciliacaoAplicacao;
 		unset($_SESSION['oVConciliacaoAplicacao']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_conciliacao_aplicacao/detalhe.php");
 		else
 			include_once("view/view/v_conciliacao_aplicacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVConciliacaoAplicacao = $oFachada->inicializarVConciliacaoAplicacao(mb_convert_case($_POST['fOrdem'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataConciliacao'],$_POST['fConta']);
 			$_SESSION['oVConciliacaoAplicacao'] = $oVConciliacaoAplicacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			//$oValidate->add_text_field("Ordem", $oVConciliacaoAplicacao->getOrdem(), "text", "y");
			//$oValidate->add_date_field("DataConciliacao", $oVConciliacaoAplicacao->getDataConciliacao(), "date", "y");
			//$oValidate->add_number_field("Conta", $oVConciliacaoAplicacao->getConta(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VConciliacaoAplicacao.preparaFormulario&sOP=".$sOP."&nIdVConciliacaoAplicacao="";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVConciliacaoAplicacao($oVConciliacaoAplicacao)){
 					unset($_SESSION['oVConciliacaoAplicacao']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VConciliacaoAplicacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VConciliacaoAplicacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVConciliacaoAplicacao($oVConciliacaoAplicacao)){
 					unset($_SESSION['oVConciliacaoAplicacao']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VConciliacaoAplicacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VConciliacaoAplicacao.preparaFormulario&sOP=".$sOP."&nIdVConciliacaoAplicacao="";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVConciliacaoAplicacao = explode("____",$_REQUEST['fIdVConciliacaoAplicacao']);
   				foreach($vIdPaiVConciliacaoAplicacao as $vIdFilhoVConciliacaoAplicacao){
  					$vIdVConciliacaoAplicacao = explode("||",$vIdFilhoVConciliacaoAplicacao);
 					foreach($vIdVConciliacaoAplicacao as $nIdVConciliacaoAplicacao){
  						$bResultado &= $oFachada->excluirVConciliacaoAplicacao($vIdVConciliacaoAplicacao[0],$vIdVConciliacaoAplicacao[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VConciliacaoAplicacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VConciliacaoAplicacao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
