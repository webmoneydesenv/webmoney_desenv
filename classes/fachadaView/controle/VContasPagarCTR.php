<?php
 class VContasPagarCTR implements IControle{
 
 	public function VContasPagarCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaViewBD();
 
 		$voVContasPagar = $oFachada->recuperarTodosVContasPagar();
 
 		$_REQUEST['voVContasPagar'] = $voVContasPagar;
 		
 		
 		include_once("view/view/v_contas_pagar/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$oVContasPagar = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdVContasPagar = ($_POST['fIdVContasPagar'][0]) ? $_POST['fIdVContasPagar'][0] : $_GET['nIdVContasPagar'];
 	
 			if($nIdVContasPagar){
 				$vIdVContasPagar = explode("||",$nIdVContasPagar);
 				$oVContasPagar = $oFachada->recuperarUmVContasPagar();
 			}
 		}
 		
 		$_REQUEST['oVContasPagar'] = ($_SESSION['oVContasPagar']) ? $_SESSION['oVContasPagar'] : $oVContasPagar;
 		unset($_SESSION['oVContasPagar']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/view/v_contas_pagar/detalhe.php");
 		else
 			include_once("view/view/v_contas_pagar/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaViewBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oVContasPagar = $oFachada->inicializarVContasPagar($_POST['fTipAceCodigo'],mb_convert_case($_POST['fEmpFantasia'],MB_CASE_UPPER, "UTF-8"),$_POST['fEmpCodigo'],mb_convert_case($_POST['fCentroCusto'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fContaContabil'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fCusto'],MB_CASE_UPPER, "UTF-8"),$_POST['fTipDocCodigo'],$_POST['fCompetencia'],$_POST['fMovItem'],$_POST['fTotalItens'],$_POST['fVencimento'],$_POST['fFpgCodigo'],$_POST['fValorPrincipal'],$_POST['fJuros'],$_POST['fRetencaoDesconto'],$_POST['fMovCodigo'],mb_convert_case($_POST['fNumeroDocumento'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fHistorico'],MB_CASE_UPPER, "UTF-8"),$_POST['fUniCodigo'],mb_convert_case($_POST['fPessoa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fTipoDoc'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fFormaPagamento'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fUnidade'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oVContasPagar'] = $oVContasPagar;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("TipAceCodigo", $oVContasPagar->getTipAceCodigo(), "number", "y");
			$oValidate->add_text_field("EmpFantasia", $oVContasPagar->getEmpFantasia(), "text", "y");
			//$oValidate->add_text_field("CentroCusto", $oVContasPagar->getCentroCusto(), "text", "y");
			$oValidate->add_text_field("ContaContabil", $oVContasPagar->getContaContabil(), "text", "y");
			//$oValidate->add_text_field("Custo", $oVContasPagar->getCusto(), "text", "y");
			$oValidate->add_number_field("TipDocCodigo", $oVContasPagar->getTipDocCodigo(), "number", "y");
			$oValidate->add_date_field("Competencia", $oVContasPagar->getCompetencia(), "date", "y");
			$oValidate->add_number_field("MovItem", $oVContasPagar->getMovItem(), "number", "y");
			//$oValidate->add_number_field("TotalItens", $oVContasPagar->getTotalItens(), "number", "y");
			$oValidate->add_date_field("Vencimento", $oVContasPagar->getVencimento(), "date", "y");
			$oValidate->add_number_field("FpgCodigo", $oVContasPagar->getFpgCodigo(), "number", "y");
			$oValidate->add_number_field("ValorPrincipal", $oVContasPagar->getValorPrincipal(), "number", "y");
			$oValidate->add_number_field("Juros", $oVContasPagar->getJuros(), "number", "y");
			$oValidate->add_number_field("RetencaoDesconto", $oVContasPagar->getRetencaoDesconto(), "number", "y");
			$oValidate->add_number_field("MovCodigo", $oVContasPagar->getMovCodigo(), "number", "y");
			$oValidate->add_text_field("NumeroDocumento", $oVContasPagar->getNumeroDocumento(), "text", "y");
			//$oValidate->add_text_field("Historico", $oVContasPagar->getHistorico(), "text", "y");
			$oValidate->add_text_field("Pessoa", $oVContasPagar->getPessoa(), "text", "y");
			$oValidate->add_text_field("TipoDoc", $oVContasPagar->getTipoDoc(), "text", "y");
			$oValidate->add_text_field("FormaPagamento", $oVContasPagar->getFormaPagamento(), "text", "y");
			$oValidate->add_text_field("Unidade", $oVContasPagar->getUnidade(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=VContasPagar.preparaFormulario&sOP=".$sOP."&nIdVContasPagar=";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirVContasPagar($oVContasPagar)){
 					unset($_SESSION['oVContasPagar']);
 					$_SESSION['sMsg'] = "VIEW inserido com sucesso!";
 					$sHeader = "?bErro=0&action=VContasPagar.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o VIEW!";
 					$sHeader = "?bErro=1&action=VContasPagar.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarVContasPagar($oVContasPagar)){
 					unset($_SESSION['oVContasPagar']);
 					$_SESSION['sMsg'] = "VIEW alterado com sucesso!";
 					$sHeader = "?bErro=0&action=VContasPagar.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o VIEW!";
 					$sHeader = "?bErro=1&action=VContasPagar.preparaFormulario&sOP=".$sOP."&nIdVContasPagar=";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiVContasPagar = explode("____",$_REQUEST['fIdVContasPagar']);
   				foreach($vIdPaiVContasPagar as $vIdFilhoVContasPagar){
  					$vIdVContasPagar = explode("||",$vIdFilhoVContasPagar);
 					foreach($vIdVContasPagar as $nIdVContasPagar){
  						$bResultado &= $oFachada->excluirVContasPagar($vIdVContasPagar[0],$vIdVContasPagar[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "VIEW(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=VContasPagar.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) VIEW!";
 					$sHeader = "?bErro=1&action=VContasPagar.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
