<?php
 /**
  * @author Auto-Generated
  * @package fachadaView
  * @SGBD mysql
  * @tabela v_supri_tramitacao_scm
  */
 class VSupriTramitacaoScm{
 	/**
	* @campo cod_protocolo_scm
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodProtocoloScm;
	/**
	* @campo cod_scm
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodScm;
	/**
	* @campo tram_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTramDescricao;
	/**
	* @campo usuario
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUsuario;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo cod_de
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodDe;
	/**
	* @campo de
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDe;
	/**
	* @campo cod_para
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodPara;
	/**
	* @campo para
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPara;
     /**
	* @campo de_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDeCodTipoProtocolo;
     /**
	* @campo para_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nParaCodTipoProtocolo;

 	public function __construct(){

 	}

 	public function setCodProtocoloScm($nCodProtocoloScm){
		$this->nCodProtocoloScm = $nCodProtocoloScm;
	}
	public function getCodProtocoloScm(){
		return $this->nCodProtocoloScm;
	}
	public function setCodScm($nCodScm){
		$this->nCodScm = $nCodScm;
	}
	public function getCodScm(){
		return $this->nCodScm;
	}
	public function setTramDescricao($sTramDescricao){
		$this->sTramDescricao = $sTramDescricao;
	}
	public function getTramDescricao(){
		return $this->sTramDescricao;
	}
	public function setUsuario($sUsuario){
		$this->sUsuario = $sUsuario;
	}
	public function getUsuario(){
		return $this->sUsuario;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		 if($dData){
			 $oData = DateTime::createFromFormat('d/m/Y', $dData);
			 $this->dData = $oData->format('Y-m-d') ;
	}
		 }
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setCodDe($nCodDe){
		$this->nCodDe = $nCodDe;
	}
	public function getCodDe(){
		return $this->nCodDe;
	}
	public function setDe($sDe){
		$this->sDe = $sDe;
	}
	public function getDe(){
		return $this->sDe;
	}
	public function setCodPara($nCodPara){
		$this->nCodPara = $nCodPara;
	}
	public function getCodPara(){
		return $this->nCodPara;
	}
	public function setPara($sPara){
		$this->sPara = $sPara;
	}
	public function getPara(){
		return $this->sPara;
	}

    public function setDeCodTipoProtocolo($nDeCodTipoProtocolo){
		$this->nDeCodTipoProtocolo = $nDeCodTipoProtocolo;
	}
	public function getDeCodTipoProtocolo(){
		return $this->nDeCodTipoProtocolo;
	}

    public function setParaCodTipoProtocolo($nParaCodTipoProtocolo){
		$this->nParaCodTipoProtocolo = $nParaCodTipoProtocolo;
	}
	public function setParaCodTipoProtocolo(){
		return $this->nParaCodTipoProtocolo;
	}

 }
 ?>
