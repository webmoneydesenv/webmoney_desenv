<?php
 /**
  * @author Auto-Generated
  * @package fachadaView
  * @SGBD mysql
  * @tabela v_relatorio_por_empresa
  */
 class VRelatorioPorEmpresa{
 	/**
	* @campo FA
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFa;
	/**
	* @campo Credor
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCredor;
	/**
	* @campo Empresa
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpresa;
	/**
	* @campo Unidade
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUnidade;
	/**
	* @campo Descricao_Despesa
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricaoDespesa;
	/**
	* @campo aporte
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAporte;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo Parcela
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sParcela;
	/**
	* @campo competencia
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCompetencia;
	/**
	* @campo vencimento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sVencimento;
	/**
	* @campo forma_pagamento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFormaPagamento;
	/**
	* @campo mov_valor_pagar
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovValorPagar;
	/**
	* @campo acrescimo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAcrescimo;
	/**
	* @campo mov_juros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovJuros;
	/**
	* @campo desconto
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDesconto;
	/**
	* @campo reprogramado
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sReprogramado;
	/**
	* @campo pago
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPago;
	/**
	* @campo pagar
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPagar;


 	public function __construct(){

 	}

 	public function setFa($sFa){
		$this->sFa = $sFa;
	}
	public function getFa(){
		return $this->sFa;
	}
	public function setCredor($sCredor){
		$this->sCredor = $sCredor;
	}
	public function getCredor(){
		return $this->sCredor;
	}
	public function setEmpresa($sEmpresa){
		$this->sEmpresa = $sEmpresa;
	}
	public function getEmpresa(){
		return $this->sEmpresa;
	}
	public function setUnidade($sUnidade){
		$this->sUnidade = $sUnidade;
	}
	public function getUnidade(){
		return $this->sUnidade;
	}
	public function setDescricaoDespesa($sDescricaoDespesa){
		$this->sDescricaoDespesa = $sDescricaoDespesa;
	}
	public function getDescricaoDespesa(){
		return $this->sDescricaoDespesa;
	}
	public function setAporte($sAporte){
		$this->sAporte = $sAporte;
	}
	public function getAporte(){
		return $this->sAporte;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setParcela($sParcela){
		$this->sParcela = $sParcela;
	}
	public function getParcela(){
		return $this->sParcela;
	}
	public function setCompetencia($sCompetencia){
		$this->sCompetencia = $sCompetencia;
	}
	public function getCompetencia(){
		return $this->sCompetencia;
	}
	public function setVencimento($sVencimento){
		$this->sVencimento = $sVencimento;
	}
	public function getVencimento(){
		return $this->sVencimento;
	}
	public function setFormaPagamento($sFormaPagamento){
		$this->sFormaPagamento = $sFormaPagamento;
	}
	public function getFormaPagamento(){
		return $this->sFormaPagamento;
	}
	public function setMovValorPagar($nMovValorPagar){
		$this->nMovValorPagar = $nMovValorPagar;
	}
	public function getMovValorPagar(){
		return $this->nMovValorPagar;
	}
	public function getMovValorPagarFormatado(){
		 $vRetorno = number_format($this->nMovValorPagar , 2, ',', '.');		 return $vRetorno;
	}
	public function setMovValorPagarBanco($nMovValorPagar){
		if($nMovValorPagar){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPagar = str_replace($sOrigem, $sDestino, $nMovValorPagar);

		}else{
		$this->nMovValorPagar = 'null';
			}
		}
public function setAcrescimo($nAcrescimo){
		$this->nAcrescimo = $nAcrescimo;
	}
	public function getAcrescimo(){
		return $this->nAcrescimo;
	}
	public function getAcrescimoFormatado(){
		 $vRetorno = number_format($this->nAcrescimo , 2, ',', '.');		 return $vRetorno;
	}
	public function setAcrescimoBanco($nAcrescimo){
		if($nAcrescimo){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nAcrescimo = str_replace($sOrigem, $sDestino, $nAcrescimo);

		}else{
		$this->nAcrescimo = 'null';
			}
		}
public function setMovJuros($nMovJuros){
		$this->nMovJuros = $nMovJuros;
	}
	public function getMovJuros(){
		return $this->nMovJuros;
	}
	public function getMovJurosFormatado(){
		 $vRetorno = number_format($this->nMovJuros , 2, ',', '.');		 return $vRetorno;
	}
	public function setMovJurosBanco($nMovJuros){
		if($nMovJuros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovJuros = str_replace($sOrigem, $sDestino, $nMovJuros);

		}else{
		$this->nMovJuros = 'null';
			}
		}
public function setDesconto($nDesconto){
		$this->nDesconto = $nDesconto;
	}
	public function getDesconto(){
		return $this->nDesconto;
	}
	public function getDescontoFormatado(){
		 $vRetorno = number_format($this->nDesconto , 2, ',', '.');		 return $vRetorno;
	}
	public function setDescontoBanco($nDesconto){
		if($nDesconto){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDesconto = str_replace($sOrigem, $sDestino, $nDesconto);

		}else{
		$this->nDesconto = 'null';
			}
		}
public function setReprogramado($sReprogramado){
		$this->sReprogramado = $sReprogramado;
	}
	public function getReprogramado(){
		return $this->sReprogramado;
	}
	public function setPago($sPago){
		$this->sPago = $sPago;
	}
	public function getPago(){
		return $this->sPago;
	}
	public function setPagar($sPagar){
		$this->sPagar = $sPagar;
	}
	public function getPagar(){
		return $this->sPagar;
	}

 }
 ?>
