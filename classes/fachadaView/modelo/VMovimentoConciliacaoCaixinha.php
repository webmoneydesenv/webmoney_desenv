<?php
 /**
  * @author Auto-Generated
  * @package fachadaView
  * @SGBD mysql
  * @tabela v_movimento_conciliacao_caixinha
  */
 class VMovimentoConciliacaoCaixinha{
 	/**
	* @campo cod_conciliacao
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodConciliacao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo cod_unidade
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo tipo_caixa
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipoCaixa;
	/**
	* @campo data_conciliacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataConciliacao;
	/**
	* @campo consolidado
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nConsolidado;
	/**
	* @campo ordem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nOrdem;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo mov_valor_parcela
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorParcela;
	/**
	* @campo pes_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesCodigo;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo historico
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sHistorico;
	/**
	* @campo unidade_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUnidadeDescricao;
	/**
	* @campo bco_nome
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sBcoNome;
	/**
	* @campo conta_corrente_conc
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nContaCorrenteConc;
	/**
	* @campo agencia_conta_conc
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAgenciaContaConc;

	/**
	* @campo agencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAgencia;
	/**
	* @campo conta
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sConta;


 	public function __construct(){

 	}

 	public function setCodConciliacao($nCodConciliacao){
		$this->nCodConciliacao = $nCodConciliacao;
	}
	public function getCodConciliacao(){
		return $this->nCodConciliacao;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setTipoCaixa($nTipoCaixa){
		$this->nTipoCaixa = $nTipoCaixa;
	}
	public function getTipoCaixa(){
		return $this->nTipoCaixa;
	}
	public function setDataConciliacao($dDataConciliacao){
		$this->dDataConciliacao = $dDataConciliacao;
	}
	public function getDataConciliacao(){
		return $this->dDataConciliacao;
	}
	public function getDataConciliacaoFormatado(){
		$oData = new DateTime($this->dDataConciliacao);
		 return $oData->format("d/m/Y");
	}
	public function setDataConciliacaoBanco($dDataConciliacao){
		 if($dDataConciliacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataConciliacao);
			 $this->dDataConciliacao = $oData->format('Y-m-d') ;
		}
	}
	public function setConsolidado($nConsolidado){
		$this->nConsolidado = $nConsolidado;
	}
	public function getConsolidado(){
		return $this->nConsolidado;
	}

	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setMovValorParcela($nMovValorParcela){
		$this->nMovValorParcela = $nMovValorParcela;
	}
	public function getMovValorParcela(){
		//$oFachada = new FachadaFinanceiroBD();
		//$this->nMovValorParcela = $oFachada->recuperarUmMnyMovimentoItemValorLiquido($this->nMovCodigo,$this->nMovItem);
		return $this->nMovValorParcela;
	}
	public function getMovValorParcelaFormatado(){
		 $vRetorno = number_format($this->nMovValorParcela , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorParcelaBanco($nMovValorParcela){
		if($nMovValorParcela){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorParcela = str_replace($sOrigem, $sDestino, $nMovValorParcela);
		}else{
			$this->nMovValorParcela = 'null';
			}
		}
	public function setPesCodigo($nPesCodigo){
		$this->nPesCodigo = $nPesCodigo;
	}
	public function getPesCodigo(){
		return $this->nPesCodigo;
	}
     public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setHistorico($sHistorico){
		$this->sHistorico = $sHistorico;
	}
	public function getHistorico(){
		return $this->sHistorico;
	}



	public function setUnidadeDescricao($nUnidadeDescricao){
		$this->nUnidadeDescricao = $nUnidadeDescricao;
	}
	public function getUnidadeDescricao(){
		return $this->nUnidadeDescricao;
	}

	public function setBcoNome($sBcoNome){
		$this->sBcoNome = $sBcoNome;
	}
	public function getBcoNome(){
		return $this->sBcoNome;
	}
		public function setContaCorrenteConc($nContaCorrenteConc){
		$this->nContaCorrenteConc = $nContaCorrenteConc;
	}
	public function getContaCorrenteConc(){
		return $this->nContaCorrenteConc;
	}
		public function setAgenciaContaConc($nAgenciaContaConc){
		$this->nAgenciaContaConc = $nAgenciaContaConc;
	}
	public function getAgenciaContaConc(){
		return $this->nAgenciaContaConc;
	}


	public function setContaCodigoConc($nContaCodigoConc){
		$this->nContaCodigoConc = $nContaCodigoConc;
	}
	public function getContaCodigoConc(){
		return $this->nContaCodigoConc;
	}
	public function setAgencia($sAgencia){
		$this->sAgencia = $sAgencia;
	}
	public function getAgencia(){
		return $this->sAgencia;
	}
	public function setConta($sConta){
		$this->sConta = $sConta;
	}
	public function getConta(){
		return $this->sConta;
	}
	public function setOrdem($nOrdem){
		$this->nOrdem = $nOrdem;
	}
	public function getOrdem(){
		return $this->nOrdem;
	}



	//metodo usado na ViewMovimentoConciliacao
	public function getPesCodigoFormatado(){
		 $vRetorno = number_format($this->nPesCodigo , 2, ',', '.');
		 return $vRetorno;
	}
	//metodo usado na ViewMovimentoConciliacao
	public function getCodUnidadeFormatado(){
		 $vRetorno = number_format($this->nCodUnidade , 2, ',', '.');
		 return $vRetorno;
	}
	//Metodo usado na ViewMovimentoConciliacao
	public function getContaCodigoFormatado(){
		 $vRetorno = number_format($this->nContaCodigoConc , 2, ',', '.');
		 return $vRetorno;
	}

	public function getTipoCaixaFormatado(){
		 $vRetorno = number_format($this->nTipoCaixa , 2, ',', '.');
		 return $vRetorno;
	}
 }
 ?>
