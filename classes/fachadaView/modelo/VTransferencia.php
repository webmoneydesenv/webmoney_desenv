<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_transferencia 
  */
 class VTransferencia{
 	/**
	* @campo mov_data_emissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataEmissao;
	/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValor;	
	/**
	* @campo cod_transferencia
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTransferencia;
	/**
	* @campo mov_codigo_origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigoOrigem;
	/**
	* @campo mov_item_origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItemOrigem;
	/**
	* @campo origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nOrigem;
	/**
	* @campo conta_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sContaOrigem;
	/**
	* @campo agencia_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAgenciaOrigem;
	/**
	* @campo banco_nome_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBancoNomeOrigem;
	/**
	* @campo cod_empresa_origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodEmpresaOrigem;
	/**
	* @campo empresa_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpresaOrigem;
	/**
	* @campo mov_codigo_destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigoDestino;
	/**
	* @campo mov_item_destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItemDestino;
	/**
	* @campo destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDestino;
	/**
	* @campo conta_destino
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sContaDestino;
	/**
	* @campo agencia_destino
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sAgenciaDestino;
	/**
	* @campo banco_nome_destino
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBancoNomeDestino;
	/**
	* @campo cod_empresa_destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodEmpresaDestino;
	/**
	* @campo empresa_destino
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpresaDestino;
	/**
	* @campo conta_contabil
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sContaContabil;
		/**
	* @campo centro_negocio
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCentroNegocio;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setMovDataEmissao($dMovDataEmissao){
		$this->dMovDataEmissao = $dMovDataEmissao;
	}
	public function getMovDataEmissao(){
		return $this->dMovDataEmissao;
	}
	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		 $vRetorno = number_format($this->nValor , 2, ',', '.');
		 return $vRetorno;
	}	
	public function getMovDataEmissaoFormatado(){
		$oData = new DateTime($this->dMovDataEmissao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataEmissaoBanco($dMovDataEmissao){
		 if($dMovDataEmissao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataEmissao);
			 $this->dMovDataEmissao = $oData->format('Y-m-d') ;
	}
		 }
	public function setCodTransferencia($nCodTransferencia){
		$this->nCodTransferencia = $nCodTransferencia;
	}
	public function getCodTransferencia(){
		return $this->nCodTransferencia;
	}
	public function setMovCodigoOrigem($nMovCodigoOrigem){
		$this->nMovCodigoOrigem = $nMovCodigoOrigem;
	}
	public function getMovCodigoOrigem(){
		return $this->nMovCodigoOrigem;
	}
	public function setMovItemOrigem($nMovItemOrigem){
		$this->nMovItemOrigem = $nMovItemOrigem;
	}
	public function getMovItemOrigem(){
		return $this->nMovItemOrigem;
	}
	public function setOrigem($nOrigem){
		$this->nOrigem = $nOrigem;
	}
	public function getOrigem(){
		return $this->nOrigem;
	}
	public function setContaOrigem($sContaOrigem){
		$this->sContaOrigem = $sContaOrigem;
	}
	public function getContaOrigem(){
		return $this->sContaOrigem;
	}
	public function setAgenciaOrigem($sAgenciaOrigem){
		$this->sAgenciaOrigem = $sAgenciaOrigem;
	}
	public function getAgenciaOrigem(){
		return $this->sAgenciaOrigem;
	}
	public function setBancoNomeOrigem($sBancoNomeOrigem){
		$this->sBancoNomeOrigem = $sBancoNomeOrigem;
	}
	public function getBancoNomeOrigem(){
		return $this->sBancoNomeOrigem;
	}
	public function setCodEmpresaOrigem($nCodEmpresaOrigem){
		$this->nCodEmpresaOrigem = $nCodEmpresaOrigem;
	}
	public function getCodEmpresaOrigem(){
		return $this->nCodEmpresaOrigem;
	}
	public function setEmpresaOrigem($sEmpresaOrigem){
		$this->sEmpresaOrigem = $sEmpresaOrigem;
	}
	public function getEmpresaOrigem(){
		return $this->sEmpresaOrigem;
	}
	public function setMovCodigoDestino($nMovCodigoDestino){
		$this->nMovCodigoDestino = $nMovCodigoDestino;
	}
	public function getMovCodigoDestino(){
		return $this->nMovCodigoDestino;
	}
	public function setMovItemDestino($nMovItemDestino){
		$this->nMovItemDestino = $nMovItemDestino;
	}
	public function getMovItemDestino(){
		return $this->nMovItemDestino;
	}
	public function setDestino($nDestino){
		$this->nDestino = $nDestino;
	}
	public function getDestino(){
		return $this->nDestino;
	}
	public function setContaDestino($sContaDestino){
		$this->sContaDestino = $sContaDestino;
	}
	public function getContaDestino(){
		return $this->sContaDestino;
	}
	public function setAgenciaDestino($sAgenciaDestino){
		$this->sAgenciaDestino = $sAgenciaDestino;
	}
	public function getAgenciaDestino(){
		return $this->sAgenciaDestino;
	}
	public function setBancoNomeDestino($sBancoNomeDestino){
		$this->sBancoNomeDestino = $sBancoNomeDestino;
	}
	public function getBancoNomeDestino(){
		return $this->sBancoNomeDestino;
	}
	public function setCodEmpresaDestino($nCodEmpresaDestino){
		$this->nCodEmpresaDestino = $nCodEmpresaDestino;
	}
	public function getCodEmpresaDestino(){
		return $this->nCodEmpresaDestino;
	}
	public function setEmpresaDestino($sEmpresaDestino){
		$this->sEmpresaDestino = $sEmpresaDestino;
	}
	public function getEmpresaDestino(){
		return $this->sEmpresaDestino;
	}
	
	public function setContaContabil($sContaContabil){
		$this->sContaContabil = $sContaContabil;
	}
	public function getContaContabil(){
		return $this->sContaContabil;
	}
	public function setCentroNegocio($sCentroNegocio){
		$this->sCentroNegocio = $sCentroNegocio;
	}
	public function getCentroNegocio(){
		return $this->sCentroNegocio;
	}
	
 }
 ?>
