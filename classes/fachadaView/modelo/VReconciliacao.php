<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_reconciliacao 
  */
 class VReconciliacao{
 	/**
	* @campo FA
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFa;
	/**
	* @campo mov_valor_pagar
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorPagar;
	/**
	* @campo fpg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFpgCodigo;
		/**
	* @campo data_vencimento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataVencimento;
	/**
	* @campo forma_de_pagamento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFormaDePagamento;
	/**
	* @campo tip_doc_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipDocCodigo;
	/**
	* @campo tipo_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipoDescricao;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataConciliacao;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo movimento_tipo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovimentoTipo;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo conta_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sContaDescricao;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo unidade_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidadeDescricao;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo centro_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCentroDescricao;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo unidade_negocio_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidadeNegocioDescricao;
	/**
	* @campo ccr_conta
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCcrConta;
	/**
	* @campo empresa
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpresa;
	/**
	* @campo fornecedor
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFornecedor;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovObs;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setFa($sFa){
		$this->sFa = $sFa;
	}
	public function getFa(){
		return $this->sFa;
	}
	public function setMovValorPagar($nMovValorPagar){
		$this->nMovValorPagar = $nMovValorPagar;
	}
	public function getMovValorPagar(){
		return $this->nMovValorPagar;
	}
	public function getMovValorPagarFormatado(){
		 $vRetorno = number_format($this->nMovValorPagar , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorPagarBanco($nMovValorPagar){
		if($nMovValorPagar){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPagar = str_replace($sOrigem, $sDestino, $nMovValorPagar);
	
		}else{
		$this->nMovValorPagar = 'null';
			}
		}
public function setFpgCodigo($nFpgCodigo){
		$this->nFpgCodigo = $nFpgCodigo;
	}
	public function getFpgCodigo(){
		return $this->nFpgCodigo;
	}
	public function setFormaDePagamento($sFormaDePagamento){
		$this->sFormaDePagamento = $sFormaDePagamento;
	}
	public function getFormaDePagamento(){
		return $this->sFormaDePagamento;
	}
	public function setTipDocCodigo($nTipDocCodigo){
		$this->nTipDocCodigo = $nTipDocCodigo;
	}
	public function getTipDocCodigo(){
		return $this->nTipDocCodigo;
	}
	public function setTipoDescricao($sTipoDescricao){
		$this->sTipoDescricao = $sTipoDescricao;
	}
	public function getTipoDescricao(){
		return $this->sTipoDescricao;
	}
	public function setDataConciliacao($dDataConciliacao){
		$this->dDataConciliacao = $dDataConciliacao;
	}
	public function getDataConciliacao(){
		return $this->dDataConciliacao;
	}
	public function getDataConciliacaoFormatado(){
		$oData = new DateTime($this->dDataConciliacao);
		 return $oData->format("d/m/Y");
	}
	public function setDataConciliacaoBanco($dDataConciliacao){
		 if($dDataConciliacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataConciliacao);
			 $this->dData = $oData->format('Y-m-d') ;
	}
		 }
	public function setDataVencimento($dDataVencimento){
		$this->dDataVencimento = $dDataVencimento;
	}
	public function getDataVencimento(){
		return $this->dDataVencimento;
	}
	public function getDataVencimentoFormatado(){
		$oData = new DateTime($this->dDataVencimento);
		 return $oData->format("d/m/Y");
	}
	public function setDataVencimentoBanco($dDataVencimento){
		 if($dDataVencimento){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataVencimento);
			 $this->dData = $oData->format('Y-m-d') ;
	}
		 }
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setMovimentoTipo($sMovimentoTipo){
		$this->sMovimentoTipo = $sMovimentoTipo;
	}
	public function getMovimentoTipo(){
		return $this->sMovimentoTipo;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setConCodigo($nConCodigo){
		$this->nConCodigo = $nConCodigo;
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	public function setContaDescricao($sContaDescricao){
		$this->sContaDescricao = $sContaDescricao;
	}
	public function getContaDescricao(){
		return $this->sContaDescricao;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setUnidadeDescricao($sUnidadeDescricao){
		$this->sUnidadeDescricao = $sUnidadeDescricao;
	}
	public function getUnidadeDescricao(){
		return $this->sUnidadeDescricao;
	}
	public function setCenCodigo($nCenCodigo){
		$this->nCenCodigo = $nCenCodigo;
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setCentroDescricao($sCentroDescricao){
		$this->sCentroDescricao = $sCentroDescricao;
	}
	public function getCentroDescricao(){
		return $this->sCentroDescricao;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setUnidadeNegocioDescricao($sUnidadeNegocioDescricao){
		$this->sUnidadeNegocioDescricao = $sUnidadeNegocioDescricao;
	}
	public function getUnidadeNegocioDescricao(){
		return $this->sUnidadeNegocioDescricao;
	}
	public function setCcrConta($sCcrConta){
		$this->sCcrConta = $sCcrConta;
	}
	public function getCcrConta(){
		return $this->sCcrConta;
	}
	public function setEmpresa($sEmpresa){
		$this->sEmpresa = $sEmpresa;
	}
	public function getEmpresa(){
		return $this->sEmpresa;
	}
	public function setFornecedor($sFornecedor){
		$this->sFornecedor= $sFornecedor;
	}
	public function getFornecedor(){
		return $this->sFornecedor;
	}
	public function setMovObs($sMovObs){
		$this->sMovObs = $sMovObs;
	}
	public function getMovObs(){
		return $this->sMovObs;
	}
	
 }
 ?>
