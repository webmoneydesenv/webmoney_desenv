<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_contas_pagar 
  */
 class VContasPagar{
 	/**
	* @campo tip_ace_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipAceCodigo;
	/**
	* @campo emp_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpFantasia;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo centro_custo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCentroCusto;
	/**
	* @campo conta_contabil
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sContaContabil;
	/**
	* @campo custo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCusto;
	/**
	* @campo tip_doc_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipDocCodigo;
	/**
	* @campo competencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dCompetencia;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo total_itens
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTotalItens;
	/**
	* @campo parcela
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nParcela;
	/**
	* @campo vencimento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dVencimento;
	/**
	* @campo fpg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFpgCodigo;
	/**
	* @campo valor_principal
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValorPrincipal;
	/**
	* @campo juros
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nJuros;
	/**
	* @campo retencao_desconto
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nRetencaoDesconto;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo numero_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNumeroDocumento;
	/**
	* @campo historico
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sHistorico;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nUniCodigo;	
	/**
	* @campo pessoa
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPessoa;
	/**
	* @campo tipo_doc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipoDoc;
	/**
	* @campo forma_pagamento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFormaPagamento;
	/**
	* @campo unidade
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidade;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setTipAceCodigo($nTipAceCodigo){
		$this->nTipAceCodigo = $nTipAceCodigo;
	}
	public function getTipAceCodigo(){
		return $this->nTipAceCodigo;
	}
	public function setEmpFantasia($sEmpFantasia){
		$this->sEmpFantasia = $sEmpFantasia;
	}
	public function getEmpFantasia(){
		return $this->sEmpFantasia;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setCentroCusto($sCentroCusto){
		$this->sCentroCusto = $sCentroCusto;
	}
	
	public function getCentroCusto(){
		return $this->sCentroCusto;
	}
	public function setContaContabil($sContaContabil){
		$this->sContaContabil = $sContaContabil;
	}
	public function getContaContabil(){
		return $this->sContaContabil;
	}
	public function setCusto($sCusto){
		$this->sCusto = $sCusto;
	}
	public function getCusto(){
		return $this->sCusto;
	}
	public function setTipDocCodigo($nTipDocCodigo){
		$this->nTipDocCodigo = $nTipDocCodigo;
	}
	public function getTipDocCodigo(){
		return $this->nTipDocCodigo;
	}
	public function setCompetencia($dCompetencia){
		$this->dCompetencia = $dCompetencia;
	}
	public function getCompetencia(){
		return $this->dCompetencia;
	}
	public function getCompetenciaFormatado(){
		$oData = new DateTime($this->dCompetencia);
		 return $oData->format("d/m/Y");
	}
	public function setCompetenciaBanco($dCompetencia){
		 if($dCompetencia){
			 $oData = DateTime::createFromFormat('d/m/Y', $dCompetencia);
			 $this->dCompetencia = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setTotalItens($nTotalItens){
		$this->nTotalItens = $nTotalItens;
	}
	public function getTotalItens(){
		return $this->nTotalItens;
	}
	public function setParcela($nParcela){
		$this->nParcela = $nParcela;
	}
	public function getParcela(){
		return $this->nParcela;
	}
	public function setVencimento($dVencimento){
		$this->dVencimento = $dVencimento;
	}
	public function getVencimento(){
		return $this->dVencimento;
	}
	public function getVencimentoFormatado(){
		$oData = new DateTime($this->dVencimento);
		 return $oData->format("d/m/Y");
	}
	public function setVencimentoBanco($dVencimento){
		 if($dVencimento){
			 $oData = DateTime::createFromFormat('d/m/Y', $dVencimento);
			 $this->dVencimento = $oData->format('Y-m-d') ;
	}
		 }
	public function setFpgCodigo($nFpgCodigo){
		$this->nFpgCodigo = $nFpgCodigo;
	}
	public function getFpgCodigo(){
		return $this->nFpgCodigo;
	}
	public function setValorPrincipal($nValorPrincipal){
		$this->nValorPrincipal = $nValorPrincipal;
	}
	public function getValorPrincipal(){
		return $this->nValorPrincipal;
	}
	public function getValorPrincipalFormatado(){
		 $vRetorno = number_format($this->nValorPrincipal , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorPrincipalBanco($nValorPrincipal){
		if($nValorPrincipal){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorPrincipal = str_replace($sOrigem, $sDestino, $nValorPrincipal);
	
		}else{
		$this->nValorPrincipal = 'null';
			}
		}
public function setJuros($nJuros){
		$this->nJuros = $nJuros;
	}
	public function getJuros(){
		return $this->nJuros;
	}
	public function getJurosFormatado(){
		 $vRetorno = number_format($this->nJuros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setJurosBanco($nJuros){
		if($nJuros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nJuros = str_replace($sOrigem, $sDestino, $nJuros);
	
		}else{
		$this->nJuros = 'null';
			}
		}
public function setRetencaoDesconto($nRetencaoDesconto){
		$this->nRetencaoDesconto = $nRetencaoDesconto;
	}
	public function getRetencaoDesconto(){
		return $this->nRetencaoDesconto;
	}
	public function getRetencaoDescontoFormatado(){
		 $vRetorno = number_format($this->nRetencaoDesconto , 2, ',', '.');
		 return $vRetorno;
	}
	public function setRetencaoDescontoBanco($nRetencaoDesconto){
		if($nRetencaoDesconto){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nRetencaoDesconto = str_replace($sOrigem, $sDestino, $nRetencaoDesconto);
	
		}else{
		$this->nRetencaoDesconto = 'null';
			}
		}
public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setNumeroDocumento($sNumeroDocumento){
		$this->sNumeroDocumento = $sNumeroDocumento;
	}
	public function getNumeroDocumento(){
		return $this->sNumeroDocumento;
	}
	public function setHistorico($sHistorico){
		$this->sHistorico = $sHistorico;
	}
	public function getHistorico(){
		return $this->sHistorico;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setPessoa($sPessoa){
		$this->sPessoa = $sPessoa;
	}
	public function getPessoa(){
		return $this->sPessoa;
	}
	public function setTipoDoc($sTipoDoc){
		$this->sTipoDoc = $sTipoDoc;
	}
	public function getTipoDoc(){
		return $this->sTipoDoc;
	}
	public function setFormaPagamento($sFormaPagamento){
		$this->sFormaPagamento = $sFormaPagamento;
	}
	public function getFormaPagamento(){
		return $this->sFormaPagamento;
	}
	public function setUnidade($sUnidade){
		$this->sUnidade = $sUnidade;
	}
	public function getUnidade(){
		return $this->sUnidade;
	}
	
 }
 ?>
