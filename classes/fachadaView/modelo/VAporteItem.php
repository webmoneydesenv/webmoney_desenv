<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_aporte_item 
  */
 class VAporteItem{
	/**
	* @campo cod_solicitacao_aporte
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodSolicitacaoAporte;
 	/**
	* @campo data_solicitacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataSolicitacao;
  /**
	* @campo descricao_aporte
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricaoAporte;

	/**
	* @campo unidade
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUnidade;

     /**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovItem;
    /**
	* @campo FA
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFA;


	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo emp_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpFantasia;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovObs;
	/**
	* @campo tipo_documento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipoDocumento;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo parcela
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sParcela;
	/**
	* @campo competencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dCompetencia;
	/**
	* @campo mov_data_vencto
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataVencto;
	/**
	* @campo forma_pagamento
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFormaPagamento;
	/**
	* @campo mov_valor_parcela
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValor;
	/**
	* @campo mov_juros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovJuros;
	/**
	* @campo desconto
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nDesconto;
	/**
	* @campo valor_liquido
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovValorLiquido;

	
 	
 	public function __construct(){
 		
 	}

	public function setCodSolicitacaoAporte($nCodSolicitacaoAporte){
		$this->nCodSolicitacaoAporte = $nCodSolicitacaoAporte;
	}
	public function getCodSolicitacaoAporte(){
		return $this->nCodSolicitacaoAporte;
	}
	
 	
 	public function setDataSolicitacao($dDataSolicitacao){
		$this->dDataSolicitacao = $dDataSolicitacao;
	}
	public function getDataSolicitacao(){
		return $this->dDataSolicitacao;
	}
	public function getDataSolicitacaoFormatado(){
		$oData = new DateTime($this->dDataSolicitacao);
		 return $oData->format("d/m/Y");
	}
	public function setDataSolicitacaoBanco($dDataSolicitacao){
		  if($dDataSolicitacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataSolicitacao);
			 $this->dDataSolicitacao = $oData->format('Y-m-d') ;
	       }
    }

   public function setDescricaoAporte($sDescricaoAporte){
		$this->sDescricaoAporte = $sDescricaoAporte;
	}
	public function getDescricaoAporte(){
		return $this->sDescricaoAporte;
	}

	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}

    public function setEmpFantasia($sEmpFantasia){
		$this->sEmpFantasia = $sEmpFantasia;
	}
	public function getEmpFantasia(){
		return $this->sEmpFantasia;
	}

	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}

 public function setUnidade($nUnidade){
		$this->nUnidade = $nUnidade;
	}
	public function getUnidade(){
		return $this->nUnidade;
	}


	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
    public function setFA($sFA){
		$this->sFA = $sFA;
	}
	public function getFA(){
		return $this->sFA;
	}


	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}

	public function setMovObs($sMovObs){
		$this->sMovObs = $sMovObs;
	}
	public function getMovObs(){
		return $this->sMovObs;
	}
	public function setTipoDocumento($sTipoDocumento){
		$this->sTipoDocumento = $sTipoDocumento;
	}
	public function getTipoDocumento(){
		return $this->sTipoDocumento;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setParcela($sParcela){
		$this->sParcela = $sParcela;
	}
	public function getParcela(){
		return $this->sParcela;
	}
	public function setCompetencia($dCompetencia){
		$this->dCompetencia = $dCompetencia;
	}
	public function getCompetencia(){
		return $this->dCompetencia;
	}
	public function getCompetenciaFormatado(){
		$oData = new DateTime($this->dCompetencia);
		 return $oData->format("m/Y");
	}
	public function setCompetenciaBanco($dCompetencia){
		 if($dCompetencia){
			 $oData = DateTime::createFromFormat('d/m/Y', $dCompetencia);
			 $this->dCompetencia = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovDataVencto($dMovDataVencto){
		$this->dMovDataVencto = $dMovDataVencto;
	}
	public function getMovDataVencto(){
		return $this->dMovDataVencto;
	}
	public function getMovDataVenctoFormatado(){
		$oData = new DateTime($this->dMovDataVencto);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataVenctoBanco($dMovDataVencto){
		 if($dMovDataVencto){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataVencto);
			 $this->dMovDataVencto = $oData->format('Y-m-d') ;
	}
		 }
	public function setFormaPagamento($sFormaPagamento){
		$this->sFormaPagamento = $sFormaPagamento;
	}
	public function getFormaPagamento(){
		return $this->sFormaPagamento;
	}
	public function setMovValor($nMovValor){
		$this->nMovValor = $nMovValor;
	}
	public function getMovValor(){
		return $this->nMovValor;
	}
	public function getMovValorFormatado(){
		 $vRetorno = number_format($this->nMovValor , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorBanco($nMovValor){
		if($nMovValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValor = str_replace($sOrigem, $sDestino, $nMovValor);
	
		}else{
		$this->nMovValor = 'null';
			}
		}
public function setMovJuros($nMovJuros){
		$this->nMovJuros = $nMovJuros;
	}
	public function getMovJuros(){
		return $this->nMovJuros;
	}
	public function getMovJurosFormatado(){
		 $vRetorno = number_format($this->nMovJuros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovJurosBanco($nMovJuros){
		if($nMovJuros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovJuros = str_replace($sOrigem, $sDestino, $nMovJuros);
	
		}else{
		$this->nMovJuros = 'null';
			}
		}

public function setDesconto($nDesconto){
		$this->nDesconto = $nDesconto;
	}
	public function getDesconto(){
		return $this->nDesconto;
	}
	public function getDescontoFormatado(){
		 $vRetorno = number_format($this->nDesconto , 2, ',', '.');
		 return $vRetorno;
	}

     public function setDescontoBanco($nDesconto){
		if($nDesconto){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nDesconto = str_replace($sOrigem, $sDestino, $nDesconto);

		}else{
		$this->nDesconto = 'null';
			}
		}
public function setMovValorLiquido($nMovValorLiquido){
		$this->nMovValorLiquido = $nMovValorLiquido;
	}
	public function getMovValorLiquido(){
		return $this->nMovValorLiquido;
	}
	public function getMovValorLiquidoFormatado(){
		 $vRetorno = number_format($this->nMovValorLiquido , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorLiquidoBanco($nMovValorLiquido){
		if($nMovValorLiquido){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorLiquido = str_replace($sOrigem, $sDestino, $nMovValorLiquido);
	
		}else{
		$this->nMovValorLiquido = 'null';
			}
		}


 }
 ?>
