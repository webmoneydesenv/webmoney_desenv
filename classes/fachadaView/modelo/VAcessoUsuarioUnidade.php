<?php
 /**
  * @author Auto-Generated
  * @package fachadaView
  * @SGBD mysql
  * @tabela v_acesso_usuario_unidade
  */
 class VAcessoUsuarioUnidade{
 	/**
	* @campo cod_usuario
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUsuario;

	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo login
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sLogin;
     /**
	* @campo grupo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nGrupo;

	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;

    /**
	* @campo unidade
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidade;
	/**
	* @campo plano_contas_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPlanoContasCodigo;
	/**
	* @campo codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodigo;


 	public function __construct(){

 	}

 	public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}
	public function getCodUsuario(){
		return $this->nCodUsuario;
	}

	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}

	public function setLogin($sLogin){
		$this->sLogin = $sLogin;
	}
	public function getLogin(){
		return $this->sLogin;
	}

	public function setGrupo($nGrupo){
		$this->nGrupo = $nGrupo;
	}
	public function getGrupo(){
		return $this->nGrupo;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setPlanoContasCodigo($nPlanoContasCodigo){
		$this->nPlanoContasCodigo = $nPlanoContasCodigo;
	}
	public function getPlanoContasCodigo(){
		return $this->nPlanoContasCodigo;
	}

	public function setUnidade($sUnidade){
		$this->sUnidade = $sUnidade;
	}
	public function getUnidade(){
		return $this->sUnidade;
	}

	public function setCodigo($nCodigo){
		$this->nCodigo = $nCodigo;
	}
	public function getCodigo(){
		return $this->nCodigo;
	}

 }
 ?>
