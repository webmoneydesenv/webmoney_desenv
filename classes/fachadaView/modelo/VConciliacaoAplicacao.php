<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_conciliacao_aplicacao 
  */
 class VConciliacaoAplicacao{
 	/**
	* @campo ordem
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sOrdem;
	/**
	* @campo data_conciliacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataConciliacao;
	/**
	* @campo conta
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConta;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setOrdem($sOrdem){
		$this->sOrdem = $sOrdem;
	}
	public function getOrdem(){
		return $this->sOrdem;
	}
	public function setDataConciliacao($dDataConciliacao){
		$this->dDataConciliacao = $dDataConciliacao;
	}
	public function getDataConciliacao(){
		return $this->dDataConciliacao;
	}
	public function getDataConciliacaoFormatado(){
		$oData = new DateTime($this->dDataConciliacao);		 return $oData->format("d/m/Y");
	}
	public function setDataConciliacaoBanco($dDataConciliacao){
		 if($dDataConciliacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataConciliacao);
			 $this->dDataConciliacao = $oData->format('Y-m-d') ;
	}
		 }
	public function setConta($nConta){
		$this->nConta = $nConta;
	}
	public function getConta(){
		return $this->nConta;
	}
	
 }
 ?>
