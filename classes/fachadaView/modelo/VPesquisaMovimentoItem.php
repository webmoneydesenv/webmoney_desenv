<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_pesquisa_movimento_item 
  */
 class VPesquisaMovimentoItem{
 	/**
	* @campo competencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCompetencia;
	/**
	* @campo emp_razao_social
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpRazaoSocial;
	/**
	* @campo emp_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpFantasia;
	/**
	* @campo emp_cnpj
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpCnpj;
	/**
	* @campo emp_imagem
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpImagem;
	/**
	* @campo mov_data_emissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataEmissao;
	/**
	* @campo tip_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipNome;
	/**
	* @campo pesg_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEmail;
	/**
	* @campo pes_fones
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesFones;
	/**
	* @campo tipocon_cod
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTipoconCod;
	/**
	* @campo pesg_bco_agencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgBcoAgencia;
	/**
	* @campo pesg_bco_conta
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgBcoConta;
	/**
	* @campo bco_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBcoNome;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo identificacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIdentificacao;
	/**
	* @campo cus_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCusCodigo;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo pes_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesCodigo;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovObs;
	/**
	* @campo mov_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovInc;
	/**
	* @campo mov_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovAlt;
	/**
	* @campo mov_contrato
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovContrato;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo mov_parcelas
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovParcelas;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo mov_icms_aliq
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovIcmsAliq;
	/**
	* @campo mov_valor_glob
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorGlob;
	/**
	* @campo mov_pis
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovPis;
	/**
	* @campo mov_confins
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovConfins;
	/**
	* @campo mov_csll
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCsll;
	/**
	* @campo mov_iss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIss;
	/**
	* @campo mov_ir
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIr;
	/**
	* @campo mov_irrf
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIrrf;
	/**
	* @campo mov_inss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovInss;
	/**
	* @campo mov_outros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutros;
	/**
	* @campo mov_devolucao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovDevolucao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo mov_outros_desc
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutrosDesc;
	/**
	* @campo com_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nComCodigo;
	/**
	* @campo custo_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCustoCodigo;
	/**
	* @campo custo_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCustoDescricao;
	/**
	* @campo negocio_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNegocioCodigo;
	/**
	* @campo negocio_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNegocioDescricao;
	/**
	* @campo centro_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCentroCodigo;
	/**
	* @campo centro_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCentroDescricao;
	/**
	* @campo unidade_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUnidadeCodigo;
	/**
	* @campo unidade_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUnidadeDescricao;
	/**
	* @campo aceite_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAceiteCodigo;
	/**
	* @campo aceite_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAceiteDescricao;
	/**
	* @campo setor_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSetorCodigo;
	/**
	* @campo setor_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSetorDescricao;
	/**
	* @campo tipo_documento_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipoDocumentoCodigo;
	/**
	* @campo tipo_documento_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipoDocumentoDescricao;
	/**
	* @campo tipo_movimento_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipoMovimentoCodigo;
	/**
	* @campo tipo_movimento_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipoMovimentoDescricao;
	/**
	* @campo conta_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sContaCodigo;
	/**
	* @campo conta_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sContaDescricao;
	/**
	* @campo forma_pagamento_codigo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFormaPagamentoCodigo;
	/**
	* @campo forma_pagamento_descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFormaPagamentoDescricao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo mov_data_vencto
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataVencto;
	/**
	* @campo mov_data_prev
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dMovDataPrev;
	/**
	* @campo mov_valor
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValor;
	/**
	* @campo mov_juros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovJuros;
	/**
	* @campo mov_valor_pagar
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovValorPagar;
	/**
	* @campo fpg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFpgCodigo;
	/**
	* @campo tip_doc_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipDocCodigo;
	/**
	* @campo mov_retencao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovRetencao;
	/**
	* @campo mov_data_inclusao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataInclusao;
	/**
	* @campo mov_valor_pago
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovValorPago;
	/**
	* @campo tip_ace_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipAceCodigo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCompetencia($sCompetencia){
		$this->sCompetencia = $sCompetencia;
	}
	public function getCompetencia(){
		return $this->sCompetencia;
	}
	public function getCompetenciaFormatado(){
		 $oData = new DateTime($this->sCompetencia);
		 return $oData->format("m/Y");
	}
	public function setEmpRazaoSocial($sEmpRazaoSocial){
		$this->sEmpRazaoSocial = $sEmpRazaoSocial;
	}
	public function getEmpRazaoSocial(){
		return $this->sEmpRazaoSocial;
	}
	public function setEmpFantasia($sEmpFantasia){
		$this->sEmpFantasia = $sEmpFantasia;
	}
	public function getEmpFantasia(){
		return $this->sEmpFantasia;
	}
	public function setEmpCnpj($sEmpCnpj){
		$this->sEmpCnpj = $sEmpCnpj;
	}
	public function getEmpCnpj(){
		return $this->sEmpCnpj;
	}
	public function setEmpImagem($sEmpImagem){
		$this->sEmpImagem = $sEmpImagem;
	}
	public function getEmpImagem(){
		return $this->sEmpImagem;
	}
	public function setMovDataEmissao($dMovDataEmissao){
		$this->dMovDataEmissao = $dMovDataEmissao;
	}
	public function getMovDataEmissao(){
		return $this->dMovDataEmissao;
	}
	public function getMovDataEmissaoFormatado(){
		$oData = new DateTime($this->dMovDataEmissao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataEmissaoBanco($dMovDataEmissao){
		 if($dMovDataEmissao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataEmissao);
			 $this->dMovDataEmissao = $oData->format('Y-m-d') ;
	}
		 }
	public function setTipNome($sTipNome){
		$this->sTipNome = $sTipNome;
	}
	public function getTipNome(){
		return $this->sTipNome;
	}
	public function setPesgEmail($sPesgEmail){
		$this->sPesgEmail = $sPesgEmail;
	}
	public function getPesgEmail(){
		return $this->sPesgEmail;
	}
	public function setPesFones($sPesFones){
		$this->sPesFones = $sPesFones;
	}
	public function getPesFones(){
		return $this->sPesFones;
	}
	public function setTipoconCod($nTipoconCod){
		$this->nTipoconCod = $nTipoconCod;
	}
	public function getTipoconCod(){
		return $this->nTipoconCod;
	}
	public function setPesgBcoAgencia($sPesgBcoAgencia){
		$this->sPesgBcoAgencia = $sPesgBcoAgencia;
	}
	public function getPesgBcoAgencia(){
		return $this->sPesgBcoAgencia;
	}
	public function setPesgBcoConta($sPesgBcoConta){
		$this->sPesgBcoConta = $sPesgBcoConta;
	}
	public function getPesgBcoConta(){
		return $this->sPesgBcoConta;
	}
	public function setBcoNome($sBcoNome){
		$this->sBcoNome = $sBcoNome;
	}
	public function getBcoNome(){
		return $this->sBcoNome;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setIdentificacao($sIdentificacao){
		$this->sIdentificacao = $sIdentificacao;
	}
	public function getIdentificacao(){
		return $this->sIdentificacao;
	}
	public function setCusCodigo($nCusCodigo){
		$this->nCusCodigo = $nCusCodigo;
	}
	public function getCusCodigo(){
		return $this->nCusCodigo;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setCenCodigo($nCenCodigo){
		$this->nCenCodigo = $nCenCodigo;
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setPesCodigo($nPesCodigo){
		$this->nPesCodigo = $nPesCodigo;
	}
	public function getPesCodigo(){
		return $this->nPesCodigo;
	}
	public function setConCodigo($nConCodigo){
		$this->nConCodigo = $nConCodigo;
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}
	public function setMovObs($sMovObs){
		$this->sMovObs = $sMovObs;
	}
	public function getMovObs(){
		return $this->sMovObs;
	}
	public function setMovInc($sMovInc){
		$this->sMovInc = $sMovInc;
	}
	public function getMovInc(){
		return $this->sMovInc;
	}
	public function setMovAlt($sMovAlt){
		$this->sMovAlt = $sMovAlt;
	}
	public function getMovAlt(){
		return $this->sMovAlt;
	}
	public function setMovContrato($nMovContrato){
		$this->nMovContrato = $nMovContrato;
	}
	public function getMovContrato(){
		return $this->nMovContrato;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setMovParcelas($nMovParcelas){
		$this->nMovParcelas = $nMovParcelas;
	}
	public function getMovParcelas(){
		return $this->nMovParcelas;
	}
	public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setMovIcmsAliq($nMovIcmsAliq){
		$this->nMovIcmsAliq = $nMovIcmsAliq;
	}
	public function getMovIcmsAliq(){
		return $this->nMovIcmsAliq;
	}
	public function getMovIcmsAliqFormatado(){
		 $vRetorno = number_format($this->nMovIcmsAliq , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIcmsAliqBanco($nMovIcmsAliq){
		if($nMovIcmsAliq){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIcmsAliq = str_replace($sOrigem, $sDestino, $nMovIcmsAliq);
	
		}else{
		$this->nMovIcmsAliq = 'null';
			}
		}
public function setMovValorGlob($nMovValorGlob){
		$this->nMovValorGlob = $nMovValorGlob;
	}
	public function getMovValorGlob(){
		return $this->nMovValorGlob;
	}
	public function getMovValorGlobFormatado(){
		 $vRetorno = number_format($this->nMovValorGlob , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorGlobBanco($nMovValorGlob){
		if($nMovValorGlob){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorGlob = str_replace($sOrigem, $sDestino, $nMovValorGlob);
	
		}else{
		$this->nMovValorGlob = 'null';
			}
		}
public function setMovPis($nMovPis){
		$this->nMovPis = $nMovPis;
	}
	public function getMovPis(){
		return $this->nMovPis;
	}
	public function getMovPisFormatado(){
		 $vRetorno = number_format($this->nMovPis , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovPisBanco($nMovPis){
		if($nMovPis){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovPis = str_replace($sOrigem, $sDestino, $nMovPis);
	
		}else{
		$this->nMovPis = 'null';
			}
		}
public function setMovConfins($nMovConfins){
		$this->nMovConfins = $nMovConfins;
	}
	public function getMovConfins(){
		return $this->nMovConfins;
	}
	public function getMovConfinsFormatado(){
		 $vRetorno = number_format($this->nMovConfins , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovConfinsBanco($nMovConfins){
		if($nMovConfins){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovConfins = str_replace($sOrigem, $sDestino, $nMovConfins);
	
		}else{
		$this->nMovConfins = 'null';
			}
		}
public function setMovCsll($nMovCsll){
		$this->nMovCsll = $nMovCsll;
	}
	public function getMovCsll(){
		return $this->nMovCsll;
	}
	public function getMovCsllFormatado(){
		 $vRetorno = number_format($this->nMovCsll , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovCsllBanco($nMovCsll){
		if($nMovCsll){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovCsll = str_replace($sOrigem, $sDestino, $nMovCsll);
	
		}else{
		$this->nMovCsll = 'null';
			}
		}
public function setMovIss($nMovIss){
		$this->nMovIss = $nMovIss;
	}
	public function getMovIss(){
		return $this->nMovIss;
	}
	public function getMovIssFormatado(){
		 $vRetorno = number_format($this->nMovIss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIssBanco($nMovIss){
		if($nMovIss){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIss = str_replace($sOrigem, $sDestino, $nMovIss);
	
		}else{
		$this->nMovIss = 'null';
			}
		}
public function setMovIr($nMovIr){
		$this->nMovIr = $nMovIr;
	}
	public function getMovIr(){
		return $this->nMovIr;
	}
	public function getMovIrFormatado(){
		 $vRetorno = number_format($this->nMovIr , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrBanco($nMovIr){
		if($nMovIr){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIr = str_replace($sOrigem, $sDestino, $nMovIr);
	
		}else{
		$this->nMovIr = 'null';
			}
		}
public function setMovIrrf($nMovIrrf){
		$this->nMovIrrf = $nMovIrrf;
	}
	public function getMovIrrf(){
		return $this->nMovIrrf;
	}
	public function getMovIrrfFormatado(){
		 $vRetorno = number_format($this->nMovIrrf , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrrfBanco($nMovIrrf){
		if($nMovIrrf){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIrrf = str_replace($sOrigem, $sDestino, $nMovIrrf);
	
		}else{
		$this->nMovIrrf = 'null';
			}
		}
public function setMovInss($nMovInss){
		$this->nMovInss = $nMovInss;
	}
	public function getMovInss(){
		return $this->nMovInss;
	}
	public function getMovInssFormatado(){
		 $vRetorno = number_format($this->nMovInss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovInssBanco($nMovInss){
		if($nMovInss){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovInss = str_replace($sOrigem, $sDestino, $nMovInss);
	
		}else{
		$this->nMovInss = 'null';
			}
		}
public function setMovOutros($nMovOutros){
		$this->nMovOutros = $nMovOutros;
	}
	public function getMovOutros(){
		return $this->nMovOutros;
	}
	public function getMovOutrosFormatado(){
		 $vRetorno = number_format($this->nMovOutros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosBanco($nMovOutros){
		if($nMovOutros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovOutros = str_replace($sOrigem, $sDestino, $nMovOutros);
	
		}else{
		$this->nMovOutros = 'null';
			}
		}
public function setMovDevolucao($nMovDevolucao){
		$this->nMovDevolucao = $nMovDevolucao;
	}
	public function getMovDevolucao(){
		return $this->nMovDevolucao;
	}
	public function getMovDevolucaoFormatado(){
		 $vRetorno = number_format($this->nMovDevolucao , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovDevolucaoBanco($nMovDevolucao){
		if($nMovDevolucao){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovDevolucao = str_replace($sOrigem, $sDestino, $nMovDevolucao);
	
		}else{
		$this->nMovDevolucao = 'null';
			}
		}
public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMovOutrosDesc($nMovOutrosDesc){
		$this->nMovOutrosDesc = $nMovOutrosDesc;
	}
	public function getMovOutrosDesc(){
		return $this->nMovOutrosDesc;
	}
	public function getMovOutrosDescFormatado(){
		 $vRetorno = number_format($this->nMovOutrosDesc , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosDescBanco($nMovOutrosDesc){
		if($nMovOutrosDesc){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovOutrosDesc = str_replace($sOrigem, $sDestino, $nMovOutrosDesc);
	
		}else{
		$this->nMovOutrosDesc = 'null';
			}
		}
public function setComCodigo($nComCodigo){
		$this->nComCodigo = $nComCodigo;
	}
	public function getComCodigo(){
		return $this->nComCodigo;
	}
	public function setCustoCodigo($sCustoCodigo){
		$this->sCustoCodigo = $sCustoCodigo;
	}
	public function getCustoCodigo(){
		return $this->sCustoCodigo;
	}
	public function setCustoDescricao($sCustoDescricao){
		$this->sCustoDescricao = $sCustoDescricao;
	}
	public function getCustoDescricao(){
		return $this->sCustoDescricao;
	}
	public function setNegocioCodigo($sNegocioCodigo){
		$this->sNegocioCodigo = $sNegocioCodigo;
	}
	public function getNegocioCodigo(){
		return $this->sNegocioCodigo;
	}
	public function setNegocioDescricao($sNegocioDescricao){
		$this->sNegocioDescricao = $sNegocioDescricao;
	}
	public function getNegocioDescricao(){
		return $this->sNegocioDescricao;
	}
	public function setCentroCodigo($sCentroCodigo){
		$this->sCentroCodigo = $sCentroCodigo;
	}
	public function getCentroCodigo(){
		return $this->sCentroCodigo;
	}
	public function setCentroDescricao($sCentroDescricao){
		$this->sCentroDescricao = $sCentroDescricao;
	}
	public function getCentroDescricao(){
		return $this->sCentroDescricao;
	}
	public function setUnidadeCodigo($sUnidadeCodigo){
		$this->sUnidadeCodigo = $sUnidadeCodigo;
	}
	public function getUnidadeCodigo(){
		return $this->sUnidadeCodigo;
	}
	public function setUnidadeDescricao($sUnidadeDescricao){
		$this->sUnidadeDescricao = $sUnidadeDescricao;
	}
	public function getUnidadeDescricao(){
		return $this->sUnidadeDescricao;
	}
	public function setAceiteCodigo($sAceiteCodigo){
		$this->sAceiteCodigo = $sAceiteCodigo;
	}
	public function getAceiteCodigo(){
		return $this->sAceiteCodigo;
	}
	public function setAceiteDescricao($sAceiteDescricao){
		$this->sAceiteDescricao = $sAceiteDescricao;
	}
	public function getAceiteDescricao(){
		return $this->sAceiteDescricao;
	}
	public function setSetorCodigo($sSetorCodigo){
		$this->sSetorCodigo = $sSetorCodigo;
	}
	public function getSetorCodigo(){
		return $this->sSetorCodigo;
	}
	public function setSetorDescricao($sSetorDescricao){
		$this->sSetorDescricao = $sSetorDescricao;
	}
	public function getSetorDescricao(){
		return $this->sSetorDescricao;
	}
	public function setTipoDocumentoCodigo($sTipoDocumentoCodigo){
		$this->sTipoDocumentoCodigo = $sTipoDocumentoCodigo;
	}
	public function getTipoDocumentoCodigo(){
		return $this->sTipoDocumentoCodigo;
	}
	public function setTipoDocumentoDescricao($sTipoDocumentoDescricao){
		$this->sTipoDocumentoDescricao = $sTipoDocumentoDescricao;
	}
	public function getTipoDocumentoDescricao(){
		return $this->sTipoDocumentoDescricao;
	}
	public function setTipoMovimentoCodigo($sTipoMovimentoCodigo){
		$this->sTipoMovimentoCodigo = $sTipoMovimentoCodigo;
	}
	public function getTipoMovimentoCodigo(){
		return $this->sTipoMovimentoCodigo;
	}
	public function setTipoMovimentoDescricao($sTipoMovimentoDescricao){
		$this->sTipoMovimentoDescricao = $sTipoMovimentoDescricao;
	}
	public function getTipoMovimentoDescricao(){
		return $this->sTipoMovimentoDescricao;
	}
	public function setContaCodigo($sContaCodigo){
		$this->sContaCodigo = $sContaCodigo;
	}
	public function getContaCodigo(){
		return $this->sContaCodigo;
	}
	public function setContaDescricao($sContaDescricao){
		$this->sContaDescricao = $sContaDescricao;
	}
	public function getContaDescricao(){
		return $this->sContaDescricao;
	}
	public function setFormaPagamentoCodigo($sFormaPagamentoCodigo){
		$this->sFormaPagamentoCodigo = $sFormaPagamentoCodigo;
	}
	public function getFormaPagamentoCodigo(){
		return $this->sFormaPagamentoCodigo;
	}
	public function setFormaPagamentoDescricao($sFormaPagamentoDescricao){
		$this->sFormaPagamentoDescricao = $sFormaPagamentoDescricao;
	}
	public function getFormaPagamentoDescricao(){
		return $this->sFormaPagamentoDescricao;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setMovDataVencto($dMovDataVencto){
		$this->dMovDataVencto = $dMovDataVencto;
	}
	public function getMovDataVencto(){
		return $this->dMovDataVencto;
	}
	public function getMovDataVenctoFormatado(){
		$oData = new DateTime($this->dMovDataVencto);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataVenctoBanco($dMovDataVencto){
		 if($dMovDataVencto){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataVencto);
			 $this->dMovDataVencto = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovDataPrev($dMovDataPrev){
		$this->dMovDataPrev = $dMovDataPrev;
	}
	public function getMovDataPrev(){
		return $this->dMovDataPrev;
	}
	public function getMovDataPrevFormatado(){
		$oData = new DateTime($this->dMovDataPrev);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataPrevBanco($dMovDataPrev){
		 if($dMovDataPrev){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataPrev);
			 $this->dMovDataPrev = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovValor($nMovValor){
		$this->nMovValor = $nMovValor;
	}
	public function getMovValor(){
		return $this->nMovValor;
	}
	public function getMovValorFormatado(){
		 $vRetorno = number_format($this->nMovValor , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorBanco($nMovValor){
		if($nMovValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValor = str_replace($sOrigem, $sDestino, $nMovValor);
	
		}else{
		$this->nMovValor = 'null';
			}
		}
public function setMovJuros($nMovJuros){
		$this->nMovJuros = $nMovJuros;
	}
	public function getMovJuros(){
		return $this->nMovJuros;
	}
	public function getMovJurosFormatado(){
		 $vRetorno = number_format($this->nMovJuros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovJurosBanco($nMovJuros){
		if($nMovJuros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovJuros = str_replace($sOrigem, $sDestino, $nMovJuros);
	
		}else{
		$this->nMovJuros = 'null';
			}
		}
public function setMovValorPagar($nMovValorPagar){
		$this->nMovValorPagar = $nMovValorPagar;
	}
	public function getMovValorPagar(){
		return $this->nMovValorPagar;
	}
	public function getMovValorPagarFormatado(){
		 $vRetorno = number_format($this->nMovValorPagar , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorPagarBanco($nMovValorPagar){
		if($nMovValorPagar){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPagar = str_replace($sOrigem, $sDestino, $nMovValorPagar);
	
		}else{
		$this->nMovValorPagar = 'null';
			}
		}
public function setFpgCodigo($nFpgCodigo){
		$this->nFpgCodigo = $nFpgCodigo;
	}
	public function getFpgCodigo(){
		return $this->nFpgCodigo;
	}
	public function setTipDocCodigo($nTipDocCodigo){
		$this->nTipDocCodigo = $nTipDocCodigo;
	}
	public function getTipDocCodigo(){
		return $this->nTipDocCodigo;
	}
	public function setMovRetencao($nMovRetencao){
		$this->nMovRetencao = $nMovRetencao;
	}
	public function getMovRetencao(){
		return $this->nMovRetencao;
	}
	public function getMovRetencaoFormatado(){
		 $vRetorno = number_format($this->nMovRetencao , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovRetencaoBanco($nMovRetencao){
		if($nMovRetencao){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovRetencao = str_replace($sOrigem, $sDestino, $nMovRetencao);
	
		}else{
		$this->nMovRetencao = 'null';
			}
		}
public function setMovDataInclusao($dMovDataInclusao){
		$this->dMovDataInclusao = $dMovDataInclusao;
	}
	public function getMovDataInclusao(){
		return $this->dMovDataInclusao;
	}
	public function getMovDataInclusaoFormatado(){
		$oData = new DateTime($this->dMovDataInclusao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataInclusaoBanco($dMovDataInclusao){
		 if($dMovDataInclusao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataInclusao);
			 $this->dMovDataInclusao = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovValorPago($nMovValorPago){
		$this->nMovValorPago = $nMovValorPago;
	}
	public function getMovValorPago(){
		return $this->nMovValorPago;
	}
	public function getMovValorPagoFormatado(){
		 $vRetorno = number_format($this->nMovValorPago , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorPagoBanco($nMovValorPago){
		if($nMovValorPago){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPago = str_replace($sOrigem, $sDestino, $nMovValorPago);
	
		}else{
		$this->nMovValorPago = 'null';
			}
		}
public function setTipAceCodigo($nTipAceCodigo){
		$this->nTipAceCodigo = $nTipAceCodigo;
	}
	public function getTipAceCodigo(){
		return $this->nTipAceCodigo;
	}
	
 }
 ?>
