<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_pesquisa 
  */
 class VPesquisa{
 	/**
	* @campo mov_data_vencto
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataVencto;
	/**
	* @campo tipo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTipo;
	/**
	* @campo mov_contrato
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovContrato;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo competencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dCompetencia;
	/**
	* @campo mov_valor_parcela
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorParcela;
     /**
	* @campo finalizado
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFinalizado;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo tip_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipNome;
	/**
	* @campo pesg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesgCodigo;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo identificacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIdentificacao;
	/**
	* @campo mov_data_inclusao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataInclusao;
	/**
	* @campo mov_data_prev
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dMovDataPrev;
	/**
	* @campo mov_valor_pago
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovValorPago;
	/**
	* @campo mov_data_emissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataEmissao;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpCodigo;	
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovObs;	
	/**
	* @campo mov_documento
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
     /**
	* @campo cod_transferencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTransferencia;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setMovDataVencto($dMovDataVencto){
		$this->dMovDataVencto = $dMovDataVencto;
	}
	public function getMovDataVencto(){
		return $this->dMovDataVencto;
	}
	public function getMovDataVenctoFormatado(){
		$oData = new DateTime($this->dMovDataVencto);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataVenctoBanco($dMovDataVencto){
		 if($dMovDataVencto){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataVencto);
			 $this->dMovDataVencto = $oData->format('Y-m-d') ;
	}
		 }
	public function setTipo($sTipo){
		$this->sTipo = $sTipo;
	}
	public function getTipo(){
		return $this->sTipo;
	}
	public function setMovContrato($sMovContrato){
		$this->sMovContrato = $sMovContrato;
	}
	public function getMovContrato(){
		return $this->sMovContrato;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setCompetencia($dCompetencia){
		$this->dCompetencia = $dCompetencia;
	}
	public function getCompetencia(){
		return $this->dCompetencia;
	}

	public function setCompetenciaBanco($dCompetencia){
		 if($dCompetencia){
			 $oData = DateTime::createFromFormat('d/m/Y', $dCompetencia);
			 $this->dCompetencia = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovValorParcela($nMovValorParcela){
		$this->nMovValorParcela = $nMovValorParcela;
	}
	public function getMovValorParcela(){
		return $this->nMovValorParcela;
	}
	public function getMovValorParcelaFormatado(){
		 $vRetorno = number_format($this->nMovValorParcela , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorParcelaBanco($nMovValorParcela){
		if($nMovValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorParcela = str_replace($sOrigem, $sDestino, $nMovValor);
	
		}else{
		$this->nMovValorParcela = 'null';
			}
		}
    public function setFinalizado($nFinalizado){
        $this->nFinalizado = $nFinalizado;
    }
    public function getFinalizado(){
        return $this->nFinalizado;
    }
    public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setTipNome($sTipNome){
		$this->sTipNome = $sTipNome;
	}
	public function getTipNome(){
		return $this->sTipNome;
	}
	public function setPesgCodigo($nPesgCodigo){
		$this->nPesgCodigo = $nPesgCodigo;
	}
	public function getPesgCodigo(){
		return $this->nPesgCodigo;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setIdentificacao($sIdentificacao){
		$this->sIdentificacao = $sIdentificacao;
	}
	public function getIdentificacao(){
		return $this->sIdentificacao;
	}
	public function setMovDataInclusao($dMovDataInclusao){
		$this->dMovDataInclusao = $dMovDataInclusao;
	}
	public function getMovDataInclusao(){
		return $this->dMovDataInclusao;
	}
	public function getMovDataInclusaoFormatado(){
		$oData = new DateTime($this->dMovDataInclusao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataInclusaoBanco($dMovDataInclusao){
		 if($dMovDataInclusao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataInclusao);
			 $this->dMovDataInclusao = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovDataPrev($dMovDataPrev){
		$this->dMovDataPrev = $dMovDataPrev;
	}
	public function getMovDataPrev(){
		return $this->dMovDataPrev;
	}
	public function getMovDataPrevFormatado(){
		$oData = new DateTime($this->dMovDataPrev);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataPrevBanco($dMovDataPrev){
		 if($dMovDataPrev){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataPrev);
			 $this->dMovDataPrev = $oData->format('Y-m-d') ;
	}
		 }
	public function setMovValorPago($nMovValorPago){
		$this->nMovValorPago = $nMovValorPago;
	}
	public function getMovValorPago(){
		return $this->nMovValorPago;
	}
	public function getMovValorPagoFormatado(){
		 $vRetorno = number_format($this->nMovValorPago , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorPagoBanco($nMovValorPago){
		if($nMovValorPago){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPago = str_replace($sOrigem, $sDestino, $nMovValorPago);
	
		}else{
		$this->nMovValorPago = 'null';
			}
		}
public function setMovDataEmissao($dMovDataEmissao){
		$this->dMovDataEmissao = $dMovDataEmissao;
	}
	public function getMovDataEmissao(){
		return $this->dMovDataEmissao;
	}
	public function getMovDataEmissaoFormatado(){
		$oData = new DateTime($this->dMovDataEmissao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataEmissaoBanco($dMovDataEmissao){
		 if($dMovDataEmissao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataEmissao);
			 $this->dMovDataEmissao = $oData->format('Y-m-d') ;
	}
		 }
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}		 
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	
    public function getCodTransferencia(){
		return $this->nCodTransferencia;
	}
	public function setCodTransferencia($nCodTransferencia){
		$this->nCodTransferencia = $nCodTransferencia;
	}

	public function getMovObs(){
		return $this->sMovObs;
	}	public function setMovObs($sMovObs){
		$this->sMovObs = $sMovObs;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	
 }
 ?>
