<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela v_pessoa_formatada 
  */
 class VPessoaFormatada{
 	/**
	* @campo pesg_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nPesgCodigo;
	/**
	* @campo pesg_inc
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgInc;
	/**
	* @campo pesg_alt
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgAlt;
	/**
	* @campo tip_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nTipCodigo;
	/**
	* @campo cod_status
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodStatus;
	/**
	* @campo pes_fones
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesFones;
	private $oMnyTipoPessoa;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPesgCodigo($nPesgCodigo){
		$this->nPesgCodigo = $nPesgCodigo;
	}
	public function getPesgCodigo(){
		return $this->nPesgCodigo;
	}
	public function setPesgInc($sPesgInc){
		$this->sPesgInc = $sPesgInc;
	}
	public function getPesgInc(){
		return $this->sPesgInc;
	}
	public function setPesgAlt($sPesgAlt){
		$this->sPesgAlt = $sPesgAlt;
	}
	public function getPesgAlt(){
		return $this->sPesgAlt;
	}
	public function setTipCodigo($nTipCodigo){
		$this->nTipCodigo = $nTipCodigo;
	}
	public function getTipCodigo(){
		return $this->nTipCodigo;
	}
	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setPesFones($sPesFones){
		$this->sPesFones = $sPesFones;
	}
	public function getPesFones(){
		return $this->sPesFones;
	}
	
	public function setMnyTipoPessoa($oMnyTipoPessoa){
		$this->oMnyTipoPessoa = $oMnyTipoPessoa;
	}
	public function getMnyTipoPessoa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyTipoPessoa = $oFachada->recuperarUmMnyTipoPessoa($this->getTipCodigo());
		return $this->oMnyTipoPessoa;
	}
	
	
 }
 ?>
