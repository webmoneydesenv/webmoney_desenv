<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_movimento_contrato 
  */
 class VMovimentoContrato{
 	/**
	* @campo emp_razao_social
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpRazaoSocial;
	/**
	* @campo emp_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpFantasia;
	/**
	* @campo emp_cnpj
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpCnpj;
	/**
	* @campo emp_imagem
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpImagem;
	/**
	* @campo tip_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipNome;
	/**
	* @campo pesg_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEmail;
	/**
	* @campo pes_fones
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesFones;
	/**
	* @campo tipocon_cod
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTipoconCod;
	/**
	* @campo pesg_bco_agencia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgBcoAgencia;
	/**
	* @campo pesg_bco_conta
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgBcoConta;
	/**
	* @campo bco_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBcoNome;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo identificacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sIdentificacao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_data_emissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataEmissao;
	/**
	* @campo mov_data_inclusao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataInclusao;
	/**
	* @campo cus_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCusCodigo;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo pes_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesCodigo;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovObs;
	/**
	* @campo mov_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovInc;
	/**
	* @campo mov_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovAlt;
	/**
	* @campo mov_contrato
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovContrato;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo mov_parcelas
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovParcelas;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo mov_icms_aliq
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovIcmsAliq;
	/**
	* @campo valor_contrato
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValorContrato;
	/**
	* @campo mov_pis
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovPis;
	/**
	* @campo mov_confins
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovConfins;
	/**
	* @campo mov_csll
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCsll;
	/**
	* @campo mov_iss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIss;
	/**
	* @campo mov_ir
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIr;
	/**
	* @campo mov_irrf
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIrrf;
	/**
	* @campo mov_inss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovInss;
	/**
	* @campo mov_outros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutros;
	/**
	* @campo mov_devolucao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovDevolucao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo mov_outros_desc
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutrosDesc;
	/**
	* @campo custo_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCustoCodigo;
	/**
	* @campo custo_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCustoDescricao;
	/**
	* @campo negocio_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNegocioCodigo;
	/**
	* @campo negocio_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNegocioDescricao;
	/**
	* @campo centro_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCentroCodigo;
	/**
	* @campo centro_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCentroDescricao;
	/**
	* @campo unidade_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidadeCodigo;
	/**
	* @campo unidade_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUnidadeDescricao;
	/**
	* @campo setor_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sSetorCodigo;
	/**
	* @campo setor_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sSetorDescricao;
	/**
	* @campo tipo_movimento_codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipoMovimentoCodigo;
	/**
	* @campo tipo_movimento_descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipoMovimentoDescricao;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setEmpRazaoSocial($sEmpRazaoSocial){
		$this->sEmpRazaoSocial = $sEmpRazaoSocial;
	}
	public function getEmpRazaoSocial(){
		return $this->sEmpRazaoSocial;
	}
	public function setEmpFantasia($sEmpFantasia){
		$this->sEmpFantasia = $sEmpFantasia;
	}
	public function getEmpFantasia(){
		return $this->sEmpFantasia;
	}
	public function setEmpCnpj($sEmpCnpj){
		$this->sEmpCnpj = $sEmpCnpj;
	}
	public function getEmpCnpj(){
		return $this->sEmpCnpj;

	}
	public function setEmpImagem($sEmpImagem){
		$this->sEmpImagem = $sEmpImagem;
	}
	public function getEmpImagem(){
		return $this->sEmpImagem;
	}
	public function setTipNome($sTipNome){
		$this->sTipNome = $sTipNome;
	}
	public function getTipNome(){
		return $this->sTipNome;
	}
	public function setPesgEmail($sPesgEmail){
		$this->sPesgEmail = $sPesgEmail;
	}
	public function getPesgEmail(){
		return $this->sPesgEmail;
	}
	public function setPesFones($sPesFones){
		$this->sPesFones = $sPesFones;
	}
	public function getPesFones(){
		return $this->sPesFones;
	}
	public function setTipoconCod($nTipoconCod){
		$this->nTipoconCod = $nTipoconCod;
	}
	public function getTipoconCod(){
		return $this->nTipoconCod;
	}
	public function setPesgBcoAgencia($sPesgBcoAgencia){
		$this->sPesgBcoAgencia = $sPesgBcoAgencia;
	}
	public function getPesgBcoAgencia(){
		return $this->sPesgBcoAgencia;
	}
	public function setPesgBcoConta($sPesgBcoConta){
		$this->sPesgBcoConta = $sPesgBcoConta;
	}
	public function getPesgBcoConta(){
		return $this->sPesgBcoConta;
	}
	public function setBcoNome($sBcoNome){
		$this->sBcoNome = $sBcoNome;
	}
	public function getBcoNome(){
		return $this->sBcoNome;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setIdentificacao($sIdentificacao){
		$this->sIdentificacao = $sIdentificacao;
	}
	public function getIdentificacao(){
		return $this->sIdentificacao;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovDataEmissao($dMovDataEmissao){
		$this->dMovDataEmissao = $dMovDataEmissao;
	}
	public function getMovDataEmissao(){
		return $this->dMovDataEmissao;
	}
	public function getMovDataEmissaoFormatado(){
		$oData = new DateTime($this->dMovDataEmissao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataEmissaoBanco($dMovDataEmissao){
		 if($dMovDataEmissao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataEmissao);
			 $this->dMovDataEmissao = $oData->format('Y-m-d') ;
	}
		 }
		 
	public function setMovDataInclusao($dMovDataInclusao){
		$this->dMovDataInclusao = $dMovDataInclusao;
	}
	public function getMovDataInclusao(){
		return $this->dMovDataInclusao;
	}
	public function getMovDataInclusaoFormatado(){
		$oData = new DateTime($this->dMovDataInclusao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataInclusaoBanco($dMovDataInclusao){
		 if($dMovDataInclusao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataInclusao);
			 $this->dMovDataInclusao = $oData->format('Y-m-d') ;
	}
		 }
		 
	public function setCusCodigo($nCusCodigo){
		$this->nCusCodigo = $nCusCodigo;
	}
	public function getCusCodigo(){
		return $this->nCusCodigo;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setCenCodigo($nCenCodigo){
		$this->nCenCodigo = $nCenCodigo;
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setPesCodigo($nPesCodigo){
		$this->nPesCodigo = $nPesCodigo;
	}
	public function getPesCodigo(){
		return $this->nPesCodigo;
	}
	public function setConCodigo($nConCodigo){
		$this->nConCodigo = $nConCodigo;
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}
	public function setMovObs($sMovObs){
		$this->sMovObs = $sMovObs;
	}
	public function getMovObs(){
		return $this->sMovObs;
	}
	public function setMovInc($sMovInc){
		$this->sMovInc = $sMovInc;
	}
	public function getMovInc(){
		return $this->sMovInc;
	}
	public function setMovAlt($sMovAlt){
		$this->sMovAlt = $sMovAlt;
	}
	public function getMovAlt(){
		return $this->sMovAlt;
	}
	public function setMovContrato($nMovContrato){
		$this->nMovContrato = $nMovContrato;
	}
	public function getMovContrato(){
		return $this->nMovContrato;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = $sMovDocumento;
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setMovParcelas($nMovParcelas){
		$this->nMovParcelas = $nMovParcelas;
	}
	public function getMovParcelas(){
		return $this->nMovParcelas;
	}
	public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setMovIcmsAliq($nMovIcmsAliq){
		$this->nMovIcmsAliq = $nMovIcmsAliq;
	}
	public function getMovIcmsAliq(){
		return $this->nMovIcmsAliq;
	}
	public function getMovIcmsAliqFormatado(){
		 $vRetorno = number_format($this->nMovIcmsAliq , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIcmsAliqBanco($nMovIcmsAliq){
		if($nMovIcmsAliq){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIcmsAliq = str_replace($sOrigem, $sDestino, $nMovIcmsAliq);
	
		}else{
		$this->nMovIcmsAliq = 'null';
			}
		}
public function setValorContrato($nValorContrato){
		$this->nValorContrato = $nValorContrato;
	}
	public function getValorContrato(){
		return $this->nValorContrato;
	}
	public function getValorContratoFormatado(){
		 $vRetorno = number_format($this->nValorContrato , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorContratoBanco($nValorContrato){
		if($nValorContrato){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorContrato = str_replace($sOrigem, $sDestino, $nValorContrato);
	
		}else{
		$this->nValorContrato = 'null';
			}
		}
public function setMovPis($nMovPis){
		$this->nMovPis = $nMovPis;
	}
	public function getMovPis(){
		return $this->nMovPis;
	}
	public function getMovPisFormatado(){
		 $vRetorno = number_format($this->nMovPis , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovPisBanco($nMovPis){
		if($nMovPis){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovPis = str_replace($sOrigem, $sDestino, $nMovPis);
	
		}else{
		$this->nMovPis = 'null';
			}
		}
public function setMovConfins($nMovConfins){
		$this->nMovConfins = $nMovConfins;
	}
	public function getMovConfins(){
		return $this->nMovConfins;
	}
	public function getMovConfinsFormatado(){
		 $vRetorno = number_format($this->nMovConfins , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovConfinsBanco($nMovConfins){
		if($nMovConfins){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovConfins = str_replace($sOrigem, $sDestino, $nMovConfins);
	
		}else{
		$this->nMovConfins = 'null';
			}
		}
public function setMovCsll($nMovCsll){
		$this->nMovCsll = $nMovCsll;
	}
	public function getMovCsll(){
		return $this->nMovCsll;
	}
	public function getMovCsllFormatado(){
		 $vRetorno = number_format($this->nMovCsll , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovCsllBanco($nMovCsll){
		if($nMovCsll){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovCsll = str_replace($sOrigem, $sDestino, $nMovCsll);
	
		}else{
		$this->nMovCsll = 'null';
			}
		}
public function setMovIss($nMovIss){
		$this->nMovIss = $nMovIss;
	}
	public function getMovIss(){
		return $this->nMovIss;
	}
	public function getMovIssFormatado(){
		 $vRetorno = number_format($this->nMovIss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIssBanco($nMovIss){
		if($nMovIss){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIss = str_replace($sOrigem, $sDestino, $nMovIss);
	
		}else{
		$this->nMovIss = 'null';
			}
		}
public function setMovIr($nMovIr){
		$this->nMovIr = $nMovIr;
	}
	public function getMovIr(){
		return $this->nMovIr;
	}
	public function getMovIrFormatado(){
		 $vRetorno = number_format($this->nMovIr , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrBanco($nMovIr){
		if($nMovIr){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIr = str_replace($sOrigem, $sDestino, $nMovIr);
	
		}else{
		$this->nMovIr = 'null';
			}
		}
public function setMovIrrf($nMovIrrf){
		$this->nMovIrrf = $nMovIrrf;
	}
	public function getMovIrrf(){
		return $this->nMovIrrf;
	}
	public function getMovIrrfFormatado(){
		 $vRetorno = number_format($this->nMovIrrf , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrrfBanco($nMovIrrf){
		if($nMovIrrf){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovIrrf = str_replace($sOrigem, $sDestino, $nMovIrrf);
	
		}else{
		$this->nMovIrrf = 'null';
			}
		}
public function setMovInss($nMovInss){
		$this->nMovInss = $nMovInss;
	}
	public function getMovInss(){
		return $this->nMovInss;
	}
	public function getMovInssFormatado(){
		 $vRetorno = number_format($this->nMovInss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovInssBanco($nMovInss){
		if($nMovInss){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovInss = str_replace($sOrigem, $sDestino, $nMovInss);
	
		}else{
		$this->nMovInss = 'null';
			}
		}
public function setMovOutros($nMovOutros){
		$this->nMovOutros = $nMovOutros;
	}
	public function getMovOutros(){
		return $this->nMovOutros;
	}
	public function getMovOutrosFormatado(){
		 $vRetorno = number_format($this->nMovOutros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosBanco($nMovOutros){
		if($nMovOutros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovOutros = str_replace($sOrigem, $sDestino, $nMovOutros);
	
		}else{
		$this->nMovOutros = 'null';
			}
		}
public function setMovDevolucao($nMovDevolucao){
		$this->nMovDevolucao = $nMovDevolucao;
	}
	public function getMovDevolucao(){
		return $this->nMovDevolucao;
	}
	public function getMovDevolucaoFormatado(){
		 $vRetorno = number_format($this->nMovDevolucao , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovDevolucaoBanco($nMovDevolucao){
		if($nMovDevolucao){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovDevolucao = str_replace($sOrigem, $sDestino, $nMovDevolucao);
	
		}else{
		$this->nMovDevolucao = 'null';
			}
		}
public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMovOutrosDesc($nMovOutrosDesc){
		$this->nMovOutrosDesc = $nMovOutrosDesc;
	}
	public function getMovOutrosDesc(){
		return $this->nMovOutrosDesc;
	}
	public function getMovOutrosDescFormatado(){
		 $vRetorno = number_format($this->nMovOutrosDesc , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosDescBanco($nMovOutrosDesc){
		if($nMovOutrosDesc){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovOutrosDesc = str_replace($sOrigem, $sDestino, $nMovOutrosDesc);
		}else{
			$this->nMovOutrosDesc = 'null';
		}
	}
public function setCustoCodigo($sCustoCodigo){
		$this->sCustoCodigo = $sCustoCodigo;
	}
	public function getCustoCodigo(){
		return $this->sCustoCodigo;
	}
	public function setCustoDescricao($sCustoDescricao){
		$this->sCustoDescricao = $sCustoDescricao;
	}
	public function getCustoDescricao(){
		return $this->sCustoDescricao;
	}
	public function setNegocioCodigo($sNegocioCodigo){
		$this->sNegocioCodigo = $sNegocioCodigo;
	}
	public function getNegocioCodigo(){
		return $this->sNegocioCodigo;
	}
	public function setNegocioDescricao($sNegocioDescricao){
		$this->sNegocioDescricao = $sNegocioDescricao;
	}
	public function getNegocioDescricao(){
		return $this->sNegocioDescricao;
	}
	public function setCentroCodigo($sCentroCodigo){
		$this->sCentroCodigo = $sCentroCodigo;
	}
	public function getCentroCodigo(){
		return $this->sCentroCodigo;
	}
	public function setCentroDescricao($sCentroDescricao){
		$this->sCentroDescricao = $sCentroDescricao;
	}
	public function getCentroDescricao(){
		return $this->sCentroDescricao;
	}
	public function setUnidadeCodigo($sUnidadeCodigo){
		$this->sUnidadeCodigo = $sUnidadeCodigo;
	}
	public function getUnidadeCodigo(){
		return $this->sUnidadeCodigo;
	}
	public function setUnidadeDescricao($sUnidadeDescricao){
		$this->sUnidadeDescricao = $sUnidadeDescricao;
	}
	public function getUnidadeDescricao(){
		return $this->sUnidadeDescricao;
	}
	public function setSetorCodigo($sSetorCodigo){
		$this->sSetorCodigo = $sSetorCodigo;
	}
	public function getSetorCodigo(){
		return $this->sSetorCodigo;
	}
	public function setSetorDescricao($sSetorDescricao){
		$this->sSetorDescricao = $sSetorDescricao;
	}
	public function getSetorDescricao(){
		return $this->sSetorDescricao;
	}
	public function setTipoMovimentoCodigo($sTipoMovimentoCodigo){
		$this->sTipoMovimentoCodigo = $sTipoMovimentoCodigo;
	}
	public function getTipoMovimentoCodigo(){
		return $this->sTipoMovimentoCodigo;
	}
	public function setTipoMovimentoDescricao($sTipoMovimentoDescricao){
		$this->sTipoMovimentoDescricao = $sTipoMovimentoDescricao;
	}
	public function getTipoMovimentoDescricao(){
		return $this->sTipoMovimentoDescricao;
	}
	
 }
 ?>
