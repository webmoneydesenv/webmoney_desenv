<?php
 /**
  * @author Auto-Generated
  * @package fachadaView
  * @SGBD mysql
  * @tabela v_contrato_aditivo
  */
 class VContratoAditivo{
 	/**
	* @campo contrato_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nContratoCodigo;
	/**
	* @campo numero
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNumero;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo data_inicio
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataInicio;
	/**
	* @campo data_validade
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataValidade;
	/**
	* @campo valor_contrato
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorContrato;
	/**
	* @campo tipo_contrato
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sTipoContrato;


 	public function __construct(){

 	}

 	public function setContratoCodigo($nContratoCodigo){
		$this->nContratoCodigo = $nContratoCodigo;
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
	public function setNumero($sNumero){
		$this->sNumero = $sNumero;
	}
	public function getNumero(){
		return $this->sNumero;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setDataInicio($dDataInicio){
		$this->dDataInicio = $dDataInicio;
	}
	public function getDataInicio(){
		return $this->dDataInicio;
	}
	public function getDataInicioFormatado(){
		$oData = new DateTime($this->dDataInicio);		 return $oData->format("d/m/Y");
	}
	public function setDataInicioBanco($dDataInicio){
		 if($dDataInicio){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataInicio);
			 $this->dDataInicio = $oData->format('Y-m-d') ;
	}
		 }
	public function setDataValidade($dDataValidade){
		$this->dDataValidade = $dDataValidade;
	}
	public function getDataValidade(){
		return $this->dDataValidade;
	}
	public function getDataValidadeFormatado(){
		$oData = new DateTime($this->dDataValidade);		 return $oData->format("d/m/Y");
	}
	public function setDataValidadeBanco($dDataValidade){
		 if($dDataValidade){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataValidade);
			 $this->dDataValidade = $oData->format('Y-m-d') ;
	}
		 }
	public function setValorContrato($nValorContrato){
		$this->nValorContrato = $nValorContrato;
	}
	public function getValorContrato(){
		return $this->nValorContrato;
	}
	public function getValorContratoFormatado(){
		 $vRetorno = number_format($this->nValorContrato , 2, ',', '.');		 return $vRetorno;
	}
	public function setValorContratoBanco($nValorContrato){
		if($nValorContrato){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorContrato = str_replace($sOrigem, $sDestino, $nValorContrato);

		}else{
		$this->nValorContrato = 'null';
			}
		}
public function setTipoContrato($sTipoContrato){
		$this->sTipoContrato = $sTipoContrato;
	}
	public function getTipoContrato(){
		return $this->sTipoContrato;
	}

 }
 ?>
