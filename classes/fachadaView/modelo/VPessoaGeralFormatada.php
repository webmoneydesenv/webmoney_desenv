<?php
 /**
  * @author Auto-Generated 
  * @package fachadaView 
  * @SGBD mysql 
  * @tabela v_pessoa_geral_formatada 
  */
 class VPessoaGeralFormatada{
 	/**
	* @campo tip_nome
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sTipNome;
	/**
	* @campo pesg_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nPesgCodigo;
	/**
	* @campo tip_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nTipCodigo;
	/**
	* @campo cod_status
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodStatus;
	/**
	* @campo pesg_end_logra
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgEndLogra;
	/**
	* @campo pesg_end_bairro
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgEndBairro;
	/**
	* @campo pesg_end_cep
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgEndCep;
	/**
	* @campo cidade
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sCidade;
	/**
	* @campo estado
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sEstado;
	/**
	* @campo pesg_email
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgEmail;
	/**
	* @campo pes_fones
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesFones;
	/**
	* @campo pesg_inc
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgInc;
	/**
	* @campo pesg_alt
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgAlt;
	/**
	* @campo bco_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nBcoCodigo;
	/**
	* @campo tipocon_cod
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nTipoconCod;
	/**
	* @campo pesg_bco_agencia
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgBcoAgencia;
	/**
	* @campo pesg_bco_conta
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sPesgBcoConta;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;

	/**
	* @campo bco_nome
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sBcoNome;
	/**
	* @campo desc_status
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescStatus;
	/**
	* @campo tipocon_inc
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sTipoconInc;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo identificacao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sIdentificacao;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setTipNome($sTipNome){
		$this->sTipNome = $sTipNome;
	}
	public function getTipNome(){
		return $this->sTipNome;
	}
	public function setPesgCodigo($nPesgCodigo){
		$this->nPesgCodigo = $nPesgCodigo;
	}
	public function getPesgCodigo(){
		return $this->nPesgCodigo;
	}
	public function setTipCodigo($nTipCodigo){
		$this->nTipCodigo = $nTipCodigo;
	}
	public function getTipCodigo(){
		return $this->nTipCodigo;
	}
	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setPesgEndLogra($sPesgEndLogra){
		$this->sPesgEndLogra = $sPesgEndLogra;
	}
	public function getPesgEndLogra(){
		return $this->sPesgEndLogra;
	}
	public function setPesgEndBairro($sPesgEndBairro){
		$this->sPesgEndBairro = $sPesgEndBairro;
	}
	public function getPesgEndBairro(){
		return $this->sPesgEndBairro;
	}
	public function setPesgEndCep($sPesgEndCep){
		$this->sPesgEndCep = $sPesgEndCep;
	}
	public function getPesgEndCep(){
		return $this->sPesgEndCep;
	}
	public function setCidade($sCidade){
		$this->sCidade = $sCidade;
	}
	public function getCidade(){
		return $this->sCidade;
	}
	public function setEstado($sEstado){
		$this->sEstado = $sEstado;
	}
	public function getEstado(){
		return $this->sEstado;
	}
	public function setPesgEmail($sPesgEmail){
		$this->sPesgEmail = $sPesgEmail;
	}
	public function getPesgEmail(){
		return $this->sPesgEmail;
	}
	public function setPesFones($sPesFones){
		$this->sPesFones = $sPesFones;
	}
	public function getPesFones(){
		return $this->sPesFones;
	}
	public function setPesgInc($sPesgInc){
		$this->sPesgInc = $sPesgInc;
	}
	public function getPesgInc(){
		return $this->sPesgInc;
	}
	public function setPesgAlt($sPesgAlt){
		$this->sPesgAlt = $sPesgAlt;
	}
	public function getPesgAlt(){
		return $this->sPesgAlt;
	}
	public function setBcoCodigo($nBcoCodigo){
		$this->nBcoCodigo = $nBcoCodigo;
	}
	public function getBcoCodigo(){
		return $this->nBcoCodigo;
	}
	public function setTipoconCod($nTipoconCod){
		$this->nTipoconCod = $nTipoconCod;
	}
	public function getTipoconCod(){
		return $this->nTipoconCod;
	}
	public function setPesgBcoAgencia($sPesgBcoAgencia){
		$this->sPesgBcoAgencia = $sPesgBcoAgencia;
	}
	public function getPesgBcoAgencia(){
		return $this->sPesgBcoAgencia;
	}
	public function setPesgBcoConta($sPesgBcoConta){
		$this->sPesgBcoConta = $sPesgBcoConta;
	}
	public function getPesgBcoConta(){
		return $this->sPesgBcoConta;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

	public function setBcoNome($sBcoNome){
		$this->sBcoNome = $sBcoNome;
	}
	public function getBcoNome(){
		return $this->sBcoNome;
	}
	public function setDescStatus($sDescStatus){
		$this->sDescStatus = $sDescStatus;
	}
	public function getDescStatus(){
		return $this->sDescStatus;
	}
	public function setTipoconInc($sTipoconInc){
		$this->sTipoconInc = $sTipoconInc;
	}
	public function getTipoconInc(){
		return $this->sTipoconInc;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setIdentificacao($sIdentificacao){
		$this->sIdentificacao = $sIdentificacao;
	}
	public function getIdentificacao(){
		return $this->sIdentificacao;
	}
	
 }
 ?>
