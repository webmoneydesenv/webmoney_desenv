<?


class FachadaViewBD {

/**
 	 *
 	 * Método para inicializar um Objeto VContratoAditivo
 	 *
 	 * @return VContratoAditivo
 	 */
 	public function inicializarVContratoAditivo($nContratoCodigo,$sNumero,$sDescricao,$dDataInicio,$dDataValidade,$nValorContrato,$sTipoContrato){
 		$oVContratoAditivo = new VContratoAditivo();

		$oVContratoAditivo->setContratoCodigo($nContratoCodigo);
		$oVContratoAditivo->setNumero($sNumero);
		$oVContratoAditivo->setDescricao($sDescricao);
		$oVContratoAditivo->setDataInicioBanco($dDataInicio);
		$oVContratoAditivo->setDataValidadeBanco($dDataValidade);
		$oVContratoAditivo->setValorContratoBanco($nValorContrato);
		$oVContratoAditivo->setTipoContrato($sTipoContrato);
 		return $oVContratoAditivo;
 	}
/**
 	 *
 	 * Método para recuperar um objeto VContratoAditivo da base de dados
 	 *
 	 * @return VContratoAditivo
 	 */
 	public function recuperarUmVContratoAditivo(){
 		$oVContratoAditivo = new VContratoAditivo();
  		$oPersistencia = new Persistencia($oVContratoAditivo);
  		$sTabelas = "v_contrato_aditivo";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContratoAditivo)
  			return $voVContratoAditivo[0];
  		return false;
 	}

/**
 	 *
 	 * Método para recuperar um objeto VContratoAditivo da base de dados
 	 *
 	 * @return VContratoAditivo
 	 */
 	public function recuperarUmVContratoAditivoValidadeValorPorContrato($nContratoCodigo){
 		$oVContratoAditivo = new VContratoAditivo();
  		$oPersistencia = new Persistencia($oVContratoAditivo);
  		$sTabelas = "v_contrato_aditivo";
  		$sCampos = "contrato_codigo,MIN(data_inicio) as data_inicio,MAX(data_validade) as data_validade,SUM(valor_contrato) as valor_contrato";
  		$sComplemento = " WHERE contrato_codigo=".$nContratoCodigo;
  		$voVContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContratoAditivo)
  			return $voVContratoAditivo[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VContratoAditivo da base de dados
 	 *
 	 * @return VContratoAditivo[]
 	 */
 	public function recuperarTodosVContratoAditivo(){
 		$oVContratoAditivo = new VContratoAditivo();
  		$oPersistencia = new Persistencia($oVContratoAditivo);
  		$sTabelas = "v_contrato_aditivo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContratoAditivo)
  			return $voVContratoAditivo;
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VContratoAditivo da base de dados
 	 *
 	 * @return VContratoAditivo[]
 	 */
 	public function recuperarTodosVContratoAditivoPorContrato($nCodContrato){
 		$oVContratoAditivo = new VContratoAditivo();
  		$oPersistencia = new Persistencia($oVContratoAditivo);
  		$sTabelas = "v_contrato_aditivo";
  		$sCampos = "*";
  		$sComplemento = "WHERE contrato_codigo=".$nCodContrato;
  		$voVContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContratoAditivo)
  			return $voVContratoAditivo;
  		return false;
 	}

/**
 	 *
 	 * Método para inicializar um Objeto VMovimento
 	 *
 	 * @return VMovimento
 	 */
 	public function inicializarVMovimento($sEmpRazaoSocial,$sEmpFantasia,$sEmpCnpj,$sEmpImagem,$sEmpEndLogra,$sEmpEndBairro,$sEmpEndCidade,$sEmpEndCep,$sEmpEndUf,$sEmpEndFone,$sEmpSite,$sEmpFtp,$sEmpEmail,$sEmpCustos,$sEmpFinanceiro,$sEmpDiretor,$nEmpAceite,$dMovDataEmissao,$sTipNome,$sPesgEmail,$sPesFones,$nTipoconCod,$sPesgBcoAgencia,$sPesgBcoConta,$sBcoNome,$sNome,$sIdentificacao,$nCusCodigo,$nNegCodigo,$nCenCodigo,$nUniCodigo,$nPesCodigo,$nConCodigo,$nSetCodigo,$sMovObs,$sMovInc,$sMovAlt,$nMovContrato,$nTipAceCodigo,$sMovDocumento,$nMovParcelas,$nMovTipo,$nEmpCodigo,$nMovIcmsAliq,$nMovPis,$nMovConfins,$nMovCsll,$nMovIss,$nMovIr,$nMovIrrf,$nMovInss,$nMovOutros,$nMovDevolucao,$nAtivo,$nMovOutrosDesc,$nMovCodigo,$nMovItem,$dMovDataVencto,$dMovDataPrev,$nMovValor,$nMovJuros,$nMovValorPagar,$nTipDocCodigo,$nMovRetencao,$dMovDataInclusao,$nMovValorPago,$sCustoCodigo,$sCustoDescricao,$sNegocioCodigo,$sNegocioDescricao,$sCentroCodigo,$sCentroDescricao,$sUnidadeCodigo,$sUnidadeDescricao,$sSetorCodigo,$sSetorDescricao,$sTipoMovimentoCodigo,$sContaContabil,$sTipoMovimentoDescricao,$nValorLiquido){
 		$oVMovimento = new VMovimento();

		$oVMovimento->setEmpRazaoSocial($sEmpRazaoSocial);
		$oVMovimento->setEmpFantasia($sEmpFantasia);
		$oVMovimento->setEmpCnpj($sEmpCnpj);
		$oVMovimento->setEmpImagem($sEmpImagem);
		$oVMovimento->setEmpEndLogra($sEmpEndLogra);
		$oVMovimento->setEmpEndBairro($sEmpEndBairro);
		$oVMovimento->setEmpEndCidade($sEmpEndCidade);
		$oVMovimento->setEmpEndCep($sEmpEndCep);
		$oVMovimento->setEmpEndUf($sEmpEndUf);
		$oVMovimento->setEmpEndFone($sEmpEndFone);
		$oVMovimento->setEmpSite($sEmpSite);
		$oVMovimento->setEmpFtp($sEmpFtp);
		$oVMovimento->setEmpEmail($sEmpEmail);
		$oVMovimento->setEmpCustos($sEmpCustos);
		$oVMovimento->setEmpFinanceiro($sEmpFinanceiro);
		$oVMovimento->setEmpDiretor($sEmpDiretor);
		$oVMovimento->setEmpAceite($nEmpAceite);
		$oVMovimento->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVMovimento->setTipNome($sTipNome);
		$oVMovimento->setPesgEmail($sPesgEmail);
		$oVMovimento->setPesFones($sPesFones);
		$oVMovimento->setTipoconCod($nTipoconCod);
		$oVMovimento->setPesgBcoAgencia($sPesgBcoAgencia);
		$oVMovimento->setPesgBcoConta($sPesgBcoConta);
		$oVMovimento->setBcoNome($sBcoNome);
		$oVMovimento->setNome($sNome);
		$oVMovimento->setIdentificacao($sIdentificacao);
		$oVMovimento->setCusCodigo($nCusCodigo);
		$oVMovimento->setNegCodigo($nNegCodigo);
		$oVMovimento->setCenCodigo($nCenCodigo);
		$oVMovimento->setUniCodigo($nUniCodigo);
		$oVMovimento->setPesCodigo($nPesCodigo);
		$oVMovimento->setConCodigo($nConCodigo);
		$oVMovimento->setSetCodigo($nSetCodigo);
		$oVMovimento->setMovObs($sMovObs);
		$oVMovimento->setMovInc($sMovInc);
		$oVMovimento->setMovAlt($sMovAlt);
		$oVMovimento->setMovContrato($nMovContrato);
		$oVMovimento->setTipAceCodigo($nTipAceCodigo);
		$oVMovimento->setMovDocumento($sMovDocumento);
		$oVMovimento->setMovParcelas($nMovParcelas);
		$oVMovimento->setMovTipo($nMovTipo);
		$oVMovimento->setEmpCodigo($nEmpCodigo);
		$oVMovimento->setMovIcmsAliqBanco($nMovIcmsAliq);
		$oVMovimento->setMovPisBanco($nMovPis);
		$oVMovimento->setMovConfinsBanco($nMovConfins);
		$oVMovimento->setMovCsllBanco($nMovCsll);
		$oVMovimento->setMovIssBanco($nMovIss);
		$oVMovimento->setMovIrBanco($nMovIr);
		$oVMovimento->setMovIrrfBanco($nMovIrrf);
		$oVMovimento->setMovInssBanco($nMovInss);
		$oVMovimento->setMovOutrosBanco($nMovOutros);
		$oVMovimento->setMovDevolucaoBanco($nMovDevolucao);
		$oVMovimento->setAtivo($nAtivo);
		$oVMovimento->setMovOutrosDescBanco($nMovOutrosDesc);
		$oVMovimento->setMovValorBanco($nMovValor);
		$oVMovimento->setMovValorPagarBanco($nMovValorPagar);
		$oVMovimento->setFpgCodigo($nFpgCodigo);
		$oVMovimento->setTipDocCodigo($nTipDocCodigo);
		$oVMovimento->setMovRetencaoBanco($nMovRetencao);
		$oVMovimento->setMovDataInclusaoBanco($dMovDataInclusao);
		$oVMovimento->setMovValorPagoBanco($nMovValorPago);
		$oVMovimento->setCustoCodigo($sCustoCodigo);
		$oVMovimento->setCustoDescricao($sCustoDescricao);
		$oVMovimento->setNegocioCodigo($sNegocioCodigo);
		$oVMovimento->setNegocioDescricao($sNegocioDescricao);
		$oVMovimento->setCentroCodigo($sCentroCodigo);
		$oVMovimento->setCentroDescricao($sCentroDescricao);
		$oVMovimento->setUnidadeCodigo($sUnidadeCodigo);
		$oVMovimento->setUnidadeDescricao($sUnidadeDescricao);
		$oVMovimento->setSetorCodigo($sSetorCodigo);
		$oVMovimento->setSetorDescricao($sSetorDescricao);
		$oVMovimento->setTipoMovimentoCodigo($sTipoMovimentoCodigo);
		$oVMovimento->setContaContabil($sContaContabil);
		$oVMovimento->setTipoMovimentoDescricao($sTipoMovimentoDescricao);
		$oVMovimento->setValorLiquido($nValorLiquido);

 		return $oVMovimento;
 	}



 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimento da base de dados
 	 *
 	 * @return VMovimento[]
 	 */
 	public function recuperarTodosVMovimentoPorTipo($sTipo){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas = "v_movimento";
  		$sCampos = "*";
  		$sComplemento = " WHERE tipo_movimento_codigo = '".$sTipo."'";
  		$voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimento)
  			return $voVMovimento;
  		return false;
 	}

	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimento da base de dados
 	 *
 	 * @return VMovimento[]
 	 */
 	public function recuperarTodosVMovimentoPorContrato($nMovContrato){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas = "v_movimento";
  		$sCampos = "*";
  		$sComplemento = " WHERE mov_contrato = ".$nMovContrato;
  		$voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimento)
  			return $voVMovimento;
  		return false;
 	}



        /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnyAporteItem[]
         */
        public function recuperarUmVMovimentoItemAgrupadoLinha($nCodPessoa,$nMovCodigo,$nMovItem){
            $oVMovimento = new VMovimento();
            $oPersistencia = new Persistencia($oVMovimento);
            $sTabelas = "v_movimento";
            $sCampos = "*";
            $sComplemento = " WHERE pes_codigo=".$nCodPessoa." AND mov_codigo=". $nMovCodigo ." AND mov_item=".$nMovItem;

           $voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		    if($voVMovimento)
  			   return $voVMovimento[0];
  		    return false;
        }


 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimento da base de dados
 	 *
 	 * @return VMovimento[]
 	 */
 	public function recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuarioTotais($nCodUnidade,$nCodGrupoUsuario,$nSolicitacaoAporte){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas ="v_movimento";
  		$sCampos  ="(select sum(f_valor_parcela_liquido(v_movimento.mov_codigo,v_movimento.mov_item))
                    from v_movimento
                    INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.mov_codigo = v_movimento.mov_codigo And mny_movimento_item_protocolo.mov_item = v_movimento.mov_item
                    Where v_movimento.uni_codigo = ".$nCodUnidade." And mny_movimento_item_protocolo.para_cod_tipo_protocolo = ".$nCodGrupoUsuario." AND CONCAT(mny_movimento_item_protocolo.mov_codigo,'.',mny_movimento_item_protocolo.mov_item) not in (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) as FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo=1 and mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = ".$nCodUnidade.")
                    ) as mov_valor_parcela,
                    (SELECT sum(f_valor_parcela_liquido(mny_aporte_item.mov_codigo,mny_aporte_item.mov_item)) as ativo
                    from   		mny_aporte_item
                    INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                    INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                    WHERE mny_solicitacao_aporte.uni_codigo =".$nCodUnidade." AND mny_solicitacao_aporte.numero_solicitacao=".$nSolicitacaoAporte.") as ativo ";


  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die('aqui');
        $voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimento)
  			return $voVMovimento[0];
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimento da base de dados
 	 *
 	 * @return VMovimento[]
 	 */
 	public function recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuario($nCodUnidade,$nCodGrupoUsuario){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas ="v_movimento";
  		$sCampos  ="DISTINCT v_movimento.mov_codigo,v_movimento.mov_item,DATE_FORMAT(v_movimento.mov_data_vencto,'%d/%m/%Y') as mov_data_vencto,mov_valor_parcela,v_movimento.mov_obs,	v_movimento.mov_tipo";
  		$sComplemento = "INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.mov_codigo = v_movimento.mov_codigo And mny_movimento_item_protocolo.mov_item = v_movimento.mov_item
                        Where  v_movimento.mov_item Not In(98,99) And v_movimento.mov_tipo=188 And v_movimento.uni_codigo = ".$nCodUnidade." And mny_movimento_item_protocolo.para_cod_tipo_protocolo = " . $nCodGrupoUsuario ." AND CONCAT(mny_movimento_item_protocolo.mov_codigo,'.',mny_movimento_item_protocolo.mov_item) not in (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) as FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo=1 and mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = ".$nCodUnidade.")  Order BY mov_data_vencto ASC limit 1 ";
        //Where  v_movimento.mov_item Not In(98,99) And v_movimento.mov_tipo=188 And v_movimento.uni_codigo = ".$nCodUnidade." And mny_movimento_item_protocolo.para_cod_tipo_protocolo = " . $nCodGrupoUsuario ." AND CONCAT(mny_movimento_item_protocolo.mov_codigo,'.',mny_movimento_item_protocolo.mov_item) not in (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) as FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo=1 and mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = ".$nCodUnidade.")  Order BY mov_data_vencto ASC limit 20";
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
        $voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimento)
  			return $voVMovimento;
  		return false;
 	}

 	public function recuperarUmVMovimento($nMovCodigo, $nMovItem){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas = "v_movimento";
  		$sCampos = "*";
		$sComplemento = " where mov_codigo = ". $nMovCodigo ."  and mov_item = " . $nMovItem;
  	    //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
		$oVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($oVMovimento)
  			return $oVMovimento[0];
  		return false;
 	}
    public function recuperarUmVMovimentoPROCEDURE($nMovCodigo, $nMovItem){
 		$oVMovimento = new VMovimento();
  		$oPersistencia = new Persistencia($oVMovimento);
  		$sTabelas = "sp_movimento";
  		//$sCampos = "*";
		$sComplemento = $nMovCodigo ."," . $nMovItem;
  	    //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
		$oVMovimento = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
  		if($oVMovimento)
  			return $oVMovimento[0];
  		return false;
 	}

/**
 	 *
 	 * Método para inicializar um Objeto VPessoaFormatada
 	 *
 	 * @return VPessoaFormatada
 	 */
 	public function inicializarVPessoaFormatada($nPesgCodigo,$sPesgInc,$sPesgAlt,$nTipCodigo,$nCodStatus,$sPesFones){
 		$oVPessoaFormatada = new VPessoaFormatada();

		$oVPessoaFormatada->setPesgCodigo($nPesgCodigo);
		$oVPessoaFormatada->setPesgInc($sPesgInc);
		$oVPessoaFormatada->setPesgAlt($sPesgAlt);
		$oVPessoaFormatada->setTipCodigo($nTipCodigo);
		$oVPessoaFormatada->setCodStatus($nCodStatus);
		$oVPessoaFormatada->setPesFones($sPesFones);
 		return $oVPessoaFormatada;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPessoaFormatada da base de dados
 	 *
 	 * @return VPessoaFormatada[]
 	 */
 	public function recuperarTodosVPessoaFormatada(){
 		$oVPessoaFormatada = new VPessoaFormatada();
  		$oPersistencia = new Persistencia($oVPessoaFormatada);
  		$sTabelas = "v_pessoa_formatada";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVPessoaFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaFormatada)
  			return $voVPessoaFormatada;
  		return false;
 	}




	/**
 	 *
 	 * Método para inicializar um Objeto VPessoaGeralFormatada
 	 *
 	 * @return VPessoaGeralFormatada
 	 */
 	public function inicializarVPessoaGeralFormatada($sTipNome,$nPesgCodigo,$nTipCodigo,$nCodStatus,$sPesgEndLogra,$sPesgEndBairro,$sPesgEndCep,$sCidade,$sEstado,$sPesgEmail,$sPesFones,$sPesgInc,$sPesgAlt,$nBcoCodigo,$nTipoconCod,$sPesgBcoAgencia,$sPesgBcoConta,$nAtivo,$sBcoNome,$sDescStatus,$sTipoconInc,$sNome,$sIdentificacao){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();

		$oVPessoaGeralFormatada->setTipNome($sTipNome);
		$oVPessoaGeralFormatada->setPesgCodigo($nPesgCodigo);
		$oVPessoaGeralFormatada->setTipCodigo($nTipCodigo);
		$oVPessoaGeralFormatada->setCodStatus($nCodStatus);
		$oVPessoaGeralFormatada->setPesgEndLogra($sPesgEndLogra);
		$oVPessoaGeralFormatada->setPesgEndBairro($sPesgEndBairro);
		$oVPessoaGeralFormatada->setPesgEndCep($sPesgEndCep);
		$oVPessoaGeralFormatada->setCidade($sCidade);
		$oVPessoaGeralFormatada->setEstado($sEstado);
		$oVPessoaGeralFormatada->setPesgEmail($sPesgEmail);
		$oVPessoaGeralFormatada->setPesFones($sPesFones);
		$oVPessoaGeralFormatada->setPesgInc($sPesgInc);
		$oVPessoaGeralFormatada->setPesgAlt($sPesgAlt);
		$oVPessoaGeralFormatada->setBcoCodigo($nBcoCodigo);
		$oVPessoaGeralFormatada->setTipoconCod($nTipoconCod);
		$oVPessoaGeralFormatada->setPesgBcoAgencia($sPesgBcoAgencia);
		$oVPessoaGeralFormatada->setPesgBcoConta($sPesgBcoConta);
		$oVPessoaGeralFormatada->setAtivo($nAtivo);
		$oVPessoaGeralFormatada->setBcoNome($sBcoNome);
		$oVPessoaGeralFormatada->setDescStatus($sDescStatus);
		$oVPessoaGeralFormatada->setTipoconInc($sTipoconInc);
		$oVPessoaGeralFormatada->setNome($sNome);
		$oVPessoaGeralFormatada->setIdentificacao($sIdentificacao);
 		return $oVPessoaGeralFormatada;
 	}



 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPessoaGeralFormatada da base de dados
 	 *
 	 * @return VPessoaGeralFormatada[]
 	 */
 	public function recuperarTodosVPessoaGeralFormatada(){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();
  		$oPersistencia = new Persistencia($oVPessoaGeralFormatada);
  		$sTabelas = "v_pessoa_geral_formatada";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1 ORDER BY nome ASC";

		$voVPessoaGeralFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaGeralFormatada)
  			return $voVPessoaGeralFormatada;
  		return false;
 	}

     public function recuperarTodosVPessoaGeralFormatadaPorTipoContratoVigentesPorEmpresa($nTipoContrato,$nCodEmpresa){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();
  		$oPersistencia = new Persistencia($oVPessoaGeralFormatada);
  		$sTabelas = "v_pessoa_geral_formatada vp";
  		$sCampos = "DISTINCT pesg_codigo,identificacao,nome";
        //,f_contrato_saldo(cp.contrato_codigo) as numero,f_contrato_validade(cp.contrato_codigo) as data_validade";

        if(in_array($_SESSION['Perfil']['CodUnidade'],$_SESSION['UnidadeCentral'])){
    	$sComplemento = " inner join mny_contrato_pessoa as cp on cp.pes_codigo = vp.pesg_codigo
                          WHERE cp.ativo=1 and cp.tipo_contrato =".$nTipoContrato." and f_contrato_saldo(cp.contrato_codigo)>0 AND emp_codigo=".$nCodEmpresa."  order by nome";
        }else{
            $sComplemento = " inner join mny_contrato_pessoa as cp on cp.pes_codigo = vp.pesg_codigo
                          WHERE cp.ativo=1 and cp.tipo_contrato =".$nTipoContrato." and f_contrato_saldo(cp.contrato_codigo)>0 AND emp_codigo=".$nCodEmpresa." And cp.uni_codigo=".$_SESSION['Perfil']['CodUnidade']."  order by nome";
        }
            //" AND emp_codigo=" . $_SESSION['oEmpresa']->getEmpCodigo();
        //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
		//die();
		$voVPessoaGeralFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaGeralFormatada)
  			return $voVPessoaGeralFormatada;
  		return false;
 	}

	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPessoaGeralFormatada da base de dados
 	 *
 	 * @return VPessoaGeralFormatada[]
 	 */
 	public function recuperarTodosVPessoaGeralFormatadaFormulario(){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();
  		$oPersistencia = new Persistencia($oVPessoaGeralFormatada);
  		$sTabelas = "v_pessoa_geral_formatada";
  		$sCampos = " distinct pesg_codigo,nome";
  		$sComplemento = "WHERE ativo=1 ORDER BY nome ASC";
        //echo "SELECT " . $sCampos . " FROM " . $sTabelas . " " . $sComplemento;
		$voVPessoaGeralFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaGeralFormatada)
  			return $voVPessoaGeralFormatada;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPessoaGeralFormatada da base de dados
 	 *
 	 * @return VPessoaGeralFormatada[]
 	 */
 	public function recuperarUmVPessoaGeralFormatada($PesCodigo){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();
  		$oPersistencia = new Persistencia($oVPessoaGeralFormatada);
  		$sTabelas = "v_pessoa_geral_formatada";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1 and pesg_codigo=" . $PesCodigo;
  		$voVPessoaGeralFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaGeralFormatada)
  			return $voVPessoaGeralFormatada[0];
  		return false;
 	}
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPessoaGeralFormatada da base de dados
 	 *
 	 * @return VPessoaGeralFormatada[]
 	 */
 	public function recuperarUmVPessoaGeralFormatadaPorNome($sNome){
 		$oVPessoaGeralFormatada = new VPessoaGeralFormatada();
  		$oPersistencia = new Persistencia($oVPessoaGeralFormatada);
  		$sTabelas = "v_pessoa_geral_formatada";
  		$sCampos = "*";
  		$sComplemento = "WHERE nome like'$sNome%' limit 1";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voVPessoaGeralFormatada = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPessoaGeralFormatada)
  			return $voVPessoaGeralFormatada[0];
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto VPesquisaMovimento
 	 *
 	 * @return VPesquisaMovimento
 	 */
 	public function inicializarVPesquisaMovimento($sNome,$sIdentificacao,$dMovDataEmissao,$nEmpCodigo,$nMovContrato,$sMovDocumento,$sMovObs,$nMovTipo,$nMovCodigo,$nMovItem,$dMovDataVencto,$sCompetencia,$nPesgCodigo){
 		$oVPesquisaMovimento = new VPesquisaMovimento();

		$oVPesquisaMovimento->setNome($sNome);
		$oVPesquisaMovimento->setIdentificacao($sIdentificacao);
		$oVPesquisaMovimento->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVPesquisaMovimento->setEmpCodigo($nEmpCodigo);
		$oVPesquisaMovimento->setMovContrato($nMovContrato);
		$oVPesquisaMovimento->setMovDocumento($sMovDocumento);
		$oVPesquisaMovimento->setMovObs($sMovObs);
		$oVPesquisaMovimento->setMovTipo($nMovTipo);
		$oVPesquisaMovimento->setMovCodigo($nMovCodigo);
		$oVPesquisaMovimento->setMovDataVenctoBanco($dMovDataVencto);
		$oVPesquisaMovimento->setCompetencia($sCompetencia);
		$oVPesquisaMovimento->setPesgCodigo($nPesgCodigo);
 		return $oVPesquisaMovimento;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPesquisaMovimento da base de dados
 	 *
 	 * @return VPesquisaMovimento[]
 	 */
 	public function recuperarTodosVPesquisaMovimento(){
 		$oVPesquisaMovimento = new VPesquisaMovimento();
  		$oPersistencia = new Persistencia($oVPesquisaMovimento);
  		$sTabelas = "v_pesquisa_movimento";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVPesquisaMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimento)
  			return $voVPesquisaMovimento;
  		return false;
 	}

 	 public function recuperarTodosPesquisaMovimentoPorComplemento($sComplemento){
 		$oVPesquisaMovimento = new VPesquisaMovimento();
  		$oPersistencia = new Persistencia($oVPesquisaMovimento);
  		$sTabelas = "v_pesquisa_movimento";
  		$sCampos = "*";
  		$voVPesquisaMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimento)
  			return $voVPesquisaMovimento;
  		return false;
 	}




	/**
 	 *
 	 * Método para inicializar um Objeto VMovimentoContrato
 	 *
 	 * @return VMovimentoContrato
 	 */
 	 	public function inicializarVMovimentoContrato($sEmpRazaoSocial,$sEmpFantasia,$sEmpCnpj,$sEmpImagem,$sTipNome,$sPesgEmail,$sPesFones,$nTipoconCod,$sPesgBcoAgencia,$sPesgBcoConta,$sBcoNome,$sNome,$sIdentificacao,$nMovCodigo,$dMovDataEmissao,$dMovDataInclusao,$nCusCodigo,$nNegCodigo,$nCenCodigo,$nUniCodigo,$nPesCodigo,$nConCodigo,$nSetCodigo,$sMovObs,$sMovInc,$sMovAlt,$nMovContrato,$sMovDocumento,$nMovParcelas,$nMovTipo,$nEmpCodigo,$nMovIcmsAliq,$nValorContrato,$nMovPis,$nMovConfins,$nMovCsll,$nMovIss,$nMovIr,$nMovIrrf,$nMovInss,$nMovOutros,$nMovDevolucao,$nAtivo,$nMovOutrosDesc,$sCustoCodigo,$sCustoDescricao,$sNegocioCodigo,$sNegocioDescricao,$sCentroCodigo,$sCentroDescricao,$sUnidadeCodigo,$sUnidadeDescricao,$sSetorCodigo,$sSetorDescricao,$sTipoMovimentoCodigo,$sTipoMovimentoDescricao){
 		$oVMovimentoContrato = new VMovimentoContrato();

		$oVMovimentoContrato->setEmpRazaoSocial($sEmpRazaoSocial);
		$oVMovimentoContrato->setEmpFantasia($sEmpFantasia);
		$oVMovimentoContrato->setEmpCnpj($sEmpCnpj);
		$oVMovimentoContrato->setEmpImagem($sEmpImagem);
		$oVMovimentoContrato->setTipNome($sTipNome);
		$oVMovimentoContrato->setPesgEmail($sPesgEmail);
		$oVMovimentoContrato->setPesFones($sPesFones);
		$oVMovimentoContrato->setTipoconCod($nTipoconCod);
		$oVMovimentoContrato->setPesgBcoAgencia($sPesgBcoAgencia);
		$oVMovimentoContrato->setPesgBcoConta($sPesgBcoConta);
		$oVMovimentoContrato->setBcoNome($sBcoNome);
		$oVMovimentoContrato->setNome($sNome);
		$oVMovimentoContrato->setIdentificacao($sIdentificacao);
		$oVMovimentoContrato->setMovCodigo($nMovCodigo);
		$oVMovimentoContrato->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVMovimentoContrato->setMovDataInclusaoBanco($dMovDataInclusao);
		$oVMovimentoContrato->setCusCodigo($nCusCodigo);
		$oVMovimentoContrato->setNegCodigo($nNegCodigo);
		$oVMovimentoContrato->setCenCodigo($nCenCodigo);
		$oVMovimentoContrato->setUniCodigo($nUniCodigo);
		$oVMovimentoContrato->setPesCodigo($nPesCodigo);
		$oVMovimentoContrato->setConCodigo($nConCodigo);
		$oVMovimentoContrato->setSetCodigo($nSetCodigo);
		$oVMovimentoContrato->setMovObs($sMovObs);
		$oVMovimentoContrato->setMovInc($sMovInc);
		$oVMovimentoContrato->setMovAlt($sMovAlt);
		$oVMovimentoContrato->setMovContrato($nMovContrato);
		$oVMovimentoContrato->setMovDocumento($sMovDocumento);
		$oVMovimentoContrato->setMovParcelas($nMovParcelas);
		$oVMovimentoContrato->setMovTipo($nMovTipo);
		$oVMovimentoContrato->setEmpCodigo($nEmpCodigo);
		$oVMovimentoContrato->setMovIcmsAliqBanco($nMovIcmsAliq);
		$oVMovimentoContrato->setValorContratoBanco($nValorContrato);
		$oVMovimentoContrato->setMovPisBanco($nMovPis);
		$oVMovimentoContrato->setMovConfinsBanco($nMovConfins);
		$oVMovimentoContrato->setMovCsllBanco($nMovCsll);
		$oVMovimentoContrato->setMovIssBanco($nMovIss);
		$oVMovimentoContrato->setMovIrBanco($nMovIr);
		$oVMovimentoContrato->setMovIrrfBanco($nMovIrrf);
		$oVMovimentoContrato->setMovInssBanco($nMovInss);
		$oVMovimentoContrato->setMovOutrosBanco($nMovOutros);
		$oVMovimentoContrato->setMovDevolucaoBanco($nMovDevolucao);
		$oVMovimentoContrato->setAtivo($nAtivo);
		$oVMovimentoContrato->setMovOutrosDescBanco($nMovOutrosDesc);
		$oVMovimentoContrato->setCustoCodigo($sCustoCodigo);
		$oVMovimentoContrato->setCustoDescricao($sCustoDescricao);
		$oVMovimentoContrato->setNegocioCodigo($sNegocioCodigo);
		$oVMovimentoContrato->setNegocioDescricao($sNegocioDescricao);
		$oVMovimentoContrato->setCentroCodigo($sCentroCodigo);
		$oVMovimentoContrato->setCentroDescricao($sCentroDescricao);
		$oVMovimentoContrato->setUnidadeCodigo($sUnidadeCodigo);
		$oVMovimentoContrato->setUnidadeDescricao($sUnidadeDescricao);
		$oVMovimentoContrato->setSetorCodigo($sSetorCodigo);
		$oVMovimentoContrato->setSetorDescricao($sSetorDescricao);
		$oVMovimentoContrato->setTipoMovimentoCodigo($sTipoMovimentoCodigo);
		$oVMovimentoContrato->setTipoMovimentoDescricao($sTipoMovimentoDescricao);
 		return $oVMovimentoContrato;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VMovimentoContrato
 	 *
 	 * @return boolean
 	 */
 	public function inserirVMovimentoContrato($oVMovimentoContrato){
 		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VMovimentoContrato
 	 *
 	 * @return boolean
 	 */
 	public function alterarVMovimentoContrato($oVMovimentoContrato){
 		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VMovimentoContrato
 	 *
 	 * @return boolean
 	 */
 	public function excluirVMovimentoContrato(){
 		$oVMovimentoContrato = new VMovimentoContrato();

  		$oPersistencia = new Persistencia($oVMovimentoContrato);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VMovimentoContrato na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVMovimentoContrato(){
 		$oVMovimentoContrato = new VMovimentoContrato();

  		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VMovimentoContrato da base de dados
 	 *
 	 * @return VMovimentoContrato
 	 */
 	public function recuperarUmVMovimentoContrato(){
 		$oVMovimentoContrato = new VMovimentoContrato();
  		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		$sTabelas = "v_movimento_contrato";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVMovimentoContrato = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoContrato)
  			return $voVMovimentoContrato[0];
  		return false;
 	}

		public function recuperarUmVMovimentoContratoPorMovimento($nMovCodigo){
 		$oVMovimentoContrato = new VMovimentoContrato();
  		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		$sTabelas = "v_movimento_contrato";
  		$sCampos = "*";
  		$sComplemento = " WHERE mov_codigo=".$nMovCodigo;
  		$voVMovimentoContrato = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoContrato)
  			return $voVMovimentoContrato[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimentoContrato da base de dados
 	 *
 	 * @return VMovimentoContrato[]
 	 */
 	public function recuperarTodosVMovimentoContrato(){
 		$oVMovimentoContrato = new VMovimentoContrato();
  		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		$sTabelas = "v_movimento_contrato";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVMovimentoContrato = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoContrato)
  			return $voVMovimentoContrato;
  		return false;
 	}

	public function recuperarTodosVMovimentoContratoSemContrato($nEmpCodigo){
 		$oVMovimentoContrato = new VMovimentoContrato();
  		$oPersistencia = new Persistencia($oVMovimentoContrato);
  		$sTabelas = "v_movimento_contrato";
  		$sCampos = "*";
  	 	$sComplemento = "where mov_contrato is Null and emp_codigo =". $nEmpCodigo;
		//echo "SELECT * FROM $sTabelas " . $sComplemento;
  		$voVMovimentoContrato = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoContrato)
  			return $voVMovimentoContrato;
  		return false;
 	}


	/**
 	 *
 	 * Método para inicializar um Objeto VPesquisaMovimentoItem
 	 *
 	 * @return VPesquisaMovimentoItem
 	 */
 	public function inicializarVPesquisaMovimentoItem($sCompetencia,$sEmpRazaoSocial,$sEmpFantasia,$sEmpCnpj,$sEmpImagem,$dMovDataEmissao,$sTipNome,$sPesgEmail,$sPesFones,$nTipoconCod,$sPesgBcoAgencia,$sPesgBcoConta,$sBcoNome,$sNome,$sIdentificacao,$nCusCodigo,$nNegCodigo,$nCenCodigo,$nUniCodigo,$nPesCodigo,$nConCodigo,$nSetCodigo,$sMovObs,$sMovInc,$sMovAlt,$nMovContrato,$sMovDocumento,$nMovParcelas,$nMovTipo,$nEmpCodigo,$nMovIcmsAliq,$nMovValorGlob,$nMovPis,$nMovConfins,$nMovCsll,$nMovIss,$nMovIr,$nMovIrrf,$nMovInss,$nMovOutros,$nMovDevolucao,$nAtivo,$nMovOutrosDesc,$nComCodigo,$sCustoCodigo,$sCustoDescricao,$sNegocioCodigo,$sNegocioDescricao,$sCentroCodigo,$sCentroDescricao,$sUnidadeCodigo,$sUnidadeDescricao,$sAceiteCodigo,$sAceiteDescricao,$sSetorCodigo,$sSetorDescricao,$sTipoDocumentoCodigo,$sTipoDocumentoDescricao,$sTipoMovimentoCodigo,$sTipoMovimentoDescricao,$sContaCodigo,$sContaDescricao,$sFormaPagamentoCodigo,$sFormaPagamentoDescricao,$nMovCodigo,$nMovItem,$dMovDataVencto,$dMovDataPrev,$nMovValor,$nMovJuros,$nMovValorPagar,$nFpgCodigo,$nTipDocCodigo,$nMovRetencao,$dMovDataInclusao,$nMovValorPago,$nTipAceCodigo){
 		$oVPesquisaMovimentoItem = new VPesquisaMovimentoItem();

		$oVPesquisaMovimentoItem->setCompetencia($sCompetencia);
		$oVPesquisaMovimentoItem->setEmpRazaoSocial($sEmpRazaoSocial);
		$oVPesquisaMovimentoItem->setEmpFantasia($sEmpFantasia);
		$oVPesquisaMovimentoItem->setEmpCnpj($sEmpCnpj);
		$oVPesquisaMovimentoItem->setEmpImagem($sEmpImagem);
		$oVPesquisaMovimentoItem->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVPesquisaMovimentoItem->setTipNome($sTipNome);
		$oVPesquisaMovimentoItem->setPesgEmail($sPesgEmail);
		$oVPesquisaMovimentoItem->setPesFones($sPesFones);
		$oVPesquisaMovimentoItem->setTipoconCod($nTipoconCod);
		$oVPesquisaMovimentoItem->setPesgBcoAgencia($sPesgBcoAgencia);
		$oVPesquisaMovimentoItem->setPesgBcoConta($sPesgBcoConta);
		$oVPesquisaMovimentoItem->setBcoNome($sBcoNome);
		$oVPesquisaMovimentoItem->setNome($sNome);
		$oVPesquisaMovimentoItem->setIdentificacao($sIdentificacao);
		$oVPesquisaMovimentoItem->setCusCodigo($nCusCodigo);
		$oVPesquisaMovimentoItem->setNegCodigo($nNegCodigo);
		$oVPesquisaMovimentoItem->setCenCodigo($nCenCodigo);
		$oVPesquisaMovimentoItem->setUniCodigo($nUniCodigo);
		$oVPesquisaMovimentoItem->setPesCodigo($nPesCodigo);
		$oVPesquisaMovimentoItem->setConCodigo($nConCodigo);
		$oVPesquisaMovimentoItem->setSetCodigo($nSetCodigo);
		$oVPesquisaMovimentoItem->setMovObs($sMovObs);
		$oVPesquisaMovimentoItem->setMovInc($sMovInc);
		$oVPesquisaMovimentoItem->setMovAlt($sMovAlt);
		$oVPesquisaMovimentoItem->setMovContrato($nMovContrato);
		$oVPesquisaMovimentoItem->setMovDocumento($sMovDocumento);
		$oVPesquisaMovimentoItem->setMovParcelas($nMovParcelas);
		$oVPesquisaMovimentoItem->setMovTipo($nMovTipo);
		$oVPesquisaMovimentoItem->setEmpCodigo($nEmpCodigo);
		$oVPesquisaMovimentoItem->setMovIcmsAliqBanco($nMovIcmsAliq);
		$oVPesquisaMovimentoItem->setMovValorGlobBanco($nMovValorGlob);
		$oVPesquisaMovimentoItem->setMovPisBanco($nMovPis);
		$oVPesquisaMovimentoItem->setMovConfinsBanco($nMovConfins);
		$oVPesquisaMovimentoItem->setMovCsllBanco($nMovCsll);
		$oVPesquisaMovimentoItem->setMovIssBanco($nMovIss);
		$oVPesquisaMovimentoItem->setMovIrBanco($nMovIr);
		$oVPesquisaMovimentoItem->setMovIrrfBanco($nMovIrrf);
		$oVPesquisaMovimentoItem->setMovInssBanco($nMovInss);
		$oVPesquisaMovimentoItem->setMovOutrosBanco($nMovOutros);
		$oVPesquisaMovimentoItem->setMovDevolucaoBanco($nMovDevolucao);
		$oVPesquisaMovimentoItem->setAtivo($nAtivo);
		$oVPesquisaMovimentoItem->setMovOutrosDescBanco($nMovOutrosDesc);
		$oVPesquisaMovimentoItem->setComCodigo($nComCodigo);
		$oVPesquisaMovimentoItem->setCustoCodigo($sCustoCodigo);
		$oVPesquisaMovimentoItem->setCustoDescricao($sCustoDescricao);
		$oVPesquisaMovimentoItem->setNegocioCodigo($sNegocioCodigo);
		$oVPesquisaMovimentoItem->setNegocioDescricao($sNegocioDescricao);
		$oVPesquisaMovimentoItem->setCentroCodigo($sCentroCodigo);
		$oVPesquisaMovimentoItem->setCentroDescricao($sCentroDescricao);
		$oVPesquisaMovimentoItem->setUnidadeCodigo($sUnidadeCodigo);
		$oVPesquisaMovimentoItem->setUnidadeDescricao($sUnidadeDescricao);
		$oVPesquisaMovimentoItem->setAceiteCodigo($sAceiteCodigo);
		$oVPesquisaMovimentoItem->setAceiteDescricao($sAceiteDescricao);
		$oVPesquisaMovimentoItem->setSetorCodigo($sSetorCodigo);
		$oVPesquisaMovimentoItem->setSetorDescricao($sSetorDescricao);
		$oVPesquisaMovimentoItem->setTipoDocumentoCodigo($sTipoDocumentoCodigo);
		$oVPesquisaMovimentoItem->setTipoDocumentoDescricao($sTipoDocumentoDescricao);
		$oVPesquisaMovimentoItem->setTipoMovimentoCodigo($sTipoMovimentoCodigo);
		$oVPesquisaMovimentoItem->setTipoMovimentoDescricao($sTipoMovimentoDescricao);
		$oVPesquisaMovimentoItem->setContaCodigo($sContaCodigo);
		$oVPesquisaMovimentoItem->setContaDescricao($sContaDescricao);
		$oVPesquisaMovimentoItem->setFormaPagamentoCodigo($sFormaPagamentoCodigo);
		$oVPesquisaMovimentoItem->setFormaPagamentoDescricao($sFormaPagamentoDescricao);
		$oVPesquisaMovimentoItem->setMovCodigo($nMovCodigo);
		$oVPesquisaMovimentoItem->setMovItem($nMovItem);
		$oVPesquisaMovimentoItem->setMovDataVenctoBanco($dMovDataVencto);
		$oVPesquisaMovimentoItem->setMovDataPrevBanco($dMovDataPrev);
		$oVPesquisaMovimentoItem->setMovValorBanco($nMovValor);
		$oVPesquisaMovimentoItem->setMovJurosBanco($nMovJuros);
		$oVPesquisaMovimentoItem->setMovValorPagarBanco($nMovValorPagar);
		$oVPesquisaMovimentoItem->setFpgCodigo($nFpgCodigo);
		$oVPesquisaMovimentoItem->setTipDocCodigo($nTipDocCodigo);
		$oVPesquisaMovimentoItem->setMovRetencaoBanco($nMovRetencao);
		$oVPesquisaMovimentoItem->setMovDataInclusaoBanco($dMovDataInclusao);
		$oVPesquisaMovimentoItem->setMovValorPagoBanco($nMovValorPago);
		$oVPesquisaMovimentoItem->setTipAceCodigo($nTipAceCodigo);
 		return $oVPesquisaMovimentoItem;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VPesquisaMovimentoItem
 	 *
 	 * @return boolean
 	 */
 	public function inserirVPesquisaMovimentoItem($oVPesquisaMovimentoItem){
 		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VPesquisaMovimentoItem
 	 *
 	 * @return boolean
 	 */
 	public function alterarVPesquisaMovimentoItem($oVPesquisaMovimentoItem){
 		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VPesquisaMovimentoItem
 	 *
 	 * @return boolean
 	 */
 	public function excluirVPesquisaMovimentoItem(){
 		$oVPesquisaMovimentoItem = new VPesquisaMovimentoItem();

  		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VPesquisaMovimentoItem na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVPesquisaMovimentoItem(){
 		$oVPesquisaMovimentoItem = new VPesquisaMovimentoItem();

  		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VPesquisaMovimentoItem da base de dados
 	 *
 	 * @return VPesquisaMovimentoItem
 	 */
 	public function recuperarUmVPesquisaMovimentoItem($nMovCodigo, $nMovItem){
 		$oVPesquisaMovimentoItem = new VPesquisaMovimentoItem();
  		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
  		$sTabelas = "v_pesquisa_movimento_item";
  		$sCampos = "*";
		$sComplemento = " where mov_codigo = ". $nMovCodigo ."  and mov_item = " . $nMovItem;
  		$voVPesquisaMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimentoItem)
  			return $voVPesquisaMovimentoItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPesquisaMovimentoItem da base de dados
 	 *
 	 * @return VPesquisaMovimentoItem[]
 	 */
 	public function recuperarTodosVPesquisaMovimentoItem(){
 		$oVPesquisaMovimentoItem = new VPesquisaMovimentoItem();
  		$oPersistencia = new Persistencia($oVPesquisaMovimentoItem);
  		$sTabelas = "v_pesquisa_movimento_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVPesquisaMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimentoItem)
  			return $voVPesquisaMovimentoItem;
  		return false;
 	}

	public function recuperarTodosPesquisaMovimentoItemPorComplemento($sComplemento){
 		$oVPesquisaMovimento = new VPesquisaMovimentoItem();
  		$oPersistencia = new Persistencia($oVPesquisaMovimento);
  		$sTabelas = "v_pesquisa_movimento_item";
  		$sCampos = "*";
//		echo "SELECT $sCampos FROM $sTabelas $sComplemento";
  		$voVPesquisaMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimento)
  			return $voVPesquisaMovimento;
  		return false;
 	}

	//pesquisa pela procedure...
	public function recuperarTodosPesquisaMovimentoItemPorComplementoProcedure($sComplemento){
 		$oVPesquisaMovimento = new VPesquisaMovimentoItem();
  		$oPersistencia = new Persistencia($oVPesquisaMovimento);
  		$sProcedure = "sp_ficha_aceite";
        $sComplemento = "(47,1)";
		//echo "CALL $sProcedure $sComplemento";
  		$voVPesquisaMovimento = $oPersistencia->consultarProcedure($sProcedure,$sComplemento);
  		if($voVPesquisaMovimento)
  			return $voVPesquisaMovimento;
  		return false;
 	}

	public function recuperarTodosPesquisaContratoMovimentoItemPorComplemento($nMovCodigo){
 		$oVPesquisaMovimento = new VPesquisaMovimentoItem();
  		$oPersistencia = new Persistencia($oVPesquisaMovimento);
  		$sTabelas = "v_pesquisa_movimento_item";
  		$sCampos = " nome, identificacao, mov_tipo,
		             mov_data_emissao, mov_documento,
					 mov_codigo, mov_data_vencto,
					 mov_tipo, competencia,mov_item";
		$sComplemento = "where mov_codigo= " . $nMovCodigo;
  		$voVPesquisaMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisaMovimento)
  			return $voVPesquisaMovimento;
  		return false;
 	}

	public function inicializarVMovimentoConciliacao($nCodConciliacao,$nMovCodigo,$nMovItem,$nCodUnidade,$nTipoCaixa,$dDataConciliacao,$nConsolidado,$nOrdem,$sNome,$nMovValor,$nPesCodigo,$sHistorico,$nMovTipo,$nContaCodigo,  $nUnidadeDescricao,$sBcoNome,$nContaCorrenteConc,$nAgenciaContaConc,  $sAgencia,$sConta,$sCaminhoArquivo,$nCodArquivo,$nCodSolicitacaoAporte,$nFinalizado){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();

		$oVMovimentoConciliacao->setCodConciliacao($nCodConciliacao);
		$oVMovimentoConciliacao->setMovCodigo($nMovCodigo);
		$oVMovimentoConciliacao->setMovItem($nMovItem);
		$oVMovimentoConciliacao->setFinalizado($nFinalizado);
		$oVMovimentoConciliacao->setCodUnidade($nCodUnidade);
		$oVMovimentoConciliacao->setTipoCaixa($nTipoCaixa);
		$oVMovimentoConciliacao->setDataConciliacaoBanco($dDataConciliacao);
		$oVMovimentoConciliacao->setConsolidado($nConsolidado);
		$oVMovimentoConciliacao->setOrdem($nOrdem);
		$oVMovimentoConciliacao->setNome($sNome);
		$oVMovimentoConciliacao->setMovValorBanco($nMovValor);
		$oVMovimentoConciliacao->setPesCodigo($nPesCodigo);
		$oVMovimentoConciliacao->setHistorico($sHistorico);
		$oVMovimentoConciliacao->setMovTipo($nMovTipo);
		$oVMovimentoConciliacao->setContaCodigo($nContaCodigo);

		$oVMovimentoConciliacao->setUnidadeDescricao($nUnidadeDescricao);
		$oVMovimentoConciliacao->setBcoNome($sBcoNome);
		$oVMovimentoConciliacao->setContaCorrenteConc($nContaCorrenteConc);
		$oVMovimentoConciliacao->setAgenciaContaConc($nAgenciaContaConc);

		$oVMovimentoConciliacao->setAgencia($sAgencia);
		$oVMovimentoConciliacao->setConta($sConta);
		$oVMovimentoConciliacao->setCaminhoArquivo($sCaminhoArquivo);
		$oVMovimentoConciliacao->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
		$oVMovimentoConciliacao->setCodArquivo($nCodArquivo);
 		return $oVMovimentoConciliacao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VMovimentoConciliacao
 	 *
 	 * @return boolean
 	 */
 	public function inserirVMovimentoConciliacao($oVMovimentoConciliacao){
 		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VMovimentoConciliacao
 	 *
 	 * @return boolean
 	 */
 	public function alterarVMovimentoConciliacao($oVMovimentoConciliacao){
 		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VMovimentoConciliacao
 	 *
 	 * @return boolean
 	 */
 	public function excluirVMovimentoConciliacao(){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();

  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VMovimentoConciliacao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVMovimentoConciliacao(){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();

  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VMovimentoConciliacao da base de dados
 	 *
 	 * @return VMovimentoConciliacao
 	 */
 	public function recuperarUmVMovimentoConciliacao(){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao[0];
  		return false;
 	}

	/**
 	 *
 	 * Método para recuperar um objeto VMovimentoConciliacao da base de dados
 	 *
 	 * @return VMovimentoConciliacao
 	 */
    //CONCILIACAO CONTA / UNIDADE
 	public function recuperarUmVMovimentoConciliacaoPorMaiorOrdem2($dData,$nCodUnidade,$nCodContaCorrente){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "MAX(ordem) AS ordem";
  		$sComplemento = " WHERE  cod_unidade = ".$nCodUnidade." AND conta_codigo_conc=".$nCodContaCorrente." AND data_conciliacao ='".$dData."' AND consolidado = 0";
  		//echo "SELECT $sCampos FROm $sTabelas $sComplemento";
		//die();
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao[0];
  		return false;
 	}
    //CONCILIACAO CONTA
    public function recuperarUmVMovimentoConciliacaoPorMaiorOrdem($dData,$nCodContaCorrente){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "MAX(ordem) AS ordem";
  		$sComplemento = " WHERE conta_codigo_conc=".$nCodContaCorrente." AND data_conciliacao ='".$dData."' AND consolidado = 0";
  		//echo "SELECT $sCampos FROm $sTabelas $sComplemento";
		//die();
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao[0];
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um objeto VMovimentoConciliacao da base de dados
 	 *
 	 * @return VMovimentoConciliacao
 	 */
 	public function recuperarUmVMovimentoConciliacaoPorMenorOrdem($dData,$nCodUnidade,$nCodContaCorrente){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "MIN(ordem) As ordem";
  		$sComplemento = " WHERE cod_unidade = ".$nCodUnidade." AND conta_codigo_conc=".$nCodContaCorrente." AND data_conciliacao ='".$dData."' AND consolidado = 0 ";
  		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		//echo "SELECT $sCampos FROm $sTabelas $sComplemento";
  		//die();
		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimentoConciliacao da base de dados
 	 *
 	 * @return VMovimentoConciliacao[]
 	 */
 	public function recuperarTodosVMovimentoConciliacao(){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VMovimentoConciliacao da base de dados
 	 *
 	 * @return VMovimentoConciliacao[]
 	 */
 	public function recuperarTodosVMovimentoConciliacaoPorData($nDataConciliacao){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "*";
  		$sComplemento = "WHERE data_conciliacao='" . $nDataConciliacao . "'";
  		//echo "select  $sCampos from $sTabelas $sComplemento ";
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}

	public function recuperarTodosVMovimentoConciliacaoPorDataUnidadeConta($nDataConciliacao, $nCodUnidade, $nCodConta){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "*";
  		$sComplemento = "WHERE data_conciliacao='" . $nDataConciliacao . "' AND cod_unidade = ". $nCodUnidade ." AND conta_codigo_conc = ". $nCodConta ." ORDER BY ordem ASC";
  		//echo "select  $sCampos from $sTabelas $sComplemento ";
		//die();
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}

    //CONCILIACAO POR CONTA...
    public function recuperarTodosVMovimentoConciliacaoPorDataConta($nDataConciliacao, $nCodConta){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "*";
  		$sComplemento = "WHERE data_conciliacao='" . $nDataConciliacao . "'  AND conta_codigo_conc = ". $nCodConta ." ORDER BY ordem ASC";
  		//echo "select  $sCampos from $sTabelas $sComplemento ";
		//die();
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}



 	public function recuperarTodosVMovimentoConciliacaoTotalPorData($dDataConciliacao){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "(SELECT SUM(mov_valor) FROM v_movimento_conciliacao WHERE tipo_caixa = 2	AND data_conciliacao = '" . $dDataConciliacao . "') AS mov_valor,
			        (SELECT SUM(mov_valor) FROM v_movimento_conciliacao WHERE tipo_caixa = 1    AND data_conciliacao = '" . $dDataConciliacao . "') AS cod_unidade,
			        (SELECT SUM(mov_valor) FROM v_movimento_conciliacao WHERE tipo_caixa = 3    AND data_conciliacao = '" . $dDataConciliacao . "') AS conta_codigo,
					(SELECT SUM(mov_valor) FROM v_movimento_conciliacao WHERE tipo_caixa = 4	AND data_conciliacao = '" . $dDataConciliacao . "') AS pes_codigo";


	//	echo "SELECT $sCampos FROM $sTabelas";
		$sComplemento = "";
  		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}

 	public function recuperarTodosVMovimentoConciliacaoTotalConsolidadoPorData($dDataConciliacao){
 		$oVMovimentoConciliacao = new VMovimentoConciliacao();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacao);
  		$sTabelas = "v_movimento_conciliacao";
  		$sCampos = "DISTINCT
					(SELECT IFNULL(Sum(mny_movimento_item.mov_valor_parcela),0) FROM mny_conciliacao inner join mny_movimento_item ON mny_conciliacao.mov_codigo = mny_movimento_item.mov_codigo AND mny_conciliacao.mov_item = mny_movimento_item.mov_item where mny_conciliacao.data_conciliacao = '".$dDataConciliacao."' and tipo_caixa = 1 and mny_conciliacao.consolidado = 1 and mny_conciliacao.ativo = 1)AS tipo_caixa,
					(SELECT SUM(valor_vale) FROM mny_conciliacao WHERE tipo_caixa = 2	AND consolidado = 1 AND ativo = 1 AND data_conciliacao = '".$dDataConciliacao."') + (SELECT IFNULL(SUM(valor_doc_pendente),0) FROM mny_conciliacao WHERE tipo_caixa = 2	AND consolidado = 1 AND ativo = 1 AND data_conciliacao = '".$dDataConciliacao."' and mny_conciliacao.consolidado = 1 and mny_conciliacao.ativo = 1) AS mov_valor,
					(SELECT IFNULL(Sum(mny_movimento_item.mov_valor_parcela),0) FROM mny_conciliacao inner join mny_movimento_item ON mny_conciliacao.mov_codigo = mny_movimento_item.mov_codigo AND mny_conciliacao.mov_item = mny_movimento_item.mov_item where mny_conciliacao.data_conciliacao = '".$dDataConciliacao."' and tipo_caixa = 3 and mny_conciliacao.consolidado = 1 and mny_conciliacao.ativo = 1) AS conta_codigo,
					(SELECT IFNULL(Sum(mny_movimento_item.mov_valor_parcela),0) FROM mny_conciliacao inner join mny_movimento_item ON mny_conciliacao.mov_codigo = mny_movimento_item.mov_codigo AND mny_conciliacao.mov_item = mny_movimento_item.mov_item where mny_conciliacao.data_conciliacao = '".$dDataConciliacao."' and tipo_caixa = 4 and mny_conciliacao.consolidado = 1 and mny_conciliacao.ativo = 1)AS pes_codigo,
					(SELECT DISTINCT data_conciliacao FROM mny_conciliacao WHERE consolidado = 1 AND ativo = 1 AND data_conciliacao = '".$dDataConciliacao."') AS data_conciliacao, cod_unidade,conta_codigo_conc, data_conciliacao, unidade_descricao, bco_nome, conta_corrente_conc, agencia_conta_conc";
				//$sCampos = "*";
  		$sComplemento = " -- WHERE consolidado = 1 ";
  		//echo "SELECT $sCampos FROM $sTabelas WHERE $sComplemento";
		$voVMovimentoConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacao)
  			return $voVMovimentoConciliacao;
  		return false;
 	}



public function inicializarVMovimentoConciliacaoCaixinha($nCodConciliacao,$nMovCodigo,$nMovItem,$nCodUnidade,$nTipoCaixa,$dDataConciliacao,$nConsolidado,$nOrdem,$sNome,$nMovValorParcela,$nPesCodigo,$sHistorico,$nContaCodigo,  $nUnidadeDescricao,$nContaCorrenteConc,$nAgenciaContaConc,  $sAgencia,$sConta){
 		$oVMovimentoConciliacaoCaixinha = new VMovimentoConciliacaoCaixinha();

		$oVMovimentoConciliacao->setCodConciliacao($nCodConciliacao);
		$oVMovimentoConciliacao->setMovCodigo($nMovCodigo);
		$oVMovimentoConciliacao->setMovItem($nMovItem);
		$oVMovimentoConciliacao->setCodUnidade($nCodUnidade);
		$oVMovimentoConciliacao->setTipoCaixa($nTipoCaixa);
		$oVMovimentoConciliacao->setDataConciliacaoBanco($dDataConciliacao);
		$oVMovimentoConciliacao->setConsolidado($nConsolidado);
		$oVMovimentoConciliacao->setOrdem($nOrdem);
		$oVMovimentoConciliacao->setNome($sNome);
		$oVMovimentoConciliacao->setMovValorParcelaBanco($nMovValorParcela);
		$oVMovimentoConciliacao->setPesCodigo($nPesCodigo);
		$oVMovimentoConciliacao->setHistorico($sHistorico);
		$oVMovimentoConciliacao->setUnidadeDescricao($nUnidadeDescricao);
		$oVMovimentoConciliacao->setContaCorrenteConc($nContaCorrenteConc);
		$oVMovimentoConciliacao->setAgenciaContaConc($nAgenciaContaConc);
		$oVMovimentoConciliacao->setAgencia($sAgencia);
		$oVMovimentoConciliacao->setConta($sConta);
 		return $oVMovimentoConciliacao;
 	}

	public function recuperarTodosVMovimentoConciliacaoCaixinhaPorDataUnidadeConta($nDataConciliacao,  $nCodConta){
 		$oVMovimentoConciliacaoCaixinha = new VMovimentoConciliacaoCaixinha();
  		$oPersistencia = new Persistencia($oVMovimentoConciliacaoCaixinha);
  		$sTabelas = "v_movimento_conciliacao_caixinha";
  		$sCampos = "*";
  		$sComplemento = "WHERE data_conciliacao='" . $nDataConciliacao . "' AND  conta_corrente_conc = ". $nCodConta ." ORDER BY ordem ASC";
        //echo "Select $sCampos From  $sTabelas $sComplemento";
        //die();
		$voVMovimentoConciliacaoCaixinha = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVMovimentoConciliacaoCaixinha)
  			return $voVMovimentoConciliacaoCaixinha;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto VTransferencia
 	 *
 	 * @return VTransferencia
 	 */
 	public function inicializarVTransferencia($dMovDataEmissao,$nCodTransferencia,$nValor,$nMovCodigoOrigem,$nMovItemOrigem,$nOrigem,$sContaOrigem,$sAgenciaOrigem,$sBancoNomeOrigem,$nCodEmpresaOrigem,$sEmpresaOrigem,$nMovCodigoDestino,$nMovItemDestino,$nDestino,$sContaDestino,$sAgenciaDestino,$sBancoNomeDestino,$nCodEmpresaDestino,$sEmpresaDestino,$sContaContabil,$sCentroNegocio){
 		$oVTransferencia = new VTransferencia();

		$oVTransferencia->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVTransferencia->setValor($nValor);
		$oVTransferencia->setCodTransferencia($nCodTransferencia);
		$oVTransferencia->setMovCodigoOrigem($nMovCodigoOrigem);
		$oVTransferencia->setMovItemOrigem($nMovItemOrigem);
		$oVTransferencia->setOrigem($nOrigem);
		$oVTransferencia->setContaOrigem($sContaOrigem);
		$oVTransferencia->setAgenciaOrigem($sAgenciaOrigem);
		$oVTransferencia->setBancoNomeOrigem($sBancoNomeOrigem);
		$oVTransferencia->setCodEmpresaOrigem($nCodEmpresaOrigem);
		$oVTransferencia->setEmpresaOrigem($sEmpresaOrigem);
		$oVTransferencia->setMovCodigoDestino($nMovCodigoDestino);
		$oVTransferencia->setMovItemDestino($nMovItemDestino);
		$oVTransferencia->setDestino($nDestino);
		$oVTransferencia->setContaDestino($sContaDestino);
		$oVTransferencia->setAgenciaDestino($sAgenciaDestino);
		$oVTransferencia->setBancoNomeDestino($sBancoNomeDestino);
		$oVTransferencia->setCodEmpresaDestino($nCodEmpresaDestino);
		$oVTransferencia->setEmpresaDestino($sEmpresaDestino);
		$oVTransferencia->setContaContabil($sContaContabil);
		$oVTransferencia->setCentroNegocio($sCentroNegocio	);
 		return $oVTransferencia;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VTransferencia
 	 *
 	 * @return boolean
 	 */
 	public function inserirVTransferencia($oVTransferencia){
 		$oPersistencia = new Persistencia($oVTransferencia);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VTransferencia
 	 *
 	 * @return boolean
 	 */
 	public function alterarVTransferencia($oVTransferencia){
 		$oPersistencia = new Persistencia($oVTransferencia);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VTransferencia
 	 *
 	 * @return boolean
 	 */
 	public function excluirVTransferencia(){
 		$oVTransferencia = new VTransferencia();

  		$oPersistencia = new Persistencia($oVTransferencia);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VTransferencia na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVTransferencia(){
 		$oVTransferencia = new VTransferencia();

  		$oPersistencia = new Persistencia($oVTransferencia);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VTransferencia da base de dados
 	 *
 	 * @return VTransferencia
 	 */
 	public function recuperarUmVTransferencia(){
 		$oVTransferencia = new VTransferencia();
  		$oPersistencia = new Persistencia($oVTransferencia);
  		$sTabelas = "v_transferencia";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVTransferencia)
  			return $voVTransferencia[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VTransferencia da base de dados
 	 *
 	 * @return VTransferencia[]
 	 */
 	public function recuperarTodosVTransferencia($nEmpCodigo){
 		$oVTransferencia = new VTransferencia();
  		$oPersistencia = new Persistencia($oVTransferencia);
  		$sTabelas = "v_transferencia";
  		$sCampos = "*";
  		$sComplemento = "WHERE mov_item_origem<>98 and cod_empresa_origem = " . $nEmpCodigo;
		$voVTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
        //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
  		if($voVTransferencia)
  			return $voVTransferencia;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto VPesquisa
 	 *
 	 * @return VPesquisa
 	 */
 	public function inicializarVPesquisa($dMovDataVencto,$sTipo,$sMovContrato,$nMovCodigo,$nMovItem,$dCompetencia,$nMovValorParcela,$nFinalizado,$nMovTipo,$sDescricao,$sTipNome,$nPesgCodigo,$sNome,$sIdentificacao,$dMovDataInclusao,$dMovDataPrev,$nMovValorPago,$dMovDataEmissao,$sMovDocumento,$nEmpCodigo,$nUniCodigo){
 		$oVPesquisa = new VPesquisa();

		$oVPesquisa->setMovDataVenctoBanco($dMovDataVencto);
		$oVPesquisa->setTipo($sTipo);
		$oVPesquisa->setMovContrato($sMovContrato);
		$oVPesquisa->setMovCodigo($nMovCodigo);
		$oVPesquisa->setMovItem($nMovItem);
		$oVPesquisa->setCompetenciaBanco($dCompetencia);
		$oVPesquisa->setnMovValorParcelaBanco($nMovValorParcela);
		$oVPesquisa->setFinalizado($nFinalizado);
		$oVPesquisa->setMovTipo($nMovTipo);
		$oVPesquisa->setDescricao($sDescricao);
		$oVPesquisa->setTipNome($sTipNome);
		$oVPesquisa->setPesgCodigo($nPesgCodigo);
		$oVPesquisa->setNome($sNome);
		$oVPesquisa->setIdentificacao($sIdentificacao);
		$oVPesquisa->setMovDataInclusaoBanco($dMovDataInclusao);
		$oVPesquisa->setMovDataPrevBanco($dMovDataPrev);
		$oVPesquisa->setMovValorPagoBanco($nMovValorPago);
		$oVPesquisa->setMovDataEmissaoBanco($dMovDataEmissao);
		$oVPesquisa->setMovObs($sMovObs);
		$oVPesquisa->setMovDocumento($sMovDocumento);
        $oVPesquisa->setEmpCodigo($nEmpCodigo);
		$oVPesquisa->setUniCodigo($nUniCodigo);

 		return $oVPesquisa;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VPesquisa
 	 *
 	 * @return boolean
 	 */
 	public function inserirVPesquisa($oVPesquisa){
 		$oPersistencia = new Persistencia($oVPesquisa);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VPesquisa
 	 *
 	 * @return boolean
 	 */
 	public function alterarVPesquisa($oVPesquisa){
 		$oPersistencia = new Persistencia($oVPesquisa);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VPesquisa
 	 *
 	 * @return boolean
 	 */
 	public function excluirVPesquisa(){
 		$oVPesquisa = new VPesquisa();

  		$oPersistencia = new Persistencia($oVPesquisa);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VPesquisa na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVPesquisa(){
 		$oVPesquisa = new VPesquisa();

  		$oPersistencia = new Persistencia($oVPesquisa);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VPesquisa da base de dados
 	 *
 	 * @return VPesquisa
 	 */
 	public function recuperarUmVPesquisa(){
 		$oVPesquisa = new VPesquisa();
  		$oPersistencia = new Persistencia($oVPesquisa);
  		$sTabelas = "v_pesquisa";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVPesquisa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisa)
  			return $voVPesquisa[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPesquisa da base de dados
 	 *
 	 * @return VPesquisa[]
 	 */
 	public function recuperarTodosVPesquisa(){
 		$oVPesquisa = new VPesquisa();
  		$oPersistencia = new Persistencia($oVPesquisa);
  		$sTabelas = "v_pesquisa";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVPesquisa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVPesquisa)
  			return $voVPesquisa;
  		return false;
 	}

	 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VPesquisa da base de dados
 	 *
 	 * @return VPesquisa[]
 	 */
 	public function recuperarTodosVPesquisaPorComplemento($sComplemento){
 		$oVPesquisa = new VPesquisa();
  		$oPersistencia = new Persistencia($oVPesquisa);
  		$sTabelas = "v_pesquisa";
  		$sCampos = "*";

		//echo "SELECT $sCampos FROM $sTabelas $sComplemento" ;
		$voVPesquisa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);

		if($voVPesquisa)
  			return $voVPesquisa;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto VAporteItem
 	 *
 	 * @return VAporteItem
 	 */
 	public function inicializarVAporteItem($nCodSolicitacaoAporte,$dDataSolicitacao,$sDescricaoAporte,$sUnidade,$nUniCodigo,$nMovCodigo,$nMovItem,$sFA,$sNome,$nEmpCodigo,$sEmpFantasia,$sMovObs,$sTipoDocumento,$sMovDocumento,$sParcela,$dCompetencia,$dMovDataVencto,$sFormaPagamento,$nMovValor,$nMovJuros,$nDesconto,$nMovValorLiquido){
 		$oVAporteItem = new VAporteItem();

		$oVAporteItem->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
		$oVAporteItem->setDataSolicitacaoBanco($dDataSolicitacao);
		$oVAporteItem->setDescricaoAporte($sDescricaoAporte);
        $oVAporteItem->setUnidade($sUnidade);
        $oVAporteItem->setUniCodigo($nUniCodigo);
        $oVAporteItem->setMovCodigo($nMovCodigo);
		$oVAporteItem->setMovItem($nMovItem);
        $oVAporteItem->setFA($sFA);
		$oVAporteItem->setNome($sNome);
		$oVAporteItem->setEmpCodigo($nEmpCodigo);
		$oVAporteItem->setEmpFantasia($sEmpFantasia);
		$oVAporteItem->setMovObs($sMovObs);
		$oVAporteItem->setTipoDocumento($sTipoDocumento);
		$oVAporteItem->setMovDocumento($sMovDocumento);
		$oVAporteItem->setParcela($sParcela);
		$oVAporteItem->setCompetenciaBanco($dCompetencia);
		$oVAporteItem->setMovDataVenctoBanco($dMovDataVencto);
		$oVAporteItem->setFormaPagamento($sFormaPagamento);
		$oVAporteItem->setMovValorBanco($nMovValor);
		$oVAporteItem->setMovJurosBanco($nMovJuros);
		$oVAporteItem->setDescontoBanco($nDesconto);
		$oVAporteItem->setMovValorLiquidoBanco($nMovValorLiquido);
 		return $oVAporteItem;
 	}



 	/**
 	 *
 	 * Método para recuperar um objeto VAporteItem da base de dados
 	 *
 	 * @return VAporteItem
 	 */
 	public function recuperarUmVAporteItem(){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sTabelas = "v_aporte_item";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAporteItem)
  			return $voVAporteItem[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VAporteItem da base de dados
 	 *
 	 * @return VAporteItem[]
 	 */
 	public function recuperarTodosVAporteItem(){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sTabelas = "v_aporte_item";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAporteItem)
  			return $voVAporteItem;
  		return false;
 	}


	/**
 	 *
 	 * Método para recuperar um vetor de objetos VAporteItem da base de dados
 	 *
 	 * @return VAporteItem[]
 	 */
 	public function recuperarTodosVAporteItemPorAporte($nCodAporte){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sTabelas = "v_aporte_item";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_solicitacao_aporte=".$nCodAporte;
  		$voVAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAporteItem)
  			return $voVAporteItem;
  		return false;
 	}


	 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VAporteItem da base de dados
 	 *
 	 * @return VAporteItem[]
 	 */
 	public function recuperarTodosVAporteItemPorSolicitacaoAporteItem($nCodSolicitacaoAporte){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sTabelas = "v_aporte_item";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_solicitacao_aporte =" .$nCodSolicitacaoAporte;
  		    //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
		$voVAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAporteItem)
  			return $voVAporteItem;
  		return false;
 	}
    public function recuperarTodosVAporteItemPorSolicitacaoAporteItemPROCEDURE($nCodSolicitacaoAporte){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sProcedure = "sp_aporte_item";
  		$sParamentros = $nCodSolicitacaoAporte;
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
		$voVAporteItem = $oPersistencia->consultarProcedureLista($sProcedure,$sParamentros);
  		if($voVAporteItem)
  			return $voVAporteItem;
  		return false;
 	}
    public function recuperarTodosVAporteItemPorSolicitacaoAporteItemPROCEDUREEstorno($nCodSolicitacaoAporte){
 		$oVAporteItem = new VAporteItem();
  		$oPersistencia = new Persistencia($oVAporteItem);
  		$sProcedure = "sp_aporte_item_estorno";
  		$sParamentros = $nCodSolicitacaoAporte;
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
		$voVAporteItem = $oPersistencia->consultarProcedureLista($sProcedure,$sParamentros);
  		if($voVAporteItem)
  			return $voVAporteItem;
  		return false;
 	}

		/**
 	 *
 	 * Método para inicializar um Objeto VReconciliacao
 	 *
 	 * @return VReconciliacao
 	 */
 	public function inicializarVReconciliacao($sFa,$nMovValorPagar,$nFpgCodigo,$sFormaDePagamento,$nTipDocCodigo,$sTipoDescricao,$dData,$nEmpCodigo,$nMovTipo,$sMovimentoTipo,$sMovDocumento,$nConCodigo,$sContaDescricao,$nUniCodigo,$sUnidadeDescricao,$nCenCodigo,$sCentroDescricao,$nNegCodigo,$sUnidadeNegocioDescricao,$sCcrConta,$sEmpresa,$sMovObs){
 		$oVReconciliacao = new VReconciliacao();

		$oVReconciliacao->setFa($sFa);
		$oVReconciliacao->setMovValorPagarBanco($nMovValorPagar);
		$oVReconciliacao->setFpgCodigo($nFpgCodigo);
		$oVReconciliacao->setFormaDePagamento($sFormaDePagamento);
		$oVReconciliacao->setTipDocCodigo($nTipDocCodigo);
		$oVReconciliacao->setTipoDescricao($sTipoDescricao);
		$oVReconciliacao->setDataConciliacaoBanco($dData);
		$oVReconciliacao->setEmpCodigo($nEmpCodigo);
		$oVReconciliacao->setMovTipo($nMovTipo);
		$oVReconciliacao->setMovimentoTipo($sMovimentoTipo);
		$oVReconciliacao->setMovDocumento($sMovDocumento);
		$oVReconciliacao->setConCodigo($nConCodigo);
		$oVReconciliacao->setContaDescricao($sContaDescricao);
		$oVReconciliacao->setUniCodigo($nUniCodigo);
		$oVReconciliacao->setUnidadeDescricao($sUnidadeDescricao);
		$oVReconciliacao->setCenCodigo($nCenCodigo);
		$oVReconciliacao->setCentroDescricao($sCentroDescricao);
		$oVReconciliacao->setNegCodigo($nNegCodigo);
		$oVReconciliacao->setUnidadeNegocioDescricao($sUnidadeNegocioDescricao);
		$oVReconciliacao->setCcrConta($sCcrConta);
		$oVReconciliacao->setEmpresa($sEmpresa);
		$oVReconciliacao->setMovObs($sMovObs);
 		return $oVReconciliacao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VReconciliacao
 	 *
 	 * @return boolean
 	 */
 	public function inserirVReconciliacao($oVReconciliacao){
 		$oPersistencia = new Persistencia($oVReconciliacao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VReconciliacao
 	 *
 	 * @return boolean
 	 */
 	public function alterarVReconciliacao($oVReconciliacao){
 		$oPersistencia = new Persistencia($oVReconciliacao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VReconciliacao
 	 *
 	 * @return boolean
 	 */
 	public function excluirVReconciliacao(){
 		$oVReconciliacao = new VReconciliacao();

  		$oPersistencia = new Persistencia($oVReconciliacao);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VReconciliacao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVReconciliacao(){
 		$oVReconciliacao = new VReconciliacao();

  		$oPersistencia = new Persistencia($oVReconciliacao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VReconciliacao da base de dados
 	 *
 	 * @return VReconciliacao
 	 */
 	public function recuperarUmVReconciliacao(){
 		$oVReconciliacao = new VReconciliacao();
  		$oPersistencia = new Persistencia($oVReconciliacao);
  		$sTabelas = "v_reconciliacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVReconciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVReconciliacao)
  			return $voVReconciliacao[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VReconciliacao da base de dados
 	 *
 	 * @return VReconciliacao[]
 	 */
 	public function recuperarTodosVReconciliacao(){
 		$oVReconciliacao = new VReconciliacao();
  		$oPersistencia = new Persistencia($oVReconciliacao);
  		$sTabelas = "v_reconciliacao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVReconciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVReconciliacao)
  			return $voVReconciliacao;
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VReconciliacao da base de dados
 	 *
 	 * @return VReconciliacao[]
 	 */
 	public function recuperarTodosVReconciliacaoTotais($nCodUnidade, $nCcrCodigo,$dDataInicial, $dDataFinal){
 		$oVReconciliacao = new VReconciliacao();
  		$oPersistencia = new Persistencia($oVReconciliacao);
  		$sTabelas = "v_reconciliacao";
  		//$sCampos = "DISTINCT (select sum(mov_valor_pagar) from v_reconciliacao where mov_tipo = 188) as mov_valor_pagar,
		//		   (select sum(mov_valor_pagar) from v_reconciliacao where mov_tipo = 189) as mov_tipo";
		$sCampos = "DISTINCT (select sum(f_valor_parcela_liquido(mny_movimento_item.mov_codigo,mny_movimento_item.mov_item)) as valor_parcela_liquido
							from mny_conciliacao
							INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo AND mny_movimento_item.mov_item = mny_conciliacao.mov_item
							INNER JOIN mny_movimento ON mny_movimento.mov_codigo = mny_conciliacao.mov_codigo
							where
							cod_unidade= ".$nCodUnidade." AND cod_conta_corrente= ".$nCcrCodigo." AND data_conciliacao BETWEEN '".$dDataInicial."' AND '".$dDataFinal."'
							AND consolidado= 1 ) as mov_valor_pagar,
							((select
							sum(f_valor_parcela_liquido(mny_movimento_item.mov_codigo,mny_movimento_item.mov_item)) as valor_parcela_liquido
							from mny_conciliacao
							INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo AND mny_movimento_item.mov_item = mny_conciliacao.mov_item
							INNER JOIN mny_movimento ON mny_movimento.mov_codigo = mny_conciliacao.mov_codigo
							where
							cod_unidade= ".$nCodUnidade." AND cod_conta_corrente= ".$nCcrCodigo." AND data_conciliacao BETWEEN '".$dDataInicial."' AND '".$dDataFinal."'
							AND consolidado= 1 AND mny_movimento.mov_tipo= 188)-
							(select
							sum(f_valor_parcela_liquido(mny_movimento_item.mov_codigo,mny_movimento_item.mov_item)) as valor_parcela_liquido
							from mny_conciliacao
							INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo AND mny_movimento_item.mov_item = mny_conciliacao.mov_item
							INNER JOIN mny_movimento ON mny_movimento.mov_codigo = mny_conciliacao.mov_codigo
							where
							cod_unidade= ".$nCodUnidade." AND cod_conta_corrente= ".$nCcrCodigo." AND data_conciliacao BETWEEN '".$dDataInicial."' AND '".$dDataFinal."'
							AND consolidado= 1 AND mny_movimento.mov_tipo= 189)) as mov_tipo";
  		$sComplemento = "";
		//echo "SELECT $sCampos FROM $sTabelas ";
  		//die();
		$voVReconciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		if($voVReconciliacao)
  			return $voVReconciliacao[0];
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VReconciliacao da base de dados
 	 *
 	 * @return VReconciliacao[]
 	 */
 	public function recuperarTodosVReconciliacaoPorUnidadePeriodo($nUniCodigo, $nCcrCodigo, $dData1, $dData2){
 		$oVReconciliacao = new VReconciliacao();
  		$oPersistencia = new Persistencia($oVReconciliacao);
  		$sTabelas = "v_reconciliacao";
  		$sCampos = "*";
  		//$sComplemento = "where MONTH(data_conciliacao) = ". $dMesAno[1]." and YEAR(data_conciliacao)=". $dMesAno[2];
  		$sComplemento = "WHERE uni_codigo =". $nUniCodigo ." AND cod_conta_corrente = " . $nCcrCodigo . " AND data BETWEEN  '".$dData1."' AND '".$dData2."' ORDER BY data ASC ";
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento ";
		//die();
		$voVReconciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVReconciliacao)
  			return $voVReconciliacao;
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um vetor de objetos VReconciliacao da base de dados
 	 *
 	 * @return VReconciliacao[]
 	 */
 	public function recuperarTodosVReconciliacaoPorUnidadePeriodoTotais($nUniCodigo, $nCcrCodigo, $dData1, $dData2){
 		$oVReconciliacao = new VReconciliacao();
  		$oPersistencia = new Persistencia($oVReconciliacao);
  		$sTabelas = "v_reconciliacao";
  		$sCampos = "DISTINCT (select sum(mov_valor_pagar)  from v_reconciliacao WHERE uni_codigo =". $nUniCodigo ." AND cod_conta_corrente = " . $nCcrCodigo . " AND data BETWEEN  '".$dData1."' AND '".$dData2."' and  mov_tipo = 189)as mov_tipo,
		            (select sum(mov_valor_pagar)  from v_reconciliacao WHERE uni_codigo =". $nUniCodigo ." AND cod_conta_corrente = " . $nCcrCodigo . " AND data BETWEEN  '".$dData1."' AND '".$dData2."' and  mov_tipo = 188)as emp_codigo,
					(select valor_inicial FROM mny_saldo_diario WHERE cod_unidade =". $nUniCodigo ." AND ccr_codigo = " . $nCcrCodigo . " AND data= '".$dData1."' ) as con_codigo,
					/*
					 ((select sum(mov_valor_pagar) from v_reconciliacao WHERE uni_codigo =". $nUniCodigo ." AND cod_conta_corrente = " . $nCcrCodigo . " AND data BETWEEN  '".$dData1."' AND '".$dData2."' and  mov_tipo = 188)
		            -(select sum(mov_valor_pagar) from v_reconciliacao WHERE uni_codigo =". $nUniCodigo ." AND cod_conta_corrente = " . $nCcrCodigo . " AND data BETWEEN  '".$dData1."' AND '".$dData2."' and  mov_tipo = 189)) as mov_valor_pagar
					*/
					(select valor_final FROM mny_saldo_diario WHERE cod_unidade =212 AND ccr_codigo = 261 AND data= '2016-03-24' ) as mov_valor_pagar
					";

		//$sComplemento = "where MONTH(data_conciliacao) = ". $dMesAno[1]." and YEAR(data_conciliacao)=". $dMesAno[2];
  		$sComplemento = "";
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento ";
		//die();
		$voVReconciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVReconciliacao)
  			return $voVReconciliacao[0];
  		return false;
 	}

		/**
 	 *
 	 * Método para inicializar um Objeto VConciliacaoAplicacao
 	 *
 	 * @return VConciliacaoAplicacao
 	 */
 	public function inicializarVConciliacaoAplicacao($sOrdem,$dDataConciliacao,$nConta){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();

		$oVConciliacaoAplicacao->setOrdem($sOrdem);
		$oVConciliacaoAplicacao->setDataConciliacaoBanco($dDataConciliacao);
		$oVConciliacaoAplicacao->setConta($nConta);
 		return $oVConciliacaoAplicacao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VConciliacaoAplicacao
 	 *
 	 * @return boolean
 	 */
 	public function inserirVConciliacaoAplicacao($oVConciliacaoAplicacao){
 		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VConciliacaoAplicacao
 	 *
 	 * @return boolean
 	 */
 	public function alterarVConciliacaoAplicacao($oVConciliacaoAplicacao){
 		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VConciliacaoAplicacao
 	 *
 	 * @return boolean
 	 */
 	public function excluirVConciliacaoAplicacao(){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();

  		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VConciliacaoAplicacao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVConciliacaoAplicacao(){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();

  		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VConciliacaoAplicacao da base de dados
 	 *
 	 * @return VConciliacaoAplicacao
 	 */
 	public function recuperarUmVConciliacaoAplicacao(){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();
  		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		$sTabelas = "v_conciliacao_aplicacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVConciliacaoAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVConciliacaoAplicacao)
  			return $voVConciliacaoAplicacao[0];
  		return false;
 	}
	/**
 	 *
 	 * Método para recuperar um objeto VConciliacaoAplicacao da base de dados
 	 *
 	 * @return VConciliacaoAplicacao
 	 */
 	public function recuperarTodosVConciliacaoAplicacaoPorDataConta($nData, $nConta){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();
  		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		$sTabelas = "v_conciliacao_aplicacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE  data_conciliacao = '". $nData ."' AND conta=" . $nConta;
  		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
		//die();
		$voVConciliacaoAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		if($voVConciliacaoAplicacao)
  			return $voVConciliacaoAplicacao;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VConciliacaoAplicacao da base de dados
 	 *
 	 * @return VConciliacaoAplicacao[]
 	 */
 	public function recuperarTodosVConciliacaoAplicacao(){
 		$oVConciliacaoAplicacao = new VConciliacaoAplicacao();
  		$oPersistencia = new Persistencia($oVConciliacaoAplicacao);
  		$sTabelas = "v_conciliacao_aplicacao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVConciliacaoAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVConciliacaoAplicacao)
  			return $voVConciliacaoAplicacao;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto VContasPagar
 	 *
 	 * @return VContasPagar
 	 */
 	public function inicializarVContasPagar($nTipAceCodigo,$sEmpFantasia,$nEmpCodigo,$sCentroCusto,$sContaContabil,$sCusto,$nTipDocCodigo,$dCompetencia,$nMovItem,$nTotalItens,$nParcela,$dVencimento,$nFpgCodigo,$nValorPrincipal,$nJuros,$nRetencaoDesconto,$nMovCodigo,$sNumeroDocumento,$sHistorico,$nUniCodigo,$sPessoa,$sTipoDoc,$sFormaPagamento,$sUnidade){
 		$oVContasPagar = new VContasPagar();

		$oVContasPagar->setTipAceCodigo($nTipAceCodigo);
		$oVContasPagar->setEmpFantasia($sEmpFantasia);
		$oVContasPagar->setEmpCodigo($nEmpCodigo);
		$oVContasPagar->setCentroCusto($sCentroCusto);
		$oVContasPagar->setContaContabil($sContaContabil);
		$oVContasPagar->setCusto($sCusto);
		$oVContasPagar->setTipDocCodigo($nTipDocCodigo);
		$oVContasPagar->setCompetenciaBanco($dCompetencia);
		$oVContasPagar->setMovItem($nMovItem);
		$oVContasPagar->setTotalItens($nTotalItens);
		$oVContasPagar->setParcela($nParcela);
		$oVContasPagar->setVencimentoBanco($dVencimento);
		$oVContasPagar->setFpgCodigo($nFpgCodigo);
		$oVContasPagar->setValorPrincipalBanco($nValorPrincipal);
		$oVContasPagar->setJurosBanco($nJuros);
		$oVContasPagar->setRetencaoDescontoBanco($nRetencaoDesconto);
		$oVContasPagar->setMovCodigo($nMovCodigo);
		$oVContasPagar->setNumeroDocumento($sNumeroDocumento);
		$oVContasPagar->setHistorico($sHistorico);
		$oVContasPagar->setUniCodigo($nUniCodigo);
		$oVContasPagar->setPessoa($sPessoa);
		$oVContasPagar->setTipoDoc($sTipoDoc);
		$oVContasPagar->setFormaPagamento($sFormaPagamento);
		$oVContasPagar->setUnidade($sUnidade);
		return $oVContasPagar;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VContasPagar
 	 *
 	 * @return boolean
 	 */
 	public function inserirVContasPagar($oVContasPagar){
 		$oPersistencia = new Persistencia($oVContasPagar);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VContasPagar
 	 *
 	 * @return boolean
 	 */
 	public function alterarVContasPagar($oVContasPagar){
 		$oPersistencia = new Persistencia($oVContasPagar);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VContasPagar
 	 *
 	 * @return boolean
 	 */
 	public function excluirVContasPagar(){
 		$oVContasPagar = new VContasPagar();

  		$oPersistencia = new Persistencia($oVContasPagar);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VContasPagar na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVContasPagar(){
 		$oVContasPagar = new VContasPagar();

  		$oPersistencia = new Persistencia($oVContasPagar);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto VContasPagar da base de dados
 	 *
 	 * @return VContasPagar
 	 */
 	public function recuperarUmVContasPagar(){
 		$oVContasPagar = new VContasPagar();
  		$oPersistencia = new Persistencia($oVContasPagar);
  		$sTabelas = "v_contas_pagar";
  		$sCampos = "*";
  		$sComplemento = " WHERE ";
  		$voVContasPagar = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContasPagar)
  			return $voVContasPagar[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VContasPagar da base de dados
 	 *
 	 * @return VContasPagar[]
 	 */
 	public function recuperarTodosVContasPagar(){
 		$oVContasPagar = new VContasPagar();
  		$oPersistencia = new Persistencia($oVContasPagar);
  		$sTabelas = "v_contas_pagar";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVContasPagar = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVContasPagar)
  			return $voVContasPagar;
  		return false;
 	}

		/**
 	 *
 	 * Método para recuperar um vetor de objetos VContasPagar da base de dados
 	 *
 	 * @return VContasPagar[]
 	 */
 	public function recuperarTodosVContasPagarPorPeriodoUnidade($dDataInicial, $dDataFinal, $nUnidade, $nEmpCodigo){
 		$voVContasPagar = new VContasPagar();
  		$oPersistencia = new Persistencia($voVContasPagar);
  		$sTabelas = "v_contas_pagar";
		$sCampos ="*";
		if($nUnidade){
  			$sComplemento = "WHERE vencimento BETWEEN  '".$dDataInicial."' AND '".$dDataFinal."' AND emp_codigo=".$nEmpCodigo." AND uni_codigo =". $nUnidade;
		}else{
  			$sComplemento = "WHERE vencimento BETWEEN '".$dDataInicial."' AND '".$dDataFinal."' AND emp_codigo=".$nEmpCodigo;

		}
  		//echo "SELECT $sCampos FROM $sTabelas  $sComplemento";
		//die();
		$voVContasPagar = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		//print_r($voVContasPagar);
		//die('bd');
		if($voVContasPagar)
  			return $voVContasPagar;
  		return false;
 	}

    	/**
 	 *
 	 * Método para inicializar um Objeto VAcessoUsuarioUnidade
 	 *
 	 * @return VAcessoUsuarioUnidade
 	 */
 	public function inicializarVAcessoUsuarioUnidade($nCodUsuario,$sNome,$sLogin,$nGrupo,$nGrupoUsuario,$sUnidade,$nPlanoContasCodigo,$nCodigo){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();

		$oVAcessoUsuarioUnidade->setCodUsuario($nCodUsuario);
		$oVAcessoUsuarioUnidade->setNome($sNome);
		$oVAcessoUsuarioUnidade->setLogin($sLogin);
        $oVAcessoUsuarioUnidade->setGrupo($nGrupo);
		$oVAcessoUsuarioUnidade->setCodGrupoUsuario($nGrupoUsuario);
		$oVAcessoUsuarioUnidade->setUnidade($sUnidade);
		$oVAcessoUsuarioUnidade->setPlanoContasCodigo($nPlanoContasCodigo);
		$oVAcessoUsuarioUnidade->setCodigo($nCodigo);

 		return $oVAcessoUsuarioUnidade;
 	}



 	/**
 	 *
 	 * Método para recuperar um objeto VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade
 	 */
 	public function recuperarUmVAcessoUsuarioUnidade($nCodUsuario,$nCodGrupoUsuario,$nCodUnidade){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);
  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_usuario=$nCodUsuario and cod_unidade=$nCodUnidade and cod_grupo_usuario=$nCodGrupoGusuario";
  		$voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosVAcessoUsuarioUnidade(){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);
  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade;
  		return false;
 	}
    /**
 	 *
 	 * Método para recuperar um vetor de objetos VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosVAcessoUsuarioUnidadePorUsuarioUnidade($nCodUsuario){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);

  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "DISTINCT cod_usuario,login,unidade,plano_contas_codigo,codigo";
  		$sComplemento = "WHERE cod_usuario=".$nCodUsuario;

        $voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade;
  		return false;
 	}
    /**
 	 *
 	 * Método para recuperar um vetor de objetos VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosVAcessoUsuarioUnidadePorUsuario($nCodUsuario){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);

  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "DISTINCT cod_usuario,login,unidade,plano_contas_codigo,codigo,codigo,cod_grupo_usuario,grupo ";
  		$sComplemento = "WHERE cod_usuario=".$nCodUsuario;

        $voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade;
  		return false;
 	}
    /**
 	 *
 	 * Método para recuperar um vetor de objetos VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosVAcessoUsuarioUnidadePorUsuarioDISTINCT($nCodUsuario){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);

  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "DISTINCT cod_usuario,login,unidade,plano_contas_codigo,codigo ";
  		$sComplemento = "WHERE cod_usuario=".$nCodUsuario;

        $voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade;
  		return false;
 	}
    /**
 	 *
 	 * Método para recuperar um vetor de objetos VAcessoUsuarioUnidade da base de dados
 	 *
 	 * @return VAcessoUsuarioUnidade[]
 	 */
 	public function recuperaTodosVAcessoUsuarioUnidadePorPessoaPorUnidade($nCodUsuario,$nCodUnidade){
 		$oVAcessoUsuarioUnidade = new VAcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oVAcessoUsuarioUnidade);
  		$sTabelas = "v_acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = "Where cod_usuario=".$nCodUsuario." And plano_contas_codigo=".$nCodUnidade;
  	//echo "Select $sCampos From $sTabelas $sComplemento";
        $voVAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVAcessoUsuarioUnidade)
  			return $voVAcessoUsuarioUnidade;
  		return false;
 	}

    /**
 	 *
 	 * Método para inicializar um Objeto VRelatorioPorEmpresa
 	 *
 	 * @return VRelatorioPorEmpresa
 	 */
 	public function inicializarVRelatorioPorEmpresa($sFa,$sCredor,$sEmpresa,$sUnidade,$sDescricaoDespesa,$sAporte,$sMovDocumento,$sParcela,$sCompetencia,$sVencimento,$sFormaPagamento,$nMovValorPagar,$nAcrescimo,$nMovJuros,$nDesconto,$sReprogramado,$sPago,$sPagar){
 		$oVRelatorioPorEmpresa = new VRelatorioPorEmpresa();

		$oVRelatorioPorEmpresa->setFa($sFa);
		$oVRelatorioPorEmpresa->setCredor($sCredor);
		$oVRelatorioPorEmpresa->setEmpresa($sEmpresa);
		$oVRelatorioPorEmpresa->setUnidade($sUnidade);
		$oVRelatorioPorEmpresa->setDescricaoDespesa($sDescricaoDespesa);
		$oVRelatorioPorEmpresa->setAporte($sAporte);
		$oVRelatorioPorEmpresa->setMovDocumento($sMovDocumento);
		$oVRelatorioPorEmpresa->setParcela($sParcela);
		$oVRelatorioPorEmpresa->setCompetencia($sCompetencia);
		$oVRelatorioPorEmpresa->setVencimento($sVencimento);
		$oVRelatorioPorEmpresa->setFormaPagamento($sFormaPagamento);
		$oVRelatorioPorEmpresa->setMovValorPagarBanco($nMovValorPagar);
		$oVRelatorioPorEmpresa->setAcrescimoBanco($nAcrescimo);
		$oVRelatorioPorEmpresa->setMovJurosBanco($nMovJuros);
		$oVRelatorioPorEmpresa->setDescontoBanco($nDesconto);
		$oVRelatorioPorEmpresa->setReprogramado($sReprogramado);
		$oVRelatorioPorEmpresa->setPago($sPago);
		$oVRelatorioPorEmpresa->setPagar($sPagar);
 		return $oVRelatorioPorEmpresa;
 	}



 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VRelatorioPorEmpresa da base de dados
 	 *
 	 * @return VRelatorioPorEmpresa[]
 	 */
 	public function recuperarTodosVRelatorioPorEmpresa(){
 		$oVRelatorioPorEmpresa = new VRelatorioPorEmpresa();
  		$oPersistencia = new Persistencia($oVRelatorioPorEmpresa);
  		$sTabelas = "v_relatorio_por_empresa";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVRelatorioPorEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVRelatorioPorEmpresa)
  			return $voVRelatorioPorEmpresa;
  		return false;
 	}

    /**
 	 *
 	 * Método para recuperar um vetor de objetos VRelatorioPorEmpresa da base de dados
 	 *
 	 * @return VRelatorioPorEmpresa[]
 	 */
 	public function recuperarTodosVRelatorioPorEmpresaPorComplemento($sComplemento){
 		$oVRelatorioPorEmpresa = new VRelatorioPorEmpresa();
  		$oPersistencia = new Persistencia($oVRelatorioPorEmpresa);
  		$sTabelas = "v_relatorio_por_empresa";
  		$sCampos = "*";
        //echo "Select * From $sTabelas $sComplemento";
        //die();
  		$voVRelatorioPorEmpresa = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
  		if($voVRelatorioPorEmpresa)
  			return $voVRelatorioPorEmpresa;
  		return false;
 	}

    /**
 	 *
 	 * Método para inicializar um Objeto VSupriTramitacaoScm
 	 *
 	 * @return VSupriTramitacaoScm
 	 */
 	public function inicializarVSupriTramitacaoScm($nCodProtocoloScm,$nCodScm,$sTramDescricao,$sUsuario,$dData,$nAtivo,$nCodDe,$sDe,$nCodPara,$sPara,$nDeCodTipoProtocolo,$nParaCodTipoProtocolo){
 		$oVSupriTramitacaoScm = new VSupriTramitacaoScm();

		$oVSupriTramitacaoScm->setCodProtocoloScm($nCodProtocoloScm);
		$oVSupriTramitacaoScm->setCodScm($nCodScm);
		$oVSupriTramitacaoScm->setTramDescricao($sTramDescricao);
		$oVSupriTramitacaoScm->setUsuario($sUsuario);
		$oVSupriTramitacaoScm->setDataBanco($dData);
		$oVSupriTramitacaoScm->setAtivo($nAtivo);
		$oVSupriTramitacaoScm->setCodDe($nCodDe);
		$oVSupriTramitacaoScm->setDe($sDe);
		$oVSupriTramitacaoScm->setCodPara($nCodPara);
		$oVSupriTramitacaoScm->setPara($sPara);
		$oVSupriTramitacaoScm->setDeCodTipoProtocolo($nDeCodTipoProtocolo);
		$oVSupriTramitacaoScm->setParaCodTipoProtocolo($nParaCodTipoProtocolo);
 		return $oVSupriTramitacaoScm;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto VSupriTramitacaoScm
 	 *
 	 * @return boolean
 	 */
 	public function inserirVSupriTramitacaoScm($oVSupriTramitacaoScm){
 		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto VSupriTramitacaoScm
 	 *
 	 * @return boolean
 	 */
 	public function alterarVSupriTramitacaoScm($oVSupriTramitacaoScm){
 		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto VSupriTramitacaoScm
 	 *
 	 * @return boolean
 	 */
 	public function excluirVSupriTramitacaoScm(){
 		$oVSupriTramitacaoScm = new VSupriTramitacaoScm();

  		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto VSupriTramitacaoScm na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteVSupriTramitacaoScm(){
 		$oVSupriTramitacaoScm = new VSupriTramitacaoScm();

  		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar o ultimo objeto VSupriTramitacaoScm lançado na base de dados
 	 *
 	 * @return VSupriTramitacaoScm
 	 */
 	public function recuperarUmVSupriTramitacaoScm($nCodScm){
 		$oVSupriTramitacaoScm = new VSupriTramitacaoScm();
  		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
  		$sTabelas = "v_supri_tramitacao_scm";
  		$sCampos = "*";
  		$sComplemento = "Where cod_scm = ".$nCodScm." ORDER BY data DESC LIMIT 1";
  		$voVSupriTramitacaoScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVSupriTramitacaoScm)
  			return $voVSupriTramitacaoScm[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos VSupriTramitacaoScm da base de dados
 	 *
 	 * @return VSupriTramitacaoScm[]
 	 */
 	public function recuperarTodosVSupriTramitacaoScm(){
 		$oVSupriTramitacaoScm = new VSupriTramitacaoScm();
  		$oPersistencia = new Persistencia($oVSupriTramitacaoScm);
  		$sTabelas = "v_supri_tramitacao_scm";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voVSupriTramitacaoScm = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voVSupriTramitacaoScm)
  			return $voVSupriTramitacaoScm;
  		return false;
 	}

}
?>
