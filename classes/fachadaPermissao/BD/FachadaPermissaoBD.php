<?

class FachadaPermissaoBD {
	/**
 	 *
 	 * Método para inicializar um Objeto AcessoGrupoUsuario
 	 *
 	 * @return AcessoGrupoUsuario
 	 */
 	public function inicializarAcessoGrupoUsuario($nCodGrupoUsuario,$sDescricao,$nAtivo){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();

		$oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
		$oAcessoGrupoUsuario->setDescricao($sDescricao);
		$oAcessoGrupoUsuario->setAtivo($nAtivo);

 		return $oAcessoGrupoUsuario;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoGrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoGrupoUsuario($oAcessoGrupoUsuario){
 		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoGrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoGrupoUsuario($oAcessoGrupoUsuario){
 		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoGrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoGrupoUsuario($nCodGrupoUsuario){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();

		$oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoGrupoUsuario na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoGrupoUsuario($nCodGrupoUsuario){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();

		$oAcessoGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoGrupoUsuario da base de dados
 	 *
 	 * @return AcessoGrupoUsuario
 	 */
 	public function recuperarUmAcessoGrupoUsuario($nCodGrupoUsuario){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
  		$voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoGrupoUsuario)
  			return $voAcessoGrupoUsuario[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoGrupoUsuario da base de dados
 	 *
 	 * @return AcessoGrupoUsuario[]
 	 */
 	public function recuperarTodosAcessoGrupoUsuario(){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1 ";
  		$voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoGrupoUsuario)
  			return $voAcessoGrupoUsuario;
  		return false;
 	}
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoGrupoUsuario da base de dados
 	 *
 	 * @return AcessoGrupoUsuario[]
 	 */
 	public function recuperarTodosAcessoGrupoUsuarioPorTramitacao(){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = " Distinct acesso_grupo_usuario.*";
  		$sComplemento = "Inner Join sys_protocolo_encaminhamento On sys_protocolo_encaminhamento.de = acesso_grupo_usuario.cod_grupo_usuario
                          WHERE acesso_grupo_usuario.ativo = 1";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoGrupoUsuario)
  			return $voAcessoGrupoUsuario;
  		return false;
 	}
  	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoGrupoUsuario da base de dados
 	 *
 	 * @return AcessoGrupoUsuario[]
 	 */
 	public function recuperarTodosAcessoGrupoTramitacao(){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = " cod_grupo_usuario";
  		$sComplemento = "WHERE cod_grupo_usuario in (select distinct de from sys_protocolo_encaminhamento) and acesso_grupo_usuario.ativo = 1";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoGrupoUsuario)
  			return $voAcessoGrupoUsuario;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoGrupoUsuario da base de dados
 	 *
 	 * @return AcessoGrupoUsuario[]
 	 */
 	public function recuperarTodosAcessoGrupoSemTramitacao(){
 		$oAcessoGrupoUsuario = new AcessoGrupoUsuario();
  		$oPersistencia = new Persistencia($oAcessoGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = " cod_grupo_usuario";
  		$sComplemento = "WHERE cod_grupo_usuario not in (select distinct de from sys_protocolo_encaminhamento) and acesso_grupo_usuario.ativo = 1";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voAcessoGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoGrupoUsuario)
  			return $voAcessoGrupoUsuario;
  		return false;
 	}


	/**
 	 *
 	 * Método para inicializar um Objeto AcessoLog
 	 *
 	 * @return AcessoLog
 	 */
 	public function inicializarAcessoLog($nCodAcesso,$sUsuario,$sTabela,$sOperacao,$dDataLog,$sDescricao,$sSqlDescricao,$nAtivo){
 		$oAcessoLog = new AcessoLog();

		$oAcessoLog->setCodAcesso($nCodAcesso);
		$oAcessoLog->setUsuario($sUsuario);
		$oAcessoLog->setTabela($sTabela);
		$oAcessoLog->setOperacao($sOperacao);
		$oAcessoLog->setDataLog($dDataLog);
		$oAcessoLog->setDescricao($sDescricao);
		$oAcessoLog->setSqlDescricao($sSqlDescricao);
		$oAcessoLog->setAtivo($nAtivo);
 		return $oAcessoLog;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoLog($oAcessoLog){
 		$oPersistencia = new Persistencia($oAcessoLog);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoLog($oAcessoLog){
 		$oPersistencia = new Persistencia($oAcessoLog);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();

		$oAcessoLog->setCodAcesso($nCodAcesso);
  		$oPersistencia = new Persistencia($oAcessoLog);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoLog na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();

		$oAcessoLog->setCodAcesso($nCodAcesso);
  		$oPersistencia = new Persistencia($oAcessoLog);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoLog da base de dados
 	 *
 	 * @return AcessoLog
 	 */
 	public function recuperarUmAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();
  		$oPersistencia = new Persistencia($oAcessoLog);
  		$sTabelas = "acesso_log";
  		$sCampos = "cod_acesso,nome as usuario,tabela,operacao,data_log,descricao,sql_descricao,acesso_log.ativo";
  		$sComplemento = "inner join acesso_usuario ON acesso_usuario.cod_usuario = acesso_log.usuario  WHERE cod_acesso = $nCodAcesso order by data_log desc";

  		$voAcessoLog = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLog)
  			return $voAcessoLog[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoLog da base de dados
 	 *
 	 * @return AcessoLog[]
 	 */
 	public function recuperarTodosAcessoLog(){
 		$oAcessoLog = new AcessoLog();
  		$oPersistencia = new Persistencia($oAcessoLog);
  		$sTabelas = "acesso_log";
  		$sCampos = "cod_acesso,nome as usuario,tabela,operacao,data_log,descricao,sql_descricao,acesso_log.ativo";
  		$sComplemento = "inner join acesso_usuario ON acesso_usuario.cod_usuario = acesso_log.usuario order by data_log desc";
  		$voAcessoLog = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLog)
  			return $voAcessoLog;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto AcessoLogExclusao
 	 *
 	 * @return AcessoLogExclusao
 	 */
 	public function inicializarAcessoLogExclusao($nCodExclusao,$sFa,$sRealizadoPor,$dData,$sMotivo){
 		$oAcessoLogExclusao = new AcessoLogExclusao();

		$oAcessoLogExclusao->setCodExclusao($nCodExclusao);
		$oAcessoLogExclusao->setFa($sFa);
		$oAcessoLogExclusao->setRealizadoPor($sRealizadoPor);
		$oAcessoLogExclusao->setData($dData);
		$oAcessoLogExclusao->setMotivo($sMotivo);
 		return $oAcessoLogExclusao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoLogExclusao
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoLogExclusao($oAcessoLogExclusao){
 		$oPersistencia = new Persistencia($oAcessoLogExclusao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoLogExclusao
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoLogExclusao($oAcessoLogExclusao){
 		$oPersistencia = new Persistencia($oAcessoLogExclusao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoLogExclusao
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoLogExclusao($nCodExclusao){
 		$oAcessoLogExclusao = new AcessoLogExclusao();

		$oAcessoLogExclusao->setCodExclusao($nCodExclusao);
  		$oPersistencia = new Persistencia($oAcessoLogExclusao);
    	$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoLogExclusao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoLogExclusao($nCodExclusao){
 		$oAcessoLogExclusao = new AcessoLogExclusao();

		$oAcessoLogExclusao->setCodExclusao($nCodExclusao);
  		$oPersistencia = new Persistencia($oAcessoLogExclusao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoLogExclusao da base de dados
 	 *
 	 * @return AcessoLogExclusao
 	 */
 	public function recuperarUmAcessoLogExclusao($nCodExclusao){
 		$oAcessoLogExclusao = new AcessoLogExclusao();
  		$oPersistencia = new Persistencia($oAcessoLogExclusao);
  		$sTabelas = "acesso_log_exclusao";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_exclusao = $nCodExclusao";
  		$voAcessoLogExclusao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLogExclusao)
  			return $voAcessoLogExclusao[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoLogExclusao da base de dados
 	 *
 	 * @return AcessoLogExclusao[]
 	 */
 	public function recuperarTodosAcessoLogExclusao(){
 		$oAcessoLogExclusao = new AcessoLogExclusao();
  		$oPersistencia = new Persistencia($oAcessoLogExclusao);
  		$sTabelas = "acesso_log_exclusao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoLogExclusao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLogExclusao)
  			return $voAcessoLogExclusao;
  		return false;
 	}

	/**
 	 *
 	 * Método para inicializar um Objeto AcessoModulo
 	 *
 	 * @return AcessoModulo
 	 */
 	public function inicializarAcessoModulo($nCodModulo,$sDescricao,$nAtivo){
 		$oAcessoModulo = new AcessoModulo();

		$oAcessoModulo->setCodModulo($nCodModulo);
		$oAcessoModulo->setDescricao($sDescricao);
		$oAcessoModulo->setAtivo($nAtivo);
 		return $oAcessoModulo;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoModulo
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoModulo($oAcessoModulo){
 		$oPersistencia = new Persistencia($oAcessoModulo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoModulo
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoModulo($oAcessoModulo){
 		$oPersistencia = new Persistencia($oAcessoModulo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoModulo
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoModulo($nCodModulo){
 		$oAcessoModulo = new AcessoModulo();

		$oAcessoModulo->setCodModulo($nCodModulo);
  		$oPersistencia = new Persistencia($oAcessoModulo);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoModulo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoModulo($nCodModulo){
 		$oAcessoModulo = new AcessoModulo();

		$oAcessoModulo->setCodModulo($nCodModulo);
  		$oPersistencia = new Persistencia($oAcessoModulo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoModulo da base de dados
 	 *
 	 * @return AcessoModulo
 	 */
 	public function recuperarUmAcessoModulo($nCodModulo){
 		$oAcessoModulo = new AcessoModulo();
  		$oPersistencia = new Persistencia($oAcessoModulo);
  		$sTabelas = "acesso_modulo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_modulo = $nCodModulo";
		//echo "select * from $sTabelas " . $sComplemento;
  		$voAcessoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);

		if($voAcessoModulo)
  			return $voAcessoModulo[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoModulo da base de dados
 	 *
 	 * @return AcessoModulo[]
 	 */
 	public function recuperarTodosAcessoModulo(){
 		$oAcessoModulo = new AcessoModulo();
  		$oPersistencia = new Persistencia($oAcessoModulo);
  		$sTabelas = "acesso_modulo";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1";
  		$voAcessoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoModulo)
  			return $voAcessoModulo;
  		return false;
 	}



	/**
 	 *
 	 * Método para inicializar um Objeto AcessoPermissao
 	 *
 	 * @return AcessoPermissao
 	 */
 	public function inicializarAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
 		$oAcessoPermissao = new AcessoPermissao();

		$oAcessoPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oAcessoPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
 		return $oAcessoPermissao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoPermissao
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoPermissao($oAcessoPermissao){
 		$oPersistencia = new Persistencia($oAcessoPermissao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoPermissao
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoPermissao($oAcessoPermissao){
 		$oPersistencia = new Persistencia($oAcessoPermissao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoPermissao
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoPermissao(){
 		$oAcessoPermissao = new AcessoPermissao();

  		$oPersistencia = new Persistencia($oAcessoPermissao);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;


 	}


	/**
 	 *
 	 * Método para excluir da base de dados um Objeto Permissao
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoPermissaoPorGrupoUsuario($nCodGrupoUsuario){
 		$oAcessoPermissao = new AcessoPermissao();
		$sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
  		$oPersistencia = new Persistencia($oAcessoPermissao);
  		if($oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento))
  			return true;
  		else
  			return false;

 	}


 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoPermissao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
 		$oAcessoPermissao = new AcessoPermissao();
  		$oAcessoPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oAcessoPermissao->setCodGrupoUsuario($nCodGrupoUsuario);

  		$oPersistencia = new Persistencia($oAcessoPermissao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoPermissao da base de dados
 	 *
 	 * @return AcessoPermissao
 	 */
 	public function recuperarUmAcessoPermissao($nCodTransacaoModulo,$nCodGrupousuario){
 		$oAcessoPermissao = new AcessoPermissao();
  		$oPersistencia = new Persistencia($oAcessoPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";

  		$sComplemento = " WHERE cod_transacao_modulo=". $nCodTransacaoModulo ." AND cod_grupo_usuario= " . $nCodGrupousuario;
  		$voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoPermissao)
  			return $voAcessoPermissao[0];
  		return false;
 	}


 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoPermissao da base de dados
 	 *
 	 * @return AcessoPermissao[]
 	 */
 	public function recuperarTodosAcessoPermissao(){
 		$oAcessoPermissao = new AcessoPermissao();
  		$oPersistencia = new Persistencia($oAcessoPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoPermissao)
  			return $voAcessoPermissao;
  		return false;
 	}
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoPermissao da base de dados
 	 *
 	 * @return AcessoPermissao[]
 	 */
 	public function recuperarTodosAcessoPermissaoPorGrupoUsuario($nCodGrupoUsuario){
 		$oAcessoPermissao = new AcessoPermissao();
  		$oPersistencia = new Persistencia($oAcessoPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_grupo_usuario=". $nCodGrupoUsuario;
  		$voAcessoPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoPermissao)
  			return $voAcessoPermissao;
  		return false;
 	}



	/**
 	 *
 	 * Método para inicializar um Objeto AcessoResponsavelTransacao
 	 *
 	 * @return AcessoResponsavelTransacao
 	 */
 	public function inicializarAcessoResponsavelTransacao($nCodResponsavelTransacao,$nCodTransacaoModulo,$sResponsavel,$sOperacao){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

		$oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
		$oAcessoResponsavelTransacao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oAcessoResponsavelTransacao->setResponsavel($sResponsavel);
		$oAcessoResponsavelTransacao->setOperacao($sOperacao);
 		return $oAcessoResponsavelTransacao;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoResponsavelTransacao($oAcessoResponsavelTransacao){
 		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoResponsavelTransacao($oAcessoResponsavelTransacao){
 		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoResponsavelTransacao($nCodResponsavelTransacao){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

		$oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
  		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoResponsavelTransacao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoResponsavelTransacao($nCodResponsavelTransacao){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();

		$oAcessoResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
  		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoResponsavelTransacao da base de dados
 	 *
 	 * @return AcessoResponsavelTransacao
 	 */
 	public function recuperarUmAcessoResponsavelTransacao($nCodResponsavelTransacao){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
  		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_responsavel_transacao = $nCodResponsavelTransacao";
  		$voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoResponsavelTransacao)
  			return $voAcessoResponsavelTransacao[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoResponsavelTransacao da base de dados
 	 *
 	 * @return AcessoResponsavelTransacao[]
 	 */
 	public function recuperarTodosAcessoResponsavelTransacao(){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
  		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoResponsavelTransacao)
  			return $voAcessoResponsavelTransacao;
  		return false;
 	}


 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoResponsavelTransacao da base de dados
 	 *
 	 * @return AcessoResponsavelTransacao[]
 	 */
 	public function recuperarTodosAcessoResponsavelTransacaoPorTransacaoModulo($nCodTransacaoModulo){
 		$oAcessoResponsavelTransacao = new AcessoResponsavelTransacao();
  		$oPersistencia = new Persistencia($oAcessoResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = "where cod_transacao_modulo = $nCodTransacaoModulo";

  		$voAcessoResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoResponsavelTransacao)
  			return $voAcessoResponsavelTransacao;
  		return false;
 	}


	/**
 	 *
 	 * Método para inicializar um Objeto AcessoTransacaoModulo
 	 *
 	 * @return AcessoTransacaoModulo
 	 */
 	public function inicializarAcessoTransacaoModulo($nCodTransacaoModulo,$nCodModulo,$sDescricao,$nAtivo){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();

		$oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
		$oAcessoTransacaoModulo->setCodModulo($nCodModulo);
		$oAcessoTransacaoModulo->setDescricao($sDescricao);
		$oAcessoTransacaoModulo->setAtivo($nAtivo);
 		return $oAcessoTransacaoModulo;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoTransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoTransacaoModulo($oAcessoTransacaoModulo){
 		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoTransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoTransacaoModulo($oAcessoTransacaoModulo){
 		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoTransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoTransacaoModulo($nCodTransacaoModulo){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();

		$oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoTransacaoModulo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoTransacaoModulo($nCodTransacaoModulo){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();

		$oAcessoTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoTransacaoModulo da base de dados
 	 *
 	 * @return AcessoTransacaoModulo
 	 */
 	public function recuperarUmAcessoTransacaoModulo($nCodTransacaoModulo){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_transacao_modulo = $nCodTransacaoModulo";
  		$voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoTransacaoModulo)
  			return $voAcessoTransacaoModulo[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoTransacaoModulo da base de dados
 	 *
 	 * @return AcessoTransacaoModulo
 	 */
 	public function recuperarUmAcessoTransacaoModuloPorResponsavelOperacao($sResponsavel,$sOP,$nCodGrupoUsuario){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "acesso_transacao_modulo.*";
  		$sComplemento = " INNER JOIN acesso_responsavel_transacao ON acesso_transacao_modulo.cod_transacao_modulo = acesso_responsavel_transacao.cod_transacao_modulo
						  INNER JOIN acesso_permissao ON acesso_permissao.cod_transacao_modulo = acesso_transacao_modulo.cod_transacao_modulo AND cod_grupo_usuario=".$nCodGrupoUsuario."
                          WHERE responsavel = '$sResponsavel' AND operacao= '$sOP'";

		//echo "SELECT * FROM $sTabelas " . $sComplemento;
        //die();
  		$voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoTransacaoModulo)
  			return $voAcessoTransacaoModulo[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoTransacaoModulo da base de dados
 	 *
 	 * @return AcessoTransacaoModulo[]
 	 */
 	public function recuperarTodosAcessoTransacaoModulo(){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoTransacaoModulo)
  			return $voAcessoTransacaoModulo;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoTransacaoModulo da base de dados
 	 *
 	 * @return AcessoTransacaoModulo[]
 	 */
 	public function recuperarTodosAcessoTransacaoModuloPorModulo($nCodModulo){
 		$oAcessoTransacaoModulo = new AcessoTransacaoModulo();
  		$oPersistencia = new Persistencia($oAcessoTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_modulo=".$nCodModulo;
		//echo "SELECT * FROM $sTabelas " . $sComplemento;
  		$voAcessoTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoTransacaoModulo)
  			return $voAcessoTransacaoModulo;
  		return false;
 	}


/**
 	 *
 	 * Método para inicializar um Objeto AcessoUnidadeEmpresa
 	 *
 	 * @return AcessoUnidadeEmpresa
 	 */
 	public function inicializarAcessoUnidadeEmpresa($nCodUnidade,$nEmpCodigo){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();

		$oAcessoUnidadeEmpresa->setCodUnidade($nCodUnidade);
		$oAcessoUnidadeEmpresa->setEmpCodigo($nEmpCodigo);
 		return $oAcessoUnidadeEmpresa;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoUnidadeEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoUnidadeEmpresa($oAcessoUnidadeEmpresa){
 		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoUnidadeEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoUnidadeEmpresa($oAcessoUnidadeEmpresa){
 		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUnidadeEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoUnidadeEmpresa($nCodUnidade,$nEmpCodigo){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();

		$oAcessoUnidadeEmpresa->setCodUnidade($nCodUnidade);
		$oAcessoUnidadeEmpresa->setEmpCodigo($nEmpCodigo);
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
    		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}
    /**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUnidadeEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function excluirEmpresasPorUnidade($nCodUnidade,$nEmpCodigo){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);

        $sComplemento = "WHERE cod_unidade = ". $nCodUnidade;

	    $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoUnidadeEmpresa na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoUnidadeEmpresa($nCodUnidade,$nEmpCodigo){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();

		$oAcessoUnidadeEmpresa->setCodUnidade($nCodUnidade);
		$oAcessoUnidadeEmpresa->setEmpCodigo($nEmpCodigo);
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoUnidadeEmpresa da base de dados
 	 *
 	 * @return AcessoUnidadeEmpresa
 	 */
 	public function recuperarUmAcessoUnidadeEmpresa($nCodUnidade,$nEmpCodigo){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		$sTabelas = "acesso_unidade_empresa";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_unidade = $nCodUnidade AND emp_codigo = $nEmpCodigo";
  		$voAcessoUnidadeEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUnidadeEmpresa)
  			return $voAcessoUnidadeEmpresa[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoUnidadeEmpresa da base de dados
 	 *
 	 * @return AcessoUnidadeEmpresa[]
 	 */
 	public function recuperarTodosAcessoUnidadeEmpresa(){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		$sTabelas = "acesso_unidade_empresa";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoUnidadeEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUnidadeEmpresa)
  			return $voAcessoUnidadeEmpresa;
  		return false;
 	}



  	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoUnidadeEmpresa da base de dados
 	 *
 	 * @return AcessoUnidadeEmpresa[]
 	 */
 	public function recuperarTodosAcessoUnidadeEmpresaPorUnidade($nCodUnidade){
 		$oAcessoUnidadeEmpresa = new AcessoUnidadeEmpresa();
  		$oPersistencia = new Persistencia($oAcessoUnidadeEmpresa);
  		$sTabelas = "acesso_unidade_empresa";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_unidade=".$nCodUnidade;
  		$voAcessoUnidadeEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUnidadeEmpresa)
  			return $voAcessoUnidadeEmpresa;
  		return false;
 	}


	/**
 	 *
 	 * Método para inicializar um Objeto AcessoUsuario
 	 *
 	 * @return AcessoUsuario
 	 */
 	public function inicializarAcessoUsuario($nCodUsuario,$nPesgCodigo,$sNome,$sLogin,$sSenha,$nAtivo){
 		$oAcessoUsuario = new AcessoUsuario();

		$oAcessoUsuario->setCodUsuario($nCodUsuario);
		$oAcessoUsuario->setPesgCodigo($nPesgCodigo);
		$oAcessoUsuario->setNome($sNome);
		$oAcessoUsuario->setLogin($sLogin);
		$oAcessoUsuario->setSenha($sSenha);
		$oAcessoUsuario->setAtivo($nAtivo);
 		return $oAcessoUsuario;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoUsuario($oAcessoUsuario){
 		$oPersistencia = new Persistencia($oAcessoUsuario);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoUsuario($oAcessoUsuario){
 		$oPersistencia = new Persistencia($oAcessoUsuario);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoUsuario($nCodUsuario){
 		$oAcessoUsuario = new AcessoUsuario();

		$oAcessoUsuario->setCodUsuario($nCodUsuario);
  		$oPersistencia = new Persistencia($oAcessoUsuario);
    		$bExcluir = $oPersistencia->excluir();

 		if($bExcluir)
  				return true;
  		return false;

 	}
/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function excluirFisicamenteAcessoUsuario($nCodUsuario){
 		$oAcessoUsuario = new AcessoUsuario();

		$oAcessoUsuario->setCodUsuario($nCodUsuario);
  		$oPersistencia = new Persistencia($oAcessoUsuario);
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoUsuario na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoUsuario($nCodUsuario){
 		$oAcessoUsuario = new AcessoUsuario();

		$oAcessoUsuario->setCodUsuario($nCodUsuario);
  		$oPersistencia = new Persistencia($oAcessoUsuario);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoUsuario da base de dados
 	 *
 	 * @return AcessoUsuario
 	 */
 	public function recuperarUmAcessoUsuario($nCodUsuario){
 		$oAcessoUsuario = new AcessoUsuario();
  		$oPersistencia = new Persistencia($oAcessoUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_usuario = $nCodUsuario";
  		$voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuario)
  			return $voAcessoUsuario[0];
  		return false;
 	}

	/**
 	 *
 	 * Método para recuperar um objeto AcessoUsuario da base de dados
 	 *
 	 * @return AcessoUsuario
 	 */
 	public function recuperarUmUsuarioPorLogin($sLogin){
 		$oAcessoUsuario = new AcessoUsuario();
  		$oPersistencia = new Persistencia($oAcessoUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE login = '$sLogin' And ativo = 1";
  		$voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuario)
  			return $voAcessoUsuario[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoUsuario da base de dados
 	 *
 	 * @return AcessoUsuario[]
 	 */
 	public function recuperarTodosAcessoUsuario(){
 		$oAcessoUsuario = new AcessoUsuario();
  		$oPersistencia = new Persistencia($oAcessoUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "cod_usuario,pesg_alt AS pesg_codigo,login,acesso_usuario.ativo as ativo";
        $sComplemento = "Inner Join v_pessoa_formatada On v_pessoa_formatada.pesg_codigo = acesso_usuario.pesg_codigo Where acesso_usuario.ativo=1";
		$voAcessoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuario)
  			return $voAcessoUsuario;
  		return false;
 	}

/**
 	 *
 	 * Método para inicializar um Objeto AcessoUsuarioUnidade
 	 *
 	 * @return AcessoUsuarioUnidade
 	 */
 	public function inicializarAcessoUsuarioUnidade($nCodUsuario,$nCodGrupoUsuario,$nCodPlanoContas){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();

		$oAcessoUsuarioUnidade->setCodUsuario($nCodUsuario);
		$oAcessoUsuarioUnidade->setCodGrupoUsuario($nCodGrupoUsuario);
		$oAcessoUsuarioUnidade->setCodPlanoContas($nCodPlanoContas);
 		return $oAcessoUsuarioUnidade;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto AcessoUsuarioUnidade
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoUsuarioUnidade($oAcessoUsuarioUnidade){
 		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto AcessoUsuarioUnidade
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoUsuarioUnidade($oAcessoUsuarioUnidade){
 		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUsuarioUnidade
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoUsuarioUnidade($nCodUsuario,$nCodGrupoUsuario,$nCodPlanoContas){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();

		$oAcessoUsuarioUnidade->setCodUsuario($nCodUsuario);
		$oAcessoUsuarioUnidade->setCodGrupoUsuario($nCodGrupoUsuario);
		$oAcessoUsuarioUnidade->setCodPlanoContas($nCodPlanoContas);
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}
    /**
 	 *
 	 * Método para excluir da base de dados um Objeto AcessoUsuarioUnidade
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoUsuarioUnidadeFisicamentePorUsuario($nCodUsuario){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
        $sComplemento = "WHERE cod_usuario = ". $nCodUsuario;
	    $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto AcessoUsuarioUnidade na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoUsuarioUnidade($nCodUsuario,$nCodGrupoUsuario,$nCodPlanoContas){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();

		$oAcessoUsuarioUnidade->setCodUsuario($nCodUsuario);
		$oAcessoUsuarioUnidade->setCodGrupoUsuario($nCodGrupoUsuario);
		$oAcessoUsuarioUnidade->setCodPlanoContas($nCodPlanoContas);
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto AcessoUsuarioUnidade da base de dados
 	 *
 	 * @return AcessoUsuarioUnidade
 	 */
 	public function recuperarUmAcessoUsuarioUnidade($nCodUsuario,$nCodGrupoUsuario,$nCodPlanoContas){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		$sTabelas = "acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_usuario = $nCodUsuario AND cod_grupo_usuario = $nCodGrupoUsuario AND cod_plano_contas = $nCodPlanoContas";
  		$voAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuarioUnidade)
  			return $voAcessoUsuarioUnidade[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoUsuarioUnidade da base de dados
 	 *
 	 * @return AcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosAcessoUsuarioUnidade(){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		$sTabelas = "acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuarioUnidade)
  			return $voAcessoUsuarioUnidade;
    }
	/**
 	 *
 	 * Método para recuperar um vetor de objetos AcessoUsuarioUnidade da base de dados
 	 *
 	 * @return AcessoUsuarioUnidade[]
 	 */
 	public function recuperarTodosAcessoUsuarioUnidadePorUsuario($nCodUsuario){
 		$oAcessoUsuarioUnidade = new AcessoUsuarioUnidade();
  		$oPersistencia = new Persistencia($oAcessoUsuarioUnidade);
  		$sTabelas = "acesso_usuario_unidade";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_usuario=" . $nCodUsuario;
  		$voAcessoUsuarioUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoUsuarioUnidade)
  			return $voAcessoUsuarioUnidade;
    }

}

?>
