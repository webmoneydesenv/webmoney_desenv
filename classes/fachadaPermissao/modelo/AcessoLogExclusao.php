<?php
 /**
  * @author Auto-Generated
  * @package fachadaPermissao
  * @SGBD mysql
  * @tabela acesso_log_exclusao
  */
 class AcessoLogExclusao{
 	/**
	* @campo cod_exclusao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodExclusao;
	/**
	* @campo FA
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFa;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment true
	*/
	private $dData;
	/**
	* @campo motivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMotivo;


 	public function __construct(){

 	}

 	public function setCodExclusao($nCodExclusao){
		$this->nCodExclusao = $nCodExclusao;
	}
	public function getCodExclusao(){
		return $this->nCodExclusao;
	}
	public function setFa($sFa){
		$this->sFa = $sFa;
	}
	public function getFa(){
		return $this->sFa;
	}
	public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y H:i");
	}
	public function setDataBanco($dData){
		 if($dData){
			 $oData = DateTime::createFromFormat('d/m/Y', $dData);
			 $this->dData = $oData->format('Y-m-d') ;
	}
		 }
	public function setMotivo($sMotivo){
		$this->sMotivo = $sMotivo;
	}
	public function getMotivo(){
		return $this->sMotivo;
	}

 }
 ?>
