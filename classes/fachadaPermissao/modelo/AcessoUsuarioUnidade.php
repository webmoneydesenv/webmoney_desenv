<?php
 /**
  * @author Auto-Generated
  * @package fachadaPermissao
  * @SGBD mysql
  * @tabela acesso_usuario_unidade
  */
 class AcessoUsuarioUnidade{
 	/**
	* @campo cod_usuario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUsuario;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	/**
	* @campo cod_plano_contas
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodPlanoContas;
	private $oAcessoUsuario;
	private $oAcessoGrupoUsuario;
	private $oMnyPlanoContas;


 	public function __construct(){

 	}

 	public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}
	public function getCodUsuario(){
		return $this->nCodUsuario;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setCodPlanoContas($nCodPlanoContas){
		$this->nCodPlanoContas = $nCodPlanoContas;
	}
	public function getCodPlanoContas(){
		return $this->nCodPlanoContas;
	}
	public function setAcessoUsuario($oAcessoUsuario){
		$this->oAcessoUsuario = $oAcessoUsuario;
	}
	public function getAcessoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($this->getCodUsuario());
		return $this->oAcessoUsuario;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oAcessoGrupoUsuario;
	}
	public function setMnyPlanoContas($oMnyPlanoContas){
		$this->oMnyPlanoContas = $oMnyPlanoContas;
	}
	public function getMnyPlanoContas(){
		$oFachada = new FachadaPermissaoBD();
		$this->oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($this->getCodPlanoContas());
		return $this->oMnyPlanoContas;
	}

 }
 ?>
