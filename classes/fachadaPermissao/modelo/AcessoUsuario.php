<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql 
  * @tabela acesso_usuario 
  */
 class AcessoUsuario{
 	/**
	* @campo cod_usuario
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodUsuario;
 	/**
	* @campo pesg_codigo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nPesgCodigo;

	/**
	* @campo nome
	* @var String
	* @primario false
	* @auto-increment false
	*/

	private $sNome;
	/**
	* @campo login
	* @var String
	* @primario false
	* @auto-increment false
	*/

	private $sLogin;
	/**
	* @campo senha
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sSenha;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoGrupoUsuario;
	private $oAcessoUsuarioEmpresa;
	private $oAcessoPessoaGeral;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}
	public function getCodUsuario(){
		return $this->nCodUsuario;
	}
 	public function setPesgCodigo($nPesgCodigo){
		$this->nPesgCodigo = $nPesgCodigo;
	}
	public function getPesgCodigo(){
		return $this->nPesgCodigo;
	}

	public function setLogin($sLogin){
		$this->sLogin = $sLogin;
	}
	public function getLogin(){
		return $this->sLogin;
	}
    public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setSenha($sSenha){
		$this->sSenha = $sSenha;
	}
	public function getSenha(){
		return $this->sSenha;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
    public function getAcessoUsuarioEmpresa(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoUsuarioEmpresa = $oFachada->recuperarTodosAcessoUsuarioEmpresaPorUsuario($this->getCodUsuario());
		return $this->oAcessoUsuarioEmpresa;
	}
     public function getPessoa(){
		$oFachadaView = new FachadaViewBD();
		$this->oAcessoPessoaGeral = $oFachadaView->recuperarUmVPessoaGeralFormatada($this->getPesgCodigo());
		return $this->oAcessoPessoaGeral;
	}
	
 }
 ?>
