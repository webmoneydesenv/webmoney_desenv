<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql 
  * @tabela acesso_transacao_modulo 
  */
 class AcessoTransacaoModulo{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_modulo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodModulo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oAcessoModulo;
	private $voResponsavel;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodModulo($nCodModulo){
		$this->nCodModulo = $nCodModulo;
	}
	public function getCodModulo(){
		return $this->nCodModulo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setAcessoModulo($oAcessoModulo){
		$this->oAcessoModulo = $oAcessoModulo;
	}
	public function getAcessoModulo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoModulo = $oFachada->recuperarUmAcessoModulo($this->getCodModulo());
		return $this->oAcessoModulo;
	}
	public function getResponsaveis(){
		$oFachada = new FachadaPermissaoBD();
		$this->voResponsavel = $oFachada->recuperarTodosAcessoResponsavelTransacaoPorTransacaoModulo($this->nCodTransacaoModulo);
		return $this->voResponsavel;
	}
	
 }
 ?>
