<?php
 /**
  * @author Auto-Generated
  * @package fachadaPermissao
  * @SGBD mysql
  * @tabela acesso_unidade_empresa
  */
 class AcessoUnidadeEmpresa{
 	/**
	* @campo cod_unidade
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo emp_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	private $voSysEmpresa;
	private $oSysEmpresa;
	private $oMnyPlanoContas;

 	public function __construct(){

 	}

 	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}

public function setSysEmpresa($oSysEmpresa){
		$this->oSysEmpresa = $oSysEmpresa;
	}
	public function getSysEmpresa(){
		$oFachada = new FachadaPermissaoBD();
		$voSysEmpresa = $oFachada->recuperarTodosAcessoUnidadeEmpresaPorUnidade($this->getCodUnidade());
        if($voSysEmpresa){
            foreach($voSysEmpresa as $oEmpresa){
                $voEmpresa[] = $oEmpresa->getEmpCodigo();
            }
        }
        $this->voSysEmpresa = $voEmpresa;

        return $this->voSysEmpresa;
	}

    public function getMnyPlanoContas(){
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $this->oMnyPlanoContas = $oFachadaFinanceiro->recuperarUmMnyPlanoContas($this->getCodUnidade());
        return $this->oMnyPlanoContas;
    }

    public function getEmpresa(){
        $oFachadaSys = new FachadaSysBD();
        $this->oSysEmpresa = $oFachadaSys->recuperarUmSysEmpresa($this->getEmpCodigo());
        return $this->oSysEmpresa;
    }

 }
 ?>
