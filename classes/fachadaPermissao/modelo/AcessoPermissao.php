<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql 
  * @tabela acesso_permissao 
  */
 class AcessoPermissao{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario true
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	private $oAcessoTransacaoModulo;
	private $oAcessoGrupoUsuario;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setAcessoTransacaoModulo($oAcessoTransacaoModulo){
		$this->oAcessoTransacaoModulo = $oAcessoTransacaoModulo;
	}
	public function getAcessoTransacaoModulo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($this->getCodTransacaoModulo());
		return $this->oAcessoTransacaoModulo;
	}
	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oAcessoGrupoUsuario;
	}
	
 }
 ?>
