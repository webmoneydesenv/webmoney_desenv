<?php
 class AcessoModuloCTR implements IControle{
 
 	public function AcessoModuloCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$voAcessoModulo = $oFachada->recuperarTodosAcessoModulo();
 
 		$_REQUEST['voAcessoModulo'] = $voAcessoModulo;
 		
 		
 		include_once("view/permissao/acesso_modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oAcessoModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoModulo = ($_POST['fIdAcessoModulo'][0]) ? $_POST['fIdAcessoModulo'][0] : $_GET['nIdAcessoModulo'];
 	
 			if($nIdAcessoModulo){
 				$vIdAcessoModulo = explode("||",$nIdAcessoModulo);
 				$oAcessoModulo = $oFachada->recuperarUmAcessoModulo($vIdAcessoModulo[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoModulo'] = ($_SESSION['oAcessoModulo']) ? $_SESSION['oAcessoModulo'] : $oAcessoModulo;
 		unset($_SESSION['oAcessoModulo']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_modulo/detalhe.php");
 		else
 			include_once("view/permissao/acesso_modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoModulo = $oFachada->inicializarAcessoModulo($_POST['fCodModulo'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oAcessoModulo'] = $oAcessoModulo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			if($sOP == 'Alterar')
	 		$oValidate->add_number_field("CodModulo", $oAcessoModulo->getCodModulo(), "number", "y");
			$oValidate->add_text_field("Descri&ccedil;&atilde;o", $oAcessoModulo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoModulo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoModulo=".$_POST['fCodModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoModulo($oAcessoModulo)){
 					unset($_SESSION['oAcessoModulo']);
 					$_SESSION['sMsg'] = "M&oacute;dulo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss&iacute;vel inserir o M&oacute;dulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoModulo($oAcessoModulo)){
 					unset($_SESSION['oAcessoModulo']);
 					$_SESSION['sMsg'] = "M&oacute;dulo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss&iacute;vel alterar o M&oacute;dulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoModulo=".$_POST['fCodModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoModulo']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirAcessoModulo($sCampoChave);\n");
				}				

 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "M&oacute;dulo(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoModulo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) M&oacute;dulo!";
 					$sHeader = "?bErro=1&action=AcessoModulo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
