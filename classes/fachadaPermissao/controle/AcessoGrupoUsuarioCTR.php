<?php
 class AcessoGrupoUsuarioCTR implements IControle{
 
 	public function AcessoGrupoUsuarioCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$voAcessoGrupoUsuario = $oFachada->recuperarTodosAcessoGrupoUsuario();
 
 		$_REQUEST['voAcessoGrupoUsuario'] = $voAcessoGrupoUsuario;
 		
 		
 		include_once("view/permissao/acesso_grupo_usuario/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oAcessoGrupoUsuario = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoGrupoUsuario = ($_POST['fIdAcessoGrupoUsuario'][0]) ? $_POST['fIdAcessoGrupoUsuario'][0] : $_GET['nIdAcessoGrupoUsuario'];
 	
 			if($nIdAcessoGrupoUsuario){
 				$vIdAcessoGrupoUsuario = explode("||",$nIdAcessoGrupoUsuario);
 				$oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($vIdAcessoGrupoUsuario[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoGrupoUsuario'] = ($_SESSION['oAcessoGrupoUsuario']) ? $_SESSION['oAcessoGrupoUsuario'] : $oAcessoGrupoUsuario;
 		unset($_SESSION['oAcessoGrupoUsuario']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_grupo_usuario/detalhe.php");
 		else
 			include_once("view/permissao/acesso_grupo_usuario/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoGrupoUsuario = $oFachada->inicializarAcessoGrupoUsuario($_POST['fCodGrupoUsuario'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oAcessoGrupoUsuario'] = $oAcessoGrupoUsuario;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 	
			if($sOP == 'Alterar')
				$oValidate->add_number_field("CodGrupoUsuario", $oAcessoGrupoUsuario->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_text_field("Descri&ccedil;&atilde;o", $oAcessoGrupoUsuario->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoGrupoUsuario->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoGrupoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoGrupoUsuario=".$_POST['fCodGrupoUsuario']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoGrupoUsuario($oAcessoGrupoUsuario)){
 					unset($_SESSION['oAcessoGrupoUsuario']);
 					$_SESSION['sMsg'] = "Grupo de Usu&aacute;rio inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoGrupoUsuario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss&iacute;vel inserir o Grupo de Usu�rio!";
 					$sHeader = "?bErro=1&action=AcessoGrupoUsuario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoGrupoUsuario($oAcessoGrupoUsuario)){
 					unset($_SESSION['oAcessoGrupoUsuario']);
 					$_SESSION['sMsg'] = "Grupo de Usu&aacute;rio alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoGrupoUsuario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss&iacute;vel alterar o Grupo de Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoGrupoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoGrupoUsuario=".$_POST['fCodGrupoUsuario']."";
 				}
 			break;
 			case "Excluir":
			
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoGrupoUsuario']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirAcessoGrupoUsuario($sCampoChave);\n");
				}				
			
 				if($bResultado){
 					$_SESSION['sMsg'] = "Grupo de Usu&aacute;rio(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoGrupoUsuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Grupo(s) de Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoGrupoUsuario.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
