<?php
 class AcessoResponsavelTransacaoCTR implements IControle{
 
 	public function AcessoResponsavelTransacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
		$nCodModulo = $_REQUEST['nIdModulo'];
		$_REQUEST['oModulo'] = $oFachada->recuperarUmAcessoModulo($nCodModulo);
		
		$nCodTransacaoModulo = ($_REQUEST['fIdAcessoTransacaoModulo'][0]) ? $_REQUEST['fIdAcessoTransacaoModulo'][0] : $_REQUEST['nCodTransacaoModulo'];  
		$_REQUEST['oAcessoTransacaoModulo'] = $oFachada->recuperarUmAcessoTransacaoModulo($nCodTransacaoModulo);
		
 		$voAcessoResponsavelTransacao = $oFachada->recuperarTodosAcessoResponsavelTransacaoPorTransacaoModulo($nCodTransacaoModulo);
 		$_REQUEST['voAcessoResponsavelTransacao'] = $voAcessoResponsavelTransacao;
/*		print_r ($_REQUEST);
		die();
*/ 		include_once("view/permissao/acesso_responsavel_transacao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oAcessoResponsavelTransacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoResponsavelTransacao = ($_POST['fIdAcessoResponsavelTransacao'][0]) ? $_POST['fIdAcessoResponsavelTransacao'][0] : $_GET['nIdAcessoResponsavelTransacao'];
 	
 			if($nIdAcessoResponsavelTransacao){
 				$vIdAcessoResponsavelTransacao = explode("||",$nIdAcessoResponsavelTransacao);
 				$oAcessoResponsavelTransacao = $oFachada->recuperarUmAcessoResponsavelTransacao($vIdAcessoResponsavelTransacao[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoResponsavelTransacao'] = ($_SESSION['oAcessoResponsavelTransacao']) ? $_SESSION['oAcessoResponsavelTransacao'] : $oAcessoResponsavelTransacao;
		 
		if($_REQUEST['oAcessoResponsavelTransacao']) 
			$_REQUEST['oAcessoTransacaoModulo'] = $_REQUEST['oAcessoResponsavelTransacao']->getAcessoTransacaoModulo();
		else{
			$nCodTransacaoModulo = $_REQUEST['fCodTransacaoModulo'];
			$_REQUEST['oAcessoTransacaoModulo'] = $oFachada->recuperarUmAcessoTransacaoModulo($nCodTransacaoModulo);
		}
		
		
		if($_REQUEST['oAcessoTransacaoModulo']){
			$_REQUEST['oAcessoModulo'] = $_REQUEST['oAcessoTransacaoModulo']->getAcessoModulo();
		}else{
			$_REQUEST['oAcessoModulo'] = $oFachada->recuperarUmAcessoModulo($_REUQEST['nIdModulo']);
		}
 		
		unset($_SESSION['oAcessoResponsavelTransacao']);
 
 		$_REQUEST['voAcessoTransacaoModulo'] = $oFachada->recuperarTodosAcessoTransacaoModulo();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_responsavel_transacao/detalhe.php");
 		else
 			include_once("view/permissao/acesso_responsavel_transacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoResponsavelTransacao = $oFachada->inicializarAcessoResponsavelTransacao($_POST['fCodResponsavelTransacao'],$_POST['fCodTransacaoModulo'],$_POST['fResponsavel'],$_POST['fOperacao']);
 			$_SESSION['oAcessoResponsavelTransacao'] = $oAcessoResponsavelTransacao;
			$oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($_POST['fCodTransacaoModulo']);
			$_REQUEST['oAcessoTransacaoModulo'] = $oAcessoTransacaoModulo;

 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			if($sOP == 'Alterar')
 				$oValidate->add_number_field("Cod", $oAcessoResponsavelTransacao->getCodResponsavelTransacao(), "number", "y");
			$oValidate->add_number_field("Transa��o", $oAcessoResponsavelTransacao->getCodTransacaoModulo(), "number", "y");
			$oValidate->add_text_field("Respons�vel", $oAcessoResponsavelTransacao->getResponsavel(), "text", "y");
			$oValidate->add_text_field("Opera��o", $oAcessoResponsavelTransacao->getOperacao(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nIdAcessoResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoResponsavelTransacao($oAcessoResponsavelTransacao)){
 					unset($_SESSION['oAcessoResponsavelTransacao']);
 					$_SESSION['sMsg'] = "Respons&aacute;vel Transa&ccedil;&atilde;o inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo()."";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Respons&aacute;vel Transa&ccedil;&atilde;o!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoResponsavelTransacao($oAcessoResponsavelTransacao)){
 					unset($_SESSION['oAcessoResponsavelTransacao']);
 					$_SESSION['sMsg'] = "Respons&aacute;vel Transa&ccedil;&atilde;o alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo();
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Respons&aacute;vel Transa&ccedil;&atilde;o!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nIdAcessoResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoResponsavelTransacao']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirAcessoResponsavelTransacao($sCampoChave);\n");
				}				
			
				if($bResultado){
 					$_SESSION['sMsg'] = "Respons&aacute;vel Transa&ccedil;&atilde;o(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoResponsavelTransacao.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getAcessoTransacaoModulo()->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo();
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Respons&aacute;vel Transa&ccedil;&atilde;o!";
 					$sHeader = "?bErro=1&action=AcessoResponsavelTransacao.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getAcessoTransacaoModulo()->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo();
 				}                                                            
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
