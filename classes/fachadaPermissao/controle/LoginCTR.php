<?php
 class LoginCTR implements IControle{
 
 	public function LoginCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$this->preparaFormulario();
 	
 	}
 
 	public function preparaFormulario(){

        if($_REQUEST['sOP'] == 'EscolheEmpresa'){
            $oFachadaSys = new FachadaSysBD();
            $oFachadaView = new FachadaViewBD();

            if(!$_SESSION['Perfil']['Unidade']){
                //se ele tiver mais de um registro na tabela acesso_usuario_unidade deve selecionar a unidade
                // se não tiver unidade associada ao usuario
                $_REQUEST['voVAcessoUsuarioUnidade'] = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuarioDISTINCT($_SESSION['oUsuarioImoney']->getCodUsuario());
            }

            include_once("view/permissao/login/seleciona_empresa.php");
        }else{
           include_once("view/permissao/login/index.php");
           //include_once("view/permissao/login/index_manutencao.php");
        }
 
 		exit();
 	
 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
        $oFachadaFinanceiro = new FachadaFinanceiroBD();

        //RECUPERA TODAS AS UNIDADES CENTRAIS...
        $voMnyPlanoContas = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasCENTRAL();


        foreach($voMnyPlanoContas as $oMnyPlanoContas){
             $sPalnoContasCodigo[]= $oMnyPlanoContas->getPlanoContasCodigo();
        }
        //USADO PARA VERIFICAR A PERMISSAO...
        $_SESSION['UnidadeCentral'] = $sPalnoContasCodigo;


 		$sOP = $_REQUEST['sOP'];

 		if( ($sOP != "Logoff") && ($sOP != 'Selecionar') && ($sOP !='EscolheEmpresa')&& ($sOP !='EscolheEmpresa2')){
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Login", $_REQUEST['fLogin'], "text", "y");
			$oValidate->add_text_field("Senha", $_REQUEST['fSenha'], "text", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Login.preparaFormulario";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
		
 		switch($sOP){
 			case "Logon":
 				$oUsuario = $oFachada->recuperarUmUsuarioPorLogin($_REQUEST['fLogin']);

                if(is_object($oUsuario)){
	 				if($oUsuario->getSenha() == md5($_REQUEST['fSenha'])){
//					if($oUsuario->getSenha() == ($_REQUEST['fSenha'])){
						$_SESSION['oUsuarioImoney'] = $oUsuario;

                           $oFachadaView = new FachadaViewBD();
                           $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuario($_SESSION['oUsuarioImoney']->getCodUsuario());

                           if(count($voVAcessoUsuarioUnidade) == 1){

                               $_SESSION['Perfil']['CodUnidade']     =  $voVAcessoUsuarioUnidade[0]->getPlanoContasCodigo();
                               $_SESSION['Perfil']['Unidade']         = $voVAcessoUsuarioUnidade[0]->getUnidade();
                               $_SESSION['Perfil']['CodUsuario'] =  $voVAcessoUsuarioUnidade[0]->getCodUsuario();
                               $_SESSION['Perfil']['CodGrupoUsuario'] =  $voVAcessoUsuarioUnidade[0]->getCodGrupoUsuario();
                               $_SESSION['Perfil']['Nome']    =  $voVAcessoUsuarioUnidade[0]->getNome();
                               $_SESSION['Perfil']['Login']    =  $voVAcessoUsuarioUnidade[0]->getLogin();
                               $_SESSION['Perfil']['GrupoUsuario']    =  $voVAcessoUsuarioUnidade[0]->getGrupo();
                             //  print_r($_SESSION['Perfil']);die();
                               $sHeader = "?action=Login.preparaFormulario&sOP=EscolheEmpresa";
                           }else{
                               $sHeader = "?action=Login.preparaFormulario&sOP=EscolheEmpresa";

                           }

	 				} else {
	 					$_SESSION['sMsg'] = "Senha incorreta!";
 						$sHeader = "index.php?bErro=2&action=Login.preparaFormulario";
	 				}
 				} else {
 					$_SESSION['sMsg'] = "Usu&aacute;rio n&atilde;o cadastrado no sistema!";
 					$sHeader = "?bErro=1&action=Login.preparaFormulario";
 				}
 			break;



			case "EscolheEmpresa":

                $oFachadaSys = new FachadaSysBD();
                $oFachadaView = new FachadaViewBD();

				 if($_REQUEST['nCodUnidade']){
                    $voEmpresa = $oFachadaSys->recuperarTodosSysEmpresaPorUnidade($_REQUEST['nCodUnidade']);
                    $_REQUEST['voEmpresa'] = $voEmpresa;
                }

                $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuario($_SESSION['oUsuarioImoney']->getCodUsuario());
                if($_SESSION['Perfil']){
                    unset($_SESSION['oEmpresa']);
                    $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuario($_SESSION['oUsuarioImoney']->getCodUsuario());
                    if(count($voVAcessoUsuarioUnidade) > 1){
                        unset($_SESSION['Perfil']['Unidade']);
                        unset($_SESSION['Perfil']['CodGrupoUsuario']);
                        unset($_SESSION['Perfil']['GrupoUsuario']);
                        $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuarioUnidade($_SESSION['oUsuarioImoney']->getCodUsuario());
                        $_REQUEST['voVAcessoUsuarioUnidade'] = $voVAcessoUsuarioUnidade;
                    }
                    $_REQUEST['oEmpresaSelecionada'] = $oFachadaSys->recuperarUmSysEmpresa($_REQUEST['fIdSysEmpresa'][0]);

                }

                include_once("view/permissao/login/seleciona_empresa.php");
                die();

			break;

            case "EscolheEmpresa2":
                $oFachada = new FachadaSysBD();
                $oFachadaPermissao = new FachadaPermissaoBD();
                $oFachadaView = new FachadaViewBD();
                $nCodEmpresa = ($_REQUEST['fIdSysEmpresa'][0]) ? $_REQUEST['fIdSysEmpresa'][0] : $_POST['fEmpCodigo'];
                $_SESSION['oEmpresa'] = $oFachada->recuperarUmSysEmpresa($nCodEmpresa);

               $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuario($_SESSION['oUsuarioImoney']->getCodUsuario());
               if(count($voVAcessoUsuarioUnidade) == 1){

                   $sHeader = "?";
                    header("Location: ".$sHeader);
                    die();
               }else{


                $oAcessoGrupoUsuario = $oFachadaPermissao->recuperarUmAcessoGrupoUsuario($_POST['fCodGrupoUsuario']);

               $_SESSION['Perfil']['CodGrupoUsuario'] =  $oAcessoGrupoUsuario->getCodGrupoUsuario();
               $_SESSION['Perfil']['Nome']    =  $oFachadaView->recuperarUmVPessoaGeralFormatada($_SESSION['oUsuarioImoney']->getPesgCodigo())->getNome();
               $_SESSION['Perfil']['GrupoUsuario']    =  $oAcessoGrupoUsuario->getDescricao();
                $_SESSION['Perfil']['Empresa']         = $_SESSION['oEmpresa']->getEmpFantasia();
                $_SESSION['Perfil']['EmpCodigo']       = $_SESSION['oEmpresa']->getEmpCodigo();
               }


                $sHeader = "?";
                header("Location: ".$sHeader);
                die();
            break;

 			case "Logoff":
				session_unset();
				unset($_SESSION['oUsuarioImoney']);								
				unset($_SESSION['oEmpresa']);								
				unset($_SESSION['Perfil']);
				$sHeader = "?action=Login.preparaFormulario";
 			break;

 		}
			header("Location: ".$sHeader);
			exit();
 	
 	}


    public function atualizaPerfil(){
          $oFachadaView = new FachadaViewBD();
          $oFachadaSys = new FachadaSysBD();
          $_REQUEST['voVAcessoUsuarioUnidade'] = $oFachadaView->recuperaTodosVAcessoUsuarioUnidadePorPessoaPorUnidade($_GET['nCodUsuario'],$_GET['nCodUnidade']);
          $_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresaPorUnidade($_GET['nCodUnidade']);

         include_once('view/permissao/login/perfil_ajax.php');
         exit();
    }
	
    public function recuperaUnidadePorEmpresa(){
        $oFachadaFinanceiro = new FachadaFinanceiroBD();

        $_REQUEST['voUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_REQUEST['nEmpCodigo']);

        include_once('view/permissao/login/unidade_ajax.php');

        exit();

    }

}
 ?>
