<?php
 class AcessoUsuarioUnidadeCTR implements IControle{

 	public function AcessoUsuarioUnidadeCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

 		$voAcessoUsuarioUnidade = $oFachada->recuperarTodosAcessoUsuarioUnidade();

 		$_REQUEST['voAcessoUsuarioUnidade'] = $voAcessoUsuarioUnidade;
 		$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();

		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

		//$_REQUEST['voSysEmpresa'] = $oFachada->recuperarTodosSysEmpresa();

		$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();


 		include_once("view/permissao/acesso_usuario_empresa/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$oAcessoUsuarioUnidade = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoUsuarioUnidade = ($_POST['fIdAcessoUsuarioUnidade'][0]) ? $_POST['fIdAcessoUsuarioUnidade'][0] : $_GET['nIdAcessoUsuarioUnidade'];

 			if($nIdAcessoUsuarioUnidade){
 				$vIdAcessoUsuarioUnidade = explode("||",$nIdAcessoUsuarioUnidade);
 				$oAcessoUsuarioUnidade = $oFachada->recuperarUmAcessoUsuarioUnidade($vIdAcessoUsuarioUnidade[0],$vIdAcessoUsuarioUnidade[1],$vIdAcessoUsuarioUnidade[2]);
 			}
 		}

 		$_REQUEST['oAcessoUsuarioEmpresa'] = ($_SESSION['oAcessoUsuarioEmpresa']) ? $_SESSION['oAcessoUsuarioEmpresa'] : $oAcessoUsuarioEmpresa;
 		unset($_SESSION['oAcessoUsuarioEmpresa']);

 		$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();

		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

		$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_usuario_empresa/detalhe.php");
 		else
 			include_once("view/permissao/acesso_usuario_empresa/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir" And $sOP != "Excluir2"){
 			$oAcessoUsuarioEmpresa = $oFachada->inicializarAcessoUsuarioUnidade($_POST['fCodUsuario'],$_POST['fCodGrupoUsuario'],$_POST['fCodPlanoContas']);
 			$_SESSION['oAcessoUsuarioEmpresa'] = $oAcessoUsuarioEmpresa;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodUsuario", $oAcessoUsuarioUnidade->getCodUsuario(), "number", "y");
			$oValidate->add_number_field("CodGrupoUsuario", $oAcessoUsuarioUnidade->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_number_field("CodPlanoContas", $oAcessoUsuarioUnidade->getCodPlanoContas(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoUsuarioUnidade.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuarioUnidade=".$_POST['fCodUsuario']."||".$_POST['fCodGrupoUsuario']."||".$_POST['fCodPlanoContas']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoUsuarioUnidade($oAcessoUsuarioUnidade)){
 					unset($_SESSION['oAcessoUsuarioUnidade']);
 					$_SESSION['sMsg'] = "acesso_usuario_empresa inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuarioUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o acesso_usuario_empresa!";
 					$sHeader = "?bErro=1&action=AcessoUsuarioUnidade.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoUsuarioUnidade($oAcessoUsuarioUnidade)){
 					unset($_SESSION['oAcessoUsuarioUnidade']);
 					$_SESSION['sMsg'] = "acesso_usuario_empresa alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuarioUnidade.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o acesso_usuario_empresa!";
 					$sHeader = "?bErro=1&action=AcessoUsuarioUnidade.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuarioUnidade=".$_POST['fCodUsuario']."||".$_POST['fEmpCodigo']."||".$_POST['fCodGrupoUsuario']."||".$_POST['fCodPlanoContas']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoUsuarioUnidade']);
				$nRegistros = count($voRegistros);

				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirAcessoUsuarioUnidade($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "acesso_usuario_empresa(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuarioUnidade.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) acesso_usuario_empresa!";
 					$sHeader = "?bErro=1&action=AcessoUsuarioUnidade.preparaLista";
 				}
 			break;
            case "Excluir2":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoUsuarioUnidade']);
				$nRegistros = count($voRegistros);

				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirAcessoUsuarioUnidade($sCampoChave);\n");
				}

                exit();
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
