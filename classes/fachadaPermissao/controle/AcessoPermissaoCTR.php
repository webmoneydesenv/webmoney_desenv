<?php
 class AcessoPermissaoCTR implements IControle{
 
 	public function AcessoPermissaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
  		$nIdAcessoPermissao = ($_POST['fIdAcessoGrupoUsuario'][0]) ? $_POST['fIdAcessoGrupoUsuario'][0] : $_GET['IdAcessoGrupoUsuario'];
		$voAcessoPermissao = $oFachada->recuperarTodosAcessoPermissaoPorGrupoUsuario($nIdAcessoPermissao);
 		$_REQUEST['voAcessoPermissao'] = $voAcessoPermissao;
		
		$_REQUEST['voAcessoModulo'] = $oFachada->recuperarTodosAcessoModulo();

		$_REQUEST['oGrupoUsuario'] = $oFachada->recuperarUmAcessoGrupoUsuario($nIdAcessoPermissao);

		include_once("view/permissao/acesso_permissao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oAcessoPermissao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoPermissao = ($_POST['fIdAcessoPermissao'][0]) ? $_POST['fIdAcessoPermissao'][0] : $_GET['nIdAcessoPermissao'];
 	
 			if($nIdAcessoPermissao){
 				$vIdAcessoPermissao = explode("||",$nIdAcessoPermissao);
 				$oAcessoPermissao = $oFachada->recuperarUmAcessoPermissao();
 			}
 		}
 		
 		$_REQUEST['oAcessoPermissao'] = ($_SESSION['oAcessoPermissao']) ? $_SESSION['oAcessoPermissao'] : $oAcessoPermissao;
 		unset($_SESSION['oAcessoPermissao']);
 
 		$_REQUEST['voAcessoTransacaoModulo'] = $oFachada->recuperarTodosAcessoTransacaoModulo();

		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_permissao/detalhe.php");
 		else
 			include_once("view/permissao/acesso_permissao/insere_altera.php");
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
         $bResultado = true;
		$i=0;
	    set_time_limit(3600); // 30 minutos



        if($oFachada->excluirAcessoPermissaoPorGrupoUsuario($_POST['fCodGrupoUsuario'])){
		   foreach($_POST['fCodTransacaoModulo'] as $nCodTransacaoModulo){
                $oPermissao = $oFachada->inicializarAcessoPermissao($nCodTransacaoModulo, $_POST['fCodGrupoUsuario']);
                $bResultado &= $oFachada->inserirAcessoPermissao($oPermissao);
				$i++;
            }

        } else
            $bResultado = false;
 		 
        if($bResultado){
 			$_SESSION['oPermissao'] = array();
            $_SESSION['sMsg'] = "Permiss&atilde;o alterada com sucesso!";
        } else {
            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Permiss&atilde;o!";
        }

 		header("Location: index.php?action=AcessoPermissao.preparaLista&IdAcessoGrupoUsuario=".$_POST['fCodGrupoUsuario']);

        die();

 
	}
 }
 
 
 ?>
