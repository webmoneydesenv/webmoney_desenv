<?php
 class AcessoLogExclusaoCTR implements IControle{

 	public function AcessoLogExclusaoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

 		$voAcessoLogExclusao = $oFachada->recuperarTodosAcessoLogExclusao();

 		$_REQUEST['voAcessoLogExclusao'] = $voAcessoLogExclusao;


 		include_once("view/permissao/acesso_log_exclusao/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$oAcessoLogExclusao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoLogExclusao = ($_POST['fIdAcessoLogExclusao'][0]) ? $_POST['fIdAcessoLogExclusao'][0] : $_GET['nIdAcessoLogExclusao'];

 			if($nIdAcessoLogExclusao){
 				$vIdAcessoLogExclusao = explode("||",$nIdAcessoLogExclusao);
 				$oAcessoLogExclusao = $oFachada->recuperarUmAcessoLogExclusao($vIdAcessoLogExclusao[0]);
 			}
 		}

 		$_REQUEST['oAcessoLogExclusao'] = ($_SESSION['oAcessoLogExclusao']) ? $_SESSION['oAcessoLogExclusao'] : $oAcessoLogExclusao;
 		unset($_SESSION['oAcessoLogExclusao']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_log_exclusao/detalhe.php");
 		else
 			include_once("view/permissao/acesso_log_exclusao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oAcessoLogExclusao = $oFachada->inicializarAcessoLogExclusao($_POST['fCodExclusao'],mb_convert_case($_POST['fFa'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fMotivo'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oAcessoLogExclusao'] = $oAcessoLogExclusao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_text_field("Fa", $oAcessoLogExclusao->getFa(), "text", "y");
			//$oValidate->add_text_field("RealizadoPor", $oAcessoLogExclusao->getRealizadoPor(), "text", "y");
			//$oValidate->add_date_field("Data", $oAcessoLogExclusao->getData(), "date", "y");
			//$oValidate->add_text_field("Motivo", $oAcessoLogExclusao->getMotivo(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoLogExclusao.preparaFormulario&sOP=".$sOP."&nIdAcessoLogExclusao=".$_POST['fCodExclusao']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoLogExclusao($oAcessoLogExclusao)){
 					unset($_SESSION['oAcessoLogExclusao']);
 					$_SESSION['sMsg'] = "acesso_log_exclusao inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLogExclusao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o acesso_log_exclusao!";
 					$sHeader = "?bErro=1&action=AcessoLogExclusao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoLogExclusao($oAcessoLogExclusao)){
 					unset($_SESSION['oAcessoLogExclusao']);
 					$_SESSION['sMsg'] = "acesso_log_exclusao alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLogExclusao.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o acesso_log_exclusao!";
                    $sHeader = "?bErro=1&action=AcessoLogExclusao.preparaFormulario&sOP=".$sOP."&nIdAcessoLogExclusao=".$_POST['fCodExclusao'];
 				}
 			break;
 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdAcessoLogExclusao']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirAcessoLogExclusao($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "acesso_log_exclusao(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLogExclusao.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) acesso_log_exclusao!";
 					$sHeader = "?bErro=1&action=AcessoLogExclusao.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
