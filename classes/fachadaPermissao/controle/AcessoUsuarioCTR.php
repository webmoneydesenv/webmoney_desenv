
<?php
 class AcessoUsuarioCTR implements IControle{

 	public function AcessoUsuarioCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

 		$voAcessoUsuario = $oFachada->recuperarTodosAcessoUsuario();

 		$_REQUEST['voAcessoUsuario'] = $voAcessoUsuario;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();



 		include_once("view/permissao/acesso_usuario/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();

 		$oAcessoUsuario = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] == "AlterarSenha"){
 			$nIdAcessoUsuario = ($_POST['fIdAcessoUsuario'][0]) ? $_POST['fIdAcessoUsuario'][0] : $_GET['nIdAcessoUsuario'];

 			if($nIdAcessoUsuario){
 				$vIdAcessoUsuario = explode("||",$nIdAcessoUsuario);
 				$oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($vIdAcessoUsuario[0]);
                if($_REQUEST['sOP'] == 'Alterar'){
                    $_REQUEST['voAcessoUsuarioUnidade'] = $oFachada->recuperarTodosAcessoUsuarioUnidadePorUsuario($vIdAcessoUsuario[0]);
                }
 			}
 		}

        $_REQUEST['oAcessoUsuario'] = ($_SESSION['oAcessoUsuario']) ? $_SESSION['oAcessoUsuario'] : $oAcessoUsuario;
 		unset($_SESSION['oAcessoUsuario']);

        $sGrupo = '8'; // Unidade
        $_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
        $_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();

        switch($_REQUEST['sOP']){
			case "Detalhar":
	 			include_once("view/permissao/acesso_usuario/detalhe.php");
			break;
			case "Cadastrar":
			case "Alterar":
                $oFachadaView = new FachadaViewBD();
                $_REQUEST['voVPessoaGeralFormatada'] = $oFachadaView->recuperarTodosVPessoaGeralFormatadaFormulario();

	 			include_once("view/permissao/acesso_usuario/insere_altera.php");
			break;
			case "AlterarSenha":
	 			include_once("view/permissao/acesso_usuario/alterar_senha.php");
			break;
			case "LimparSenha":
				$_REQUEST['voAcessoUsuario'] = $oFachada->recuperarTodosAcessoUsuario();
				include_once("view/permissao/acesso_usuario/limpar_senha.php");
			break;
		}

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir" && $sOP != "AlterarSenha" && $sOP != 'LimparSenha'){
			if($sOP == 'Cadastrar')
				$_POST['fSenha'] = md5('carmona123');

            $sNome =  explode("_",$_POST['fLogin']);
            $_POST['fNome'] = $sNome[0] . " " . $sNome[1];
 			$oAcessoUsuario = $oFachada->inicializarAcessoUsuario($_POST['fCodUsuario'],$_POST['fPesgCodigo'],$_POST['fNome'],$_POST['fLogin'],$_POST['fSenha'],$_POST['fAtivo']);
 			$_SESSION['oAcessoUsuario'] = $oAcessoUsuario;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			if($sOP == 'Alterar')
				$oValidate->add_number_field("CodUsuario", $oAcessoUsuario->getCodUsuario(), "number", "y");
			$oValidate->add_text_field("Nome", $oAcessoUsuario->getPesgCodigo(), "text", "y");
			$oValidate->add_text_field("Login", $oAcessoUsuario->getLogin(), "text", "y");
			$oValidate->add_text_field("Senha", $oAcessoUsuario->getSenha(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oAcessoUsuario->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				header("Location: ".$sHeader);
 				die();
 			}
		}

 		switch($sOP){
 			case "Cadastrar":
                $bResultado = true;

 				if($nIdUsuario = $oFachada->inserirAcessoUsuario($oAcessoUsuario)){
 					unset($_SESSION['oAcessoUsuario']);

						//$item = 1;
						$cont = count($_REQUEST['fUnidade']);

						for($i = 0 ; $cont > $i  ;$i++){
							// inserir todos os itens da ase do formulario no banco
							$_POST['fItem'] = $item;
							$oAcessoUsuarioUnidade = $oFachada->inicializarAcessoUsuarioUnidade($nIdUsuario,$_POST['fGrupoUsuario'][$i],$_REQUEST['fUnidade'][$i]);
						    $bResultado &= $oFachada->inserirAcessoUsuarioUnidade($oAcessoUsuarioUnidade);
							$item++;
						}
                    if($bResultado){
                      $_SESSION['sMsg'] = "Usu&aacute;rio inserido com sucesso!";
 					  $sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";
                    }else{
                        $oFachada->excluirAcessoUsuarioUnidadeFisicamentePorUsuario($nIdUsuario);
                        $oFachada->excluirFisicamenteAcessoUsuario($nIdUsuario);
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Usu&aacute;rio!";
 					    $sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
                    }

 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel inserir o Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoUsuario($oAcessoUsuario)){
                   	// apagar todos os itens da ase do banco
					$oFachada->excluirAcessoUsuarioUnidadeFisicamentePorUsuario($oAcessoUsuario->getCodUsuario());
						$item = 1;
						$cont = count($_REQUEST['fUnidade']);

						for($i = 0 ; $cont > $i  ;$i++){
							// inserir todos os itens da ase do formulario no banco
							$_POST['fItem'] = $item;
							$oAcessoUsuarioUnidade = $oFachada->inicializarAcessoUsuarioUnidade($oAcessoUsuario->getCodUsuario(),$_POST['fGrupoUsuario'][$i],$_REQUEST['fUnidade'][$i]);
							$oFachada->inserirAcessoUsuarioUnidade($oAcessoUsuarioUnidade);
							$item++;
						}


 					unset($_SESSION['oAcessoUsuario']);
 					$_SESSION['sMsg'] = "Usu&aacute;rio alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				}
 			break;
 			case "Excluir":

				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoUsuario']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirAcessoUsuario($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Usu&aacute;rio(s) excluído(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaLista";
 				}
 			break;
			case "AlterarSenha":
				$oUsuario = $oFachada->recuperarUmUsuarioPorLogin($_SESSION['oUsuarioImoney']->getLogin());
				if(md5($_REQUEST['fSenhaAtual']) == $oUsuario->getSenha()){
					if($_REQUEST['fSenha1'] == $_REQUEST['fSenha2']){
						$oUsuario->setSenha(md5($_REQUEST['fSenha1']));
						if($oFachada->alterarAcessoUsuario($oUsuario)){
					 		$_SESSION['oUsuario'] = array();
							$_SESSION['sMsg'] = "Senha alterada com sucesso!";
							$_SESSION['oUsuarioImoney']->setSenha(md5($_REQUEST['fSenha1']));
							$sHeader = "?bErro=0";
						} else {
							$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Senha!";
							$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
						}
					}else{
						$_SESSION['sMsg'] = "Os campos 'Nova Senha' e 'Repita Nova Senha' est&atilde;o diferentes!";
						$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
					}
				}else{
					$_SESSION['sMsg'] = "A 'Senha Atual' est&aacute; incorreta!";
					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
				}
			break;
			case "LimparSenha":
			    $oUsuario = $oFachada->recuperarUmAcessoUsuario($_REQUEST['fCodUsuario']);
				$sSenha = "carmona123";
				$oUsuario->setSenha(md5($sSenha));

 				if($oFachada->alterarAcessoUsuario($oUsuario)){
			 		$_SESSION['oUsuario'] = array();
					$_SESSION['sMsg'] = "Senha alterada com sucesso!";
 					$sHeader = "?bErro=0";

 				} else {
 					$_SESSION['sMsg'] = "Não foi possível alterar a Senha!";
 					$sHeader = "?bErro=2&action=Usuario.preparaFormulario&sOP=".$sOP."&nIdUsuario=".$_POST['fCodUsuario']."";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }
/*

 class AcessoUsuarioCTR implements IControle{

 	public function AcessoUsuarioCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

 		$voAcessoUsuario = $oFachada->recuperarTodosAcessoUsuario();

 		$_REQUEST['voAcessoUsuario'] = $voAcessoUsuario;
 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();



 		include_once("view/permissao/acesso_usuario/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$oAcessoUsuario = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] == "AlterarSenha"){
 			$nIdAcessoUsuario = ($_POST['fIdAcessoUsuario'][0]) ? $_POST['fIdAcessoUsuario'][0] : $_GET['nIdAcessoUsuario'];

 			if($nIdAcessoUsuario){
 				$vIdAcessoUsuario = explode("||",$nIdAcessoUsuario);
 				$oAcessoUsuario = $oFachada->recuperarUmAcessoUsuario($vIdAcessoUsuario[0]);
 			}
 		}

 		$_REQUEST['oAcessoUsuario'] = ($_SESSION['oAcessoUsuario']) ? $_SESSION['oAcessoUsuario'] : $oAcessoUsuario;
 		unset($_SESSION['oAcessoUsuario']);

 		$_REQUEST['voAcessoGrupoUsuario'] = $oFachada->recuperarTodosAcessoGrupoUsuario();


 		switch($_REQUEST['sOP']){
			case "Detalhar":
	 			include_once("view/permissao/acesso_usuario/detalhe.php");
			break;
			case "Cadastrar":
			case "Alterar":
	 			include_once("view/permissao/acesso_usuario/insere_altera.php");
			break;
			case "AlterarSenha":
	 			include_once("view/permissao/acesso_usuario/alterar_senha.php");
			break;
		}

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir" && $sOP != "AlterarSenha"){
			if($sOP == 'Cadastrar')
				$_POST['fSenha'] = md5('carmona123');

 			$oAcessoUsuario = $oFachada->inicializarAcessoUsuario($_POST['fCodUsuario'],$_POST['fNome'],$_POST['fLogin'],$_POST['fSenha'],$_POST['fAtivo']);
 			$_SESSION['oAcessoUsuario'] = $oAcessoUsuario;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			if($sOP == 'Alterar')
				$oValidate->add_number_field("CodUsuario", $oAcessoUsuario->getCodUsuario(), "number", "y");
			$oValidate->add_number_field("Grupo de Usu&aacute;rio", $oAcessoUsuario->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_text_field("Nome", $oAcessoUsuario->getNome(), "text", "y");
			$oValidate->add_text_field("Login", $oAcessoUsuario->getLogin(), "text", "y");
			$oValidate->add_text_field("Senha", $oAcessoUsuario->getSenha(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoUsuario->getAtivo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				header("Location: ".$sHeader);
 				die();
 			}
		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoUsuario($oAcessoUsuario)){
 					unset($_SESSION['oAcessoUsuario']);
 					$_SESSION['sMsg'] = "Usu&aacute;rio inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel inserir o Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoUsuario($oAcessoUsuario)){
 					unset($_SESSION['oAcessoUsuario']);
 					$_SESSION['sMsg'] = "Usu&aacute;rio alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fIdAcessoUsuario'] as $nIdAcessoUsuario){
 					$vIdAcessoUsuario = explode("||",$nIdAcessoUsuario);
 					$bResultado &= $oFachada->excluirAcessoUsuario($vIdAcessoUsuario[0]);
 				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Usu&aacute;rio(s) excluído(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUsuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Usu&aacute;rio!";
 					$sHeader = "?bErro=1&action=AcessoUsuario.preparaLista";
 				}
 			break;
			case "AlterarSenha":
				$oUsuario = $oFachada->recuperarUmUsuarioPorLogin($_SESSION['oUsuarioImoney']->getLogin());
				if(md5($_REQUEST['fSenhaAtual']) == $oUsuario->getSenha()){
					if($_REQUEST['fSenha1'] == $_REQUEST['fSenha2']){
						$oUsuario->setSenha(md5($_REQUEST['fSenha1']));
						if($oFachada->alterarAcessoUsuario($oUsuario)){
					 		$_SESSION['oUsuario'] = array();
							$_SESSION['sMsg'] = "Senha alterada com sucesso!";
							$_SESSION['oUsuarioImoney']->setSenha(md5($_REQUEST['fSenha1']));
							$sHeader = "?bErro=0";
						} else {
							$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Senha!";
							$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
						}
					}else{
						$_SESSION['sMsg'] = "Os campos 'Nova Senha' e 'Repita Nova Senha' est&atilde;o diferentes!";
						$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
					}
				}else{
					$_SESSION['sMsg'] = "A 'Senha Atual' est&aacute; incorreta!";
					$sHeader = "?bErro=1&action=AcessoUsuario.preparaFormulario&sOP=".$sOP."&nIdAcessoUsuario=".$_POST['fCodUsuario'];
				}
			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }

*/
 ?>
