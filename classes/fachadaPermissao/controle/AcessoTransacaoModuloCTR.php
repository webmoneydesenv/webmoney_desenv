<?php
 class AcessoTransacaoModuloCTR implements IControle{
 
 	public function AcessoTransacaoModuloCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
		
		$nCodModulo = ($_REQUEST['fIdAcessoModulo'][0]) ? $_REQUEST['fIdAcessoModulo'][0] : $_REQUEST['nIdModulo'];
		
 		$voAcessoTransacaoModulo = $oFachada->recuperarTodosAcessoTransacaoModuloPorModulo($nCodModulo);
 		$oModulo = $oFachada->recuperarUmAcessoModulo($nCodModulo);
		
		$_REQUEST['voAcessoTransacaoModulo'] = $voAcessoTransacaoModulo;
		$_REQUEST['oModulo'] = $oModulo;
		
 		include_once("view/permissao/acesso_transacao_modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oAcessoTransacaoModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoTransacaoModulo = ($_POST['fIdAcessoTransacaoModulo'][0]) ? $_POST['fIdAcessoTransacaoModulo'][0] : $_GET['nIdAcessoTransacaoModulo'];
 	
 			if($nIdAcessoTransacaoModulo){
 				$vIdAcessoTransacaoModulo = explode("||",$nIdAcessoTransacaoModulo);
 				$oAcessoTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModulo($vIdAcessoTransacaoModulo[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoTransacaoModulo'] = ($_SESSION['oAcessoTransacaoModulo']) ? $_SESSION['oAcessoTransacaoModulo'] : $oAcessoTransacaoModulo;
 		unset($_SESSION['oAcessoTransacaoModulo']);
 
 		$_REQUEST['oModulo'] = $oFachada->recuperarUmAcessoModulo($_REQUEST['nIdModulo']);

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/permissao/acesso_transacao_modulo/detalhe.php");
 		else
 			include_once("view/permissao/acesso_transacao_modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoTransacaoModulo = $oFachada->inicializarAcessoTransacaoModulo($_POST['fCodTransacaoModulo'],$_POST['fCodModulo'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oAcessoTransacaoModulo'] = $oAcessoTransacaoModulo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			if($sOP == 'Alterar')
				$oValidate->add_number_field("CodTransacaoModulo", $oAcessoTransacaoModulo->getCodTransacaoModulo(), "number", "y");
			$oValidate->add_number_field("CodModulo", $oAcessoTransacaoModulo->getCodModulo(), "number", "y");
			$oValidate->add_text_field("Descricao", $oAcessoTransacaoModulo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoTransacaoModulo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoTransacaoModulo=".$_POST['fCodTransacaoModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoTransacaoModulo($oAcessoTransacaoModulo)){
 					unset($_SESSION['oAcessoTransacaoModulo']);
 					$_SESSION['sMsg'] = "Transa&ccedil;&atilde;o inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo();
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Transa&ccedil;&atilde;o!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoTransacaoModulo($oAcessoTransacaoModulo)){
 					unset($_SESSION['oAcessoTransacaoModulo']);
 					$_SESSION['sMsg'] = "Transa&ccedil;&atilde;o alterada com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista&nIdModulo=".$oAcessoTransacaoModulo->getCodModulo()."&nCodTransacaoModulo=".$oAcessoTransacaoModulo->getCodTransacaoModulo();;
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Transa&ccedil;&atilde;o!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaFormulario&sOP=".$sOP."&nIdAcessoTransacaoModulo=".$_POST['fCodTransacaoModulo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdAcessoTransacaoModulo']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirAcessoTransacaoModulo($sCampoChave);\n");
				}				
		
		
 				if($bResultado){
 					$_SESSION['sMsg'] = "Transa&ccedil;&atilde;o(s) exclu�da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoTransacaoModulo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Transa&ccedil;&atilde;o(s)!";
 					$sHeader = "?bErro=1&action=AcessoTransacaoModulo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
