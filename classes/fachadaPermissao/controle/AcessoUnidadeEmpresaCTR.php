<?php
 class AcessoUnidadeEmpresaCTR implements IControle{

 	public function AcessoUnidadeEmpresaCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();

 		$voAcessoUnidadeEmpresa = $oFachada->recuperarTodosMnyPlanoContasPorUnidade();

 		$_REQUEST['voAcessoUnidadeEmpresa'] = $voAcessoUnidadeEmpresa;

 		include_once("view/Permissao/acesso_unidade_empresa/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();
 		$oFachadaSys = new FachadaSysBD();

 		$oAcessoUnidadeEmpresa = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoUnidadeEmpresa = ($_POST['fIdAcessoUnidadeEmpresa'][0]) ? $_POST['fIdAcessoUnidadeEmpresa'][0] : $_GET['nIdAcessoUnidadeEmpresa'];

 			if($nIdAcessoUnidadeEmpresa){
 				$vIdAcessoUnidadeEmpresa = explode("||",$nIdAcessoUnidadeEmpresa);
 				$voAcessoUnidadeEmpresa = $oFachada->recuperarTodosAcessoUnidadeEmpresaPorUnidade($vIdAcessoUnidadeEmpresa[0]);
 			}
 		}

        $_REQUEST['voEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
        $_REQUEST['voUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorUnidade();

 		$_REQUEST['voAcessoUnidadeEmpresa'] = ($_SESSION['voAcessoUnidadeEmpresa']) ? $_SESSION['voAcessoUnidadeEmpresa'] : $voAcessoUnidadeEmpresa;
 		unset($_SESSION['oAcessoUnidadeEmpresa']);



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/Permissao/acesso_unidade_empresa/detalhe.php");
 		else
 			include_once("view/Permissao/acesso_unidade_empresa/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oAcessoUnidadeEmpresa = $oFachada->inicializarAcessoUnidadeEmpresa($_POST['fCodUnidade'],$_POST['fEmpCodigo']);
 			$_SESSION['oAcessoUnidadeEmpresa'] = $oAcessoUnidadeEmpresa;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("CodUnidade", $oAcessoUnidadeEmpresa->getCodUnidade(), "number", "y");



 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoUnidadeEmpresa.preparaFormulario&sOP=".$sOP."&nIdAcessoUnidadeEmpresa=".$_POST['fCodUnidade']."||".$_POST['fEmpCodigo'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
            case "Alterar":
                $nErro=0;
               if ($sOP =='Alterar')
                $oFachada->excluirEmpresasPorUnidade($oAcessoUnidadeEmpresa->getCodUnidade());
                foreach($oAcessoUnidadeEmpresa->getEmpCodigo() as $nEmpresa){
                    $oAcessoUnidadeEmpresa->setEmpCodigo($nEmpresa);
                    if(!$oFachada->inserirAcessoUnidadeEmpresa($oAcessoUnidadeEmpresa))
                        $nErro++;
                }
 				   if($nErro ==0){
 					unset($_SESSION['oAcessoUnidadeEmpresa']);
 					$_SESSION['sMsg'] = "Unidade vinculada com sucesso!";
                       $sHeader = "?bErro=0&action=AcessoUnidadeEmpresa.preparaLista";
                   }else{
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel vincular Unidade!";
                       $sHeader = "?bErro=1&action=AcessoUnidadeEmpresa.preparaFormulario&sOP=".$sOP;
                   }



 			break;

 			case "Excluir":
  				$bResultado = true;
  				$voRegistros = explode("____",$_REQUEST['fIdAcessoUnidadeEmpresa']);
                 $nRegistros = count($voRegistros);

                 foreach($voRegistros as $oRegistros){
                     $sCampoChave = str_replace("||", ",", $oRegistros);
                     eval("\$bResultado &= \$oFachada->excluirAcessoUnidadeEmpresa($sCampoChave);\n");
                 }

 				if($bResultado){
 					$_SESSION['sMsg'] = "acesso_unidade_empresa(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoUnidadeEmpresa.preparaLista";
 				} else {
 					$_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o(s) acesso_unidade_empresa!";
 					$sHeader = "?bErro=1&action=AcessoUnidadeEmpresa.preparaLista";
 				}
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
