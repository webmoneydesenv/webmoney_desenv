<?

/*
Classe Responsável por todas as operações no Banco de Dados
*/

class BancoDeDados implements IBancoDeDados{

	private $sSGBD;
	private $sBanco;
	private $sServidor;
	private $sUsuario;
	private $sSenha;
	private $oPDO;
	private $oStmt;
	
	public function __construct($sSGBD,$sBanco,$sServidor,$sUsuario,$sSenha){
		$this->sSGBD = $sSGBD;
		$this->sBanco = $sBanco;
		$this->sServidor = $sServidor;
		$this->sUsuario = $sUsuario;
		$this->sSenha = $sSenha;
	}
	
	//Faz a conexão com o Banco de Dados
	public function iniciaConexaoBanco(){
		if(!$this->PDO){
			$this->oPDO = new PDO($this->sSGBD.":dbname=".$this->sBanco.";charset=utf8;host=".$this->sServidor,$this->sUsuario,$this->sSenha);
			if($this->oPDO){
				return true;
			}
			return false;
		}
		return true;
	}
	
	//Desfaz a conexão com o Banco de Dados
	public function terminaConexaoBanco(){
		if (!$this->oPDO){
			return 0;
		}
		else{
			unset($this->oStmt);
			unset($this->oPDO);
			return 1;
		}
	}
	
	//Insere um registro no tabela mandada como parametro
	public function insereRegistroNoBanco($sTabela,$sCampos,$sValores){
		if($this->iniciaConexaoBanco()){

         $sSql = " INSERT INTO $sTabela($sCampos) VALUES ($sValores)";
        /*
        if($sTabela!='acesso_log'){
            echo  "<br>".$sSql;
            //die('aqui');
        }*/

			$this->oStmt = $this->oPDO->query($sSql);
			$bResultado = ($this->oStmt) ? true : false;
			$nId = $this->oPDO->lastInsertId();
			$this->terminaConexaoBanco();

			if($nId)
				return $nId;
			else 
				return $bResultado;
		}
	}
	
	//Recupera registro da tabela mandada com parametro *Variável $sComplemento
	//é opcional e se refere as clausulas adicionais como WHERE, ORDER BY, LIMIT
	//e etc.*
	public function recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//if($sTabelas == 'v_tramitacao')
				//die("SELECT $sCampos FROM $sTabelas $sComplemento");
		//echo "<br>".	 $sSql = "SELECT $sCampos FROM $sTabelas $sComplemento;";
		  $sSql = "SELECT $sCampos FROM $sTabelas $sComplemento";

            if ($this->oStmt = $this->oPDO->query($sSql)){
                while ($oReg = $this->oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}
				$this->terminaConexaoBanco();

                return $vObjeto;

			}
		}
	}

	
	
	//Recupera registro da tabela mandada usando procedure
	public function recuperaRegistrosProcedureDoBanco($sNomeProcedure, $sComplemento){
		if($this->iniciaConexaoBanco()){		
			 $sSql = "CALL $sNomeProcedure($sComplemento)";
			if ($this->oStmt = $this->oPDO->query($sSql)){
				while ($oReg = $this->oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}

				//LogCaminho::escreverCaminho("SELECT $sCampos FROM $sTabelas $sComplemento");
				return $vObjeto;
			}else{

                return false;
            }
            $this->terminaConexaoBanco();
		}
	}
	
	
	//Recupera registro da tabela mandada usando função
	public function recuperaRegistrosFuncaoDoBanco($sNomeFuncao,$sCampos, $sComplemento){
		if($this->iniciaConexaoBanco()){		
			//die("SELECT $sNomeFuncao($sComplemento)";
			 $sSql = "SELECT $sNomeFuncao($sComplemento) as $sCampos";
			if ($this->oStmt = $this->oPDO->query($sSql)){
				while ($oReg = $this->oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}
				$this->terminaConexaoBanco();
				//LogCaminho::escreverCaminho("SELECT $sCampos FROM $sTabelas $sComplemento");
				return $vObjeto;
			}
		}
	}

	//Recupera registro da tabela mandada com parametro *Variável $sComplemento
	//é opcional e se refere as clausulas adicionais como WHERE, ORDER BY, LIMIT
	//e etc.*
	public function recuperaRegistrosTabelaTemporariaDoBanco($sCampos,$sTabelas,$sComplemento){
		//die("CREATE TEMPORARY TABLE $sTabelas"."_temp SELECT $sCampos FROM $sTabelas $sComplemento; SELECT * FROM $sTabelas"."_temp;");
		//die("CREATE TEMPORARY TABLE $sTabelas"."_temp SELECT $sCampos FROM $sTabelas $sComplemento; SELECT $sCampos FROM $sTabelas"."_temp;");		
		if($this->iniciaConexaoBanco()){
			$sSql = "DROP TABLE $sTabelas"."_temp ";
		   	$this->oStmt = $this->oPDO->query($sSql);
		    //$sSql = "CREATE TEMPORARY TABLE $sTabelas"."_temp SELECT $sCampos FROM $sTabelas $sComplemento";			 
		    $sSql = "CREATE TEMPORARY TABLE $sTabelas"."_temp SELECT $sCampos FROM $sTabelas $sComplemento";			 
			if($this->oStmt = $this->oPDO->query($sSql)){	
				$sCamposSelect = explode('.',$sCampos);	
			 	//$sSql = "SELECT $sCamposSelect[1] FROM $sTabelas"."_temp;";
			 	$sSql = "SELECT * FROM $sTabelas"."_temp; ";
				if ($this->oStmt = $this->oPDO->query($sSql)){
					while ($oReg = $this->oStmt->fetchObject()){
						$vObjeto[] = $oReg;
					}
					$this->terminaConexaoBanco();
					//LogCaminho::escreverCaminho("SELECT $sCampos FROM $sTabelas $sComplemento");
					return $vObjeto;
				}
			}
		}
	}
	
	//Atera o campo de um registro da tabela mandada como parametro
	public function alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//if($sTabela=='mny_contrato_aditivo')
			//echo "<br>UPDATE $sTabela SET $sCampos $sComplemento";
			$this->oStmt = $this->oPDO->query("UPDATE $sTabela SET $sCampos $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("UPDATE $sTabela SET $sCampos $sComplemento");
			return $bResultado;
		}
	}
	
	//Exclui logicamente um registro da tabela
	public function excluiRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//if($sTabela=='acesso_usuario')
				//die("UPDATE $sTabela SET ativo=0 $sComplemento");
			$this->oStmt = $this->oPDO->query("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			return $bResultado;
		}
	}
	//Exclui logicamente um registro da tabela
	public function alteraGeralRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$this->oStmt = $this->oPDO->query("UPDATE $sTabela SET  $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			return $bResultado;
		}
	}
	
	//Exclui fisicamente um registro da tabela
	public function excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){		
            $sSql = "DELETE FROM $sTabela $sComplemento";
		   //die($sSql);
			$this->oStmt = $this->oPDO->query($sSql);
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();

			return $bResultado;
		}
	}

    //Exclui fisicamente um registro da tabela usando procedure
	public function excluiFisicamenteRegistrosDoBancoProcedure($sProcedure,$sParametros){
		if($this->iniciaConexaoBanco()){

            $sSql = "Call $sProcedure ($sParametros);";
           // $sSqlRetorno = "Select * From exclusao_temp";

            $this->oStmt = $this->oPDO->query($sSql);

				while ($oReg = $this->oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}

                $this->terminaConexaoBanco();

            return $vObjeto;

		}
	}
}
?>
