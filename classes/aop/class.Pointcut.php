<?php

class Pointcut
{
	var $className;
	var $functionName;
	var $notInClassName;
	var $notInFunctionName;
	var $advice;

    function Pointcut($advice, $class, $function, $nclass, $nfunction)
    {
    	// Defining Class(es)
		$this->className = (is_array($class) ? $class : split(",[ ]*", $class));

		// Defining Not In Class(es)
        $this->notInClassName = (is_array($nclass) ? $nclass : split(",[ ]*", $nclass));

        // Defining Function(s)
		$this->functionName = (is_array($function) ? $function : split(",[ ]*", $function));
		
        // Defining Not In Function(s)
		$this->notInFunctionName = (is_array($nfunction) ? $nfunction : split(",[ ]*", $nfunction));
		
    	// Remove start/end carriage return chars in the code
    	$this->advice = $advice;
    }
    

	function getClassName() { return $this->className; }

	function hasClassName($v) { return in_array($v, $this->className, true); }

	function getFunctionName() { return $this->functionName; }

	function hasFunctionName($v) { return in_array($v, $this->functionName, true); }
	
	function getNotInClassName() { return $this->notInClassName; }

	function hasNotInClassName($v) { return in_array($v, $this->notInClassName, true); } 
	
	function getNotInFunctionName() { return $this->notInFunctionName; }

	function hasNotInFunctionName($v) { return in_array($v, $this->notInFunctionName, true); }

	function & getAdvice() { return $this->advice; }
}

?>