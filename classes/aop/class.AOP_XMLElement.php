<?php


class AOP_XMLElement
{
	var $tag;
	var $value;
	var $attributes;
	var $childNodes;
	var $parentNode;


	function AOP_XMLElement($tag = "")
	{
		if ($tag != "") {
	    	$this->setTag($tag);
	    }
	    
	    $this->attributes = array();
	    $this->childNodes = array();
	}
	
	
	function setTag($tag) { $this->tag = $tag; }


	function getTag() { return $this->tag; }


	function setValue($value) { $this->value = $value; }


	function getValue() { return $this->value; }
	

	function setParentNode(& $parent) { $this->parentNode = & $parent; }


	function & getParentNode() { return $this->parentNode; }


	function setAttributes($attrs) { $this->attributes = $attrs; }


	function getAttributes() { return $this->attributes; }
	

	function setAttribute($attrName, $attrValue)
	{
    	$this->attributes[$attrName] = $attrValue;
	}
	

	function getAttribute($attrName) 
	{
		if ($this->hasAttribute($attrName)) {
			return $this->attributes[$attrName];
		}
		
		return null;
	}
	

	function hasAttribute($attrName)
	{
		if (array_key_exists($attrName, $this->attributes)) {
			return true;
		}

		return false;
	}
	

	function setChildNodes($nodes) { $this->childNodes = $nodes; }
	

	function & getChildNodes() { return $this->childNodes; }


	function addChildNode(& $node)
	{
		$this->childNodes[count($this->childNodes)] = & $node;
	}


	function & getChildNode($i)
	{
		if (array_key_exists($i, $this->childNodes)) {
			return $this->childNodes[$i];
		}
		
		return null;
	}
}


?>