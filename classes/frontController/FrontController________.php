<?
class FrontController{

	public function __construct(){

                $action = (isset($_GET['action'])) ? $_GET['action'] : "";
                $vAction = explode(".",$action);

                if(is_object($_SESSION['oUsuarioImoney']) || $action == "Login.processaFormulario"){

                    if(count($vAction) != 2){
                            include_once('controle/index.php');
                    } else {
                        if($vAction[0] == "Site" || $vAction[0] == "Login")
                            $bPemissao = true;
                        else
                            $bPemissao = $this->verificarPermissao();
//teste
                        if($bPemissao){
                            $sClasse = $vAction[0];
                            $sMetodo = $vAction[1];
                            $oFactoryControle = new FactoryControle();
                            $oControle = $oFactoryControle->getObject($sClasse);
                            $oControle->$sMetodo();
                        } else {
                            $_SESSION['sMsg'] = "Voc&ecirc; n&atilde;o tem permiss&atilde;o para acessar essa &aacute;rea!";
                            include_once('controle/index.php');
                        }
                    }
                } else {
                    include_once('controle/login/index.php');
                }
	}

        public function verificarPermissao(){
            $sOP = (!$_REQUEST['sOP']) ? "Visualizar" : $_REQUEST['sOP'];
            $oFachada = new FachadaPermissaoBD();
            $oTransacaoModulo = $oFachada->recuperarUmAcessoTransacaoModuloPorResponsavelOperacao($_REQUEST['action'], $sOP);
			//print_r($oTransacaoModulo);die();
            if(is_object($oTransacaoModulo)){
                if($oFachada->presenteAcessoPermissao($oTransacaoModulo->getCodTransacaoModulo(), $_SESSION['oUsuarioImoney']->getCodGrupoUsuario()))
                        return true;
               // die('aqui');
				return false;
            }
            return false;
        }
}
?>
