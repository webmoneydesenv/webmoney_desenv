<?


class FachadaSysBD {

    /**
 	 *
 	 * Método para inicializar um Objeto SysArquivo
 	 *
 	 * @return SysArquivo
 	 */
 	public function inicializarSysArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$sTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem){
 		$oSysArquivo = new SysArquivo();

		$oSysArquivo->setCodArquivo($nCodArquivo);
		$oSysArquivo->setNome($sNome);
		$oSysArquivo->setArquivo($sArquivo);
		$oSysArquivo->setExtensao($sExtensao);
		$oSysArquivo->setCodTipo($nCodTipo);
		$oSysArquivo->setTamanho($sTamanho);
		$oSysArquivo->setDataHora($dDataHora);
		$oSysArquivo->setRealizadoPor($sRealizadoPor);
		$oSysArquivo->setAtivo($nAtivo);
		$oSysArquivo->setContratoCodigo($nContratoCodigo);
		$oSysArquivo->setCodConciliacao($nCodConciliacao);
		$oSysArquivo->setMovCodigo($nMovCodigo);
		$oSysArquivo->setMovItem($nMovItem);
 		return $oSysArquivo;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto SysArquivo
 	 *
 	 * @return boolean
 	 */
 	public function inserirSysArquivo($oSysArquivo){

 		$oPersistencia = new Persistencia($oSysArquivo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}

 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto SysArquivo
 	 *
 	 * @return boolean
 	 */
 	public function alterarSysArquivo($oSysArquivo){
 		$oPersistencia = new Persistencia($oSysArquivo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}


 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto SysArquivo
 	 *
 	 * @return boolean
 	 */
 	public function excluirSysArquivo($nCodArquivo){
 		$oSysArquivo = new SysArquivo();

		$oSysArquivo->setCodArquivo($nCodArquivo);
  		$oPersistencia = new Persistencia($oSysArquivo);
    		//$bExcluir = $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();
    		$bExcluir = $oPersistencia->excluirFisicamente();

 		if($bExcluir)
  				return true;
  		return false;

 	}

 	/**
 	 *
 	 * Método para verificar se existe um Objeto SysArquivo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSysArquivo($nCodArquivo){
 		$oSysArquivo = new SysArquivo();

		$oSysArquivo->setCodArquivo($nCodArquivo);
  		$oPersistencia = new Persistencia($oSysArquivo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto SysArquivo da base de dados
 	 *
 	 * @return SysArquivo
 	 */
 	public function recuperarUmSysArquivo($nCodArquivo){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_arquivo = $nCodArquivo";
        //echo "Select $sCampos From $sTabelas $sComplemento";
        //die();
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysArquivo da base de dados
 	 *
 	 * @return SysArquivo[]
 	 */
 	public function recuperarTodosSysArquivo(){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo";
  		$sCampos = "cod_arquivo,nome,extensao,cod_tipo,tamanho,data_hora,realizado_por,contrato_codigo,cod_conciliacao,mov_codigo,mov_item,mny_cod_arquivo";
  		$sComplemento = "";
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}
     	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysArquivo da base de dados
 	 *
 	 * @return SysArquivo[]
 	 */
 	public function recuperarTodosSysArquivoPorMovimentoPorTipoDocumento($nMovCodigo,$nCodTipo){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo";
  		$sCampos = "cod_arquivo,nome,extensao,cod_tipo,tamanho,data_hora,realizado_por,contrato_codigo,cod_conciliacao,mov_codigo,mov_item,mny_cod_arquivo";
  		$sComplemento = "WHERE ativo=1 and cod_tipo=$nCodTipo AND  mov_codigo=".$nMovCodigo ;
        //echo "SELECT ". $sCampos . "FROM " . $sTabelas . $sComplemento;
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}


/**
 	 *
 	 * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
 	 *
 	 * @return MnyContratoArquivo[]
 	 */
 	public function recuperarTodosSysArquivoPorContratoTipoArquivo($nCodContrato, $nContratoDocTipo){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo";
  		$sCampos = "cod_arquivo,mny_contrato_pessoa.contrato_codigo AS contrato_codigo,mny_contrato_doc_tipo.cod_contrato_doc_tipo as cod_contrato_doc_tipo,nome,realizado_por,item as ativo";
  		$sComplemento = "INNER JOIN mny_contrato_pessoa ON sys_arquivo.contrato_codigo = mny_contrato_pessoa.contrato_codigo
						 INNER JOIN mny_contrato_doc_tipo ON mny_contrato_doc_tipo.cod_contrato_doc_tipo = sys_arquivo.cod_tipo
						 WHERE sys_arquivo.ativo=1 and mny_contrato_pessoa.contrato_codigo=" .  $nCodContrato .   " AND  cod_tipo=" . $nContratoDocTipo ;
	//echo "SELECT " . $sCampos . " FROM " .$sTabelas . " ". $sComplemento;

  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}



    /**
 	 *
 	 * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
 	 *
 	 * @return MnyContratoArquivo[]
 	 */
 	public function recuperarTodosSysArquivoDetalheMovimento($nMovCodigo){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo arq";
  		$sCampos = "arq.cod_arquivo, arq.nome, doc_tipo.descricao as  cod_tipo";
  		$sComplemento = "INNER JOIN mny_movimento mov ON mov.mov_contrato = arq.contrato_codigo
        Inner Join mny_contrato_doc_tipo doc_tipo On doc_tipo.cod_contrato_doc_tipo = arq.cod_tipo
        WHERE mov.mov_codigo = ".$nMovCodigo."  And ISNULL(arq.mov_codigo)";
//        echo "Select $sCampos From $sTabelas $sComplemento";
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}

  /**
 	 *
 	 * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
 	 *
 	 * @return MnyContratoArquivo[]
 	 */
 	public function recuperarSysArquivoDetalheMovimentoDespesaCaixa($nMovCodigo){
 		$oSysArquivo = new SysArquivo();
  		$oPersistencia = new Persistencia($oSysArquivo);
  		$sTabelas = "sys_arquivo arq";
  		$sCampos = "arq.cod_arquivo, arq.nome, doc_tipo.descricao as  cod_tipo";
  		$sComplemento = "INNER JOIN mny_movimento mov ON mov.mov_codigo = arq.mov_codigo
        Inner Join mny_contrato_doc_tipo doc_tipo On doc_tipo.cod_contrato_doc_tipo = arq.cod_tipo
        WHERE mov.mov_codigo = ".$nMovCodigo;
        //echo "Select $sCampos From $sTabelas $sComplemento";
  		$voSysArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysArquivo)
  			return $voSysArquivo;
  		return false;
 	}




 /**
 	 *
 	 * Método para inicializar um Objeto SysBanco
 	 *
 	 * @return SysBanco  
 	 */
 	public function inicializarSysBanco($nBcoCodigo,$sBcoNome,$sBcoSigla,$sBcoInc,$sBcoAlt,$nAtivo){
 		$oSysBanco = new SysBanco();
 		
		$oSysBanco->setBcoCodigo($nBcoCodigo);
		$oSysBanco->setBcoNome($sBcoNome);
		$oSysBanco->setBcoSigla($sBcoSigla);
		$oSysBanco->setBcoInc($sBcoInc);
		$oSysBanco->setBcoAlt($sBcoAlt);
		$oSysBanco->setAtivo($nAtivo);
 		return $oSysBanco;
 	}

 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto SysBanco
 	 *
 	 * @return boolean
 	 */
 	public function inserirSysBanco($oSysBanco){
 		$oPersistencia = new Persistencia($oSysBanco);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto SysBanco
 	 *
 	 * @return boolean
 	 */
 	public function alterarSysBanco($oSysBanco){
 		$oPersistencia = new Persistencia($oSysBanco);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	
 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto SysBanco
 	 *
 	 * @return boolean
 	 */
 	public function excluirSysBanco($nBcoCodigo){
 		$oSysBanco = new SysBanco();
  		
		$oSysBanco->setBcoCodigo($nBcoCodigo);
  		$oPersistencia = new Persistencia($oSysBanco);
    		$bExcluir = $oPersistencia->excluir();
 
 		if($bExcluir)
  				return true;
  		return false;
 		
 	}	
 	
 	/**
 	 *
 	 * Método para verificar se existe um Objeto SysBanco na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSysBanco($nBcoCodigo){
 		$oSysBanco = new SysBanco();
  		
		$oSysBanco->setBcoCodigo($nBcoCodigo);
  		$oPersistencia = new Persistencia($oSysBanco);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um objeto SysBanco da base de dados
 	 *
 	 * @return SysBanco
 	 */
 	public function recuperarUmSysBanco($nBcoCodigo){
 		$oSysBanco = new SysBanco();
  		$oPersistencia = new Persistencia($oSysBanco);
  		$sTabelas = "sys_banco";
  		$sCampos = "*";
  		$sComplemento = " WHERE bco_codigo = $nBcoCodigo";
  		$voSysBanco = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysBanco)
  			return $voSysBanco[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysBanco da base de dados
 	 *
 	 * @return SysBanco[]
 	 */
 	public function recuperarTodosSysBanco(){
 		$oSysBanco = new SysBanco();
  		$oPersistencia = new Persistencia($oSysBanco);
  		$sTabelas = "sys_banco";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1";
  		$voSysBanco = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysBanco)
  			return $voSysBanco;
  		return false;
 	}	
 	
 
	
 	
 
	/**
 	 *
 	 * Método para inicializar um Objeto SysEmpresa
 	 *
 	 * @return SysEmpresa  
 	 */
 	//public function inicializarSysEmpresa($nEmpCodigo,$sEmpRazaoSocial,$sEmpFantasia,$sEmpCnpj,$sEmpImagem,$sEmpEndLogra,$sEmpEndBairro,$sEmpEndCidade,$sEmpEndCep,$sEmpEndUf,$sEmpEndFone,$sEmpSite,$sEmpFtp,$sEmpEmail,$sEmpInc,$sEmpAlt,$nEmpAceite,$nAtivo,$nEmpCodPessoa,$sApelido,$sArquivo){
 	public function inicializarSysEmpresa($nEmpCodigo,$sEmpRazaoSocial,$sEmpFantasia,$sEmpCnpj,$sEmpImagem,$sEmpEndLogra,$sEmpEndBairro,$sEmpEndCidade,$sEmpEndCep,$sEmpEndUf,$sEmpEndFone,$sEmpSite,$sEmpFtp,$sEmpEmail,$sEmpInc,$sEmpAlt,$nEmpAceite,$nAtivo,$nEmpCodPessoa,$sApelido,$nUniCodigo){
 		$oSysEmpresa = new SysEmpresa();
 		
		$oSysEmpresa->setEmpCodigo($nEmpCodigo);
		$oSysEmpresa->setEmpRazaoSocial($sEmpRazaoSocial);
		$oSysEmpresa->setEmpFantasia($sEmpFantasia);
		$oSysEmpresa->setEmpCnpj($sEmpCnpj);
		$oSysEmpresa->setEmpImagem($sEmpImagem);
		$oSysEmpresa->setEmpEndLogra($sEmpEndLogra);
		$oSysEmpresa->setEmpEndBairro($sEmpEndBairro);
		$oSysEmpresa->setEmpEndCidade($sEmpEndCidade);
		$oSysEmpresa->setEmpEndCep($sEmpEndCep);
		$oSysEmpresa->setEmpEndUf($sEmpEndUf);
		$oSysEmpresa->setEmpEndFone($sEmpEndFone);
		$oSysEmpresa->setEmpSite($sEmpSite);
		$oSysEmpresa->setEmpFtp($sEmpFtp);
		$oSysEmpresa->setEmpEmail($sEmpEmail);
		$oSysEmpresa->setEmpInc($sEmpInc);
		$oSysEmpresa->setEmpAlt($sEmpAlt);
		$oSysEmpresa->setEmpAceite($nEmpAceite);
		$oSysEmpresa->setAtivo($nAtivo);
		$oSysEmpresa->setEmpCodPessoa($nEmpCodPessoa);
		$oSysEmpresa->setApelido($sApelido);
		$oSysEmpresa->setUniCodigo($nUniCodigo);
//        $oSysEmpresa->setArquivo($sArquivo);
 		return $oSysEmpresa;
 	}
 	
 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto SysEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function inserirSysEmpresa($oSysEmpresa){
 		$oPersistencia = new Persistencia($oSysEmpresa);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto SysEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function alterarSysEmpresa($oSysEmpresa){
 		$oPersistencia = new Persistencia($oSysEmpresa);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	
 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto SysEmpresa
 	 *
 	 * @return boolean
 	 */
 	public function excluirSysEmpresa($nEmpCodigo){
 		$oSysEmpresa = new SysEmpresa();
		$oSysEmpresa->setEmpCodigo($nEmpCodigo);
  		$oPersistencia = new Persistencia($oSysEmpresa);
    		$bExcluir =  $oPersistencia->excluir();
 
 		if($bExcluir)
  				return true;
  		return false;
 		
 	}	
 	
 	/**
 	 *
 	 * Método para verificar se existe um Objeto SysEmpresa na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSysEmpresa($nEmpCodigo){
 		$oSysEmpresa = new SysEmpresa();
  		
		$oSysEmpresa->setEmpCodigo($nEmpCodigo);
  		$oPersistencia = new Persistencia($oSysEmpresa);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um objeto SysEmpresa da base de dados
 	 *
 	 * @return SysEmpresa
 	 */
 	public function recuperarUmSysEmpresa($nEmpCodigo){
 		$oSysEmpresa = new SysEmpresa();
  		$oPersistencia = new Persistencia($oSysEmpresa);
  		$sTabelas = "sys_empresa";
  		$sCampos = "*";
  		$sComplemento = " WHERE emp_codigo = $nEmpCodigo";
  		$voSysEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysEmpresa)
  			return $voSysEmpresa[0];
  		return false;
 	}

 	/**
 	 *
 	 * Método para recuperar um objeto SysEmpresa da base de dados
 	 *
 	 * @return SysEmpresa
 	 */
 	public function recuperarUmSysEmpresaPorPessoa($nCodPessoa){
 		$oSysEmpresa = new SysEmpresa();
  		$oPersistencia = new Persistencia($oSysEmpresa);
  		$sTabelas = "sys_empresa";
  		$sCampos = "*";
  		$sComplemento = " WHERE emp_cod_pessoa = $nCodPessoa";
  		$voSysEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysEmpresa)
  			return $voSysEmpresa[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysEmpresa da base de dados
 	 *
 	 * @return SysEmpresa[]
 	 */
 	public function recuperarTodosSysEmpresa(){
 		$oSysEmpresa = new SysEmpresa();
  		$oPersistencia = new Persistencia($oSysEmpresa);
  		$sTabelas = "sys_empresa";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo = 1 ORDER BY emp_fantasia ASC ";
  		$voSysEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysEmpresa)
  			return $voSysEmpresa;
  		return false;
 	}
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysEmpresa da base de dados
 	 *
 	 * @return SysEmpresa[]
 	 */
 	public function recuperarTodosSysEmpresaPorUnidade($nCodUnidade){
 		$oSysEmpresa = new SysEmpresa();
  		$oPersistencia = new Persistencia($oSysEmpresa);
  		$sTabelas = "sys_empresa";
  		$sCampos = "*";
  		$sComplemento = " INNER JOIN acesso_unidade_empresa on sys_empresa.emp_codigo = acesso_unidade_empresa.emp_codigo where cod_unidade=$nCodUnidade AND sys_empresa.ativo = 1 ORDER BY emp_fantasia ASC ";
  		//echo "Select $sCampos From $sTabelas $sComplemento";
        $voSysEmpresa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysEmpresa)
  			return $voSysEmpresa;
  		return false;
 	}
	/**
 	 *
 	 * Método para inicializar um Objeto SysStatus
 	 *
 	 * @return SysStatus  
 	 */
 	public function inicializarSysStatus($nCodStatus,$sDescStatus,$nAtivo){
 		$oSysStatus = new SysStatus();
 		
		$oSysStatus->setCodStatus($nCodStatus);
		$oSysStatus->setDescStatus($sDescStatus);
		$oSysStatus->setAtivo($nAtivo);
 		return $oSysStatus;
 	}
 	
 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto SysStatus
 	 *
 	 * @return boolean
 	 */
 	public function inserirSysStatus($oSysStatus){
 		$oPersistencia = new Persistencia($oSysStatus);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto SysStatus
 	 *
 	 * @return boolean
 	 */
 	public function alterarSysStatus($oSysStatus){
 		$oPersistencia = new Persistencia($oSysStatus);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	
 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto SysStatus
 	 *
 	 * @return boolean
 	 */
 	public function excluirSysStatus($nCodStatus){
 		$oSysStatus = new SysStatus();
  		
		$oSysStatus->setCodStatus($nCodStatus);
  		$oPersistencia = new Persistencia($oSysStatus);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();
 
 		if($bExcluir)
  				return true;
  		return false;
 		
 	}	
 	
 	/**
 	 *
 	 * Método para verificar se existe um Objeto SysStatus na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSysStatus($nCodStatus){
 		$oSysStatus = new SysStatus();
  		
		$oSysStatus->setCodStatus($nCodStatus);
  		$oPersistencia = new Persistencia($oSysStatus);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um objeto SysStatus da base de dados
 	 *
 	 * @return SysStatus
 	 */
 	public function recuperarUmSysStatus($nCodStatus){
 		$oSysStatus = new SysStatus();
  		$oPersistencia = new Persistencia($oSysStatus);
  		$sTabelas = "sys_status";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_status = $nCodStatus";
  		$voSysStatus = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysStatus)
  			return $voSysStatus[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysStatus da base de dados
 	 *
 	 * @return SysStatus[]
 	 */
 	public function recuperarTodosSysStatus(){
 		$oSysStatus = new SysStatus();
  		$oPersistencia = new Persistencia($oSysStatus);
  		$sTabelas = "sys_status";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1";
  		$voSysStatus = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);		
  		if($voSysStatus)
  			return $voSysStatus;
  		return false;
 	}
 	
 	

/**
 	 *
 	 * Método para inicializar um Objeto SysProtocoloEncaminhamento
 	 *
 	 * @return SysProtocoloEncaminhamento  
 	 */
 	public function inicializarSysProtocoloEncaminhamento($nCodProtocoloEncaminhamento,$nDe,$nPara,$nInterno,$nMovTipo,$nAtivo,$nOrdem){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
 		
		$oSysProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
		$oSysProtocoloEncaminhamento->setDe($nDe);
		$oSysProtocoloEncaminhamento->setPara($nPara);
		$oSysProtocoloEncaminhamento->setInterno($nInterno);
		$oSysProtocoloEncaminhamento->setMovTipo($nMovTipo);
		$oSysProtocoloEncaminhamento->setAtivo($nAtivo);
		$oSysProtocoloEncaminhamento->setOrdem($nOrdem);
 		return $oSysProtocoloEncaminhamento;
 	}
 	
 	/**
 	 *
 	 * Método para inserir na base de dados um Objeto SysProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function inserirSysProtocoloEncaminhamento($oSysProtocoloEncaminhamento){
 		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para alterar na base de dados um Objeto SysProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function alterarSysProtocoloEncaminhamento($oSysProtocoloEncaminhamento){
 		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	
 	/**
 	 *
 	 * Método para excluir da base de dados um Objeto SysProtocoloEncaminhamento
 	 *
 	 * @return boolean
 	 */
 	public function excluirSysProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		
		$oSysProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
    		$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();
 
 		if($bExcluir)
  				return true;
  		return false;
 		
 	}	
 	
 	/**
 	 *
 	 * Método para verificar se existe um Objeto SysProtocoloEncaminhamento na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteSysProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		
		$oSysProtocoloEncaminhamento->setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento);
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um objeto SysProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SysProtocoloEncaminhamento
 	 */
 	public function recuperarUmSysProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		$sTabelas = "sys_protocolo_encaminhamento";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_protocolo_encaminhamento = $nCodProtocoloEncaminhamento";
  		$voSysProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysProtocoloEncaminhamento)
  			return $voSysProtocoloEncaminhamento[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SysProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSysProtocoloEncaminhamento(){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		$sTabelas = "sys_protocolo_encaminhamento";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voSysProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysProtocoloEncaminhamento)
  			return $voSysProtocoloEncaminhamento;
  		return false;
 	} 	

 	/**
 	 *
 	 * Método para recuperar um vetor de objetos SysProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SysProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSysProtocoloEncaminhamentoPorOrigem($nDe,$nMovTipo){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		$sTabelas = "sys_protocolo_encaminhamento";
  		$sCampos = "cod_protocolo_encaminhamento,de,para,acesso_grupo_usuario.descricao as ativo";
  		$sComplemento = "INNER JOIN acesso_grupo_usuario ON acesso_grupo_usuario.cod_grupo_usuario = sys_protocolo_encaminhamento.para
						 WHERE de<>para AND de =" .$nDe . " And interno=0 And mov_tipo = " . $nMovTipo ;
		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
  		$voSysProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysProtocoloEncaminhamento)
  			return $voSysProtocoloEncaminhamento;
  		return false;
 	} 	
	
 /**
 	 *
 	 * Método para recuperar um vetor de objetos SysProtocoloEncaminhamento da base de dados
 	 *
 	 * @return SysProtocoloEncaminhamento[]
 	 */
 	public function recuperarTodosSysProtocoloEncaminhamentoPorTipo($nMovTipo){
 		$oSysProtocoloEncaminhamento = new SysProtocoloEncaminhamento();
  		$oPersistencia = new Persistencia($oSysProtocoloEncaminhamento);
  		$sTabelas = "sys_protocolo_encaminhamento";
  		$sCampos = "DISTINCT de,descricao as ativo,ordem";
  		$sComplemento = " INNER JOIN acesso_grupo_usuario ON sys_protocolo_encaminhamento.de = cod_grupo_usuario and  cod_grupo_usuario in (select DISTINCT de from sys_protocolo_encaminhamento where mov_tipo=".$nMovTipo.")
                          where not ISNULL(ordem)	 and mov_tipo=".$nMovTipo." ORDER BY 3	"  ;
		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
  		$voSysProtocoloEncaminhamento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voSysProtocoloEncaminhamento)
  			return $voSysProtocoloEncaminhamento;
  		return false;
 	}
}
?>
