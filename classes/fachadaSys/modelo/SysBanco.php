<?php
 /**
  * @author Auto-Generated 
  * @package fachadaSys 
  * @SGBD mysql 
  * @tabela sys_banco 
  */
 class SysBanco{
 	/**
	* @campo bco_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nBcoCodigo;
	/**
	* @campo bco_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBcoNome;
	/**
	* @campo bco_sigla
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sBcoSigla;
	/**
	* @campo bco_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sBcoInc;
	/**
	* @campo bco_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sBcoAlt;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setBcoCodigo($nBcoCodigo){
		$this->nBcoCodigo = $nBcoCodigo;
	}
	public function getBcoCodigo(){
		return $this->nBcoCodigo;
	}
	
	public function getBcoCodigoFormatado(){
		return str_pad($this->nBcoCodigo, 3, "0", STR_PAD_LEFT); 
	}
	
	public function setBcoNome($sBcoNome){
		$this->sBcoNome = $sBcoNome;
	}
	public function getBcoNome(){
		return $this->sBcoNome;
	}
	public function setBcoSigla($sBcoSigla){
		$this->sBcoSigla = $sBcoSigla;
	}
	public function getBcoSigla(){
		return $this->sBcoSigla;
	}
	public function setBcoInc($sBcoInc){
		$this->sBcoInc = $sBcoInc;
	}
	public function getBcoInc(){
		return $this->sBcoInc;
	}
	public function setBcoAlt($sBcoAlt){
		$this->sBcoAlt = $sBcoAlt;
	}
	public function getBcoAlt(){
		return $this->sBcoAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
