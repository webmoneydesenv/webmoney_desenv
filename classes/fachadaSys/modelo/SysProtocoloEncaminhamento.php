<?php
 /**
  * @author Auto-Generated 
  * @package fachadaSys 
  * @SGBD mysql 
  * @tabela sys_protocolo_encaminhamento 
  */
 class SysProtocoloEncaminhamento{
 	/**
	* @campo cod_protocolo_encaminhamento
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProtocoloEncaminhamento;
	/**
	* @campo de
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDe;
	/**
	* @campo para
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPara;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo interno
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nInterno;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
     /**
	* @campo ordem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nOrdem;
	private $oAcessoGrupoUsuario;
//	private $oSysTipoProtocolo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodProtocoloEncaminhamento($nCodProtocoloEncaminhamento){
		$this->nCodProtocoloEncaminhamento = $nCodProtocoloEncaminhamento;
	}
	public function getCodProtocoloEncaminhamento(){
		return $this->nCodProtocoloEncaminhamento;
	}
	public function setDe($nDe){
		$this->nDe = $nDe;
	}
	public function getDe(){
		return $this->nDe;
	}
	public function setPara($nPara){
		$this->nPara = $nPara;
	}
	public function getPara(){
		return $this->nPara;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setInterno($nInterno){
		$this->nInterno = $nInterno;
	}
	public function getInterno(){
		return $this->nInterno;
	}
     public function getMovTipo(){
		return $this->nMovTipo;
	}
     public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}

    public function getOrdem(){
		return $this->nOrdem;
	}
     public function setOrdem($nOrdem){
		$this->nOrdem = $nOrdem;
	}

	public function setAcessoGrupoUsuario($oAcessoGrupoUsuario){
		$this->oAcessoGrupoUsuario = $oAcessoGrupoUsuario;
	}
	public function getAcessoGrupoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oAcessoGrupoUsuario = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDe());
		return $this->oAcessoGrupoUsuario;
	}
	/*public function setSysTipoProtocolo($oSysTipoProtocolo){
		$this->oSysTipoProtocolo = $oSysTipoProtocolo;
	}
	public function getSysTipoProtocolo(){
		$oFachada = new FachadaSysBD();
		$this->oSysTipoProtocolo = $oFachada->recuperarUmSysTipoProtocolo($this->getPara());
		return $this->oSysTipoProtocolo;
	}*/
	
 }
 ?>
