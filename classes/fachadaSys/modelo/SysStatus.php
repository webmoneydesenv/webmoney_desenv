<?php
 /**
  * @author Auto-Generated 
  * @package fachadaSys 
  * @SGBD mysql 
  * @tabela sys_status 
  */
 class SysStatus{
 	/**
	* @campo cod_status
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodStatus;
	/**
	* @campo desc_status
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescStatus;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setDescStatus($sDescStatus){
		$this->sDescStatus = $sDescStatus;
	}
	public function getDescStatus(){
		return $this->sDescStatus;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
