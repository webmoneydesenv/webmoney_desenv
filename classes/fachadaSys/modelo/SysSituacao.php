<?php
 /**
  * @author Auto-Generated 
  * @package fachadaSys 
  * @SGBD mysql 
  * @tabela sys_situacao 
  */
 class SysSituacao{
 	/**
	* @campo sit_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nSitCodigo;
	/**
	* @campo sit_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sSitNome;
	/**
	* @campo sit_inc
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSitInc;
	/**
	* @campo sit_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSitAlt;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setSitCodigo($nSitCodigo){
		$this->nSitCodigo = $nSitCodigo;
	}
	public function getSitCodigo(){
		return $this->nSitCodigo;
	}
	public function setSitNome($sSitNome){
		$this->sSitNome = $sSitNome;
	}
	public function getSitNome(){
		return $this->sSitNome;
	}
	public function setSitInc($sSitInc){
		$this->sSitInc = $sSitInc;
	}
	public function getSitInc(){
		return $this->sSitInc;
	}
	public function setSitAlt($sSitAlt){
		$this->sSitAlt = $sSitAlt;
	}
	public function getSitAlt(){
		return $this->sSitAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
