<?php
 /**
  * @author Auto-Generated 
  * @package fachadaSys 
  * @SGBD mysql 
  * @tabela sys_empresa 
  */
 class SysEmpresa{
 	/**
	* @campo emp_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nEmpCodigo;
	/**
	* @campo emp_razao_social
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpRazaoSocial;
	/**
	* @campo emp_fantasia
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpFantasia;
	/**
	* @campo emp_cnpj
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpCnpj;
	/**
	* @campo emp_imagem
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpImagem;
	/**
	* @campo emp_end_logra
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndLogra;
	/**
	* @campo emp_end_bairro
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndBairro;
	/**
	* @campo emp_end_cidade
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndCidade;
	/**
	* @campo emp_end_cep
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndCep;
	/**
	* @campo emp_end_uf
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndUf;
	/**
	* @campo emp_end_fone
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEndFone;
	/**
	* @campo emp_site
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpSite;
	/**
	* @campo emp_ftp
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpFtp;
	/**
	* @campo emp_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpEmail;
	/**
	* @campo emp_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEmpInc;
	/**
	* @campo emp_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sEmpAlt;

	/**
	* @campo emp_aceite
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpAceite;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo emp_cod_pessoa
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodPessoa;
	
	/**
	* @campo apelido
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sApelido;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
    private $oUnidade;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setEmpRazaoSocial($sEmpRazaoSocial){
		$this->sEmpRazaoSocial = $sEmpRazaoSocial;
	}
	public function getEmpRazaoSocial(){
		return $this->sEmpRazaoSocial;
	}
	public function setEmpFantasia($sEmpFantasia){
		$this->sEmpFantasia = $sEmpFantasia;
	}
	public function getEmpFantasia(){
		return $this->sEmpFantasia;
	}
	public function setEmpCnpj($sEmpCnpj){
		$this->sEmpCnpj = $sEmpCnpj;
	}
	public function getEmpCnpj(){
		return $this->sEmpCnpj;
	}
	public function setEmpImagem($sEmpImagem){
		$this->sEmpImagem = $sEmpImagem;
	}
	public function getEmpImagem(){
		return $this->sEmpImagem;
	}
	public function setEmpEndLogra($sEmpEndLogra){
		$this->sEmpEndLogra = $sEmpEndLogra;
	}
	public function getEmpEndLogra(){
		return $this->sEmpEndLogra;
	}
	public function setEmpEndBairro($sEmpEndBairro){
		$this->sEmpEndBairro = $sEmpEndBairro;
	}
	public function getEmpEndBairro(){
		return $this->sEmpEndBairro;
	}
	public function setEmpEndCidade($sEmpEndCidade){
		$this->sEmpEndCidade = $sEmpEndCidade;
	}
	public function getEmpEndCidade(){
		return $this->sEmpEndCidade;
	}
	public function setEmpEndCep($sEmpEndCep){
		$this->sEmpEndCep = $sEmpEndCep;
	}
	public function getEmpEndCep(){
		return $this->sEmpEndCep;
	}
	public function setEmpEndUf($sEmpEndUf){
		$this->sEmpEndUf = $sEmpEndUf;
	}
	public function getEmpEndUf(){
		return $this->sEmpEndUf;
	}
	public function setEmpEndFone($sEmpEndFone){
		$this->sEmpEndFone = $sEmpEndFone;
	}
	public function getEmpEndFone(){
		return $this->sEmpEndFone;
	}
	public function setEmpSite($sEmpSite){
		$this->sEmpSite = $sEmpSite;
	}
	public function getEmpSite(){
		return $this->sEmpSite;
	}
	public function setEmpFtp($sEmpFtp){
		$this->sEmpFtp = $sEmpFtp;
	}
	public function getEmpFtp(){
		return $this->sEmpFtp;
	}
	public function setEmpEmail($sEmpEmail){
		$this->sEmpEmail = $sEmpEmail;
	}
	public function getEmpEmail(){
		return $this->sEmpEmail;
	}
	public function setEmpInc($sEmpInc){
		$this->sEmpInc = $sEmpInc;
	}
	public function getEmpInc(){
		return $this->sEmpInc;
	}
	public function setEmpAlt($sEmpAlt){
		$this->sEmpAlt = $sEmpAlt;
	}
	public function getEmpAlt(){
		return $this->sEmpAlt;
	}	
	public function setEmpAceite($nEmpAceite){
		$this->nEmpAceite = $nEmpAceite;
	}
	public function getEmpAceite(){
		return $this->nEmpAceite;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setEmpCodPessoa($nEmpCodPessoa){
		$this->nEmpCodPessoa = $nEmpCodPessoa;
	}
	public function getEmpCodPessoa(){
		return $this->nEmpCodPessoa;
	}
	public function setApelido($sApelido){
		$this->sApelido = $sApelido;
	}
	public function getApelido(){
		return $this->sApelido;
	}
    public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}

     public function getUnidade(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oUnidade = $oFachada->recuperarUmMnyPlanoContas($this->getUniCodigo());
		return $this->oUnidade;
	}
	public function setUnidade($oUnidade){
		$this->oUnidade = $oUnidade;
	}
     /*
    public function setArquivo($sArquivo){
		$this->sArquivo = $sArquivo;
	}
	public function getArquivo(){
		return $this->sArquivo;
	}
    */
 }
 ?>
