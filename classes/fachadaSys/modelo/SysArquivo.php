<?php
 /**
  * @author Auto-Generated
  * @package fachadaSys
  * @SGBD mysql
  * @tabela sys_arquivo
  */
 class SysArquivo{
 	/**
	* @campo cod_arquivo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodArquivo;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNome;
	/**
	* @campo arquivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sArquivo;
	/**
	* @campo extensao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sExtensao;
	/**
	* @campo cod_tipo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodTipo;
	/**
	* @campo tamanho
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sTamanho;
	/**
	* @campo data_hora
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataHora;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo contrato_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nContratoCodigo;
	/**
	* @campo cod_conciliacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodConciliacao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovItem;


    private $oMnyContratoDocTipo;
	private $oMnyContratoPessoa;
	private $oMnyConciliacao;


 	public function __construct(){

 	}

 	public function setCodArquivo($nCodArquivo){
		$this->nCodArquivo = $nCodArquivo;
	}
	public function getCodArquivo(){
		return $this->nCodArquivo;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setArquivo($sArquivo){
		$this->sArquivo = $sArquivo;
	}
	public function getArquivo(){
		return $this->sArquivo;
	}
	public function setExtensao($sExtensao){
		$this->sExtensao = $sExtensao;
	}
	public function getExtensao(){
		return $this->sExtensao;
	}
	public function setCodTipo($nCodTipo){
		$this->nCodTipo = $nCodTipo;
	}
	public function getCodTipo(){
		return $this->nCodTipo;
	}
	public function setTamanho($sTamanho){
		$this->sTamanho = $sTamanho;
	}
	public function getTamanho(){
		return $this->sTamanho;
	}
	public function setDataHora($dDataHora){
		$this->dDataHora = $dDataHora;
	}
	public function getDataHora(){
		return $this->dDataHora;
	}
	public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setContratoCodigo($nContratoCodigo){
		$this->nContratoCodigo = $nContratoCodigo;
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
	public function setCodConciliacao($nCodConciliacao){
		$this->nCodConciliacao = $nCodConciliacao;
	}
	public function getCodConciliacao(){
		return $this->nCodConciliacao;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}

 	public function setMnyCodArquivo($nMnyCodArquivo){
		$this->nMnyCodArquivo = $nMnyCodArquivo;
	}
	public function getMnyCodArquivo(){
		return $this->nMnyCodArquivo;
	}
	public function setMnyContratoDocTipo($oMnyContratoDocTipo){
		$this->oMnyContratoDocTipo = $oMnyContratoDocTipo;
	}
	public function getMnyContratoDocTipo(){
		$oFachada = new FachadaSysBD();
		$this->oMnyContratoDocTipo = $oFachada->recuperarUmMnyContratoDocTipo($this->getCodTipo());
		return $this->oMnyContratoDocTipo;
	}
	public function setMnyContratoPessoa($oMnyContratoPessoa){
		$this->oMnyContratoPessoa = $oMnyContratoPessoa;
	}
	public function getMnyContratoPessoa(){
		$oFachada = new FachadaSysBD();
		$this->oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($this->getContratoCodigo());
		return $this->oMnyContratoPessoa;
	}
	public function setMnyConciliacao($oMnyConciliacao){
		$this->oMnyConciliacao = $oMnyConciliacao;
	}
	public function getMnyConciliacao(){
		$oFachada = new FachadaSysBD();
		$this->oMnyConciliacao = $oFachada->recuperarUmMnyConciliacao($this->getCodConciliacao());
		return $this->oMnyConciliacao;
	}

 }
 ?>
