<?php
 class SysProtocoloEncaminhamentoCTR implements IControle{
 
 	public function SysProtocoloEncaminhamentoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();
 
 		$voSysProtocoloEncaminhamento = $oFachada->recuperarTodosSysProtocoloEncaminhamento();
 
 		$_REQUEST['voSysProtocoloEncaminhamento'] = $voSysProtocoloEncaminhamento;
 		$_REQUEST['voSysTipoProtocolo'] = $oFachada->recuperarTodosSysTipoProtocolo();

		$_REQUEST['voSysTipoProtocolo'] = $oFachada->recuperarTodosSysTipoProtocolo();

		
 		
 		include_once("view/sys/sys_protocolo_encaminhamento/index.php");
 
 		exit();
 	
 	}
 	public function preparaLista2(){
 		$oFachada = new FachadaSysBD();
		//$nOrigem = $_REQUEST[''];
 		$voSysProtocoloEncaminhamento = $oFachada->recuperarTodosSysProtocoloEncaminhamentoPorOrigem($_REQUEST['nOrigem']);
 		$_REQUEST['voSysProtocoloEncaminhamento'] = $voSysProtocoloEncaminhamento;
 		include_once("view/sys/sys_protocolo_encaminhamento/lista_ajax.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$oSysProtocoloEncaminhamento = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysProtocoloEncaminhamento = ($_POST['fIdSysProtocoloEncaminhamento'][0]) ? $_POST['fIdSysProtocoloEncaminhamento'][0] : $_GET['nIdSysProtocoloEncaminhamento'];
 	
 			if($nIdSysProtocoloEncaminhamento){
 				$vIdSysProtocoloEncaminhamento = explode("||",$nIdSysProtocoloEncaminhamento);
 				$oSysProtocoloEncaminhamento = $oFachada->recuperarUmSysProtocoloEncaminhamento($vIdSysProtocoloEncaminhamento[0]);
 			}
 		}
 		
 		$_REQUEST['oSysProtocoloEncaminhamento'] = ($_SESSION['oSysProtocoloEncaminhamento']) ? $_SESSION['oSysProtocoloEncaminhamento'] : $oSysProtocoloEncaminhamento;
 		unset($_SESSION['oSysProtocoloEncaminhamento']);
 
 		$_REQUEST['voSysTipoProtocolo'] = $oFachada->recuperarTodosSysTipoProtocolo();

		$_REQUEST['voSysTipoProtocolo'] = $oFachada->recuperarTodosSysTipoProtocolo();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/sys/sys_protocolo_encaminhamento/detalhe.php");
 		else
 			include_once("view/sys/sys_protocolo_encaminhamento/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oSysProtocoloEncaminhamento = $oFachada->inicializarSysProtocoloEncaminhamento($_POST['fCodProtocoloEncaminhamento'],$_POST['fDe'],$_POST['fPara'],$_POST['fInterno'],$_POST['fMovTipo'],$_POST['fAtivo'],$_POST['fOrdem']);
 			$_SESSION['oSysProtocoloEncaminhamento'] = $oSysProtocoloEncaminhamento;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("De", $oSysProtocoloEncaminhamento->getDe(), "number", "y");
			$oValidate->add_number_field("Para", $oSysProtocoloEncaminhamento->getPara(), "number", "y");
			$oValidate->add_number_field("Ativo", $oSysProtocoloEncaminhamento->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP."&nIdSysProtocoloEncaminhamento=".$_POST['fCodProtocoloEncaminhamento']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSysProtocoloEncaminhamento($oSysProtocoloEncaminhamento)){
 					unset($_SESSION['oSysProtocoloEncaminhamento']);
 					$_SESSION['sMsg'] = "Encaminhamentos inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SysProtocoloEncaminhamento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Encaminhamentos!";
 					$sHeader = "?bErro=1&action=SysProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSysProtocoloEncaminhamento($oSysProtocoloEncaminhamento)){
 					unset($_SESSION['oSysProtocoloEncaminhamento']);
 					$_SESSION['sMsg'] = "Encaminhamentos alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SysProtocoloEncaminhamento.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Encaminhamentos!";
 					$sHeader = "?bErro=1&action=SysProtocoloEncaminhamento.preparaFormulario&sOP=".$sOP."&nIdSysProtocoloEncaminhamento=".$_POST['fCodProtocoloEncaminhamento']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSysProtocoloEncaminhamento']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSysProtocoloEncaminhamento($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Encaminhamentos(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SysProtocoloEncaminhamento.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Encaminhamentos!";
 					$sHeader = "?bErro=1&action=SysProtocoloEncaminhamento.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
