<?php
 class SysBancoCTR implements IControle{
 
 	public function SysBancoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();
 
 		$voSysBanco = $oFachada->recuperarTodosSysBanco();
 
 		$_REQUEST['voSysBanco'] = $voSysBanco;
 		
 		
 		include_once("view/sys/sys_banco/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$oSysBanco = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysBanco = ($_POST['fIdSysBanco'][0]) ? $_POST['fIdSysBanco'][0] : $_GET['nIdSysBanco'];
 	
 			if($nIdSysBanco){
 				$vIdSysBanco = explode("||",$nIdSysBanco);
 				$oSysBanco = $oFachada->recuperarUmSysBanco($vIdSysBanco[0]);
 			}
 		}
 		
 		$_REQUEST['oSysBanco'] = ($_SESSION['oSysBanco']) ? $_SESSION['oSysBanco'] : $oSysBanco;
 		unset($_SESSION['oSysBanco']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/sys/sys_banco/detalhe.php");
 		else
 			include_once("view/sys/sys_banco/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
			$_POST['fBcoInc'] = date('d/m/Y H:i:s'). " ". $_SESSION['oUsuarioImoney']->getLogin();
 			$oSysBanco = $oFachada->inicializarSysBanco($_POST['fBcoCodigo'],mb_convert_case($_POST['fBcoNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fBcoSigla'],MB_CASE_UPPER, "UTF-8"),$_POST['fBcoInc'],$_POST['fBcoAlt'],$_POST['fAtivo']);
 																																		
			$_SESSION['oSysBanco'] = $oSysBanco;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("BcoCodigo", $oSysBanco->getBcoCodigo(), "number", "y");
			$oValidate->add_text_field("BcoNome", $oSysBanco->getBcoNome(), "text", "y");
			//$oValidate->add_text_field("BcoSigla", $oSysBanco->getBcoSigla(), "text", "y");
			$oValidate->add_text_field("BcoInc", $oSysBanco->getBcoInc(), "text", "y");
			//$oValidate->add_text_field("BcoAlt", $oSysBanco->getBcoAlt(), "text", "y");
			$oValidate->add_number_field("Ativo", $oSysBanco->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysBanco.preparaFormulario&sOP=".$sOP."&nIdSysBanco=".$_POST['fBcoCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSysBanco($oSysBanco)){
 					unset($_SESSION['oSysBanco']);
 					$_SESSION['sMsg'] = "Banco inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SysBanco.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Banco!";
 					$sHeader = "?bErro=1&action=SysBanco.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSysBanco($oSysBanco)){
 					unset($_SESSION['oSysBanco']);
 					$_SESSION['sMsg'] = "Banco alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SysBanco.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Banco!";
 					$sHeader = "?bErro=1&action=SysBanco.preparaFormulario&sOP=".$sOP."&nIdSysBanco=".$_POST['fBcoCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSysBanco']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSysBanco($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Banco(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SysBanco.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Banco!";
 					$sHeader = "?bErro=1&action=SysBanco.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
