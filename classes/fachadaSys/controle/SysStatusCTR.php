<?php
 class SysStatusCTR implements IControle{
 
 	public function SysStatusCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();
 
 		$voSysStatus = $oFachada->recuperarTodosSysStatus();
 
 		$_REQUEST['voSysStatus'] = $voSysStatus;
 		
 		
 		include_once("view/sys/sys_status/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$oSysStatus = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysStatus = ($_POST['fIdSysStatus'][0]) ? $_POST['fIdSysStatus'][0] : $_GET['nIdSysStatus'];
 	
 			if($nIdSysStatus){
 				$vIdSysStatus = explode("||",$nIdSysStatus);
 				$oSysStatus = $oFachada->recuperarUmSysStatus($vIdSysStatus[0]);
 			}
 		}
 		
 		$_REQUEST['oSysStatus'] = ($_SESSION['oSysStatus']) ? $_SESSION['oSysStatus'] : $oSysStatus;
 		unset($_SESSION['oSysStatus']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/sys/sys_status/detalhe.php");
 		else
 			include_once("view/sys/sys_status/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oSysStatus = $oFachada->inicializarSysStatus($_POST['fCodStatus'],mb_convert_case($_POST['fDescStatus'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
 																
			$_SESSION['oSysStatus'] = $oSysStatus;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("DescStatus", $oSysStatus->getDescStatus(), "text", "y");
			$oValidate->add_number_field("Ativo", $oSysStatus->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysStatus.preparaFormulario&sOP=".$sOP."&nIdSysStatus=".$_POST['fCodStatus']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSysStatus($oSysStatus)){
 					unset($_SESSION['oSysStatus']);
 					$_SESSION['sMsg'] = "Status inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SysStatus.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Status!";
 					$sHeader = "?bErro=1&action=SysStatus.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSysStatus($oSysStatus)){
 					unset($_SESSION['oSysStatus']);
 					$_SESSION['sMsg'] = "Status alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SysStatus.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Status!";
 					$sHeader = "?bErro=1&action=SysStatus.preparaFormulario&sOP=".$sOP."&nIdSysStatus=".$_POST['fCodStatus']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSysStatus']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSysStatus($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Status(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SysStatus.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Status!";
 					$sHeader = "?bErro=1&action=SysStatus.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
