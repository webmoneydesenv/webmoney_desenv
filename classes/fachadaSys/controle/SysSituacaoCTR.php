<?php
 class SysSituacaoCTR implements IControle{
 
 	public function SysSituacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();
 
 		$voSysSituacao = $oFachada->recuperarTodosSysSituacao();
 
 		$_REQUEST['voSysSituacao'] = $voSysSituacao;
 		
 		
 		include_once("view/sys/sys_situacao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$oSysSituacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysSituacao = ($_POST['fIdSysSituacao'][0]) ? $_POST['fIdSysSituacao'][0] : $_GET['nIdSysSituacao'];
 	
 			if($nIdSysSituacao){
 				$vIdSysSituacao = explode("||",$nIdSysSituacao);
 				$oSysSituacao = $oFachada->recuperarUmSysSituacao($vIdSysSituacao[0]);
 			}
 		}
 		
 		$_REQUEST['oSysSituacao'] = ($_SESSION['oSysSituacao']) ? $_SESSION['oSysSituacao'] : $oSysSituacao;
 		unset($_SESSION['oSysSituacao']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/sys/sys_situacao/detalhe.php");
 		else
 			include_once("view/sys/sys_situacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oSysSituacao = $oFachada->inicializarSysSituacao($_POST['fSitCodigo'],$_POST['fSitNome'],$_POST['fSitInc'],$_POST['fSitAlt'],$_POST['fAtivo']);
 			$_SESSION['oSysSituacao'] = $oSysSituacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("SitNome", $oSysSituacao->getSitNome(), "text", "y");
			//$oValidate->add_text_field("SitInc", $oSysSituacao->getSitInc(), "text", "y");
			//$oValidate->add_text_field("SitAlt", $oSysSituacao->getSitAlt(), "text", "y");
			$oValidate->add_number_field("Ativo", $oSysSituacao->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysSituacao.preparaFormulario&sOP=".$sOP."&nIdSysSituacao=".$_POST['fSitCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSysSituacao($oSysSituacao)){
 					unset($_SESSION['oSysSituacao']);
 					$_SESSION['sMsg'] = "Situação inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SysSituacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Situação!";
 					$sHeader = "?bErro=1&action=SysSituacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarSysSituacao($oSysSituacao)){
 					unset($_SESSION['oSysSituacao']);
 					$_SESSION['sMsg'] = "Situação alterado com sucesso!";
 					$sHeader = "?bErro=0&action=SysSituacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Situação!";
 					$sHeader = "?bErro=1&action=SysSituacao.preparaFormulario&sOP=".$sOP."&nIdSysSituacao=".$_POST['fSitCodigo']."";
 				}
 			break;
 			case "Excluir":
 				
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSysSituacao']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSysSituacao($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Situação(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SysSituacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Situação!";
 					$sHeader = "?bErro=1&action=SysSituacao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
