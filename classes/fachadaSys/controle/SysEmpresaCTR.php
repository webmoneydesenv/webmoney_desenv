<?php
 class SysEmpresaCTR implements IControle{
 
 	public function SysEmpresaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();
 
 		$voSysEmpresa = $oFachada->recuperarTodosSysEmpresa();
 		$_REQUEST['voSysEmpresa'] = $voSysEmpresa;
 		include_once("view/sys/sys_empresa/index.php");
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();
 
 		$oSysEmpresa = false;
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysEmpresa = ($_POST['fIdSysEmpresa'][0]) ? $_POST['fIdSysEmpresa'][0] : $_GET['nIdSysEmpresa'];
 			if($nIdSysEmpresa){
 				$vIdSysEmpresa = explode("||",$nIdSysEmpresa);
 				$oSysEmpresa = $oFachada->recuperarUmSysEmpresa($vIdSysEmpresa[0]);
 			}
 		}
 		
 		$_REQUEST['oSysEmpresa'] = ($_SESSION['oSysEmpresa']) ? $_SESSION['oSysEmpresa'] : $oSysEmpresa;
 		unset($_SESSION['oSysEmpresa']);
 
 		switch($_REQUEST['sOP']){
			case "Detalhar":
				include_once("view/sys/sys_empresa/detalhe.php");
			break;
			case "Cadastrar":
			case "Alterar":
                $_REQUEST['voUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorCentral();
				include_once("view/sys/sys_empresa/insere_altera.php");
			break;
			case "EscolheEmpresa":

                $oFachadaSys = new FachadaSysBD();
                $oFachadaView = new FachadaViewBD();
				$voEmpresa = $oFachadaSys->recuperarTodosSysEmpresa();

                $_REQUEST['voEmpresa'] = $voEmpresa;
                $voVAcessoUsuarioUnidade = $oFachadaView->recuperarTodosVAcessoUsuarioUnidadePorUsuario($_SESSION['oUsuarioImoney']->getCodUsuario());

                unset($_SESSION['Perfil']['Unidade']);
                unset($_SESSION['Perfil']['CodGrupoUsuario']);
                unset($_SESSION['Perfil']['GrupoUsuario']);

                include_once("view/sys/login/seleciona_empresa.php");

			break;
			
		
		}
 		
 		exit();
 	
 	}
 
    public function preparaImagem(){
        $oFachadaSys = new FachadaSysBD();
        $_REQUEST['oSysEmpresa'] = $oFachadaSys->recuperarUmSysEmpresa($_GET['nCodEmpresa']);
        include_once('view/sys/sys_empresa/imagem.php');
        exit();
    }

 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaPermissao = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
		
		if($sOP == "EscolheEmpresa"){

			$nCodEmpresa = ($_REQUEST['fIdSysEmpresa'][0]) ? $_REQUEST['fIdSysEmpresa'][0] : $_POST['fEmpCodigo'];
			$_SESSION['oEmpresa'] = $oFachada->recuperarUmSysEmpresa($nCodEmpresa);
            //$nCodGrupoUsuario = () ? : $_POST['fCodGrupoUsuario'];
            $oAcessoGrupoUsuario = $oFachadaPermissao->recuperarUmAcessoGrupoUsuario($_POST['fCodGrupoUsuario']);



            $sHeader = "?";
 			header("Location: ".$sHeader);	
 			die();
		
		}elseif($sOP != "Excluir"){
            /*
            //Upload Extrato Bancário...
            $Arquivo = $_FILES["fArquivo"]["tmp_name"];
            $Tipo = $_FILES["fArquivo"]["type"];
            move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.img");
            $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.img", "rb");
            $Imagem = fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.img"));

            $oSysEmpresa = $oFachada->inicializarSysEmpresa($_POST['fEmpCodigo'],mb_convert_case($_POST['fEmpRazaoSocial'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpFantasia'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpCnpj'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpImagem'],mb_convert_case($_POST['fEmpEndLogra'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndBairro'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCidade'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCep'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndUf'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndFone'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpSite'],$_POST['fEmpFtp'],$_POST['fEmpEmail'],mb_strtoupper($_POST['fEmpInc']),mb_strtoupper($_POST['fEmpAlt']),$_POST['fEmpAceite'],$_POST['fAtivo'],$_POST['fEmpCodPessoa'],mb_convert_case($_POST['fApelido'], MB_CASE_UPPER, "UTF-8"),$Imagem);*/
            $oSysEmpresa = $oFachada->inicializarSysEmpresa($_POST['fEmpCodigo'],mb_convert_case($_POST['fEmpRazaoSocial'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpFantasia'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpCnpj'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpImagem'],mb_convert_case($_POST['fEmpEndLogra'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndBairro'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCidade'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCep'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndUf'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndFone'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpSite'],$_POST['fEmpFtp'],$_POST['fEmpEmail'],mb_strtoupper($_POST['fEmpInc']),mb_strtoupper($_POST['fEmpAlt']),$_POST['fEmpAceite'],$_POST['fAtivo'],$_POST['fEmpCodPessoa'],mb_convert_case($_POST['fApelido'], MB_CASE_UPPER, "UTF-8"),$_POST['fUniCodigo']);

            $_SESSION['oSysEmpresa'] = $oSysEmpresa;
 			
			$_POST['fCodMatriz'] = 'null';
			$oMnyPessoaJuridica = $oFachadaFinanceiro->inicializarMnyPessoaJuridica($_POST['fEmpCodPessoa'],mb_convert_case($_POST['fEmpRazaoSocial'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpFantasia'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpCnpj'], MB_CASE_UPPER, "UTF-8"),$_POST['fPesjurInsmun'],$_POST['fPesjurInsest'],$_POST['fCodMatriz']);
 			$_SESSION['oMnyPessoaJuridica'] = $oMnyPessoaJuridica;
			
			$_POST['fTipCodigo']='257';
			$_POST['fCodStatus'] = '1';
			$_POST['fBcoCodigo'] = 'null';
			$_POST['fTipoconCod'] = 'null';
			
 			$oMnyPessoaGeral = $oFachadaFinanceiro->inicializarMnyPessoaGeral($_POST['fEmpCodPessoa'],$_POST['fTipCodigo'],$_POST['fCodStatus'],mb_convert_case($_POST['fEmpEndLogra'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndBairro'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCep'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndCidade'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpEndUf'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpEmail'],mb_convert_case($_POST['fEmpEndFone'], MB_CASE_UPPER, "UTF-8"),mb_strtoupper($_POST['fEmpInc']),mb_strtoupper($_POST['fEmpAlt']),$_POST['fBcoCodigo'],$_POST['fTipoconCod'],$_POST['fPesgBcoAgencia'],$_POST['fPesgBcoConta'],$_POST['fAtivo']);
			$_SESSION['oMnyPessoaGeral'] = $oMnyPessoaGeral;
			
						
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		

			$oValidate->add_text_field("Razao Social", $oSysEmpresa->getEmpRazaoSocial(), "text", "y");
			$oValidate->add_text_field("Nome Fantasia", $oSysEmpresa->getEmpFantasia(), "text", "y");
			$oValidate->add_text_field("Apelido", $oSysEmpresa->getApelido(), "text", "y");
			$oValidate->add_text_field("CNPJ", $oSysEmpresa->getEmpCnpj(), "text", "y");
			//$oValidate->add_text_field("Imagem", $oSysEmpresa->getEmpImagem(), "text", "y");
			$oValidate->add_text_field("Endereço", $oSysEmpresa->getEmpEndLogra(), "text", "y");
			$oValidate->add_text_field("Bairro", $oSysEmpresa->getEmpEndBairro(), "text", "y");
			$oValidate->add_text_field("Cidade", $oSysEmpresa->getEmpEndCidade(), "text", "y");
			$oValidate->add_text_field("Cep", $oSysEmpresa->getEmpEndCep(), "text", "y");
			$oValidate->add_text_field("Uf", $oSysEmpresa->getEmpEndUf(), "text", "y");
			$oValidate->add_text_field("Fone", $oSysEmpresa->getEmpEndFone(), "text", "y");
//			$oValidate->add_text_field("EmpSite", $oSysEmpresa->getEmpSite(), "text", "y");
//			$oValidate->add_text_field("EmpFtp", $oSysEmpresa->getEmpFtp(), "text", "y");
			$oValidate->add_text_field("Email", $oSysEmpresa->getEmpEmail(), "text", "y");
			$oValidate->add_text_field("EmpInc", $oSysEmpresa->getEmpInc(), "text", "y");
			if($sOP == 'Alterar')
				$oValidate->add_text_field("EmpAlt", $oSysEmpresa->getEmpAlt(), "text", "y");
			$oValidate->add_number_field("Aceite", $oSysEmpresa->getEmpAceite(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysEmpresa.preparaFormulario&sOP=".$sOP."&nIdSysEmpresa=".$_POST['fEmpCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		
		}
		


 		switch($sOP){
 			case "Cadastrar":
				$oFachadaFinanceiro = new FachadaFinanceiroBD();

				if($nId = $oFachadaFinanceiro->inserirMnyPessoaGeral($oMnyPessoaGeral)){
					$oMnyPessoaJuridica->setPesjurCodigo($nId);
					$oFachadaFinanceiro->inserirMnyPessoaJuridica($oMnyPessoaJuridica);
					unset($_SESSION['oMnyPessoaGeral']);
					unset($_SESSION['oMnyPessoaJuridica']);
					$oSysEmpresa->setEmpCodPessoa($nId);
 					if($nCodEmpresa = $oFachada->inserirSysEmpresa($oSysEmpresa)){
						// INICIO UPLOAD
						$sNomedoArquivo = $_FILES['fEmpImagem']['name'];
						if ($sNomedoArquivo){
							$oUpload = new Upload();
							if($_REQUEST['fEmpCodigo']){
								$oSysEmpresa = $oFachada->recuperarUmSysEmpresa($_REQUEST['fEmpCodigo']);
								$sDiretorio = $_SERVER['DOCUMENT_ROOT'] . "webmoney/view/sys/sys_empresa/logomarcas/";
								$sArquivo = $sDiretorio . $oSysEmpresa->getEmpImagem();
								if(is_file($sArquivo))
									unlink($sArquivo);
							}

							if (!($sNomedoArquivo = $oUpload->putFile($_FILES['fEmpImagem'],$nCodigo,$nCodEmpresa)))
								echo $_SESSION['sMsg'] = $oUpload->error;
								die();
						}else {
							$sNomedoArquivo = $_REQUEST['fEmpImagem'];
						}
                        $oSysEmpresa->setEmpImagem($sNomedoArquivo);
                        $oFachada->alterarSysEmpresa($oSysEmpresa);
                    }

				
				// FIM UPLOAD	
					
					
 					unset($_SESSION['oSysEmpresa']);
 					$_SESSION['sMsg'] = "Empresa inserida com sucesso!";
 					$sHeader = "?bErro=0&action=SysEmpresa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Empresa!";
 					$sHeader = "?bErro=1&action=SysEmpresa.preparaFormulario&sOP=".$sOP;
 				}


 			break;
 			case "Alterar":
			
				if($oFachadaFinanceiro->alterarMnyPessoaGeral($oMnyPessoaGeral)){
					$oFachadaFinanceiro->alterarMnyPessoaJuridica($oMnyPessoaJuridica);
 				//	$oFachada->alterarSysEmpresa($oSysEmpresa);
 				//	unset($_SESSION['oSysEmpresa']);
 					unset($_SESSION['oMnyPessoaGeral']);
					unset($_SESSION['oMnyPessoaJuridica'])
					;$_SESSION['sMsg'] = "Empresa alterada com sucesso!";
 					$sHeader = "?bErro=0&action=SysEmpresa.preparaLista";
 					
 				}
				
					// INICIO UPLOAD
				
					$sNomedoArquivo = $_FILES['fEmpImagem']['name'];
					if ($sNomedoArquivo) {
						$oUpload = new Upload();
						if($_REQUEST['fEmpCodigo']){
							$oSysEmpresa = $oFachada->recuperarUmSysEmpresa($_REQUEST['fEmpCodigo']);
							
							$sDiretorio = $_SERVER['DOCUMENT_ROOT'] . "webmoney_desenv/view/sys/sys_empresa/logomarcas/";
							$sArquivo = $sDiretorio . $oSysEmpresa->getEmpImagem();
							
							if(is_file($sArquivo))
								unlink($sArquivo);
						}
		
						if (!($sNomedoArquivo = $oUpload->putFile($_FILES['fEmpImagem'],$_REQUEST['fEmpCodigo'],""))) 
							echo $_SESSION['sMsg'] = $oUpload->error;
					}else {
						$sNomedoArquivo = $_REQUEST['fEmpImagem'];
					}
					
					$oSysEmpresa->setEmpImagem($sNomedoArquivo);

				
				// FIM UPLOAD					
			
 				if($oFachada->alterarSysEmpresa($oSysEmpresa)){
 					unset($_SESSION['oSysEmpresa']);
 					$_SESSION['sMsg'] = "Empresa alterada com sucesso!";
 					$sHeader = "?bErro=0&action=SysEmpresa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Empresa!";
 					$sHeader = "?bErro=1&action=SysEmpresa.preparaFormulario&sOP=".$sOP."&nIdSysEmpresa=".$_POST['fEmpCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSysEmpresa']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSysEmpresa($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Empresa(s) exclu&iacute;da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SysEmpresa.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir a(s) SysEmpresa!";
 					$sHeader = "?bErro=1&action=SysEmpresa.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}

 
    public function carregaEmpresaPorUnidade(){
        $oFachada = new FachadaSysBD;
        $_REQUEST['voSysEmpresa'] = $oFachada->recuperarTodosSysEmpresaPorUnidade($_GET['nCodUnidade']);
        $_REQUEST['nCodUnidade'] = $_GET['nCodUnidade'];
        include_once("view/sys/sys_empresa/empresa_ajax.php");
        exit();



    }



 }
 
 
 ?>
