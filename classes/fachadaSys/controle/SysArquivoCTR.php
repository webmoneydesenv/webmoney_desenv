<?php
 class SysArquivoCTR implements IControle{

 	public function SysArquivoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSysBD();

 		$voSysArquivo = $oFachada->recuperarTodosSysArquivo();

 		$_REQUEST['voSysArquivo'] = $voSysArquivo;
 		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();

		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();

		$_REQUEST['voMnyConciliacao'] = $oFachada->recuperarTodosMnyConciliacao();


 		include_once("view/sys/sys_arquivo/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSysBD();

 		$oSysArquivo = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSysArquivo = ($_POST['fIdSysArquivo'][0]) ? $_POST['fIdSysArquivo'][0] : $_GET['nIdSysArquivo'];

 			if($nIdSysArquivo){
 				$vIdSysArquivo = explode("||",$nIdSysArquivo);
 				$oSysArquivo = $oFachada->recuperarUmSysArquivo($vIdSysArquivo[0]);
 			}
 		}

 		$_REQUEST['oSysArquivo'] = ($_SESSION['oSysArquivo']) ? $_SESSION['oSysArquivo'] : $oSysArquivo;
 		unset($_SESSION['oSysArquivo']);

 		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();

		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();

		$_REQUEST['voMnyConciliacao'] = $oFachada->recuperarTodosMnyConciliacao();


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/sys/sys_arquivo/detalhe.php");
 		else
 			include_once("view/sys/sys_arquivo/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaSysBD();
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaView = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oSysArquivo = $oFachada->inicializarSysArquivo($_POST['fCodArquivo'],mb_convert_case($_POST['fNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fArquivo'],mb_convert_case($_POST['fExtensao'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodTipo'],$_POST['fTamanho'],$_POST['fDataHora'],mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo'],$_POST['fContratoCodigo'],$_POST['fCodConciliacao'],$_POST['fMovCodigo'],$_POST['fMovItem']);
 			$_SESSION['oSysArquivo'] = $oSysArquivo;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_text_field("Nome", $oSysArquivo->getNome(), "text", "y");
			//$oValidate->add_text_field("Arquivo", $oSysArquivo->getArquivo(), "text", "y");
			//$oValidate->add_text_field("Extensao", $oSysArquivo->getExtensao(), "text", "y");
			//$oValidate->add_number_field("CodTipo", $oSysArquivo->getCodTipo(), "number", "y");
			//$oValidate->add_number_field("Tamanho", $oSysArquivo->getTamanho(), "number", "y");
			//$oValidate->add_date_field("DataHora", $oSysArquivo->getDataHora(), "date", "y");
			//$oValidate->add_text_field("RealizadoPor", $oSysArquivo->getRealizadoPor(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oSysArquivo->getAtivo(), "number", "y");
			//$oValidate->add_number_field("ContratoCodigo", $oSysArquivo->getContratoCodigo(), "number", "y");
			//$oValidate->add_number_field("CodConciliacao", $oSysArquivo->getCodConciliacao(), "number", "y");
			//$oValidate->add_number_field("MovCodigo", $oSysArquivo->getMovCodigo(), "number", "y");
			//$oValidate->add_number_field("MovItem", $oSysArquivo->getMovItem(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SysArquivo.preparaFormulario&sOP=".$sOP."&nIdSysArquivo=".$_POST['fCodArquivo'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirSysArquivo($oSysArquivo)){
 					unset($_SESSION['oSysArquivo']);
 					$_SESSION['sMsg'] = "sys_arquivo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=SysArquivo.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o sys_arquivo!";
 					$sHeader = "?bErro=1&action=SysArquivo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
                    // gravar arquivos
                    //tramitar

                    //print_r($_REQUEST['fCodArquivo']);
                    //die();

                    $nErro=0;
                    $vCampoChave = array();
                    $arquivos = $_FILES["arquivo"];
                    $tamanhoMaximo = $_REQUEST['fTamanhoArquivo'];
                    $nCodArquivo = $_REQUEST['fCodArquivo'];
                    $oSysArquivo =  $oFachada->recuperarUmSysArquivo($_REQUEST['fCodArquivo']);

                    $vIdArquivo = array();

                    if(!$arquivos['name']){
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir comprovante!";
                        $sHeader = "?bErro=1&action=?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=".$_REQUEST['fFA'];
                    }else{
                        if($arquivos['name'] != ""){
                            $vFA = explode("||",$_REQUEST['fFA']);
                            $_POST['fMovCodigo'] = $vFA[0];
                            $_POST['fMovItem'] = $vFA[1];
                            $nCodContratoDocTipo = $_REQUEST['fCodContratoDocTipo'];//11;
                            if($oSysArquivo){
                                switch($oSysArquivo->getCodTipo()){
                                    case 11:
                                        $_POST['fDescricao'] = "Comprovante atualizado conforme orientação da Reconciliação";

                                        break;

                                }
                            }else{
                                $_POST['fDescricao'] = "Comprovante inserido com sucesso";
                            }

                            $oSysArquivo->setTamanho($arquivos['size']);
                            $nomeTemporario = $arquivos['tmp_name'];
                           // $vPermissao = array("application/pdf");
                            //strtok($arquivos['type'], ";");
                            $oSysArquivo->setRealizadoPor($_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i:s'));

                             //  USADO PARA ARMAZENAR O ARQUIVO NO BANCO DE DADOS...
                             //$oSysArquivo = $oFachadaSys->inicializarSysArquivo($_POST['fCodArquivo'],$nomeArquivo,$blobArquivo,$tipoArquivo,$nCodContratoDocTipo,$tamanhoArquivo,date('Y-m-d H:i:s'),mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),1,'NULL','NULL', $_POST['fMovCodigo'], $_POST['fMovItem']);
                            if($_FILES["arquivo"]["tmp_name"]){
                               $Arquivo = $_FILES["arquivo"]["tmp_name"];
                               $Tipo = $_FILES["arquivo"]["type"];

                                move_uploaded_file($Arquivo, "c:\\wamp64\\tmp\\arquivo.pdf");

                                $blobArquivo = fopen("c:\\wamp64\\tmp\\arquivo.pdf", "rb");
                                $blobArquivo = addslashes(fread($blobArquivo, filesize("c:\\wamp64\\tmp\\arquivo.pdf")));
                                $oSysArquivo->setArquivo($blobArquivo);
                                unlink("c:\\wamp64\\tmp\\arquivo.pdf");


                                $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                                $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];

                                //$oUplaod = UploadMultiplo::AlteraUpload($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$_POST['fMovCodigo']);

                                if($_POST['fCodArquivo']){
                                    $oFachada->alterarSysArquivo($oSysArquivo);
                                    $oMnyMovimentoItemProtocolo = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],10,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                     $vIdMovimentoItemProtocolo = $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                                }else{
                                    if(!$nIdArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo)){
                                        $nErro++;
                                        $_SESSION['sMsg'] = "Erro 6: não inserido no banco";
                                    }else{
                                        $vIdArquivo[] = $nIdArquivo;
                                        $_POST['fDescricao'] = "ENCAMINHADO PARA CONCILIAÇÃO BANCÁRIA.";
                                        $oMnyMovimentoItemProtocolo = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],10,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                        $vIdMovimentoItemProtocolo[] = $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                                    }
                                }


                            }
                        }

                }
                if($nErro ==0){
                     $_SESSION['sMsg'] = "Comprovante atualizado com sucesso!";
                    // falta ver se esse link esta correto
                }else{
                    $_SESSION['sMsg'] .= "Ocorreu [". $nErro ."] problema(s) na inserção do comprovante! Tente novamente!";
                }
                $sHeader = "?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=".$_REQUEST['fFA'];

                break;
 			case "Excluir":
  				$bResultado = true;
                $nCodArquivo = $_REQUEST['fCodArquivo'];

                if($oFachada->presenteSysArquivo($nCodArquivo)){
                    if($oFachada->excluirSysArquivo($nCodArquivo)){
                        $_SESSION['sMsg'] = "Arquivo exclu&iacute;do com sucesso!";
                    } else {
                        $_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel excluir o arquivo!";
                    }
                }else{
                     $_SESSION['sMsg2'] = "N&atilde;o foi poss&iacute;vel encontrar o arquivo!";
                }

                if($_REQUEST['nCodTransferencia']){
                   $sHeader = "?action=MnyMovimento.preparaFormularioTransferencia&sOP=AlterarTransferencia&nIdMnyTransferencia=".$_REQUEST['nCodTransferencia'];

                }elseif($_REQUEST['fMovCodigo']){
                    $oMnyMovimento = $oFachadaFinanceiro->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);
                    $sTipoMovimento = ($oMnyMovimento->getMovTipo() == 188) ? "Pagar" : "Receber";
                    $sHeader = "?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$sTipoMovimento."&nIdMnyMovimento=".$_REQUEST['fMovCodigo'];
                }elseif($_REQUEST['fContratoCodigo']){
                    $oContrato = $oFachadaFinanceiro->recuperarUmMnyContratoPessoa($_REQUEST['fContratoCodigo']);
                    switch($oContrato->getTipoContrato()){
                            case 1:	$sTipoContrato = "CONTRATO"; break;
                            case 2:	$sTipoContrato = "ASE"; break;
                            case 10:	$sTipoContrato = "ASEFRETE"; break;
                            case 3:	$sTipoContrato = "OC";  break;
                    }
                    $sHeader = "?action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&sOP=Alterar&Pagina=2&fTipoContrato=".$oContrato->getTipoContrato()."&nIdMnyContratoPessoa=".$oContrato->getContratoCodigo();

                }
 			break;

 		}

 		header("Location: ".$sHeader);

 	}

    function preparaArquivo(){
         $nCodArquivo = $_GET['fCodArquivo'];
         $oFachadaSys = new FachadaSysBD();
        if($_REQUEST['oArquivo'] = $oFachadaSys->recuperarUmSysArquivo($nCodArquivo)){
            $_REQUEST['oArquivo'] = $_REQUEST['oArquivo']->getArquivo();
            include_once('view/sys/sys_arquivo/anexo.php');
        }else{
            $_SESSION['sMsg'] = "Arquivo não encontrado!";
 			$sHeader = "?bErro=1";
            header("Location: ".$sHeader);
        }
         exit();
    }

 }


 ?>
