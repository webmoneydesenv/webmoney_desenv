<?php
 /**
  * @author Auto-Generated
  * @package fachadaSuporte
  * @SGBD mysql
  * @tabela sup_chamado
  */
 class SupChamado{
 	/**
	* @campo cod_chamado
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodChamado;
	/**
	* @campo motivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMotivo;
	/**
	* @campo descricao_chamado
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricaoChamado;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo cod_usuario
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodUsuario;
    /**
	* @campo situacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nSituacao;
    /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;


 	public function __construct(){

 	}

 	public function setCodChamado($nCodChamado){
		$this->nCodChamado = $nCodChamado;
	}

    public function getCodChamado(){
		return $this->nCodChamado;
	}

    public function setMotivo($sMotivo){
		$this->sMotivo = $sMotivo;
	}

    public function getMotivo(){
		return $this->sMotivo;
	}

    public function setDescricaoChamado($sDescricaoChamado){
		$this->sDescricaoChamado = $sDescricaoChamado;
	}

    public function getDescricaoChamado(){
		return $this->sDescricaoChamado;
	}

    public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}

    public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}

    public function setSituacao($nSituacao){
		$this->nSituacao = $nSituacao;
	}

    public function getSituacao(){
		return $this->nSituacao;
	}

    public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}

    public function getCodUsuario(){
		return $this->nCodUsuario;
	}

    public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}

    public function getAtivo(){
		return $this->nAtivo;
	}

 }
 ?>
