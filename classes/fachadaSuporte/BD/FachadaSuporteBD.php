    <?

    class FachadaSuporteBD {
    /**
         *
         * Método para inicializar um Objeto SupChamado
         *
         * @return SupChamado
         */
        public function inicializarSupChamado($nCodChamado,$sMotivo,$sDescricaoChamado,$sRealizadoPor,$nCodUsuario,$nSituacao,$nAtivo){
            $oSupChamado = new SupChamado();

            $oSupChamado->setCodChamado($nCodChamado);
            $oSupChamado->setMotivo($sMotivo);
            $oSupChamado->setDescricaoChamado($sDescricaoChamado);
            $oSupChamado->setRealizadoPor($sRealizadoPor);
            $oSupChamado->setCodUsuario($nCodUsuario);
            $oSupChamado->setSituacao($nSituacao);
            $oSupChamado->setAtivo($nAtivo);
            return $oSupChamado;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto SupChamado
         *
         * @return boolean
         */
        public function inserirSupChamado($oSupChamado){
            $oPersistencia = new Persistencia($oSupChamado);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto SupChamado
         *
         * @return boolean
         */
        public function alterarSupChamado($oSupChamado){
            $oPersistencia = new Persistencia($oSupChamado);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto SupChamado
         *
         * @return boolean
         */
        public function excluirSupChamado($nCodChamado){
            $oSupChamado = new SupChamado();

            $oSupChamado->setCodChamado($nCodChamado);
            $oPersistencia = new Persistencia($oSupChamado);
                $bExcluir = $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto SupChamado na base de dados
         *
         * @return boolean
         */
        public function presenteSupChamado($nCodChamado){
            $oSupChamado = new SupChamado();

            $oSupChamado->setCodChamado($nCodChamado);
            $oPersistencia = new Persistencia($oSupChamado);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto SupChamado da base de dados
         *
         * @return SupChamado
         */
        public function recuperarUmSupChamado($nCodChamado){
            $oSupChamado = new SupChamado();
            $oPersistencia = new Persistencia($oSupChamado);
            $sTabelas = "sup_chamado";
            $sCampos = "*";
            $sComplemento = " WHERE cod_chamado = $nCodChamado";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            $voSupChamado = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voSupChamado)
                return $voSupChamado[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos SupChamado da base de dados
         *
         * @return SupChamado[]
         */
        public function recuperarTodosSupChamado(){
            $oSupChamado = new SupChamado();
            $oPersistencia = new Persistencia($oSupChamado);
            $sTabelas = "sup_chamado";
            $sCampos = "*";
            $sComplemento = "WHERE ativo=1";
            $voSupChamado = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voSupChamado)
                return $voSupChamado;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos SupChamado da base de dados
         *
         * @return SupChamado[]
         */
        public function recuperarTodosSupChamadoPorSolicitante($nCodUsuario){
            $oSupChamado = new SupChamado();
            $oPersistencia = new Persistencia($oSupChamado);
            $sTabelas = "sup_chamado";
            $sCampos = "*";
            if($_SESSION['Perfil']['CodGrupoUsuario']=='1'){
                $sComplemento = "WHERE ativo=1";
            }else{
                $sComplemento = "WHERE ativo=1 And cod_usuario=".$nCodUsuario;
            }

            $voSupChamado = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voSupChamado)
                return $voSupChamado;
            return false;
        }

    }
    ?>
