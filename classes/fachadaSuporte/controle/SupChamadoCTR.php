<?php
 class SupChamadoCTR implements IControle{

 	public function SupChamadoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaSuporteBD();

        $voSupChamado = $oFachada->recuperarTodosSupChamadoPorSolicitante($_SESSION['Perfil']['CodUsuario']);

 		$_REQUEST['voSupChamado'] = $voSupChamado;


        if($_SESSION['Perfil']['CodUsuario'] != 1){
            include_once("view/suporte/sup_chamado/index.php");
        }else{
            include_once("view/suporte/sup_chamado/index_admin.php");
        }



 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaSuporteBD();

 		$oSupChamado = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdSupChamado = ($_POST['fIdSupChamado'][0]) ? $_POST['fIdSupChamado'][0] : $_GET['nIdSupChamado'];

 			if($nIdSupChamado){
 				$vIdSupChamado = explode("||",$nIdSupChamado);
 				$oSupChamado = $oFachada->recuperarUmSupChamado($vIdSupChamado[0]);
 			}
 		}

 		$_REQUEST['oSupChamado'] = ($_SESSION['oSupChamado']) ? $_SESSION['oSupChamado'] : $oSupChamado;
 		unset($_SESSION['oSupChamado']);


 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/suporte/sup_chamado/detalhe.php");
 		else
 			include_once("view/suporte/sup_chamado/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
        date_default_timezone_set('America/Sao_Paulo');
        include('email/PHPMailer.php'); //chama a classe de onde você a colocou.
        include('email/SMTP.php'); //chama a classe de onde você a colocou.
        include('email/POP3.php'); //chama a classe de onde você a colocou.
 		$oFachada = new FachadaSuporteBD();
 		$oFachadaView = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){

            $_POST['fSituacao'] = ($_POST['fSituacao']) ? $_POST['fSituacao'] : 0;
            $_POST['fCodUsuario'] = ($_POST['fCodUsuario']) ? $_POST['fCodUsuario'] : $_SESSION['Perfil']['CodUsuario'];
 			$oSupChamado = $oFachada->inicializarSupChamado($_POST['fCodChamado'],$_POST['fMotivo'],$_POST['fDescricaoChamado'],$_POST['fRealizadoPor'],$_POST['fCodUsuario'],$_POST['fSituacao'],$_POST['fAtivo']);

            $_SESSION['oSupChamado'] = $oSupChamado;
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			//$oValidate->add_number_field("CodSolicitacaoAporte", $oMnyAporteItem->getCodSolicitacaoAporte(), "number", "y");
			//$oValidate->add_number_field("AporteItem", $oMnyAporteItem->getAporteItem(), "number", "y");
			//$oValidate->add_number_field("MovCodigo", $oMnyAporteItem->getMovCodigo(), "number", "y");
			//$oValidate->add_number_field("MovItem", $oMnyAporteItem->getMovItem(), "number", "y");
			//$oValidate->add_number_field("Ativo", $oMnyAporteItem->getAtivo(), "number", "y");

 			if(!$oValidate->validation()){
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=SupChamado.preparaFormulario&sOP=".$sOP."&nIdSupChamado=".$_POST['fCodSupChamado'];
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 	          if($oFachada->inserirSupChamado($oSupChamado)){
                  	$_SESSION['sMsg'] = "solicitação inserida com sucesso!";
 					$sHeader = "?bErro=1&action=SupChamado.preparaLista";

              } else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a sua solicitação!";
 					$sHeader = "?bErro=1&action=SupChamado.preparaFormulario&sOP=".$sOP;
 			  }
 			break;
 			case "Alterar":
 				if($oFachada->alterarSupChamado($oSupChamado)){
 					unset($_SESSION['oSupChamado']);
 					$_SESSION['sMsg'] = "Chamadpo alterada com sucesso!";
 					$sHeader = "?bErro=0&action=SupChamado.preparaLista";
                } else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a solicitação!";
 					$sHeader = "?bErro=1&action=SupChamado.preparaFormulario&sOP=".$sOP."&nIdSupChamado=".$_POST['fCodChamado'];
 				}
 			break;
 			case "Finalizar":
                     //print_r($oSupChamado->getMotivo()." <br><p><span style='font-weight:bold;color:red;'>SOLICITAÇÃO REALIZADA COM SUCESSO !</span> <br><p> <span style='font-weight:italic;'>att, suporte WebMoney</span>");die();
                     $oFachadaPermissao = new FachadaPermissaoBD();

                     $oSupChamado->setSituacao(2);
                     if($oFachada->alterarSupChamado($oSupChamado)){

                            $oAcessoUsuario = $oFachadaPermissao->recuperarUmAcessoUsuario($oSupChamado->getCodUsuario());

                            $sEmailUsuario =  $oFachadaView->recuperarUmVPessoaGeralFormatada($oAcessoUsuario->getPesgCodigo())->getPesgEmail();

                           //  echo !extension_loaded('openssl')?"Not Available":"Available <br>";



                           $sRemetente = $_SESSION['Perfil'][''];
                            //$sAssunto = $oSupChamado->getMovitvo();
                           // $sMensagem = $oSupChamado->getDescricaoChamado();


                            $enviaFormularioParaNome = $oAcessoUsuario->getNome();
                            //$enviaFormularioParaEmail = 'diegorafaelbiga@gmail.com';
                            $enviaFormularioParaEmail = strtolower($sEmailUsuario);
                            $caixaPostalServidorNome = 'WebMoney | Suporte';
                            $caixaPostalServidorEmail = 'suporte@carmonacabrera.com.br';
                            $caixaPostalServidorSenha = 'suporte2017';

                            $mail = new PHPMailer();

                            $mail->IsSMTP();
                            $mail->SMTPAuth  = true;
                            $mail->Charset   = 'utf8_decode()';
                            $mail->Host  = 'smtp.'.substr(strstr($caixaPostalServidorEmail, '@'), 1);
                            $mail->Port  = '587';
                            $mail->Username  = $caixaPostalServidorEmail;
                            $mail->Password  = $caixaPostalServidorSenha;
                            $mail->From  = $caixaPostalServidorEmail;
                            $mail->FromName  = utf8_decode($caixaPostalServidorNome);
                            $mail->IsHTML(true);
                            $mail->Subject  = utf8_decode($oSupChamado->getDescricaoChamado());
                            //$mail->Subject  = utf8_decode($sAssunto);
                            $mail->Body  = utf8_decode($oSupChamado->getMotivo()." <br><p><span style='font-weight:bold;color:red;'>SOLICITAÇÃO REALIZADA COM SUCESSO !</span> <br><p> <span style='font-weight:italic;'>att, suporte WebMoney</span>");

                            $mail->AddAddress($enviaFormularioParaEmail,utf8_decode($enviaFormularioParaNome));

                            if(!$mail->Send()){
                              print($mail->ErrorInfo);
                            }else{
                                echo  'Formulário enviado com sucesso!';
                            }


                            unset($_SESSION['oSupChamado']);
                            $_SESSION['sMsg'] = "Chamadpo alterada com sucesso!";
                            $sHeader = "?bErro=0&action=SupChamado.preparaLista";
                } else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a solicitação!";
 					$sHeader = "?bErro=1&action=SupChamado.preparaFormulario&sOP=".$sOP."&nIdSupChamado=".$_POST['fCodChamado'];
 				}
 			break;
            case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdSupChamado']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirSupChamado($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Solicitação(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=SupChamado.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) solicitação!";
 					$sHeader = "?bErro=1&action=SupChamado.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}


 }


 ?>
