<?
 class LogPersistencia{
 	
 	public static function escreverPersistencia($sDescricao){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/'))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/');
 		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/');
 		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y"));
 		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m"));
 		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")."/".date("d")))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")."/".date("d"));
 		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")."/".date("d")."/".session_id()))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")."/".date("d")."/".session_id());
 			
 		$sPersistencia = "[".date("m/d/Y H:i:s")."] ".$sDescricao.";\r\n";	
 			
 		$fonte = fopen($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/persistencia/'.date("Y")."/".date("m")."/".date("d")."/".session_id().'/log_persistencia.txt','a+');
 		if(fwrite($fonte,$sPersistencia."\r\n"))
 			return true;
 	}
 }
 ?>