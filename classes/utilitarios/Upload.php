<?
class Upload {
	var $arquivo_permissao;
	var $sDir;
	var $max_filesize;
	var $idioma;
	var $error = 0;
	var $file_type;
	var $file_name;
	var $file_size;
	var $arquivo;
	var $file_path;
	var $warning = 0;
	var $temp;

	function Upload($permissao="" , $max_file = 12000000, $idioma="portugues"){
		if (empty ($permissao)) 
			$permissao = array ("application/pdf","image/jpeg","image/gif","image/png");
//			$permissao = array ("application/octet-stream","text/plain","image/jpeg","image/png","application/msword","application/vnd.ms-excel","application/vnd.ms-powerpoint","application/zip","application/pdf","image/vnd.dwg");
		$this->arquivo_permissao = $permissao;
		$this->sDir = $_SERVER['DOCUMENT_ROOT'] . "webmoney/controle/sys_empresa/logomarcas";
		$this->max_filesize = $max_file;
		$this->idioma = $idioma;
		$this->file_path =   "arquivos/";
	}
	
	function pegaExtensao($file) {
  	     $sExtensao = strrchr($file, '.');
  	     return $sExtensao;
  	}
	
	function geraNomeAleatorio(){
		$sConso = 'bcdfghjklmnpqrstvwxyzbcdfghjklmnpqrstvwxyz';
		$sVogal = 'aeiou';
		$sNum = '123456789';
		$sSenha = ' ';
	
		$y = strlen($sConso)-1; //conta o nº de caracteres da variável $sConso
		$z = strlen($sVogal)-1; //conta o nº de caracteres da variável $sVogal
		$r = strlen($sNum)-1; //conta o nº de caracteres da variável $sNum
		
		for($x=0;$x<=1;$x++){
			$rand = rand(0,$y); //Funçao rand() - gera um valor randômico
			$rand1 = rand(0,$z);
			$rand2 = rand(0,$r);
			$str = substr($sConso,$rand,3); // substr() - retorna parte de uma string
			$str1 = substr($sVogal,$rand1,2);
			$str2 = substr($sNum,$rand2,3);
			$sSenha = $str.$str1.$str2;
		}
		
		return $sSenha;
	}

	function putFile($file,$sPrefixo,$nUsuario){
		
		$nomeArquivo =  $sPrefixo . "_" . $nUsuario . "_" . $file['name'];					
		$tamanhoArquivo = $file["size"];
		$nomeTemporario = $file["tmp_name"];
		$caminho =   $_SERVER['DOCUMENT_ROOT'] . "/webmoney/controle/sys_empresa/logomarcas";
		
		$this->file_type = strtok($file['type'], ";");
		//$sNomeArquivo = $file['name'];
		//$sNomeArquivo = strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($sNomeArquivo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ "),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
		//$sNomeArquivo = $sPrefixo ."_". $nUsuario . '_'. $sNomeArquivo;
		
		$sNomeArquivo = str_replace(" ", "-",$sNomeArquivo);
	
		$this->file_name =  $sNomeArquivo;
		$this->file_size =  $file['size'];
		$this->temp = $file['tmp_name'];  // upload para o diretorio temp
	
		if (!in_array($this->file_type, $this->arquivo_permissao)) 	$sMsg = $this->Error(1);
        if (($this->file_size <= 0) || ($this->file_size > $this->max_filesize)) $sMsg = $this->Error(2);
        if ($this->error == ""){
			$filename = basename ($this->file_name);
			if(!is_dir($this->sDir)){
				 mkdir ($this->sDir);
				 chmod($_SERVER['DOCUMENT_ROOT'].$this->sDir, 777);
			}
 		 	$this->arquivo = $this->sDir . $filename;
		//	if (file_exists($this->arquivo)) unlink($this->arquivo);
			$sArquivo = $this->arquivo;//$this->file_path . $filename;
			// deu erro $this->arquivo;
			// deu erro $sArquivo;
			// deu erro $this->file_path.$this->file_name
	 
			if(is_dir($_SERVER['DOCUMENT_ROOT'].$this->sDir)){
				chmod($_SERVER['DOCUMENT_ROOT'].$this->sDir, 777);
			}
			
			if(!is_uploaded_file($this->temp)) $sMsg = $this->Error(3);
			//if(!move_uploaded_file($this->temp,$sArquivo)) $sMsg = $this->Error(4);
			if(!(move_uploaded_file($nomeTemporario, ($caminho . $nomeArquivo)))) $sMsg = $this->Error(4);
			
			if($sMsg) return false; else return  $nomeArquivo ;
			
			//if($sMsg) return false; else return $sArquivo;
		}
		else {
			//echo $this->error;
			return false;
        }

	}

	function Error($op){
		if($this->idioma="portugues"){
			switch ($op){
				case 0: return; break;
				case 1: $this->error = "Erro 1: Este tipo de arquivo n&atilde;o tem permiss&atilde;o para upload : $this->file_type."; break;
				case 2: $this->error = "Erro 2: Erro no tamanho do arquivo: $this->file_size Kb. Tamanho M&aacute;ximo &eacute; $this->max_filesize."; break;
				case 3: $this->error = "Erro 3: Ocorreu um erro na transf&ecirc;rencia do arquivo: $this->file_name."; break;
				case 4: $this->error = "Erro 4: Ocorreu um erro na transf&ecirc;rencia de $this->temp para $this->arquivo."; break;
			//	case 5: $this->error = "Erro 5: J&aacute; existe um arquivo com este nome, renomeie o arquivo $this->file_name e tente novamente! ";break;
			}
		return $this->error;
		}
	}
}
?>
