    <?

    class FachadaFinanceiroBD {

    /**
         *
         * Método para inicializar um Objeto MnyConciliacao
         *
         * @return MnyConciliacao
         */
        public function inicializarMnyConciliacao($nCodConciliacao,$nMovCodigo,$nMovItem,$nCodUnidade,$nTipoCaixa,$nCodCartaoCredito,$nCodContaCorrente,$dData,$nValorVale,$nValorResgateIof,$ValorResgateIrrf,$sObservacao,$sRealizadoPor,$nConsolidado,$dDataConciliacao,$nOrdem,$nAtivo){
            $oMnyConciliacao = new MnyConciliacao();

            $oMnyConciliacao->setCodConciliacao($nCodConciliacao);
            $oMnyConciliacao->setMovCodigo($nMovCodigo);
            $oMnyConciliacao->setMovItem($nMovItem);
            $oMnyConciliacao->setCodUnidade($nCodUnidade);
            $oMnyConciliacao->setTipoCaixa($nTipoCaixa);
            $oMnyConciliacao->setCodCartaoCredito($nCodCartaoCredito);
            $oMnyConciliacao->setCodContaCorrente($nCodContaCorrente);
            $oMnyConciliacao->setDataBanco($dData);
            $oMnyConciliacao->setValorValeBanco($nValorVale);
            $oMnyConciliacao->setValorResgateIofBanco($nValorResgateIof);
            $oMnyConciliacao->setValorResgateIrrfBanco($ValorResgateIrrf);
            $oMnyConciliacao->setObservacao($sObservacao);
            $oMnyConciliacao->setRealizadoPor($sRealizadoPor);
            $oMnyConciliacao->setConsolidado($nConsolidado);
            $oMnyConciliacao->setDataConciliacaoBanco($dDataConciliacao);
            $oMnyConciliacao->setOrdem($nOrdem);
            $oMnyConciliacao->setAtivo($nAtivo);
            return $oMnyConciliacao;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyConciliacao
         *
         * @return boolean
         */
        public function inserirMnyConciliacao($oMnyConciliacao){
            $oPersistencia = new Persistencia($oMnyConciliacao);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyConciliacao
         *
         * @return boolean
         */
        public function alterarMnyConciliacao($oMnyConciliacao){
            $oPersistencia = new Persistencia($oMnyConciliacao);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }

         /**
         *
         * Método para abrir a conciliacao na base de dados
         *
         * @return boolean
         */
        public function abrirMnyConciliacao($dDataConciliacao, $nCcrCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sProcedure = "sp_abre_sistema_para_lancamento";
            $sParametros = "'". $dDataConciliacao . "'," . $nCcrCodigo ;
            $oMnyConciliacao = $oPersistencia->consultarProcedureLista($sProcedure,$sParametros);

            if($oMnyConciliacao)
                return $oMnyConciliacao[0]->data_conciliacao;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyConciliacao
         *
         * @return boolean
         */
        public function excluirMnyConciliacao($nCodConciliacao){
            $oMnyConciliacao = new MnyConciliacao();

            $oMnyConciliacao->setCodConciliacao($nCodConciliacao);
            $oPersistencia = new Persistencia($oMnyConciliacao);
                $bExcluir = $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }
        /**
         *
         * Método para excluir da base de dados um Objeto MnyConciliacao
         *
         * @return boolean
         */
        public function alterarGeralMnyConciliacao($dData, $nCcrCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabela = "mny_conciliacao";
            $sComplemento = " consolidado = 1 WHERE data_conciliacao='" . $dData . "' AND cod_conta_corrente = " . $nCcrCodigo . " AND consolidado = 0";
          // echo " $sTabela $sComplemento";
            if($oPersistencia->alterarSemChavePrimaria($sComplemento))
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyConciliacao na base de dados
         *
         * @return boolean
         */
        public function presenteMnyConciliacao($nCodConciliacao){
            $oMnyConciliacao = new MnyConciliacao();

            $oMnyConciliacao->setCodConciliacao($nCodConciliacao);
            $oPersistencia = new Persistencia($oMnyConciliacao);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyConciliacao da base de dados
         *
         * @return MnyConciliacao
         */
        public function recuperarUmMnyConciliacao($nCodConciliacao){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = " WHERE cod_conciliacao = $nCodConciliacao";
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyConciliacao da base de dados
         *
         * @return MnyConciliacao
         */
        public function recuperarUmMnyConciliacaoPorFA($nMovCodigo,$nMovItem){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = $nMovCodigo AND mov_item=$nMovItem";
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }


        public function recuperarUmMnyConciliacaoMesmaOrdem($nOrdem, $nCodConciliacao,$nData,$nCodUnidade, $nCodConta){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = " WHERE ordem = $nOrdem  AND data_conciliacao ='". $nData ."'  AND cod_conciliacao <> $nCodConciliacao AND cod_unidade=$nCodUnidade AND cod_conta_corrente=$nCodConta";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
        public function recuperarTodosMnyConciliacao(){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = "WHERE ativo = 1 ";
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
        public function recuperarTodosMnyConciliacaoAplcicaoResgate($nCodUnidade, $nCodContaCorrente, $nDataConcilaicao){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "  mny_conciliacao.mov_codigo,
                          mny_movimento_item.mov_item,
                          mny_movimento_item.mov_valor_parcela,
                          mny_transferencia.operacao AS ordem";
            $sComplemento = "INNER JOIN mny_transferencia ON (mny_transferencia.mov_codigo_origem=mny_conciliacao.mov_codigo) OR (mny_transferencia.mov_codigo_destino=mny_conciliacao.mov_codigo)
                             INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_transferencia.mov_codigo_destino OR mny_movimento_item.mov_codigo = mny_transferencia.mov_codigo_origem
                             WHERE cod_unidade=".$nCodUnidade." AND cod_conta_corrente=".$nCodContaCorrente." AND data_conciliacao=". $nDataConcilaicao;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

            /**
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
        //CONCILIAÇÃO CONTA / UNIDADE
        public function recuperarTodosMnyConciliacaoDadosGerais2($nMovCodigo, $nMovItem, $nCcrCodigo, $nCodUnidade , $dDataConciliacao){
            //$oMnyConciliacao = new MnyConciliacao();
            $oMnyConciliacao = new VMovimentoConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "sp_insere_linha";
            //$sCampos = "";
            $sComplemento = $nMovCodigo . "," . $nMovItem . ", " . $nCcrCodigo . ", " .$nCodUnidade . ", '" . $dDataConciliacao . "'";
        //     echo "call sp_insere_linha($sComplemento)  ";
        //     die();
            $voMnyConciliacao = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarTodosMnyConciliacaoDadosGerais($nMovCodigo, $nMovItem, $nCcrCodigo, $dDataConciliacao){
            //$oMnyConciliacao = new MnyConciliacao();
            $oMnyConciliacao = new VMovimentoConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "sp_insere_linha";
            //$sCampos = "";
            $sComplemento = $nMovCodigo . "," . $nMovItem . ", " . $nCcrCodigo . ", '" . $dDataConciliacao . "'";
            //echo "call sp_insere_linha($sComplemento)  ";
            //die();
            $voMnyConciliacao = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }


        /** agora está usando PROCEDURE...
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
     /*	public function recuperarTodosMnyConciliacaoDadosGerais($nMovCodigo, $nMovItem, $nCcrCodigo, $nCodUnidade){
            //$oMnyConciliacao = new MnyConciliacao();
            $oMnyConciliacao = new VMovimentoConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_movimento";
            $sCampos = "DISTINCT  mny_movimento.ativo AS cod_conciliacao,
                                  mny_movimento.mov_codigo AS mov_codigo,
                                  mny_movimento_item.mov_item AS mov_item,
                                 `v_pessoa_geral_formatada`.`nome` AS `nome`,
                                 `mny_movimento_item`.`mov_valor_parcela` AS `mov_valor_parcela`,
                                 `mny_movimento`.`pes_codigo` AS `pes_codigo`,
                                 `mny_movimento`.`mov_obs` AS `historico`,
                                 `mny_movimento`.`con_codigo` AS `conta_codigo`,
                                 `con`.`ccr_codigo` AS `conta_codigo_conc`,
                                 `mny_plano_contas`.`descricao` AS `unidade_descricao`,
                                 `sys_banco`.`bco_nome` AS `bco_nome`,
                                 `cc`.`ccr_conta` AS `conta_corrente_conc`,
                                 `cc`.`ccr_agencia` AS `agencia_conta_conc`,
                                 `mny_conta_corrente`.`ccr_agencia` AS `agencia`,
                                 `mny_conta_corrente`.`ccr_conta` AS `conta`";
            $sComplemento = "JOIN `mny_movimento_item` ON `mny_movimento_item`.`mov_codigo` = " . $nMovCodigo . " AND mny_movimento_item.mov_item = " . $nMovItem . " AND `mny_movimento_item`.`ativo` = 1
                             JOIN `mny_conta_corrente` `con` ON `con`.`ccr_codigo` = " . $nCcrCodigo . " AND `con`.`ativo` = 1
                             JOIN `mny_conta_corrente` ON `mny_conta_corrente`.`ccr_codigo` = " . $nCcrCodigo . " AND `mny_conta_corrente`.`ativo` = 1
                             JOIN `v_pessoa_geral_formatada` ON `v_pessoa_geral_formatada`.`pesg_codigo` = `mny_movimento`.`pes_codigo`
                             JOIN `mny_plano_contas` ON `mny_plano_contas`.`plano_contas_codigo` = " . $nCodUnidade . "
                             JOIN `sys_banco` ON `sys_banco`.`bco_codigo` = (SELECT `mny_conta_corrente`.`ban_codigo` FROM `mny_conta_corrente` WHERE `mny_conta_corrente`.`ccr_codigo` = " . $nCcrCodigo . ")
                             JOIN `mny_conta_corrente` `cc` ON `cc`.`ccr_codigo` = " . $nCcrCodigo . "  AND `cc`.`ativo` = 1
                        WHERE
                          `mny_movimento_item`.`ativo` = 1  AND mny_movimento.mov_codigo = " . $nMovCodigo ;
    // echo "SELECT $sCampos FROM $sTabelas $sComplemento";
    // die();
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }
        */





       /**
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
        public function recuperarTodosMnyConciliacaoDataConsolidado(){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "DISTINCT data_conciliacao";
            $sComplemento = "-- WHERE consolidado = 1";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

           /**
         *
         * Método para recuperar um vetor de objetos MnyConciliacao da base de dados
         *
         * @return MnyConciliacao[]
         */
        public function recuperarTodosMnyConciliacaoConsolidadoPorData($dDataConciliacao){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = "WHERE data_conciliacao ='" . $dDataConciliacao . ".' AND consolidado = 1 AND ativo = 1";
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            //echo "SELECT $sCampos FROM $sTabelas WHERE $sComplemento";
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }


         public function recuperarUmMnyConciliacaoPorMovimentoItem($nMovCodigo , $nMovItem){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "mny_conciliacao";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = " .  $nMovCodigo ." AND mov_item = " .$nMovItem . " AND ativo = 1";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die() ;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao[0];
            return false;
        }

        public function recuperarUmMnyConsiliacaoPorMnyMovimentoItem( $nMovItem, $nMovCodigo){
            //$nParamentros = explode('||' , $nMovCodigo);
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = " mny_conciliacao";
            $sCampos = "mny_conciliacao.ativo, mny_conciliacao.mov_codigo AS mov_codigo, mny_conciliacao.mov_item  AS mov_item, mny_movimento_item.mov_valor AS mov_valor,  mny_conciliacao.valor_pago as mov_valor_pago";
            $sComplemento = "   JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo
                                AND mny_movimento_item.mov_item = mny_conciliacao.mov_item AND mny_conciliacao.ativo = 1
                                and mny_conciliacao.mov_item =" . $nMovItem ." and mny_conciliacao.mov_codigo=" . $nMovCodigo ;
            //"SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }
        //CONCILIAÇÃO CONTA/ UNIDADE
        public function recuperarUmMnyConciliacaoMaxDataPorContaUnidade( $nCodConta, $nCodUnidade){
            //$nParamentros = explode('||' , $nMovCodigo);
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = "max(data_conciliacao) as data_conciliacao";
            $sComplemento = " WHERE cod_conta_corrente =" . $nCodConta . " AND cod_unidade = " . $nCodUnidade . "  AND  consolidado = 0";
          //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarUmMnyConciliacaoMaxDataPorConta( $nCodConta){
            //$nParamentros = explode('||' , $nMovCodigo);
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = "max(data_conciliacao) as data_conciliacao";
            $sComplemento = " WHERE cod_conta_corrente =" . $nCodConta . "  AND  consolidado = 0";
          //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

        public function recuperarTodosMnyConciliacaoPorDataUnidadeTipoCaixaConsolidado($dDataConciliacao, $nCodUnidade, $nTipoCaixa, $nCcrCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = "*";
            $sComplemento = "WHERE ativo = 1  AND  consolidado = 0 AND data_conciliacao='" . $dDataConciliacao . "' AND cod_unidade=" .$nCodUnidade . " AND tipo_caixa= " . $nTipoCaixa . " AND cod_conta_corrente= " . $nCcrCodigo;
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        public function recuperarTodosMnyConciliacaoPorDataUnidadeConta($dDataConciliacao, $nCodUnidade, $nCcrCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = "*";
            $sComplemento = "WHERE ativo = 1  AND  consolidado = 0 AND data_conciliacao='" . $dDataConciliacao . "' AND cod_unidade=" .$nCodUnidade . "  AND cod_conta_corrente= " . $nCcrCodigo . " Order By ordem ASC";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        public function recuperarTodosMnyConciliacaoPorCodUnidadeTipoCaixaContaCorrenteConsolidado($nDataConciliacao, $nCcrCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = " DISTINCT cod_unidade, tipo_caixa, cod_conta_corrente ";
            $sComplemento = "WHERE ativo = 1  AND  consolidado = 0 AND data_conciliacao='" . $nDataConciliacao . "' AND  cod_conta_corrente = " . $nCcrCodigo;
           // echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        public function recuperarTodosMnyConciliacaoPorMnyMovimento($nMovCodigo){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = " mny_conciliacao";
            $sCampos = "";
            $sComplemento = "WHERE ativo = 1  AND  consolidado = 1 AND mov_codigo='" . $nMovCodigo . "'";
    //	    echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyConciliacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        public function recuperarTodosMnyConciliacaoProcedure($dData ,$nConta ){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "sp_conciliacao_magrugada";
            $sCampos = "";
            $sComplemento = "'". $dData ."',". $nConta ;
            //echo "CALL $sTabelas($sComplemento)" ;
            //die();
            $voMnyConciliacao = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
    //  		print_r($voMnyConciliacao);die('aqui');
            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

        public function recuperarTodosReconciliacaoPorUnidadePeriodoProcedure( $nCodConta, $dDataInicial, $sdDataFinal){
            $oMnyConciliacao = new MnyConciliacao();
            $oPersistencia = new Persistencia($oMnyConciliacao);
            $sTabelas = "sp_reconciliacao";
            $sComplemento =$nCodConta . ",'". $dDataInicial ."', '". $sdDataFinal ."'" ;
            $voMnyConciliacao = $oPersistencia->consultarProcedureLista($sTabelas,$sComplemento);

            if($voMnyConciliacao)
                return $voMnyConciliacao;
            return false;
        }

    /**
         *
         * Método para inicializar um Objeto MnyContaCaixa
         *
         * @return MnyContaCaixa
         */
        public function inicializarMnyContaCaixa($nCodContaCaixa,$nCodUnidade,$dDataConta,$sCreditoDebito,$nValor,$sDescricao,$sDocPendente,$nCodArquivo,$sInseridoPor,$sAlteradoPor,$nAtivo){
            $oMnyContaCaixa = new MnyContaCaixa();

            $oMnyContaCaixa->setCodContaCaixa($nCodContaCaixa);
            $oMnyContaCaixa->setCodUnidade($nCodUnidade);
            $oMnyContaCaixa->setDataContaBanco($dDataConta);
            $oMnyContaCaixa->setCreditoDebito($sCreditoDebito);
            $oMnyContaCaixa->setValorBanco($nValor);
            $oMnyContaCaixa->setDescricao($sDescricao);
            $oMnyContaCaixa->setDocPendente($sDocPendente);
            $oMnyContaCaixa->setCodArquivo($nCodArquivo);
            $oMnyContaCaixa->setInseridoPor($sInseridoPor);
            $oMnyContaCaixa->setAlteradoPor($sAlteradoPor);
            $oMnyContaCaixa->setAtivo($nAtivo);
            return $oMnyContaCaixa;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContaCaixa
         *
         * @return boolean
         */
        public function inserirMnyContaCaixa($oMnyContaCaixa){
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContaCaixa
         *
         * @return boolean
         */
        public function alterarMnyContaCaixa($oMnyContaCaixa){
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContaCaixa
         *
         * @return boolean
         */
        public function excluirMnyContaCaixa($nCodContaCaixa){
            $oMnyContaCaixa = new MnyContaCaixa();

            $oMnyContaCaixa->setCodContaCaixa($nCodContaCaixa);
            $oPersistencia = new Persistencia($oMnyContaCaixa);
                //$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();
                $bExcluir = $oPersistencia->excluir();
            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContaCaixa na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContaCaixa($nCodContaCaixa){
            $oMnyContaCaixa = new MnyContaCaixa();

            $oMnyContaCaixa->setCodContaCaixa($nCodContaCaixa);
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContaCaixa da base de dados
         *
         * @return MnyContaCaixa
         */
        public function recuperarUmMnyContaCaixa($nCodContaCaixa){
            $oMnyContaCaixa = new MnyContaCaixa();
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            $sTabelas = "mny_conta_caixa";
            $sCampos = "*";
            $sComplemento = " WHERE cod_conta_caixa = $nCodContaCaixa";
            $voMnyContaCaixa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCaixa)
                return $voMnyContaCaixa[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCaixa da base de dados
         *
         * @return MnyContaCaixa[]
         */
        public function recuperarTodosMnyContaCaixa(){
            $oMnyContaCaixa = new MnyContaCaixa();
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            $sTabelas = "mny_conta_caixa";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyContaCaixa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCaixa)
                return $voMnyContaCaixa;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCaixa da base de dados
         *
         * @return MnyContaCaixa[]
         */
        public function recuperarTodosMnyContaCaixaPorUnidadeData($nCodUnidade,$dData){
            $oMnyContaCaixa = new MnyContaCaixa();
            $oPersistencia = new Persistencia($oMnyContaCaixa);
            $sTabelas = "mny_conta_caixa";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=$nCodUnidade AND data_conta='".$dData."' AND ativo=1";

            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            $voMnyContaCaixa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCaixa)
                return $voMnyContaCaixa;
            return false;
        }

       /**
         *
         * Método para inicializar um Objeto MnyContaCorrente
         *
         * @return MnyContaCorrente
         */
        public function inicializarMnyContaCorrente($nCcrCodigo,$sCcrAgencia,$sCcrAgenciaDv,$sCcrConta,$sCcrContaDv,$nCcrTipo,$nBanCodigo,$sCcrInc,$sCcrAlt,$nClassificacao,$dDataEncerramento,$nAtivo,$nEmpCodigo){
            $oMnyContaCorrente = new MnyContaCorrente();

            $oMnyContaCorrente->setCcrCodigo($nCcrCodigo);
            $oMnyContaCorrente->setCcrAgencia($sCcrAgencia);
            $oMnyContaCorrente->setCcrAgenciaDv($sCcrAgenciaDv);
            $oMnyContaCorrente->setCcrConta($sCcrConta);
            $oMnyContaCorrente->setCcrContaDv($sCcrContaDv);
            $oMnyContaCorrente->setCcrTipo($nCcrTipo);
            $oMnyContaCorrente->setBanCodigo($nBanCodigo);
            $oMnyContaCorrente->setCcrInc($sCcrInc);
            $oMnyContaCorrente->setCcrAlt($sCcrAlt);
            $oMnyContaCorrente->setClassificacao($nClassificacao);
            $oMnyContaCorrente->setDataEncerramento($dDataEncerramento);
            $oMnyContaCorrente->setAtivo($nAtivo);
            $oMnyContaCorrente->setEmpCodigo($nEmpCodigo);
            return $oMnyContaCorrente;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContaCorrente
         *
         * @return boolean
         */
        public function inserirMnyContaCorrente($oMnyContaCorrente){
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContaCorrente
         *
         * @return boolean
         */
        public function alterarMnyContaCorrente($oMnyContaCorrente){
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContaCorrente
         *
         * @return boolean
         */
        public function excluirMnyContaCorrente($nCcrCodigo){
            $oMnyContaCorrente = new MnyContaCorrente();

            $oMnyContaCorrente->setCcrCodigo($nCcrCodigo);
            $oPersistencia = new Persistencia($oMnyContaCorrente);
                $bExcluir =  $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContaCorrente na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContaCorrente($nCcrCodigo){
            $oMnyContaCorrente = new MnyContaCorrente();

            $oMnyContaCorrente->setCcrCodigo($nCcrCodigo);
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContaCorrente da base de dados
         *
         * @return MnyContaCorrente
         */
        public function recuperarUmMnyContaCorrente($nCcrCodigo){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "*";
            $sComplemento = " WHERE ccr_codigo = $nCcrCodigo";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCorrente da base de dados
         *
         * @return MnyContaCorrente[]
         */
        public function recuperarTodosMnyContaCorrente(){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "mny_conta_corrente.cod_unidade as cod_unidade,ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,pc.descricao as ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo";
            $sComplemento = " inner join sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                              Left Join mny_plano_contas pc ON pc.plano_contas_codigo = mny_conta_corrente.cod_unidade
                              WHERE mny_conta_corrente.ativo=1 ORDER BY bco_nome";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }


       public function recuperarTodosMnyContaCorrenteIndex(){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "mny_conta_corrente.ccr_codigo,mny_conta_corrente.ccr_agencia,mny_conta_corrente.ccr_agencia_dv,mny_conta_corrente.ccr_conta,mny_conta_corrente.ccr_conta_dv,mny_conta_corrente.ccr_tipo,mny_conta_corrente.ccr_inc,mny_conta_corrente.ccr_alt,mny_conta_corrente.classificacao,mny_conta_corrente.ativo,sys_banco.bco_nome as ban_codigo";
            $sComplemento = " INNER JOIN sys_banco ON mny_conta_corrente.ban_codigo = sys_banco.bco_codigo
                              where mny_conta_corrente.ativo=1
                            ";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }

        public function recuperarUmMnyContaCorrenteCaixinhaPorMovimento($nMovCodigo){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente c";
            $sCampos = "*";
            $sComplemento = "Inner Join mny_movimento m On m.mov_codigo=".$nMovCodigo." And c.cod_unidade=m.uni_codigo Where ccr_conta=999";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            if($voMnyContaCorrente)
                return $voMnyContaCorrente[0];
            return false;
        }

        public function recuperarTodosMnyContaCorrenteCaixinha(){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "*";
            $sComplemento = "Where ccr_conta=999";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCorrente da base de dados
         *
         * @return MnyContaCorrente[]
         */
        public function recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "mny_conta_corrente.cod_unidade as cod_unidade,ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo";
            $sComplemento = " inner join sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                              inner join mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_conta_corrente.cod_unidade
                              WHERE mny_conta_corrente.ativo=1 AND cod_unidade=".$nCodUnidade."
                              Union
                              Select cc.cod_unidade as cod_unidade,mny_conta_corrente.ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo
                              From mny_conta_corrente
                              Inner Join mny_conta_corrente_unidade cc On mny_conta_corrente.ccr_codigo = cc.ccr_codigo
                              INNER JOIN sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                              INNER JOIN mny_plano_contas ON mny_plano_contas.plano_contas_codigo = cc.cod_unidade
                              WHERE mny_conta_corrente.ativo = 1 And cc.cod_unidade=".$nCodUnidade."
                              ORDER BY ban_codigo";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }
        public function recuperarTodosMnyContaCorrentePorUnidadeNOVO($nCodUnidade){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "mny_conta_corrente.cod_unidade as cod_unidade,ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo";
            $sComplemento = "	Inner Join mny_conta_corrente_unidade cc On mny_conta_corrente.ccr_codigo = cc.ccr_codigo
                                INNER JOIN sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                                INNER JOIN mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_conta_corrente.cod_unidade

                            WHERE
                                mny_conta_corrente.ativo = 1 And cc.cod_unidade=".$nCodUnidade."
                            ORDER BY
                                bco_nome";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }

        public function recuperarTodosMnyContaCorrentePorUnidadeCaixinha($nCodUnidade){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "mny_conta_corrente.cod_unidade as cod_unidade,ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo";
            $sComplemento = " inner join sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                              inner join mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_conta_corrente.cod_unidade
                              WHERE mny_conta_corrente.ccr_conta='999' And mny_conta_corrente.ativo=1 AND cod_unidade=".$nCodUnidade."
                              Union
                              Select cc.cod_unidade as cod_unidade,mny_conta_corrente.ccr_codigo,ccr_agencia,ccr_agencia_dv,ccr_conta,ccr_conta_dv,ccr_tipo,bco_nome as ban_codigo,ccr_inc,ccr_alt,classificacao,mny_conta_corrente.ativo
                              From mny_conta_corrente
                              Inner Join mny_conta_corrente_unidade cc On mny_conta_corrente.ccr_codigo = cc.ccr_codigo
                              INNER JOIN sys_banco ON sys_banco.bco_codigo = mny_conta_corrente.ban_codigo
                              INNER JOIN mny_plano_contas ON mny_plano_contas.plano_contas_codigo = cc.cod_unidade
                              WHERE mny_conta_corrente.ccr_conta='999' And mny_conta_corrente.ativo = 1 And cc.cod_unidade=".$nCodUnidade."
                              ORDER BY ban_codigo";;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }




     /**
         *
         * Método para inicializar um Objeto MnyContaCorrenteUnidade
         *
         * @return MnyContaCorrenteUnidade
         */
        public function inicializarMnyContaCorrenteUnidade($nCcrCodigo,$nCodUnidade){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();

            $oMnyContaCorrenteUnidade->setCcrCodigo($nCcrCodigo);
            $oMnyContaCorrenteUnidade->setCodUnidade($nCodUnidade);
            return $oMnyContaCorrenteUnidade;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContaCorrenteUnidade
         *
         * @return boolean
         */
        public function inserirMnyContaCorrenteUnidade($oMnyContaCorrenteUnidade){
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContaCorrenteUnidade
         *
         * @return boolean
         */
        public function alterarMnyContaCorrenteUnidade($oMnyContaCorrenteUnidade){
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContaCorrente
         *
         * @return boolean
         */
        public function excluirMnyContaCorrenteUnidade($nCcrCodigo,$nCodUnidade){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();

            $oMnyContaCorrenteUnidade->setCcrCodigo($nCcrCodigo);
            $oMnyContaCorrenteUnidade->setCodUnidade($nCodUnidade);
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
                $bExcluir =  $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }

    /**
         *
         * Método para excluir da base de dados um Objeto MnyContaCorrente
         *
         * @return boolean
         */
        public function excluirMnyContaCorrenteUnidadePorConta($nCcrCodigo){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();
            $sComplemento = " WHERE ccr_codigo=".$nCcrCodigo;
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;
        }
        /**
         *
         * Método para verificar se existe um Objeto MnyContaCorrente na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContaCorrenteUnidade($nCcrCodigo,$nCodUnidade){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();

            $oMnyContaCorrenteUnidade->setCcrCodigo($nCcrCodigo);
            $oMnyContaCorrenteUnidade->setCodUnidade($nCodUnidade);
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContaCorrenteUnidade da base de dados
         *
         * @return MnyContaCorrenteUnidade
         */
        public function recuperarUmMnyContaCorrenteUnidade($nCcrCodigo,$nCodUnidade){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            $sTabelas = "mny_conta_corrente";
            $sCampos = "*";
            $sComplemento = " WHERE cod_unidade = ".$nCodUnidade . " AND ccr_codigo =". $nCcrCodigo;
            $voMnyContaCorrenteUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrenteUnidade)
                return $voMnyContaCorrenteUnidade[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCorrenteUnidade da base de dados
         *
         * @return MnyContaCorrenteUnidade[]
         */
        public function recuperarTodosMnyContaCorrenteUnidadePorUnidade($nCodUnidade){
            $oMnyContaCorrente = new MnyContaCorrente();
            $oPersistencia = new Persistencia($oMnyContaCorrente);
            $sTabelas = "mny_conta_corrente_unidade";
            $sCampos = "cod_unidade,ccr_codigo";
            $sComplemento = " inner join mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_conta_corrente_unidade.cod_unidade
                              WHERE cod_unidade=".$nCodUnidade." ORDER BY mny_plano_contas.descricao ASC ";
            $voMnyContaCorrente = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrente)
                return $voMnyContaCorrente;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContaCorrenteUnidade da base de dados
         *
         * @return MnyContaCorrenteUnidade[]
         */
        public function recuperarTodosMnyMnyContaCorrenteUnidadePorConta($nCcrCodigo){
            $oMnyContaCorrenteUnidade = new MnyContaCorrenteUnidade();
            $oPersistencia = new Persistencia($oMnyContaCorrenteUnidade);
            $sTabelas = "mny_conta_corrente_unidade";
            $sCampos = "*";
            $sComplemento = " inner join mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_conta_corrente_unidade.cod_unidade
                              WHERE ccr_codigo=".$nCcrCodigo." ORDER BY mny_plano_contas.descricao ASC ";
            $voMnyContaCorrenteUnidade = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContaCorrenteUnidade)
                return $voMnyContaCorrenteUnidade;
            return false;
        }
        /**
         *
         * Método para inicializar um Objeto MnyContratoAditivo
         *
         * @return MnyContratoAditivo
         */
        public function inicializarMnyContratoAditivo($nAditivoCodigo,$nContratoCodigo,$nCodStatus,$sNumero,$sMotivoAditivo,$dDataAditivo,$dDataValidade,$nValorAditivo,$sAnexo,$nAtivo){
            $oMnyContratoAditivo = new MnyContratoAditivo();

            $oMnyContratoAditivo->setAditivoCodigo($nAditivoCodigo);
            $oMnyContratoAditivo->setContratoCodigo($nContratoCodigo);
            $oMnyContratoAditivo->setCodStatus($nCodStatus);
            $oMnyContratoAditivo->setNumero($sNumero);
            $oMnyContratoAditivo->setMotivoAditivo($sMotivoAditivo);
            $oMnyContratoAditivo->setDataAditivoBanco($dDataAditivo);
            $oMnyContratoAditivo->setDataValidadeBanco($dDataValidade);
            $oMnyContratoAditivo->setValorAditivoBanco($nValorAditivo);
            $oMnyContratoAditivo->setAnexo($sAnexo);
            $oMnyContratoAditivo->setAtivo($nAtivo);
            return $oMnyContratoAditivo;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoAditivo
         *
         * @return boolean
         */
        public function inserirMnyContratoAditivo($oMnyContratoAditivo){
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoAditivo
         *
         * @return boolean
         */
        public function alterarMnyContratoAditivo($oMnyContratoAditivo){
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoAditivo
         *
         * @return boolean
         */
        public function excluirMnyContratoAditivo($nAditivoCodigo){
            $oMnyContratoAditivo = new MnyContratoAditivo();

            $oMnyContratoAditivo->setAditivoCodigo($nAditivoCodigo);
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
                $bExcluir = $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContratoAditivo na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoAditivo($nAditivoCodigo){
            $oMnyContratoAditivo = new MnyContratoAditivo();

            $oMnyContratoAditivo->setAditivoCodigo($nAditivoCodigo);
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoAditivo da base de dados
         *
         * @return MnyContratoAditivo
         */
        public function recuperarUmMnyContratoAditivo($nAditivoCodigo){
            $oMnyContratoAditivo = new MnyContratoAditivo();
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            $sTabelas = "mny_contrato_aditivo";
            $sCampos = "*";
            $sComplemento = " WHERE aditivo_codigo = $nAditivoCodigo";
            $voMnyContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAditivo)
                return $voMnyContratoAditivo[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoAditivo da base de dados
         *
         * @return MnyContratoAditivo[]
         */
        public function recuperarTodosMnyContratoAditivo(){
            $oMnyContratoAditivo = new MnyContratoAditivo();
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            $sTabelas = "mny_contrato_aditivo";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAditivo)
                return $voMnyContratoAditivo;
            return false;
        }

        public function recuperarTodosMnyContratoAditivoPorContrato($nContratoCodigo){
            $oMnyContratoAditivo = new MnyContratoAditivo();
            $oPersistencia = new Persistencia($oMnyContratoAditivo);
            $sTabelas = "mny_contrato_aditivo";
            $sCampos = "*";
            $sComplemento = "where ativo =1 and  contrato_codigo=".$nContratoCodigo;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnyContratoAditivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAditivo)
                return $voMnyContratoAditivo;
            return false;
        }


    /**
         *
         * Método para inicializar um Objeto MnyContratoAse
         *
         * @return MnyContratoAse
         */
        public function inicializarMnyContratoAse($nContratoCodigo,$nItem,$sDescricao,$nValor,$nAtivo){
            $oMnyContratoAse = new MnyContratoAse();

            $oMnyContratoAse->setContratoCodigo($nContratoCodigo);
            $oMnyContratoAse->setItem($nItem);
            $oMnyContratoAse->setDescricao($sDescricao);
            $oMnyContratoAse->setValorBanco($nValor);
            $oMnyContratoAse->setAtivo($nAtivo);
            return $oMnyContratoAse;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoAse
         *
         * @return boolean
         */
        public function inserirMnyContratoAse($oMnyContratoAse){
            $oPersistencia = new Persistencia($oMnyContratoAse);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoAse
         *
         * @return boolean
         */
        public function alterarMnyContratoAse($oMnyContratoAse){
            $oPersistencia = new Persistencia($oMnyContratoAse);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoAse
         *
         * @return boolean
         */
        public function excluirMnyContratoAse($nContratoCodigo,$nItem){
            $oMnyContratoAse = new MnyContratoAse();

            $oMnyContratoAse->setContratoCodigo($nContratoCodigo);
            $oMnyContratoAse->setItem($nItem);
            $oPersistencia = new Persistencia($oMnyContratoAse);
                $bExcluir = $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoAse
         *
         * @return boolean
         */
        public function excluirMnyContratoAseFisicamentePorContrato($nContratoCodigo){
            $oMnyContratoAse = new MnyContratoAse();
            $oPersistencia = new Persistencia($oMnyContratoAse);
            $sComplemento = "WHERE contrato_codigo = ". $nContratoCodigo;

                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContratoAse na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoAse($nContratoCodigo){
            $oMnyContratoAse = new MnyContratoAse();

            $oMnyContratoAse->setContratoCodigo($nContratoCodigo);
            $oMnyContratoAse->setItem($nItem);
            $oPersistencia = new Persistencia($oMnyContratoAse);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoAse da base de dados
         *
         * @return MnyContratoAse
         */
        public function recuperarUmMnyContratoAse($nContratoCodigo,$nItem){
            $oMnyContratoAse = new MnyContratoAse();
            $oPersistencia = new Persistencia($oMnyContratoAse);
            $sTabelas = "mny_contrato_ase";
            $sCampos = "*";
            $sComplemento = " WHERE contrato_codigo = $nContratoCodigo AND item=$nItem";
            $voMnyContratoAse = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAse)
                return $voMnyContratoAse[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoAse da base de dados
         *
         * @return MnyContratoAse[]
         */
        public function recuperarTodosMnyContratoAse(){
            $oMnyContratoAse = new MnyContratoAse();
            $oPersistencia = new Persistencia($oMnyContratoAse);
            $sTabelas = "mny_contrato_ase";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyContratoAse = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAse)
                return $voMnyContratoAse;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoAse da base de dados
         *
         * @return MnyContratoAse[]
         */
        public function recuperarTodosMnyContratoAsePorContrato($nContratoCodigo){
            $oMnyContratoAse = new MnyContratoAse();
            $oPersistencia = new Persistencia($oMnyContratoAse);
            $sTabelas = "mny_contrato_ase";
            $sCampos = "*";
            $sComplemento = "WHERE contrato_codigo=" . $nContratoCodigo;
            $voMnyContratoAse = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoAse)
                return $voMnyContratoAse;
            return false;
        }



        /**
         *
         * Método para inicializar um Objeto MnyContratoPessoa
         *
         * @return MnyContratoPessoa
         */
        public function inicializarMnyContratoPessoa($nContratoCodigo,$nTipoContrato,$nPesCodigo,$nCodStatus,$sNumero,$sDescricao,$dDataInicio,$dDataContrato,$dDataValidade,$nValorContrato,$nCusCodigo,$nNegCodigo,$nCenCodigo,$nUniCodigo,$nConCodigo,$nSetCodigo,$nEmpCodigo,$nAtivo){
            $oMnyContratoPessoa = new MnyContratoPessoa();

            $oMnyContratoPessoa->setContratoCodigo($nContratoCodigo);
            $oMnyContratoPessoa->setTipoContrato($nTipoContrato);
            $oMnyContratoPessoa->setPesCodigo($nPesCodigo);
            $oMnyContratoPessoa->setCodStatus($nCodStatus);
            $oMnyContratoPessoa->setNumero($sNumero);
            $oMnyContratoPessoa->setDescricao($sDescricao);
            if($dDataInicio)
                $oMnyContratoPessoa->setDataInicioBanco($dDataInicio);
            else
                $oMnyContratoPessoa->setDataInicio(NULL);

            $oMnyContratoPessoa->setDataContratoBanco($dDataContrato);
            $oMnyContratoPessoa->setDataValidadeBanco($dDataValidade);
            $oMnyContratoPessoa->setValorContratoBanco($nValorContrato);
            $oMnyContratoPessoa->setCusCodigo($nCusCodigo);
            $oMnyContratoPessoa->setNegCodigo($nNegCodigo);
            $oMnyContratoPessoa->setCenCodigo($nCenCodigo);
            $oMnyContratoPessoa->setUniCodigo($nUniCodigo);
            $oMnyContratoPessoa->setConCodigo($nConCodigo);
            $oMnyContratoPessoa->setSetCodigo($nSetCodigo);
            $oMnyContratoPessoa->setEmpCodigo($nEmpCodigo);
            $oMnyContratoPessoa->setAtivo($nAtivo);
            return $oMnyContratoPessoa;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoPessoa
         *
         * @return boolean
         */
        public function inserirMnyContratoPessoa($oMnyContratoPessoa){
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            if($nId = $oPersistencia->inserir())
                return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoPessoa
         *
         * @return boolean
         */
        public function alterarMnyContratoPessoa($oMnyContratoPessoa){
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoPessoa
         *
         * @return boolean
         */
        public function excluirMnyContratoPessoa($nContratoCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oMnyContratoPessoa->setContratoCodigo($nContratoCodigo);
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
                $bExcluir =  $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

    /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoPessoa
         *
         * @return boolean
         */
        public function excluirMnyContratoPessoaFisicamente($nContratoCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();

            $oMnyContratoPessoa->setContratoCodigo($nContratoCodigo);
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
                $bExcluir = $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContratoPessoa na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoPessoa($nContratoCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();

            $oMnyContratoPessoa->setContratoCodigo($nContratoCodigo);
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoPessoa da base de dados
         *
         * @return MnyContratoPessoa
         */
        public function recuperarUmMnyContratoPessoa($nContratoCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "*";
            $sComplemento = " WHERE contrato_codigo =". $nContratoCodigo;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa[0];
            return false;
        }


        public function recuperarUmMnyContratoPessoaPorPessoa($nCodPessoa){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "*";
            $sComplemento = " WHERE pes_codigo =" .  $nCodPessoa;
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa[0];
            return false;
        }


        public function recuperarUmMnyContratoPessoaPorMovimento($nMovCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "*";
            $sComplemento = " WHERE contrato_codigo = (SELECT mov_contrato from mny_movimento where mov_codigo=".$nMovCodigo.")"  ;
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoPessoa da base de dados
         *
         * @return MnyContratoPessoa[]
         */
        public function recuperarTodosMnyContratoPessoa(){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoPessoa da base de dados
         *
         * @return MnyContratoPessoa[]
         */
        public function recuperarValorGlobalMnyContratoPessoaPorContrato($nCodContrato,$nMovCodigo){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "(SELECT valor_contrato FROM mny_contrato_pessoa WHERE contrato_codigo = ".$nCodContrato." )
                        +(SELECT IFNULL(sum(valor_aditivo),0) FROM mny_contrato_aditivo WHERE contrato_codigo = ".$nCodContrato.")
                        -(SELECT IFNULL(sum(mov_valor),0) FROM mny_movimento_item WHERE mov_codigo = ".$nMovCodigo.")  as valor_contrato";
            $sComplemento = "";
            //echo "SELECT $sCampos FROM $sTabelas";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa[0]->getValorContrato();
            return false;
        }


        public function recuperarTodosMnyContratoPessoaPorPessoa($nIdPessoa){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "contrato_codigo, tipo_contrato, pes_codigo, cod_status, numero, mny_contrato_pessoa.descricao as descricao, data_inicio, data_contrato, data_validade, valor_contrato, setor.descricao as set_codigo, unidade.descricao as uni_codigo, cus_codigo, neg_codigo, cen_codigo, con_codigo, emp_codigo, mny_contrato_pessoa.ativo as ativo";
            $sComplemento = " inner join mny_plano_contas setor ON setor.plano_contas_codigo = mny_contrato_pessoa.set_codigo
                              inner join mny_plano_contas unidade ON unidade.plano_contas_codigo = mny_contrato_pessoa.uni_codigo
                                WHERE mny_contrato_pessoa.ativo=1 AND pes_codigo=$nIdPessoa ";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa;
            return false;
        }
        public function recuperarTodosMnyContratoPessoaPorPessoaPorTipoContrato($nIdPessoa,$nTipoContrato){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "*";

            if(in_array($_SESSION['Perfil']['CodUnidade'],$_SESSION['UnidadeCentral'])){
                $sComplemento = "WHERE ativo=1 AND pes_codigo=".$nIdPessoa. " AND tipo_contrato=".$nTipoContrato ." AND emp_codigo=" . $_SESSION['oEmpresa']->getEmpCodigo();
            }else{
                $sComplemento = "WHERE ativo=1 AND pes_codigo=".$nIdPessoa. " AND tipo_contrato=".$nTipoContrato ." AND emp_codigo=" . $_SESSION['oEmpresa']->getEmpCodigo() . " And uni_codigo=".$_SESSION['Perfil']['CodUnidade'];
            }

            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa;
            return false;
        }



            public function recuperarTodosMnyContratoPessoaIndex(){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "mny_contrato_pessoa.contrato_codigo,mny_contrato_tipo.descricao as tipo_contrato,mny_contrato_pessoa.pes_codigo as pes_codigo,numero,mny_contrato_pessoa.descricao as descricao,data_contrato,data_validade,valor_contrato,setor.descricao as set_codigo,emp_codigo,mny_contrato_pessoa.ativo,v_pessoa_geral_formatada.nome as cod_status,neg_codigo";

            if(in_array($_SESSION['Perfil']['CodGrupoUsuario'], array(18,6))){
            $sComplemento = "INNER JOIN mny_plano_contas  setor  ON mny_contrato_pessoa.set_codigo = setor.plano_contas_codigo
                             INNER JOIN v_pessoa_geral_formatada ON v_pessoa_geral_formatada.pesg_codigo = mny_contrato_pessoa.pes_codigo
                             INNER JOIN mny_contrato_tipo ON mny_contrato_pessoa.tipo_contrato = mny_contrato_tipo.cod_contrato_tipo
                             WHERE mny_contrato_pessoa.ativo=1 AND mny_contrato_pessoa.emp_codigo = " . $_SESSION['oEmpresa']->getEmpCodigo(). " And mny_contrato_pessoa.uni_codigo=".$_SESSION['Perfil']['CodUnidade'];;

            }else{
            $sComplemento = "INNER JOIN mny_plano_contas  setor  ON mny_contrato_pessoa.set_codigo = setor.plano_contas_codigo
                             INNER JOIN v_pessoa_geral_formatada ON v_pessoa_geral_formatada.pesg_codigo = mny_contrato_pessoa.pes_codigo
                             INNER JOIN mny_contrato_tipo ON mny_contrato_pessoa.tipo_contrato = mny_contrato_tipo.cod_contrato_tipo
                             WHERE mny_contrato_pessoa.ativo=1 AND mny_contrato_pessoa.emp_codigo = " . $_SESSION['oEmpresa']->getEmpCodigo();
            }
      //	echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa;
            return false;
        }

            public function recuperarTodosMnyContratoPessoaIndexPorTipo($nCodTipoContrato){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "mny_contrato_pessoa.contrato_codigo,mny_contrato_tipo.descricao as tipo_contrato,mny_contrato_pessoa.pes_codigo as pes_codigo,numero,mny_contrato_pessoa.descricao as descricao,data_contrato,data_validade,valor_contrato,setor.descricao as set_codigo,emp_codigo,mny_contrato_pessoa.ativo,v_pessoa_geral_formatada.identificacao as data_inicio,v_pessoa_geral_formatada.nome as cod_status,neg_codigo";

            if(in_array($_SESSION['Perfil']['CodGrupoUsuario'], array(18,6))){
                $sComplemento = "INNER JOIN mny_plano_contas  setor  ON mny_contrato_pessoa.set_codigo = setor.plano_contas_codigo
                                 INNER JOIN v_pessoa_geral_formatada ON v_pessoa_geral_formatada.pesg_codigo = mny_contrato_pessoa.pes_codigo
                                 INNER JOIN mny_contrato_tipo ON mny_contrato_pessoa.tipo_contrato = mny_contrato_tipo.cod_contrato_tipo
                                 WHERE mny_contrato_pessoa.ativo=1 AND mny_contrato_pessoa.tipo_contrato=".$nCodTipoContrato." AND mny_contrato_pessoa.emp_codigo=" . $_SESSION['oEmpresa']->getEmpCodigo() . " And mny_contrato_pessoa.uni_codigo=".$_SESSION['Perfil']['CodUnidade'];
            }else{
                $sComplemento = "INNER JOIN mny_plano_contas  setor  ON mny_contrato_pessoa.set_codigo = setor.plano_contas_codigo
                                 INNER JOIN v_pessoa_geral_formatada ON v_pessoa_geral_formatada.pesg_codigo = mny_contrato_pessoa.pes_codigo
                                 INNER JOIN mny_contrato_tipo ON mny_contrato_pessoa.tipo_contrato = mny_contrato_tipo.cod_contrato_tipo
                                 WHERE mny_contrato_pessoa.ativo=1 AND mny_contrato_pessoa.tipo_contrato=".$nCodTipoContrato." AND mny_contrato_pessoa.emp_codigo=" . $_SESSION['oEmpresa']->getEmpCodigo();
            }
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoPessoa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa;
            return false;
        }

     /**
         *
         * Método para inicializar um Objeto MnyContratoTipo
         *
         * @return MnyContratoTipo
         */
        public function inicializarMnyContratoTipo($nCodContratoTipo,$sDescricao,$nAtivo){
            $oMnyContratoTipo = new MnyContratoTipo();

            $oMnyContratoTipo->setCodContratoTipo($nCodContratoTipo);
            $oMnyContratoTipo->setDescricao($sDescricao);
            $oMnyContratoTipo->setAtivo($nAtivo);
            return $oMnyContratoTipo;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoTipo
         *
         * @return boolean
         */
        public function inserirMnyContratoTipo($oMnyContratoTipo){
            $oPersistencia = new Persistencia($oMnyContratoTipo);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoTipo
         *
         * @return boolean
         */
        public function alterarMnyContratoTipo($oMnyContratoTipo){
            $oPersistencia = new Persistencia($oMnyContratoTipo);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoTipo
         *
         * @return boolean
         */
        public function excluirMnyContratoTipo($nCodContratoTipo){
            $oMnyContratoTipo = new MnyContratoTipo();

            $oMnyContratoTipo->setCodContratoTipo($nCodContratoTipo);
            $oPersistencia = new Persistencia($oMnyContratoTipo);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContratoTipo na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoTipo($nCodContratoTipo){
            $oMnyContratoTipo = new MnyContratoTipo();

            $oMnyContratoTipo->setCodCodContratoTipo($nCodContratoTipo);
            $oPersistencia = new Persistencia($oMnyContratoTipo);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoTipo da base de dados
         *
         * @return MnyContratoTipo
         */
        public function recuperarUmMnyContratoTipo($nCodContratoTipo){
            $oMnyContratoTipo = new MnyContratoTipo();
            $oPersistencia = new Persistencia($oMnyContratoTipo);
            $sTabelas = "mny_contrato_tipo";
            $sCampos = "*";
            $sComplemento = " WHERE cod_contrato_tipo = $nCodContratoTipo";
            //echo "select * from $sTabelas " . $sComplemento;
            //die();
            $voMnyContratoTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);

            if($voMnyContratoTipo)
                return $voMnyContratoTipo[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoTipo da base de dados
         *
         * @return MnyContratoTipo[]
         */
        public function recuperarTodosMnyContratoTipo(){
            $oMnyContratoTipo = new MnyContratoTipo();
            $oPersistencia = new Persistencia($oMnyContratoTipo);
            $sTabelas = "mny_contrato_tipo";
            $sCampos = "*";
            $sComplemento = "WHERE ativo=1 AND cod_contrato_tipo IN (1,2,3,4,10)";
            $voMnyContratoTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoTipo)
                return $voMnyContratoTipo;
            return false;
        }


        /**
         *
         * Método para inicializar um Objeto MnyFuncao
         *
         * @return MnyFuncao
         */
        public function inicializarMnyFuncao($nFunCodigo,$sFunNome,$sFunInc,$sFunAlt,$nAtivo){
            $oMnyFuncao = new MnyFuncao();

            $oMnyFuncao->setFunCodigo($nFunCodigo);
            $oMnyFuncao->setFunNome($sFunNome);
            $oMnyFuncao->setFunInc($sFunInc);
            $oMnyFuncao->setFunAlt($sFunAlt);
            $oMnyFuncao->setAtivo($nAtivo);
            return $oMnyFuncao;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyFuncao
         *
         * @return boolean
         */
        public function inserirMnyFuncao($oMnyFuncao){
            $oPersistencia = new Persistencia($oMnyFuncao);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyFuncao
         *
         * @return boolean
         */
        public function alterarMnyFuncao($oMnyFuncao){
            $oPersistencia = new Persistencia($oMnyFuncao);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyFuncao
         *
         * @return boolean
         */
        public function excluirMnyFuncao($nFunCodigo){
            $oMnyFuncao = new MnyFuncao();

            $oMnyFuncao->setFunCodigo($nFunCodigo);
            $oPersistencia = new Persistencia($oMnyFuncao);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyFuncao na base de dados
         *
         * @return boolean
         */
        public function presenteMnyFuncao($nFunCodigo){
            $oMnyFuncao = new MnyFuncao();

            $oMnyFuncao->setFunCodigo($nFunCodigo);
            $oPersistencia = new Persistencia($oMnyFuncao);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyFuncao da base de dados
         *
         * @return MnyFuncao
         */
        public function recuperarUmMnyFuncao($nFunCodigo){
            $oMnyFuncao = new MnyFuncao();
            $oPersistencia = new Persistencia($oMnyFuncao);
            $sTabelas = "mny_funcao";
            $sCampos = "*";
            $sComplemento = " WHERE fun_codigo = $nFunCodigo";
            $voMnyFuncao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyFuncao)
                return $voMnyFuncao[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyFuncao da base de dados
         *
         * @return MnyFuncao[]
         */
        public function recuperarTodosMnyFuncao(){
            $oMnyFuncao = new MnyFuncao();
            $oPersistencia = new Persistencia($oMnyFuncao);
            $sTabelas = "mny_funcao";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyFuncao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyFuncao)
                return $voMnyFuncao;
            return false;
        }



        /**
         *
         * Método para inicializar um Objeto MnyMovimento
         *
         * @return MnyMovimento
         */
        public function inicializarMnyMovimento($nMovCodigo,$dMovDataInclusao,$dMovDataEmissao,$nCusCodigo,$nNegCodigo,$nCenCodigo,$nUniCodigo,$nConCodigo,$nPesCodigo,$nSetCodigo,$sMovObs,$sMovInc,$sMovAlt,$nMovContrato,$sMovDocumento,$nMovParcelas,$nMovTipo,$nEmpCodigo,$nMovIcmsAliq,$nMovPis,$nMovConfins,$nMovCsll,$nMovIss,$nMovIr,$nMovIrrf,$nMovInss,$nMovOutros,$nMovDevolucao,$nAtivo,$nMovOutrosDesc){
            $oMnyMovimento = new MnyMovimento();
            $oMnyMovimento->setMovCodigo($nMovCodigo);
            $oMnyMovimento->setMovDataInclusaoBanco($dMovDataInclusao);
            $oMnyMovimento->setMovDataEmissaoBanco($dMovDataEmissao);
            $oMnyMovimento->setCusCodigo($nCusCodigo);
            $oMnyMovimento->setNegCodigo($nNegCodigo);
            $oMnyMovimento->setCenCodigo($nCenCodigo);
            $oMnyMovimento->setUniCodigo($nUniCodigo);
            $oMnyMovimento->setConCodigo($nConCodigo);
            $oMnyMovimento->setPesCodigo($nPesCodigo);
            $oMnyMovimento->setSetCodigo($nSetCodigo);
            $oMnyMovimento->setMovObs($sMovObs);
            $oMnyMovimento->setMovInc($sMovInc);
            $oMnyMovimento->setMovAlt($sMovAlt);
            $oMnyMovimento->setMovContrato($nMovContrato);
            $oMnyMovimento->setMovDocumento($sMovDocumento);
            $oMnyMovimento->setMovParcelas($nMovParcelas);
            $oMnyMovimento->setMovTipo($nMovTipo);
            $oMnyMovimento->setEmpCodigo($nEmpCodigo);
            $oMnyMovimento->setMovIcmsAliqBanco($nMovIcmsAliq);
            $oMnyMovimento->setMovPisBanco($nMovPis);
            $oMnyMovimento->setMovConfinsBanco($nMovConfins);
            $oMnyMovimento->setMovOutrosBanco($nMovOutros);
            $oMnyMovimento->setMovDevolucaoBanco($nMovDevolucao);
            $oMnyMovimento->setMovCsllBanco($nMovCsll);
            $oMnyMovimento->setMovIssBanco($nMovIss);
            $oMnyMovimento->setMovIrBanco($nMovIr);
            $oMnyMovimento->setMovIrrfBanco($nMovIrrf);
            $oMnyMovimento->setMovInssBanco($nMovInss);
            $oMnyMovimento->setAtivo($nAtivo);
            $oMnyMovimento->setMovOutrosDescBanco($nMovOutrosDesc);
            return $oMnyMovimento;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyMovimento
         *
         * @return boolean
         */
        public function inserirMnyMovimento($oMnyMovimento){
            $oPersistencia = new Persistencia($oMnyMovimento);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyMovimento
         *
         * @return boolean
         */
        public function alterarMnyMovimento($oMnyMovimento){
            $oPersistencia = new Persistencia($oMnyMovimento);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimento
         *
         * @return boolean
         */
        public function excluirMnyMovimentoFisicamente($nMovCodigo){
            $oMnyMovimento = new MnyMovimento();

            $oMnyMovimento->setMovCodigo($nMovCodigo);
            $oPersistencia = new Persistencia($oMnyMovimento);
                $bExcluir = $oPersistencia->excluirFisicamente() ;

            if($bExcluir)
                    return true;
            return false;

        }


/**
         *
         * Método para excluir da base de dados um Objeto MnyMovimento
         *
         * @return boolean
         */
        public function excluirMnyMovimento($nMovCodigo){
            $oMnyMovimento = new MnyMovimento();

            $oMnyMovimento->setMovCodigo($nMovCodigo);
            $oPersistencia = new Persistencia($oMnyMovimento);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyMovimento na base de dados
         *
         * @return boolean
         */
        public function presenteMnyMovimento($nMovCodigo){
            $oMnyMovimento = new MnyMovimento();

            $oMnyMovimento->setMovCodigo($nMovCodigo);
            $oPersistencia = new Persistencia($oMnyMovimento);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimento da base de dados
         *
         * @return MnyMovimento
         */
        public function recuperarUmMnyMovimento($nMovCodigo){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = $nMovCodigo";
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimento da base de dados
         *
         * @return MnyMovimento
         */
        public function recuperarUmMnyMovimentoPorContrato($nMovContrato){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "*";
            $sComplemento = " WHERE mov_contrato = " . $nMovContrato;
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimento da base de dados
         *
         * @return MnyMovimento
         */
        public function recuperarUmMnyMovimentoPorCaixinha($nMovCodigo,$nMovItem){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "mny_movimento.*";
            $sComplemento = "Inner Join mny_movimento_item On mny_movimento_item.mov_codigo=mny_movimento.mov_codigo And mny_movimento_item.mov_item=$nMovItem
            Inner Join (Select * From v_tramitacao Where mov_codigo = $nMovCodigo  And mov_item=$nMovItem ORDER BY data DESC LIMIT 1) t On t.cod_para=11
            WHERE mny_movimento.mov_codigo=$nMovCodigo And (mny_movimento_item.caixinha =1 Or mny_movimento_item.caixinha =2)";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimento da base de dados
         *
         * @return MnyMovimento[]
         */
        public function recuperarTodosMnyMovimento(){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento;
            return false;
        }


            /**
         *
         * Método para recuperar um vetor de objetos MnyMovimento da base de dados
         *
         * @return MnyMovimento[]
         */
        public function recuperarTodosMnyMovimentoImpostoRelatorio($sCampos ,$sComplemento ){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = $sCampos;
            $sComplemento = $sComplemento;
            $voMnyMovimento = $oPersistencia->consultarTabelaTemporaria($sTabelas,$sCampos,$sComplemento);
            //echo "SELECT $sCampos  FROM $sTabelas  $sComplemento";
            if($voMnyMovimento)
                    return $voMnyMovimento;
                return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimento da base de dados
         *
         * @return MnyMovimento[]
         */
        public function recuperarTodosMnyMovimentoPorTipo($nMovTipo){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "*";
            $sComplemento = "WHERE mov_tipo= $nMovTipo";
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento;
            return false;
        }

        public function recuperarTodosMnyMovimentoPorContrato($nCodContrato){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "*";
            $sComplemento = " inner join mny_movimento_item on mny_movimento_item.mov_codigo = mny_movimento.mov_codigo and mny_movimento_item.ativo = 1 WHERE tip_ace_codigo=169 AND mov_contrato=".  $nCodContrato;
    //  		echo "$sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento;
            return false;
        }

        public function recuperarTodosMnyMovimentoPorAportePorUnidadePeriodo($sComplemento){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento m";
            $sCampos = "concat(mi.mov_codigo,'.',mi.mov_item) As FA,
                        e.emp_fantasia, IFNULL(ai.aporte_descricao,'-') AS numero_aporte,
                         DATE_FORMAT(mi.mov_data_vencto,'%d/%m/%Y') As vencimento,
                        IF(arq.cod_arquivo,'SIM',IF(mi.caixinha=1,'CAIXINHA','NÃO'))AS pago,
                        ifnull(mi.mov_juros,0) AS mov_juros,
                        ((ifnull(m.mov_outros_desc,0) + ifnull(mi.mov_valor_tarifa_banco,0)) + ifnull(mi.mov_retencao,0)) AS desconto,
                        ifnull(f_valor_parcela_liquido (m.mov_codigo,mi.mov_item),0) AS valor_liquido,p.nome,
                        m.mov_obs As descricao,unidade.descricao As unidade,concat(conta.codigo,'-',conta.descricao) As conta,
                        concat(c_custo.codigo,'-',c_custo.descricao) AS custo,
                        (SELECT para FROM v_tramitacao WHERE v_tramitacao.mov_codigo = mi.mov_codigo AND v_tramitacao.mov_item = mi.mov_item ORDER BY DATA DESC LIMIT 1) As tramitacao_para";
            $sComplemento = "INNER JOIN mny_movimento_item mi ON mi.mov_codigo = m.mov_codigo And ( mi.mov_item <> 98 Or mi.mov_item <> 98)
                            Left  Join v_aporte_item ai On ai.mov_codigo=mi.mov_codigo And ai.mov_item=mi.mov_item
                            Left  Join v_sys_arquivo arq On arq.mov_codigo=ai.mov_codigo And arq.mov_item=ai.mov_item And arq.cod_tipo=11
                            Inner Join mny_plano_contas unidade On unidade.plano_contas_codigo=m.uni_codigo
                            Inner Join mny_plano_contas conta On conta.plano_contas_codigo=m.con_codigo
                            INNER JOIN mny_plano_contas c_custo ON c_custo.plano_contas_codigo = m.cen_codigo
                            INNER JOIN v_pessoa_geral_formatada p On p.pesg_codigo = m.pes_codigo
                            INNER JOIN sys_empresa e On e.emp_codigo = m.emp_codigo
                            Where
                            /* m.mov_codigo not in ((Select mov_codigo_origem From mny_transferencia Where ativo=1))
                             and m.mov_codigo not in ((Select mov_codigo_destino From mny_transferencia Where ativo=1))
                             And*/ m.mov_tipo=188 And  ".$sComplemento ." GROUP BY ai.cod_solicitacao_aporte,mi.mov_codigo,mi.mov_item,arq.cod_arquivo,ai.valor_liquido,p.nome ORDER BY vencimento DESC ";
     		//echo "Select ".$sCampos." From $sTabelas $sComplemento";
            //die();
            $voMnyMovimento = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento;
            return false;
        }

        /**
         *
         * Método para inicializar um Objeto MnyMovimentoDoc
         *
         * @return MnyMovimentoDoc
         */
        public function inicializarMnyMovimentoDoc($nMovCodigo,$nDocSeq,$sDocPath,$sDocNome,$sDocOrigem){
            $oMnyMovimentoDoc = new MnyMovimentoDoc();

            $oMnyMovimentoDoc->setMovCodigo($nMovCodigo);
            $oMnyMovimentoDoc->setDocSeq($nDocSeq);
            $oMnyMovimentoDoc->setDocPath($sDocPath);
            $oMnyMovimentoDoc->setDocNome($sDocNome);
            $oMnyMovimentoDoc->setDocOrigem($sDocOrigem);
            return $oMnyMovimentoDoc;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyMovimentoDoc
         *
         * @return boolean
         */
        public function inserirMnyMovimentoDoc($oMnyMovimentoDoc){
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyMovimentoDoc
         *
         * @return boolean
         */
        public function alterarMnyMovimentoDoc($oMnyMovimentoDoc){
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoDoc
         *
         * @return boolean
         */
        public function excluirMnyMovimentoDoc($nMovCodigo,$nDocSeq){
            $oMnyMovimentoDoc = new MnyMovimentoDoc();

            $oMnyMovimentoDoc->setMovCodigo($nMovCodigo);
            $oMnyMovimentoDoc->setDocSeq($nDocSeq);
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyMovimentoDoc na base de dados
         *
         * @return boolean
         */
        public function presenteMnyMovimentoDoc($nMovCodigo,$nDocSeq){
            $oMnyMovimentoDoc = new MnyMovimentoDoc();

            $oMnyMovimentoDoc->setMovCodigo($nMovCodigo);
            $oMnyMovimentoDoc->setDocSeq($nDocSeq);
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimentoDoc da base de dados
         *
         * @return MnyMovimentoDoc
         */
        public function recuperarUmMnyMovimentoDoc($nMovCodigo,$nDocSeq){
            $oMnyMovimentoDoc = new MnyMovimentoDoc();
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
            $sTabelas = "mny_movimento_doc";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = $nMovCodigo AND doc_seq = $nDocSeq";
            $voMnyMovimentoDoc = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoDoc)
                return $voMnyMovimentoDoc[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoDoc da base de dados
         *
         * @return MnyMovimentoDoc[]
         */
        public function recuperarTodosMnyMovimentoDoc(){
            $oMnyMovimentoDoc = new MnyMovimentoDoc();
            $oPersistencia = new Persistencia($oMnyMovimentoDoc);
            $sTabelas = "mny_movimento_doc";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyMovimentoDoc = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoDoc)
                return $voMnyMovimentoDoc;
            return false;
        }

    /**
         *
         * Método para inicializar um Objeto MnyContratoDocTipo
         *
         * @return MnyContratoDocTipo
         */
        public function inicializarMnyContratoDocTipo($nCodContratoDocTipo,$nCodContratoTipo,$sDescricao,$nObrigatorio,$nAcaoInicial,$nItem,$nTamanhoArquivo,$nAtivo){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();

            $oMnyContratoDocTipo->setCodContratoDocTipo($nCodContratoDocTipo);
            $oMnyContratoDocTipo->setCodContratoTipo($nCodContratoTipo);
            $oMnyContratoDocTipo->setDescricao($sDescricao);
            $oMnyContratoDocTipo->setObrigatorio($nObrigatorio);
            $oMnyContratoDocTipo->setAcaoInicial($nAcaoInicial);
            $oMnyContratoDocTipo->setItem($nItem);
            $oMnyContratoDocTipo->setTamanhoArquivo($nTamanhoArquivo);
            $oMnyContratoDocTipo->setAtivo($nAtivo);
            return $oMnyContratoDocTipo;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoDocTipo
         *
         * @return boolean
         */
        public function inserirMnyContratoDocTipo($oMnyContratoDocTipo){
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoDocTipo
         *
         * @return boolean
         */
        public function alterarMnyContratoDocTipo($oMnyContratoDocTipo){
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoDocTipo
         *
         * @return boolean
         */
        public function excluirMnyContratoDocTipo($nCodContratoDocTipo){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();

            $oMnyContratoDocTipo->setCodContratoDocTipo($nCodContratoDocTipo);
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
                $bExcluir = $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyContratoDocTipo na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoDocTipo($nCodContratoDocTipo){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();

            $oMnyContratoDocTipo->setCodContratoDocTipo($nCodContratoDocTipo);
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoDocTipo da base de dados
         *
         * @return MnyContratoDocTipo
         */
        public function recuperarUmMnyContratoDocTipo($nCodContratoDocTipo){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            $sTabelas = "mny_contrato_doc_tipo";
            $sCampos = "*";
            $sComplemento = " WHERE cod_contrato_doc_tipo = $nCodContratoDocTipo";
    //  		echo " $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoDocTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoDocTipo)
                return $voMnyContratoDocTipo[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoDocTipo da base de dados
         *
         * @return MnyContratoDocTipo[]
         */
        public function recuperarTodosMnyContratoDocTipo(){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            $sTabelas = "mny_contrato_doc_tipo";
            $sCampos = "cod_contrato_doc_tipo, mny_contrato_tipo.descricao as cod_contrato_tipo,mny_contrato_doc_tipo.descricao as descricao, obrigatorio,acao_inicial, item, mny_contrato_doc_tipo.ativo as ativo";
            $sComplemento = " INNER JOIN mny_contrato_tipo ON mny_contrato_doc_tipo.cod_contrato_tipo = mny_contrato_tipo.cod_contrato_tipo WHERE mny_contrato_doc_tipo.ativo=1";

            $voMnyContratoDocTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoDocTipo)
                return $voMnyContratoDocTipo;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoDocTipo da base de dados
         *
         * @return MnyContratoDocTipo[]
         */
        public function recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($nCodContratoTipo,$nAcaoInicial){
            $oMnyContratoDocTipo = new MnyContratoDocTipo();
            $oPersistencia = new Persistencia($oMnyContratoDocTipo);
            $sTabelas = "mny_contrato_doc_tipo";
            $sCampos = "*";
            $sComplemento = "  WHERE ativo=1 AND cod_contrato_tipo=".$nCodContratoTipo." AND acao_inicial=".$nAcaoInicial;
            // echo "SELECT * FROM $sTabelas " . $sComplemento;
            //die();
            $voMnyContratoDocTipo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoDocTipo)
                return $voMnyContratoDocTipo;
            return false;
        }

        /**
         *
         * Método para inicializar um Objeto MnyMovimentoItem
         *
         * @return MnyMovimentoItem
         */
        public function inicializarMnyMovimentoItem($nMovCodigo,$nMovItem,$dMovDataVencto,$dMovDataPrev,$nMovValorParcela,$nMovJuros,$nMovValorPagar,$nFpgCodigo,$nTipDocCodigo,$nMovRetencao,$dMovDataInclusao,$nMovValorTarifaBanco,$nTipAceCodigo,$dCompetencia,$nContabil,$nFinalizado,$nAtivo,$nCaixinha,$sFaOrigem,$nNf,$nConciliado){
            $oMnyMovimentoItem = new MnyMovimentoItem();

            $oMnyMovimentoItem->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItem->setMovItem($nMovItem);
            $oMnyMovimentoItem->setMovDataVenctoBanco($dMovDataVencto);
            $oMnyMovimentoItem->setMovDataPrevBanco($dMovDataPrev);
            $oMnyMovimentoItem->setMovValorParcelaBanco($nMovValorParcela);
            $oMnyMovimentoItem->setMovJurosBanco($nMovJuros);
            $oMnyMovimentoItem->setMovValorPagarBanco($nMovValorPagar);
            $oMnyMovimentoItem->setFpgCodigo($nFpgCodigo);
            $oMnyMovimentoItem->setTipDocCodigo($nTipDocCodigo);
            $oMnyMovimentoItem->setMovRetencaoBanco($nMovRetencao);
            $oMnyMovimentoItem->setMovDataInclusaoBanco($dMovDataInclusao);
            $oMnyMovimentoItem->setMovValorTarifaBancoBanco($nMovValorTarifaBanco);
            $oMnyMovimentoItem->setTipAceCodigo($nTipAceCodigo);
            $oMnyMovimentoItem->setCompetenciaBanco($dCompetencia);
            $oMnyMovimentoItem->setContabil($nContabil);
            $oMnyMovimentoItem->setFinalizado($nFinalizado);
            $oMnyMovimentoItem->setAtivo($nAtivo);
            $oMnyMovimentoItem->setCaixinha($nCaixinha);
            $oMnyMovimentoItem->setFaOrigem($sFaOrigem);
            $oMnyMovimentoItem->setNf($nNf);
            $oMnyMovimentoItem->setConciliado($nConciliado);
            return $oMnyMovimentoItem;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyMovimentoItem
         *
         * @return boolean
         */
        public function inserirMnyMovimentoItem($oMnyMovimentoItem){
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyMovimentoItem
         *
         * @return boolean
         */
        public function alterarMnyMovimentoItem($oMnyMovimentoItem){
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItem
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItem($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();

            $oMnyMovimentoItem->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItem->setMovItem($nMovItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $bExcluir =  $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItem
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItemProcedure($nMovCodigo,$nMovItem,$sLogin,$sMotivo){
            $oMnyMovimentoItem = new MnyMovimentoItem();

            //$oMnyMovimentoItem->setMovCodigo($nMovCodigo);
            //$oMnyMovimentoItem->setMovItem($nMovItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sProcedure = "sp_exclui_movimento_completo";

            $sParametros = $nMovCodigo.",".$nMovItem.",'".$sLogin."','".$sMotivo."'";
            $oMnyMovimentoItem = $oPersistencia->excluirFisicamenteProcedure($sProcedure,$sParametros);
            return $oMnyMovimentoItem[0];


        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function excluirTodosMnyMovimentoItemPorMovimento($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $sComplemento = " WHERE mov_codigo=".$nMovCodigo;
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para verificar se existe um Objeto MnyMovimentoItem na base de dados
         *
         * @return boolean
         */
        public function presenteMnyMovimentoItem($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();

            $oMnyMovimentoItem->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItem->setMovItem($nMovItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarUmMnyMovimentoItem($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = $nMovCodigo AND mov_item = $nMovItem";
    //		echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarUmMyMovimentoItemDEVOLUCAOESTORNOPorMovimentoOrigem($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = " WHERE  fa_origem='".$nMovCodigo.'.'.$nMovItem."' And mov_item=1";
    		//echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }


        /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarUmMnyMovimentoItemValorLiquido($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sFuncao = "f_valor_parcela_liquido";
            $sCampos = "mov_valor_parcela";
            $sComplemento = $nMovCodigo .",". $nMovItem;
            $voMnyMovimentoItem = $oPersistencia->consultarFuncao($sFuncao,$sCampos,$sComplemento);

            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }


            /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarUmMnyMovimentoItemValorGlobal($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sFuncao = "f_valor_global";
            $sCampos = "mov_valor_parcela";
            $sComplemento = $nMovCodigo ;
            $voMnyMovimentoItem = $oPersistencia->consultarFuncao($sFuncao,$sCampos,$sComplemento);

            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

         /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarUmMnyMovimentoItemValorGlobalLiquido($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sFuncao = "f_global_liquido";
            $sCampos = "mov_valor_parcela";
            $sComplemento = $nMovCodigo ;
            $voMnyMovimentoItem = $oPersistencia->consultarFuncao($sFuncao,$sCampos,$sComplemento);

            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }


        /**
         *
         * Método para recuperar um grupo de objetos MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarTodosMnyMovimentoItemValorGlobal($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "	sum(mny_movimento_item.mov_valor_parcela) as mov_valor_parcela,
                       (select
                         IFNULL(mny_movimento_item.mov_juros,0) +
                         IFNULL(mny_movimento_item.mov_retencao,0) +
                         IFNULL(mny_movimento_item.mov_valor_tarifa_banco,0)
                       ) as mov_juros,
                       (sum(mny_movimento_item.mov_valor_parcela) - (select
                         IFNULL(mny_movimento_item.mov_juros,0) +
                         IFNULL(mny_movimento_item.mov_retencao,0) +
                         IFNULL(mny_movimento_item.mov_valor_tarifa_banco,0)
                        ) ) as mov_valor_pagar";
            $sComplemento = " WHERE ativo= 1 and mov_codigo =". $nMovCodigo;
        //	echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }


 /**
         *
         * Método para inicializar um Objeto MnyMovimentoItemAgrupador
         *
         * @return MnyMovimentoItemAgrupador
         */
        public function inicializarMnyMovimentoItemAgrupador($nMovCodigoAgrupador,$nMovItemAgrupador,$nMovCodigo,$nMovItem){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();

            $oMnyMovimentoItemAgrupador->setMovCodigoAgrupador($nMovCodigoAgrupador);
            $oMnyMovimentoItemAgrupador->setMovItemAgrupador($nMovItemAgrupador);
            $oMnyMovimentoItemAgrupador->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItemAgrupador->setMovItem($nMovItem);

            return $oMnyMovimentoItemAgrupador;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyMovimentoItemAgrupador
         *
         * @return boolean
         */
        public function inserirMnyMovimentoItemAgrupador($oMnyMovimentoItemAgrupador){
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyMovimentoItemAgrupador
         *
         * @return boolean
         */
        public function alterarMnyMovimentoItemAgrupador($oMnyMovimentoItemAgrupador){
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItemAgrupador
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItemAgrupador($nMovCodigoAgrupador,$nMovItemAgrupador,$nMovCodigo,$nMovItem){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();

            $oMnyMovimentoItemAgrupador->setMovCodigoAgrupador($nMovCodigoAgrupador);
            $oMnyMovimentoItemAgrupador->setMovItemAgrupador($nMovItemAgrupador);
            $oMnyMovimentoItemAgrupador->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItemAgrupador->setMovItem($nMovItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            $bExcluir =  $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItemAgrupador
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItemAgrupadorProcedure($nMovCodigoAgrupador,$nMovItemAgrupador,$sLogin,$sMotivo){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();

            //$oMnyMovimentoItemAgrupador)->setMovCodigoAgrupador($nMovCodigoAgrupador);
            //$oMnyMovimentoItemAgrupador)->setMovItemAgrupador)($nMovItemAgrupador));
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            $sProcedure = "sp_exclui_movimento_agrupado_completo";

            $sParametros = $nMovCodigoAgrupador.",".$nMovItemAgrupador.",'".$sLogin."','".$sMotivo."'";
            $oMnyMovimentoItem = $oPersistencia->excluirFisicamenteProcedure($sProcedure,$sParametros);
            return $oMnyMovimentoItem[0];


        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function excluirTodosMnyMovimentoItemAgrupadorPorMovimentoItem($nMovCodigoAgrupador,$nMovItemAgrupador){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();
            $sComplemento = " WHERE mov_codigo_agrupador=".$nMovItemAgrupador . " AND mov_item_agrupador=".$nMovItemAgrupador;
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para verificar se existe um Objeto MnyMovimentoItem na base de dados
         *
         * @return boolean
         */
        public function presenteMnyMovimentoItemAgrupador($nMovCodigoAgrupador,$nMovItemAgrupador,$nMovCodigo,$nMovItem){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();

            $oMnyMovimentoItemAgrupador->setMovCodigoAgrupador($nMovCodigoAgrupador);
            $oMnyMovimentoItemAgrupador->setMovItemAgrupador($nMovItemAgrupador);
            $oMnyMovimentoItemAgrupador->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItemAgrupador->setMovItem($nMovItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarTodosMnyMovimentoItemAgrupadorPorMovimentoItemAgrupador($nMovCodigoAgrupador,$nMovItemAgrupador){
            $oMnyMovimentoItemAgrupador = new MnyMovimentoItemAgrupador();
            $oPersistencia = new Persistencia($oMnyMovimentoItemAgrupador);
            $sTabelas = "mny_movimento_item_agrupador";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo_agrupador = $nMovCodigoAgrupador  AND mov_item_agrupador = $nMovItemAgrupador";
//    		echo "SELECT $sCampos FROM $sTabelas $sComplemento";

            $voMnyMovimentoItemAgrupador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemAgrupador)
                return $voMnyMovimentoItemAgrupador;
            return false;
        }

        /**
         *
         * Método para recuperar um grupo de objetos MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarValoresGlobaisMovimento($nMovCodigo){
            $oMnyMovimento = new MnyMovimento();
            $oPersistencia = new Persistencia($oMnyMovimento);
            $sTabelas = "mny_movimento";
            $sCampos = "sum(mny_movimento_item.mov_valor_parcela) AS mov_pis,
                        sum(mny_movimento_item.mov_valor_parcela) -
                        (IFNULL(mny_movimento.mov_icms_aliq, 0)	+
                        IFNULL(mny_movimento.mov_pis, 0)	+
                        IFNULL(mny_movimento.mov_confins, 0)	+
                        IFNULL(mny_movimento.mov_csll, 0) +
                        IFNULL(mny_movimento.mov_iss, 0)	+
                        IFNULL(mny_movimento.mov_ir, 0)	+
                        IFNULL(mny_movimento.mov_irrf, 0) -
                        IFNULL(mny_movimento.mov_outros, 0)) AS mov_devolucao
                        ";
            $sComplemento = " JOIN mny_movimento_item ON mny_movimento.mov_codigo= mny_movimento_item.mov_codigo
                            WHERE mny_movimento.mov_codigo = ".$nMovCodigo." AND mny_movimento_item.ativo = 1 ";
    //		echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimento)
                return $voMnyMovimento[0];
            return false;
        }


        /**
         *
         * Método para recuperar um grupo de objetos MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem
         */
        public function recuperarTodosMnyMovimentoItemPorMovimento($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = " WHERE ativo=1 and mov_codigo = $nMovCodigo ";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }


      /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnyAporteItem[]
         */
        public function recuperarUmMnyMovimentoItemAgrupadoVerificacao($nCodPessoa,$nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "sp_adiciona_item_movimento_agrupado";
            //$sCampos = "mov_codigo,mov_item,ativo,aporte_item ";
            $sParametros = $nCodPessoa.",".$nMovCodigo .",".$nMovItem;
            $oMnyMovimentoItem = $oPersistencia->consultarProcedure($sTabelas,$sParametros);

            if($oMnyMovimentoItem)
                return $oMnyMovimentoItem[0]->getAtivo();
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos VMovimento da base de dados
         *
         * @return MnyMovimentoItem[]
         */
        public function recuperarTodosFADisponiveisAportePorUnidadeGrupoUsuario($nCodUnidade,$nCodGrupoUsuario){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas ="mny_movimento_item";
            $sCampos  =" DISTINCT mny_movimento_item.mov_codigo,mny_movimento_item.mov_item,DATE_FORMAT(mny_movimento_item.mov_data_vencto,'%d/%m/%Y') AS mov_data_vencto, f_valor_parcela_liquido (mny_movimento_item.mov_codigo,mny_movimento_item.mov_item) AS mov_valor_parcela,mny_movimento.mov_obs as fa_origem";
            $sComplemento = "INNER JOIN mny_movimento ON mny_movimento_item.mov_codigo = mny_movimento.mov_codigo AND mny_movimento.ativo = 1 AND mny_movimento.mov_tipo = 188 AND mny_movimento.uni_codigo = ".$nCodUnidade."
INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.mov_codigo = mny_movimento_item.mov_codigo AND mny_movimento_item_protocolo.mov_item = mny_movimento_item.mov_item AND mny_movimento_item_protocolo.para_cod_tipo_protocolo = ".$nCodGrupoUsuario."
WHERE mny_movimento_item.mov_item NOT IN (98,99,97) and conciliado=0
   -- AND CONCAT(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) NOT IN (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) AS FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo = 1 AND mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = ".$nCodUnidade." AND mny_aporte_item.estornado = 0)
    AND CONCAT(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) NOT IN (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item)  FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo = 1 AND mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte INNER JOIN mny_transferencia ON mny_solicitacao_aporte.cod_solicitacao = mny_transferencia.cod_solicitacao_aporte INNER JOIN mny_movimento ON mny_transferencia.mov_codigo_origem = mny_movimento.mov_codigo and mov_tipo=188 WHERE uni_codigo = ".$nCodUnidade." AND mny_aporte_item.estornado = 0)

                            ORDER BY
                                mov_data_vencto ASC";

            // antiga
            // -- AND CONCAT(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) NOT IN (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) AS FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo = 1 AND mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = 209 AND mny_aporte_item.estornado = 0)
            //Where  v_movimento.mov_item Not In(98,99) And v_movimento.mov_tipo=188 And v_movimento.uni_codigo = ".$nCodUnidade." And mny_movimento_item_protocolo.para_cod_tipo_protocolo = " . $nCodGrupoUsuario ." AND CONCAT(mny_movimento_item_protocolo.mov_codigo,'.',mny_movimento_item_protocolo.mov_item) not in (SELECT CONCAT(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) as FA FROM mny_aporte_item INNER JOIN mny_solicitacao_aporte ON mny_solicitacao_aporte.ativo=1 and mny_solicitacao_aporte.cod_solicitacao = mny_aporte_item.cod_solicitacao_aporte WHERE uni_codigo = ".$nCodUnidade.")  Order BY mov_data_vencto ASC limit 20";
            //AND concat(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) NOT IN(SELECT CONCAT(mny_conciliacao.mov_codigo,'.',mny_conciliacao.mov_item) AS FA FROM mny_conciliacao)
          // echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voVMovimento = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voVMovimento)
                return $voVMovimento;
            return false;
        }



        public function recuperarTodosMnyMovimentoItemValorPorMovimento($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "SUM(mov_valor_parcela) AS mov_valor_parcela";
            $sComplemento = "WHERE ativo=1 and mov_codigo = $nMovCodigo";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

        public function recuperarTodosMnyMovimentoItemPorContrato($nCodContrato, $nMovcodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "SUM(mov_valor_parcela) AS mov_valor_parcela";
            $sComplemento ="JOIN mny_movimento WHERE mny_movimento.mov_codigo = ".$nMovcodigo."
                            AND  mny_movimento_item.mov_codigo = mny_movimento.mov_codigo
                            AND mny_movimento.mov_contrato = ".$nCodContrato."
                            AND mny_movimento.ativo = 1
                            AND mny_movimento_item.ativo = 1";
        //	echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

        public function recuperarTodosMnyMovimentoItemPorAporte($nCodSolicitacaoAporte){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item mi";
            $sCampos = "mi.*";
            $sComplemento ="INNER JOIN mny_aporte_item ai ON ai.mov_codigo = mi.mov_codigo AND ai.mov_item = mi.mov_item
                            WHERE ai.cod_solicitacao_aporte =" . $nCodSolicitacaoAporte;
        //	echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }


        public function recuperarTodosMnyMovimentoItemPorConciliacao($nCodConciliacao){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item mi";
            $sCampos = "mi.*";
            $sComplemento ="INNER JOIN mny_conciliacao c ON c.mov_codigo = mi.mov_codigo AND c.mov_item = mi.mov_item
                            WHERE c.cod_conciliacao =" . $nCodConciliacao;
        //	echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }


        public function recuperarTodosMnyMovimentoItemPorConciliacaoDataConta($dData,$nCodConta){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item mi";
            $sCampos = "mi.*";
            $sComplemento ="INNER JOIN mny_conciliacao c ON c.mov_codigo = mi.mov_codigo AND c.mov_item = mi.mov_item
                            WHERE c.cod_conta_corrente=".$nCodConta." And c.data_conciliacao ='" . $dData."'";
        	//echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }


        /**
         * Método para recuperar um vetor de objetos MnyMovimentoItem da base de dados
         *
         * @return MnyMovimentoItem[]
         */
        public function recuperarTodosMnyMovimentoItem(){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }



     /**
         *
         * Método para inicializar um Objeto MnyMovimentoItemProtocolo
         *
         * @return MnyMovimentoItemProtocolo
         */
        public function inicializarMnyMovimentoItemProtocolo($nCodProtocoloMovimentoItem,$nMovCodigo,$nMovItem,$nDeCodTipoProtocolo,$nParaCodTipoProtocolo,$sResponsavel,$dData,$sDescricao,$nAtivo){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();

            $oMnyMovimentoItemProtocolo->setCodProtocoloMovimentoItem($nCodProtocoloMovimentoItem);
            $oMnyMovimentoItemProtocolo->setMovCodigo($nMovCodigo);
            $oMnyMovimentoItemProtocolo->setMovItem($nMovItem);
            $oMnyMovimentoItemProtocolo->setDeCodTipoProtocolo($nDeCodTipoProtocolo);
            $oMnyMovimentoItemProtocolo->setParaCodTipoProtocolo($nParaCodTipoProtocolo);
            $oMnyMovimentoItemProtocolo->setResponsavel($sResponsavel);
            $oMnyMovimentoItemProtocolo->setDataBanco($dData);
            $oMnyMovimentoItemProtocolo->setDescricao($sDescricao);
            $oMnyMovimentoItemProtocolo->setAtivo($nAtivo);
            return $oMnyMovimentoItemProtocolo;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyMovimentoItemProtocolo
         *
         * @return boolean
         */
        public function inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo){
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyMovimentoItemProtocolo
         *
         * @return boolean
         */
        public function alterarMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo){
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItemProtocolo
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItemProtocolo($nCodProtocoloMovimentoItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();

            $oMnyMovimentoItemProtocolo->setCodProtocoloMovimentoItem($nCodProtocoloMovimentoItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
                $bExcluir = $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItemProtocolo
         *
         * @return boolean
         */
        public function excluirMnyMovimentoItemProtocoloAGRUPADO($nMovCodigo,$nMovItem,$nMovCodigoAgrupador){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();

           $sComplemento = "Where mov_codigo=".$nMovCodigo." And mov_item=".$nMovItem." And descricao like'%[".$nMovCodigoAgrupador.".1]%'";

            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

            if($bExcluir)
                    return true;
            return false;

        }
            /**
         *
         * Método para excluir da base de dados um Objeto MnyMovimentoItemProtocolo
         *
         * @return boolean
         */
        public function desativarMnyMovimentoItemProtocolo($nCodigo,$nItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sComplemento = "WHERE mov_codigo=".$nCodigo." AND mov_item=".$nItem;
                $bExcluir = $oPersistencia->excluirSemChavePrimaria($sComplemento);

            if($bExcluir)
                    return true;
            return false;

        }



        /**
         *
         * Método para verificar se existe um Objeto MnyMovimentoItemProtocolo na base de dados
         *
         * @return boolean
         */
        public function presenteMnyMovimentoItemProtocolo($nCodProtocoloMovimentoItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();

            $oMnyMovimentoItemProtocolo->setCodProtocoloMovimentoItem($nCodProtocoloMovimentoItem);
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo
         */
        public function recuperarUmMnyMovimentoItemProtocolo($nCodProtocoloMovimentoItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "mny_movimento_item_protocolo";
            $sCampos = "*";
            $sComplemento = " WHERE cod_protocolo_movimento_item = $nCodProtocoloMovimentoItem";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemProtocolo(){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "mny_movimento_item_protocolo";
            $sCampos = "*";
            $sComplemento = "WHERE ativo=1";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemProtocoloPorMovimentoItem($nCodMovimento,$nCodItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "mny_movimento_item_protocolo";
            $sCampos = "*";
            $sComplemento = "WHERE ativo=1 AND mov_codigo=".$nCodMovimento." AND mov_item= ".$nCodItem . " ORDER BY cod_protocolo_movimento_item,data ASC";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }

         /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarUmMnyMovimentoItemTramitacaoPorMovimentoItemPerfilValidado($nMovCodigo,$nMovItem,$nCodPerfil){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "v_tramitacao";
            $sCampos = "*";
            $sComplemento = " Where mov_codigo=".$nMovCodigo ." AND mov_item=" . $nMovItem ." AND para_cod_tipo_protocolo " . $nCodPerfil . "
            and data = (SELECT max(data) from mny_movimento_item_protocolo where mov_codigo=".$nMovCodigo ." AND mov_item=" . $nMovItem ." limit 1)";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo[0];
            return false;
        }

         /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarUmMnyMovimentoItemPermiteAlteracaoFA($nMovCodigo,$nMovItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "v_tramitacao";
            $sCampos = "*";
            $sComplemento = " Where mov_codigo=".$nMovCodigo ." AND mov_item=" . $nMovItem ." AND para_cod_tipo_protocolo IN (18,15,6,7)
            and data = (SELECT max(data) from mny_movimento_item_protocolo where mov_codigo=".$nMovCodigo ." AND mov_item=" . $nMovItem ." limit 1)";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo[0];
            return false;
        }

       /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPorPendenciasPorUnidade($nParaCodTipoProtocolo,$nValidado,$nValidados='0',$nCodUnidade){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "v_tramitacao";
            $sCampos = " DISTINCT mov_codigo,mov_item, mov_valor,unidade,de,para,validado,tram_descricao,usuario,data,mov_contrato,tipo,de_cod_tipo_protocolo,para_cod_tipo_protocolo";
            $sComplemento = "Where para_cod_tipo_protocolo=".$nParaCodTipoProtocolo." And validado =".$nValidado." And cod_unidade =".$nCodUnidade."GROUP BY mov_codigo,mov_item";

        //	echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }




    /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPendenciasPorGrupoUsuario($nParaCodTipoProtocolo){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "mny_movimento_item_protocolo mip";
            $sCampos = "IF ((	mny_contrato_pessoa.tipo_contrato IN (1, 2, 3)),m.mov_contrato,'4') AS cod_protocolo_movimento_item,
    IF ((m.mov_tipo <> '189'),IF ((mny_contrato_pessoa.tipo_contrato IN (1, 2, 3)	),mny_contrato_pessoa.tipo_contrato,4), '9') AS mov_item, -- OK
    CONCAT(mip.mov_codigo,'.',mip.mov_item) AS mov_codigo,
    mip.de_cod_tipo_protocolo AS de_cod_tipo_protocolo, CONCAT(date_format(mi.mov_data_vencto,'%d/%m/%Y'),' - ',vpg.nome) AS para_cod_tipo_protocolo,
     mip.responsavel AS responsavel,
		mip.data AS data,
		DATE_FORMAT(mip.data,'%d/%m/%Y') AS data_formatada,
		DATE_FORMAT(conc.data_conciliacao,'%d/%m/%Y') AS data_conciliacao_formatada,
	  mip.descricao AS descricao,
    format(f_valor_parcela_liquido(mip.mov_codigo,mip.mov_item),2,'de_DE') AS ativo,
CONCAT(cc.ccr_agencia,IF(cc.ccr_agencia_dv,CONCAT('-',cc.ccr_agencia_dv),'')) AS agencia,
CONCAT(cc.ccr_conta,IF(cc.ccr_conta_dv,CONCAT('-',cc.ccr_conta_dv),'')) AS conta,
(SELECT descricao from mny_plano_contas where m.uni_codigo=plano_contas_codigo) as unidade_descricao,
m.mov_tipo as mov_tipo";
                $sComplemento = "
                INNER JOIN mny_movimento_item mi ON mi.finalizado = 0 AND mi.mov_codigo = mip.mov_codigo AND mi.mov_item = mip.mov_item
 INNER JOIN acesso_grupo_usuario para ON para.cod_grupo_usuario = mip.para_cod_tipo_protocolo
     INNER JOIN mny_movimento m ON m.mov_codigo = mi.mov_codigo
    INNER JOIN v_pessoa_geral_formatada vpg ON vpg.pesg_codigo = m.pes_codigo
LEFT JOIN mny_conciliacao conc ON 	 conc.mov_codigo = mi.mov_codigo and conc.mov_item = mi.mov_item
LEFT join mny_conta_corrente cc on cc.ccr_codigo = conc.cod_conta_corrente
    LEFT JOIN mny_contrato_pessoa ON mny_contrato_pessoa.contrato_codigo = m.mov_contrato
    WHERE mip.para_cod_tipo_protocolo = ".$nParaCodTipoProtocolo." and CONCAT(mi.mov_codigo,'.',mi.mov_item) in(select CONCAT(mov_codigo,'.',mov_item) from mny_movimento_item where finalizado=0 and mny_movimento_item.ativo=1) AND mip.data = (SELECT max(mny_movimento_item_protocolo.data) FROM mny_movimento_item_protocolo WHERE mny_movimento_item_protocolo.mov_codigo = mip.mov_codigo AND mny_movimento_item_protocolo.mov_item = mip.mov_item )";

            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }

    /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoRealizadosPorUnidade($nCodUnidade){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "mny_movimento_item_protocolo mip";

                $sCampos = " DISTINCT IF ((	contrato.tipo_contrato IN (1, 2, 3)),m.mov_contrato,'4') AS cod_protocolo_movimento_item,
                            IF ((m.mov_tipo <> '189'),IF ((contrato.tipo_contrato IN (1, 2, 3)	),contrato.tipo_contrato,4), '9') AS mov_item,
                            CONCAT(mip.mov_codigo,'.',mip.mov_item) AS mov_codigo,
                            mip.de_cod_tipo_protocolo AS de_cod_tipo_protocolo, mip.para_cod_tipo_protocolo AS para_cod_tipo_protocolo,
                             mip.responsavel AS responsavel, mip.data AS data, mip.descricao AS descricao,
                            format(f_valor_parcela_liquido(mip.mov_codigo,mip.mov_item),2,'de_DE') AS ativo";
                $sComplemento = " INNER JOIN mny_movimento_item mi ON mi.mov_codigo = mip.mov_codigo AND mi.mov_item = mip.mov_item AND mi.finalizado=0 And mip.data =(Select Max(data) From mny_movimento_item_protocolo Where mov_codigo=  mi.mov_codigo And mov_item=mi.mov_item)
                                INNER JOIN mny_movimento m ON m.mov_codigo = mi.mov_codigo AND mi.ativo = 1 AND m.uni_codigo = ".$nCodUnidade."
                                LEFT JOIN mny_contrato_pessoa contrato ON contrato.contrato_codigo = m.mov_contrato
                                INNER JOIN mny_plano_contas pc ON pc.plano_contas_codigo = m.uni_codigo
                                INNER JOIN acesso_grupo_usuario gu_de ON gu_de.cod_grupo_usuario = mip.de_cod_tipo_protocolo
                                INNER JOIN acesso_grupo_usuario gu_para ON gu_para.cod_grupo_usuario = mip.para_cod_tipo_protocolo";

        //	echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }


       /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoRealizados(){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "mny_movimento_item_protocolo mip";

                $sCampos = " IF ((	contrato.tipo_contrato IN (1, 2, 3)),m.mov_contrato,'4') AS cod_protocolo_movimento_item,
                             IF ((m.mov_tipo <> '189'),IF ((contrato.tipo_contrato IN (1, 2, 3)	),contrato.tipo_contrato,4), '9') AS mov_item,
                             CONCAT(mip.mov_codigo,'.',mip.mov_item) AS mov_codigo,
                             mip.de_cod_tipo_protocolo AS de_cod_tipo_protocolo, mip.para_cod_tipo_protocolo AS para_cod_tipo_protocolo,
                             mip.responsavel AS responsavel, mip.data AS data, mip.descricao AS descricao,
                            format(f_valor_parcela_liquido(mip.mov_codigo,mip.mov_item),2,'de_DE') AS ativo";

                $sComplemento = " INNER JOIN mny_movimento_item mi ON mi.mov_codigo = mip.mov_codigo AND mi.mov_item = mip.mov_item AND mi.finalizado=0 And mip.data =(Select Max(data) From mny_movimento_item_protocolo Where mov_codigo=  mi.mov_codigo And mov_item=mi.mov_item)
                                INNER JOIN mny_movimento m ON m.mov_codigo = mi.mov_codigo AND mi.ativo = 1
                                LEFT JOIN mny_contrato_pessoa contrato ON contrato.contrato_codigo = m.mov_contrato
                                INNER JOIN mny_plano_contas pc ON pc.plano_contas_codigo = m.uni_codigo
                                INNER JOIN acesso_grupo_usuario gu_de ON gu_de.cod_grupo_usuario = mip.de_cod_tipo_protocolo
                                INNER JOIN acesso_grupo_usuario gu_para ON gu_para.cod_grupo_usuario = mip.para_cod_tipo_protocolo";

            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }


    /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPendenciasPorGrupoUsuarioUnidade($nParaCodTipoProtocolo,$nCodUnidade){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "mny_movimento_item_protocolo mip";
            $sCampos = " IF (( contrato.tipo_contrato IN (1, 2, 3)),m.mov_contrato,'4') AS cod_protocolo_movimento_item
                            , IF ((m.mov_tipo <> '189'),IF ((contrato.tipo_contrato IN (1, 2, 3) ),contrato.tipo_contrato,4), '9') AS mov_item
                            , CONCAT(mip.mov_codigo,'.',mip.mov_item) AS mov_codigo
                            , mip.de_cod_tipo_protocolo AS de_cod_tipo_protocolo
                            ,   CONCAT(
                                date_format(
                                    mi.mov_data_vencto,
                                    '%d/%m/%Y'
                                ),
                                ' - ',
                                vpg.nome
                            ) AS para_cod_tipo_protocolo
                            , mip.responsavel AS responsavel, mip.data AS data
                            , date_format(mip.data,'%d/%m/%Y') AS data_formatada
                            , mip.descricao AS descricao, format(f_valor_parcela_liquido(mip.mov_codigo,mip.mov_item),2,'de_DE') AS ativo ";

                $sComplemento = "  INNER JOIN acesso_grupo_usuario para ON para.cod_grupo_usuario = mip.para_cod_tipo_protocolo
                                    INNER JOIN mny_movimento_item mi ON mi.finalizado = 0 AND mi.mov_codigo = mip.mov_codigo AND mi.mov_item = mip.mov_item
                                    INNER JOIN mny_movimento m ON m.mov_codigo = mi.mov_codigo
                                    INNER JOIN v_pessoa_geral_formatada vpg ON vpg.pesg_codigo = m.pes_codigo
                                    LEFT JOIN mny_contrato_pessoa contrato ON contrato.contrato_codigo = m.mov_contrato
                                    WHERE mip.para_cod_tipo_protocolo = ".$nParaCodTipoProtocolo."	AND m.uni_codigo=".$nCodUnidade." AND CONCAT(mi.mov_codigo,'.',mi.mov_item) in(select CONCAT(mov_codigo,'.',mov_item) from mny_movimento_item where finalizado=0 and mny_movimento_item.ativo=1)
                                    AND mip.data = (SELECT max(mny_movimento_item_protocolo.data) FROM mny_movimento_item_protocolo
                                    WHERE mny_movimento_item_protocolo.mov_codigo = mip.mov_codigo AND mny_movimento_item_protocolo.mov_item = mip.mov_item )";

            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }


       /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPorPendenciasPorRealizados($nParaCodTipoProtocolo,$nValidado,$nValidados='0'){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);

            $sTabelas = "mny_movimento_item_protocolo";
            if($nValidados==1){
                $sCampos = " DISTINCT mny_movimento_item.mov_codigo,mny_movimento_item.mov_item, mov_valor,unidade,de,para,validado,tram_descricao,usuario,data,mny_movimento.mov_contrato,tipo,de_cod_tipo_protocolo,para_cod_tipo_protocolo";

                $sComplemento = "inner JOIN mny_movimento_item on mny_movimento_item.mov_codigo = v_tramitacao.mov_codigo and mny_movimento_item.mov_item = v_tramitacao.mov_item
                                 inner JOIN mny_movimento on mny_movimento.mov_codigo = mny_movimento_item.mov_codigo and mny_movimento_item.ativo=1
                                where concat(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) not in (select concat(mny_conciliacao.mov_codigo,'.',mny_conciliacao.mov_item) from mny_conciliacao where consolidado=1)
                                and  para_cod_tipo_protocolo=$nParaCodTipoProtocolo";
            }else{
                $sCampos = " DISTINCT mov_codigo,mov_item, mov_valor,unidade,de,para,validado,tram_descricao,usuario,data,mov_contrato,tipo,de_cod_tipo_protocolo,para_cod_tipo_protocolo";
                $sComplemento = "Where para_cod_tipo_protocolo=".$nParaCodTipoProtocolo." And validado =".$nValidado."
                                GROUP BY mov_codigo,mov_item ";
            }
        //	echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }



        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarUmMnyMovimentoItemProtocoloPorMovimentoItem($nCodGrupoUsuario,$nMovCodigo,$nMovItem,$nEmpresa){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas ="mny_movimento_item_protocolo PROTOCOLO";
            $sCampos = "max(PROTOCOLO.cod_protocolo_movimento_item),
                        PROTOCOLO.mov_codigo,
                        PROTOCOLO.mov_item,
                        -- PROTOCOLO.de_cod_tipo_protocolo,
                        -- PROTOCOLO.para_cod_tipo_protocolo,
                        -- PROTOCOLO.responsavel,
                        PROTOCOLO.DATA,
                        PROTOCOLO.descricao,
                        TIPO_PROTOCOLO.descricao AS responsavel,
                        VPESQUISA.mov_contrato AS de_cod_tipo_protocolo,
                        VPESQUISA.tipo AS para_cod_tipo_protocolo,
                        PROTOCOLO.ativo";
            $sComplemento = "INNER JOIN v_pesquisa VPESQUISA ON VPESQUISA.mov_codigo = PROTOCOLO.mov_codigo AND VPESQUISA.mov_item = PROTOCOLO.mov_item
                             INNER JOIN sys_tipo_protocolo TIPO_PROTOCOLO ON TIPO_PROTOCOLO.cod_tipo_protocolo = PROTOCOLO.de_cod_tipo_protocolo
            WHERE  VPESQUISA.item_ativo=1 AND PROTOCOLO.para_cod_tipo_protocolo=".$nCodGrupoUsuario."
            AND PROTOCOLO.mov_codigo=".$nMovCodigo."
            AND PROTOCOLO.mov_item=".$nMovItem . "
            AND emp_codigo=".$nEmpresa."
                    AND PROTOCOLO.cod_protocolo_movimento_item = (SELECT
                    max(cod_protocolo_movimento_item)
                    FROM
                    mny_movimento_item_protocolo
                 WHERE
                    para_cod_tipo_protocolo=".$nCodGrupoUsuario."
                    AND mov_codigo=".$nMovCodigo.")";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo[0];
            return false;
        }


         /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarMnyMovimentoImtemProtocoloPorMovimentoItemWizardBreadcrumb($nMovCodigo,$nMovItem){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas ="v_tramitacao";
            $sCampos = "*";
            $sComplemento = "Where mov_codigo = ".$nMovCodigo."  And mov_item=".$nMovItem." ORDER BY data DESC LIMIT 1";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultarLista($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyMovimentoItemProtocolo da base de dados
         *
         * @return MnyMovimentoItemProtocolo[]
         */
        public function recuperarTodosMnyMovimentoItemProtocoloPorGrupoUsuario($nCodGrupoUsuario,$nEmpCodigo){
            $oMnyMovimentoItemProtocolo = new MnyMovimentoItemProtocolo();
            $oPersistencia = new Persistencia($oMnyMovimentoItemProtocolo);
            $sTabelas = "mny_movimento_item_protocolo PROTOCOLO";
            $sCampos = "DISTINCT PROTOCOLO.mov_codigo,PROTOCOLO.mov_item";
            $sComplemento = "INNER JOIN mny_movimento MOVIMENTO ON MOVIMENTO.mov_codigo = PROTOCOLO.mov_codigo AND MOVIMENTO.emp_codigo =".$nEmpCodigo."
                             WHERE para_cod_tipo_protocolo=".$nCodGrupoUsuario;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItemProtocolo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItemProtocolo)
                return $voMnyMovimentoItemProtocolo;
            return false;
        }
        /**
         *
         * Método para inicializar um Objeto MnyPessoaColaborador
         *
         * @return MnyPessoaColaborador
         */
        public function inicializarMnyPessoaColaborador($nPescCodigo,$sPescNome,$sPescCpf,$sPescMat,$nFunCodigo,$nUniCodigo,$sPescSal,$nSetCodigo,$dAdmissao,$dDemissao,$sValext,$sMotivo){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();

            $oMnyPessoaColaborador->setPescCodigo($nPescCodigo);
            $oMnyPessoaColaborador->setPescNome($sPescNome);
            $oMnyPessoaColaborador->setPescCpf($sPescCpf);
            $oMnyPessoaColaborador->setPescMat($sPescMat);
            $oMnyPessoaColaborador->setFunCodigo($nFunCodigo);
            $oMnyPessoaColaborador->setUniCodigo($nUniCodigo);
            $oMnyPessoaColaborador->setPescSal($sPescSal);
            $oMnyPessoaColaborador->setSetCodigo($nSetCodigo);
            $oMnyPessoaColaborador->setAdmissaoBanco($dAdmissao);
            $oMnyPessoaColaborador->setDemissaoBanco($dDemissao);
            $oMnyPessoaColaborador->setValext($sValext);
            $oMnyPessoaColaborador->setMotivo($sMotivo);
            return $oMnyPessoaColaborador;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyPessoaColaborador
         *
         * @return boolean
         */
        public function inserirMnyPessoaColaborador($oMnyPessoaColaborador){
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyPessoaColaborador
         *
         * @return boolean
         */
        public function alterarMnyPessoaColaborador($oMnyPessoaColaborador){
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyPessoaColaborador
         *
         * @return boolean
         */
        public function excluirMnyPessoaColaborador($nPescCodigo){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();

            $oMnyPessoaColaborador->setPescCodigo($nPescCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyPessoaColaborador na base de dados
         *
         * @return boolean
         */
        public function presenteMnyPessoaColaborador($nPescCodigo){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();

            $oMnyPessoaColaborador->setPescCodigo($nPescCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPessoaColaborador da base de dados
         *
         * @return MnyPessoaColaborador
         */
        public function recuperarUmMnyPessoaColaborador($nPescCodigo){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            $sTabelas = "mny_pessoa_colaborador";
            $sCampos = "*";
            $sComplemento = " WHERE pesc_codigo = $nPescCodigo";
            $voMnyPessoaColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaColaborador)
                return $voMnyPessoaColaborador[0];
            return false;
        }


        /**
         *
         * Método para recuperar um objeto MnyPessoaColaborador da base de dados
         *
         * @return MnyPessoaColaborador
         */
        public function recuperarUmMnyPessoaColaboradorPorCPF($nPescCpf){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            $sTabelas = "mny_pessoa_colaborador";
            $sCampos = "count(*) as pesc_cpf";
            $sComplemento = " WHERE pesc_cpf = '".$nPescCpf."'";
            $voMnyPessoaColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaColaborador)
                return $voMnyPessoaColaborador[0];
            return false;
        }






        /**
         *
         * Método para recuperar um vetor de objetos MnyPessoaColaborador da base de dados
         *
         * @return MnyPessoaColaborador[]
         */
        public function recuperarTodosMnyPessoaColaborador(){
            $oMnyPessoaColaborador = new MnyPessoaColaborador();
            $oPersistencia = new Persistencia($oMnyPessoaColaborador);
            $sTabelas = "mny_pessoa_colaborador";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyPessoaColaborador = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaColaborador)
                return $voMnyPessoaColaborador;
            return false;
        }



        /**
         *
         * Método para inicializar um Objeto MnyPessoaFisica
         *
         * @return MnyPessoaFisica
         */
        public function inicializarMnyPessoaFisica($nPesfisCodigo,$sPesfisNome,$sPesfisCpf){
            $oMnyPessoaFisica = new MnyPessoaFisica();

            $oMnyPessoaFisica->setPesfisCodigo($nPesfisCodigo);
            $oMnyPessoaFisica->setPesfisNome($sPesfisNome);
            $oMnyPessoaFisica->setPesfisCpf($sPesfisCpf);
            return $oMnyPessoaFisica;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyPessoaFisica
         *
         * @return boolean
         */
        public function inserirMnyPessoaFisica($oMnyPessoaFisica){
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyPessoaFisica
         *
         * @return boolean
         */
        public function alterarMnyPessoaFisica($oMnyPessoaFisica){
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyPessoaFisica
         *
         * @return boolean
         */
        public function excluirMnyPessoaFisica($nPesfisCodigo){
            $oMnyPessoaFisica = new MnyPessoaFisica();

            $oMnyPessoaFisica->setPesfisCodigo($nPesfisCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyPessoaFisica na base de dados
         *
         * @return boolean
         */
        public function presenteMnyPessoaFisica($nPesfisCodigo){
            $oMnyPessoaFisica = new MnyPessoaFisica();

            $oMnyPessoaFisica->setPesfisCodigo($nPesfisCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPessoaFisica da base de dados
         *
         * @return MnyPessoaFisica
         */
        public function recuperarUmMnyPessoaFisica($nPesfisCodigo){
            $oMnyPessoaFisica = new MnyPessoaFisica();
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            $sTabelas = "mny_pessoa_fisica";
            $sCampos = "*";
            $sComplemento = " WHERE pesfis_codigo = $nPesfisCodigo";
            $voMnyPessoaFisica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaFisica)
                return $voMnyPessoaFisica[0];
            return false;
        }



        /**
         *
         * Método para recuperar um objeto MnyPessoaJuridica da base de dados
         *
         * @return MnyPessoaJuridica
         */
        public function recuperarUmMnyPessoaFisicaPorCPF($nPesfCpf){
            $oMnyPessoaFisica = new MnyPessoaFisica();
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            $sTabelas = "mny_pessoa_fisica";
            $sCampos = "count(*) as pesfis_cpf";
            $sComplemento = " WHERE pesfis_cpf = '".$nPesfCpf."'";
            $voMnyPessoaFisica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaFisica)
                return $voMnyPessoaFisica[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyPessoaFisica da base de dados
         *
         * @return MnyPessoaFisica[]
         */
        public function recuperarTodosMnyPessoaFisica(){
            $oMnyPessoaFisica = new MnyPessoaFisica();
            $oPersistencia = new Persistencia($oMnyPessoaFisica);
            $sTabelas = "mny_pessoa_fisica";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyPessoaFisica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaFisica)
                return $voMnyPessoaFisica;
            return false;
        }



        /**
         *
         * Método para inicializar um Objeto MnyPessoaGeral
         *
         * @return MnyPessoaGeral
         */
        public function inicializarMnyPessoaGeral($nPesgCodigo,$nTipCodigo,$nCodStatus,$sPesgEndLogra,$sPesgEndBairro,$sPesgEndCep,$sCidade,$sEstado,$sPesgEmail,$sPesFones,$sPesgInc,$sPesgAlt,$nBcoCodigo,$nTipoconCod,$sPesgBcoAgencia,$sPesgBcoConta,$nAtivo){
            $oMnyPessoaGeral = new MnyPessoaGeral();

            $oMnyPessoaGeral->setPesgCodigo($nPesgCodigo);
            $oMnyPessoaGeral->setTipCodigo($nTipCodigo);
            $oMnyPessoaGeral->setCodStatus($nCodStatus);
            $oMnyPessoaGeral->setPesgEndLogra($sPesgEndLogra);
            $oMnyPessoaGeral->setPesgEndBairro($sPesgEndBairro);
            $oMnyPessoaGeral->setPesgEndCep($sPesgEndCep);
            $oMnyPessoaGeral->setCidade($sCidade);
            $oMnyPessoaGeral->setEstado($sEstado);
            $oMnyPessoaGeral->setPesgEmail($sPesgEmail);
            $oMnyPessoaGeral->setPesFones($sPesFones);
            $oMnyPessoaGeral->setPesgInc($sPesgInc);
            $oMnyPessoaGeral->setPesgAlt($sPesgAlt);
            $oMnyPessoaGeral->setBcoCodigo($nBcoCodigo);
            $oMnyPessoaGeral->setTipoconCod($nTipoconCod);
            $oMnyPessoaGeral->setPesgBcoAgencia($sPesgBcoAgencia);
            $oMnyPessoaGeral->setPesgBcoConta($sPesgBcoConta);
            $oMnyPessoaGeral->setAtivo($nAtivo);
            return $oMnyPessoaGeral;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyPessoaGeral
         *
         * @return boolean
         */
        public function inserirMnyPessoaGeral($oMnyPessoaGeral){
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyPessoaGeral
         *
         * @return boolean
         */
        public function alterarMnyPessoaGeral($oMnyPessoaGeral){
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyPessoaGeral
         *
         * @return boolean
         */
        public function excluirMnyPessoaGeral($nPesgCodigo){
            $oMnyPessoaGeral = new MnyPessoaGeral();

            $oMnyPessoaGeral->setPesgCodigo($nPesgCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
                $bExcluir = $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        public function excluirFisicamenteMnyPessoaGeral($nPesgCodigo){
            $oMnyPessoaGeral = new MnyPessoaGeral();
            $oMnyPessoaGeral->setPesgCodigo($nPesgCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
                $bExcluir = $oPersistencia->excluirFisicamente();



            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyPessoaGeral na base de dados
         *
         * @return boolean
         */
        public function presenteMnyPessoaGeral($nPesgCodigo){
            $oMnyPessoaGeral = new MnyPessoaGeral();

            $oMnyPessoaGeral->setPesgCodigo($nPesgCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPessoaGeral da base de dados
         *
         * @return MnyPessoaGeral
         */
        public function recuperarUmMnyPessoaGeral($nPesgCodigo){
            $oMnyPessoaGeral = new MnyPessoaGeral();
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
            $sTabelas = "mny_pessoa_geral";
            $sCampos = "*";
            $sComplemento = " WHERE pesg_codigo = $nPesgCodigo";
            $voMnyPessoaGeral = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaGeral)
                return $voMnyPessoaGeral[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyPessoaGeral da base de dados
         *
         * @return MnyPessoaGeral[]
         */
        public function recuperarTodosMnyPessoaGeral(){
            $oMnyPessoaGeral = new MnyPessoaGeral();
            $oPersistencia = new Persistencia($oMnyPessoaGeral);
            $sTabelas = "mny_pessoa_geral";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyPessoaGeral = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaGeral)
                return $voMnyPessoaGeral;
            return false;
        }


        /**
         *
         * Método para inicializar um Objeto MnyPessoaJuridica
         *
         * @return MnyPessoaJuridica
         */
        public function inicializarMnyPessoaJuridica($nPesjurCodigo,$sPesjurRazao,$sPesjurNome,$sPesjurCnpj,$sPesjurInsmun,$sPesjurInsest,$nCodMatriz){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();

            $oMnyPessoaJuridica->setPesjurCodigo($nPesjurCodigo);
            $oMnyPessoaJuridica->setPesjurRazao($sPesjurRazao);
            $oMnyPessoaJuridica->setPesjurNome($sPesjurNome);
            $oMnyPessoaJuridica->setPesjurCnpj($sPesjurCnpj);
            $oMnyPessoaJuridica->setPesjurInsmun($sPesjurInsmun);
            $oMnyPessoaJuridica->setPesjurInsest($sPesjurInsest);
            $oMnyPessoaJuridica->setCodMatriz($nCodMatriz);
            return $oMnyPessoaJuridica;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyPessoaJuridica
         *
         * @return boolean
         */
        public function inserirMnyPessoaJuridica($oMnyPessoaJuridica){
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyPessoaJuridica
         *
         * @return boolean
         */
        public function alterarMnyPessoaJuridica($oMnyPessoaJuridica){
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyPessoaJuridica
         *
         * @return boolean
         */
        public function excluirMnyPessoaJuridica($nPesjurCodigo){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();

            $oMnyPessoaJuridica->setPesjurCodigo($nPesjurCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyPessoaJuridica na base de dados
         *
         * @return boolean
         */
        public function presenteMnyPessoaJuridica($nPesjurCodigo){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();

            $oMnyPessoaJuridica->setPesjurCodigo($nPesjurCodigo);
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPessoaJuridica da base de dados
         *
         * @return MnyPessoaJuridica
         */
        public function recuperarUmMnyPessoaJuridica($nPesjurCodigo){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            $sTabelas = "mny_pessoa_juridica";
            $sCampos = "*";
            $sComplemento = " WHERE pesjur_codigo = $nPesjurCodigo";
            $voMnyPessoaJuridica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaJuridica)
                return $voMnyPessoaJuridica[0];
            return false;
        }


        /**
         *
         * Método para recuperar um objeto MnyPessoaJuridica da base de dados
         *
         * @return MnyPessoaJuridica
         */
        public function recuperarUmMnyPessoaJuridicaPorCnpj($nPesjurCnpj){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            $sTabelas = "mny_pessoa_juridica";
            $sCampos = "count(*) as pesjur_cnpj";
            $sComplemento = " WHERE pesjur_cnpj = '".$nPesjurCnpj."'";
            $voMnyPessoaJuridica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaJuridica)
                return $voMnyPessoaJuridica[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyPessoaJuridica da base de dados
         *
         * @return MnyPessoaJuridica[]
         */
        public function recuperarTodosMnyPessoaJuridica(){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            $sTabelas = "mny_pessoa_juridica";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyPessoaJuridica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaJuridica)
                return $voMnyPessoaJuridica;
            return false;
        }

            public function recuperarTodosMnyPessoaJuridicaPorMatriz($nCodPessoa){
            $oMnyPessoaJuridica = new MnyPessoaJuridica();
            $oPersistencia = new Persistencia($oMnyPessoaJuridica);
            $sTabelas = "mny_pessoa_juridica";
            $sCampos = "*";
            $sComplemento = "INNER JOIN mny_pessoa_geral on mny_pessoa_geral.pesg_codigo= mny_pessoa_juridica.pesjur_codigo WHERE mny_pessoa_geral.ativo=1 and cod_matriz=".$nCodPessoa;
            $voMnyPessoaJuridica = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPessoaJuridica)
                return $voMnyPessoaJuridica;
            return false;
        }



        /**
         *
         * Método para inicializar um Objeto MnyPlanoContas
         *
         * @return MnyPlanoContas
         */
        public function inicializarMnyPlanoContas($nPlanoContasCodigo,$sCodigo,$sDescricao,$nVisivel,$sUsuInc,$sUsuAlt,$nAtivo){
            $oMnyPlanoContas = new MnyPlanoContas();

            $oMnyPlanoContas->setPlanoContasCodigo($nPlanoContasCodigo);
            $oMnyPlanoContas->setCodigo($sCodigo);
            $oMnyPlanoContas->setDescricao($sDescricao);
            $oMnyPlanoContas->setUsuInc($sUsuInc);
            $oMnyPlanoContas->setUsuAlt($sUsuAlt);
            $oMnyPlanoContas->setAtivo($nAtivo);
            $oMnyPlanoContas->setVisivel($nVisivel);
            return $oMnyPlanoContas;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyPlanoContas
         *
         * @return boolean
         */
        public function inserirMnyPlanoContas($oMnyPlanoContas){
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyPlanoContas
         *
         * @return boolean
         */
        public function alterarMnyPlanoContas($oMnyPlanoContas){
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyPlanoContas
         *
         * @return boolean
         */
        public function excluirMnyPlanoContas($nPlanoContasCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();

            $oMnyPlanoContas->setPlanoContasCodigo($nPlanoContasCodigo);
            $oPersistencia = new Persistencia($oMnyPlanoContas);
                $bExcluir = $oPersistencia->excluir();
                //$bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyPlanoContas na base de dados
         *
         * @return boolean
         */
        public function presenteMnyPlanoContas($nPlanoContasCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();

            $oMnyPlanoContas->setPlanoContasCodigo($nPlanoContasCodigo);
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            if($oPersistencia->presente())
                    return true;
            return false;
        }
        /**
         *
         * Método para verificar se existe um Objeto MnyPlanoContas na base de dados
         *
         * @return boolean
         */
        public function preparaUnidadeRelatorio($nUniCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $sProc = "sp_contas_pagar";
            $nParam = $nUniCodigo;
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            if($oPersistencia->consultarProcedure($sProc,$nParam))
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPlanoContas da base de dados
         *
         * @return MnyPlanoContas
         */
        public function recuperarUmMnyPlanoContas($nPlanoContasCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "*";
             $sComplemento = " WHERE plano_contas_codigo = $nPlanoContasCodigo";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyPlanoContas da base de dados
         *
         * @return MnyPlanoContas
         */
        public function recuperarUmMnyPlanoContasPorContaCorrente($nCcrCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas pc";
            $sCampos = "pc.descricao";
            $sComplemento = "Inner Join mny_conta_corrente_unidade c  On pc.plano_contas_codigo=c.cod_unidade
                             Inner Join mny_conta_corrente cc On cc.ccr_codigo = c.ccr_codigo
                             Where cc.ccr_codigo=$nCcrCodigo And cc.ccr_conta='999'";
            //echo "Select $sCampos From $sTabelas $sComplemento";die();
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyPlanoContas da base de dados
         *
         * @return MnyPlanoContas[]
         */
        public function recuperarTodosMnyPlanoContas(){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "plano_contas_codigo, codigo,CAST(REPLACE(codigo,'.','') as UNSIGNED) as ordem, descricao,visivel,usu_inc,usu_alt,ativo";

            $sComplemento = " WHERE ativo=1";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }

        public function recuperarTodosMnyPlanoContasCENTRAL(){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "plano_contas_codigo";
            $sComplemento = " WHERE ativo=1 And descricao like'%CENTRAL -%'";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }
       public function recuperarTodosMnyPlanoContasCENTRALObjeto(){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "*";
            $sComplemento = " WHERE ativo=1 And descricao like'%CENTRAL -%'";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }

    public function recuperarTodosMnyPlanoContasPorGrupo($nGrupo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "plano_contas_codigo, codigo,CAST(REPLACE(codigo,'.','') as UNSIGNED) as ordem, descricao,visivel,usu_inc,usu_alt,ativo";
            $sComplemento = "WHERE ativo=1 AND visivel=1 AND codigo LIKE '".$nGrupo.".%' order by ordem ";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
    //echo  "SELECT $sCampos FROM $sTabelas " . $sComplemento;
    //die();
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }

        public function recuperarTodosMnyPlanoContasPorUnidade(){
                    $oMnyPlanoContas = new MnyPlanoContas();
                    $oPersistencia = new Persistencia($oMnyPlanoContas);
                    $sTabelas = "mny_plano_contas";
                    $nGrupo = '8';
                    $sCampos = "plano_contas_codigo, CONCAT(SUBSTR(codigo,5,LENGTH(codigo)),' ',descricao) as descricao";
                    $sComplemento = "WHERE ativo=1 AND visivel=1 AND codigo LIKE '".$nGrupo.".%' order by descricao ";
                    $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
           // echo  "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
                    if($voMnyPlanoContas)
                        return $voMnyPlanoContas;
                    return false;
        }

       public function recuperarTodosMnyPlanoContasPorCentral(){
                    $oMnyPlanoContas = new MnyPlanoContas();
                    $oPersistencia = new Persistencia($oMnyPlanoContas);
                    $sTabelas = "mny_plano_contas";
                    $nGrupo = '8';
                    $sCampos = "plano_contas_codigo, CONCAT(SUBSTR(codigo,5,LENGTH(codigo)),' ',descricao) as descricao";
                    $sComplemento = "WHERE ativo=1 AND visivel=1 AND codigo LIKE '".$nGrupo.".%' AND descricao LIKE 'CENTRAL%'order by descricao ";
                    $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
           // echo  "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
                    if($voMnyPlanoContas)
                        return $voMnyPlanoContas;
                    return false;
        }


        public function recuperarTodosMnyPlanoContasUnidadePorEmpresa($nEmpCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "plano_contas_codigo, codigo,CAST(REPLACE(codigo,'.','') as UNSIGNED) as ordem, descricao,visivel,usu_inc,usu_alt,ativo";
            $sComplemento = " INNER JOIN acesso_unidade_empresa ue ON ue.cod_unidade = mny_plano_contas.plano_contas_codigo and ue.emp_codigo=".$nEmpCodigo."
                             WHERE ativo=1 AND visivel=1 AND codigo LIKE '8.%'  AND descricao NOT like '%CENTRAL%'
                            UNION
                            SELECT plano_contas_codigo, codigo,CAST(REPLACE(codigo,'.','') as UNSIGNED) as ordem, descricao,visivel,usu_inc,usu_alt,mny_plano_contas.ativo
                            FROM mny_plano_contas
                            INNER JOIN sys_empresa ON sys_empresa.uni_codigo = mny_plano_contas.plano_contas_codigo
                            WHERE emp_codigo=".$nEmpCodigo." ORDER BY ordem ASC ";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
   // echo  "SELECT $sCampos FROM $sTabelas " . $sComplemento;
  //  die();
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }

        public function recuperarTodosMnyPlanoContasTransf($nGrupo1, $nGrupo2){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "plano_contas_codigo, codigo,CAST(REPLACE(codigo,'.','') as UNSIGNED) as ordem, descricao,visivel,usu_inc,usu_alt,ativo";
            $sComplemento = "WHERE ativo=1 AND visivel=1 AND (codigo LIKE '".$nGrupo1.".%' OR codigo LIKE '".$nGrupo2.".%') order by ordem ";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
    //echo  "SELECT $sCampos FROM $sTabelas " . $sComplemento;
    //die();
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }


    /**
         *
         * Método para recuperar um vetor de objetos MnyPlanoContas da base de dados
         *
         * @return MnyPlanoContas[]
         */

         public function recuperarTodosMnyPlanoContasCabeca($nCodigo){
            $oMnyPlanoContas = new MnyPlanoContas();
            $oPersistencia = new Persistencia($oMnyPlanoContas);
            $sTabelas = "mny_plano_contas";
            $sCampos = "*";
            $sComplemento = "WHERE ativo=1 and (codigo like '".$nCodigo."_' or codigo like '".$nCodigo."__')";
            $voMnyPlanoContas = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyPlanoContas)
                return $voMnyPlanoContas;
            return false;
        }

         /**
         *
         * Método para inicializar um Objeto MnyContratoArquivo
         *
         * @return MnyContratoArquivo
         */
        public function inicializarMnyContratoArquivo($nCodContratoArquivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem,$nCodContratoDocTipo,$sNome,$sRealizadoPor,$nAtivo){
            $oMnyContratoArquivo = new MnyContratoArquivo();

            $oMnyContratoArquivo->setCodContratoArquivo($nCodContratoArquivo);
            $oMnyContratoArquivo->setContratoCodigo($nContratoCodigo);
            $oMnyContratoArquivo->setCodConciliacao($nCodConciliacao);
            $oMnyContratoArquivo->setMovCodigo($nMovCodigo);
            $oMnyContratoArquivo->setMovItem($nMovItem);
            $oMnyContratoArquivo->setCodContratoDocTipo($nCodContratoDocTipo);
            $oMnyContratoArquivo->setNome($sNome);
            $oMnyContratoArquivo->setRealizadoPor($sRealizadoPor);
            $oMnyContratoArquivo->setAtivo($nAtivo);
            return $oMnyContratoArquivo;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function inserirMnyContratoArquivo($oMnyContratoArquivo){
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function alterarMnyContratoArquivo($oMnyContratoArquivo){
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function excluirMnyContratoArquivo($nCodContratoArquivo){
            $oMnyContratoArquivo = new MnyContratoArquivo();

            $oMnyContratoArquivo->setCodContratoArquivo($nCodContratoArquivo);
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
                $bExcluir = $oPersistencia->excluirFisicamente();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function excluirTodosMnyContratoArquivoPorMovimento($nMovCodigo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $sComplemento = " WHERE mov_codigo=".$nMovCodigo;
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

            if($bExcluir)
                    return true;
            return false;

        }

     /**
         *
         * Método para excluir da base de dados um Objeto MnyContratoArquivo
         *
         * @return boolean
         */
        public function excluirTodosMnyContratoArquivoPorMovimentoItem($nMovCodigo,$nMovItem){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $sComplemento = " WHERE mov_codigo=".$nMovCodigo . "AND mov_item= ".$nMovItem;
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);

            if($bExcluir)
                    return true;
            return false;

        }
        /**
         *
         * Método para verificar se existe um Objeto MnyContratoArquivo na base de dados
         *
         * @return boolean
         */
        public function presenteMnyContratoArquivo($nCodContratoArquivo){
            $oMnyContratoArquivo = new MnyContratoArquivo();

            $oMnyContratoArquivo->setCodContratoArquivo($nCodContratoArquivo);
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo
         */
        public function recuperarUmMnyContratoArquivo($nCodContratoArquivo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo";
            $sCampos = "*";
            $sComplemento = " WHERE cod_arquivo = " .  $nCodContratoArquivo;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivo(){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_contrato_arquivo";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }
        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivoDetalheMovimento($nMovCodigo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo arq";
            $sCampos = "arq.nome, doc_tipo.descricao as  cod_contrato_doc_tipo";
            $sComplemento = "INNER JOIN mny_movimento mov ON mov.mov_contrato = arq.contrato_codigo
            Inner Join mny_contrato_doc_tipo doc_tipo On doc_tipo.cod_contrato_doc_tipo = arq.cod_contrato_doc_tipo
            WHERE mov.mov_codigo = ".$nMovCodigo."  And ISNULL(arq.mov_codigo)";
    //        echo "Select $sCampos From $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }

      /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivoDetalheMovimentoDespesaCaixa($nMovCodigo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo arq";
            $sCampos = "arq.nome, doc_tipo.descricao as  cod_contrato_doc_tipo";
            $sComplemento = "INNER JOIN mny_movimento mov ON mov.mov_codigo = arq.mov_codigo
            Inner Join mny_contrato_doc_tipo doc_tipo On doc_tipo.cod_contrato_doc_tipo = arq.cod_contrato_doc_tipo
            WHERE mov.mov_codigo = ".$nMovCodigo;
            //echo "Select $sCampos From $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }
        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivoPorContrato($nCodContrato){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo";
            $sCampos = "*";
            $sComplemento = "where contrato_codigo= " . $nCodContrato;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }
        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivoPorContratoTipoArquivo($nCodContrato, $nContratoDocTipo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo";
            $sCampos = "cod_arquivo,mny_contrato_pessoa.contrato_codigo AS contrato_codigo,mny_contrato_doc_tipo.cod_contrato_doc_tipo as cod_contrato_doc_tipo,nome,realizado_por,item as ativo";
            $sComplemento = "INNER JOIN mny_contrato_pessoa ON mny_arquivo.contrato_codigo = mny_contrato_pessoa.contrato_codigo
                             INNER JOIN mny_contrato_doc_tipo ON mny_contrato_doc_tipo.cod_contrato_doc_tipo = mny_arquivo.cod_contrato_doc_tipo
                             WHERE mny_contrato_pessoa.contrato_codigo=" .  $nCodContrato .   " AND  mny_arquivo.cod_contrato_doc_tipo=" . $nContratoDocTipo ;

        //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
        //die();
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }

      public function recuperarTodosMnyContratoArquivoPorContratoTipoArquivoMovimento($nCodContrato, $nContratoDocTipo, $nMovCodigo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas ="mny_arquivo";
            $sCampos = "cod_arquivo,mny_contrato_pessoa.contrato_codigo AS contrato_codigo,mny_contrato_doc_tipo.cod_contrato_doc_tipo as cod_contrato_doc_tipo,nome,realizado_por,item as ativo";
            $sComplemento = "INNER JOIN mny_contrato_pessoa ON mny_arquivo.contrato_codigo = mny_contrato_pessoa.contrato_codigo
                             INNER JOIN mny_contrato_doc_tipo ON mny_contrato_doc_tipo.cod_contrato_doc_tipo = mny_arquivo.cod_contrato_doc_tipo
                             WHERE mny_contrato_pessoa.contrato_codigo=" .  $nCodContrato .   " AND  mny_arquivo.cod_contrato_doc_tipo=" . $nContratoDocTipo ." AND mov_codigo= ". $nMovCodigo;
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }

        public function recuperarTodosMnyContratoArquivoPorMovimento($nMovCodigo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas ="mny_arquivo";
            $sCampos = "*";
            $sComplemento = "WHERE  mov_codigo= ". $nMovCodigo ." AND ativo = 1";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }
        //temporario
        public function recuperarTodosMnyContratoArquivoPorTipoArquivoTransferencia(){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas ="mny_arquivo arq";
            $sCampos = "*";
            $sComplemento = " INNER JOIN mny_transferencia t ON (arq.mov_codigo=t.mov_codigo_origem AND arq.mov_item=t.mov_item_origem)
                             WHERE arq.cod_contrato_doc_tipo IN (19,20) AND CONCAT(mov_codigo_origem,'.',mov_item_origem) in(SELECT concat(mov_codigo,'.',mov_item) FROM mny_arquivo WHERE cod_contrato_doc_tipo IN (19,20)) AND t.ativo=1 AND arq.ativo=1 AND mov_codigo not in (44102,44337,35360,43627,44356)
                            and arq.cod_arquivo not in(select cod_conciliacao from sys_arquivo)  order by t.cod_transferencia asc limit 3
                            ";
                     //       and cod_transferencia > ". $nCodTransferencia ."LIMIT 2";

            //$sComplemento .= " and mov_codigo_origem=44123";

            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;

            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }
        //fim temporario
        public function recuperarTodosMnyContratoArquivoPorFA($nMovCodigo,$nMovItem){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas ="mny_arquivo";
            $sCampos = "*";
            $sComplemento = "WHERE  mov_codigo= ". $nMovCodigo ." AND mov_item= ". $nMovItem ." AND ativo = 1";
            //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            //die();
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }

        public function recuperarTodosMnyContratoArquivoPorMovimentoDocTipo($nMovCodigo, $nCodContratoDocTipo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas ="mny_arquivo";
            $sCampos = "*";
            $sComplemento = "WHERE  mov_codigo= ". $nMovCodigo ." AND cod_contrato_doc_tipo = ".$nCodContratoDocTipo." AND ativo = 1";
    //		echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
    //      die();
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        public function recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($nCodContrato, $nContratoDocTipo){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_arquivo";
            $sCampos = "count(*) as realizado_por";

            $nContratoCodigo = ($nCodContrato) ? $nCodContrato : NULL;

            $sComplemento = "INNER JOIN mny_contrato_pessoa ON mny_arquivo.contrato_codigo = mny_contrato_pessoa.contrato_codigo
                             INNER JOIN mny_contrato_doc_tipo ON mny_contrato_doc_tipo.cod_contrato_doc_tipo = mny_arquivo.cod_contrato_doc_tipo
                             WHERE mny_contrato_pessoa.contrato_codigo=" . $nContratoCodigo  . " AND  mny_arquivo.cod_contrato_doc_tipo=" . $nContratoDocTipo ;

            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyContratoArquivo da base de dados
         *
         * @return MnyContratoArquivo[]
         */
        /*public function recuperarUmMnyContratoPessoaSaldo($nCodContrato){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "(valor_contrato - SUM(mov_valor_parcela)) as ativo";
            $sComplemento = "	inner join mny_movimento ON mny_movimento.mov_contrato = mny_contrato_pessoa.contrato_codigo and mny_movimento.ativo=1
                                inner join mny_movimento_item ON mny_movimento.mov_codigo = mny_movimento_item.mov_codigo and mny_movimento_item.ativo=1 and tip_ace_codigo=169
                                where contrato_codigo=" .  $nCodContrato;
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo[0];
            return false;
        }*/

        //altera na data 14/04/2016
        /* 	public function recuperarUmMnyContratoPessoaSaldo($nCodContrato){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "IFNULL((SELECT(	valor_contrato - SUM(mov_valor_parcela))
                        FROM
                            mny_contrato_pessoa
                        INNER JOIN mny_movimento ON mny_movimento.mov_contrato = mny_contrato_pessoa.contrato_codigo
                        AND mny_movimento.ativo = 1
                        INNER JOIN mny_movimento_item ON mny_movimento.mov_codigo = mny_movimento_item.mov_codigo
                        AND mny_movimento_item.ativo = 1
                        AND tip_ace_codigo = 169
                        WHERE
                            contrato_codigo = ".$nCodContrato."), (select valor_contrato from mny_contrato_pessoa where contrato_codigo=".$nCodContrato.")) as ativo";
            $sComplemento = "";
    //echo "SELECT $sCampos FROM  $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo[0];
            return false;
        }


        public function recuperarUmMnyContratoPessoaSaldo($nCodContrato){
            $oMnyContratoArquivo = new MnyContratoArquivo();
            $oPersistencia = new Persistencia($oMnyContratoArquivo);
            $sTabelas = "mny_contrato_pessoa";
            $sCampos = "IFNULL((SELECT(	valor_contrato - SUM(mov_valor_parcela))
                        FROM
                            mny_contrato_pessoa
                        INNER JOIN mny_movimento ON mny_movimento.mov_contrato = mny_contrato_pessoa.contrato_codigo AND mny_movimento.ativo = 1
                        INNER JOIN mny_movimento_item ON mny_movimento.mov_codigo = mny_movimento_item.mov_codigo AND mny_movimento_item.ativo = 1	AND tip_ace_codigo = 169
                        WHERE
                            contrato_codigo =".$nCodContrato . "), (select valor_contrato from mny_contrato_pessoa where contrato_codigo= ".$nCodContrato . ")) as ativo";
            $sComplemento = "";
    //echo "SELECT $sCampos FROM  $sTabelas $sComplemento";
            $voMnyContratoArquivo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyContratoArquivo)
                return $voMnyContratoArquivo[0];
            return false;
        }

        */
        public function recuperarUmMnyContratoPessoaSaldo($nCodContrato){
            $oMnyContratoPessoa = new MnyContratoPessoa();
            $oPersistencia = new Persistencia($oMnyContratoPessoa);
            $sTabelas = "sp_calc_saldo_contrato";
            $sComplemento = $nCodContrato;
            //echo "CALL $sTabelas($sComplemento)";
            $voMnyContratoPessoa = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
            if($voMnyContratoPessoa)
                return $voMnyContratoPessoa[0];
            return false;
        }

        /**
         *
         * Método para inicializar um Objeto MnySaldoDiario
         *
         * @return MnySaldoDiario
         */
        public function inicializarMnySaldoDiario($nCodSaldoDiario,$nCodTipoCaixa,$nCcrCodigo,$nCodUnidade,$dData,$nValorFinal,$nValorInicial,$sRealizadoPor,$nTipoSaldo,$oExtratoBancario,$nAtivo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oMnySaldoDiario->setCodSaldoDiario($nCodSaldoDiario);
            $oMnySaldoDiario->setCodTipoCaixa($nCodTipoCaixa);
            $oMnySaldoDiario->setCcrCodigo(($nCcrCodigo) ? $nCcrCodigo : "NULL" );
            $oMnySaldoDiario->setCodUnidade($nCodUnidade);
            $oMnySaldoDiario->setDataBanco($dData);
            $oMnySaldoDiario->setValorFinalBanco($nValorFinal);
            $oMnySaldoDiario->setValorInicialBanco($nValorInicial);
            $oMnySaldoDiario->setRealizadoPor($sRealizadoPor);
            $oMnySaldoDiario->setTipoSaldo($nTipoSaldo);
            $oMnySaldoDiario->setExtratoBancario($oExtratoBancario);
            $oMnySaldoDiario->setAtivo($nAtivo);
            return $oMnySaldoDiario;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnySaldoDiario
         *
         * @return boolean
         */
        public function inserirMnySaldoDiario($oMnySaldoDiario){
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnySaldoDiario
         *
         * @return boolean
         */
        public function alterarMnySaldoDiario($oMnySaldoDiario){
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnySaldoDiario
         *
         * @return boolean
         */
        public function excluirMnySaldoDiario($nCodSaldoDiario){
            $oMnySaldoDiario = new MnySaldoDiario();

            $oMnySaldoDiario->setCodSaldoDiario($nCodSaldoDiario);
            $oPersistencia = new Persistencia($oMnySaldoDiario);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnySaldoDiario na base de dados
         *
         * @return boolean
         */
        public function presenteMnySaldoDiario($nCodSaldoDiario){
            $oMnySaldoDiario = new MnySaldoDiario();

            $oMnySaldoDiario->setCodSaldoDiario($nCodSaldoDiario);
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario
         */
        public function recuperarUmMnySaldoDiario($nCodSaldoDiario){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = " WHERE cod_saldo_diario =" .  $nCodSaldoDiario;
            // echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        public function recuperarUmMnySaldoDiarioPorConta($nCodContaCorrente){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = " WHERE ccr_codigo = $nCodContaCorrente";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        public function recuperarUmMnySaldoDiarioPorDataUnidadeConta($dDtada,$nCodUnidade,$nCodContaCorrente){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = " WHERE data= $dDataConciliacao AND cod_unidade= $nCodUnidade AND ccr_codigo = $nCodContaCorrente";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }



            /**
         *
         * Método para recuperar um objeto MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario
         */
        public function recuperarUmMnySaldoDiarioPorUnidade($nCodUnidade, $nCodTipoCaixa){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = " WHERE cod_unidade =". $nCodUnidade ." AND cod_tipo_caixa =". $nCodTipoCaixa;
            //echo "SELECT $sCampos  FROM $sTabelas $sComplemento" ;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario
         */
        public function recuperarUmMnySaldoDiarioPorUnidadeCaixinha($nCodUnidade){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE ccr_codigo in (SELECT
                                                ccr_codigo
                                            FROM
                                                mny_conta_corrente cc
                                            INNER JOIN mny_plano_contas pc ON pc.plano_contas_codigo = cc.cod_unidade
                                            WHERE
                                                ccr_conta = '999' And cc.cod_unidade=".$nCodUnidade."
                                            UNION
                                            SELECT
                                                c.ccr_codigo
                                            FROM
                                              mny_conta_corrente_unidade c
                                            INNER JOIN mny_plano_contas pc2 ON pc2.plano_contas_codigo = c.cod_unidade
                                            INNER JOIN mny_conta_corrente cc2 ON cc2.ccr_codigo = c.ccr_codigo
                                            WHERE
                                                cc2.ccr_conta = '999' And c.cod_unidade=".$nCodUnidade."
                                            )
                                            ORDER BY data DESC  limit 1
                                ";
            //echo "SELECT $sCampos  FROM $sTabelas $sComplemento" ;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }



        //CONCILIAÇÃO CONTA / UNIDADE
        public function recuperarUmMnySaldoDiarioPorUnidadeContaData($nCodUnidade, $nCcrCodigo, $dData){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "cod_saldo_diario";
            $sComplemento = " WHERE data=DATE_SUB('".$dData."',INTERVAL 1 DAY) and cod_unidade=".$nCodUnidade." and ccr_codigo=".$nCcrCodigo;
            //echo "SELECT $sCampos  FROM $sTabelas $sComplemento" ;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return true;
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarUmMnySaldoDiarioAnteriorPorContaData($nCcrCodigo, $dData){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "cod_saldo_diario";

            if($dData == '2018-01-01'){
                $sComplemento = "WHERE data=DATE_SUB('".$dData."',INTERVAL 1 DAY) and ccr_codigo=".$nCcrCodigo;
            }elseif($dData < '2018-06-01'){
                $sComplemento = "WHERE data=DATE_SUB('".$dData."',INTERVAL 1 DAY) and ccr_codigo=".$nCcrCodigo."  And year(data)=year('".$dData."')";
            }else{
               $sComplemento = "WHERE data=DATE_SUB('".$dData."',INTERVAL 1 DAY) and ccr_codigo=".$nCcrCodigo."  And year(data)=year('".$dData."') AND MONTH('".$dData."')>5";
            }

            //echo "SELECT $sCampos  FROM $sTabelas $sComplemento" ;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return true;
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarUmMnySaldoDiarioPorContaDataCortes($nCcrCodigo, $dData){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE data=(f_verifica_ultimo_saldo('".$dData."',".$nCcrCodigo .")) and ccr_codigo=".$nCcrCodigo;
            //echo  "SELECT * FROM " . $sTabelas . " " . $sComplemento;
//die();

            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarUmMnySaldoDiarioPorContaData($nCcrCodigo, $dData){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "cod_saldo_diario";
            $sComplemento = " WHERE data='".$dData."' and ccr_codigo=".$nCcrCodigo;
            //echo "SELECT $sCampos  FROM $sTabelas $sComplemento" ;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return true;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         */
        //CONCILIACAO CONTA / UNIDADE
        public function recuperarUmMnySaldoDiarioPorUnidadeTipoCaixaContaCorrente2($nCodUnidade,$nTipo, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=". $nCodUnidade ." AND cod_tipo_caixa=". $nTipo ." AND data= (SELECT MAX(data) FROM mny_saldo_diario WHERE cod_unidade=". $nCodUnidade ." AND cod_tipo_caixa=". $nTipo ." AND ccr_codigo = " .$nCcrCodigo. ")" ;
     //		echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        //CONCILIACAO CONTA
        public function recuperarUmMnySaldoDiarioPorUnidadeTipoCaixaContaCorrente($nTipo, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE  cod_tipo_caixa=". $nTipo ." AND data= (SELECT MAX(data) FROM mny_saldo_diario WHERE cod_tipo_caixa=". $nTipo ." AND ccr_codigo = " .$nCcrCodigo. ")" ;
     //		echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }


        /** bkp realizado no dia 2016-07-22
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         */
        /*public function recuperarUmMnySaldoDiarioPorUnidadeContaCorrente($dData, $nCodUnidade, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "valor_inicial as valor_inicial,(select IFNULL(valor_final,0) from mny_saldo_diario where ccr_codigo = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . " and 	data = (select max(data) from mny_saldo_diario where ccr_codigo = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . " )) -
                       ((select IFNULL(sum(mov_valor_parcela),0) from mny_conciliacao inner join mny_movimento_item on mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo and mny_movimento_item.mov_item = mny_conciliacao.mov_item
                        inner join mny_movimento on mny_movimento.mov_codigo = mny_conciliacao.mov_codigo
                        where cod_conta_corrente = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . "  and data_conciliacao = '" . $dData . "' and mny_movimento.mov_tipo = 188 AND mny_conciliacao.ativo = 1)
                        - (select IFNULL(sum(mov_valor_parcela),0) from mny_conciliacao inner join mny_movimento_item on mny_movimento_item.mov_codigo = mny_conciliacao.mov_codigo and mny_movimento_item.mov_item = mny_conciliacao.mov_item
                        inner join mny_movimento on mny_movimento.mov_codigo = mny_conciliacao.mov_codigo
                        where cod_conta_corrente = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . "  and data_conciliacao = '" . $dData . "' and mny_movimento.mov_tipo = 189 AND mny_conciliacao.ativo = 1)) as valor_final ";
            $sComplemento = "WHERE ccr_codigo = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . " and 	data = (select max(data) from mny_saldo_diario where ccr_codigo = " . $nCcrCodigo . " and cod_unidade = " . $nCodUnidade . " )" ;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }*/

            /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         */
        //CONCILIACAO CONTA / UNIDADE
        public function recuperarUmMnySaldoDiarioPorUnidadeContaCorrente($dData, $nCodUnidade, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "sp_saldo";
            //$sCampos = "valor_inicial, valor_final";
            $sComplemento = $nCcrCodigo.",".$nCodUnidade.",'".$dData."'";
            //echo "CALL $sTabelas ($sComplemento)";
            //die();
            $voMnySaldoDiario = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        //CONCILIAÇÃO CONTA
        public function recuperarUmMnySaldoDiarioPorContaCorrente($dData,  $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "sp_saldo";
            //$sCampos = "valor_inicial, valor_final";
            $sComplemento = $nCcrCodigo.",'".$dData."'";
            //echo "CALL $sTabelas ($sComplemento)";
            //die();
            $voMnySaldoDiario = $oPersistencia->consultarProcedure($sTabelas,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         */
        public function recuperarUmMnySaldoDiarioPorUnidadeTipoCaixaCcrCodigo($nCodUnidade,$nTipo, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=" . $nCodUnidade ." AND cod_tipo_caixa=". $nTipo . " AND ccr_codigo= " . $nCcrCodigo ." AND data=(SELECT MAX(data) from mny_saldo_diario WHERE cod_unidade=" . $nCodUnidade ." AND cod_tipo_caixa=". $nTipo . " AND ccr_codigo= " . $nCcrCodigo .")";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento -- saldo diario";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         * segue o fluxo normal da conciliacao sem exceção de ANO
         *
         */
        public function recuperarUmMnySaldoDiarioPorTipoContaFluxoNormal($nCcrCodigo,$dDataConciliacao){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE ccr_codigo=".$nCcrCodigo." and  data=
                            IF(
                            (SELECT tipo_saldo FROM mny_saldo_diario WHERE data='".$dDataConciliacao."' AND ccr_codigo=".$nCcrCodigo." ) <> 1
                            ,'".$dDataConciliacao."',
                            IF((SELECT	 	DISTINCT CONCILIACAO.consolidado
                                FROM
                                  mny_conciliacao CONCILIACAO
                                WHERE
                                    CONCILIACAO.cod_conta_corrente=".$nCcrCodigo." AND
                                    CONCILIACAO.data_conciliacao='" . $dDataConciliacao . "' )=1 ,
                                    '". $dDataConciliacao."',
                                 (SELECT MAX(data) FROM mny_saldo_diario WHERE ccr_codigo = ".$nCcrCodigo.") ))" ;

           // echo "SELECT $sCampos FROM $sTabelas $sComplemento  ";
           // die('ctr');
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        // concilia varios anos silmultaneamente 2017/2018
        public function recuperarUmMnySaldoDiarioPorTipoConta($nCcrCodigo,$dDataConciliacao){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE ccr_codigo=".$nCcrCodigo." and  data=
                            IF(
                            (SELECT tipo_saldo FROM mny_saldo_diario WHERE data='".$dDataConciliacao."' AND ccr_codigo=".$nCcrCodigo." ) <> 1
                            ,'".$dDataConciliacao."',
                            IF((SELECT	 	DISTINCT CONCILIACAO.consolidado
                                FROM
                                  mny_conciliacao CONCILIACAO
                                WHERE
                                    CONCILIACAO.cod_conta_corrente=".$nCcrCodigo." AND
                                    CONCILIACAO.data_conciliacao='" . $dDataConciliacao . "' )=1 ,
                                    '". $dDataConciliacao."',
                                 	if((select year('" . $dDataConciliacao . "')='2017')
                                    ,(select Max(data) From mny_saldo_diario Where ccr_codigo=".$nCcrCodigo." And year(mny_saldo_diario.data)='2017')
                                    ,(
                                        SELECT
                                            MAX(DATA)
                                        FROM
                                            mny_saldo_diario
                                        WHERE
                                            ccr_codigo = ".$nCcrCodigo."
                                    )) ))" ;

            //echo "SELECT $sCampos FROM $sTabelas $sComplemento  ";
            //die('ctr');
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

            /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiario da base de dados
         *
         * @return MnySaldoDiario[]
         */
        public function recuperarTodosMnySaldoDiario(){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "cod_saldo_diario,cod_tipo_caixa,CONCAT(ccr_conta,'-',ccr_conta_dv)as ccr_codigo, mny_plano_contas.descricao as cod_unidade,data,valor_inicial,valor_final,realizado_por,tipo_saldo";
            $sComplemento = "INNER JOIN mny_conta_corrente ON mny_saldo_diario.ccr_codigo = mny_conta_corrente.ccr_codigo
                            INNER JOIN mny_plano_contas ON mny_saldo_diario.cod_unidade = mny_plano_contas.plano_contas_codigo
                            WHERE mny_saldo_diario.ativo = 1";
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario;
            return false;
        }
        public function recuperarTodosMnySaldoDiarioUltimaData(){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "where data = (select MAX(data) from mny_saldo_diario)";
            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario;
            return false;
        }
        //CONCILIACAO DA CONTA / UNIDADE
        public function recuperarUmMnySaldoDiarioUltimaDataPorUnidadeContaCorrente($nCodUnidade, $nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=". $nCodUnidade ." AND ccr_codigo = ". $nCcrCodigo ." AND data = (select MAX(data) from mny_saldo_diario WHERE cod_unidade=". $nCodUnidade ." AND ccr_codigo = ". $nCcrCodigo .")";
            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        //CONCILIACAO DA CONTA
        public function recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($nCcrCodigo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE ccr_codigo = ". $nCcrCodigo ." AND data = (select MAX(data) from mny_saldo_diario WHERE  ccr_codigo = ". $nCcrCodigo .")";
            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }
        //CONCILIACAO DA CONTA
        public function recuperarUmMnySaldoDiarioUltimaDataPorContaCorrentePorAno($nCcrCodigo,$dData){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
       //     $sComplemento = "WHERE ccr_codigo = ". $nCcrCodigo ." AND data = (select MAX(data) from mny_saldo_diario WHERE  ccr_codigo = ". $nCcrCodigo ." And year(data) = '".$dData."')";
            $sComplemento = "WHERE ccr_codigo =  ". $nCcrCodigo ." AND DATA = (SELECT IFNULL(MAX(DATA),(SELECT MAX(DATA) FROM mny_saldo_diario WHERE ccr_codigo =  ". $nCcrCodigo ." AND MONTH(DATA)=12 and day(DATA)=31 and YEAR (DATA) = '".$dData."'-1)) FROM mny_saldo_diario WHERE ccr_codigo =  ". $nCcrCodigo ." AND YEAR (DATA) = '".$dData."')";



            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario[0];
            return false;
        }

        public function recuperarTodosMnySaldoDiarioUltimaDataPorUnidade($nCodUnidade){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=". $nCodUnidade ." AND data = (select MAX(data) from mny_saldo_diario )";
            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario;
            return false;
        }

        public function recuperarTodosMnySaldoDiarioUltimaDataPorUnidadeConta($nCodUnidade, $nCodContaCorrente){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "WHERE cod_unidade=". $nCodUnidade ." AND data = (select MAX(data) from mny_saldo_diario WHERE ccr_codigo=".$nCodContaCorrente.")";
            //echo "SELECT * FROM $sTabelas " . $sComplemento;
            //die();
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario;
            return false;
        }

         public function recuperarTodosMnySaldoDiarioPorTipo($nTipo){
            $oMnySaldoDiario = new MnySaldoDiario();
            $oPersistencia = new Persistencia($oMnySaldoDiario);
            $sTabelas = "mny_saldo_diario";
            $sCampos = "*";
            $sComplemento = "where cod_tipo_caixa = $nTipo "; // and data = CURDATE()
            $voMnySaldoDiario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiario)
                return $voMnySaldoDiario;
            return false;
        }


    /**
         *
         * Método para inicializar um Objeto MnySaldoDiarioAplicacao
         *
         * @return MnySaldoDiarioAplicacao
         */
        public function inicializarMnySaldoDiarioAplicacao($nCodSaldoDiario,$nCodAplicacao,$nValorRendimento,$nValor,$sOperacao,$nValorFinal){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();

            $oMnySaldoDiarioAplicacao->setCodSaldoDiario($nCodSaldoDiario);
            $oMnySaldoDiarioAplicacao->setCodAplicacao($nCodAplicacao);
            $oMnySaldoDiarioAplicacao->setValorRendimentoBanco($nValorRendimento);
            $oMnySaldoDiarioAplicacao->setValorBanco($nValor);
            $oMnySaldoDiarioAplicacao->setOperacao($sOperacao);
            $oMnySaldoDiarioAplicacao->setValorFinalBanco($nValorFinal);
            return $oMnySaldoDiarioAplicacao;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnySaldoDiarioAplicacao
         *
         * @return boolean
         */
        public function inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao){
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnySaldoDiarioAplicacao
         *
         * @return boolean
         */
        public function alterarMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao){
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnySaldoDiarioAplicacao
         *
         * @return boolean
         */
        public function excluirMnySaldoDiarioAplicacao($nCodSaldoDiario,$nCodAplicacao){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();

            $oMnySaldoDiarioAplicacao->setCodSaldoDiario($nCodSaldoDiario);
            $oMnySaldoDiarioAplicacao->setCodAplicacao($nCodAplicacao);
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnySaldoDiarioAplicacao na base de dados
         *
         * @return boolean
         */
        public function presenteMnySaldoDiarioAplicacao($nCodSaldoDiario,$nCodAplicacao){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();

            $oMnySaldoDiarioAplicacao->setCodSaldoDiario($nCodSaldoDiario);
            $oMnySaldoDiarioAplicacao->setCodAplicacao($nCodAplicacao);
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySaldoDiarioAplicacao da base de dados
         *
         * @return MnySaldoDiarioAplicacao
         */
        public function recuperarUmMnySaldoDiarioAplicacao($nCodSaldoDiario,$nCodAplicacao){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            $sTabelas = "mny_saldo_diario_aplicacao";
            $sCampos = "*";
            $sComplemento = " WHERE cod_saldo_diario = $nCodSaldoDiario AND cod_aplicacao = $nCodAplicacao";
            $voMnySaldoDiarioAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiarioAplicacao)
                return $voMnySaldoDiarioAplicacao[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiarioAplicacao da base de dados
         *
         * @return MnySaldoDiarioAplicacao[]
         */
        public function recuperarUmMnySaldoDiarioAplicacaoMaxData($nCcrCodigo, $nCodUnidade, $nCodTipoCaixa ){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            $sTabelas = "mny_saldo_diario_aplicacao";
            $sCampos = "mny_saldo_diario_aplicacao.*";
            $sComplemento ="INNER JOIN mny_saldo_diario ON mny_saldo_diario.cod_saldo_diario = mny_saldo_diario_aplicacao.cod_saldo_diario
                            WHERE mny_saldo_diario.data = (select MAX(data) from mny_saldo_diario) AND ccr_codigo = " .$nCcrCodigo. " AND cod_unidade = " . $nCodUnidade. " AND cod_tipo_caixa = " . $nCodTipoCaixa;
            //echo "SELECT * FROM " .$sTabelas . $sComplemento;
            $voMnySaldoDiarioAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiarioAplicacao)
                return $voMnySaldoDiarioAplicacao[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiarioAplicacao da base de dados
         *
         * @return MnySaldoDiarioAplicacao[]
         */
        public function recuperarTodosMnySaldoDiarioAplicacaoCdiAutomaticaPorContaUnidade($nCcrCodigo, $nCodUnidade){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            $sTabelas = "mny_saldo_diario_aplicacao";
            $sCampos = "cod_aplicacao,operacao,valor,valor_rendimento , valor_final";
            $sComplemento ="WHERE  mny_saldo_diario_aplicacao.cod_saldo_diario = (select max(mny_saldo_diario_aplicacao.cod_saldo_diario) from mny_saldo_diario_aplicacao INNER JOIN mny_saldo_diario ON mny_saldo_diario.cod_saldo_diario = mny_saldo_diario_aplicacao.cod_saldo_diario
                            WHERE mny_saldo_diario.ccr_codigo= $nCcrCodigo AND mny_saldo_diario.cod_unidade= $nCodUnidade) ";
            //echo "SELECT $sCampos FROM " .$sTabelas . $sComplemento;
            //die();
            $voMnySaldoDiarioAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiarioAplicacao)
                return $voMnySaldoDiarioAplicacao;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySaldoDiarioAplicacao da base de dados
         *
         * @return MnySaldoDiarioAplicacao[]
         */
        public function recuperarTodosMnySaldoDiarioAplicacaoPorSaldoDiario($nCodSaldoDiario){
            $oMnySaldoDiarioAplicacao = new MnySaldoDiarioAplicacao();
            $oPersistencia = new Persistencia($oMnySaldoDiarioAplicacao);
            $sTabelas = "mny_saldo_diario_aplicacao";
            $sCampos = "*";
            $sComplemento = "WHERE cod_saldo_diario = " . $nCodSaldoDiario;
        //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
        //die();
            $voMnySaldoDiarioAplicacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySaldoDiarioAplicacao)
                return $voMnySaldoDiarioAplicacao;
            return false;
        }

        /**
         *
         * Método para inicializar um Objeto MnyTipoCaixa
         *
         * @return MnyTipoCaixa
         */
        public function inicializarMnyTipoCaixa($nCodTipoCaixa,$sDescricao,$sInseridoPor,$sAlteradoPor,$nAtivo){
            $oMnyTipoCaixa = new MnyTipoCaixa();

            $oMnyTipoCaixa->setCodTipoCaixa($nCodTipoCaixa);
            $oMnyTipoCaixa->setDescricao($sDescricao);
            $oMnyTipoCaixa->setInseridoPor($sInseridoPor);
            $oMnyTipoCaixa->setAlteradoPor($sAlteradoPor);
            $oMnyTipoCaixa->setAtivo($nAtivo);
            return $oMnyTipoCaixa;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyTipoCaixa
         *
         * @return boolean
         */
        public function inserirMnyTipoCaixa($oMnyTipoCaixa){
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyTipoCaixa
         *
         * @return boolean
         */
        public function alterarMnyTipoCaixa($oMnyTipoCaixa){
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyTipoCaixa
         *
         * @return boolean
         */
        public function excluirMnyTipoCaixa($nCodTipoCaixa){
            $oMnyTipoCaixa = new MnyTipoCaixa();

            $oMnyTipoCaixa->setCodTipoCaixa($nCodTipoCaixa);
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyTipoCaixa na base de dados
         *
         * @return boolean
         */
        public function presenteMnyTipoCaixa($nCodTipoCaixa){
            $oMnyTipoCaixa = new MnyTipoCaixa();

            $oMnyTipoCaixa->setCodTipoCaixa($nCodTipoCaixa);
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyTipoCaixa da base de dados
         *
         * @return MnyTipoCaixa
         */
        public function recuperarUmMnyTipoCaixa($nCodTipoCaixa){
            $oMnyTipoCaixa = new MnyTipoCaixa();
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
            $sTabelas = "mny_tipo_caixa";
            $sCampos = "*";
            $sComplemento = " WHERE cod_tipo_caixa = $nCodTipoCaixa";
            $voMnyTipoCaixa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTipoCaixa)
                return $voMnyTipoCaixa[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyTipoCaixa da base de dados
         *
         * @return MnyTipoCaixa[]
         */
        public function recuperarTodosMnyTipoCaixa(){
            $oMnyTipoCaixa = new MnyTipoCaixa();
            $oPersistencia = new Persistencia($oMnyTipoCaixa);
            $sTabelas = "mny_tipo_caixa";
            $sCampos = "*";
            $sComplemento = "WHERE cod_tipo_caixa NOT IN(2)";
            $voMnyTipoCaixa = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTipoCaixa)
                return $voMnyTipoCaixa;
            return false;
        }

        public function recuperarUmMnyMovimentoItemPorMovimento($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = " WHERE ativo=1 and mov_codigo = $nMovCodigo ";
          //echo "SELECT $sCampos FROM $sTabelas " . $sComplemento;
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem;
            return false;
        }

        public function recuperarUmMnyMovimentoItemPorItem($nMovCodigo,$nMovItem){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = " . $nMovCodigo . " AND mov_item =" . $nMovItem . " and ativo = 1";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }

        public function recuperarUmMnyMovimentoItemPorMaxItem($nMovCodigo){
            $oMnyMovimentoItem = new MnyMovimentoItem();
            $oPersistencia = new Persistencia($oMnyMovimentoItem);
            $sTabelas = "mny_movimento_item";
            $sCampos = "max(mov_item) as mov_item";
            $sComplemento = " WHERE mov_codigo = " . $nMovCodigo .  " and ativo = 1";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnyMovimentoItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyMovimentoItem)
                return $voMnyMovimentoItem[0];
            return false;
        }


        /**
         *
         * Método para inicializar um Objeto MnyTransferencia
         *
         * @return MnyTransferencia
         */
        public function inicializarMnyTransferencia($nCodTransferencia,$nMovCodigoOrigem,$nMovItemOrigem,$nDe,$nMovCodigoDestino,$nMovItemDestino,$nPara,$nTipoAplicacao,$nCodSolicitacaoAporte,$sOperacao,$nAtivo){
            $oMnyTransferencia = new MnyTransferencia();

            $oMnyTransferencia->setCodTransferencia($nCodTransferencia);
            $oMnyTransferencia->setMovCodigoOrigem($nMovCodigoOrigem);
            $oMnyTransferencia->setMovItemOrigem($nMovItemOrigem);
            $oMnyTransferencia->setDe($nDe);
            $oMnyTransferencia->setMovCodigoDestino($nMovCodigoDestino);
            $oMnyTransferencia->setMovItemDestino($nMovItemDestino);
            $oMnyTransferencia->setPara($nPara);
            $oMnyTransferencia->setTipoAplicacao($nTipoAplicacao);
            $oMnyTransferencia->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
            $oMnyTransferencia->setOperacao($sOperacao);
            $oMnyTransferencia->setAtivo($nAtivo);
            return $oMnyTransferencia;
        }


        /**
         *
         * Método para inserir na base de dados um Objeto MnyTransferencia
         *
         * @return boolean
         */
        public function inserirMnyTransferencia($oMnyTransferencia){
            $oPersistencia = new Persistencia($oMnyTransferencia);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyTransferencia
         *
         * @return boolean
         */
        public function alterarMnyTransferencia($oMnyTransferencia){
            $oPersistencia = new Persistencia($oMnyTransferencia);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyTransferencia
         *
         * @return boolean
         */
        public function excluirMnyTransferencia($nCodTransferencia){
            $oMnyTransferencia = new MnyTransferencia();

            $oMnyTransferencia->setCodTransferencia($nCodTransferencia);
            $oPersistencia = new Persistencia($oMnyTransferencia);

            $sProcedure = "sp_exclui_transferencia";

            $sParametros = $nCodTransferencia;

            $MnyTransferencia = $oPersistencia->excluirFisicamenteProcedure($sProcedure,$sParametros);


            //print_r($MnyTransferencia[0]);
            //die();


            return $MnyTransferencia;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnyTransferencia na base de dados
         *
         * @return boolean
         */
        public function presenteMnyTransferencia($nCodTransferencia){
            $oMnyTransferencia = new MnyTransferencia();

            $oMnyTransferencia->setCodTransferencia($nCodTransferencia);
            $oPersistencia = new Persistencia($oMnyTransferencia);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyTransferencia da base de dados
         *
         * @return MnyTransferencia
         */
        public function recuperarUmMnyTransferencia($nCodTransferencia){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = " WHERE cod_transferencia = $nCodTransferencia";
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia[0];
            return false;
        }
        /**
         *
         * Método para recuperar um objeto MnyTransferencia da base de dados
         *
         * @return MnyTransferencia
         */
        public function recuperarUmMnyTransferenciaPorMovimentoItem($nMovCodigo, $nMovItem){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = " WHERE (mov_codigo_origem = ".$nMovCodigo." AND mov_item_origem = " . $nMovItem.") OR (mov_codigo_destino = ".$nMovCodigo." AND mov_item_destino = " . $nMovItem.")";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia[0];
            return false;
        }
        /**
         *
         * Método para recuperar um objeto MnyTransferencia da base de dados
         *
         * @return MnyTransferencia
         */
        public function recuperarUmMnyTransferenciaPorMovimento($nMovCodigo){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = " WHERE (mov_codigo_origem = ".$nMovCodigo." ) OR (mov_codigo_destino = ".$nMovCodigo." )";
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyTransferencia da base de dados
         *
         * @return MnyTransferencia
         */
        public function recuperarUmMnytransferenciaPorSoliticatacaoAporte($nCodSolicitacao){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = " WHERE cod_solicitacao_aporte = " . $nCodSolicitacao;
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnyTransferencia da base de dados
         *
         * @return MnyTransferencia
         */
        public function presenteMnyTransferenciaPorMovimentoItemResgate($nMovCodigo, $nMovItem){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = " WHERE ((mov_codigo_origem = ".$nMovCodigo." AND mov_item_origem = " . $nMovItem.") OR (mov_codigo_destino = ".$nMovCodigo." AND mov_item_destino = " . $nMovItem.")) AND de = para AND operacao='D'";
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            //die();
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return true;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyTransferencia da base de dados
         *
         * @return MnyTransferencia[]
         */
        public function recuperarTodosMnyTransferencia(){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos = "*";
            $sComplemento = "";
            // echo "Select $sCampos From $sTabelas $sComplemento";
           // die();
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyTransferencia da base de dados
         *
         * @return MnyTransferencia[]
         */
        public function recuperarTodosMnyTransferenciaPorGrupoAplicacaoConta($nGrupoAplicacao,$nCcrCodigo,$dDataConciliacao){
            $oMnyTransferencia = new MnyTransferencia();
            $oPersistencia = new Persistencia($oMnyTransferencia);
            $sTabelas = "mny_transferencia";
            $sCampos  = " cod_transferencia, mov_codigo_origem, mov_item_origem ,de , mov_codigo_destino, mov_item_destino,
                          para, tipo_aplicacao, operacao, mny_movimento_item.mov_valor_parcela AS ativo";
            $sComplemento = "INNER JOIN mny_conciliacao ON mny_conciliacao.mov_codigo = mny_transferencia.mov_codigo_origem AND mny_conciliacao.data_conciliacao = '". $dDataConciliacao ."' AND mny_conciliacao.consolidado = 0
                             INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_transferencia.mov_codigo_origem AND mny_movimento_item.mov_item = 1
                             WHERE  mny_transferencia.tipo_aplicacao = ". $nGrupoAplicacao ." AND  mny_conciliacao.cod_conta_corrente = " . $nCcrCodigo;
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnyTransferencia = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyTransferencia)
                return $voMnyTransferencia;
            return false;
        }

        public function inicializarMnyAporteItem($nCodSolicitacaoAporte,$nAporteItem,$nMovCodigo,$nMovItem,$nAssinado,$nEstornado,$nAtivo){
            $oMnyAporteItem = new MnyAporteItem();

            $oMnyAporteItem->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
            $oMnyAporteItem->setAporteItem($nAporteItem);
            $oMnyAporteItem->setMovCodigo($nMovCodigo);
            $oMnyAporteItem->setMovItem($nMovItem);
            $oMnyAporteItem->setAssinado($nAssinado);
            $oMnyAporteItem->setEstornado($nEstornado);
            $oMnyAporteItem->setAtivo($nAtivo);
            return $oMnyAporteItem;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnyAporteItem
         *
         * @return boolean
         */
        public function inserirMnyAporteItem($oMnyAporteItem){
            $oPersistencia = new Persistencia($oMnyAporteItem);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnyAporteItem
         *
         * @return boolean
         */
        public function alterarMnyAporteItem($oMnyAporteItem){
            $oPersistencia = new Persistencia($oMnyAporteItem);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnyAporteItem
         *
         * @return boolean
         */
        public function excluirMnyAporteItem($nCodSolicitacaoAporte,$nAporteItem){
            $oMnyAporteItem = new MnyAporteItem();

            $oMnyAporteItem->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
            $oMnyAporteItem->setAporteItem($nAporteItem);
            $oPersistencia = new Persistencia($oMnyAporteItem);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['oUsuarioImoney']->getCodGrupoUsuario() == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }
            /**
         *
         * Método para excluir da base de dados um Objeto MnyAporteItem
         *
         * @return boolean
         */
        public function excluirMnyAporteItemPorMovimentoPorItem($nMovCodigo,$nMovItem){
            $oMnyAporteItem = new MnyAporteItem();
            $sComplemento = "Where mov_codigo=". $nMovCodigo." And mov_item=" . $nMovItem;
            $oPersistencia = new Persistencia($oMnyAporteItem);
                $bExcluir =  $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;
        }

            /**
         *
         * Método para excluir da base de dados um Objeto MnyAporteItem
         *
         * @return boolean
         */
        public function exluirMnyAporteItemPorSolicitacaoAporte($nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $sComplemento = " WHERE cod_solicitacao_aporte=".$nCodSolicitacaoAporte;
            $oPersistencia = new Persistencia($oMnyAporteItem);
                $bExcluir = $oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento);
            if($bExcluir)
                    return true;
            return false;

        }


        /**
         *
         * Método para verificar se existe um Objeto MnyAporteItem na base de dados
         *
         * @return boolean
         */
        public function presenteMnyAporteItem($nCodSolicitacaoAporte,$nAporteItem){
            $oMnyAporteItem = new MnyAporteItem();

            $oMnyAporteItem->setCodSolicitacaoAporte($nCodSolicitacaoAporte);
            $oMnyAporteItem->setAporteItem($nAporteItem);
            $oPersistencia = new Persistencia($oMnyAporteItem);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para verificar se existe um Objeto MnyAporteItem na base de dados
         *
         * @return boolean
         */
        public function recuperarUmMnyAporteItemPorFA($nMovCodigo,$nMovItem){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo = $nMovCodigo AND mov_item = $nMovItem";
            //echo "SELECT * FROM ".$sTabelas . " " . $sComplemento;
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }


        /**
         *
         * Método para recuperar um objeto MnyAporteItem da base de dados
         *
         * @return MnyAporteItem
         */
        public function recuperarUmMnyAporteItem($nCodSolicitacaoAporte,$nAporteItem){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = " WHERE cod_solicitacao_aporte = $nCodSolicitacaoAporte AND aporte_item = $nAporteItem";
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }
            /**
         *
         * Método para recuperar um objeto MnyAporteItem da base de dados
         *
         * @return MnyAporteItem
         */
        public function recuperarUmMnyAporteItemPorMaxItem($nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "max(aporte_item) as aporte_item";
            $sComplemento = " WHERE cod_solicitacao_aporte =" .  $nCodSolicitacaoAporte ;
            //echo "Select $sCampos From $sTabelas $sComplemento";
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }


            /**
         *
         * Método para recuperar um objeto MnyAporteItem da base de dados
         *
         * @return MnyAporteItem
         */
        public function recuperarUmMnyAporteItemPorMovimentoPorItem($nMovCodigo,$nMovItem){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo=". $nMovCodigo ." AND mov_item =". $nMovItem ." And estornado=0";
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }

         /**
         *
         * Método para recuperar um objeto MnyAporteItem da base de dados
         *
         * @return MnyAporteItem
         */
        public function recuperarUmMnyAporteItemPorMovimento($nMovCodigo){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = " WHERE mov_codigo=". $nMovCodigo;
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }


        /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados
         *
         * @return MnyAporteItem[]
         */
        public function recuperarTodosMnyAporteItem(){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = "";
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados
         *
         * @return MnyAporteItem[]
         */
        public function assinaMnyAporteItenPorSolicitacaoAporte($nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = "assinado = 1 Where cod_solicitacao_aporte=".$nCodSolicitacaoAporte;
            $voMnyAporteItem = $oPersistencia->alterarSemChavePrimaria($sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem;
            return false;
        }

                /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnySolicitacaoAporte[]
         */
        public function recuperarTodosMnyAporteItemPorSolicitacao($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_aporte_item";
            $sCampos = "*";
            $sComplemento = "WHERE ativo = 1 AND cod_solicitacao_aporte = ".$nCodSolicitacao;
            //echo "SELECT $sCampos FROM $sTabelas $sComplemento";
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnySolicitacaoAporte[]
         */
        public function recuperarTodosMnyAporteItemPorUnidade($nCodUnidade,$nNumeroSolicitacaoAporte=''){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas ="mny_aporte_item";
            $sCampos = "Concat(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) AS mov_codigo,DATE_FORMAT(mny_movimento_item.mov_data_vencto, '%d/%m/%Y') AS mov_item,f_valor_parcela_liquido (mny_aporte_item.mov_codigo,mny_aporte_item.mov_item) AS ativo,mny_aporte_item.assinado,mov_obs AS aporte_item";
            if($nNumeroSolicitacaoAporte){
                $sComplemento = "INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                                 INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                                 INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_aporte_item.mov_codigo and mny_movimento_item.mov_item = mny_aporte_item.mov_item  and mny_movimento_item.ativo=1
                                 WHERE /*mny_movimento.uni_codigo=".$nCodUnidade." AND*/ mny_aporte_item.cod_solicitacao_aporte=".$nNumeroSolicitacaoAporte ." And mny_aporte_item.estornado=0";
            }else{
                $sComplemento = "INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                                 INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                                 INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_aporte_item.mov_codigo and mny_movimento_item.mov_item = mny_aporte_item.mov_item  and mny_movimento_item.ativo=1
                                 WHERE mny_movimento.uni_codigo=".$nCodUnidade ;//." AND mny_solicitacao_aporte.numero_solicitacao=".$nNumeroSolicitacaoAporte;
            }
            //echo "Select $sCampos From $sTabelas From $sComplemento";
            //die();
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem;
            return false;
        }
        /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnySolicitacaoAporte[]

        public function recuperarTodosMnyAporteItemPorUnidadePorSolicitacaoAporte($nCodUnidade,$nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "mny_aporte_item";
            $sCampos  = "concat(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) as mov_codigo,(Select nome From mny_arquivo Where mov_codigo=mny_aporte_item.mov_codigo And mov_item=mny_aporte_item.mov_item And cod_contrato_doc_tipo=11) AS mov_item,mny_solicitacao_aporte.assinado as assinado,
                        f_valor_parcela_liquido(mny_aporte_item.mov_codigo,mny_aporte_item.mov_item) as ativo,mov_obs as aporte_item ";
            $sComplemento = "INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                             INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                             INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.para_cod_tipo_protocolo = 10 AND mny_movimento_item_protocolo.mov_codigo = mny_aporte_item.mov_codigo AND mny_movimento_item_protocolo.mov_item = mny_aporte_item.mov_item And mny_movimento_item_protocolo.`data` = (Select max(data) From mny_movimento_item_protocolo Where para_cod_tipo_protocolo = 10 And mov_codigo = mny_aporte_item.mov_codigo And mov_item= mny_aporte_item.mov_item)
                             WHERE mny_movimento.uni_codigo = ".$nCodUnidade." And mny_solicitacao_aporte.cod_solicitacao =".$nCodSolicitacaoAporte;
            //echo "SELECT " . $sCampos. "FROM " . $sTabelas . "  " . $sComplemento;

            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem;
            return false;
        }
        */

        public function recuperarUmMnyAporteItemPorUnidadePorSolicitacaoAporte($nCodUnidade,$nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas     = "mny_aporte_item";
            $sCampos      = "DISTINCT 	IFNULL(sys_arquivo.cod_arquivo,0) AS cod_solicitacao_aporte,
                                concat(mny_transferencia.mov_codigo_origem,'.',mny_transferencia.mov_item_origem) AS mov_codigo,
                                mny_transferencia.cod_transferencia AS assinado,
                                f_valor_parcela_liquido (mny_aporte_item.mov_codigo,mny_aporte_item.mov_item) AS ativo,
                                mov_obs AS aporte_item";
            $sComplemento = " INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                            INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                            INNER JOIN mny_transferencia ON mny_transferencia.cod_solicitacao_aporte=".$nCodSolicitacaoAporte."
                            LEFT JOIN sys_arquivo ON sys_arquivo.mov_codigo = mny_transferencia.mov_codigo_origem AND sys_arquivo.mov_item = mny_transferencia.mov_item_origem AND sys_arquivo.cod_tipo = 11
                            INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.para_cod_tipo_protocolo = 10
                            AND mny_movimento_item_protocolo.mov_codigo = mny_aporte_item.mov_codigo
                            AND mny_movimento_item_protocolo.mov_item = mny_aporte_item.mov_item
                            AND mny_movimento_item_protocolo.`data` = (
                                SELECT
                                    max(DATA)
                                FROM
                                    mny_movimento_item_protocolo
                                WHERE
                                    para_cod_tipo_protocolo = 10
                                AND mov_codigo = mny_aporte_item.mov_codigo
                                AND mov_item = mny_aporte_item.mov_item
                            )
                            WHERE
                                 mny_solicitacao_aporte.cod_solicitacao =".$nCodSolicitacaoAporte;
                               // mny_movimento.uni_codigo =".$nCodUnidade." AND mny_solicitacao_aporte.cod_solicitacao =".$nCodSolicitacaoAporte;
            //echo "SELECT " . $sCampos. "FROM " . $sTabelas . "  " . $sComplemento;
            //die();
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }

        public function recuperarTodosMnyAporteItemPorUnidadePorSolicitacaoAporte($nCodUnidade,$nCodSolicitacaoAporte){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas     = "mny_aporte_item";
            $sCampos      = "sys_arquivo.cod_arquivo As cod_solicitacao_aporte,concat(mny_aporte_item.mov_codigo,'.',mny_aporte_item.mov_item) AS mov_codigo,mny_solicitacao_aporte.assinado AS assinado,f_valor_parcela_liquido (mny_aporte_item.mov_codigo,mny_aporte_item.mov_item) AS ativo,mov_obs AS aporte_item";
            $sComplemento = "INNER JOIN mny_solicitacao_aporte ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                             INNER JOIN mny_movimento ON mny_aporte_item.mov_codigo = mny_movimento.mov_codigo
                             LEFT JOIN sys_arquivo ON sys_arquivo.mov_codigo = mny_aporte_item.mov_codigo AND sys_arquivo.mov_item = mny_aporte_item.mov_item AND sys_arquivo.cod_tipo = 11
                             INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.para_cod_tipo_protocolo = 10 AND mny_movimento_item_protocolo.mov_codigo = mny_aporte_item.mov_codigo AND mny_movimento_item_protocolo.mov_item = mny_aporte_item.mov_item And mny_movimento_item_protocolo.`data` = (Select max(data) From mny_movimento_item_protocolo Where para_cod_tipo_protocolo = 10 And mov_codigo = mny_aporte_item.mov_codigo And mov_item= mny_aporte_item.mov_item)
                             WHERE mny_aporte_item.estornado=0 And mny_solicitacao_aporte.cod_solicitacao =".$nCodSolicitacaoAporte."

                             UNION
                                SELECT
            sys_arquivo.cod_arquivo AS cod_solicitacao_aporte,
            concat(mny_movimento_item.mov_codigo,'.',mny_movimento_item.mov_item) AS mov_codigo,
            mny_transferencia.cod_transferencia AS assinado,
            f_valor_parcela_liquido (mny_movimento_item.mov_codigo,mny_movimento_item.mov_item) AS ativo,
            mov_obs AS aporte_item
        FROM
            mny_transferencia
        INNER JOIN mny_solicitacao_aporte ON mny_transferencia.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
        INNER JOIN mny_movimento ON mny_transferencia.mov_codigo_origem = mny_movimento.mov_codigo
        INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_movimento.mov_codigo And mny_movimento_item.mov_item=98
        LEFT JOIN sys_arquivo ON sys_arquivo.mov_codigo = mny_movimento_item.mov_codigo
        AND sys_arquivo.mov_item = mny_movimento_item.mov_item
        AND sys_arquivo.cod_tipo = 11
        INNER JOIN mny_movimento_item_protocolo ON mny_movimento_item_protocolo.para_cod_tipo_protocolo = 10
        AND mny_movimento_item_protocolo.mov_codigo = mny_movimento_item.mov_codigo
        AND mny_movimento_item_protocolo.mov_item = mny_movimento_item.mov_item
        AND mny_movimento_item_protocolo.`data` = (
            SELECT
                max(DATA)
            FROM
                mny_movimento_item_protocolo
            WHERE
                para_cod_tipo_protocolo = 10
            AND mov_codigo = mny_transferencia.mov_codigo_origem
            AND mov_item = 98
        )
        WHERE
            mny_solicitacao_aporte.cod_solicitacao = ".$nCodSolicitacaoAporte;
            //  mny_movimento.uni_codigo =".$nCodUnidade." AND mny_solicitacao_aporte.cod_solicitacao =".$nCodSolicitacaoAporte;
            //echo "SELECT " . $sCampos. "FROM " . $sTabelas . "  " . $sComplemento;
            //die();
            $voMnyAporteItem = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnyAporteItem)
                return $voMnyAporteItem;
            return false;
        }

       /**
         *
         * Método para recuperar um vetor de objetos MnyAporteItem da base de dados por cod_solicitacao
         *
         * @return MnyAporteItem[]
         */
        public function recuperarUmMnyAporteItemLiberadoPorMovimentoPorItem($nMovCodigo,$nMovItem){
            $oMnyAporteItem = new MnyAporteItem();
            $oPersistencia = new Persistencia($oMnyAporteItem);
            $sTabelas = "sp_adiciona_item_conciliacao";
            //$sCampos = "mov_codigo,mov_item,ativo,aporte_item ";
            $sParametros = $nMovCodigo .",".$nMovItem;
            $voMnyAporteItem = $oPersistencia->consultarProcedure($sTabelas,$sParametros);
            if($voMnyAporteItem)
                return $voMnyAporteItem[0];
            return false;
        }
        /**
         *
         * Método para inicializar um Objeto MnySolicitacaoAporte
         *
         * @return MnySolicitacaoAporte
         */
        public function inicializarMnySolicitacaoAporte($nCodSolicitacao,$dDataSolicitacao,$nNumeroSolicitacao,$sNumeroAporte,$sSolInc,$sSolAlt,$nAssinado,$nLiberado,$nAtivo){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oMnySolicitacaoAporte->setCodSolicitacao($nCodSolicitacao);
            $oMnySolicitacaoAporte->setDataSolicitacaoBanco($dDataSolicitacao);
            $oMnySolicitacaoAporte->setNumeroSolicitacao($nNumeroSolicitacao);
            $oMnySolicitacaoAporte->setNumeroAporte($sNumeroAporte);
            $oMnySolicitacaoAporte->setSolInc($sSolInc);
            $oMnySolicitacaoAporte->setSolAlt($sSolAlt);
            $oMnySolicitacaoAporte->setAssinado($nAssinado);
            $oMnySolicitacaoAporte->setLiberado($nLiberado);
            $oMnySolicitacaoAporte->setAtivo($nAtivo);
            return $oMnySolicitacaoAporte;
        }

        /**
         *
         * Método para inserir na base de dados um Objeto MnySolicitacaoAporte
         *
         * @return boolean
         */
        public function inserirMnySolicitacaoAporte($oMnySolicitacaoAporte){
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            if($nId = $oPersistencia->inserir())
                    return $nId;
            return false;
        }

        /**
         *
         * Método para alterar na base de dados um Objeto MnySolicitacaoAporte
         *
         * @return boolean
         */
        public function alterarMnySolicitacaoAporte($oMnySolicitacaoAporte){
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            if($oPersistencia->alterar())
                    return true;
            return false;
        }


        /**
         *
         * Método para excluir da base de dados um Objeto MnySolicitacaoAporte
         *
         * @return boolean
         */
        public function excluirMnySolicitacaoAporte($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();

            $oMnySolicitacaoAporte->setCodSolicitacao($nCodSolicitacao);
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
                $bExcluir = ($_SESSION['oUsuarioImoney'] && $_SESSION['Perfil']['CodGrupoUsuario'] == '1') ? $oPersistencia->excluirFisicamente() : $oPersistencia->excluir();

            if($bExcluir)
                    return true;
            return false;

        }

        /**
         *
         * Método para verificar se existe um Objeto MnySolicitacaoAporte na base de dados
         *
         * @return boolean
         */
        public function presenteMnySolicitacaoAporte($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();

            $oMnySolicitacaoAporte->setCodSolicitacao($nCodSolicitacao);
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            if($oPersistencia->presente())
                    return true;
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function abreUmMnySolicitacaoAporte($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sProcedure = "sp_abre_aporte";
            $sParametros = $nCodSolicitacao.",".$_SESSION['Perfil']['CodUsuario'];
           // echo "Call $sProcedure $sParametros";
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultarProcedure($sProcedure,$sParametros);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }
        /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function recuperarUmMnySolicitacaoAporte($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte";
            $sCampos = "*";
            $sComplemento = " WHERE cod_solicitacao = $nCodSolicitacao AND ativo =1";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function recuperarUmMnySolicitacaoAporteValorGlobal($nCodSolicitacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte a";
            $sCampos = " cod_solicitacao,data_solicitacao,numero_solicitacao,numero_aporte,sol_inc,
            (Select format(sum(f_valor_parcela_liquido(mov_codigo,mov_item)),2,'de_DE') From mny_aporte_item Where cod_solicitacao_aporte=$nCodSolicitacao And estornado=0) as sol_alt,
            assinado,liberado,ativo";
            $sComplemento = "WHERE cod_solicitacao = $nCodSolicitacao AND ativo =1";
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }
         /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function recuperarUmMnySolicitacaoAportePorMovimentoPorItem($nMovCodigo, $nMovItem){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte s";

            $sCampos = " DISTINCT s.cod_solicitacao,s.numero_aporte,s.data_solicitacao,e.emp_fantasia As sol_alt,arq.cod_arquivo As sol_inc, s.liberado,ct.tamanho_arquivo As assinado";
            $sComplemento ="INNER JOIN mny_aporte_item ai ON s.cod_solicitacao = ai.cod_solicitacao_aporte And ai.estornado=0
                            LEFT JOIN mny_transferencia t ON t.cod_solicitacao_aporte= s.cod_solicitacao
                            INNER JOIN mny_movimento m ON m.mov_codigo = ".$nMovCodigo."
                            INNER JOIN sys_empresa e On e.emp_codigo = m.emp_codigo
                            LEFT  JOIN sys_arquivo arq On arq.mov_codigo=ai.mov_codigo And arq.mov_item=ai.mov_item And arq.cod_tipo=11
                            INNER JOIN mny_contrato_doc_tipo ct On ct.cod_contrato_tipo=11
                            Where s.ativo=1 And ai.mov_codigo=".$nMovCodigo." AND ai.mov_item=".$nMovItem;
           // echo "Select $sCampos From $sTabelas $sComplemento";
           // die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }
         /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function recuperarUmMnySolicitacaoAportePorFAdetalhe($nMovCodigo, $nMovItem){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sProc = "sp_detalhe_aporte";
            $sParametros = $nMovCodigo.",".$nMovItem;
            $voMnySolicitacaoAporte = $oPersistencia->consultarProcedureLista($sProc,$sParametros);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }

        /**
         *
         * Método para recuperar um objeto MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte
         */
        public function recuperarUmMnySolicitacaoAporteProximoNumeroSolicitacao($nUniCodigo,$nAno){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte sa";

            $sCampos = "IFNULL(max(numero_solicitacao),0) +1 as  numero_solicitacao,CONCAT('SA.',substr(mny_plano_contas.codigo,5),'.',IFNULL(YEAR(MAX(data_solicitacao)),".$nAno."),'.',LPAD(IFNULL(max(numero_solicitacao),0) +1,3,'0')) as liberado";
            $sComplemento = "	INNER JOIN mny_transferencia t ON t.cod_solicitacao_aporte = sa.cod_solicitacao
                                INNER JOIN mny_movimento m ON m.mov_codigo = t.mov_codigo_destino and m.uni_codigo = ".$nUniCodigo."
                                INNER JOIN mny_plano_contas ON m.uni_codigo = mny_plano_contas.plano_contas_codigo
                                where sa.ativo = 1  And YEAR(data_solicitacao) =".$nAno;
            //echo " SELECT " . $sCampos ." FROM ". $sTabelas ." ". $sComplemento;
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte[0];
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte[]
         */
        public function recuperarTodosMnySolicitacaoAporte(){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte";
            $sCampos = "cod_solicitacao,data_solicitacao, emp_fantasia as emp_codigo, descricao as uni_codigo,periodo1,periodo2,sol_inc,sol_alt,mny_solicitacao_aporte.ativo as ativo";
            $sComplemento = "JOIN sys_empresa ON mny_solicitacao_aporte.emp_codigo = sys_empresa.emp_codigo
                             LEFT JOIN mny_plano_contas ON mny_solicitacao_aporte.uni_codigo = mny_plano_contas.plano_contas_codigo
                            WHERE mny_solicitacao_aporte.ativo =1";
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte;
            return false;
        }	/**


        /**
         *
         * Método para recuperar um vetor de objetos MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte[]
         */
        public function recuperarTodosMnySolicitacaoAportePorEmpresaPorSituacao($nEmpCodigo,$nSituacao){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte";
            $sCampos = "mny_transferencia.mov_codigo_origem As sol_inc,mny_transferencia.mov_codigo_destino As assinado,cod_solicitacao,data_solicitacao,(SELECT format(sum(f_valor_parcela_liquido (mov_codigo, mov_item)),2,'de_DE')	FROM	mny_aporte_item	WHERE	cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao AND estornado = 0) as numero_solicitacao, emp_fantasia as ativo, descricao as sol_alt,liberado,numero_aporte";
            $sComplemento = "INNER JOIN mny_transferencia ON mny_transferencia.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                             INNER JOIN mny_movimento ON mny_movimento.mov_codigo = mny_transferencia.mov_codigo_destino
                             INNER JOIN mny_movimento_item mi ON mi.mov_codigo=mny_movimento.mov_codigo And mi.mov_item=99
                             LEFT JOIN mny_plano_contas ON mny_movimento.uni_codigo = mny_plano_contas.plano_contas_codigo
                             JOIN sys_empresa ON mny_movimento.emp_codigo = sys_empresa.emp_codigo
                             WHERE mny_solicitacao_aporte.ativo = 1 AND mny_movimento.emp_codigo = " . $nEmpCodigo ." AND liberado=".$nSituacao;
            //echo "Select $sCampos From $sTabelas $sComplemento";
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte;
            return false;
        }

        /**
         *
         * Método para recuperar um vetor de objetos MnySolicitacaoAporte da base de dados
         *
         * @return MnySolicitacaoAporte[]
         */
        public function recuperarTodosMnySolicitacaoAportePorPendencia($nLiberado){
            $oMnySolicitacaoAporte = new MnySolicitacaoAporte();
            $oPersistencia = new Persistencia($oMnySolicitacaoAporte);
            $sTabelas = "mny_solicitacao_aporte";
            $sCampos = "DISTINCT
           (select sum(f_valor_parcela_liquido(mi.mov_codigo,mi.mov_item)) FROM mny_movimento_item mi inner join mny_aporte_item ai on ai.mov_codigo=mi.mov_codigo AND ai.mov_item=mi.mov_item WHERE concat(mi.mov_codigo,'.',mi.mov_item) IN(concat(ai.mov_codigo,'.',ai.mov_item)) AND ai.cod_solicitacao_aporte=mny_solicitacao_aporte.cod_solicitacao) as ativo,
            mny_solicitacao_aporte.data_solicitacao,mny_plano_contas.descricao AS sol_inc,numero_aporte As numero_solicitacao,mny_solicitacao_aporte.cod_solicitacao,	(
            CONCAT((SELECT	IF (COUNT(mny_aporte_item.mov_codigo),0,NULL)
                    FROM
                        mny_aporte_item
                    INNER JOIN mny_arquivo ON mny_arquivo.mov_codigo = mny_aporte_item.mov_codigo
                    AND mny_arquivo.mov_item
                    AND mny_aporte_item.mov_item
                    AND mny_arquivo.cod_contrato_doc_tipo = 11
                    WHERE
                        cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                    AND liberado = 0
                ),
                '/',
                (
                    SELECT

                    IF (
                        COUNT(mny_aporte_item.mov_codigo),
                        0,
                        NULL
                    )
                    FROM
                        mny_aporte_item
                    WHERE
                        cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                    AND liberado = 0
                )
            )
        ) AS sol_alt";
            $sComplemento = "INNER JOIN mny_aporte_item ON mny_aporte_item.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                             INNER JOIN mny_transferencia ON mny_transferencia.cod_solicitacao_aporte = mny_solicitacao_aporte.cod_solicitacao
                             INNER JOIN mny_movimento ON mny_movimento.mov_codigo = mny_transferencia.mov_codigo_destino
                             INNER JOIN mny_movimento_item ON mny_movimento_item.mov_codigo = mny_movimento.mov_codigo
                             INNER JOIN mny_plano_contas ON mny_plano_contas.plano_contas_codigo = mny_movimento.uni_codigo
                             WHERE mny_solicitacao_aporte.liberado =".$nLiberado;
            //echo "Select $sCampos From ". $sTabelas . " ". $sComplemento .";";
            //die();
            $voMnySolicitacaoAporte = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
            if($voMnySolicitacaoAporte)
                return $voMnySolicitacaoAporte;
            return false;
        }





    }
    ?>
