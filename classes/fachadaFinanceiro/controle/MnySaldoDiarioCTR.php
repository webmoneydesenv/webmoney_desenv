<?php
 class MnySaldoDiarioCTR implements IControle{
 
 	public function MnySaldoDiarioCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnySaldoDiario = $oFachada->recuperarTodosMnySaldoDiario();
 
 		$_REQUEST['voMnySaldoDiario'] = $voMnySaldoDiario;
 		$_REQUEST['voMnyTipoCaixa'] = $oFachada->recuperarTodosMnyTipoCaixa();

		
 		
 		include_once("view/financeiro/mny_saldo_diario/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnySaldoDiario = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnySaldoDiario = ($_POST['fIdMnySaldoDiario'][0]) ? $_POST['fIdMnySaldoDiario'][0] : $_GET['nIdMnySaldoDiario'];
 	
 			if($nIdMnySaldoDiario){
 				$vIdMnySaldoDiario = explode("||",$nIdMnySaldoDiario);
 				$oMnySaldoDiario = $oFachada->recuperarUmMnySaldoDiario($vIdMnySaldoDiario[0]);
 			}
 		}
 		
 		$_REQUEST['oMnySaldoDiario'] = ($_SESSION['oMnySaldoDiario']) ? $_SESSION['oMnySaldoDiario'] : $oMnySaldoDiario;
 		unset($_SESSION['oMnySaldoDiario']);
 
 		$_REQUEST['voMnyTipoCaixa'] = $oFachada->recuperarTodosMnyTipoCaixa();
     	$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
		// 8 é o grupo das unidades 
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo(8);

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_saldo_diario/detalhe.php");
 		else
 			include_once("view/financeiro/mny_saldo_diario/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnySaldoDiario = $oFachada->inicializarMnySaldoDiario($_POST['fCodSaldoDiario'],$_POST['fCodTipoCaixa'],$_POST['fCcrCodigo'],$_POST['fCodUnidade'],$_POST['fData'],$_POST['fValorFinal'],$_POST['fValorInicial'],mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fTipoSaldo'],$_POST['fExtratoBancario'],$_POST['fAtivo']);
 			$_SESSION['oMnySaldoDiario'] = $oMnySaldoDiario;
		
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("CodTipoCaixa", $oMnySaldoDiario->getCodTipoCaixa(), "number", "y");
			$oValidate->add_date_field("Data", $oMnySaldoDiario->getData(), "date", "y");
			//$oValidate->add_number_field("Valor", $oMnySaldoDiario->getValor(), "number", "y");
			$oValidate->add_text_field("RealizadoPor", $oMnySaldoDiario->getRealizadoPor(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnySaldoDiario->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnySaldoDiario.preparaFormulario&sOP=".$sOP."&nIdMnySaldoDiario=".$_POST['fCodSaldoDiario']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnySaldoDiario($oMnySaldoDiario)){
 					unset($_SESSION['oMnySaldoDiario']);
 					$_SESSION['sMsg'] = "Saldo Diário inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiario.preparaLista";
 				}else{
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Saldo Diário!";
 					$sHeader = "?bErro=1&action=MnySaldoDiario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnySaldoDiario($oMnySaldoDiario)){
 					unset($_SESSION['oMnySaldoDiario']);
 					$_SESSION['sMsg'] = "Saldo Diário alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Saldo Diário!";
 					$sHeader = "?bErro=1&action=MnySaldoDiario.preparaFormulario&sOP=".$sOP."&nIdMnySaldoDiario=".$_POST['fCodSaldoDiario']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnySaldoDiario']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnySaldoDiario($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Saldo Diário(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Saldo Diário!";
 					$sHeader = "?bErro=1&action=MnySaldoDiario.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);			
 	
 	}
	
	public function carregaConta(){
			$oFachada = new FachadaFinanceiroBD();
			$nCodUnidade = $_REQUEST['fCodUnidade'];							
			$_REQUEST['voMnyContaCorrenteUnidade'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);							
			include_once("view/financeiro/mny_saldo_diario/conta_ajax.php");
	}
 
 }
 
 
 ?>
