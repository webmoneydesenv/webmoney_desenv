<?php
 class MnyPessoaFisicaCTR implements IControle{
 
 	public function MnyPessoaFisicaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 /*
 		$voMnyPessoaFisica = $oFachada->recuperarTodosMnyPessoaFisica();
 
 		$_REQUEST['voMnyPessoaFisica'] = $voMnyPessoaFisica;
 		$_REQUEST['voSysStatus'] = $oFachada->recuperarTodosSysStatus();

		
 		
 		include_once("view/financeiro/mny_pessoa_fisica/index.php");
 
 		exit();*/
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD(); 
 		$oFachadaSys = new FachadaSysBD();
		
 		$oMnyPessoaFisica = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyPessoaFisica = ($_POST['fIdMnyPessoaFisica'][0]) ? $_POST['fIdMnyPessoaFisica'][0] : $_GET['nIdMnyPessoaFisica'];
 	
 			if($nIdMnyPessoaFisica){
 				$vIdMnyPessoaFisica = explode("||",$nIdMnyPessoaFisica);
 				$oMnyPessoaFisica = $oFachada->recuperarUmMnyPessoaFisica($vIdMnyPessoaFisica[0]);
 				$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($vIdMnyPessoaFisica[0]);
 			}
 		}


 		$_REQUEST['oMnyPessoaGeral'] = ($_SESSION['oMnyPessoaGeral']) ? $_SESSION['oMnyPessoaGeral'] : $oMnyPessoaGeral;
 		unset($_SESSION['oMnyPessoaGeral']);

 		
 		$_REQUEST['oMnyPessoaFisica'] = ($_SESSION['oMnyPessoaFisica']) ? $_SESSION['oMnyPessoaFisica'] : $oMnyPessoaFisica;
 		unset($_SESSION['oMnyPessoaFisica']);
 
 		$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
		

		//$sGrupo = 14;// banco
		$_REQUEST['voSysBanco'] = $oFachadaSys->recuperarTodosSysBanco();
		
		$sGrupo = 13;// tipo de conta
		$_REQUEST['voSysContaTipo'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		
				
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_pessoa_fisica/detalhe.php");
 		else
 			include_once("view/financeiro/mny_pessoa_fisica/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyPessoaFisica = $oFachada->inicializarMnyPessoaFisica($_POST['fPesfisCodigo'],mb_convert_case($_POST['fPesfisNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesfisCpf'],mb_convert_case($_POST['fPesfisDescricao'],MB_CASE_UPPER, "UTF-8"));
 																																												
			$_SESSION['oMnyPessoaFisica'] = $oMnyPessoaFisica;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("PesfisCodigo", $oMnyPessoaFisica->getPesfisCodigo(), "number", "y");
			$oValidate->add_text_field("PesfisNome", $oMnyPessoaFisica->getPesfisNome(), "text", "y");
			$oValidate->add_text_field("PesfisCpf", $oMnyPessoaFisica->getPesfisCpf(), "text", "y");
			

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaFisica=".$_POST['fPesfisCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyPessoaFisica($oMnyPessoaFisica)){
 					unset($_SESSION['oMnyPessoaFisica']);
 					$_SESSION['sMsg'] = "Pesssoa Física inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
					
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Pesssoa Física!";
 					$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyPessoaFisica($oMnyPessoaFisica)){
 					unset($_SESSION['oMnyPessoaFisica']);
 					$_SESSION['sMsg'] = "Pesssoa Física alterado com sucesso!";
 					$sHeader = "?bError=0&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaFisica=".$_POST['fPesfisCodigo'];
					//$sHeader = "?bErro=0&action=MnyPessoaFisica.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Pesssoa Física!";
 					$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaFisica=".$_POST['fPesfisCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyPessoaFisica']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyPessoaFisica($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Pesssoa Física(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaFisica.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Pesssoa Física!";
 					$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
