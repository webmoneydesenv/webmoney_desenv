<?php
 class MnySaldoDiarioAplicacaoCTR implements IControle{
 
 	public function MnySaldoDiarioAplicacaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFachadaFinanceiroBD();
 
 		$voMnySaldoDiarioAplicacao = $oFachada->recuperarTodosMnySaldoDiarioAplicacao();
 
 		$_REQUEST['voMnySaldoDiarioAplicacao'] = $voMnySaldoDiarioAplicacao;
 		
 		
 		include_once("view/financeiro/mny_saldo_diario_aplicacao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFachadaFinanceiroBD();
 
 		$oMnySaldoDiarioAplicacao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnySaldoDiarioAplicacao = ($_POST['fIdMnySaldoDiarioAplicacao'][0]) ? $_POST['fIdMnySaldoDiarioAplicacao'][0] : $_GET['nIdMnySaldoDiarioAplicacao'];
 	
 			if($nIdMnySaldoDiarioAplicacao){
 				$vIdMnySaldoDiarioAplicacao = explode("||",$nIdMnySaldoDiarioAplicacao);
 				$oMnySaldoDiarioAplicacao = $oFachada->recuperarUmMnySaldoDiarioAplicacao($vIdMnySaldoDiarioAplicacao[0],$vIdMnySaldoDiarioAplicacao[1]);
 			}
 		}
 		
 		$_REQUEST['oMnySaldoDiarioAplicacao'] = ($_SESSION['oMnySaldoDiarioAplicacao']) ? $_SESSION['oMnySaldoDiarioAplicacao'] : $oMnySaldoDiarioAplicacao;
 		unset($_SESSION['oMnySaldoDiarioAplicacao']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_saldo_diario_aplicacao/detalhe.php");
 		else
 			include_once("view/financeiro/mny_saldo_diario_aplicacao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnySaldoDiarioAplicacao = $oFachada->inicializarMnySaldoDiarioAplicacao($_POST['fCodSaldoDiario'],$_POST['fCodAplicacao'],$_POST['fValor'],mb_convert_case($_POST['fOperacao'],MB_CASE_UPPER, "UTF-8"));
 			$_SESSION['oMnySaldoDiarioAplicacao'] = $oMnySaldoDiarioAplicacao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("CodSaldoDiario", $oMnySaldoDiarioAplicacao->getCodSaldoDiario(), "number", "y");
			$oValidate->add_number_field("CodAplicacao", $oMnySaldoDiarioAplicacao->getCodAplicacao(), "number", "y");
			//$oValidate->add_number_field("Valor", $oMnySaldoDiarioAplicacao->getValor(), "number", "y");
			$oValidate->add_text_field("Operacao", $oMnySaldoDiarioAplicacao->getOperacao(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnySaldoDiarioAplicacao.preparaFormulario&sOP=".$sOP."&nIdMnySaldoDiarioAplicacao=".$_POST['fCodSaldoDiario']."||".$_POST['fCodAplicacao']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao)){
 					unset($_SESSION['oMnySaldoDiarioAplicacao']);
 					$_SESSION['sMsg'] = "mny_saldo_diario_aplicacao inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiarioAplicacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o mny_saldo_diario_aplicacao!";
 					$sHeader = "?bErro=1&action=MnySaldoDiarioAplicacao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao)){
 					unset($_SESSION['oMnySaldoDiarioAplicacao']);
 					$_SESSION['sMsg'] = "mny_saldo_diario_aplicacao alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiarioAplicacao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o mny_saldo_diario_aplicacao!";
 					$sHeader = "?bErro=1&action=MnySaldoDiarioAplicacao.preparaFormulario&sOP=".$sOP."&nIdMnySaldoDiarioAplicacao=".$_POST['fCodSaldoDiario']."||".$_POST['fCodAplicacao']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnySaldoDiarioAplicacao']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnySaldoDiarioAplicacao($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "mny_saldo_diario_aplicacao(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnySaldoDiarioAplicacao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) mny_saldo_diario_aplicacao!";
 					$sHeader = "?bErro=1&action=MnySaldoDiarioAplicacao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
