<?php
 class MnyContratoTipoCTR implements IControle{
 
 	public function MnyContratoTipoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyContratoTipo = $oFachada->recuperarTodosMnyContratoTipo();
 
 		$_REQUEST['voMnyContratoTipo'] = $voMnyContratoTipo;
 		
 		
 		include_once("view/financeiro/mny_contrato_tipo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyContratoTipo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContratoTipo = ($_POST['fIdMnyContratoTipo'][0]) ? $_POST['fIdMnyContratoTipo'][0] : $_GET['nIdMnyContratoTipo'];
 	
 			if($nIdMnyContratoTipo){
 				$vIdMnyContratoTipo = explode("||",$nIdMnyContratoTipo);
 				$oMnyContratoTipo = $oFachada->recuperarUmMnyContratoTipo($vIdMnyContratoTipo[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyContratoTipo'] = ($_SESSION['oMnyContratoTipo']) ? $_SESSION['oMnyContratoTipo'] : $oMnyContratoTipo;
 		unset($_SESSION['oMnyContratoTipo']);
		
		$_REQUEST['voMnyContratoTipo'] = $oFachada->recuperarTodosMnyContratoTipo();
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_contrato_tipo/detalhe.php");
 		else
 			include_once("view/financeiro/mny_contrato_tipo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyContratoTipo = $oFachada->inicializarMnyContratoTipo($_POST['fCodContratoTipo'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
 																									
			$_SESSION['oMnyContratoTipo'] = $oMnyContratoTipo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			if($sOP == 'Alterar')
	 		$oValidate->add_number_field("CodContratoTipo", $oMnyContratoTipo->getCodContratoTipo(), "number", "y");
			$oValidate->add_text_field("Descri&ccedil;&atilde;o", $oMnyContratoTipo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyContratoTipo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyContratoTipo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoTipo=".$_POST['fCodContratoTipo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyContratoTipo($oMnyContratoTipo)){
 					unset($_SESSION['oMnyContratoTipo']);
 					$_SESSION['sMsg'] = "Tipo de Contrato inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Tipo de Contrato!";
 					$sHeader = "?bErro=1&action=MnyContratoTipo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyContratoTipo($oMnyContratoTipo)){
 					unset($_SESSION['oMnyContratoTipo']);
 					$_SESSION['sMsg'] = "Tipo de Contrato alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Contrato!";
 					$sHeader = "?bErro=1&action=MnyContratoTipo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoTipo=".$_POST['fCodModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fIdMnyContratoTipo'] as $nIdMnyContratoTipo){
 					$vIdMnyContratoTipo = explode("||",$nIdMnyContratoTipo);
 					$bResultado &= $oFachada->excluirMnyContratoTipo($vIdMnyContratoTipo[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "Tipo de Contrato(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoTipo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&ailde;o foi poss&iacute;vel excluir o(s) Tipo de Contrato!";
 					$sHeader = "?bErro=1&action=MnyContratoTipo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
