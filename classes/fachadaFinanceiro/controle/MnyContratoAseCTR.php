<?php
 class MnyContratoAseCTR implements IControle{
 
 	public function MnyContratoAseCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyContratoAse = $oFachada->recuperarTodosMnyContratoAse();
 
 		$_REQUEST['voMnyContratoAse'] = $voMnyContratoAse;
 		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();

		
 		
 		include_once("view/financeiro/mny_contrato_ase/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyContratoAse = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContratoAse = ($_POST['fIdMnyContratoAse'][0]) ? $_POST['fIdMnyContratoAse'][0] : $_GET['nIdMnyContratoAse'];
 	
 			if($nIdMnyContratoAse){
 				$vIdMnyContratoAse = explode("||",$nIdMnyContratoAse);
 				$oMnyContratoAse = $oFachada->recuperarUmMnyContratoAse($vIdMnyContratoAse[0]);
 			
			}	

		}
 		
 		$_REQUEST['oMnyContratoAse'] = ($_SESSION['oMnyContratoAse']) ? $_SESSION['oMnyContratoAse'] : $oMnyContratoAse;
 		unset($_SESSION['oMnyContratoAse']);
 
 		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_contrato_ase/detalhe.php");
 		else
 			include_once("view/financeiro/mny_contrato_ase/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyContratoAse = $oFachada->inicializarMnyContratoAse($_POST['fContratoCodigo'],$_POST['fItem'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fValor'],$_POST['fAtivo']);
 			$_SESSION['oMnyContratoAse'] = $oMnyContratoAse;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("ContratoCodigo", $oMnyContratoAse->getContratoCodigo(), "number", "y");
			$oValidate->add_number_field("Item", $oMnyContratoAse->getItem(), "number", "y");
			$oValidate->add_text_field("Descricao", $oMnyContratoAse->getDescricao(), "text", "y");
			$oValidate->add_number_field("Valor", $oMnyContratoAse->getValor(), "number", "y");
			$oValidate->add_number_field("Ativo", $oMnyContratoAse->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyContratoAse.preparaFormulario&sOP=".$sOP."&nIdMnyContratoAse=".$_POST['fContratoCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyContratoAse($oMnyContratoAse)){
 					unset($_SESSION['oMnyContratoAse']);
 					$_SESSION['sMsg'] = "mny_contrato_ase inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoAse.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o mny_contrato_ase!";
 					$sHeader = "?bErro=1&action=MnyContratoAse.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyContratoAse($oMnyContratoAse)){
 					unset($_SESSION['oMnyContratoAse']);
 					$_SESSION['sMsg'] = "mny_contrato_ase alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoAse.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o mny_contrato_ase!";
 					$sHeader = "?bErro=1&action=MnyContratoAse.preparaFormulario&sOP=".$sOP."&nIdMnyContratoAse=".$_POST['fContratoCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContratoAse']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyContratoAse($sCampoChave);\n");
				}
 				$_REQUEST['voMnyContratoAse'] = $oFachada->recuperarTodosMnyContratoAsePorContrato($voRegistros[0]);
				include_once('view/financeiro/mny_contrato_ase/itens_ajax.php');
				exit();
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
