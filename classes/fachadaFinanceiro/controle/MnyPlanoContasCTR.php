<?php
 class MnyPlanoContasCTR implements IControle{
 
 	public function MnyPlanoContasCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$voMnyPlanoContas = $oFachada->recuperarTodosMnyPlanoContas();
 		$_REQUEST['voMnyPlanoContas'] = $voMnyPlanoContas;
 		include_once("view/financeiro/mny_plano_contas/index.php");
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyPlanoContas = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyPlanoContas = ($_POST['fIdMnyPlanoContas'][0]) ? $_POST['fIdMnyPlanoContas'][0] : $_GET['nIdMnyPlanoContas'];
 			if($nIdMnyPlanoContas){
 				$vIdMnyPlanoContas = explode("||",$nIdMnyPlanoContas);
 				$oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($vIdMnyPlanoContas[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyPlanoContas'] = ($_SESSION['oMnyPlanoContas']) ? $_SESSION['oMnyPlanoContas'] : $oMnyPlanoContas;
 		unset($_SESSION['oMnyPlanoContas']);

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_plano_contas/detalhe.php");
 		else
 			include_once("view/financeiro/mny_plano_contas/insere_altera.php");
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
			
			$sLogado = date('d/m/Y') ." || ". $_SESSION['oUsuarioImoney']->getLogin();
			
			if($sOP =='Alterar'){
				$_POST['fUsuAlt'] = $sLogado;
			}

 			$oMnyPlanoContas = $oFachada->inicializarMnyPlanoContas($_POST['fPlanoContasCodigo'],$_POST['fCodigo'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fVisivel'],$_POST['fUsuInc'],$_POST['fUsuAlt'],$_POST['fAtivo']);
 																								
			$_SESSION['oMnyPlanoContas'] = $oMnyPlanoContas;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("C&oacute;digo", $oMnyPlanoContas->getCodigo(), "text", "y");
			$oValidate->add_text_field("Descri&ccedil;&atilde;o", $oMnyPlanoContas->getDescricao(), "text", "y");
			$oValidate->add_text_field("Inc", $oMnyPlanoContas->getUsuInc(), "text", "y");
			//$oValidate->add_text_field("UsuAlt", $oMnyPlanoContas->getUsuAlt(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyPlanoContas->getAtivo(), "number", "y");
			$oValidate->add_number_field("Visivel", $oMnyPlanoContas->getVisivel(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPlanoContas.preparaFormulario&sOP=".$sOP."&nIdMnyPlanoContas=".$_POST['fPlanoContasCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyPlanoContas($oMnyPlanoContas)){
 					unset($_SESSION['oMnyPlanoContas']);
 					$_SESSION['sMsg'] = "Plano de Contas inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPlanoContas.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Plano de Contas!";
 					$sHeader = "?bErro=1&action=MnyPlanoContas.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyPlanoContas($oMnyPlanoContas)){
 					unset($_SESSION['oMnyPlanoContas']);
 					$_SESSION['sMsg'] = "Plano de Contas alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPlanoContas.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Plano de Contas!";
 					$sHeader = "?bErro=1&action=MnyPlanoContas.preparaFormulario&sOP=".$sOP."&nIdMnyPlanoContas=".$_POST['fPlanoContasCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyPlanoContas']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyPlanoContas($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Plano de Contas(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPlanoContas.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Plano de Contas!";
 					$sHeader = "?bErro=1&action=MnyPlanoContas.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}

    public function preparaUnidade(){
         $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $UniCodigo = $_REQUEST['UniCodigo'];
        if($UniCodigo){
             $UniCodigo = explode('||',$UniCodigo);
             for($i=0;count($UniCodigo)>$i;$i++){
                 $oFachadaFinanceiro->preparaUnidadeRelatorio($UniCodigo[$i]);
             }
         }
    }
 }
 
 
 ?>
