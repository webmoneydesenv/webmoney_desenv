<?php
 class MnyMovimentoCTR implements IControle{
 
 	public function MnyMovimentoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new FachadaViewBD();
 		/*$sTipo = $_REQUEST['fMovTipo'];
		if($sTipo)
	 		$voMnyMovimento = $oFachada->recuperarTodosMnyMovimentoPorTipo($sTipo);
 		$_REQUEST['voMnyMovimento'] = $voMnyMovimento;*/
		
		$sGrupo = '5';//Tipo de Lançamento
		$_REQUEST['voMnyTipoLancamento'] = $oFachada-> recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$voPessoa = $oFachadaView->recuperarTodosVPessoaFormatada();
 		$_REQUEST['voPessoa'] = $voPessoa;
		
		if($_REQUEST['fNome'] || $_REQUEST['fIdentificacao'] || $_REQUEST['fFichaAceite'] || $_REQUEST['fDataInicial'] || $_REQUEST['fMovTipo']) {		
			$sComplemento = " WHERE ";
			$sDescricao = " - Por:";
			if($_REQUEST['fMovTipo']){
				$sComplemento .= "mov_tipo = " . $_REQUEST['fMovTipo'] . " and "  ;
				if($_REQUEST['fMovTipo'] == 188)	
					$sTipoMov = "A Pagar";
				else
					$sTipoMov = "A Receber";
				$sDescricao .=  " Tipo ". $sTipoMov  ."; ";
			}

			if($_REQUEST['fNome']){
				$sComplemento .= "nome = '" . $_REQUEST['fNome'] . "' and "  ;	
				$sDescricao .=  " Nome ". $_REQUEST['fNome'] ."; ";
			}
			if($_REQUEST['fIdentificacao']){
				$sComplemento .= "identificacao = '" . $_REQUEST['fIdentificacao'] . "' and "  ;	
				$sDescricao .=  " CNPJ/CPF ". $_REQUEST['fIdentificacao'] ."; ";

			}
			if($_REQUEST['fFichaAceite']){
				$vFichaAceite = explode(".",$_REQUEST['fFichaAceite']);
				if($vFichaAceite[1]){
					$sComplemento .= "mov_item = " . $vFichaAceite[1] . " and "  ;	
				}
				$sComplemento .= "mov_codigo = " . $vFichaAceite[0] . " and "  ;	
				$sDescricao .=  " F.A <strong>". $_REQUEST['fFichaAceite'] . "</strong>"."; ";
				
			}
			if($_REQUEST['fDataInicial']){
				if($_REQUEST['fDataFinal']){
					$sDataFinal = $_REQUEST['fDataFinal'];
				}else{
					$sDataFinal = date('d/m/Y');
				}
				$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataInicial']);
		        $sDataInicialBanco = $oData->format('Y-m-d') ;
				
				$oData = DateTime::createFromFormat('d/m/Y', $sDataFinal);
		        $sDataFinalBanco = $oData->format('Y-m-d') ;
				if($_REQUEST['fInclusao'] == 1){
					$sCampoData = "mov_data_emissao";
				}else{
					$sCampoData = "mov_data_vencto";
				}
				
				$sComplemento .= $sCampoData. " between '" . $sDataInicialBanco . "' and '"  . $sDataFinalBanco . "' and " ;	
				$sDescricao .=  " Periodo ". $_REQUEST['fDataInicial'] ." e ". $sDataFinal . ";";
			}
				  $sComplemento = substr( $sComplemento , 0, -4);

				$_REQUEST['sDescricao'] = $sDescricao;
				$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplemento($sComplemento); 
		}
		
 		include_once("view/financeiro/mny_movimento/pesquisa.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
		$oFachadaView = new FachadaViewBD();
 
 		//$oMnyMovimento = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyMovimento = ($_POST['fIdMnyMovimento'][0]) ? $_POST['fIdMnyMovimento'][0] : $_GET['nIdMnyMovimento'];
			 
 			if($nIdMnyMovimento){
 				$vIdMnyMovimento = explode("||",$nIdMnyMovimento);
 				$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($vIdMnyMovimento[0]);				
				$_REQUEST['oMnyContratoPessoa'] = $oMnyMovimento->getMnyContratoPessoa();
				$_REQUEST['voMnyMovimentoItem'] = $oFachada->recuperarTodosMnyMovimentoItemPorMovimento($vIdMnyMovimento[0]);
				
 			}
 		}
 		
 		$_REQUEST['oMnyMovimento'] = ($_SESSION['oMnyMovimento']) ? $_SESSION['oMnyMovimento'] : $oMnyMovimento;
 		unset($_SESSION['oMnyMovimento']);
		
		//$sGrupo = '2.'; // Centro de Custo
		//$_REQUEST['voMnyCentroCusto'] = $oFachada->recuperarTodosMnyPlanoContasCabeca($sGrupo);
		
		$sGrupo = '2'; // Conta
		$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '3'; // Tipo de aceite
		$_REQUEST['voMnyTipoAceite'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '4'; // Tipo de doc
		$_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '7'; // Centro de Negocio
		$_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '8'; // Unidade
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				
		$sGrupo = '10'; // Setor
		$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				
		$sGrupo = '11'; // Forma de Pag	
		$_REQUEST['voMnyFormaPagamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '5';//Tipo de Lançamento
		$_REQUEST['voMnyTipoLancamento'] = $oFachada-> recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		
		if($_REQUEST['sTipoLancamento'] == 'Pagar'){
			$_REQUEST['nTipo'] = 188;
			$sGrupo = '6'; // Custo
			$_REQUEST['voMnyCusto'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
			$sGrupo = 2; // Centro de Custo
			$_REQUEST['voMnyCentroCusto'] = $oFachada->recuperarTodosMnyPlanoContasCabeca($sGrupo);
		}elseif($_REQUEST['sTipoLancamento'] == 'Receber'){
			$_REQUEST['nTipo'] = 189;
		}
 
 		$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
		$_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();

		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
		$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();

		
		//documentos	
		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();
		
 		if($_REQUEST['sOP'] == "Transferir"){
 			include_once("view/financeiro/mny_movimento/insere_altera_trans.php");
 		}else{
			$_REQUEST['sPagina'] = $_REQUEST['sPagina'];
 			include_once("view/financeiro/mny_movimento/insere_altera.php");
 			}
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$_SESSION['sOP'] = $_REQUEST['sOP']; //(array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
		$sOP = $_SESSION['sOP'];
 		if($sOP != "Excluir"){
			$sLogado = date('d/m/Y') . $_SESSION['oUsuarioImoney']->getLogin();
			if($sOP =='Cadastrar' || $sOP =='Transferir'){
				$_POST['fMovDataInclusao'] = date('d/m/Y');
			}
						
			$_POST['fEmpCodigo']= $_SESSION['oEmpresa']->getEmpCodPessoa();
			 
			//adiciona o contrato no movimento...
			if($sOP == 'Alterar' && $_REQUEST['Pagina'] == 3){	
				$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);
				$oMnyMovimento->setMovContrato($_REQUEST['fIdMnyContratoPessoa']);		
			}else{	
				if($sOP == 'Transferir'){
					$_POST['fPesCodigo'] = $_POST['fEmpCodigo'];
				}
				$oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigo'],$_POST['fSetCodigo'],$_POST['fMovObs'],$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],$_POST['fMovDocumento'],$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovValorGlob'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],$_POST['fAtivo'],$_POST['fMovOutrosDesc']);				
			}		

			$_SESSION['oMnyMovimento'] = $oMnyMovimento;	
				
			if($sOP =='Cadastrar' || $sOP =='Transferir'){
				$oMnyMovimento->setMovInc($sLogado);
			}elseif($sOP =='Alterar'){
				$oMnyMovimento->setMovAlt($sLogado);
			}
			

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 		//	$oValidate->add_date_field("Data de Inclus&atilde;o", $oMnyMovimento->getMovDataInclusao(), "date", "y");
		//	$oValidate->add_date_field("Data de Emiss&atilde;o", $oMnyMovimento->getMovDataEmissao(), "date", "y");
			$oValidate->add_number_field("Unid Neg&oaccute;cio", $oMnyMovimento->getNegCodigo(), "number", "y");
			
			if($_REQUEST['sTipoLancamento'] == 'Pagar' && $sOP =! 'Transferir'){
				$oValidate->add_number_field("Unidade Custo", $oMnyMovimento->getCusCodigo(), "number", "y");
				$oValidate->add_number_field("Centro", $oMnyMovimento->getCenCodigo(), "number", "y");
			}
			
			$oValidate->add_number_field("Unidade", $oMnyMovimento->getUniCodigo(), "number", "y");
			$oValidate->add_number_field("Pessoa", $oMnyMovimento->getPesCodigo(), "number", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getConCodigo(), "number", "y");
			$oValidate->add_number_field("Setor", $oMnyMovimento->getSetCodigo(), "number", "y");
			//$oValidate->add_text_field("Obs", $oMnyMovimento->getMovObs(), "text", "y");
			$oValidate->add_text_field("Incluído por", $oMnyMovimento->getMovInc(), "text", "y");
			if($sOP == 'Alterar')
				$oValidate->add_text_field("MovAlt", $oMnyMovimento->getMovAlt(), "text", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getMovContrato(), "number", "y");
			$oValidate->add_text_field("Documento", $oMnyMovimento->getMovDocumento(), "text", "y");
			if($sOP =! 'Transferir')
			$oValidate->add_number_field("Parcelas", $oMnyMovimento->getMovParcelas(), "number", "y");
			
			
			$oValidate->add_number_field("Tipo", $oMnyMovimento->getMovTipo(), "number", "y");
			$oValidate->add_number_field("EmpCodigo", $oMnyMovimento->getEmpCodigo(), "number", "y");
			//$oValidate->add_number_field("Icms", $oMnyMovimento->getMovIcmsAliq(), "number", "y");
			//$oValidate->add_number_field("MovValorGlob", $oMnyMovimento->getMovValorGlob(), "number", "y");
			//$oValidate->add_number_field("Pis", $oMnyMovimento->getMovPis(), "number", "y");
			//$oValidate->add_number_field("Confins", $oMnyMovimento->getMovConfins(), "number", "y");
			//$oValidate->add_number_field("Csll", $oMnyMovimento->getMovCsll(), "number", "y");
			//$oValidate->add_number_field("Iss", $oMnyMovimento->getMovIss(), "number", "y");
			//$oValidate->add_number_field("Ir", $oMnyMovimento->getMovIr(), "number", "y");
			//$oValidate->add_number_field("Irrf", $oMnyMovimento->getMovIrrf(), "number", "y");
			//$oValidate->add_number_field("Inss", $oMnyMovimento->getMovInss(), "number", "y");
			$oValidate->add_number_field("Ativo", $oMnyMovimento->getAtivo(), "number", "y");
 			
 			if (!$oValidate->validation()) {
				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&nIdMnyMovimento=".$_POST['fMovCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
				

 		switch($sOP){
 			case "Cadastrar":
 				
 				if($nMovCodigo = $oFachada->inserirMnyMovimento($oMnyMovimento)){
					
					$nDias = $_REQUEST['fPeriodicidade'];
					$nParcelas = $_REQUEST['fMovParcelas'];
					$_POST['fMovValor']  = round ($oMnyMovimento->getMovValorGlob()/ $_REQUEST['fMovParcelas'],2) ;
					
					for ($i = 1; $i <= $nParcelas; $i++){
						$_POST['fMovItem'] = $i;
						
						// adicionar periodicidade para data de vencimento
						if($i==1){
							$dData = $_POST['fMovDataVencto'];
							$dCompetencia = $_POST['fCompetencia'];
						}else{
							$oData = new Data($_POST['fMovDataVencto']);
							$dData = $oData->avancaData($nDias * ($i-1));
							$vData = explode("/",$dData);
							$dCompetencia =  $vData[1] . "/" . $vData[2];
						}
						$_POST['fMovDataPrev'] = date('d/m/Y');
						
						$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$dData,$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],$_POST['fTipAceCodigo'],$dCompetencia,1);
						$oMnyMovimentoItem->setMovCodigo($nMovCodigo);

						$oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem);
 						unset($_SESSION['oMnyMovimentoItem']);
					}
 					//unset($_SESSION['oMnyMovimento']);
 					//$_SESSION['sMsg'] = "Movimento inserido com sucesso!";

					//$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimento=".$nMovCodigo;				
					
					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=" .$_REQUEST['sTipoLancamento']. "&fPesCodigo=" . $_REQUEST['fPesCodigo'] . "&fMovCodigo=" . $nMovCodigo;		
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
 				}
 			break;
			case "Transferir":
 				if($nMovCodigo = $oFachada->inserirMnyMovimento($oMnyMovimento)){
					unset($_SESSION['oMnyMovimento']);
 					$_SESSION['sMsg'] = "Transferência realizada com sucesso!";
					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=" .$_REQUEST['sTipoLancamento']. "&fPesCodigo=" . $_REQUEST['fPesCodigo'] . "&fMovCodigo=" . $nMovCodigo;		
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel realizar a tranferência!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyMovimento($oMnyMovimento)){
 					unset($_SESSION['oMnyMovimento']); 					
					if($_REQUEST['Pagina']==3){
						$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&nIdMnyMovimento=".$_REQUEST['fMovCodigo']."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nCodContrato=".$_REQUEST['fIdMnyContratoPessoa'];					
						$_SESSION['sMsg'] = "Movimento inserido com sucesso!";
					}else{
	 					$sHeader = "?bErro=0&action=MnyMovimento.preparaLista";
					}
					$_SESSION['sMsg'] = "Movimento alterado com sucesso!";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Movimento!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."nIdMnyMovimento=".$_POST['fMovCodigo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				
 				$vIdPaiMnyMovimento = explode("____",$_REQUEST['fIdMnyMovimento']);
   				foreach($vIdPaiMnyMovimento as $vIdFilhoMnyMovimento){
  					$vIdMnyMovimento = explode("||",$vIdFilhoMnyMovimento);
 					foreach($vIdMnyMovimento as $nIdMnyMovimento){
  						$bResultado &= $oFachada->excluirMnyMovimento($vIdMnyMovimento[0],$vIdMnyMovimento[1]);
  					}
   				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Movimento(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimento.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Movimento!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
	
	function carregaTipo(){	
		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];				
		
			$nCodCusto = $_REQUEST['fCenCodigo'];
			$oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($nCodCusto);
			$voMnyContas = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($oMnyPlanoContas->getCodigo());
			
			$_REQUEST['voMnyContas'] = $voMnyContas;
			include_once("view/financeiro/mny_movimento/centro_custo_ajax.php");
			exit();
	}
 
 
 function carregaTipoConta(){	
		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];				
		    		
			$nCodUnidade = $_REQUEST['fUniCodigo'];		
			$nCodUnidade1 = $_REQUEST['fUniCodigo1'];	
				
			if($nCodUnidade)		
			$_REQUEST['voMnyContaCorrenteUnidade'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);			
			else
			$_REQUEST['voMnyContaCorrenteUnidade'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade1);	
			
			include_once("view/financeiro/mny_movimento/tipo_conta_ajax.php");
			
			exit();
	}
 }
 ?>
