<?php
 class MnyContratoPessoaCTR implements IControle{
 
 	public function MnyContratoPessoaCTR(){
 	
 	}
 
	public function preparaListaASE(){
		$_REQUEST['fTipoContrato'] = 2;
		$this->preparaLista();	
	}
	
	public function preparaListaASEFRETE(){
		$_REQUEST['fTipoContrato'] = 10;
		$this->preparaLista();	
	}
	
	public function preparaListaOC(){
		$_REQUEST['fTipoContrato'] = 3;
		$this->preparaLista();	
	}
 
    public function preparaFormularioCONTRATO(){
        $_REQUEST['fTipoContrato'] = 1;
        $_REQUEST['nIdMnyContratoPessoa'] = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdMnyContratoPessoa'];
        $this->preparaFormulario();
    }

    public function preparaFormularioASE(){
        $_REQUEST['fTipoContrato'] = 2;
        $_REQUEST['nIdMnyContratoPessoa'] = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdMnyContratoPessoa'];
        $this->preparaFormulario();
    }
    public function preparaFormularioASEFRETE(){
        $_REQUEST['fTipoContrato'] = 10;
        $_REQUEST['nIdMnyContratoPessoa'] = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdMnyContratoPessoa'];
        $this->preparaFormulario();

    }
     public function preparaFormularioOC(){
        $_REQUEST['nIdMnyContratoPessoa'] = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdMnyContratoPessoa'];
         $_REQUEST['fTipoContrato'] = 3;
         $this->preparaFormulario();
    }
   public function preparaDocumentosAjax(){
       $oFachada = new FachadaFinanceiroBD();
       $oFachadaSys = new FachadaSysBD();
       $nTIpoContrato = $_REQUEST['nTipoContrato'];
       if($nTIpoContrato){
		  $_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($nTIpoContrato,1);
			include_once('view/financeiro/mny_contrato_pessoa/documentos_ajax.php');
		}
    }




 	public function preparaLista(){
		$_REQUEST['fTipoContrato'] = ($_REQUEST['fTipoContrato']) ?  $_REQUEST['fTipoContrato'] : 1;

		$oFachada = new FachadaFinanceiroBD();
		if($_REQUEST['fTipoContrato']){
			$voMnyContratoPessoa = $oFachada->recuperarTodosMnyContratoPessoaIndexPorTipo($_REQUEST['fTipoContrato']);
			$_REQUEST['oMnyContratoTipo'] = $oFachada->recuperarUmMnyContratoTipo($_REQUEST['fTipoContrato']);
		}else{
 			$voMnyContratoPessoa = $oFachada->recuperarTodosMnyContratoPessoaIndex($_SESSION['oEmpresa']->getEmpCodigo());
		}
 		$_REQUEST['voMnyContratoPessoa'] = $voMnyContratoPessoa;


		if($_REQUEST['sOP'] == 'Anexar'){		
	 		include_once("view/financeiro/mny_contrato_pessoa/anexo_documento.php");
		}else{
			include_once("view/financeiro/mny_contrato_pessoa/index.php");
		}
 		exit();	
 	}
	
	public function preparaLista2(){
 		$oFachada = new FachadaFinanceiroBD();
 		
		switch($_REQUEST['sOpcao']){			
			case "select":
				$nCodPessoa = $_REQUEST['nIdPesgCodigo'];
				$voMnyContratoPessoa = $oFachada->recuperarTodosMnyContratoPessoaPorPessoa($nCodPessoa);
				$_REQUEST['voMnyContratoPessoa'] = $voMnyContratoPessoa;
				include_once("view/financeiro/mny_contrato_pessoa/contrato_ajax.php");
				exit();
			break;
			case "text":
				$nCodContrato = $_REQUEST['nIdContratoPessoa'];
				$oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($nCodContrato);
				include_once("view/financeiro/mny_contrato_pessoa/valor_ajax.php");
				exit();
			break;
		}
 	
 	}	
	
	public function upload($arquivo,$nCodContratoDocTipo,$nCodContrato2,$nCodContratoArquivo2=""){
				$oFachada = new FachadaFinanceiroBD();
				$oFachadaSys = new FachadaSysBD();
				$i=0;
				$nErro=0;

				$vCampoChave = array();
				$nTotalArquivos=0;
				$arquivos = $arquivo; 
				$nCodContrato = $_POST['fContratoCodigo'];				
				$tamanhoMaximo = 100000000;				
				$nCodContratoArquivo = $nCodContratoArquivo2;//$_REQUEST['fCodContratoArquivo'];
				$nCodContratoDocTipo = $nCodContratoDocTipo;
				$nCodContrato = $nCodContrato2;	
				$vArquivosCadastrados = array();
			
				
				for($i = 0 ; count($arquivos['name']) > $i  ;$i++){	
				
					if($arquivos['name'][$i] != ""){
					 	
						$oTipoDocumento = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i] );
						$oTipoDocumento = str_replace(" ","-",strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($oTipoDocumento->getDescricao())), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ/"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) ));
						$arquivos['name'][$i] = str_replace(" ","-",strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($arquivos['name'][$i])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ/"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) ));
						// classUpload
						$nomeArquivo =  $nCodContrato . "_" . $oTipoDocumento . "_" . $arquivos['name'][$i];
						$tamanhoArquivo = $arquivos['size'][$i];
						$nomeTemporario = $arquivos['tmp_name'][$i];
						$tipoArquivo = strtok($arquivos['type'][$i], ";");

						$vPermissao = array("application/pdf");
					
					//verificacao de erros	
						if($tamanhoArquivo <= 0){	              $_SESSION['sMsg'] = "Erro 1: Erro no tamanho do arquivo: ".$tamanhoArquivo ." Kb. Tamanho Maximo é ".$tamanhoMaximo." !";	$nErro++; }
						if($tamanhoArquivo > $tamanhoMaximo){     $_SESSION['sMsg'] = "Erro 2: Tamanho máximo é: ".$tamanhoMaximo." KB!"; $nErro++;     }
						if(!is_uploaded_file($nomeTemporario)){   $_SESSION['sMsg'] = "Erro 3: Ocorreu um erro na transfêrencia do arquivo"; $nErro++;  }
						if (!in_array($tipoArquivo, $vPermissao)){$_SESSION['sMsg'] = "Erro 4: Arquivo precisa ser PDF."; $nErro++;           			}

						
                        if($nCodContratoDocTipo[$i] == 3 || $nCodContratoDocTipo[$i]==10){

                             if($_FILES["arquivo"]["tmp_name"][$i]){
                                $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                $Tipo = $_FILES["arquivo"]["type"][$i];
                                move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                $nomeArquivo = "CONTRATO_" . $nCodContrato2 . "_" . $nCodContratoDocTipo[$i];


                                 //  USADO PARA ARMAZENAR O ARQUIVO NO BANCO DE DADOS...
                                 $oSysArquivo = $oFachadaSys->inicializarSysArquivo($_POST['fCodArquivo'][$i],$nomeArquivo,$Anexo,$Tipo,$nCodContratoDocTipo[$i],$tamanhoArquivo,date('Y-m-d H:i:s'),mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),1,$nCodContrato2,'NULL', 'NULL','NULL');

                                 $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                                 $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];
                                if($_POST['fCodArquivo'][$i]){
                                    $oFachadaSys->alterarSysArquivo($oSysArquivo);
                                }else{
                                    if(!$nIdArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo)){
                                        $nErro++;
                                        $_SESSION['sMsg'] = "Erro 6: não inserido no banco";
                                    }else{
                                        $vIdArquivo[] = $nIdArquivo;
                                    }
                                }
                            }


                        }else{
                            $caminho = $_SERVER['DOCUMENT_ROOT'] . "/webmoney/arquivos/";
                            if(move_uploaded_file($nomeTemporario, ($caminho . $nomeArquivo))){
                                $sNomeArquivoBD = "arquivos/" . $nomeArquivo;
                                $oContratoDocTipoItem = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i]);
                                $_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i');

                       //   if($nCodContratoArquivo[$i] == "" ){$nCodContratoArquivo[$i] = $nCodContratoArquivo[$i -1];}

                                if((!(empty($nCodContratoArquivo[$i]))) && ($oContratoDocTipoItem->getItem() == 0)){
                                    $oMnyContratoArquivo = $oFachada->recuperarUmMnyContratoArquivo($nCodContratoArquivo[$i]);
                                    $vArquivo = explode("/",$oMnyContratoArquivo->getNome());
                                    $sNomeCompleto = $_SERVER['DOCUMENT_ROOT'] . "/webmoney/" . $oMnyContratoArquivo->getNome();
                                    unlink($sNomeCompleto);
                                    $oFachada->excluirMnyContratoArquivo($nCodContratoArquivo[$i]);
                                }
                                $nCodConciliacao = NULL;
                                $oMnyContratoArquivo = $oFachada->inicializarMnyContratoArquivo($_POST['fCodContratoArquivo'],$nCodContrato,$nCodConciliacao, $_POST['fMovCodigo'],$_POST['fMovItem'],$nCodContratoDocTipo[$i],$sNomeArquivoBD,$_POST['fRealizadoPor'],1);

                                if(!$nIdContratoArquivo = $oFachada->inserirMnyContratoArquivo($oMnyContratoArquivo)){
                                    $nErro++;
                                }else{
                                    $vArquivosCadastrados['id'][] = $nIdContratoArquivo;
                                    $vArquivosCadastrados['caminho'][] = $_SERVER['DOCUMENT_ROOT'] . "/webmoney/" . $sNomeArquivoBD;
                                }
                          }else{
                                $_SESSION['sMsg'] = "Erro 5: Ocorreu um erro na transfêrencia do arquivo";
                                $nErro++;
                           }
					}
					
				}
                }
				
				if($nErro == 0){
					return true;				
				}else {
					for($j = 0 ; count($vArquivosCadastrados) > $j  ;$j++){
						unlink($vArquivosCadastrados['caminho'][$j]);
						$oFachada->excluirMnyContratoArquivo($vArquivosCadastrados['id'][$j]);
					}
					return false;				
				}
				

	}//fim função
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
		$oFachadaView = new FachadaViewBD();

 		$oMnyContratoPessoa = false;

		switch($_REQUEST['sOP']){
			case 'Cadastrar' || 'Alterar' || 'Detalhar':				
				if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
					$nIdMnyContratoPessoa = ($_POST['fIdMnyContratoPessoa'][0]) ? $_POST['fIdMnyContratoPessoa'][0] : $_GET['nIdMnyContratoPessoa']; 			

					if($nIdMnyContratoPessoa){			
						$vIdMnyContratoPessoa = explode("||",$nIdMnyContratoPessoa);
						$_REQUEST['oMnyContratoPessoa'] = $oFachada->recuperarUmMnyContratoPessoa($vIdMnyContratoPessoa[0]);
						if($_REQUEST['fTipoContrato']!=1)
                            $_REQUEST['voMnyContratoArquivo'] = $oFachada->recuperarTodosMnyContratoArquivoPorContrato($vIdMnyContratoPessoa[0]);
                        else
                            $_REQUEST['voMnyContratoArquivo'] =  $oFachadaSys->recuperarTodosSysArquivoPorContratoTipoArquivo($vIdMnyContratoPessoa[0],$_REQUEST['fTipoContrato']);

                        $_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($_REQUEST['oMnyContratoPessoa']->getTipoContrato(),1);
						//$_REQUEST['voMnyContratoAditivo'] = $oFachada->recuperarTodosMnyContratoAditivoPorContrato($_REQUEST['oMnyContratoPessoa']->getTipoContrato());
						$_REQUEST['voMnyContratoAditivo'] = $oFachada->recuperarTodosMnyContratoAditivoPorContrato($_REQUEST['oMnyContratoPessoa']->getContratoCodigo());
						
						if($_REQUEST['oMnyContratoPessoa'] && ($_REQUEST['oMnyContratoPessoa']->getTipoContrato() == 2 || $_REQUEST['oMnyContratoPessoa']->getTipoContrato() == 10))
						$_REQUEST['voMnyContratoAse'] = $oFachada->recuperarTodosMnyContratoAsePorContrato($vIdMnyContratoPessoa[0]);
						$voMnyContratoAse = $_REQUEST['voMnyContratoAse'];
					}
				}
				
				//$_REQUEST['voMnyContratoArquivo'] = $oFachada->recuperarTodosMnyContratoArquivoPorContrato($nIdMnyContratoPessoa);
				$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
				if(!($_REQUEST['oMnyContratoPessoa']))
					$_REQUEST['oMnyContratoPessoa'] = $_SESSION['oMnyContratoPessoa'];
				
				$_REQUEST['voMnyContratoTipo'] = $oFachada->recuperarTodosMnyContratoTipo();
				
				//$sGrupo = 8;// unidade
				//$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = 10;// unidade
				$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
				$sGrupo = '2'; // Conta
				$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '3'; // Tipo de aceite
				$_REQUEST['voMnyTipoAceite'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '4'; // Tipo de doc
				$_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '7'; // Centro de Negocio
				$_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				//$sGrupo = '8'; // Unidade
				//$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
                $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());
				$sGrupo = '10'; // Setor
				$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '11'; // Forma de Pag	
				$_REQUEST['voMnyFormaPagamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '5';//Tipo de Lançamento
				$_REQUEST['voMnyTipoLancamento'] = $oFachada-> recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = '6'; // Custo
				$_REQUEST['voMnyCusto'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				$sGrupo = 2; // Centro de Custo
				$_REQUEST['voMnyCentroCusto'] = $oFachada->recuperarTodosMnyPlanoContasCabeca($sGrupo);
				$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
                $_REQUEST['nSaldoContrato'] = $oFachada->recuperarUmMnyContratoPessoaSaldo($vIdMnyContratoPessoa[0]);
				
				$nTIpoContrato = $_REQUEST['nTipoContrato'];				
				if($_REQUEST['sOP'] == 'Detalhar'){
					$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($_REQUEST['fTipoContrato'],1);	
					$_REQUEST['voMnyMovimento'] = $oFachadaView->recuperarTodosVMovimentoPorContrato($vIdMnyContratoPessoa[0]);
				    $_REQUEST['voAditivo'] = $oFachada->recuperarTodosMnyContratoAditivoPorContrato($vIdMnyContratoPessoa[0]);
					include_once('view/financeiro/mny_contrato_pessoa/detalhe.php');
				}else{
					if($_REQUEST['fTipoContrato']){
						$_REQUEST['oMnyContratoTipo'] = $oFachada->recuperarUmMnyContratoTipo($_REQUEST['fTipoContrato']);

						if($_REQUEST['fTipoContrato'] == 2 || $_REQUEST['fTipoContrato'] == 10){
								//$_REQUEST['voMnyContratoAse'] = $oFachada->recuperarTodosMnyContratoAse();
								$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($_REQUEST['fTipoContrato'],1);
							include_once('view/financeiro/mny_contrato_pessoa/insere_altera_ase.php');
						}else{
                                include_once('view/financeiro/mny_contrato_pessoa/insere_altera.php');

						}
					}
				}
					
			break;	
		}
		exit();	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaSys = new FachadaSysBD();


        switch($_REQUEST['fTipoContrato']){

			case 1:	$sTipoContrato = "CONTRATO"; break;
			case 2:	$sTipoContrato = "ASE"; break;
			case 10:	$sTipoContrato = "ASEFRETE"; break;
			case 3:	$sTipoContrato = "OC";  break;
		}

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){	
		$_POST['fEmpCodigo'] = $_SESSION['oEmpresa']->getEmpCodigo();
		$oMnyContratoPessoa = $oFachada->inicializarMnyContratoPessoa($_POST['fContratoCodigo'],$_POST['fContratoTipo'],$_POST['fPesCodigo'],$_POST['fCodStatus'],mb_convert_case($_POST['fNumero'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fDataInicio'],$_POST['fDataContrato'],$_POST['fDataValidade'],$_POST['fValorContrato'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fSetCodigo'],$_POST['fEmpCodigo'],$_POST['fAtivo']);
																																																									
		$_SESSION['oMnyContratoPessoa'] = $oMnyContratoPessoa;
		
		$oValidate = FabricaUtilitario::getUtilitario("Validate");
		$oValidate->check_4html = true;
		
		$oValidate->add_number_field("PesCodigo", $oMnyContratoPessoa->getPesCodigo(), "number", "y");
		$oValidate->add_number_field("ContratoTipo", $oMnyContratoPessoa->getTipoContrato(), "number", "y");
		$oValidate->add_number_field("CodStatus", $oMnyContratoPessoa->getCodStatus(), "number", "y");
		$oValidate->add_text_field("Numero", $oMnyContratoPessoa->getNumero(), "text", "y");
		$oValidate->add_text_field("Descricao", $oMnyContratoPessoa->getDescricao(), "text", "y");
		//$oValidate->add_date_field("DataInicio", $oMnyContratoPessoa->getDataInicio(), "date", "y");
		$oValidate->add_date_field("DataContrato", $oMnyContratoPessoa->getDataContrato(), "date", "y");
		if($oMnyContratoPessoa->getTipoContrato() != 2 && $oMnyContratoPessoa->getTipoContrato() != 10){
			$oValidate->add_date_field("DataValidade", $oMnyContratoPessoa->getDataValidade(), "date", "y");
		}
		//$oValidate->add_number_field("ValorContrato", $oMnyContratoPessoa->getValorContrato(), "number", "y");
		$oValidate->add_number_field("Unidade Custo", $oMnyContratoPessoa->getCusCodigo(), "number", "y");
		$oValidate->add_number_field("Centro", $oMnyContratoPessoa->getCenCodigo(), "number", "y");
		$oValidate->add_number_field("Unidade", $oMnyContratoPessoa->getUniCodigo(), "number", "y");
		$oValidate->add_number_field("Unid Neg&oaccute;cio", $oMnyContratoPessoa->getNegCodigo(), "number", "y");
		$oValidate->add_number_field("Setor", $oMnyContratoPessoa->getSetCodigo(), "number", "y");
		$oValidate->add_number_field("Conta", $oMnyContratoPessoa->getConCodigo(), "number", "y");
		$oValidate->add_number_field("Ativo", $oMnyContratoPessoa->getAtivo(), "number", "y");
		
		if (!$oValidate->validation()) {
			$_SESSION['sMsg'] = $oValidate->create_msg();
		 	$sHeader = "?bErro=1&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&fTipoContrato=".$oMnyContratoPessoa->getTipoContrato()."&sOP=".$sOP."&nIdMnyContratoPessoa=".$_GET['fContratoCodigo'];
			header("Location: ".$sHeader);	
			die();
		}
		
		}		

		
 		switch($sOP){
 			case "Cadastrar":
			 	$nIdContrato = $oFachada->inserirMnyContratoPessoa($oMnyContratoPessoa);
			  if($_REQUEST['fTipoContrato'] == 2 || $_REQUEST['fTipoContrato'] == 10){
					$item = 1;
					$cont = count($_REQUEST['AseItemDescricao']);
					for($i = 0 ; $cont > $i  ;$i++){	
						$_POST['fItem'] = $item;
						$oMnyContratoAse = $oFachada->inicializarMnyContratoAse($nIdContrato,$_POST['fItem'],mb_convert_case($_REQUEST['AseItemDescricao'][$i],MB_CASE_UPPER, "UTF-8"),$_REQUEST['AseItemValor'][$i],1);				
						$oFachada->inserirMnyContratoAse($oMnyContratoAse);
						$item++;
					}
				}
		

                        $arquivos= $_FILES['arquivo'];
                        for($i = 0 ; count($arquivos['name']) > $i  ;$i++){
                           if($_FILES["arquivo"]["tmp_name"][$i]){
                                $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                $Tipo = $_FILES["arquivo"]["type"][$i];
                                $Tamanho = $_FILES["arquivo"]["size"][$i];

                                move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                unlink("c:\\wamp\\tmp\\arquivo.pdf");
                                $sNome = $sTipoContrato . '_'.$_REQUEST['fCodContratoDocTipo'][$i] . "_" . $nIdContrato;
                                $sArquivo = $Anexo;

                                $sExtensao = $Tipo;
                                $nCodTipo = $_REQUEST['fCodContratoDocTipo'][$i];
                                $nTamanho = $Tamanho;
                                $dDataHora= date('Y-m-d H:i:s');
                                $sRealizadoPor = $_SESSION['Perfil']['Login'];
                                $nAtivo = 1;
                                $nContratoCodigo = $nIdContrato;
                                $nCodConciliacao = 'NULL';
                                $nMovCodigo = 'NULL';
                                $nMovItem = 'NULL';
                                $oSysArquivo = $oFachadaSys->inicializarSysArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem);
                                $oSysArquivo->setArquivo($sArquivo);
                                $oFachadaSys->inserirSysArquivo($oSysArquivo);
                            }
                        }


					
					if($nIdContrato != 0){									
						$_SESSION['sMsg'] =  $sTipoContrato." ". $nIdContrato." inserido com sucesso!";
						$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista&fTipoContrato=".$_REQUEST['fTipoContrato'];
						unset($_SESSION['oMnyContratoPessoa']);	
					}else{
						$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir". ($_REQUEST['fTipoContrato'] == 1)? "o " : "a " . $sTipoContrato." !"; 					
						//$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&fPesCodigo=" . $_REQUEST['fPesCodigo'] . "&fMovCodigo=" . $nMovCodigo;	
						$sHeader = "?bErro=3&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&sOP=Cadastrar&fTipoContrato=".$_REQUEST['fTipoContrato'];
					}
 			break;
 			case "Alterar":

                    $oMnyContratoPessoaBanco = $oFachada->recuperarUmMnyContratoPessoa($_POST['fContratoCodigo']);
                    $sOrigem = array('.',',');
                    $sDestino = array('','.');
                    $nValorContrato = $this->nValorContrato = str_replace($sOrigem, $sDestino, $_POST['fValorContrato']);

                    //print_r($nValorContrato);
                    //echo "<br>";
                    //print_r($oMnyContratoPessoaBanco->getValorContrato());
                    //die();

                    if((($voMnyMovimentoItem = $oFachada->recuperarTodosMnyMovimentoPorContrato($_POST['fContratoCodigo'])) && ($nValorContrato < $oMnyContratoPessoaBanco->getValorContrato())) ){
                            $_SESSION['sMsg'] = "Você N&atilde;o pode Alterar o valor porque já existe movimento(s) lançado(s) para o mesmo!";
                            $sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&sOP=Alterar&fTipoContrato=".$_REQUEST['fTipoContrato']."&nIdMnyContratoPessoa=".$_POST['fContratoCodigo'];

                            header("Location: ".$sHeader);

                            exit();
                    }

				//$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&fPesgCodigo=".$nId;
 				if($oFachada->alterarMnyContratoPessoa($oMnyContratoPessoa)){
 					
				//	if($oMnyContratoPessoa->getTipoContrato() !=2){
					//	if($_REQUEST['fTipoContrato'] == 1){
                        $arquivos= $_FILES['arquivo'];
                        $nIdContrato = $oMnyContratoPessoa->getContratoCodigo();
                        for($i = 0 ; count($arquivos['name']) > $i  ;$i++){
                           if($_FILES["arquivo"]["tmp_name"][$i]){
                                $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                $Tipo = $_FILES["arquivo"]["type"][$i];
                                $Tamanho = $_FILES["arquivo"]["size"][$i];
                                //$nCodArquivo = $_REQUEST['fCodArquivo'][$i];

                                move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                //unlink("c:\\wamp\\tmp\\arquivo.pdf");

                                $sNome = $sTipoContrato . '_'.$_REQUEST['fCodContratoDocTipo'][$i] . "_" . $nIdContrato;
                                $sArquivo = $Anexo;

                                $sExtensao = $Tipo;
                                $aTipoArquivo =  explode('||',$_REQUEST['fCodContratoDocTipo'][$i]);
                                $nCodTipo = $aTipoArquivo[0];
                                $nCodArquivo = $aTipoArquivo[1];
                                $nTamanho = $Tamanho;
                                $dDataHora= date('Y-m-d H:i:s');
                                $sRealizadoPor = $_SESSION['Perfil']['Login'];
                                $nAtivo = 1;
                                $nContratoCodigo = $nIdContrato;
                                $nCodConciliacao = 'NULL';
                                $nMovCodigo = 'NULL';
                                $nMovItem = 'NULL';

                                $oSysArquivo = $oFachadaSys->inicializarSysArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem);
                                $oSysArquivo->setArquivo($sArquivo);
                                if($nCodArquivo){
                                    $oFachadaSys->alterarSysArquivo($oSysArquivo);
                                }else{
                                    $oFachadaSys->inserirSysArquivo($oSysArquivo);
                                }
                            }
                        }
               //         }else{
              //              $retorno = $this->upload($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$oMnyContratoPessoa->getContratoCodigo(),$_REQUEST['fCodContratoArquivo']);
              //          }
				//	}
					
					if($oMnyContratoPessoa->getTipoContrato() ==2 || $oMnyContratoPessoa->getTipoContrato() ==10){	
						// apagar todos os itens da ase do banco
						$oFachada->excluirMnyContratoAseFisicamentePorContrato($oMnyContratoPessoa->getContratoCodigo());
						$item = 1;
						$cont = count($_REQUEST['AseItemDescricao']);
						
						for($i = 0 ; $cont > $i  ;$i++){	
							// inserir todos os itens da ase do formulario no banco
							$_POST['fItem'] = $item;
							$oMnyContratoAse = $oFachada->inicializarMnyContratoAse($oMnyContratoPessoa->getContratoCodigo(),$_POST['fItem'],mb_convert_case($_REQUEST['AseItemDescricao'][$i],MB_CASE_UPPER, "UTF-8"),$_REQUEST['AseItemValor'][$i],1);				
							$oFachada->inserirMnyContratoAse($oMnyContratoAse);
							$item++;
						}
						
					}
					unset($_SESSION['oMnyContratoPessoa']);	
 					$_SESSION['sMsg'] = $sTipoContrato." ". $oMnyContratoPessoa->getContratoCodigo()." alterado com sucesso!";
 					//$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista";			
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista&fTipoContrato=".$_REQUEST['fTipoContrato'];
 				}else{
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar". ($_REQUEST['fTipoContrato'] == 1)? "o " : "a " . $sTipoContrato." !";
 					//$sHeader = "?bErro=1&action=MnyContratoPessoa.preparaFormulario&sOP=".$sOP."&Pagina=2&nIdMnyContratoPessoa=".$_POST['fContratoCodigo'];
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&fTipoContrato=".$_REQUEST['fTipoContrato']."&nIdMnyContratoPessoa=".$_POST['fContratoCodigo'];
				}

			break;
 			case "Excluir":
                $bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContratoPessoa']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyContratoPessoa($sCampoChave);\n");
				}
 				if($bResultado){
 					$_SESSION['sMsg'] = $sTipoContrato."(s) exclu&iacute;do(s) com sucesso!";
 					//$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=".$oMnyContratoPessoa->getPesCodigo();
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista&fTipoContrato=".$_REQUEST['fTipoContrato'];
				} else{ 					
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir". ($_REQUEST['fTipoContrato'] == 1)? "o(s) " : "a(s)" . $sTipoContrato." ! Verifique a exist&ecirc;ncia de arquivos vinculados ao " . $sTipoContrato;
 					//$sHeader = "?bErro=1&action=MnyContratoPessoa.preparaLista";
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista&fTipoContrato=".$_REQUEST['fTipoContrato'];
 				}
 			break;
 		}
 			header("Location: ".$sHeader);		
 	}
	function carregaTipo(){	
		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];				
		
			$nCodCusto = $_REQUEST['fCenCodigo'];
			$oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($nCodCusto);
			$voMnyContas = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($oMnyPlanoContas->getCodigo());
			
			$_REQUEST['voMnyContas'] = $voMnyContas;
			include_once("view/financeiro/mny_contrato_pessoa/centro_custo_ajax.php");
			exit();
	}
 
 }
 ?>
