<?php
 class MnyContaCorrenteCTR implements IControle{
 
 	public function MnyContaCorrenteCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$voMnyContaCorrente = $oFachada->recuperarTodosMnyContaCorrenteIndex();
 		$_REQUEST['voMnyContaCorrente'] = $voMnyContaCorrente;
 		include_once("view/financeiro/mny_conta_corrente/index.php");
 		exit(); 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 
 		$oMnyContaCorrente = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContaCorrente = ($_POST['fIdMnyContaCorrente'][0]) ? $_POST['fIdMnyContaCorrente'][0] : $_GET['nIdMnyContaCorrente'];
 	
 			if($nIdMnyContaCorrente){
 				$vIdMnyContaCorrente = explode("||",$nIdMnyContaCorrente);
 				$oMnyContaCorrente = $oFachada->recuperarUmMnyContaCorrente($vIdMnyContaCorrente[0]);
 			}

            $voUnidade  = $oMnyContaCorrente->getUnidades();
            foreach($voUnidade as $oUnidade){
                $vUnidadeConta[] = $oUnidade->getCodUnidade();
            }

            $_REQUEST['vUnidadeConta'] = $vUnidadeConta;
 		}
 		
 		$_REQUEST['oMnyContaCorrente'] = ($_SESSION['oMnyContaCorrente']) ? $_SESSION['oMnyContaCorrente'] : $oMnyContaCorrente;
 		unset($_SESSION['oMnyContaCorrente']);
 
 		$_REQUEST['voSysBanco'] = $oFachadaSys->recuperarTodosSysBanco();
		$_REQUEST['voUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo(8);
		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
		

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_conta_corrente/detalhe.php");
 		else
 			include_once("view/financeiro/mny_conta_corrente/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
			if($sOP == 'Cadastrar')
				$_POST['fCcrInc'] = date('d/m/Y H:i') . $_SESSION['oUsuarioImoney']->getLogin();
			if($sOP == 'Alterar')
				$_POST['fCcrAlt'] = date('d/m/Y H:i') . $_SESSION['oUsuarioImoney']->getLogin();
 			    $oMnyContaCorrente = $oFachada->inicializarMnyContaCorrente($_POST['fCcrCodigo'],$_POST['fCcrAgencia'],$_POST['fCcrAgenciaDv'],$_POST['fCcrConta'],$_POST['fCcrContaDv'],$_POST['fCcrTipo'],$_POST['fBanCodigo'],$_POST['fCcrInc'],$_POST['fCcrAlt'],$_POST['fClassificacao'],$_POST['fDataEncerramento'],$_POST['fAtivo'],$_POST['fEmpCodigo']);
 			    $_SESSION['oMnyContaCorrente'] = $oMnyContaCorrente;
			
		//print_r($_SESSION['oMnyContaCorrente']);
		//die();
		
			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Agencia", $oMnyContaCorrente->getCcrAgencia(), "text", "y");
			//$oValidate->add_text_field("Dv", $oMnyContaCorrente->getCcrAgenciaDv(), "text", "y");
			$oValidate->add_text_field("Conta", $oMnyContaCorrente->getCcrConta(), "text", "y");
			//$oValidate->add_text_field("Dv", $oMnyContaCorrente->getCcrContaDv(), "text", "y");
			
			if($oMnyContaCorrente->getCcrTipo() !=0)
				$oValidate->add_number_field("Tipo", $oMnyContaCorrente->getCcrTipo(), "number", "y");
			$oValidate->add_number_field("Banco", $oMnyContaCorrente->getBanCodigo(), "number", "y");
			$oValidate->add_text_field("Inc", $oMnyContaCorrente->getCcrInc(), "text", "y");
			if($sOP == 'Alterar')
			$oValidate->add_text_field("CcrAlt", $oMnyContaCorrente->getCcrAlt(), "text", "y");
			$oValidate->add_number_field("Classificacao", $oMnyContaCorrente->getClassificacao(), "number", "y");
			//$oValidate->add_date_field("Data", $oMnyContaCorrente->getDataEncerramento(), "date", "y");
			$oValidate->add_number_field("Ativo", $oMnyContaCorrente->getAtivo(), "number", "y");
 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyContaCorrente.preparaFormulario&sOP=".$sOP."&nIdMnyContaCorrente=".$_POST['fCcrCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 //print_r($_SESSION['oMnyContaCorrente']);
		//die();
 		switch($sOP){
 			case "Cadastrar":

 				if($nCodContaCorrente = $oFachada->inserirMnyContaCorrente($oMnyContaCorrente)){
 					unset($_SESSION['oMnyContaCorrente']);

                    for($i = 0 ; count($_REQUEST['fCodUnidade']) > $i  ;$i++){
                        $oMnyContaCorrenteUnidade = $oFachada->inicializarMnyContaCorrenteUnidade($nCodContaCorrente,$_REQUEST['fCodUnidade'][$i]);
                        $oFachada->inserirMnyContaCorrenteUnidade($oMnyContaCorrenteUnidade);

                    }
 					$_SESSION['sMsg'] = "Conta Corrente inserida com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContaCorrente.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Conta Corrente!";
 					$sHeader = "?bErro=1&action=MnyContaCorrente.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyContaCorrente($oMnyContaCorrente)){
                    $oFachada->excluirMnyContaCorrenteUnidadePorConta($oMnyContaCorrente->getCcrCodigo());
                    for($i = 0 ; count($_REQUEST['fCodUnidade']) > $i  ;$i++){
                        $oMnyContaCorrenteUnidade = $oFachada->inicializarMnyContaCorrenteUnidade($oMnyContaCorrente->getCcrCodigo(),$_REQUEST['fCodUnidade'][$i]);
                        $oFachada->inserirMnyContaCorrenteUnidade($oMnyContaCorrenteUnidade);
                    }
 					unset($_SESSION['oMnyContaCorrente']);
 					$_SESSION['sMsg'] = "Conta Corrente alterada com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContaCorrente.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Conta Corrente!";
 					$sHeader = "?bErro=1&action=MnyContaCorrente.preparaFormulario&sOP=".$sOP."&nIdMnyContaCorrente=".$_POST['fCcrCodigo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContaCorrente']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyContaCorrente($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Conta Corrente(s) exclu&iacute;da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContaCorrente.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Conta Corrente!";
 					$sHeader = "?bErro=1&action=MnyContaCorrente.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
