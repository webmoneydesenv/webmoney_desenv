<?php
 class MnyMovimentoItemCTR implements IControle{
 
 	public function MnyMovimentoItemCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyMovimentoItem = $oFachada->recuperarTodosMnyMovimentoItem();
 
 		$_REQUEST['voMnyMovimentoItem'] = $voMnyMovimentoItem;
 	//	$_REQUEST['voMnyMovimento'] = $oFachada->recuperarTodosMnyMovimento();

	//	$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();

	//	$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
 		
 		include_once("view/financeiro/mny_movimento_item/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada     = new FachadaFinanceiroBD();
 		$oFachadaView = new FachadaViewBD();
 		$oFachadaSys  = new FachadaSysBD();

 		$oMnyMovimentoItem = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyMovimentoItem = ($_POST['fIdMnyMovimentoItem'][0]) ? $_POST['fIdMnyMovimentoItem'][0] : $_GET['nIdMnyMovimentoItem'];
 	        $nMovItem = explode('||',$nIdMnyMovimentoItem);
            $_REQUEST['oMnyConciliacao'] = $oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($nMovItem[0],$nMovItem[1]);

 			if($nIdMnyMovimentoItem){
 				$vIdMnyMovimentoItem = explode("||",$nIdMnyMovimentoItem);
 				$oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1]);
                //$oVMovimento = $oFachadaView->recuperarUmVMovimento($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1]);
                $oVMovimento = $oFachadaView->recuperarUmVMovimentoPROCEDURE($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1]);

                $oSTatusWizardBreadcrumb =  $oFachada->recuperarMnyMovimentoImtemProtocoloPorMovimentoItemWizardBreadcrumb($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());

                $_REQUEST['sSTatusWizardBreadcrumb']=$oSTatusWizardBreadcrumb->para_cod_tipo_protocolo;
                $_REQUEST['voTramitacaoStatus'] = $oFachadaSys->recuperarTodosSysProtocoloEncaminhamentoPorTipo($oSTatusWizardBreadcrumb->mov_tipo);
                $_REQUEST['voMovimentoItemAgrupado'] = $oFachada->recuperarTodosMnyMovimentoItemAgrupadorPorMovimentoItemAgrupador($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());

 			}
 		}else{
			$nIdMnyMovimento = $_GET['nIdMnyMovimento'];	
			$_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($nIdMnyMovimento);
		}

		$_REQUEST['oVMovimento'] = $oVMovimento;
		$sGrupo = '4'; // Tipo de doc
		$_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '11'; // Forma de Pag	
		$_REQUEST['voMnyFormaPagamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
				
		$sGrupo = '3'; // Tipo de aceite
		$_REQUEST['voMnyTipoAceite'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
 		$_REQUEST['oMnyMovimentoItem'] = ($_SESSION['oMnyMovimentoItem']) ? $_SESSION['oMnyMovimentoItem'] : $oMnyMovimentoItem;
				
		$_REQUEST['fContratoTipo'] = $_REQUEST['fContratoTipo']; //tipo_contrato vindo do altera item que esta na tela de pesquisa
		$_REQUEST['sTipoLancamento'] = $_REQUEST['sTipoLancamento']; //pagar/receber
		$_REQUEST['nIdMnyMovimentoItem'] = $_REQUEST['nIdMnyMovimentoItem'];//

		unset($_SESSION['oMnyMovimentoItem']);
		
		//adiciona um item no movimento
		if($_REQUEST['AddItem'] == 1){
			$_REQUEST['AddItem'] = $oFachada->recuperarUmMnyMovimentoItemPorMovimento($_REQUEST['nIdMnyMovimento']);
			$_REQUEST['nCodContrato'] =	$_REQUEST['nCodContrato'] ;
		}
 		if($_REQUEST['sOP'] == "Detalhar"){

            //$_REQUEST['oMnySolicitacaoAporte'] = $oFachada->recuperarUmMnySolicitacaoAportePorFAdetalhe($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());

			$oMovContrato  = $oFachada->recuperarUmMnyContratoPessoaPorMovimento($oVMovimento->getMovCodigo());
            $_REQUEST['oMnySolicitacaoAporte'] = $oFachada->recuperarUmMnySolicitacaoAportePorMovimentoPorItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());

            $sGrupo = '4'; // Tipo de doc
		    $_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

            if($oMovContrato){
                $_REQUEST['oMovContrato'] = $oMovContrato;
				$_REQUEST['MovContrato']   = $oMovContrato->getContratoCodigo();
				$_REQUEST['nTipoContrato'] = $oMovContrato->getTipoContrato();
               // $_REQUEST['voMnyContratoArquivo'] = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivo($oMovContrato->getContratoCodigo(),$oMovContrato->getTipoContrato());
                $_REQUEST['voMnyContratoArquivo'] = $oFachadaSys->recuperarTodosSysArquivoPorContratoTipoArquivo($oMovContrato->getContratoCodigo(),$oMovContrato->getTipoContrato());
                $_REQUEST['voContratoAditivo'] = $oFachada->recuperarTodosMnyContratoAditivoPorContrato($oMovContrato->getContratoCodigo());
                $_REQUEST['voArquivoContrato'] = $oFachadaSys->recuperarTodosSysArquivoDetalheMovimento($oVMovimento->getMovCodigo());
			}elseif($_REQUEST['oVMovimento']->getMovTipo() == 189){
               $_REQUEST['nTipoContrato'] = 9;
			}else{
				$_REQUEST['nTipoContrato'] = 4;
                $_REQUEST['voArquivoContrato'] = $oFachada->recuperarTodosMnyContratoArquivoDetalheMovimentoDespesaCaixa($oVMovimento->getMovCodigo());
            }





			
            if($oVMovimento->getFinalizado()==2){
               $_REQUEST['oMnyMovimentoDEVOLUCAO'] = $oFachada->recuperarUmMyMovimentoItemDEVOLUCAOESTORNOPorMovimentoOrigem($oVMovimento->getMovCodigo(),$oVMovimento->getMovItem());
            }

            //print_r($_REQUEST['oMnyMovimentoDEVOLUCAO']);
            //die();

			//$_REQUEST['MovContrato'] = $_REQUEST['MovContrato'];
			
			//$_REQUEST['voSysTipoProtocolo'] = $oFachadaSys->recuperarTodosSysTipoProtocolo();
			$_REQUEST['voTramitacao'] = $oFachada->recuperarTodosMnyMovimentoItemProtocoloPorMovimentoItem($oVMovimento->getMovCodigo(), $oVMovimento->getMovItem());
//			$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($_REQUEST['nTipoContrato'],0);
			$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($_REQUEST['nTipoContrato'],0);
			//$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();
 			
            //usado na tramitação...
            $voSysProtocoloEncaminhamento = $oFachadaSys->recuperarTodosSysProtocoloEncaminhamentoPorOrigem($_SESSION['Perfil']['CodGrupoUsuario'],$oSTatusWizardBreadcrumb->mov_tipo);
 		    $_REQUEST['voSysProtocoloEncaminhamento'] = $voSysProtocoloEncaminhamento;

			include_once("view/financeiro/mny_movimento_item/detalhe.php");
		}else{
            $_REQUEST['nSaldoContrato'] = str_replace('||','.',$_REQUEST['nSaldoContrato']);
 			include_once("view/financeiro/mny_movimento_item/insere_altera2.php");
 			exit();
		}
 	}

    public function preparaFormularioJuros(){
        $oFachada = new FachadaFinanceiroBD();
        $nFA = $_REQUEST['nIdMnyMovimentoItem'];
        $vFA = explode('||',$nFA);

        $_REQUEST['nCodSolicitacaoAporte'] = $_REQUEST['nCodSolicitacao'];

        $_REQUEST['oMnyAporteItem'] =    $oFachada->recuperarUmMnyAporteItemPorFA($vFA[0],$vFA[1]);
        $_REQUEST['oMnyMovimentoItem'] = $oFachada->recuperarUmMnyMovimentoItem($vFA[0],$vFA[1]);

        include_once('view/financeiro/mny_movimento_item/insere_altera_juros_ajax.php');
        exit();
    }

    public function preparaFormularioReativarFA(){
        include_once('view/financeiro/mny_movimento_item/reativa_item_ajax.php');
        exit();
    }

   public function preparaFormularioExcluirFA(){
        include_once('view/financeiro/mny_movimento_item/excluir_item_ajax.php');
        exit();
    }

 	public function processaFormularioReativarFA(){
 		$oFachada = new FachadaFinanceiroBD();

 		$oMnyMovimentoItem = false;

 		 $nIdMnyMovimentoItem = ($_POST['fIdMnyMovimentoItem']) ? $_POST['fIdMnyMovimentoItem'] : $_GET['nIdMnyMovimentoItem'];
         if($nIdMnyMovimentoItem){
 		     $vIdMnyMovimentoItem = explode(".",$nIdMnyMovimentoItem);
 			if($oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1])){
                 $oMnyMovimentoItem->setAtivo(1);
                if($voMovimentoItemProtocolo = $oFachada->recuperarTodosMnyMovimentoItemProtocoloPorMovimentoItem($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1])){
                    foreach($voMovimentoItemProtocolo  as $oMovimentoItemProtocolo ){
                        $oMovimentoItemProtocolo->setAtivo(1);
                        $oFachada->alterarMnyMovimentoItemProtocolo( $oMovimentoItemProtocolo->getCodProtocoloMovimentoItem());
                    }

                }

                 if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){
                     $_SESSION['sMsg'] = "Ficha de Aceite reativada!";
                 }else{
                    $_SESSION['sMsg'] = "Não foi possisel reativar esta ficha de aceite !";
                 }

             }else{
                 $_SESSION['sMsg'] = "Ficha de Aceite não encontrada!";
             }
         }else{
             $_SESSION['sMsg'] = "Ficha de Aceite não encontrada!";
         }

        header("Location: index.php?");
 	}


 	public function processaFormularioExcluirFA(){
 		$oFachada = new FachadaFinanceiroBD();

 		$oMnyMovimentoItem = false;

 		 $nIdMnyMovimentoItem = ($_POST['fIdMnyMovimentoItem']) ? $_POST['fIdMnyMovimentoItem'] : $_GET['nIdMnyMovimentoItem'];
         if($nIdMnyMovimentoItem){
 		     $vIdMnyMovimentoItem = explode(".",$nIdMnyMovimentoItem);
 			if($oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($vIdMnyMovimentoItem[0],$vIdMnyMovimentoItem[1])){
                $vAdmin = array(1,19);
                if(in_array($vAdmin,$_SESSION['Perfil']['CodGrupoUsuario'])){
                    if($oAporteItem = $oFachada->recuperarUmMnyAporteItemPorFA($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                        $_SESSION['sMsg'] = "F.A não pode ser excluída por existir Aporte vinculado a ele";
                    }else{
                         if($oTransferencia = $oFachada->recuperarUmMnyTransferenciaPorMovimentoItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                             $_SESSION['sMsg'] = "F.A não pode ser exclúida por ser parte de uma transferência";
                         }else{
                            // verifica arquivos
                            $voArquivo = $oFachada->recuperarTodosMnyContratoArquivoPorFA($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());
                            if($voArquivo){
                                $sCaminho = $_SERVER['DOCUMENT_ROOT'] . "/webmoney/";
                                foreach($voArquivo as $oArquivo){
                                    $sArquivo = $sCaminho.$oArquivo->getNome();
                                    if(is_file($sArquivo))
                                        unlink($sArquivo);
                                }
                                if(!($oFachada->excluirTodosMnyContratoArquivoPorMovimentoItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())))
                                    $_SESSION['sMsg']  = "Erro ao exlcuir arquivo(s) vinculado(s)";

                            }
                            if($oFachada->excluirTodosMnyMovimentoItemProtocoloPorFA($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                                if($oFachada->excluirTodosMnyMovimentoItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                                    $_SESSION['sMsg'] = "F.A excluida com sucesso!";
                                }else{
                                    $_SESSION['sMsg'] = "Não foi possivel excluir F.A!";
                                }

                                if(!($oFachada->recuperarTodosMnyMovimentoItemPorMovimento($oMnyMovimentoItem->getMovCodigo())))
                                    $oFachada->excluirMnyMovimento($oMnyMovimentoItem->getMovCodigo());
                            }else{
                                 $_SESSION['sMsg'] = "Não foi possivel excluir tramitação vinculada a F.A!";
                            }

                         }
                    }
                }else{
                    //exclusao logica
                    if($oAporteItem = $oFachada->recuperarUmMnyAporteItemPorFA($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                        $_SESSION['sMsg'] = "F.A não pode ser excluída por existir Aporte vinculado a ele";
                    }else{
                        if($oTransferencia = $oFachada->recuperarUmMnyTransferenciaPorMovimentoItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem())){
                            $_SESSION['sMsg'] = "F.A não pode ser exclúida por ser parte de uma transferência";
                        }else{
                             $oFachada->desativarMnyMovimentoItemProtocolo($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());
                             $oFachada->desativarMnyContratoArquivo($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem());
                             $oMnyMovimentoItem->setAtivo(0);
                             $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem);
                            $_SESSION['sMsg'] = "F.A exclúida com sucesso";
                        }
                    }
                }
            }else{
                 $_SESSION['sMsg'] = "F.A não encontrada!";
            }
         }else{
            $_SESSION['sMsg'] = "F.A não informada!";
         }


        header("Location: index.php?");
 	}

    public function processaFormularioExcluirFAProcedure(){

        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaPermissao = new FachadaPermissaoBD();

        $vfIdMnyMovimentoItem = explode('.',$_POST['fIdMnyMovimentoItem']);

        $sLogin = mb_convert_case($_SESSION['Perfil']['Login'],MB_CASE_UPPER, "UTF-8");
        $sMotivo = mb_convert_case($_POST['fMotivo'],MB_CASE_UPPER, "UTF-8");
        $vRetorno = $oFachadaFinanceiro->excluirMnyMovimentoItemProcedure($vfIdMnyMovimentoItem[0],$vfIdMnyMovimentoItem[1],$sLogin,$sMotivo);
        $_SESSION['sMsg'] = $vRetorno->ativo;
        header("Location: index.php?");

    }

 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir" && $sOP != "AlterarJuros"){
 			if($sOP == 'Cadastrar'){
				$_POST['fMovDataInclusao'] = date('d/m/Y');
			}			
					
			$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($_POST['fMovCodigo']);
			
				$_SESSION['oMnyMovimentoItem'] = $oMnyMovimentoItem;
			
				$nTipo = $oMnyMovimento->getMovTipo();
				if($nTipo == 0)
					$sTipo='Pagar';
				else
					$sTipo='Receber';
					
			//recupera o valor global do contrato e aditivo...	
			//$oValorGlobalContrato = $oFachada->recuperarValorGlobalMnyContratoPessoaPorContrato($oMnyMovimento->getMovContrato(), $oMnyMovimento->getMovCodigo());
			//if($_POST['fMovValor'] < $oValorGlobalContrato){}
/*
				print_r($_POST['fMovValorParcela']);
				die();*/
		        if ($sOP == 'Cadastrar'){
                    $_REQUEST['fContabil'] = 1;
                }
                $nCaixinha = ($_REQUEST['fCaixinha']) ? $_REQUEST['fCaixinha'] : '0';
                $dMovDataPrev = ($_POST['fMovDataPrev']) ? $_POST['fMovDataPrev'] : $_POST['fMovDataVencto'];
                //$sFaOrigem = 'NULL';
                $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
				$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$dMovDataPrev,$_POST['fMovValorParcela'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'],$_POST['fTipAceCodigo'],$_REQUEST['fCompetencia'],$_REQUEST['fContabil'],0,1,$nCaixinha,$_POST['FaOrigem'],$nNf,$nConciliado);

/*				print_r($oMnyMovimentoItem);
				die();
*/				
				$oValidate = FabricaUtilitario::getUtilitario("Validate");
				$oValidate->check_4html = true;
			
				$oValidate->add_number_field("Movimento", $oMnyMovimentoItem->getMovCodigo(), "number", "y");
				$oValidate->add_number_field("Item", $oMnyMovimentoItem->getMovItem(), "number", "y");
				$oValidate->add_date_field("Data Vencto", $oMnyMovimentoItem->getMovDataVencto(), "date", "y");
				//$oValidate->add_date_field("MovDataPrev", $oMnyMovimentoItem->getMovDataPrev(), "date", "y");
				//$oValidate->add_number_field("Valor", $oMnyMovimentoItem->getMovValorParcela(), "decimal", "y",2);
				//$oValidate->add_number_field("MovJuros", $oMnyMovimentoItem->getMovJuros(), "number", "y");
				//$oValidate->add_number_field("MovValorPagar", $oMnyMovimentoItem->getMovValorPagar(), "number", "y");
				$oValidate->add_number_field("Forma Pagamento", $oMnyMovimentoItem->getFpgCodigo(), "number", "y");
				$oValidate->add_number_field("Tipo Doc.", $oMnyMovimentoItem->getTipDocCodigo(), "number", "y");
				//$oValidate->add_number_field("MovRetencao", $oMnyMovimentoItem->getMovRetencao(), "number", "y");
				$oValidate->add_date_field("Data Inclusao", $oMnyMovimentoItem->getMovDataInclusao(), "date", "y");
				//$oValidate->add_number_field("MovValorPago", $oMnyMovimentoItem->getMovValorPago(), "number", "y");
				$oValidate->add_number_field("Tipo de Aceite", $oMnyMovimentoItem->getTipAceCodigo(), "number", "y");
				//$oValidate->add_number_field("Ativo", $oMnyMovimentoItem->getAtivo(), "number", "y");
				
				if (!$oValidate->validation()) {
					$_SESSION['sMsg'] = $oValidate->create_msg();
					//$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=Alterar&nIdMnyMovimento=".$_POST['fMovCodigo'];
					$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&fContratoTipo=".$_REQUEST['fContratoTipo']."&nIdMnyMovimento=".$_REQUEST['nIdMnyMovimentoItem'];
					header("Location: ".$sHeader);	
					die();
				}
			
		  }

 		switch($sOP){			
 			case "Cadastrar":			
				//VERIFICA SE EXISTE VALOR DISPONIVEL NO CONTRATO
				$oContratoPessoa = $oFachada->recuperarumMnyContratoPessoa($_REQUEST['fContratoCodigo']);		
				$nMovValor = str_replace(",",".", $oMnyMovimentoItem->getMovValorParcela() );				 
				$oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem(),18,15,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Lançado.",MB_CASE_UPPER, "UTF-8"),1);

                if($oContratoPessoa){
					if($nMovValor <= $oContratoPessoa->getSaldoContrato() ){										
						if($oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem)){
                            $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
							unset($_SESSION['oMnyMovimentoItem']);
							$_SESSION['sMsg'] = "Item inserido com sucesso!";
						}else {
							$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Item!";
						}					
					}else{
					   $_SESSION['sMsg'] = "O valor do contrato nao cobre o valor do item !";	
					}
				}else{ // despesa de caixa
					if($oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem)){
                        $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
						unset($_SESSION['oMnyMovimentoItem']);
						$_SESSION['sMsg'] = "Item inserido com sucesso!";
					}else {
						$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Item!";
					}	
				
				}
				//$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$sTipo."&nIdMnyMovimento=".$oMnyMovimentoItem->getMovCodigo()."&fContratoTipo=1";	
				$nMovCodigo = ($_REQUEST['nIdMnyMovimentoItem']) ? $_REQUEST['nIdMnyMovimentoItem'] : $_REQUEST['nIdMnyMovimento'];
				$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&fContratoTipo=".$_REQUEST['fContratoTipo']."&nIdMnyMovimento=".$nMovCodigo;
 			break;
 			case "Alterar":	


                if(!($oAporteItem = $oFachada->recuperarUmMnyAporteItemPorMovimentoPorItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem()))){
				
                    $oContratoPessoa = $oFachada->recuperarumMnyContratoPessoa($_REQUEST['fContratoCodigo']);
                    if($oContratoPessoa){
                        $nMovValor = str_replace(",",".", $_REQUEST['fMovValorParcela']);
                    }	else{
                        $nMovValor =0;
                        $oValorGlobalContrato = 1;
                    }
                    if($nMovValor < $oValorGlobalContrato){
                        if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){
                            unset($_SESSION['oMnyMovimentoItem']);
                            $_SESSION['sMsg'] = "Item alterado com sucesso!";
                        } else {
                            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Item!";
                        }
                    }else{
                        $_SESSION['sMsg'] = "O valor do contrato nao cobre o valor do item !";
                    }
                }else{
                    $_SESSION['sMsg'] = "Ficha de Aceite não pode ser alterada pois faz parte do aporte [".$oAporteItem->getMnySolicitacaoAporte()->getNumeroAporteFormatado()."]";

                }
/* 				usado pelo altera_item da pesquisa.php...		
				if($_REQUEST['sTipoLancamento'] == 188){
							$sTipoLancamento = "Pagar";	
						}else{
							$sTipoLancamento = "Receber";	
						}
*/						
						$sTipoLancamento = $_REQUEST['sTipoLancamento'];
						$_REQUEST['fContratoTipo'];	//tipo_contrato vindo do altera item que esta na tela de pesquisa
						 //pagar/receber
						$_REQUEST['nIdMnyMovimentoItem'];//
								
				$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$sTipoLancamento."&fContratoTipo=".$_REQUEST['fContratoTipo']."&nIdMnyMovimento=".$_REQUEST['nIdMnyMovimentoItem'];
 			break;
 			case "AlterarAuditoria":

                if($_SESSION['Perfil']['CodGrupoUsuario'] == 7){
                    if(!($oAporteItem = $oFachada->recuperarUmMnyAporteItemPorMovimentoPorItem($oMnyMovimentoItem->getMovCodigo(),$oMnyMovimentoItem->getMovItem()))){
                            if($_REQUEST['nContabil'] != $oMnyMovimentoItem->getContabil()){
                                $oMnyMovimentoItem->setContabil($_REQUEST['nContabil']);
                                if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){
                                    unset($_SESSION['oMnyMovimentoItem']);
                                    $_SESSION['sMsg'] = "Item alterado com sucesso!";
                                } else {
                                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Item!";
                                    // redirecionar para a tela de protocolo
                                    $sHeader = "?action=";
                                }
                            }
                    }else{
                        $_SESSION['sMsg'] = "Ficha de Aceite não pode ser alterada pois faz parte do aporte [".$oAporteItem->getMnySolicitacaoAporte()->getNumeroAporteFormatado()."]";

                    }
                    //verificarLink de redirecionamento (para a index com enfase para a aba pendências)
                    $sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$sTipoLancamento."&fContratoTipo=".$_REQUEST['fContratoTipo']."&nIdMnyMovimento=".$_REQUEST['nIdMnyMovimentoItem'];
                }else{
                    $_SESSION['sMsg'] = "Você não tem permissão para realizar essa ação!";
                    //verificar Link
                    $sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$sTipoLancamento."&fContratoTipo=".$_REQUEST['fContratoTipo']."&nIdMnyMovimento=".$_REQUEST['nIdMnyMovimentoItem'];
                }
 			break;
            case "AlterarJuros":

                $oFachada = new FachadaFinanceiroBD();
                /*
                $oMnyMovimentoItem = $oFachadaFinanceiro->recuperarUmMnyMovimentoItemPorItem($_POST['fMovCodigo'],$_POST['fMovItem']);
                $oMnyMovimentoItem->setMovJuros($_POST["fMovJuros"]);
                $oMnyMovimentoItem->setMovRetencao($_POST["fMovRetencao"]);
*/
                $oMnyAporteItem =  $oFachada->recuperarUmMnyAporteItemPorFA($_REQUEST['fMovCodigo'],$_REQUEST['fMovItem']);
                $oMnyTransferencia =  $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnyAporteItem->getCodSolicitacaoAporte());
                $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['fMovCodigo'],$_REQUEST['fMovItem']);
                $oMnyMovimentoItem->setMovJurosBanco($_REQUEST['fMovJuros']);
                $oMnyMovimentoItem->setMovRetencaoBanco($_REQUEST['fMovRetencao']);
                if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){
                   /*
                    if($oMnyTransferencia){
                        $oMnyMovimentoItemOrigem = $oFachada->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoOrigem(),$oMnyTransferencia->getMovItemOrigem());
                        $oMnyMovimentoItemDestino = $oFachada->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoDestino(),$oMnyTransferencia->getMovItemDestino());
                        $sOrigem = array('.',',');
                        $sDestino = array('','.');
                        $nMovJuros = str_replace($sOrigem, $sDestino, $_REQUEST['fMovJuros']);
                        $nMovRetencao = str_replace($sOrigem, $sDestino, $_REQUEST['fMovRetencao']);
                        $nJurosAtualizado = ($oMnyMovimentoItemOrigem->getMovJuros() + $nMovJuros);
                        $nRetencaoAtualizado = ($oMnyMovimentoItemOrigem->getMovJuros() + $nMovRetencao);
                        $oMnyMovimentoItemOrigem->setMovJurosBanco($nJurosAtualizado);
                        $oMnyMovimentoItemOrigem->setMovRetencaoBanco($nRetencaoAtualizado);
                        $oMnyMovimentoItemDestino->setMovJurosBanco($nJurosAtualizado);
                        $oMnyMovimentoItemDestino->setMovRetencaoBanco($nRetencaoAtualizado);

                        if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemOrigem)){
                            $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemDestino);
                            $_SESSION['sMsg'] = "Operação realizada com sucesso!";
                        }else{
                            $_SESSION['sMsg'] = "Não foi possivel realizar esta operação(1)!";
                        }
                    }else{
                        $_SESSION['sMsg'] = "Não foi possivel realizar esta operação(2)!";
                    }
                    */
                }else{
                    $_SESSION['sMsg'] = "Não foi possivel realizar esta operação(3)!";
                }

                $sHeader = "?action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar&nIdMnySolicitacaoAporte=".$_REQUEST['fCodSolicitacaoAporte'];

            break;
 			case "Excluir":
 				$bResultado = true;
 				$voRegistros = explode("____",$_REQUEST['fIdMnyMovimentoItem']);
                $nRegistros = count($voRegistros);

                $vMovimento = explode("||",$voRegistros[0]);
                $oMnyMovimento = $oFachada->recuperarUmMnyMovimento($vMovimento[0]);
                $nTipo = $oMnyMovimento->getMovTipo();

                if($nTipo ==0)
					$sTipo='Pagar';
				else
					$sTipo='Receber';

                foreach($voRegistros as $oRegistros){
                    $sCampoChave = str_replace("||", ",", $oRegistros);

                     eval("\$oAporteItem = \$oFachada->recuperarUmMnyAporteItemPorMovimentoPorItem($sCampoChave);\n");
                     eval("\$oConciliacao = \$oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($sCampoChave);\n");
                    $sCampoChave2 = explode(",",$sCampoChave);
                     eval("\$voMnyMovimentoItem = \$oFachada->recuperarTodosMnyMovimentoItemPorMovimento($sCampoChave2[0]);\n");

                    if((!($oAporteItem)) && (!($oConciliacao)) && ((count($voMnyMovimentoItem)>1)) ){
                        eval("\$bResultado &= \$oFachada->excluirMnyMovimentoItem($sCampoChave);\n ");
                    }else{
                        if($oAporteItem)
                            $_SESSION['sMsg'] = 'Ficha de Aceite não pode ser excluida pois faz parte do aporte ['.$oAporteItem->getMnySolicitacaoAporte()->getNumeroAporteFormatado().']';
                        if($oConciliacao)
                            $_SESSION['sMsg'] = 'Ficha de Aceite não pode ser excluida pois faz parte da conciliacao ['.$oConciliacao->getDataConciliacaoFormatado() . ' / Unidade:' .$oConciliacao->getMnyPlanoContas->getDescricao() . ' / Conta:' . $oConciliacao->getMnyContaCorrente()->getCcrContaFormatada() .']';
                        if (count($voMnyMovimentoItem)==1)
                            $_SESSION['sMsg'] = 'Esta Ficha de Aceite não pode ser excluida pois seu item é o único do Movimento';
                        $sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$vMovimento[0]."&fContratoTipo=".$_REQUEST['fContratoTipo'];
                        header("Location: ".$sHeader);
                        exit();
                    }
                }

                if($bResultado){
                    $_SESSION['sMsg'] = "Item(s) exclu&iacute;do(s) com sucesso!";
                } else {
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Item!";
                }


				if($_REQUEST['sOrigem'] == 'pesquisa')
					$sHeader = "?bErro=0&action=MnyMovimento.preparaLista";				
				else
					$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$vMovimento[0]."&fContratoTipo=".$_REQUEST['fContratoTipo'];
 			break;

 		}
 
 		header("Location: ".$sHeader);
 	
 	}

    public function extornaItemMovimento(){

         //die('atualizando');

         $oFachadaFinanceiro = new FachadaFinanceiroBD();

         if(substr_count($_REQUEST['fDataConciliacao'],'-') > 0){
            $oData = DateTime::createFromFormat('Y-m-d', $_REQUEST['fDataConciliacao']);
            $dData = $oData->format('d/m/Y');
         }else{
            $dData = $_REQUEST['fDataConciliacao'];
         }

         $aFa = explode('||',$_REQUEST['fa']);

         $oMnyMovimento = $oFachadaFinanceiro->recuperarUmMnyMovimento($aFa[0]);
         $oMnyMovimentoItem = $oFachadaFinanceiro->recuperarUmMnyMovimentoItem($aFa[0],$aFa[1]);

         $_POST['fMovInc'] = $_SESSION['Perfil']['Login'] .'_'. date('d/m/Y H:i:s') ;
         $_POST['fMovObs'] = "Devoluçao Referente a FA: " . $oMnyMovimentoItem->getMovCodigo().'.'.$oMnyMovimentoItem->getMovItem(); //descrição...
         $_POST['fMovDocumento']= "Estorno"; //numero documento...
         $_POST['fConCodigo'] = 9;
         $_POST['fMovParcelas']= 1;
         $_POST['fMovTipo']= 189;

         $oMnyMovimentoDEVOLUCAO = $oFachadaFinanceiro->inicializarMnyMovimento($_POST['fMovCodigo'],$oMnyMovimento->getMovDataInclusaoFormatado(),$oMnyMovimento->getMovDataEmissaoFormatado(),$oMnyMovimento->getCusCodigo() ,$oMnyMovimento->getNegCodigo(),
         $_POST['fCenCodigo'],$oMnyMovimento->getUniCodigo(),$_POST['fConCodigo'],$oMnyMovimento->getPesCodigo(),$oMnyMovimento->getSetCodigo(),
         addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$oMnyMovimento->getMovAlt(),($oMnyMovimento->getMovContrato()) ? $oMnyMovimento->getMovContrato(): "NULL",addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$oMnyMovimento->getEmpCodigo(),
         $oMnyMovimento->getMovIcmsAliq(),$oMnyMovimento->getMovPis(),$oMnyMovimento->getMovConfins(),$oMnyMovimento->getMovCsll(),$oMnyMovimento->getMovIss(),$oMnyMovimento->getMovIr(),$oMnyMovimento->getMovIrrf(),
         $oMnyMovimento->getMovInss(),$oMnyMovimento->getMovOutros(),$oMnyMovimento->getMovDevolucao(),1,$oMnyMovimento->getMovOutrosDesc());



         $oMnyMovimentoItemMaxItem = $oFachadaFinanceiro->recuperarUmMnyMovimentoItemPorMaxItem($aFa[0]) ;;

         $oMaxAporteItem = $oFachadaFinanceiro->recuperarUmMnyAporteItemPorMaxItem($_REQUEST['nCodSolicitacao']);

        //$oMnyMovimentoItem->setMovItem(97);
        $_POST['fMovCodigo'] = $oMnyMovimentoItem->getMovCodigo();
        //ULTIMO ITEM
        $_POST['fMovItem'] = $oMnyMovimentoItemMaxItem->getMovItem() + 1;
        $_POST['fMovDataVencto'] = $oMnyMovimentoItem->getMovDataVenctoFormatado();
        $_POST['fMovDataPrev'] = $oMnyMovimentoItem->getMovDataPrevFormatado();
        $_POST['fMovValorParcela'] = $oMnyMovimentoItem->getMovValorParcelaFormatado();
        $_POST['fMovJuros'] = $oMnyMovimentoItem->getMovJurosFormatado();
        $_POST['fValorPagar'] = $oMnyMovimentoItem->getMovValorPagarFormatado();
        $_POST['fFpgCodigo'] = $oMnyMovimentoItem->getFpgCodigo();
        $_POST['fTipDocCodigo'] = $oMnyMovimentoItem->getTipDocCodigo();
        $_POST['fMovRetencao'] = $oMnyMovimentoItem->getMovRetencaoFormatado();
        $_POST['fMovDataInclusao'] = $oMnyMovimentoItem->getMovDataInclusaoFormatado();
        $_POST['fMovValorTarifaBanco'] = $oMnyMovimentoItem->getMovValorTarifaBancoFormatado();
        $_POST['fTipAceCodigo'] = $oMnyMovimentoItem->getTipAceCodigo();
        $_POST['fCompetencia'] = $oMnyMovimentoItem->getCompetenciaFormatado();
        1;
        0;
        1;
        $_POST['fCaixinha'] = $oMnyMovimentoItem->getCaixinha();
        $_POST['fFaOrigem'] = $oMnyMovimentoItem->getMovCodigo() . "." .$oMnyMovimentoItem->getMovItem();
        $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
        $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
        $oClone = $oFachadaFinanceiro->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$_POST['fMovDataPrev'],$_POST['fMovValorParcela'],$_POST['fMovJuros'],$_POST['fValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'],$_POST['fTipAceCodigo'],$_POST['fCompetencia'],1,0,1,$_POST['fCaixinha'],$_POST['fFaOrigem'],$nNf,$nConciliado);
        $oMnyAporteItemPAGAR = $oFachadaFinanceiro->inicializarMnyAporteItem($_REQUEST['nCodSolicitacao'],($oMaxAporteItem->getAporteItem() + 1) ,$oClone->getMovCodigo(),$oClone->getMovItem(),0,0,1);

        //print_r( $oMnyAporteItemPAGAR );
        //die();

        //acrescenta o item como estorno
        $oMnyMovimentoItem->setFinalizado(2);
        $oFachadaFinanceiro->alterarMnyMovimentoItem($oMnyMovimentoItem);
        //TRAMITAÇÂO...
        // 10 - PAGAMENTO
        // ITEM ESTORNADO

        if($nRespConciliacao){
            //CONCILIACAO...
            $nDe = 11;
            $nPara = 11;
        }else{
            //PAGAMENTO...
            $nDe = 10;
            $nPara = 10;
        }

        $oMnyMovimentoItemProtocoloITEMESTORNADO = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$oMnyMovimentoItem->getMovItem(),11,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Estornado.",MB_CASE_UPPER, "UTF-8"),1);

        //print_r($oClone);
        //Acrescenta o item a ser pago
        $oFachadaFinanceiro->inserirMnyMovimentoItem($oClone);
        $oFachadaFinanceiro->inserirMnyAporteItem($oMnyAporteItemPAGAR);

        //insere o movimento referente a devolução do estorno
        $nMovCodigo = $oFachadaFinanceiro->inserirMnyMovimento($oMnyMovimentoDEVOLUCAO);

        //insere o item referente a Devolucao
        $oClone->setFaOrigem($oMnyMovimentoItem->getMovCodigo().'.'.$oMnyMovimentoItem->getMovItem());
        $oClone->setMovCodigo($nMovCodigo);
        $oClone->setMovItem(1);
        $oFachadaFinanceiro->inserirMnyMovimentoItem($oClone);

        //TRAMITAÇÂO...
        //ITEM PARA PAGAMENTO
        $oMnyMovimentoItemProtocoloITEMPAGAMENTO = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],11,10,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case(("Estorno do movimento : " . $oMnyMovimentoItem->getMovCodigo() .".". $oMnyMovimentoItem->getMovItem())  ,MB_CASE_UPPER, "UTF-8"),1);
        $oMnyMovimentoItemProtocoloDEVOLUCAO = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$oClone->getMovCodigo(),$oClone->getMovItem(),11,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case(("Estorno do movimento : " . $oMnyMovimentoItem->getMovCodigo() .".". $oMnyMovimentoItem->getMovItem())  ,MB_CASE_UPPER, "UTF-8"),1);

        $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloITEMESTORNADO);
        $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloITEMPAGAMENTO);
        $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloDEVOLUCAO);
        //die();
        $sHeader = "?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar&nTipoCaixa=1&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $dData;

        header("Location:".$sHeader);
        exit();

     }

    public function finalizaMovimento(){
        $oFachada = new FachadaFinanceiroBD();

        $nMovCodigo = $_REQUEST['nMovCodigo'];
        $nMovItem = $_REQUEST['nMovItem'];

        $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($nMovCodigo,$nMovItem);
        $oMnyMovimento = $oMnyMovimentoItem->getMnyMovimento();
        if($oMnyMovimento->getMovTipo()==188)
            $sTipo="Pagar";
        else
            $sTipo="Receber";
        $nTipoContrato = ($oMnyMovimento->getMovContrato()) ? $oMnyMovimento->getTipoContrato() : "4";

        //ESTORNO...
        if($oMnyMovimentoItem->getFinalizado != 2){
            $oMnyMovimentoItem->setFinalizado(1);
        }

        $nContabil =($_REQUEST['fContabil']) ? "1" : "0";
        $oMnyMovimentoItem->setContabil($nContabil);

        $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem);

        $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$nMovCodigo,$nMovItem,12,12,$_SESSION['oUsuarioImoney']->getLogin(),date('d/m/Y H:i:s'),'PROCESSO FINALIZADO',1);
        $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);

        $_SESSION['sMsg'] = "Tramitação Encerrada no dia ".date('d/m/Y')."!";
        $sHeader = "action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=".$sTipo."&nIdMnyMovimentoItem=".$nMovCodigo."||".$nMovItem."&MovContrato=".(($oMnyMovimento->getMovContrato()) ? $oMnyMovimento->getMovContrato(): '4')."&nTipoContrato=".$nTipoContrato;

        header("Location: ?".$sHeader);
        exit();



    }
	 
 }

 ?>
