<?php
 class MnyMovimentoItemProtocoloCTR implements IControle{
 
 	public function MnyMovimentoItemProtocoloCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$voMnyMovimentoItemProtocolo = $oFachada->recuperarTodosMnyMovimentoItemProtocolo();
 		$_REQUEST['voMnyMovimentoItemProtocolo'] = $voMnyMovimentoItemProtocolo;
 		
 		include_once("view/financeiro/mny_movimento_item_protocolo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 
 		$oMnyMovimentoItemProtocolo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyMovimentoItemProtocolo = ($_POST['fIdMnyMovimentoItemProtocolo'][0]) ? $_POST['fIdMnyMovimentoItemProtocolo'][0] : $_GET['nIdMnyMovimentoItemProtocolo'];
 	
 			if($nIdMnyMovimentoItemProtocolo){
 				$vIdMnyMovimentoItemProtocolo = explode("||",$nIdMnyMovimentoItemProtocolo);
 				$oMnyMovimentoItemProtocolo = $oFachada->recuperarUmMnyMovimentoItemProtocolo($vIdMnyMovimentoItemProtocolo[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyMovimentoItemProtocolo'] = ($_SESSION['oMnyMovimentoItemProtocolo']) ? $_SESSION['oMnyMovimentoItemProtocolo'] : $oMnyMovimentoItemProtocolo;
 		unset($_SESSION['oMnyMovimentoItemProtocolo']);
 
		$_REQUEST['voMnyMovimentoItem'] = $oFachada->recuperarTodosMnyMovimentoItem();
		$_REQUEST['voSysTipoProtocolo'] = $oFachadaSys->recuperarTodosSysTipoProtocolo();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_movimento_item_protocolo/detalhe.php");
 		else
 			include_once("view/financeiro/mny_movimento_item_protocolo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
        $oFachadaSys = new FachadaSysBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fDeCodTipoProtocolo'],$_POST['fParaCodTipoProtocolo'],mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),$_POST['fData'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
 			$_SESSION['oMnyMovimentoItemProtocolo'] = $oMnyMovimentoItemProtocolo;	
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 			if(!($_FILES["arquivo"]["tmp_name"])){
                $oValidate->add_number_field("Codigo",$oMnyMovimentoItemProtocolo->getMovCodigo(), "number", "y");
                $oValidate->add_number_field("Item",$oMnyMovimentoItemProtocolo->getMovItem(), "number", "y");
                $oValidate->add_number_field("De",$oMnyMovimentoItemProtocolo->getDeCodTipoProtocolo(), "number", "y");
                $oValidate->add_number_field("Para",$oMnyMovimentoItemProtocolo->getParaCodTipoProtocolo(), "number", "y");
                $oValidate->add_text_field("Responsavel",$oMnyMovimentoItemProtocolo->getResponsavel(), "text", "y");
                //$oValidate->add_date_field("Data",$oMnyMovimentoItemProtocolo->getData(), "date", "y");
                //$oValidate->add_text_field("Descricao",$oMnyMovimentoItemProtocolo->getDescricao(), "text", "y");
                $oValidate->add_number_field("Ativo",$oMnyMovimentoItemProtocolo->getAtivo(), "number", "y");
            }

			$sHeader = "?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=". $_REQUEST['sTipoLancamento']."&nIdMnyMovimentoItem=".$oMnyMovimentoItemProtocolo->getMovCodigo()."||".$oMnyMovimentoItemProtocolo->getMovItem();
 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 			//	$sHeader = "?bErro=1&action=MnyMovimentoItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdMnyMovimentoItemProtocolo=".$_POST['fCodProtocoloMovimentoItem']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
                $oUltimaTramitacao = $oFachada->recuperarMnyMovimentoImtemProtocoloPorMovimentoItemWizardBreadcrumb($oMnyMovimentoItemProtocolo->getMovCodigo(),$oMnyMovimentoItemProtocolo->getMovItem());
 				if((($oUltimaTramitacao) && ($oUltimaTramitacao->de_cod_tipo_protocolo != $_SESSION['Perfil']['CodGrupoUsuario'] || $oUltimaTramitacao->de_cod_tipo_protocolo == '13'))){

                       if($_FILES["arquivo"]["tmp_name"]){
                                      $oMnyMovimentoItemProtocolo->setDescricao('NF ANEXADA');
                                      $oMnyMovimentoItemProtocolo->setDeCodTipoProtocolo(18);
                                      $oMnyMovimentoItemProtocolo->setParaCodTipoProtocolo(12);

                                      $Arquivo = $_FILES["arquivo"]["tmp_name"];
                                      $Tipo = $_FILES["arquivo"]["type"];

                                      $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                                      $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];

                                      $nomeArquivo =  "NF_" .$_POST['fMovCodigo'].'.'.$_POST['fMovItem'];
                                      $tamanhoArquivo = $_FILES["arquivo"]["size"];
                                      $nomeTemporario = $_FILES["arquivo"]["tmp_name"];
                                      $vPermissao = array("application/pdf");
                                      $tipoArquivo = strtok($_FILES["arquivo"]["type"], ";");
                                      $nCodContratoDocTipo = $_REQUEST['fCodTipo'];
                                      $nContratoCodigo = ($_REQUEST['fContratoCodigo']) ? $_REQUEST['fContratoCodigo'] : 'NULL';
                                      $_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i:s');
                                      move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                      $blobArquivo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                      $blobArquivo = addslashes(fread($blobArquivo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                      $oSysArquivo = $oFachadaSys->inicializarSysArquivo($_POST['fCodArquivo'],$nomeArquivo,$blobArquivo,$tipoArquivo,$nCodContratoDocTipo,$tamanhoArquivo,date('Y-m-d H:i:s'),mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),1,$nContratoCodigo,'NULL', $_POST['fMovCodigo'], $_POST['fMovItem']);
                                      $oSysArquivo->setArquivo($blobArquivo);
                                      unlink("c:\\wamp\\tmp\\arquivo.pdf");

                                      $nIdArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo);
                                      $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);

                        }else{

                            if($oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo)){
                                if($_REQUEST['fContabil'] != 1){
                                    $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($oMnyMovimentoItemProtocolo->getMovCodigo(),$oMnyMovimentoItemProtocolo->getMovItem());
                                    $oMnyMovimentoItem->setContabil('0');
                                    //print_r($oMnyMovimentoItem);
                                    //die();
                                    $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem);
                                }
                                unset($_SESSION['oMnyMovimentoItemProtocolo']);
                                $_SESSION['sMsg'] = "Tramita&ccedil;&atilde;o realizada com sucesso!";
                            } else {
                                $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel tramitar!";
                            }
                    }

                }else{
                    $_SESSION['sMsg'] = "Este F.A já foi tramitada !";
                }
				
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo)){
 					unset($_SESSION['oMnyMovimentoItemProtocolo']);
 					$_SESSION['sMsg'] = "Tipo de Protocolo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimentoItemProtocolo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Protocolo!";
 					$sHeader = "?bErro=1&action=MnyMovimentoItemProtocolo.preparaFormulario&sOP=".$sOP."&nIdMnyMovimentoItemProtocolo=".$_POST['fCodProtocoloMovimentoItem']."";
 				}
 			break;
 			case "Excluir":
			    $bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyMovimentoItemProtocolo']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyMovimentoItemProtocolo($sCampoChave);\n");
				}
 				if($bResultado){
 					$_SESSION['sMsg'] = "Tipo de Protocolo(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimentoItemProtocolo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Tipo de Protocolo!";
 					$sHeader = "?bErro=1&action=MnyMovimentoItemProtocolo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}

   /*
     public function notificaTramitacao(){

 		$FachadaFinanceiro = new FachadaFinanceiroBD();
		$nCodGrupoUsuario = $_SESSION['oUsuarioImoney']->getAcessoGrupoUsuario()->getCodGrupoUsuario();
		$voMnyMovimentoItemProtocolo = $FachadaFinanceiro->recuperarTodosMnyMovimentoItemProtocoloPorGrupoUsuario($nCodGrupoUsuario,$_SESSION['oEmpresa']->getEmpCodigo());
		$i=0;
		if($voMnyMovimentoItemProtocolo){
			foreach($voMnyMovimentoItemProtocolo as $oMnyMovimentoItemProtocolo){
				$voMnyMovimentoItemProtocolo[$i] = $FachadaFinanceiro->recuperarUmMnyMovimentoItemProtocoloPorMovimentoItem($nCodGrupoUsuario,$oMnyMovimentoItemProtocolo->getMovCodigo(),$oMnyMovimentoItemProtocolo->getMovItem(),$_SESSION['oEmpresa']->getEmpCodigo());				
				$i++;
			}
			$_REQUEST['totalPendencias'] = $i;
			$_REQUEST['voMnyMovimentoItemProtocolo'] = $voMnyMovimentoItemProtocolo;
			include_once('view/financeiro/mny_movimento_item_protocolo/notificacao_tramitacao_ajax.php');
			
		}
		exit();
	}
   */
     public function notificaTramitacao(){

         $oFachadaFinanceiro = new FachadaFinanceiroBD();

         //chefe de obra...
         $nParaCodTipoProtocolo=1;
         $nValidado =0;

         $voVTramitacaoPendencias = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPorPendenciasPorRealizados($nParaCodTipoProtocolo,$nValidado);

         $nValidado = 1;

         $voVTramitacaoRealizados = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPorPendenciasPorRealizados($nParaCodTipoProtocolo,$nValidado,true);

         $_REQUEST['voVTramitacaoPendencias'] = $voVTramitacaoPendencias;

         $_REQUEST['voVTramitacaoRealizados'] = $voVTramitacaoRealizados;

         include_once('view/financeiro/mny_movimento_item_protocolo/notificacao_tramitacao_ajax.php');

         exit();
     }


     public function ListaEmAndamento(){
 		$oFachada = new FachadaFinanceiroBD();

        $nCodUnidade = $_REQUEST['fCodUnidade'];

        if($_REQUEST['fCodUnidade']){
            $_REQUEST['voRealizados'] = $oFachada->recuperarTodosMnyMovimentoItemTramitacaoRealizadosPorUnidade($nCodUnidade);
        }else{
            $_REQUEST['voRealizados'] = $oFachada->recuperarTodosMnyMovimentoItemTramitacaoRealizados();
        }
         include_once("view/includes/realizados_ajax.php");
 		exit();

 	}

 
 }
 

 ?>
