<?php
 class MnyContratoArquivoCTR implements IControle{
 
 	public function MnyContratoArquivoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$voMnyContratoArquivo = $oFachada->recuperarTodosMnyContratoArquivo();
 		$_REQUEST['voMnyContratoArquivo'] = $voMnyContratoArquivo;
 		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();
		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();
 		include_once("view/financeiro/mny_contrato_arquivo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		
		
		if($_REQUEST['$sOP'] == "Atualizar"){
			$sArquivo = $oFachada->recuperarUmMnyContratoArquivo($_REQUEST['fIdArquivo']);
		}
 
 		$oMnyContratoArquivo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContratoArquivo = ($_POST['fIdMnyContratoArquivo'][0]) ? $_POST['fIdMnyContratoArquivo'][0] : $_GET['nIdMnyContratoArquivo'];
 	
 			if($nIdMnyContratoArquivo){
 				$vIdMnyContratoArquivo = explode("||",$nIdMnyContratoArquivo);
 				$oMnyContratoArquivo = $oFachada->recuperarUmMnyContratoArquivo($vIdMnyContratoArquivo[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyContratoArquivo'] = ($_SESSION['oMnyContratoArquivo']) ? $_SESSION['oMnyContratoArquivo'] : $oMnyContratoArquivo;
 		unset($_SESSION['oMnyContratoArquivo']);
 
 		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();

		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_contrato_arquivo/detalhe.php");
 		else
 			//include_once("view/financeiro/mny_contrato_arquivo/insere_altera.php");
			$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipo();								
			$_REQUEST['oMnyContratoPessoa'] = $oFachada->recuperarUmMnyContratoPessoa($_REQUEST['CodContrato']);
			$_REQUEST['oMnyMovimento']  = $oFachada->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);			

			include_once("view/financeiro/mny_contrato_arquivo/anexo_documento.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){		
		
			if(($_REQUEST['sOP'] == "VincularDocumento" )  || ( $_REQUEST['sOP'] == "Cadastrar")){
				$i=0;
				$nErro=0;

				$vCampoChave = array();
				$nTotalArquivos=0;
				$arquivos = $_FILES["arquivo"]; 
				$nCodContrato = $_POST['fContratoCodigo'];				
				$tamanhoMaximo = 100000000;				
				$nCodContratoArquivo = $_REQUEST['fCodContratoArquivo'];
					
				for($i = 0 ; count($arquivos['name']) > $i  ;$i++){	
				
					if($arquivos['name'][$i] != ""){
						$nCodContratoDocTipo = $_REQUEST['fCodContratoDocTipo'];					
					 	
						$oTipoDocumento = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i] );
						$oTipoDocumento = str_replace(" ","-",strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($oTipoDocumento->getDescricao())), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) ));
						// classUpload
						$nomeArquivo =  $nCodContrato . "_" . $oTipoDocumento . "_" . $arquivos['name'][$i];
						
						$tamanhoArquivo = $arquivos['size'][$i];
						$nomeTemporario = $arquivos['tmp_name'][$i];
						$caminho = $_SERVER['DOCUMENT_ROOT'] . "webmoney/arquivos/";
						$vPermissao = array("application/pdf");
						$tipoArquivo = strtok($arquivos['type'][$i], ";");			
					
					//verificacao de erros	
						if($tamanhoArquivo <= 0){				
							$_SESSION['sMsg'] = "Erro 1: Erro no tamanho do arquivo: ".$tamanhoArquivo ." Kb. Tamanho Maximo é ".$tamanhoMaximo." !";
							$nErro++;
						}
						if($tamanhoArquivo > $tamanhoMaximo){
							$_SESSION['sMsg'] = "Erro 2: Tamanho máximo é: ".$tamanhoMaximo." KB!";
							$nErro++;
						}
						if(!is_uploaded_file($nomeTemporario)){
							$_SESSION['sMsg'] = "Erro 3: Ocorreu um erro na transfêrencia do arquivo";
							$nErro++;
						}
						
						if (!in_array($tipoArquivo, $vPermissao)){
							$_SESSION['sMsg'] = "Erro 4: Arquivo precisa ser PDF.";;
							$nErro++;
						}
		
						if(move_uploaded_file($nomeTemporario, ($caminho . $nomeArquivo))){
							$sNomeArquivoBD = "arquivos/" . $nomeArquivo;
					
							$oContratoDocTipoItem = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i]);
			
					
					$_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i');
				
					if($nCodContratoArquivo[$i] == "" ){$nCodContratoArquivo[$i] = $nCodContratoArquivo[$i -1];}
				
								if((!(empty($nCodContratoArquivo[$i]))) && ($oContratoDocTipoItem->getItem() == 0)){		
								
								$oMnyContratoArquivo = $oFachada->recuperarUmMnyContratoArquivo($nCodContratoArquivo[$i]);	
								$vArquivo = explode("/",$oMnyContratoArquivo->getNome());
								$sNomeCompleto = $_SERVER['DOCUMENT_ROOT'] . "imoney_novo/" . $oMnyContratoArquivo->getNome();
								unlink($sNomeCompleto);
								$oFachada->excluirMnyContratoArquivo($nCodContratoArquivo[$i]);
							}
							$_POST['fCodConciliacao'] = NULL;
							$oMnyContratoArquivo = $oFachada->inicializarMnyContratoArquivo($_POST['fCodContratoArquivo'],$_POST['fContratoCodigo'],$_POST['fCodConciliacao'],$nCodContratoDocTipo[$i],$sNomeArquivoBD,$_POST['fRealizadoPor'],1);						
							if(!$nIdContratoArquivo = $oFachada->inserirMnyContratoArquivo($oMnyContratoArquivo)){
								$nErro++;							
							}						
						}else{	
							$_SESSION['sMsg'] = "Erro 5: Ocorreu um erro na transfêrencia do arquivo";
							$nErro++;		
						}						
					}
					
				}
				
			}else{
				$_SESSION['sMsg'] = "Selecione um arquivo!";
			}
				echo "<br>" . $_SESSION['sMsg'];	
				$i++;
		
 
			if($nErro == 0){
				$_SESSION['sMsg'] = "Arquivos vinculados com sucesso!";
				$nMovimento = $_REQUEST['fMovCodigo'];
			
				
			
				if($_REQUEST['fMovCodigo']){				
					$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sPagina=1&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=". $nMovimento ;															
				}else{
					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista";	
				}			
			}else{
				$nMovimento = $_REQUEST['fMovCodigo'];
				$sHeader = "?bErro=0&action=MnyMovimento.preparaFormulario&sOP=Alterar&sPagina=1&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=". $nMovimento ;															
			}
					
		//$sHeader = "?action=MnyContratoPessoa.preparaFormulario&sOP=VincularDocumento&CodContrato=".$_POST['fContratoCodigo']."&fMovCodigo=".$_REQUEST['fMovCodigo'];
		//$sHeader = "?action=MnyContratoPessoa.preparaLista";
		header("Location: ".$sHeader);	
		die();		
				
		}else{
			
			if($_REQUEST['sOP'] == 'Excluir'){	
			
				$oMnyContratoArquivo = $oFachada->recuperarUmMnyContratoArquivo($_REQUEST['fCodContratoArquivo']);									
				$bResultado &= $oFachada->excluirMnyContratoArquivo($_REQUEST['fCodContratoArquivo']);	
				$sNomeCompleto = $_SERVER['DOCUMENT_ROOT'] . "imoney_novo/" . $oMnyContratoArquivo->getNome();			
			}
			$bResultado = true;

			
			if($bResultado){
				unlink($sNomeCompleto);
				$_SESSION['sMsg'] = "Documentos(s) exclu&iacute;do(s) com sucesso!";
//				$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaLista";


                if($_REQUEST['nTipo'] != 1){
					$sHeader = "?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_REQUEST['nMovCodigo']."&fContratoTipo=".$_REQUEST['fContratoTipo'];

                }else{
					$sHeader = "?action=MnyContratoPessoa.preparaFormulario&sOP=Alterar&Pagina=2&fTipoContrato=".$_REQUEST['fTipoContrato']."&nIdMnyContratoPessoa=".$_REQUEST['nIdMnyContratoPessoa'];
                }
			} else {
				$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Documentos!";
				$sHeader = "?bErro=1&action=MnyContratoPessoa.preparaLista";
			}	
			header("Location: ".$sHeader);		
		}
 
 }




	public function carregaArquivosServidor(){
        set_time_limit(3600);
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaSys = new FachadaSysBD();
        $nCodTransferencia = $_GET['fCodTransferencia'];
 		$voArquivo = $oFachada->recuperarTodosMnyContratoArquivoPorTipoArquivoTransferencia($nCodTransferencia);

        if($voArquivo){
            foreach($voArquivo as $oArquivo){
                //$sNomeArquivo = 'http://200.98.201.88/webmoney/' . $oArquivo->getNome();
               // $sNomeArquivo = 'http://200.98.201.88/webmoney/arquivos/44123_1_recibo-pagamento_20171027133007623.pdf';
                //$sNomeArquivo = 'C:\wamp\www\webmoney\arquivos\44123_1_recibo-pagamento_20171027133007623.pdf';

                $sNomeArquivo = $_SERVER['DOCUMENT_ROOT'] . "\\webmoney\\" . $oArquivo->getNome();

                if(file_exists($sNomeArquivo)){
                    $handle = @fopen($sNomeArquivo, "r");
                    if ($handle) {
                        while (($buffer = fgets($handle, 4096)) !== false) {
                            $sConteudoArquivo .= $buffer;

                        }
                        if (!feof($handle))
                            echo "Erro: falha inesperada de fgets()\n";

                        fclose($handle);
                    }


                    $oMnyTransferencia = $oFachada->recuperarUmMnyTransferenciaPorMovimentoItem($oArquivo->getMovCodigo(),$oArquivo->getMovItem());
                    //$oMnyTransferencia = $oFachada->recuperarUmMnyTransferenciaPorMovimentoItem(44123,1);
                    $sNome = 'TRANSF_'. $oMnyTransferencia->getCodTransferencia();
                    $sExtensao = 'application/pdf';
                    $vRealizadoPor = explode("||",$oArquivo->getRealizadoPor());
                    $vData = explode("/",$vRealizadoPor[1]);
                    $vHora = explode(" ",$vData[2]);
                    $dDataHora = $vHora[0] . '-'. $vData[1] . '-' . trim($vData[0]) . " " . $vHora[1];
                    $sRealizadoPor = $vRealizadoPor[0];
                    $nMnyCodArquivo = $oArquivo->getCodArquivo();
                    $nCodConciliacao = $oArquivo->getCodConciliacao();
                    $nContratoCodigo = $oArquivo->getContratoCodigo();
                    $oSysArquivo = $oFachadaSys->inicializarSysArquivo($nCodArquivo,$sNome,addslashes($sConteudoArquivo),$sExtensao,$oArquivo->getCodContratoDocTipo(),filesize($sNomeArquivo),$dDataHora,$sRealizadoPor,1,$nContratoCodigo,$nCodConciliacao,$oArquivo->getMovCodigo(),$oArquivo->getMovItem());
                    if($nCodArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo)){
                        echo "<br><font style='color:green'>". $oArquivo->getMovCodigo() . "." . $oArquivo->getMovItem() . "||" . $oArquivo->getCodContratoDocTipo() . "</font>";

                        unset($sConteudoArquivo);
                        unset($buffer);
                        unset($handle);
                    }else{
                        echo "<br><font style='color:red'>Não inserido: ". $oArquivo->getMovCodigo() . "." . $oArquivo->getMovItem() . "||" . $oArquivo->getCodContratoDocTipo() . "</font>";
                    }

           	    }else{
                    echo "<br>Não Encontrado:" . $sNomeArquivo;
                }

        }

        }else{
            die('sem arquivos');
       }
   }
 
}
 ?>
