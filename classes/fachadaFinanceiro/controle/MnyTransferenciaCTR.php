<?php
 class MnyTransferenciaCTR implements IControle{
 
 	public function MnyTransferenciaCTR(){
 	
 	}
 
	public function preparaLista(){		
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();
 		$_REQUEST['voVTransferencia'] = $oFachadaView->recuperarTodosVTransferencia($_SESSION['oEmpresa']->getEmpCodigo());	
		include_once("view/financeiro/mny_transferencia/index.php");
 		exit(); 	
 	} 

 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();		
		$oFachadaView = new FachadaViewBD();
 
 		$oMnyTransferencia = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyTransferencia = ($_POST['fIdMnyTransferencia'][0]) ? $_POST['fIdMnyTransferencia'][0] : $_GET['nIdMnyTransferencia'];
 	
 			if($nIdMnyTransferencia){
 				$vIdMnyTransferencia = explode("||",$nIdMnyTransferencia);
 				$oMnyTransferencia = $oFachada->recuperarUmMnyTransferencia($vIdMnyTransferencia[0]);
				
 				$_REQUEST['oMnyMovimentoDe'] = $oFachada->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoOrigem());
 				$_REQUEST['oMnyMovimentoItemDe'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['oMnyMovimentoDe']->getMovCodigo(),1);
				$_REQUEST['voMnyContaCorrenteDe'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoDe']->getUniCodigo());

 				$_REQUEST['oMnyMovimentoPara'] = $oFachada->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoDestino());
 				$_REQUEST['oMnyMovimentoItemPara'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['oMnyMovimentoPara']->getMovCodigo(),1);
                $_REQUEST['voMnyContaCorrentePara'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoPara']->getUniCodigo());

				$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
		
			}
 		}
 		
 		$_REQUEST['oMnyTransferencia'] = ($_SESSION['oMnyTransferencia']) ? $_SESSION['oMnyTransferencia'] : $oMnyTransferencia;
 		unset($_SESSION['oMnyTransferencia']);


        
		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
        
		$sGrupo = '1'; // Conta...
		$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
	
		$sGrupo = '7'; // Centro de Negocio
		$_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '8'; // Unidade
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = '6'; // Custo
		$_REQUEST['voMnyCusto'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = 2; // Centro de Custo
		$_REQUEST['voMnyCentroCusto'] = $oFachada->recuperarTodosMnyPlanoContasCabeca($sGrupo);

	    $_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial(7,0);

		if($_REQUEST['sOP'] == "Detalhar"){

            include_once("view/financeiro/mny_transferencia/detalhe.php");
        }else{
            $sGrupo = 2;
			$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasTransf($sGrupo,1);
		   $_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
			$_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
			if($_REQUEST['nIdMnyMovimento'])
				$_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['nIdMnyMovimento']);


 			include_once("view/financeiro/mny_movimento/insere_altera_trans.php");
        }
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaSys = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
			
			//if($_REQUEST['sOP'] == "Alterar"){
			if($_REQUEST['sOP'] == "AlterarTransferencia"){


                $voMnyContaCorrenteCaixinha = $oFachada->recuperarTodosMnyContaCorrenteCaixinha();
                foreach($voMnyContaCorrenteCaixinha As $oMnyContaCorrenteCaixinha){
                    $aCaixinha[] = $oMnyContaCorrenteCaixinha->getCcrCodigo();
                }

                 if(in_array($_POST['fContaPara'],$aCaixinha)){
                        $nCaixinha = 1 ;
                     }else{
                        $nCaixinha = 0;
                     }



				$oMnyTransferencia = $oFachada->inicializarMnyTransferencia($_POST['fCodTransferencia'],$_POST['fMovCodigoOrigem'],$_POST['fMovItemOrigem'],$_POST['fContaDe'],$_POST['fMovCodigoDestino'],$_POST['fMovItemDestino'],$_POST['fContaPara'],$_POST['fTipoAplicacao'],$_POST['fCodSolicitacaoAporte'],$_POST['fOperacao'],$_POST['fAtivo']);

				$oMnyMovimentoOrigem= $oFachada->recuperarUmMnyMovimento($_POST['fMovCodigoOrigem']);
				$oMnyMovimentoOrigemItem = $oFachada->recuperarUmMnyMovimentoItem($_POST['fMovCodigoOrigem'],1);
				
				$oMnyMovimentoDestino = $oFachada->recuperarUmMnyMovimento($_POST['fMovCodigoDestino']);
				$oMnyMovimentoItemDestino = $oFachada->recuperarUmMnyMovimentoItem($_POST['fMovCodigoDestino'],1);
				



                if($oMnyMovimentoDestino->getMovObs() != $_REQUEST['fMovObs']){
					 $oMnyMovimentoDestino->setMovObs($_REQUEST['fMovObs']);
					 $oMnyMovimentoOrigem->setMovObs($_REQUEST['fMovObs']);
				}

			
				if($oMnyMovimentoDestino->getMovDocumento() != $_REQUEST['fMovDocumento']){
					 $oMnyMovimentoDestino->setMovDocumento($_REQUEST['fMovDocumento']);
					 $oMnyMovimentoOrigem->setMovDocumento($_REQUEST['fMovDocumento']);
				}

				if($oMnyMovimentoDestino->getConCodigo() != $_REQUEST['fConCodigo']){
					 $oMnyMovimentoDestino->setConCodigo($_REQUEST['fConCodigo']);
					 $oMnyMovimentoOrigem->setConCodigo($_REQUEST['fConCodigo']);
				}

				if($oMnyMovimentoDestino->getNegCodigo() != $_REQUEST['fNegCodigo']){
					 $oMnyMovimentoDestino->setNegCodigo($_REQUEST['fNegCodigo']);
					 $oMnyMovimentoOrigem->setNegCodigo($_REQUEST['fNegCodigo']);
				}

				if($oMnyMovimentoOrigem->getUniCodigo() != $_REQUEST['fUniCodigo'])	 
					$oMnyMovimentoOrigem->setUniCodigo($_REQUEST['fUniCodigo']);
				if($oMnyMovimentoDestino->getUniCodigo() != $_REQUEST['fUniCodigoPara'])
					 $oMnyMovimentoDestino->setUniCodigo($_REQUEST['fUniCodigoPara']);
				
				if($oMnyMovimentoDestino->getMovDataEmissaoFormatado() != $_REQUEST['fMovDataEmissao']){
					 $oMnyMovimentoDestino->setMovDataEmissaoBanco($_REQUEST['fMovDataEmissao']);
					 $oMnyMovimentoOrigem->setMovDataEmissaoBanco($_REQUEST['fMovDataEmissao']);
				}

				if($oMnyMovimentoDestino->getEmpCodigo() != $_REQUEST['fEmpCodigoPara']) $oMnyMovimentoDestino->setEmpCodigo($_REQUEST['fEmpCodigoPara']);


				if($oMnyMovimentoItemDestino->getMovValorParcela() != $_REQUEST['fMovValor']){
					 $oMnyMovimentoItemDestino->setMovValorParcelaBanco($_REQUEST['fMovValor']);
					 $oMnyMovimentoOrigemItem->setMovValorParcelaBanco($_REQUEST['fMovValor']);
				}

				if($oMnyMovimentoItemDestino->getCompetenciaFormatado() != $_REQUEST['fCompetencia']){
					 $oMnyMovimentoItemDestino->setCompetenciaBanco($_REQUEST['fCompetencia']);
					 $oMnyMovimentoOrigemItem->setCompetenciaBanco($_REQUEST['fCompetencia']);

				}

                $oMnyMovimentoOrigemItem->setCaixinha($nCaixinha);
                $oMnyMovimentoItemDestino->setCaixinha($nCaixinha);
				
			}else{
				$oMnyTransferencia = $oFachada->inicializarMnyTransferencia($_POST['fCodTransferencia'],$_POST['fMovCodigoOrigem'],$_POST['fMovItemOrigem'],$_POST['fContaPara'],$_POST['fMovCodigoDestino'],$_POST['fMovItemDestino'],$_POST['fPara'],$_POST['nTipoAplicacao'],$_POST['fCodSolicitacaoAporte'],$_POST['fOperacao'],$_POST['fAtivo']);
			}
			
				//$oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigoOrigem'],$_POST['fSetCodigo'],$_POST['fMovObs'],$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],$_POST['fMovDocumento'],$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']); 				
				//$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataEmissao'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,169,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],248,$_REQUEST['fCompetencia'],1);						

			$_SESSION['oMnyTransferencia'] = $oMnyTransferencia;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("MovCodigoOrigem", $oMnyTransferencia->getMovCodigoOrigem(), "number", "y");
			$oValidate->add_number_field("MovItemOrigem", $oMnyTransferencia->getMovItemOrigem(), "number", "y");
			$oValidate->add_number_field("De", $oMnyTransferencia->getDe(), "number", "y");
			$oValidate->add_number_field("MovCodigoDestino", $oMnyTransferencia->getMovCodigoDestino(), "number", "y");
			$oValidate->add_number_field("MovItemDestino", $oMnyTransferencia->getMovItemDestino(), "number", "y");
			$oValidate->add_number_field("Para", $oMnyTransferencia->getPara(), "number", "y");
			$oValidate->add_number_field("Ativo", $oMnyTransferencia->getAtivo(), "number", "y");
 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyMovimento.preparaFormularioTransferencia&sOP=".$sOP."&nIdMnyTransferencia=".$_POST['fCodTransferencia']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyTransferencia($oMnyTransferencia)){
 					unset($_SESSION['oMnyTransferencia']);
 					$_SESSION['sMsg'] = "Transfer&ecirc;ncia inserida com sucesso!";
 					$sHeader = "?bErro=0&action=MnyTransferencia.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o mny_transferencia!";
 					$sHeader = "?bErro=1&action=MnyTransferencia.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "AlterarTransferencia":
			

 				if($oFachada->alterarMnyTransferencia($oMnyTransferencia)){

					$oFachada->alterarMnyMovimento($oMnyMovimentoOrigem);
					$oFachada->alterarMnyMovimentoItem($oMnyMovimentoOrigemItem);
					
					$oFachada->alterarMnyMovimento($oMnyMovimentoDestino);
					$oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemDestino);

                    //Arquivo
                   $arquivos= $_FILES['arquivo'];
                   for($i = 0 ;  count($arquivos['name']) > $i  ;$i++){

                       if($_FILES["arquivo"]["tmp_name"][$i]){
                            $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                            $Tipo = $_FILES["arquivo"]["type"][$i];
                            $Tamanho = $_FILES["arquivo"]["size"][$i];
                            move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                            $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                            $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));

                            $sNome = 'TRANSF_'.$oMnyTransferencia->getCodTransferencia();
                            $sArquivo = $Anexo;
                            //$nCodArquivo = $_REQUEST['fCodArquivo'][$i];
                            $sExtensao = $Tipo;
                            $aTipoArquivo =  explode('||',$_REQUEST['fCodContratoDocTipo'][$i]);
                            $nCodTipo = $aTipoArquivo[0];
                            $nCodArquivo = $aTipoArquivo[1];
                            $nTamanho = $Tamanho;
                            $dDataHora= date('Y-m-d H:i:s');
                            $sRealizadoPor = $_SESSION['Perfil']['Login'];
                            $nAtivo = 1;
                            $nContratoCodigo ='NULL';
                            $nCodConciliacao = 'NULL';
                            $nMovCodigo = $oMnyMovimentoOrigemItem->getMovCodigo();
                            $nMovItem = $oMnyMovimentoOrigemItem->getMovItem();

                            //$nCodArquivo = ($_REQUEST['fCodArquivo'][$i]) ? $_REQUEST['fCodArquivo'][$i] : "";



                            $oSysArquivo = $oFachadaSys->inicializarSysArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem);

                            $oSysArquivo->setArquivo($sArquivo);
                           if($nCodArquivo){
                                $oFachadaSys->alterarSysArquivo($oSysArquivo);
                            }else{
                                $oFachadaSys->inserirSysArquivo($oSysArquivo);
                            }
                        }
                   }

 					unset($_SESSION['oMnyTransferencia']);
 					$_SESSION['sMsg'] = "Transfer&ecirc;ncia alterada com sucesso!";
 					$sHeader = "?bErro=0&action=MnyTransferencia.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Transfer&ecirc;ncia!";
 					$sHeader = "?bErro=1&action=MnyTransferencia.preparaFormulario&sOP=".$sOP."&nIdMnyTransferencia=".$_POST['fCodTransferencia']."";
 				}
 			break;
 			case "Excluir":

               /*
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyTransferencia']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$oMnyTransferencia &= \$oFachada->excluirMnyTransferencia($sCampoChave);\n");
				}
 				*/

                $oMnyTransferencia = $oFachada->excluirMnyTransferencia($_REQUEST['fIdMnyTransferencia']);

                if($oMnyTransferencia[0]->aporte){
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a Transfer&ecirc;ncia porque ela já faz parte de um aporte !";
                }elseif($oMnyTransferencia[0]->conciliacao_destino){
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a Transfer&ecirc;ncia porque ela já foi conciliada !";
                }elseif($oMnyTransferencia[0]->conciliacao_origem){
                    $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a Transfer&ecirc;ncia porque ela já foi conciliada !";
                }else{
                    $_SESSION['sMsg'] = "Transfer&ecirc;ncia exclu&iacute;da com sucesso!";
                }

                $sHeader = "?bErro=1&action=MnyTransferencia.preparaLista";

 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}

    function carregaUnidadeDestino(){
        $oFachada = new FachadaFinanceiroBD();
        $nEmpCodigo = $_REQUEST['nEmpCodigo'];
        $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($nEmpCodigo);
        include_once('view/financeiro/mny_transferencia/unidade_ajax.php');
        exit();
    }
 
 }
 ?>
