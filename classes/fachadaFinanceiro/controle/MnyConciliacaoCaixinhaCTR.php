<?php
 class MnyConciliacaoCaixinhaCTR implements IControle{

 	public function MnyConciliacaoCTR(){

 	}

    public function preparaFormulario(){}

	public function verificaUltimoDiaMesRendimento($dDataConciliacao){
		$oFachadaFinanceiroBD = new FachadaFinanceiroBD();

		//Inicio Verificação do rendimento da aplicação........
		if(substr_count($dDataConciliacao,'-') > 0 ){
			$oData = DateTime::createFromFormat('Y-m-d', $dDataConciliacao);
			$dData = $oData->format('d/m/Y');
		}else{
			$dData = $dDataConciliacao;
		}

		//adiciona o rendimento na aplicação...
 		 $voData = explode('/',$dData);
		 $dUltimoDiaMes = cal_days_in_month(CAL_GREGORIAN, $voData[1] , $voData[2]);
		 $dHoje = $voData[0];
		 if($dHoje == $dUltimoDiaMes){
			if(($_REQUEST['fRendimento'])){
				if($_REQUEST['fRendimento'] != 0){
					$nRendimento = $_REQUEST['fRendimento'];

					$voAplicacaoResgate = $oFachadaFinanceiroBD->recuperarTodosMnyConciliacaoAplcicaoResgate();
					if(!$voAplicacaoResgate){
						// se nao tiver aplicacao no ultimo dia do mes
						$oMnySaldoDiarioAplicacaoRendimento = $oFachada->recuperarUmMnySaldoDiarioAplicacaoPorContaUnidade($_REQUEST['fCodContaCorrente'] , $_REQUEST['fCodUnidade']);
						$nValorAtualizado = $oMnySaldoDiarioAplicacaoRendimento->getValor() + $nRendimento;
						$oMnySaldoDiarioAplicacaoFimMes = $oFachada->inicializarSaldoDiarioAplicacao("",$oMnySaldoDiarioAplicacaoRendimento->getCodAplicacao(),$oMnySaldoDiarioAplicacaoRendimento->getValorFinal(),$oMnySaldoDiarioAplicacaoRendimento->getOperacao(),NULL,NULL,$nValorAtualizado);

						if(!$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacaoFimMes)){
							$_SESSION['sMsg'] = "N&atilde;o foi possivel inserir o rendimento na aplica&ccedil;&atilde;o.";
							$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $dData;
							header('location:' . $sHeader);
							exit();
						}
					}else{
					// se tiver aplicacao no ultimo dia do mes

					}

				}
			}else{
				$_SESSION['sMsg'] = "Você precisa informar o valor do rendimendo.";
				$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $dData;
				header('location:' . $sHeader);
				exit();
			}
		}


	}

 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new  FachadaViewBD();
 		//$voMnyConciliacao = $oFachada->recuperarTodosMnyConciliacao();

		$voDataConciliacao = $oFachada->recuperarTodosMnyConciliacaoDataConsolidado();

		if($voDataConciliacao){
			foreach($voDataConciliacao as $dDataConciliacao){
				 $voConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalConsolidadoPorData($dDataConciliacao->getDataConciliacao());
			}
		}
		$_REQUEST['voMnyConciliacao'] = $voConciliacao;
 		include_once("view/financeiro/mny_conciliacao/index_ajax.php");
 		exit();
 	}

	public function preparaFormularioCaixinhaAbreConciliacao(){
 		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$_REQUEST['voMnyContaCorrente'] = $oFachadaFinanceiro->recuperarTodosMnyContaCorrente();
		include_once('view/financeiro/mny_conciliacao/insere_altera_abre_conciliacao.php');
		exit();
	}

	public function preparaFormularioCaixinhaReConciliacao(){
        $oFachada = new FachadaFinanceiroBD();
        $oFachadaSys = new FachadaSysBD();
        $oFachadaView = new FachadaViewBD();
        $vFA = explode("||",$_GET['nIdMnyMovimentoItem']);
        $nMovCodigo = $vFA[0];
        $nMovItem = $vFA[1];
        $nPara = $oFachada->recuperarMnyMovimentoImtemProtocoloPorMovimentoItemWizardBreadcrumb($vFA[0],$vFA[1])->para_cod_tipo_protocolo;
        if($nPara == 11){
            $oConciliacaoMovimentoItem = $oFachada->recuperarUmMnyConciliacaoPorFA($nMovCodigo,$nMovItem);
            $_REQUEST['voMnyConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoPorDataConta($oConciliacaoMovimentoItem->getDataConciliacao(),$oConciliacaoMovimentoItem->getCodContaCorrente());
            $_REQUEST['oMnySaldoDiario'] = $oFachada->recuperarUmMnySaldoDiarioPorTipoConta($oConciliacaoMovimentoItem->getCodContaCorrente(),$oConciliacaoMovimentoItem->getDataConciliacao());
            include_once('view/financeiro/mny_conciliacao/corrige_concilicao.php');
        }else{
            include_once('view/financeiro/index.php');
        }
        exit();
    }

 	public function preparaFormularioCaixinha(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();

 		$oMnyConciliacao = false;

		/*
		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyConciliacao = ($_POST['fIdMnyConciliacao'][0]) ? $_POST['fIdMnyConciliacao'][0] : $_GET['nIdMnyConciliacao'];

 			if($nIdMnyConciliacao){
 				$vIdMnyConciliacao = explode("||",$nIdMnyConciliacao);
 				$oMnyConciliacao = $oFachada->recuperarUmMnyConciliacao($vIdMnyConciliacao[0]);
 			}
 		}
		*/

		if($_REQUEST['sOP'] == "Detalhar" ){
 			$dDataConciliacao = $_POST['fDataConciliacao'];
 			$_REQUEST['voMnyConciliacaoData'] = $oFachada->recuperarTodosMnyConciliacaoConsolidadoPorData($dDataConciliacao);
 		}

 		$_REQUEST['oMnyConciliacao'] = ($_SESSION['oMnyConciliacao']) ? $_SESSION['oMnyConciliacao'] : $oMnyConciliacao;
 		unset($_SESSION['oMnyConciliacao']);



       $_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();


        $_REQUEST['voMnyTipoCaixa'] = $oFachada->recuperarTodosMnyTipoCaixa();

		$sGrupo = '8';//Tipo de Unidade
		$_REQUEST['voMnyTipoUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		//$_REQUEST['voConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoPorData(date('Y-m-d'), $_REQUEST['fCodUnidade']);
		//$_REQUEST['voConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoPorDataConta(date('Y-m-d'), $_REQUEST['fCodContaCorrente']);



		//$_REQUEST['oVMovimentoConciliacaoTotal'] = $oFachada->recuperarTodosVMovimentoConciliacaoTotalPorData($dDataConciliacao);


 		if($_REQUEST['sOP'] == "Detalhar"){
 			include_once("view/financeiro/mny_conciliacao/detalhe.php");
		}else{
				if(substr_count($_REQUEST['dData'],'-') > 0){
					$oData = DateTime::createFromFormat('Y-m-d',$_REQUEST['dData']);
					$dData = $oData->format('d/m/Y');
					$_REQUEST['dData'] = $dData ;
				}

                    $_REQUEST['sOP'] = ($_POST['sOP'])?$_POST['sOP'] : $_GET['sOP'];
                    $oFachada = new FachadaFinanceiroBD();
                    $_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidadeCaixinha($_SESSION['Perfil']['CodUnidade']);
                    $_REQUEST['oSaldoCaixinha'] = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeCaixinha($_SESSION['Perfil']['CodUnidade']);
                    include_once("view/financeiro/mny_conciliacao/insere_altera_caixinha.php");
                    exit();
		}
		exit();

 	}

 	public function processaFormulario(){}
 	public function processaFormularioCaixinha(){

//		print_r($_REQUEST['fRendimento']);
//		die();

 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];


		if($_REQUEST['naoUtil'] == 'sim' || $_REQUEST['semMovimento'] == 'sim'){


			if(substr_count($_REQUEST['fDataConciliacao'],'-') > 0 ){
				$oData = DateTime::createFromFormat('Y-m-d', $_REQUEST['fDataConciliacao']);
				$dData = $oData->format('d/m/Y');
			}else{
				$dData = $_REQUEST['fDataConciliacao'];
			}

				$oData2 = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
				$dData2 = $oData2->format('Y-m-d');

         if($oFachada->recuperarUmMnySaldoDiarioAnteriorPorContaData($_REQUEST['fCodContaCorrente'],$dData2)){
			if(!$oFachada->recuperarUmMnySaldoDiarioPorContaData($_REQUEST['fCodContaCorrente'],$dData2)){
				if($_REQUEST['consolidado'] == 0){
					if($_REQUEST['naoUtil'] == 'sim'){
						$_POST['fTipoSaldo'] = 3;
						$oMnySaldoDiario = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($_REQUEST['fCodContaCorrente']);
						$oMnySaldoDiario->setTipoSaldo(3);
						$oMnySaldoDiario->setDataBanco($_REQUEST['fDataConciliacao']);
						$oMnySaldoDiario->setValorInicial($oMnySaldoDiario->getValorFinal());
                        $oMnySaldoDiario->setCodUnidade('NULL');
                        $oMnySaldoDiario->setExtratoBancario(NULL);
                        $oMnySaldoDiario->setRealizadoPor($_SESSION['oUsuarioImoney']->getLogin() ."||".date('d/m/Y H:m') );
                        //Upload Extrato Bancário...
                        $Arquivo = $_FILES["arquivo"]["tmp_name"];
                        $Tipo = $_FILES["arquivo"]["type"];
                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                        $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                        $AnexoExtrato = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                        $oMnySaldoDiario->setExtratoBancario($AnexoExtrato);
                        $oMnySaldoDiario->setRealizadoPor($_SESSION['oUsuarioImoney']->getLogin() ."||".date('d/m/Y H:m') );
						$nCodSaldoDiario = $oFachada->inserirMnySaldoDiario($oMnySaldoDiario);
						$voMnySaldoDiarioAplicacao = $oFachada->recuperarTodosMnySaldoDiarioAplicacaoPorSaldoDiario($oMnySaldoDiario->getCodSaldoDiario());
						foreach($voMnySaldoDiarioAplicacao as $oMnySaldoDiarioAplicacao){
							$oMnySaldoDiarioAplicacao->setCodSaldoDiario($nCodSaldoDiario);
							$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao);
						}

						$_SESSION['sMsg'] = "Concilia&ccedil;&atilde;o realizada com sucesso !";
						$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
						header("Location: ".$sHeader);
						exit();
					 }else if($_REQUEST['semMovimento'] == 'sim'){
						$_POST['fTipoSaldo'] = 2;
						$oMnySaldoDiario = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($_REQUEST['fCodContaCorrente']);
						$oMnySaldoDiario->setTipoSaldo(2);

						$voMnySaldoDiarioAplicacao = $oFachada->recuperarTodosMnySaldoDiarioAplicacaoPorSaldoDiario($oMnySaldoDiario->getCodSaldoDiario());
						$oMnySaldoDiario->setDataBanco($_REQUEST['fDataConciliacao']);
						$oMnySaldoDiario->setCodUnidade('NULL');
						$oMnySaldoDiario->setValorInicial($oMnySaldoDiario->getValorFinal());

                        //Upload Extrato Bancário...
                        $Arquivo = $_FILES["arquivo"]["tmp_name"];
                        $Tipo = $_FILES["arquivo"]["type"];
                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                        $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                        $AnexoExtrato = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                        $oMnySaldoDiario->setExtratoBancario($AnexoExtrato);
                        $oMnySaldoDiario->setRealizadoPor($_SESSION['oUsuarioImoney']->getLogin() ."||".date('d/m/Y H:m') );
						$nCodSaldoDiario = $oFachada->inserirMnySaldoDiario($oMnySaldoDiario);

						if($voMnySaldoDiarioAplicacao){
							foreach($voMnySaldoDiarioAplicacao as $oMnySaldoDiarioAplicacao){
								$oMnySaldoDiarioAplicacao->setCodSaldoDiario($nCodSaldoDiario);
								$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao);
							}
						}
						$_SESSION['sMsg'] = "Concilia&ccedil;&atilde;o realizada com sucesso !";
						$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
						header("Location: ".$sHeader);
						exit();
					}
				}else{
						$_SESSION['sMsg'] = "Esta concilia&ccedil;&atilde;o j&aacute; foi fechada !";
						$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
						header("Location: ".$sHeader);
						exit();
				}

            }else{
                $_SESSION['sMsg'] = "Esta concilia&ccedil;&atilde;o já foi realizada.";
                $sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
                header("Location: ".$sHeader);
                exit();
            }

        }else{
            $_SESSION['sMsg'] = "A concilia&ccedil;&atilde;o do dia anterior n&atilde;o foi realizada.";
            $sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
            header("Location: ".$sHeader);
            exit();
        }



		}


	if($_REQUEST['sOP'] == "Consolidar"){

		if(substr_count($_REQUEST['fDataConciliacao'],'-') > 0 ){
			$oData = DateTime::createFromFormat('Y-m-d', $_REQUEST['fDataConciliacao']);
			$dData = $oData->format('d/m/Y');
		}else{
			$dData = $_REQUEST['fDataConciliacao'];
		}



        //print_r($dData);die();
		if($_REQUEST['consolidado'] == 0){
             $Tipo = $_FILES["arquivo"]["type"];

          /*
          if($_REQUEST['nCx']==1){
                if($Tipo != 'application/pdf'){
                    $_SESSION['sMsg'] = "O extrato precisa ser do tipo PDF" ;
                    $sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
                    header("Location: ".$sHeader);
                    exit();
                }
           }
           */

			//if(	 $oFachada->recuperarUmMnySaldoDiarioPorUnidadeContaData($_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente'], $_REQUEST['fDataConciliacao'])){
			if($oFachada->recuperarUmMnySaldoDiarioAnteriorPorContaData($_REQUEST['fCodContaCorrente'], $_REQUEST['fDataConciliacao'])){
					$dDataConciliacao = $_REQUEST['fDataConciliacao'];
					$_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || " . date('d/m/Y H:i');

						//if($_REQUEST['fDataConciliacao'] && $_REQUEST['fCodUnidade']){
						if($_REQUEST['fDataConciliacao']){
								if($dDataConciliacao && $_REQUEST['fCodContaCorrente']){
								   // 1. verifica se nao tem registro consolidado igual  0 para data anterior a data do alcamento verificando unidade, conta e data...

									//if(!$oMaxData = $oFachada->recuperarUmMnyConciliacaoMaxDataPorContaUnidade($_REQUEST['fCodContaCorrente'],$_REQUEST['fCodUnidade'])){
									if(!$oMaxData = $oFachada->recuperarUmMnyConciliacaoMaxDataPorConta($_REQUEST['fCodContaCorrente'])){
										$_SESSION['sMsg'] = "A concilia&ccedil;&atilde;o do dia " . $oMaxData->getDataConciliacaoFormatado() . " est&aacute; em aberto !";
										$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
										header("Location: " . $sHeader);
										exit();
									}

									//procedure...
									$voMnyConciliacao   = $oFachada->recuperarTodosMnyConciliacaoProcedure( $_REQUEST['fDataConciliacao'],$_REQUEST['fCodContaCorrente']);
									$nTotal291          = 0.00;
									$nSaldoAplicacao291 = 0.00;
									$nTotal292          = 0.00;
									$nTotal292D         = 0.00;
									$nSaldoAplicacao292 = 0.00;
									$nTotalTransfD      = 0.00;
									$nTotalTransfC      = 0.00;
									$nTotalMovC         = 0.00;
									$nTotalMovD         = 0.00;
									$nSaldoDiario       = 0.00;
									$i= 0;

									if($voMnyConciliacao){
											foreach($voMnyConciliacao as $oMnyConciliacao){
                                             // if($oMnyConciliacao->getAtivo() != 2){
												if($i == 0){
													$nSaldoDiario = $oMnyConciliacao->getValorVale(); //saldo_diario
												}
													switch($oMnyConciliacao->getOrdem()){
														case'291.C': //credito na aplicacao = debito em conta
				echo "291.C: "  .						$oMnyConciliacao->getValorResgateIrrf();				                      echo "<br><br>";
										 				$nTotal291C = $nTotal291C + $oMnyConciliacao->getValorResgateIrrf();
															$nSaldoAplicacao291 = $oMnyConciliacao->getValorResgateIof(); //saldo_diario_aplicacao
															//die($nSaldoAplicacao291);
														break;
														case'291.D':
				echo "291.D: " 	.	                    $oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
														$nTotal291D = $nTotal291D + $oMnyConciliacao->getValorResgateIrrf();
														break;
														case'292.C': //credito na aplicacao = debito em conta
				echo "292.C: "  .                           $oMnyConciliacao->getValorResgateIrrf();                                  echo "<br><br>";
															$nTotal292C = $nTotal292C + $oMnyConciliacao->getValorResgateIrrf();
															$nSaldoAplicacao292 = $oMnyConciliacao->getValorResgateIof(); // saldo_diario_aplicacao
														//	echo "<br> apli. 292 " . $nSaldoAplicacao292; die();
														break;
														case'292.D':
				echo "292.D: "  .                       $oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
														$nTotal292D = $nTotal292D + $oMnyConciliacao->getValorResgateIrrf();
														break;
														case'TRANSF.D':
				echo "TRANSF.D: " .						$oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
										                $nTotalTransfD = $nTotalTransfD + $oMnyConciliacao->getValorResgateIrrf();
														break;
														case'TRANSF.C':
				echo "TRANSF.C: " .						$oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
									    				$nTotalTransfC = $nTotalTransfC + $oMnyConciliacao->getValorResgateIrrf();
														break;
														case'C':
				echo "C: "  .                           $oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
				  	   							        $nTotalMovC = $nTotalMovC + $oMnyConciliacao->getValorResgateIrrf();
														break;
														case'D':
				echo "D: "  .							$oMnyConciliacao->getValorResgateIrrf();                                      echo "<br><br>";
						  								$nTotalMovD = $nTotalMovD + $oMnyConciliacao->getValorResgateIrrf();
														break;
													}
													$i =1;
										//}
                                      }
	//die();
									}else{
										$_SESSION['sMsg'] = "N&atilde;o foram encontradas concilia&ccedil;&otilde;es" ;

										$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $_POST['fDataConciliacao'];
										header("Location: " . $sHeader);
										exit();
									}
									$_POST['fRealizadoPor']	= $_SESSION['oUsuarioImoney']->getLogin() . " || " . date('d/m/Y h:s');

									$_POST['fValorInicial'] = $nSaldoDiario;
									$_POST['fValorFinal']   = ($nSaldoDiario + $nTotalTransfC + $nTotalMovC + $nTotal291D + $nTotal292D) - ($nTotalTransfD + $nTotalMovD + $nTotal291C + $nTotal292C);
				//die();

									if($_POST['fTipoSaldo'] != 3 && $_POST['fTipoSaldo'] != 2){
										$_POST['fTipoSaldo'] = 1;
									}

                                    //Upload Extrato Bancário...
                                    $Arquivo = $_FILES["arquivo"]["tmp_name"];
                                    $Tipo = $_FILES["arquivo"]["type"];
                                    move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                    $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                    $AnexoExtrato = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));

                                    //CONTA
                                    $_REQUEST['fTipoCaixa'] = 1;

                                    //$oMnySaldoDiario = $oFachada->inicializarMnySaldoDiario($_POST['fCodSaldoDiario'],$_REQUEST['fTipoCaixa'],$voMnyConciliacao[0]->getCodContaCorrente(),$_POST['fCodUnidade'],$voMnyConciliacao[0]->getDataConciliacaoFormatado(),str_replace('.',',',$_POST['fValorFinal']),str_replace('.',',',$_POST['fValorInicial']),mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fTipoSaldo'],1);
                                    $oMnySaldoDiario = $oFachada->inicializarMnySaldoDiario($_POST['fCodSaldoDiario'],$_REQUEST['fTipoCaixa'],$_POST['fCodContaCorrente'],($_POST['fCodUnidade']) ? $_POST['fCodUnidade'] : "NULL" ,$voMnyConciliacao[0]->getDataConciliacaoFormatado(),str_replace('.',',',$_POST['fValorFinal']),str_replace('.',',',$_POST['fValorInicial']),mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fTipoSaldo'],$AnexoExtrato,1);
                                    //print_r($oMnySaldoDiario);


                                    //insere saldo, atualiza tabela conciliacao e realiza tramitação...
									if($nCodSaldoDiario = $oFachada->inserirMnySaldoDiario($oMnySaldoDiario)){
                                        if($oFachada->alterarGeralMnyConciliacao($voMnyConciliacao[0]->getDataConciliacao(), $_REQUEST['fCodContaCorrente'])){
                                            foreach($voMnyConciliacao as $oMnyConciliacao){
                                                if($oMnyConciliacao->getCodUnidade() ==0){
                                                    $_POST['fDescricao'] = "Conciliação realizada...";
                                                    $nPara = 12;
                                                }else{
                                                    $_POST['fDescricao'] = "Aguardando NF";
                                                    $nPara = 18;
                                                }
                                                    $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$oMnyConciliacao->getMovCodigo(),$oMnyConciliacao->getMovItem(),11,$nPara,mb_convert_case($_SESSION['Perfil']['Login'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                                    $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                                            }
                                        }
                                    }

									$nValor291 = ($nSaldoAplicacao291 + $nTotal291C) - $nTotal291D;
									$nValor292 = ($nSaldoAplicacao292 + $nTotal292C) - $nTotal292D;

									$voClassificacao = $oFachadaView->recuperarTodosVConciliacaoAplicacaoPorDataConta($voMnyConciliacao[0]->getDataConciliacao(),$_REQUEST['fCodContaCorrente']);

								if($voClassificacao){
								//redireciona para pagina de informar rendimentos
									foreach($voClassificacao as $oClassificacao){
										switch($oClassificacao->getOrdem()){
											case'291.C': //credito na aplicacao = debito em conta
											case'291.D':
												if($nValor291 >= 0){ // colocar irrf, ios e valor_final
													$oMnySaldoDiarioAplicacao291 = $oFachada->inicializarMnySaldoDiarioAplicacao($nCodSaldoDiario ,291,$nValor291,mb_convert_case("C",MB_CASE_UPPER, "UTF-8"), $_POST['fIrrf'],$_POST['fIos'],$_POST['fValorFinal']);
												}else{// colocar irrf, ios e valor_final
													$oMnySaldoDiarioAplicacao291 = $oFachada->inicializarMnySaldoDiarioAplicacao($nCodSaldoDiario ,291,$nValor291,mb_convert_case("D",MB_CASE_UPPER, "UTF-8"), $_POST['fIrrf'],$_POST['fIos'],$_POST['fValorFinal']);
												}
												$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao291);
											break;
											case'292.C': //credito na aplicacao = debito em conta
											case'292.D':
												if($nValor292 >= 0){// colocar irrf, ios e valor_final
													$oMnySaldoDiarioAplicacao292 = $oFachada->inicializarMnySaldoDiarioAplicacao($nCodSaldoDiario ,292,$nValor292,mb_convert_case("C",MB_CASE_UPPER, "UTF-8"), $_POST['fIrrf'],$_POST['fIos'],$_POST['fValorFinal']);
												}else{// colocar irrf, ios e valor_final
													$oMnySaldoDiarioAplicacao292 = $oFachada->inicializarMnySaldoDiarioAplicacao($nCodSaldoDiario ,292,$nValor292,mb_convert_case("D",MB_CASE_UPPER, "UTF-8"), $_POST['fIrrf'],$_POST['fIos'],$_POST['fValorFinal']);
												}
												$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao292);
											break;
										}
								   }
								}
									/*echo "Saldo Diario aplicacao 291 : " . print_r($oMnySaldoDiarioAplicacao291). "<br>";
									echo "Saldo Diario aplicacao 291D : " . print_r($oMnySaldoDiarioAplicacao291D). "<br>";
									echo "Saldo Diario aplicacao 292 : " . print_r($oMnySaldoDiarioAplicacao292). "<br>";
									echo "Saldo Diario aplicacao 292 D: " . print_r($oMnySaldoDiarioAplicacao292D). "<br>";
									die();

									if($oMnySaldoDiarioAplicacao291){
										$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao291);
									}
									if($oMnySaldoDiarioAplicacao292){
										$oFachada->inserirMnySaldoDiarioAplicacao($oMnySaldoDiarioAplicacao292);
									}*/



									$_SESSION['sMsg'] = "Opera&ccedil;&atilde;o realizada com Sucesso!";
                                    $sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'] . "&sBotao=close";
									header("Location: ".$sHeader);
									exit();

								}
						}else{
							$_SESSION['sMsg'] = "Informe uma data!";
                            $sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
							header("Location: ".$sHeader);
							exit();
						}
			}else{
				$_SESSION['sMsg'] = "A concilia&ccedil;&atilde;o do dia anterior n&atilde;o foi realizada.";
				$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
				header("Location: ".$sHeader);
				exit();
			}
		}else{
				$_SESSION['sMsg'] = "Esta concilia&ccedil;&atilde;o j&aacute; foi fechada !";
				$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $dData;
				header("Location: ".$sHeader);
				exit();
		}
	}

    if($_REQUEST['sOP'] == 'AlterarOrdem'){
         $nOcorrencias = count($_REQUEST['fFichaAceite']);

         $j=1;
         $nErro=0;
         for($i=0; $nOcorrencias >$i; $i++){
             $nFA = explode('.',$_REQUEST['fFichaAceite'][$i]);
             $oMnyConciliacao = $oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($nFA[0],$nFA[1]);
             $oMnyConciliacao->setOrdem($j);
             if(!$oFachada->alterarMnyConciliacao($oMnyConciliacao)){
                $nErro++;
             }
             $j++;
         }
         if($nErro ==0){
            $_SESSION['sMsg'] = "Ordem alterada com sucesso!";
         }else{
            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a ordem da concilia&ccedil;&atilde;!";
         }
         $sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];


    }
        if($_REQUEST['sOP'] == 'AlterarOrdem2'){
			$nOrdem = $_REQUEST['ordem'];
			$nId = $_REQUEST['nId'];
			$oMnyConciliacao = $oFachada->recuperarUmMnyConciliacao($nId);
				if($_REQUEST['sentido'] == 'cima'){
					$nOrdem = $nOrdem + 1;
					$oMnyConciliacao->setOrdem($nOrdem);
					if($oFachada->alterarMnyConciliacao($oMnyConciliacao)){
						$oMnyConciliacaoMesmaOrdem = $oFachada->recuperarUmMnyConciliacaoMesmaOrdem($nOrdem, $nId, $oMnyConciliacao->getDataConciliacao(), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);
						if($oMnyConciliacaoMesmaOrdem){
							$nOrdem = $nOrdem -1;
							$oMnyConciliacaoMesmaOrdem->setOrdem($nOrdem);
							if(!($oFachada->alterarMnyConciliacao($oMnyConciliacaoMesmaOrdem))){
								$nOrdem = $nOrdem - 1;
								$oMnyConciliacao->setOrdem($nOrdem);
								$oFachada->alterarMnyConciliacao($oMnyConciliacao);
							}
						}
						$_SESSION['sMsg'] = "Ordem alterada com sucesso!";
 							//$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $_POST['fDataConciliacao'];
					}else{ // fim da primeira alteracao...
						$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a ordem da concilia&ccedil;&atilde;!";
						//$sHeader = "?bErro=100&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
					}
			      $this->carregaTabela($_REQUEST['fDataConciliacao'], $_REQUEST['fCodContaCorrente'], $_REQUEST['fCodUnidade'] );
				}else{// fim do sentido para cima...
					$nOrdem = $nOrdem -1;
					$oMnyConciliacao->setOrdem($nOrdem);
					if($oFachada->alterarMnyConciliacao($oMnyConciliacao)){
						$oMnyConciliacaoMesmaOrdem = $oFachada->recuperarUmMnyConciliacaoMesmaOrdem($nOrdem, $nId, $oMnyConciliacao->getDataConciliacao(), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);
						if($oMnyConciliacaoMesmaOrdem){
							$nOrdem = $nOrdem + 1;
							$oMnyConciliacaoMesmaOrdem->setOrdem($nOrdem);
							if($oFachada->alterarMnyConciliacao($oMnyConciliacaoMesmaOrdem)){
								$_SESSION['sMsg'] = "Ordem alterada com sucesso!";
								//$sHeader = "?bErro=0&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar";
							}else{
								$nOrdem = $nOrdem +1;
								$oMnyConciliacao->setOrdem($nOrdem);
								$oFachada->alterarMnyConciliacao($oMnyConciliacao);
								$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a ordem da concilia&ccedil;&atilde;!";
							    //$sHeader = "?bErro=1&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodContaCorrente'];
							}
			           		$this->carregaTabela($_REQUEST['fDataConciliacao'], $_REQUEST['fCodContaCorrente'], $_REQUEST['fCodUnidade'] );
						}
					    unset($_SESSION['oMnyConciliacao']);
					}
				}

         function preparaRelatorio(){
            require_once('relatorios/Relatorios.php');
            $sOP = $_REQUEST['sOP'];
            $oRelatorio = new Relatorio($sOP);

            $_REQUEST['data_inicial']    = $_REQUEST['dtInicio'];
            $_REQUEST['data_final']      = $_REQUEST['dtFim'];
            $_REQUEST['emp_codigo']      = $_REQUEST['nCodEmpresa'];
            $_REQUEST['unidade']         = $_REQUEST['nCodUnidade'];
            $_REQUEST['mov_tipo']        = $_REQUEST['nTipo'];
            $_REQUEST['pesg_codigo'];
            $_REQUEST['item'];
            $_REQUEST['movimento'];
            $_REQUEST['cod_aporte'];
            $_REQUEST['cod_unidade'];
            $_REQUEST['conta'];
            $_REQUEST['data'];

            $_REQUEST['title'] = $oRelatorio->getTitle();
            $_REQUEST['relatorio'] = $oRelatorio->getRelatorio();

            include_once('relatorios/index.php');
            exit();

         }
    }

//		print_r($sOP);
//		die();

 		switch($sOP){
 			case "Cadastrar":
				if(substr_count($_REQUEST['fDataConciliacao'],'-') > 0){
					$oData = DateTime::createFromFormat('Y-m-d', $_REQUEST['fDataConciliacao']);
					$dData = $oData->format('d/m/Y');
				}else{
					$dData = $_REQUEST['fDataConciliacao'];
				}
				if($_REQUEST['consolidado'] == 0){

					   if(!($nValor = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeTipoCaixaContaCorrente($_REQUEST['fTipoCaixa'], $_REQUEST['fCodContaCorrente']))){
							$nSaldo=0;
					   }else{
							$nSaldo = $nValor->getValorFinal();
					   }
						$item = 1;
						$cont = count($_REQUEST['fMovCodigo']);
						for($i = 0 ; $cont > $i  ;$i++){

						   $nOrdem1 = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMaiorOrdem(($_REQUEST['fDataConciliacao'])? $_REQUEST['fDataConciliacao'] : date('Y-m-d'), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);
						   if($nOrdem1){
								if(is_null($nOrdem1->getOrdem())){
									$nOrdem1 = 1;
								}else{
									$nOrdem1 = $nOrdem1->getOrdem() + 1;
								}
							}else{
								$nOrdem1 = 1;
							}

                            // CONTA
                            $_POST['fTipoCaixa'] = 1;

							$voMnyConciliacao[$i] = $oFachada->inicializarMnyConciliacao($_POST['fCodConciliacao'],$_POST['fMovCodigo'][$i],$_POST['fMovItem'][$i],(($_POST['fCodUnidade']) ? $_POST['fCodUnidade'] : "NULL"),$_POST['fTipoCaixa'],($_POST['fCodCartaoCredito']) ? $_POST['fCodCartaoCredito'] : "NULL",($_POST['fCodContaCorrente']) ? $_POST['fCodContaCorrente'] : "NULL" ,$_POST['fData'],$_POST['fValorVale'],$_POST['fValorResgateIof'][$i],$_POST['fValorResgateIrrf'][$i],mb_convert_case($_POST['fObservacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),($_POST['fConsolidado'])? $_POST['fConsolidado'] : 0 ,$dData, $nOrdem1 , $_POST['fAtivo']);
							//print_r($voMnyConciliacao);
							//die();

							if($nCodConcilicacao[$i] = $oFachada->inserirMnyConciliacao($voMnyConciliacao[$i])){
								/*
                                 * O arquivo saiu da conciliação e foi para o pagamento
                                 *
                                 *
									$nomeArquivo[$i] = $nCodConcilicacao[$i] . "_conci_" . $_FILES['arquivo']['name'][$i];

									$tamanhoArquivo[$i] = $_FILES['arquivo']["size"][$i];
									$nomeTemporario[$i] = $_FILES['arquivo']["tmp_name"][$i];
									$caminho = $_SERVER['DOCUMENT_ROOT'] . "webmoney/arquivos/";
									$vPermissao = array("application/pdf");
									$tamanhoMaximo = 100000000;

									$sNomeArquivo = strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($sNomeArquivo)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ "),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
									$sNomeArquivo = str_replace(" ", "-",$sNomeArquivo);

									if((move_uploaded_file($nomeTemporario[$i], ($caminho . $nomeArquivo[$i])))){
										$sNomeArquivoBD = "arquivos/".$nomeArquivo[$i];
										$_POST['CodContratoDocTipo'] = 11;
										$nCodContrato  = NULL;

										$oMnyContratoArquivo = $oFachada->inicializarMnyContratoArquivo($_POST['fCodContratoArquivo'],$nCodContrato,$nCodConcilicacao[$i], $voMnyConciliacao[$i]->getMovCodigo(),$voMnyConciliacao[$i]->getMovItem(),$_POST['CodContratoDocTipo'],$sNomeArquivoBD,$_POST['fRealizadoPor'],1);

										$oFachada->inserirMnyContratoArquivo($oMnyContratoArquivo);
									}else{
										die('error...');
									}
							     */
							} else {
								//$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Concilia&ccedil;&atilde;o!";
								$voMnyConciliacaoErro[] = $voMnyConciliacao[$i];
							}

						$item++;
						}// fim foreach...

					$_SESSION['voMnyConciliacao'] = $voMnyConciliacao;
					$_SESSION['sMsg'] = "Concilia&ccedil;&otilde;es inseridas com sucesso!";
				}else{
					$_SESSION['sMsg'] = "Esta concilia&ccedil;&atilde;o j&aacute; est&aacute; fechada!";
				}
				$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente']. "&dData=" . $dData;
			break;
 			case "Alterar":
 				if($oFachada->alterarMnyConciliacao($oMnyConciliacao)){
 					unset($_SESSION['oMnyConciliacao']);
 					$_SESSION['sMsg'] = "mny_conciliacao alterado com sucesso!";
 					//$sHeader = "?bErro=0&action=MnyConciliacaoCaixinha.preparaLista";

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o mny_conciliacao!";
 					$sHeader = "?bErro=1&action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=".$sOP."&nIdMnyConciliacao=".$_POST['fCodConciliacao']."&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodContaCorrente'];
 				}
 			break;
 			case "Excluir":

 				$bResultado = true;
				$nCodArquivo = $_REQUEST['fCodArquivo'];
				//exlcui o arquivo referente ao item da conciliacao...
				//if($oFachada->excluirMnyContratoArquivo($nCodArquivo)){
					$voRegistros = explode("____",$_REQUEST['fIdMnyConciliacao']);
                    foreach($voRegistros as $oRegistros){
                        $sCampoChave = str_replace("||", ",", $oRegistros);
                    eval("\$bResultado &= \$oFachada->excluirMnyConciliacao($sCampoChave);\n");
                    }
				//}

				if($bResultado){
		            if($_REQUEST['dData']){
						$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['dData']);
						$dData = $oData->format('Y-m-d');
					}

				/*
					$voMnyConciliacao = $oFachada->recuperarTodosMnyConciliacaoPorDataUnidadeConta($dData,$_REQUEST['nCodUnidade'], $_REQUEST['nCodContaCorrente']);
					$i =1;
 					foreach($voMnyConciliacao as $oMnyConciliacao){
						 $oMnyConciliacao->setOrdem($i);
						 $oFachada->alterarMnyConciliacao($oMnyConciliacao);
						$i++;
					}
				*/
					$_SESSION['sMsg'] = "Item exclu&iacute;do com sucesso!";
				}else{
 					$_SESSION['sMsg'] = "N&atilde; foi possivel exluir o item selecionado!";
				}
           		$this->carregaTabela($_REQUEST['dData'], $_REQUEST['nCodContaCorrente'], $_REQUEST['nCodUnidade'] );
 			break;
			case "AbrirConciliacao":
				$oData3 = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
				$dData3 = $oData3->format('Y-m-d');
                $dDataPermitida = $oFachada->abrirMnyConciliacao($dData3, $_REQUEST['fCodContaCorrente']);

				if($dDataPermitida == '0000-00-00'){
					$_SESSION['sMsg'] = "A concilia&ccedil;&atilde;o do dia ".$_REQUEST['fDataConciliacao']." foi Aberta com sucesso!";
					$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar";
				}else{
					$_SESSION['sMsg'] = "Voc&ecirc; deve abrir primeiro a &uacute;ltima a concilia&ccedil;&atilde;o permitida <br>[<strong>".$dDataPermitida."</strong>]";
					$sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinhaAbreConciliacao&dData=". $_REQUEST['fDataConciliacao']."&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'];
				}
			break;
 		}

 		header("Location: ".$sHeader);

 	}

    function gravaLinha(){
			$oFachadaFinanceiroBD = new FachadaFinanceiroBD();
			$nMovItem = explode(".", $_REQUEST['fMovimentoItem']);
			$_REQUEST['idCampo'] = $_GET['idCampo'];
			if($_REQUEST['fDataConciliacao']){
				$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
				$dDataConciliacaoBanco = $oData->format('Y-m-d');
			}
			$nMovCodigo = $nMovItem[0];
			$nMovItem = $nMovItem[1];
            //continuar aqui...
           // $oMnyMovimentoItemLiberado = $oFachadaFinanceiroBD->recuperarUmMnyAporteItemLiberadoPorMovimentoPorItem($nMovCodigo,$nMovItem);
            $oMnyMovimentoItemLiberado = $oFachadaFinanceiroBD->recuperarUmMnyAporteItemLiberadoPorMovimentoPorItem($nMovCodigo,$nMovItem);


        /*
            //caixinha com unidade...
            $voMnyContaCorrenteCaixinha = $oFachadaFinanceiroBD->recuperarTodosMnyContaCorrenteCaixinha();
            foreach($voMnyContaCorrenteCaixinha as $oValue){
                $aCcrCodigo[] = $oValue->getCcrCodigo();
            }
            //Verifica se a conta é do caixinha...
            if(in_array($_REQUEST['fCodContaCorrente'],$aCcrCodigo)){
                $oMnyContaCorrenteCaixinha = $oFachadaFinanceiroBD->recuperarUmMnyContaCorrenteCaixinhaPorMovimento($nMovCodigo,$_REQUEST['fCodUnidade']);
            }
            */

            //caixinha sem unidade...
            $oMnyMovimentoCaixinha = $oFachadaFinanceiroBD->recuperarUmMnyMovimentoPorCaixinha($nMovCodigo,$nMovItem);
            if((($oMnyMovimentoItemLiberado) && ($oMnyMovimentoItemLiberado->getMovItem())) || $oMnyMovimentoCaixinha){ // aponta para o anexo mny_arquivo...
                $_REQUEST['nPendente'] = false;
                    $_REQUEST['oMnyVMovimentoConciliacao'] = $oFachadaFinanceiroBD->recuperarTodosMnyConciliacaoDadosGerais($nMovCodigo, $nMovItem , $_REQUEST['fCodContaCorrente'], $dDataConciliacaoBanco);

                    //verificações
                    if($_REQUEST['rep']==1)
                       $_SESSION['sMsg'] ="Este item j&aacute; foi inserido.";

                    if($_REQUEST['oMnyVMovimentoConciliacao']){
                        if($_REQUEST['oMnyVMovimentoConciliacao']->getCodConciliacao() != 6){
                            switch($_REQUEST['oMnyVMovimentoConciliacao']->getCodConciliacao()){
                                case 1:
                                   $_SESSION['sMsg'] ="Esta F.A pertence a ".$_REQUEST['oMnyVMovimentoConciliacao']->getConta().", diferente da conta selecionada na concilia&ccedil;&atilde;o.";
                                break;
                                case 2:
                                   $_SESSION['sMsg'] ='Este item &eacute; do tipo aceite Previs&atilde;o.';
                                break;
                                case 3:
                                   $_SESSION['sMsg'] ='Este item j&aacute; foi conciliado na CONTA '.$_REQUEST['oMnyVMovimentoConciliacao']->getContaCodigo().' da UNIDADE '.$_REQUEST['oMnyVMovimentoConciliacao']->getCodUnidade().' no DIA '.$_REQUEST['oMnyVMovimentoConciliacao']->getDataConciliacaoFormatado().'.' ;
                                break;
                                case 4:
                                case 5:
                                   $_SESSION['sMsg'] ='Esta concilia&ccedil;&atilde;o j&atilde; foi fechada.';
                                break;
                            }
                        }
                    }

                    $nTipoCaixa = 1;
                    if($_REQUEST['oMnyVMovimentoConciliacao']){
                        $_REQUEST['oSaldoConta'] = $oFachadaFinanceiroBD->recuperarUmMnySaldoDiarioPorUnidadeContaCorrente($dDataConciliacaoBanco, $_REQUEST['fCodUnidade'] ,$_REQUEST['oMnyVMovimentoConciliacao']->getContaCodigoConc());
                        if($_REQUEST['oMnyVMovimentoConciliacao']->getOrdem() =="D"){
                            $_REQUEST['resgate'] = 1;
                        }else{
                            $_REQUEST['resgate'] = 0;
                        }
                    }
            }else{
                  $_REQUEST['nPendente'] = true;
                if(($oMnyContaCorrenteCaixinha) && $oMnyContaCorrenteCaixinha->getCCrCodigo() != $_REQUEST['fCodContaCorrente']){
                  $_SESSION['sMsg']  ='A unidade lançada no movimento é diferente da conta selecionada na conciliação';
                }else{
                  $_SESSION['sMsg']  ='Este item  não está liberado para PAGAMENTO';
                }
            }
			include_once("view/financeiro/mny_conciliacao/campo_ajax.php");
			exit();
	}

	function carregaTipo(){
		$oFachada = new FachadaFinanceiroBD();
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodTipoCaixa = $_REQUEST['fCenCodigo'];
			$nCodUnidade = $_REQUEST['fUniCodigo'];

			//usado na pagina para abrir a conciliacao
			$_REQUEST['nTipoOperacao'] = $_REQUEST['nTipoOperacao'];

			(isset($_REQUEST['fUniCodigo'])) ? $nCodTipoCaixa = 1 : "" ;

			$_REQUEST['fCenCodigo2'] = $nCodTipoCaixa;
			$_REQUEST['voMnyCartao'] = $oFachada->recuperarTodosMnyCartao();
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
			if($_REQUEST['fDataConciliacao']){
				$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
				$dDataConciliacao = $oData->format('Y-m-d');
			}
			$_REQUEST['voMnyContaCorrenteUnidade'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);

			$_REQUEST['nCodContaCorrente'] = $_REQUEST['nCodContaCorrente'];

			$_REQUEST['dData'] = $_REQUEST['fDataConciliacao'];

			include_once("view/financeiro/mny_conciliacao/tipo_caixa_ajax.php");

			exit();
	}

	function carregaContaRelatorio(){
		$FachadaFinanceiroBD = new FachadaFinanceiroBD();
		$nCodUnidade = $_REQUEST['fUniCodigo'];
		$_REQUEST['voMnyContaCorrenteUnidade'] = $FachadaFinanceiroBD->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);
		include_once("view/financeiro/mny_conciliacao/conta_relatorio_ajax.php");
		exit();
	}

	/*
	function carregaContas(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodUnidade = $_REQUEST['fCodUnidade'];

			$_REQUEST['fCenCodigo2'] = $nCodTipoCaixa;
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);
			// parei aqui
			include_once("view/financeiro/mny_conciliacao/tipo_caixa_ajax.php");
			exit();
	}
	*/

    function carregaRendimento(){
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$_REQUEST['oSaldoDiarioAplicacao'] = $oFachadaFinanceiro->recuperarUmMnySaldoDiarioAplicacaoPorContaUnidade($_REQUEST['nCodContaCorrente'], $_REQUEST['nCodUnidade']);
		include_once('view/financeiro/mny_conciliacao/rendimento_ajax.php');
		exit();
	}
	//carrega apenas os itens correspondentes ao movimento selecionado...
	function carregaItem(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nMovCodigo = $_REQUEST['fMovCodigo'];

			$_REQUEST['voMnyMovimentoItem'] = $oFachada->recuperarTodosMnyMovimentoItemPorMovimento($nMovCodigo);
			include_once("view/financeiro/mny_conciliacao/item_ajax.php");
			exit();
	}

	function carregaTabela($dData="",$nConta="",$nCodUnidade=""){

		$oFachadaView = new FachadaViewBD();
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
				$dDataBanco = ($dData) ? $dData : $_REQUEST['fDataConciliacao'];
				if($dDataBanco ){
					$oData = DateTime::createFromFormat('d/m/Y', $dDataBanco );
					$dDataConciliacaoBanco = $oData->format('Y-m-d');
				}

				$_REQUEST['fCodUnidade'] = ($nCodUnidade) ? $nCodUnidade : $_REQUEST['fCodUnidade'] ;
				$_REQUEST['fCodContaCorrente'] = ($nConta) ? $nConta : $_REQUEST['fCodContaCorrente'];

                $oMnyContaCorrente = $oFachada->recuperarUmMnyContaCorrente($_REQUEST['fCodContaCorrente']);

                $_REQUEST['nMaiorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMaiorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);
                $_REQUEST['nMenorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMenorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);

                    $_REQUEST['voMnyVMovimentoConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoCaixinhaPorDataUnidadeConta(($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);



				//$oFachada->recuperarUmMnySaldoDiarioPorUnidadeContaData($_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente'], ($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'));
				$oFachada->recuperarUmMnySaldoDiarioAnteriorPorContaData($_REQUEST['fCodContaCorrente'], ($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'));
				if($_REQUEST['voMnyVMovimentoConciliacao']){
					if($_REQUEST['voMnyVMovimentoConciliacao'][0]->getConsolidado() == 1){
						$_REQUEST['consolidado'] = 1;
					}else{
						$_REQUEST['consolidado'] = 0;
					}
				}else{
						//dia nao util...
						$_REQUEST['consolidado'] = 0;
				}

				//if(!$_REQUEST['voMnyVMovimentoConciliacao']){
					$oSaldoConta = $oFachada->recuperarUmMnySaldoDiarioPorTipoConta($_REQUEST['fCodContaCorrente'], $dDataConciliacaoBanco);
					//print_r($oSaldoConta);
					//die();
					if($oSaldoConta){

						if($dDataConciliacaoBanco == $oSaldoConta->getData()){
							switch($oSaldoConta->getTipoSaldo()){
								case 1:
								  $_REQUEST['nTipoConciliacao'] = 1;
								break;
								case 2:
								  $_REQUEST['nTipoConciliacao'] = 2;
								break;
								case 3:
								  $_REQUEST['nTipoConciliacao'] = 3;
								break;
							}
						}else{
								  $_REQUEST['nTipoConciliacao'] = 0;

                                   $nAno = explode('/',$_REQUEST['fDataConciliacao']);
								  // $_REQUEST['dDataUltimaConciliacao'] = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($_REQUEST['fCodContaCorrente'])->getDataFormatado();
                                  $_REQUEST['dDataUltimaConciliacao'] = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrentePorAno($_REQUEST['fCodContaCorrente'],$nAno[2])->getDataFormatado();
						}


					$_REQUEST['oMnySaldoDiario'] = $oSaldoConta;
				}
				//print_r($_REQUEST['nTipoConcialicao']);
				//die();
				$nTipoCaixa = 1;
			  	$_REQUEST['oSaldoConta']  = $oFachada->recuperarUmMnySaldoDiarioPorContaCorrente($dDataConciliacaoBanco, $_REQUEST['fCodContaCorrente']);


                //include_once("view/financeiro/mny_conciliacao/tabela_ajax.php");
				if($oMnyContaCorrente->getCcrConta() == 999){
                    $_REQUEST['nCaixinha']=1;
                    include_once("view/financeiro/mny_conciliacao/lista_ajax_caixinha.php");
                }else{
                    $_REQUEST['nCaixinha']=0;
                    include_once("view/financeiro/mny_conciliacao/lista_ajax.php");
                }
				exit();
	}

    function preparaExtratoBancario(){
         $oFachadaFinanceiro = new FachadaFinanceiroBD();
         $_REQUEST['oExtratoBancario'] = $oFachadaFinanceiro->recuperarUmMnySaldoDiarioPorTipoConta($_REQUEST['fCodContaCorrente'], $_REQUEST['fData'])->getExtratoBancario();
         include_once('view/financeiro/mny_conciliacao/anexo.php');
         exit();
    }

	function carregaResumo(){
		$oFachadaView = new FachadaViewBD();
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
				if($_REQUEST['fDataConciliacao']){
					$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
					$dDataConciliacaoBanco = $oData->format('Y-m-d');
				}
				$_REQUEST['voResumo'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalPorData(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'));

			include_once("view/financeiro/mny_conciliacao/resumo_ajax.php");
			exit();
	}

	public function preparaReconciliacao(){
		$oFachadaView = new FachadaViewBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$sOP = $_REQUEST['sOP'];
		$nUniCodigo = $_REQUEST['unidade'];
		$_REQUEST['oUnidade'] = $oFachadaFinanceiro->recuperarUmMnyPlanoContas($nUniCodigo);
		$nCcrConta = $_REQUEST['conta'];
		$_REQUEST['oMnyContaCorrente'] = $oFachadaFinanceiro->recuperarUmMnyContaCorrente($nCcrConta);
		$_REQUEST['voVReconciliacao'] = $oFachadaFinanceiro->recuperarTodosReconciliacaoPorUnidadePeriodoProcedure($nCcrConta,$_GET['dInicio'], $_GET['dFim']);
		$_REQUEST['oTotais'] = $oFachadaView->recuperarTodosVReconciliacaoPorUnidadePeriodoTotais($nUniCodigo,$nCcrConta,$_GET['dInicio'], $_GET['dFim']);
        $vDataInicio = explode('-',$_GET['dInicio']);
		$_REQUEST['dtInicio'] = $vDataInicio[2] . "/" . $vDataInicio[1] . "/" . $vDataInicio[0];
		$vDataFim =  explode('-',$_GET['dFim']);
		$_REQUEST['dtFim'] = $vDataFim[2] . "/" . $vDataFim[1] . "/" . $vDataFim[0];

		include_once("view/financeiro/mny_conciliacao/reconciliacao_excel.php");
		exit();
 	}

	public function carregaRelatorio(){
		$oFachadaView = new FachadaViewBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo(8);
		$_REQUEST['nTipo'] = $_REQUEST['nTipo'];
		$_REQUEST['nCodEmpresa'] = $_SESSION['oEmpresa']->getEmpCodigo();
		if($_REQUEST['nTipo'] == 3){
			$voCredor = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			$_REQUEST['voCredor'] = $voCredor;
		}
		include_once('view/financeiro/mny_conciliacao/relatorio_ajax.php');
		exit();
	}

	function alteraOrdem(){
	    $_SESSION['oMnyMovConciliacao'] ;
		//cima...
		$i = $_REQUEST['nIndice'];
		$j = $_REQUEST['nProximoIndice'];

		$oAux = $_SESSION['oMnyMovConciliacao'][$i]->getCodConciliacao;

		$_SESSION['oMnyMovConciliacao'][$i]->setCodConciliacao($_SESSION['oMnyMovConciliacao'][$j]->getCodConciliacao());
		$_SESSION['oMnyMovConciliacao'][$j]->setCodConciliacao($oAux);

		/*for($cont1=0;$cont1<(count($vet) - 1);$cont1++){
			for($cont2=0;$cont2 <(count($vet) - 2);$cont2++){
				if($vet[$cont2+1]<=$vet[$cont2]){
					$aux=$vetor[$cont2];
					$vet[$cont2]=$vet[$cont2+1];
					$vet[$cont2+1] = $aux;
				}
			}
			$lista[$cont1]=$aux;
	    }	*/

		$MyArray = $_SESSION['oMnyMovConciliacao'];
		//$_SESSION['oMnyVMovimentoConciliacao']
		//print_r( usort($MyArray, function($a, $b) {return $a['order'] - $b['order'];}));

		//die();

		/*
		usort(
				$_SESSION['oMnyMovConciliacao'] , function( $a, $b ) {
					 if( $a -> distancia == $b -> distancia ) return 0;
					 return ( ( $a  -> distancia < $b  -> distancia ) ? -1 : 1 );
				 }
			);*/

		include_once("view/financeiro/mny_conciliacao/tabela_ajax_sessao.php");
		exit();
     }

    public function preparaAlteraOrdemEmergencia(){
        $oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();

        $_REQUEST['voMnyCartao'] = $oFachada->recuperarTodosMnyCartao();
		$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
		$_REQUEST['voMnyTipoCaixa'] = $oFachada->recuperarTodosMnyTipoCaixa();

		$sGrupo = '8';//Tipo de Unidade
		$_REQUEST['voMnyTipoUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$_REQUEST['voConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoPorData(date('Y-m-d'), $_REQUEST['fCodUnidade']);


		if(substr_count($_REQUEST['dData'],'-') > 0){
            $oData = DateTime::createFromFormat('Y-m-d',$_REQUEST['dData']);
            $dData = $oData->format('d/m/Y');
            $_REQUEST['dData'] = $dData ;
		}

        include_once("view/financeiro/mny_conciliacao/insere_altera_emergencia.php");


     }

    public function alteraOrdemConciliacaoEmergencia(){

         $oFachadaFinanceiro = new FachadaFinanceiroBD();
         for($i=0;count($_POST['fFichaAceite']) > $i; $i++){
             $nMovimento = explode('.',$_POST['fFichaAceite'][$i]);
             $oMnyConciliacao = $oFachadaFinanceiro->recuperarUmMnyConciliacaoPorMovimentoItem($nMovimento[0],$nMovimento[1]);
              $oMnyConciliacao->setOrdem($_POST['ordem'][$i]);
             $oFachadaFinanceiro->alterarMnyConciliacao($oMnyConciliacao);
         }

         $_SESSION['sMsg'] = "Ordem Alterada com Sucesso ";
		 $sHeader = "?action=MnyConciliacaoCaixinha.preparaAlteraOrdemEmergencia&sOP=Cadastrar&&nTipoCaixa=" . $_REQUEST['fTipoCaixa'] . "&nCodUnidade=" . $_REQUEST['fCodUnidade']. "&nCodContaCorrente=" . $_REQUEST['fCodContaCorrente'] . "&dData=" . $_REQUEST['fDataConciliacao'];
         header("Location: ".$sHeader);
         exit();
     }

    function carregaTabelaEmergencia($dData="",$nConta="",$nCodUnidade=""){

		$oFachadaView = new FachadaViewBD();
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
				$dDataBanco = ($dData) ? $dData : $_REQUEST['fDataConciliacao'];
				if($dDataBanco ){
					$oData = DateTime::createFromFormat('d/m/Y', $dDataBanco );
					$dDataConciliacaoBanco = $oData->format('Y-m-d');
				}

				$_REQUEST['fCodUnidade'] = ($nCodUnidade) ? $nCodUnidade : $_REQUEST['fCodUnidade'] ;
				$_REQUEST['fCodContaCorrente'] = ($nConta) ? $nConta : $_REQUEST['fCodContaCorrente'];

				$_REQUEST['nMaiorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMaiorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);
				$_REQUEST['nMenorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMenorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);

				$_REQUEST['voMnyVMovimentoConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoPorDataUnidadeConta(($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);

				$oFachada->recuperarUmMnySaldoDiarioPorUnidadeContaData($_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente'], ($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'));

               if($_REQUEST['voMnyVMovimentoConciliacao']){
					if($_REQUEST['voMnyVMovimentoConciliacao'][0]->getConsolidado() == 1){
						$_REQUEST['consolidado'] = 1;
					}else{
						$_REQUEST['consolidado'] = 0;
					}
				}else{
						//dia nao util...
						$_REQUEST['consolidado'] = 0;
				}

				//if(!$_REQUEST['voMnyVMovimentoConciliacao']){
					$oSaldoConta = $oFachada->recuperarUmMnySaldoDiarioPorTipoConta($_REQUEST['fCodContaCorrente'], $_REQUEST['fCodUnidade'], $dDataConciliacaoBanco);
					//print_r($oSaldoConta);
					//die();
					if($oSaldoConta){

						if($dDataConciliacaoBanco == $oSaldoConta->getData()){
							switch($oSaldoConta->getTipoSaldo()){
								case 1:
								  $_REQUEST['nTipoConciliacao'] = 1;
								break;
								case 2:
								  $_REQUEST['nTipoConciliacao'] = 2;
								break;
								case 3:
								  $_REQUEST['nTipoConciliacao'] = 3;
								break;
							}
						}else{
								  $_REQUEST['nTipoConciliacao'] = 0;
								  $_REQUEST['dDataUltimaConciliacao'] = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente'])->getDataFormatado();
						}


					$_REQUEST['oMnySaldoDiario'] = $oSaldoConta;
				}
				//print_r($_REQUEST['nTipoConcialicao']);
				//die();
				$nTipoCaixa = 1;
			  	$_REQUEST['oSaldoConta']  = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeContaCorrente($dDataConciliacaoBanco, $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);
				//include_once("view/financeiro/mny_conciliacao/tabela_ajax.php");
				include_once("view/financeiro/mny_conciliacao/lista_ajax_emergencia.php");
				exit();
	}

    public function processaFormularioReconciliacao(){

        $oFachada = new FachadaFinanceiroBD();
        $nErro=0;
        $vCampoChave = array();
        $arquivos = $_FILES["arquivo"];
        $aContaData = explode('||',$_POST['fSaldo']);
        $vFA = explode("||",$_REQUEST['fFA']);
        $_POST['fMovCodigo'] = $vFA[0];
        $_POST['fMovItem'] = $vFA[1];

        $oMnySaldoDiario = $oFachada->recuperarUmMnySaldoDiarioPorTipoConta($aContaData[0],$aContaData[1]);

        $vIdArquivo = array();

        if($_REQUEST['fFichaAceite']){

             $nOcorrencias = count($_REQUEST['fFichaAceite']);
             $j=1;
             $nErro=0;
             $nAlterado=0;
             for($i=0; $nOcorrencias >$i; $i++){
                 $nFA = explode('.',$_REQUEST['fFichaAceite'][$i]);
                 $oMnyConciliacao = $oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($nFA[0],$nFA[1]);
                 ($oMnyConciliacao->getOrdem() != $j)? $nAlterado = 1 : $nAlterado;
                 $oMnyConciliacao->setOrdem($j);
                 if(!$oFachada->alterarMnyConciliacao($oMnyConciliacao)){
                    $nErro++;
                 }
                 $j++;
             }
             if($nErro ==0 && $nAlterado == 1){
                $_SESSION['sMsg'] .= "Ordem alterada com sucesso!\n";
             }else{
                $_SESSION['sMsg'] .= "A ordem  da conciliação não foi alterada!\n";
             }
        }

        if($arquivos['name'] != ""){

                $nomeTemporario = $arquivos['tmp_name'];

                // USADO PARA ARMAZENAR O ARQUIVO NO BANCO DE DADOS...
                if($_FILES["arquivo"]["tmp_name"]){
                   $Arquivo = $_FILES["arquivo"]["tmp_name"];
                   $Tipo = $_FILES["arquivo"]["type"];

                    move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");

                    $blobArquivo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                    $blobArquivo = addslashes(fread($blobArquivo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                    $oMnySaldoDiario->setExtratoBancario($blobArquivo);
                    unlink("c:\\wamp\\tmp\\arquivo.pdf");

                    $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                    $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];

                    if($oFachada->alterarMnySaldoDiario($oMnySaldoDiario)){
                       $_SESSION['sMsg'] .= "Comprovante atualizado com sucesso!\n";
                    }
                }
            }else{
                $_SESSION['sMsg'] .= "O comprovante não foi atualizado!\n";
            }

            $_POST['fDescricao'] = $_SESSION['sMsg'];

            $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],11,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);

            if($oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo)){
                $sHeader = "?action=MnyMovimentoItem.preparaFormularioCaixinha&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=".$_REQUEST['fFA'];
            }else{
                $sHeader = "?action=MnyConciliacaoCaixinha.preparaFormularioCaixinhaReConciliacao&sOP=AlterarReconciliacao&nIdMnyMovimentoItem=".$_REQUEST['fFA'];
            }

            header("Location: ".$sHeader);
            exit();

    }

 }

 ?>
