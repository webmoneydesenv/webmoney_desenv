<?php
 class MnyPessoaGeralCTR implements IControle{
 
 	public function MnyPessoaGeralCTR(){
 	
 	}
 
 	public function preparaLista(){
		$oFachada = new FachadaViewBD();
 		$voMnyPessoaGeral = $oFachada->recuperarTodosVPessoaFormatada();

 		$_REQUEST['voMnyPessoaGeral'] = $voMnyPessoaGeral;
 		include_once("view/financeiro/mny_pessoa_geral/index.php");
 		exit();
 		
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 
 		//$oMnyPessoaGeral = false;

 		if($_REQUEST['sOP'] == "Alterar"  || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['fCodMatriz']){
 		
		
		//if($_REQUEST['fCodMatriz']){
		//	$nIdMnyPessoaGeral = $_REQUEST['fCodMatriz'];
		//}else
		if($_POST['fIdMnyPessoaGeral'][0]){
			$nIdMnyPessoaGeral = $_POST['fIdMnyPessoaGeral'][0];
		}else{
			$nIdMnyPessoaGeral =  $_GET['nIdMnyPessoaGeral'];
		}

/*		if($nIdMnyPessoaGeral){

//			$vIdMnyPessoaGeral = explode("||",$nIdMnyPessoaGeral);
			$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($nIdMnyPessoaGeral);
print_r($nIdMnyPessoaGeral);
die();*/
		if($nIdMnyPessoaGeral){

			$vIdMnyPessoaGeral = explode("||",$nIdMnyPessoaGeral);
			$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($vIdMnyPessoaGeral[0]);

			switch($oMnyPessoaGeral->getTipCodigo()){
					case 254: //EMPREGADO
						$oMnyPessoaColaborador = $oFachada->recuperarUmMnyPessoaColaborador($vIdMnyPessoaGeral[0]);
						$_REQUEST['oMnyPessoaColaborador'] = $oMnyPessoaColaborador;
						$_REQUEST['oMnyPessoaColaborador'] = ($_SESSION['oMnyPessoaColaborador']) ? $_SESSION['oMnyPessoaColaborador'] : $oMnyPessoaColaborador;
						$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
						$_REQUEST['voMnyFuncao'] = $oFachada->recuperarTodosMnyFuncao();
						
						$sGrupo = 8;// Unidade de Negócio
						$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
						$sGrupo = 10; // Setor
						$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
						
						
						$sPagina = "mny_pessoa_colaborador/insere_altera.php";
					break;
					case 256: // PRESTADOR DE SERVIÇOS -PF
						$oMnyPessoaFisica = $oFachada->recuperarUmMnyPessoaFisica($nIdMnyPessoaGeral);
						$_REQUEST['oMnyPessoaFisica'] = $oMnyPessoaFisica;
				 		$_REQUEST['oMnyPessoaFisica'] = ($_SESSION['oMnyPessoaFisica']) ? $_SESSION['oMnyPessoaFisica'] : $oMnyPessoaFisica;
						$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoaPorPessoa($nIdMnyPessoaGeral);
						
						$sGrupo=12;// Tipo de pessoa
						$_REQUEST['voMnyTipoPessoa'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
						
						$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
						$sGrupo = 8;// Unidade de Negócio
						$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
						$sGrupo = 10; // Setor
						$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
						
						
						$sPagina = "mny_pessoa_fisica/insere_altera.php";
						
						
						
					
					break;
					case 255: // FORNECEDOR
					case 257: // PSJ - PRESTADOR DE SERVIÇO P. JURÍDICA
					case 258: // BC - BANCO
						
						if($oMnyPessoaGeral->getPesgCodigo() && $_REQUEST['sOP'] == "Alterar"){
							$oMnyPessoaJuridica = $oFachada->recuperarUmMnyPessoaJuridica($nIdMnyPessoaGeral);
						}
						$_REQUEST['oMnyPessoaJuridica'] = $oMnyPessoaJuridica;
						$_REQUEST['oMnyPessoaJuridica'] = ($_SESSION['oMnyPessoaJuridica']) ? $_SESSION['oMnyPessoaJuridica'] : $oMnyPessoaJuridica;
						
						//$_REQUEST['voMnyTipoPessoa'] = $oFachada->recuperarTodosMnyTipoPessoaJuridica();
						
						$sGrupo=12;// Tipo de pessoa
						$_REQUEST['voMnyTipoPessoa'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

						$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
						$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoaPorPessoa($nIdMnyPessoaGeral);
						$_REQUEST['voFilial'] = $oFachada->recuperarTodosMnyPessoaJuridicaPorMatriz($nIdMnyPessoaGeral);
						$nCodMatriz = $_REQUEST['fCodMatriz'];
						if($nCodMatriz) {
							$_REQUEST['nFilial'] = $_GET['nFilial'];
							$_REQUEST['fCodMatriz'] = $nCodMatriz;
							if ($_REQUEST['sOP'] == 'Cadastrar')
								unset($oMnyPessoaGeral);
						}
						
						$sPagina = "mny_pessoa_juridica/insere_altera.php";
						
					break;
				
				}
			}
 		}
 		
 		$_REQUEST['oMnyPessoaGeral'] = ($_SESSION['oMnyPessoaGeral']) ? $_SESSION['oMnyPessoaGeral'] : $oMnyPessoaGeral;
 	/*	print_r($_REQUEST['oMnyPessoaGeral']);
		unset($_SESSION['oMnyPessoaGeral']);
		print_r($_REQUEST['oMnyPessoaGeral']);
		die();*/

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_pessoa_geral/detalhe.php");
 		else{
 			
//			$_REQUEST['voMnyTipoPessoa'] = $oFachada->recuperarTodosMnyTipoPessoa();
			$sGrupo=12;// Tipo de pessoa
			$_REQUEST['voMnyTipoPessoa'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
			
			//$sGrupo = 14;// banco
			$_REQUEST['voSysBanco'] = $oFachadaSys->recuperarTodosSysBanco();
			
			$sGrupo = 13;// tipo de conta
			$_REQUEST['voSysContaTipo'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
 			if($sPagina)
				include_once("view/financeiro/". $sPagina);
			else{
				header('Location:?action=MnyPessoaGeral.preparaLista');
				exit();
			}
				
 		}
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyPessoaGeral = $oFachada->inicializarMnyPessoaGeral($_POST['fPesgCodigo'],$_POST['fTipCodigo'],$_POST['fCodStatus'],mb_convert_case($_POST['fPesgEndLogra'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEndBairro'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesgEndCep'],$_POST['fCidade'],$_POST['fEstado'],mb_convert_case($_POST['fPesgEmail'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesFones'],$_POST['fPesgInc'],$_POST['fPesgAlt'],$_POST['fBcoCodigo'],$_POST['fTipoconCod'],$_POST['fPesgBcoAgencia'],$_POST['fPesgBcoConta'],$_POST['fAtivo']);
 																																																																																										

 			if($sOP == 'Cadastrar'){
				$oMnyPessoaGeral->setPesgInc(date('d/m/Y H:i:s') . $_SESSION['oUsuarioImoney']->getLogin());
				
			}elseif($sOP == 'Alterar'){
				$oMnyPessoaGeral->setPesgAlt(date('d/m/Y H:i:s') . $_SESSION['oUsuarioImoney']->getLogin());
			}
			
			$_SESSION['oMnyPessoaGeral'] = $oMnyPessoaGeral;
			
			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("Tipo", $oMnyPessoaGeral->getTipCodigo(), "number", "y");
			$oValidate->add_number_field("Status", $oMnyPessoaGeral->getCodStatus(), "number", "y");
			$oValidate->add_text_field("endereço", $oMnyPessoaGeral->getPesgEndLogra(), "text", "y");
			$oValidate->add_text_field("Bairro", $oMnyPessoaGeral->getPesgEndBairro(), "text", "y");
			$oValidate->add_text_field("Cep", $oMnyPessoaGeral->getPesgEndCep(), "text", "y");
			$oValidate->add_text_field("Cidade", $oMnyPessoaGeral->getCidade(), "text", "y");
			$oValidate->add_text_field("Estado", $oMnyPessoaGeral->getEstado(), "text", "y");
			//$oValidate->add_text_field("Email", $oMnyPessoaGeral->getPesgEmail(), "text", "y");
			$oValidate->add_text_field("Fones", $oMnyPessoaGeral->getPesFones(), "text", "y");
			$oValidate->add_text_field("Incluido Por", $oMnyPessoaGeral->getPesgInc(), "text", "y");
			if($sOP == 'Alterar') 
				$oValidate->add_text_field("Alterado por", $oMnyPessoaGeral->getPesgAlt(), "text", "y");
			//$oValidate->add_number_field("Banco", $oMnyPessoaGeral->getBcoCodigo(), "number", "y");
			//$oValidate->add_number_field("Tipo de Conta", $oMnyPessoaGeral->getTipoconCod(), "number", "y");
			//$oValidate->add_text_field("Agencia", $oMnyPessoaGeral->getPesgBcoAgencia(), "text", "y");
			//$oValidate->add_text_field("Conta", $oMnyPessoaGeral->getPesgBcoConta(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyPessoaGeral->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPessoaGeral.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
			
	switch($_POST['fTipCodigo']){
				case 254: //EMPREGADO
					$sTabela = "MnyPessoaColaborador";
					/*$sData = $_POST['fAdmissao'];
					//$dDataFormatada = DateTime::createFromFormat('d/m/Y', $sData);
					//$_POST['fAdmissao'] = $dDataFormatada->format('Y-m-d');
						
					if($_POST['fDemissao']){
						$sData = $_POST['fDemissao'];
						//$dDataFormatada = DateTime::createFromFormat('d/m/Y', $sData);
						//$_POST['fDemissao'] = $dDataFormatada->format('Y-m-d');
					}
					
		
					//$_POST['fPescSal'] = str_replace(",",".",str_replace(".","",$_POST['fPescSal']));	*/
					
					$oMoeda = FabricaUtilitario::getUtilitario("Moeda");
					$_POST['fValext'] = $oMoeda->valorPorExtenso($_POST['fPescSal'], true, false);
			

					$oMnyPessoaColaborador = $oFachada->inicializarMnyPessoaColaborador($_POST['fPescCodigo'],mb_convert_case($_POST['fPescNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPescCpf'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPescMat'],MB_CASE_UPPER, "UTF-8"),$_POST['fFunCodigo'],$_POST['fUniCodigo'],mb_convert_case($_POST['fPescSal'],MB_CASE_UPPER, "UTF-8"),$_POST['fSetCodigo'],$_POST['fAdmissao'],$_POST['fDemissao'],mb_convert_case($_POST['fValext'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fMotivo'],MB_CASE_UPPER, "UTF-8"));
					$_SESSION['oMnyPessoaColaborador'] = $oMnyPessoaColaborador;
					//if(!($_POST['fDemissao'])){
					//	$oMnyPessoaColaborador->setDemissao($oMnyPessoaColaborador->getAdmissao());
					//}

					// verifica se CPF já existe no BD
					$nQtde = $oFachada->recuperarUmMnyPessoaColaboradorPorCPF($oMnyPessoaColaborador->getPescCpf())->getPescCpf();
					$oMnyPessoaColaboradorBanco = $oFachada->recuperarUmMnyPessoaColaborador($oMnyPessoaColaborador->getPescCodigo());
					
					if($sOP == 'Alterar'){
						if($oMnyPessoaColaboradorBanco->getPescCpf() != $oMnyPessoaColaborador->getPescCpf()){
							if($nQtde > 0){
								$_SESSION['sMsg'] = "Esse CPF j&aacute; existe em nosso Banco de Dados!";
								$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
								header("Location: ".$sHeader);	
								die();
							}
						}
					}else{
						if($nQtde > 0){
							$_SESSION['sMsg'] = "Esse CPF j&aacute; existe em nosso Banco de Dados!";
							$sHeader = "?bErro=2&action=MnyPessoaColaborador.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
							header("Location: ".$sHeader);	
							die();	
						}
					
					}
			
					$oValidate->add_text_field("Nome", $oMnyPessoaColaborador->getPescNome(), "text", "y");
					$oValidate->add_text_field("CPF", $oMnyPessoaColaborador->getPescCpf(), "text", "y");
					$oValidate->add_text_field("Matric", $oMnyPessoaColaborador->getPescMat(), "text", "y");
					$oValidate->add_number_field("Fun&ccedil;&atilde;o", $oMnyPessoaColaborador->getFunCodigo(), "number", "y");
					$oValidate->add_number_field("Unidade", $oMnyPessoaColaborador->getUniCodigo(), "number", "y");
//					$oValidate->check_decimal("Sal&aacute;rio", $oMnyPessoaColaborador->getPescSal(), 2, "y");
					$oValidate->add_text_field("Sal&aacute;rio", $oMnyPessoaColaborador->getPescSal(), "text", "y");
					$oValidate->add_number_field("Setor", $oMnyPessoaColaborador->getSetCodigo(), "number", "y");
					$oValidate->add_date_field("Admiss&atilde;o", $oMnyPessoaColaborador->getAdmissao(), "date", "y");
					  //$oValidate->add_date_field("Demissao", $oMnyPessoaColaborador->getDemissao(), "date", "y");
					$oValidate->add_text_field("Valor extenso", $oMnyPessoaColaborador->getValext(), "text", "y");
					  //$oValidate->add_text_field("Motivo", $oMnyPessoaColaborador->getMotivo(), "text", "y");
 
				
					if (!$oValidate->validation()) {
						$_SESSION['sMsg'] = $oValidate->create_msg();
						$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
						header("Location: ".$sHeader);	
						die();
					}
						
						
				break;

				case 256: // PRESTADOR DE SERVIÇOS -PF
					$sTabela = "MnyPessoaFisica";
					/*$sData = $_POST['fPesfisDatact'];
					$dDataFormatada = DateTime::createFromFormat('d/m/Y', $sData);
					$_POST['fPesfisDatact'] = $dDataFormatada->format('Y-m-d');
								
					$oMoeda = FabricaUtilitario::getUtilitario("Moeda");
					$_POST['fPesfisValext'] = $oMoeda->valorPorExtenso($_POST['fPesfisValserv'], true, false);
			

 					$oMnyPessoaFisica = $oFachada->inicializarMnyPessoaFisica($_POST['fPesfisCodigo'],mb_convert_case($_POST['fPesfisNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisCpf'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisCt'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesfisDatact'],mb_convert_case($_POST['fPesfisValct'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisProrrog'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisAnexo'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisValserv'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisValext'],MB_CASE_UPPER, "UTF-8"),$_POST['fSetCodigo'],$_POST['fUniCodigo'],$_POST['fAtivo']);
*/
					$oMnyPessoaFisica = $oFachada->inicializarMnyPessoaFisica($_POST['fPesgCodigo'],mb_convert_case($_POST['fPesfisNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesfisCpf'],MB_CASE_UPPER, "UTF-8"));
					$_SESSION['oMnyPessoaFisica'] = $oMnyPessoaFisica;
					
					
					
					// verifica se CPF já existe no BD
					$nQtde = $oFachada->recuperarUmMnyPessoaFisicaPorCPF($oMnyPessoaFisica->getPesfisCpf())->getPesfisCpf();
					$oMnyPessoaFisicaBanco = $oFachada->recuperarUmMnyPessoaFisica($oMnyPessoaFisica->getPesfisCodigo());

					if($sOP == 'Alterar'){
						if($oMnyPessoaFisicaBanco->getPesfisCpf() != $oMnyPessoaFisica->getPesfisCpf()){
							if($nQtde > 0){
								$_SESSION['sMsg'] = "Esse CPF j&aacute; existe em nosso Banco de Dados!";
								$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
								header("Location: ".$sHeader);	
								die();
							}
						}
					}else{
						if($nQtde > 0){
							$_SESSION['sMsg'] = "Esse CPF j&aacute; existe em nosso Banco de Dados!";
							$sHeader = "?bErro=2&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
							header("Location: ".$sHeader);	
							die();	
						}
					
					}
					
					$oValidate = FabricaUtilitario::getUtilitario("Validate");
					$oValidate->check_4html = true;
						
				
					$oValidate->add_text_field("Nome", $oMnyPessoaFisica->getPesfisNome(), "text", "y");
					$oValidate->add_text_field("Cpf", $oMnyPessoaFisica->getPesfisCpf(), "text", "y");
					



					if (!$oValidate->validation()) {
						$_SESSION['sMsg'] = $oValidate->create_msg();
						$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
						header("Location: ".$sHeader);	
						die();
					}
						
				break;
				
				case 255: // FORNECEDOR
				case 257: // PSJ - PRESTADOR DE SERVIÇO P. JURÍDICA
				case 258: // BC - BANCO

				$sTabela = "MnyPessoaJuridica";
				
				//echo $_REQUEST['fCodMatriz'];
				
				 if($_REQUEST['fCodMatriz'] > 1){
					$oMnyPessoaJuridica = $oFachada->inicializarMnyPessoaJuridica($_POST['fPesjurCodigo'],mb_convert_case($_POST['fPesjurRazao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurCnpj'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurInsmun'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurInsest'],MB_CASE_UPPER, "UTF-8"),$_POST['fCodMatriz']);
					$oMnyPessoaJuridica->setCodMatriz($_REQUEST['fCodMatriz']);
				 }else{	
					$oMnyPessoaJuridica = $oFachada->inicializarMnyPessoaJuridica($_POST['fPesjurCodigo'],mb_convert_case($_POST['fPesjurRazao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurNome'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurCnpj'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurInsmun'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurInsest'],MB_CASE_UPPER, "UTF-8"),'');
				 }
				 //$oFachadaSys = new FachadaSysBD();
				 
				 
		     	 //$oSysEmpresa = $oFachadaSys->inicializarSysEmpresa($_POST['fEmpCodigo'],mb_convert_case($_POST['fPesjurRazao'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurNome'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurCnpj'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpImagem'],mb_convert_case($_POST['fPesgEndLogra'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEndBairro'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fCidade'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesgEndCep'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEstado'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesFones'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpSite'],$_POST['fEmpFtp'],$_POST['fPesgEmail'],mb_strtoupper($_POST['fEmpInc']),mb_strtoupper($_POST['fEmpAlt']),mb_strtoupper($_POST['fEmpCustos']),mb_convert_case($_POST['fEmpFinanceiro'], MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fEmpDiretor'], MB_CASE_UPPER, "UTF-8"),$_POST['fEmpAceite'],$_POST['fAtivo'],$_POST['fEmpCodPessoa']);
 				 //$oSysEmpresa->setEmpInc(date('d/m/Y H:i:s') . $_SESSION['oUsuarioImoney']->getLogin());
				 //$_SESSION['oSysEmpresa'] = $oSysEmpresa;
				 
				  			
				$_SESSION['oMnyPessoaJuridica'] = $oMnyPessoaJuridica;
				
				
				
					// verifica se CNPJ já existe no BD
					$nQtde = $oFachada->recuperarUmMnyPessoaJuridicaPorCNPJ($oMnyPessoaJuridica->getPesjurCnpj())->getPesjurCnpj();
					$oMnyPessoaJuridicaBanco = $oFachada->recuperarUmMnyPessoaJuridica($oMnyPessoaJuridica->getPesjurCodigo());
					
					if($sOP == 'Alterar'){
						if($oMnyPessoaJuridicaBanco->getPesjurCnpj() != $oMnyPessoaJuridica->getPesjurCnpj()){
							if($nQtde > 0){
								$_SESSION['sMsg'] = "Esse CNPJ j&aacute; existe em nosso Banco de Dados!";
								$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
								header("Location: ".$sHeader);	
								die();
							}
						}
					}else{
						if($nQtde > 0){
							$_SESSION['sMsg'] = "Esse CNPJ j&aacute; existe em nosso Banco de Dados!";
							$sHeader = "?bErro=2&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
							header("Location: ".$sHeader);	
							die();	
						}
					
					}				
				
				
				
				/* verifica se CNPJ já existe no BD
				$nQtde = $oFachada->recuperarUmMnyPessoaJuridicaPorCnpj($oMnyPessoaJuridica->getPesjurCnpj())->getPesjurCnpj();
				if($nQtde > 0){
					$_SESSION['sMsg'] = "Esse CNPJ j&aacute; existe em nosso Banco de Dados!";
					$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
					header("Location: ".$sHeader);	
					die();	
				}
*/
				$oValidate = FabricaUtilitario::getUtilitario("Validate");
 				$oValidate->check_4html = true;
 		    
			if($sOP == 'Alterar')
			$oValidate->add_text_field("Razao", $oMnyPessoaJuridica->getPesjurRazao(), "text", "y");
			$oValidate->add_text_field("Nome", $oMnyPessoaJuridica->getPesjurNome(), "text", "y");
			$oValidate->add_text_field("CNPJ", $oMnyPessoaJuridica->getPesjurCnpj(), "text", "y");
			//$oValidate->add_text_field("Insmun", $oMnyPessoaJuridica->getPesjurInsmun(), "text", "y");
			//$oValidate->add_text_field("Insc estadual", $oMnyPessoaJuridica->getPesjurInsest(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaJuridica=".$_POST['fPesjurCodigo'];
 				header("Location: ".$sHeader);	
 				die();
 			}
			
				break;
				
			}
					

				if (!$oValidate->validation()) {
						$_SESSION['sMsg'] = $oValidate->create_msg();
						$sHeader = "?bErro=1&action=".$sTabela.".preparaFormulario&sOP=".$sOP."&nTipo=".$_POST['fTipCodigo']."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
						header("Location: ".$sHeader);	
						die();
				}
			
		
		
			
		}
			
 		

 		switch($sOP){
 			case "Cadastrar":
					if($nId = $oFachada->inserirMnyPessoaGeral($oMnyPessoaGeral)){
						$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($nId);
						switch($oMnyPessoaGeral->getTipCodigo()){		
							case 254: 
								$oMnyPessoaColaborador->setPescCodigo($nId);
								if($oFachada->inserirMnyPessoaColaborador($oMnyPessoaColaborador)){
									unset($_SESSION['oMnyPessoaGeral']);
									unset($_SESSION['oMnyPessoaColaborador']);
									$_SESSION['sMsg'] = "Colaborador inserido com sucesso!";
									$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
								}else{
									$oFachada->excluirFisicamenteMnyPessoaGeral($nId);
									$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o colaborador!";
									$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=Cadastrar";
								}
							break;
							case 255: // FORNECEDOR
							case 256: // PSF - PRESTADOR DE SERVIÇO P. FISICA
								$oMnyPessoaFisica->setPesfisCodigo($nId);
								if($oFachada->inserirMnyPessoaFisica($oMnyPessoaFisica)){
									unset($_SESSION['oMnyPessoaGeral']);
									unset($_SESSION['oMnyPessoaFisica']);
									if($_REQUEST['fContrato'] == 'S'){
										$_SESSION['sMsg'] = "Informe dados de contrato!";
										unset($_SESSION['sMigalha']);										
										$_SESSION['sMigalha'] = "<a href='?action=MnyPessoaGeral.preparaLista'>Gerenciar Pessoas</a> &gt; <a href='?action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=$nId'>Alterar Pessoa F&iacute;sica</a>";
										$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&fPesgCodigo=".$nId;
									}else{
										$_SESSION['sMsg'] = "Pessoa inserida com sucesso!";
										$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
									}
								}else{
									$oFachada->excluirFisicamenteMnyPessoaGeral($nId);
									$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a pessoa!";
									$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=Cadastrar";
								}
							break;
							case 255: // FORNECEDOR
							case 257: // PSJ - PRESTADOR DE SERVIÇO P. JURÍDICA
							case 258: // BC - BANCO
							
								$oMnyPessoaJuridica->setPesjurCodigo($nId);
								//$oSysEmpresa->setEmpCodPessoa($nId);
					
				 //print_r($oSysEmpresa);
				 //die();	
				// $oFachadaSys = new FachadaSysBD();
								
								//$oFachadaSys->inserirSysEmpresa($oSysEmpresa);
								
								if($_REQUEST['fCodMatriz'] == 'null')
									$oMnyPessoaJuridica->setCodMatriz('NULL');
									if($oFachada->inserirMnyPessoaJuridica($oMnyPessoaJuridica)){
										unset($_SESSION['oMnyPessoaGeral']);
										unset($_SESSION['oMnyPessoaJuridica']);
										if($_REQUEST['fContrato'] == 'S'){
											$_SESSION['sMsg'] = "Informe dados de contrato!";
											unset($_SESSION['sMigalha']);
											$_SESSION['sMigalha'] = "<a href='?action=MnyPessoaGeral.preparaLista'>Gerenciar Pessoas</a> &gt; <a href='?action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=$nId'>Alterar Pessoa Juridica</a>";											
											$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&fPesgCodigo=".$nId;
										}else{											
											if($oMnyPessoaJuridica->getCodMatriz() > 0){
												$_SESSION['sMsg'] = "Filial inserida com sucesso!";
												//$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=".$oMnyPessoaJuridica->getCodMatriz();
												$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
											}else{
												$_SESSION['sMsg'] = "Pessoa Jur&iacute;dica inserida com sucesso!";
												//$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=".$oMnyPessoaJuridica->getPesjurCodigo();
												$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
											}
										}
									}else{
										$oFachada->excluirFisicamenteMnyPessoaGeral($nId);
										$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a pessoa jur&iacute;dica!";
										$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=Cadastrar&nIdMnyPessoaGeral=".$oMnyPessoaJuridica->getCodMatriz();
									}
							break;
						}
					} else {
						$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a pessoa!";
						$sHeader = "?bErro=3&action=MnyPessoaGeral.preparaFormulario&sOP=".$sOP;
					}
					
				break;
				
				case "Alterar":
					if($oFachada->alterarMnyPessoaGeral($oMnyPessoaGeral)){
						switch($oMnyPessoaGeral->getTipCodigo()){		
							case 254: 
								// fazer os sets
								
								if($oFachada->alterarMnyPessoaColaborador($oMnyPessoaColaborador)){
									unset($_SESSION['oMnyPessoaGeral']);
									unset($_SESSION['oMnyPessoaColaborador']);
									$_SESSION['sMsg'] = "Colaborador alterado com sucesso!";
									$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
								}else{
									$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o colaborador!";
									$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=Cadastrar";
								}
							break;
							case 255: // FORNECEDOR
							case 256: // PSF - PRESTADOR DE SERVIÇO P. FISICA 
								// fazer os sets
								
								if($oFachada->alterarMnyPessoaFisica($oMnyPessoaFisica)){
									unset($_SESSION['oMnyPessoaGeral']);
									unset($_SESSION['oMnyPessoaFisica']);
									$_SESSION['sMsg'] = "Pessoa alterada com sucesso!";
									$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
								}else{
									$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a pessoa!";
									$sHeader = "?bErro=1&action=MnyPessoaFisica.preparaFormulario&sOP=Cadastrar";
								}
							break;
							case 255: // FORNECEDOR
							case 257: // PSJ - PRESTADOR DE SERVIÇO P. JURÍDICA
							case 258: // BC - BANCO
								
								if($oFachada->alterarMnyPessoaJuridica($oMnyPessoaJuridica)){
									unset($_SESSION['oMnyPessoaGeral']);
									unset($_SESSION['oMnyPessoaJuridica']);
										if($oMnyPessoaJuridica->getCodMatriz()){
											$_SESSION['sMsg'] = "Filial alterada com sucesso!";
											$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=".$oMnyPessoaJuridica->getCodMatriz();
											
											
											
										}else{
											$_SESSION['sMsg'] = "Pessoa Jur&iacute;dica alterada com sucesso!";
											$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
										}
								}else{
									$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a pessoa jur&iacute;dica!";
									$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=Alterar";
								}
							break;
						}
						
						
						
						/*
						unset($_SESSION['oMnyPessoaGeral']);
						$_SESSION['sMsg'] = "MnyPessoaGeral alterado com sucesso!";
						$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
						*/
					} else {
						$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar a Pessoa!";
						$sHeader = "?bErro=1&action=MnyPessoaGeral.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaGeral=".$_POST['fPesgCodigo']."";
					}
				break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyPessoaGeral']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyPessoaGeral($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Pessoa(s) exclu&iacute;da(s) com sucesso!";
 					if($_REQUEST['nFilial'] == 1){
						
						$sHeader = "?bErro=1&action=MnyPessoaGeral.preparaFormulario&sOP=Alterar&nIdMnyPessoaGeral=".$oFachada->recuperarUmMnyPessoaJuridica($nIdPai)->getCodMatriz();
					}else
						$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir a(s) Pessoa(s)!";
 					$sHeader = "?bErro=1&action=MnyPessoaGeral.preparaLista";
 				}
 			break;
 		} 		
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
