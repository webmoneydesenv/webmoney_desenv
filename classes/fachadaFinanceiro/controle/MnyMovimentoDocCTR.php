<?php
 class MnyMovimentoDocCTR implements IControle{
 
 	public function MnyMovimentoDocCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyMovimentoDoc = $oFachada->recuperarTodosMnyMovimentoDoc();
 
 		$_REQUEST['voMnyMovimentoDoc'] = $voMnyMovimentoDoc;
 		
 		
 		include_once("view/financeiro/mny_movimento_doc/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyMovimentoDoc = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyMovimentoDoc = ($_POST['fIdMnyMovimentoDoc'][0]) ? $_POST['fIdMnyMovimentoDoc'][0] : $_GET['nIdMnyMovimentoDoc'];
 	
 			if($nIdMnyMovimentoDoc){
 				$vIdMnyMovimentoDoc = explode("||",$nIdMnyMovimentoDoc);
 				$oMnyMovimentoDoc = $oFachada->recuperarUmMnyMovimentoDoc($vIdMnyMovimentoDoc[0],$vIdMnyMovimentoDoc[1]);
 			}
 		}
 		
 		$_REQUEST['oMnyMovimentoDoc'] = ($_SESSION['oMnyMovimentoDoc']) ? $_SESSION['oMnyMovimentoDoc'] : $oMnyMovimentoDoc;
 		unset($_SESSION['oMnyMovimentoDoc']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_movimento_doc/detalhe.php");
 		else
 			include_once("view/financeiro/mny_movimento_doc/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyMovimentoDoc = $oFachada->inicializarMnyMovimentoDoc($_POST['fMovCodigo'],$_POST['fDocSeq'],$_POST['fDocPath'],$_POST['fDocNome'],$_POST['fDocOrigem']);
 			$_SESSION['oMnyMovimentoDoc'] = $oMnyMovimentoDoc;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("MovCodigo", $oMnyMovimentoDoc->getMovCodigo(), "number", "y");
			$oValidate->add_number_field("DocSeq", $oMnyMovimentoDoc->getDocSeq(), "number", "y");
			$oValidate->add_text_field("DocPath", $oMnyMovimentoDoc->getDocPath(), "text", "y");
			$oValidate->add_text_field("DocNome", $oMnyMovimentoDoc->getDocNome(), "text", "y");
			$oValidate->add_text_field("DocOrigem", $oMnyMovimentoDoc->getDocOrigem(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyMovimentoDoc.preparaFormulario&sOP=".$sOP."&nIdMnyMovimentoDoc=".$_POST['fMovCodigo']."||".$_POST['fDocSeq']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyMovimentoDoc($oMnyMovimentoDoc)){
 					unset($_SESSION['oMnyMovimentoDoc']);
 					$_SESSION['sMsg'] = "Documentos inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimentoDoc.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Documentos!";
 					$sHeader = "?bErro=1&action=MnyMovimentoDoc.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyMovimentoDoc($oMnyMovimentoDoc)){
 					unset($_SESSION['oMnyMovimentoDoc']);
 					$_SESSION['sMsg'] = "Documentos alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimentoDoc.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Documentos!";
 					$sHeader = "?bErro=1&action=MnyMovimentoDoc.preparaFormulario&sOP=".$sOP."&nIdMnyMovimentoDoc=".$_POST['fMovCodigo']."||".$_POST['fDocSeq']."";
 				}
 			break;
 			case "Excluir":
			    $bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyMovimentoDoc']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyMovimentoDoc($sCampoChave);\n");
				}
 				if($bResultado){
 					$_SESSION['sMsg'] = "Documentos(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimentoDoc.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Documentos!";
 					$sHeader = "?bErro=1&action=MnyMovimentoDoc.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
