<?php
 class MnyPessoaJuridicaCTR implements IControle{
 
 	public function MnyPessoaJuridicaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyPessoaJuridica = $oFachada->recuperarTodosMnyPessoaJuridica();
 
 		$_REQUEST['voMnyPessoaJuridica'] = $voMnyPessoaJuridica;
 		$_REQUEST['voMnyPessoaJuridica'] = $oFachada->recuperarTodosMnyPessoaJuridica();

		
 		
 		include_once("view/financeiro/mny_pessoa_juridica/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 
 		$oMnyPessoaJuridica = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyPessoaJuridica = ($_POST['fIdMnyPessoaJuridica'][0]) ? $_POST['fIdMnyPessoaJuridica'][0] : $_GET['nIdMnyPessoaJuridica'];
 	
 			if($nIdMnyPessoaJuridica){
 				$vIdMnyPessoaJuridica = explode("||",$nIdMnyPessoaJuridica);
 				$oMnyPessoaJuridica = $oFachada->recuperarUmMnyPessoaJuridica($vIdMnyPessoaJuridica[0]);
 				$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($vIdMnyPessoaJuridica[0]);
 			}
 		}

 		$_REQUEST['oMnyPessoaGeral'] = ($_SESSION['oMnyPessoaGeral']) ? $_SESSION['oMnyPessoaGeral'] : $oMnyPessoaGeral;
 		unset($_SESSION['oMnyPessoaGeral']);

 		
 		$_REQUEST['oMnyPessoaJuridica'] = ($_SESSION['oMnyPessoaJuridica']) ? $_SESSION['oMnyPessoaJuridica'] : $oMnyPessoaJuridica;
 		unset($_SESSION['oMnyPessoaJuridica']); 
 		$_REQUEST['voMnyPessoaJuridica'] = $oFachada->recuperarTodosMnyPessoaJuridica();		
				
		//adicionado
		$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
		
		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();

		//$sGrupo = 14;// banco
		$_REQUEST['voSysBanco'] = $oFachadaSys->recuperarTodosSysBanco();
		
		$sGrupo = 13;// tipo de conta
		$_REQUEST['voSysContaTipo'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = 8;// unidade
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = 10;// unidade
		$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_pessoa_juridica/detalhe.php");
 		else
 			include_once("view/financeiro/mny_pessoa_juridica/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyPessoaJuridica = $oFachada->inicializarMnyPessoaJuridica($_POST['fPesjurCodigo'],mb_convert_case($_POST['fPesjurRazao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fPesjurNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fPesjurCnpj'],$_POST['fPesjurInsmun'],$_POST['fPesjurInsest'],$_POST['fCodMatriz']);
 																																																			
			$_SESSION['oMnyPessoaJuridica'] = $oMnyPessoaJuridica;

			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("PesjurCodigo", $oMnyPessoaJuridica->getPesjurCodigo(), "number", "y");
			$oValidate->add_text_field("PesjurRazao", $oMnyPessoaJuridica->getPesjurRazao(), "text", "y");
			$oValidate->add_text_field("PesjurNome", $oMnyPessoaJuridica->getPesjurNome(), "text", "y");
			$oValidate->add_text_field("PesjurCnpj", $oMnyPessoaJuridica->getPesjurCnpj(), "text", "y");
			//$oValidate->add_text_field("PesjurInsmun", $oMnyPessoaJuridica->getPesjurInsmun(), "text", "y");
			//$oValidate->add_text_field("PesjurInsest", $oMnyPessoaJuridica->getPesjurInsest(), "text", "y");
			//$oValidate->add_number_field("CodMatriz", $oMnyPessoaJuridica->getCodMatriz(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaJuridica=".$_POST['fPesjurCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyPessoaJuridica($oMnyPessoaJuridica)){
 					unset($_SESSION['oMnyPessoaJuridica']);
 					$_SESSION['sMsg'] = "Pessoa Jurídica inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaJuridica.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Pessoa Jurídica!";
 					$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyPessoaJuridica($oMnyPessoaJuridica)){
 					unset($_SESSION['oMnyPessoaJuridica']);
 					$_SESSION['sMsg'] = "Pessoa Jurídica alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaJuridica.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Pessoa Jurídica!";
 					$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaJuridica=".$_POST['fPesjurCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyPessoaJuridica']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyPessoaJuridica($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Pessoa Jurídica(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaJuridica.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Pessoa Jurídica!";
 					$sHeader = "?bErro=1&action=MnyPessoaJuridica.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
