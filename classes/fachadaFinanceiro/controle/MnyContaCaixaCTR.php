<?php
 class MnyContaCaixaCTR implements IControle{
 
 	public function MnyContaCaixaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
		
		$nGrupo = 8;
		$_REQUEST['voUnidades'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($nGrupo);
		
		$nCodUnidade = $_REQUEST['fCodUnidade'];
 		$dData = $_REQUEST['fData'];
		if($dData && $nCodUnidade){
 			$voMnyContaCaixa = $oFachada->recuperarTodosMnyContaCaixaPorUnidadeData($nCodUnidade,$dData);	
			$_REQUEST['oMnySaldoDiarioCaixa'] = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeUltimaData($nCodUnidade, 2);
			$_REQUEST['voMnyContaCaixa'] = $voMnyContaCaixa;
			include_once("view/financeiro/mny_conta_caixa/lista.php");
			exit();
		}	
		include_once("view/financeiro/mny_conta_caixa/index.php");
 		exit();
 	
 	}
	
	
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyContaCaixa = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContaCaixa = ($_POST['fIdMnyContaCaixa'][0]) ? $_POST['fIdMnyContaCaixa'][0] : $_GET['nIdMnyContaCaixa'];
 	
 			if($nIdMnyContaCaixa){
 				$vIdMnyContaCaixa = explode("||",$nIdMnyContaCaixa);
 				$oMnyContaCaixa = $oFachada->recuperarUmMnyContaCaixa($vIdMnyContaCaixa[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyContaCaixa'] = ($_SESSION['oMnyContaCaixa']) ? $_SESSION['oMnyContaCaixa'] : $oMnyContaCaixa;
 		unset($_SESSION['oMnyContaCaixa']);
 
 		$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_conta_caixa/detalhe.php");
 		else
 			include_once("view/financeiro/mny_conta_caixa/insere_altera_2.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP == "Cadastrar"){
   			$oMnyContaCaixa = $oFachada->inicializarMnyContaCaixa($_POST['fCodContaCaixa'],$_POST['fCodUnidade'],$_POST['fDataConta'],mb_convert_case($_POST['fCreditoDebito'],MB_CASE_UPPER, "UTF-8"),$_POST['fValor'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fDocPendente'],MB_CASE_UPPER, "UTF-8"),($_POST['fCodArquivo']) ? $_POST['fCodArquivo'] : "NULL",mb_convert_case($_POST['fInseridoPor'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAlteradoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
			$_SESSION['oMnyContaCaixa'] = $oMnyContaCaixa;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("CodUnidade", $oMnyContaCaixa->getCodUnidade(), "number", "y");
			$oValidate->add_text_field("CreditoDebito", $oMnyContaCaixa->getCreditoDebito(), "text", "y");
			$oValidate->add_text_field("Descricao", $oMnyContaCaixa->getDescricao(), "text", "y");
			//$oValidate->add_text_field("DocPendente", $oMnyContaCaixa->getDocPendente(), "text", "y");
			//$oValidate->add_number_field("CodArquivo", $oMnyContaCaixa->getCodArquivo(), "number", "y");
			$oValidate->add_text_field("InseridoPor", $oMnyContaCaixa->getInseridoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oMnyContaCaixa->getAlteradoPor(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyContaCaixa->getAtivo(), "number", "y");


/*			print_r($oMnyContaCaixa);
			die();*/

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
				$sHeader = "?bErro=0&action=MnyContaCaixa.preparaLista&nTipo=1&nCodUnidade=" . $oMnyContaCaixa->getCodUnidade() . "&fDataConta=" . $_REQUEST['fDataConta'];
				header("Location: ".$sHeader);	
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":		        
				$oMnySaldoDiarioCaixa = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeUltimaData($oMnyContaCaixa->getCodUnidade(), 2);
				if($oMnySaldoDiarioCaixa){
					if(!($oMnySaldoDiarioCaixa->getData() == $oMnyContaCaixa->getDataConta())){
						if($oFachada->inserirMnyContaCaixa($oMnyContaCaixa)){
							unset($_SESSION['oMnyContaCaixa']);
							$_SESSION['sMsg'] = "Movimento inserido com sucesso!";
						} else {
							$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
						}
					}else{
							$_SESSION['sMsg'] = "Este Caixa j&aacute; foi fechado !";					
					}
				}else{
							$_SESSION['sMsg'] = "Saldo conta n&atilde;o encontrato!";					
				}
				$sHeader = "?bErro=0&action=MnyContaCaixa.preparaLista&nTipo=1&nCodUnidade=" . $oMnyContaCaixa->getCodUnidade() . "&fDataConta=" . $_REQUEST['fDataConta'];
				
 			break;
 			case "Alterar":
   			    $nItens = count($_POST['fDescricao']);
				$nSucesso=0;
				for($i=0;$i<$nItens;$i++){
					$oMnyContaCaixa = $oFachada->inicializarMnyContaCaixa($_POST['fCodContaCaixa'][$i],$_POST['fCodUnidade'],$_POST['fDataConta'],mb_convert_case($_POST['fCreditoDebito'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fValor'][$i],mb_convert_case($_POST['fDescricao'][$i],MB_CASE_UPPER, "UTF-8"),$_POST['fDocPendente'][$i],($_POST['fCodArquivo']) ? $_POST['fCodArquivo'] : "NULL",mb_convert_case($_POST['fInseridoPor'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAlteradoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
					if($oFachada->alterarMnyContaCaixa($oMnyContaCaixa)){					
						$nSucesso++;
					} 
 				}
				$_SESSION['sMsg'] = $nSucesso ."/" .$nItens . "Movimentos(s) alterado(s) com sucesso!";
				$sHeader = "?bErro=0&action=MnyContaCaixa.preparaLista&nTipo=2&nCodUnidade=" . $_POST['fCodUnidade'] . "&fDataConta=" . $_POST['fDataConta'];
 			break;
			case "FecharCaixa":
				$nSaldoCaixa = $_REQUEST['fCod'];
				$_POST['fCodTipoCaixa'] = 2;				
				$oMnySaldoDiario = $oFachada->recuperarUmMnySaldoDiarioPorUnidadeUltimaData($_REQUEST['fCodUnidade'], $_POST['fCodTipoCaixa']);
				$_POST['fValor'] = ($oMnySaldoDiario->getValorFinal() - $nSaldoCaixa);
				$_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i');								
				$oData = DateTime::createFromFormat('Y-m-d', $_GET['fData']);
			 	$_POST['fData'] = $oData->format('d/m/Y');				
				$oMnySaldoDiario = $oFachada->inicializarMnySaldoDiario($_POST['fCodSaldoDiario'],$_POST['fCodTipoCaixa'],$_POST['fCcrCodigo'],$_GET['fCodUnidade'],$_POST['fData'],$_POST['fValor'],mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),1);
				if($oFachada->inserirMnySaldoDiario($oMnySaldoDiario)){					
					$_SESSION['sMsg'] = "Opera&ccedil;&atilde;o realizada com sucesso !";
					$sHeader = "?action=MnyContaCaixa.preparaLista";
				}else{
					$_SESSION['sMsg'] = "N&atilde;o foi possivel realizar a opera&ccedil;&atilde;o !";
                    $sHeader = "?bErro=0&action=MnyContaCaixa.preparaLista&nTipo=2&nCodUnidade=" . $_GET['fCodUnidade'] . "&fDataConta=" . $_POST['fData'];				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContaCaixa']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyContaCaixa($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "mny_conta_caixa(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContaCaixa.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) mny_conta_caixa!";
 					$sHeader = "?bErro=1&action=MnyContaCaixa.preparaLista";
 				}
 			break;
 		}
		

		
 
 		header("Location: ".$sHeader);		
 	
 	}
	
	
	function exibeDetalhe(){
		$oFachada = new FachadaFinanceiroBD();
		$_REQUEST['oMnyContaCaixa'] = $oFachada->recuperarUmMnyContaCaixa($_GET['nCodContaCaixa']);	
		include_once("view/financeiro/mny_conta_caixa/detalhe_2.php");
		exit();
	}
			
 
 }
 
 
 ?>
