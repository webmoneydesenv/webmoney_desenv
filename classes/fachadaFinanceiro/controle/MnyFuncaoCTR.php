<?php
 class MnyFuncaoCTR implements IControle{
 
 	public function MnyFuncaoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyFuncao = $oFachada->recuperarTodosMnyFuncao();
 
 		$_REQUEST['voMnyFuncao'] = $voMnyFuncao;
 		
 		
 		include_once("view/financeiro/mny_funcao/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyFuncao = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyFuncao = ($_POST['fIdMnyFuncao'][0]) ? $_POST['fIdMnyFuncao'][0] : $_GET['nIdMnyFuncao'];
 	
 			if($nIdMnyFuncao){
 				$vIdMnyFuncao = explode("||",$nIdMnyFuncao);
 				$oMnyFuncao = $oFachada->recuperarUmMnyFuncao($vIdMnyFuncao[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyFuncao'] = ($_SESSION['oMnyFuncao']) ? $_SESSION['oMnyFuncao'] : $oMnyFuncao;
 		unset($_SESSION['oMnyFuncao']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_funcao/detalhe.php");
 		else
 			include_once("view/financeiro/mny_funcao/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyFuncao = $oFachada->inicializarMnyFuncao($_POST['fFunCodigo'],mb_convert_case($_POST['fFunNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fFunInc'],$_POST['fFunAlt'],$_POST['fAtivo']);
 																			
			$_SESSION['oMnyFuncao'] = $oMnyFuncao;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("FunNome", $oMnyFuncao->getFunNome(), "text", "y");
			$oValidate->add_text_field("FunInc", $oMnyFuncao->getFunInc(), "text", "y");
			//$oValidate->add_text_field("FunAlt", $oMnyFuncao->getFunAlt(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyFuncao->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyFuncao.preparaFormulario&sOP=".$sOP."&nIdMnyFuncao=".$_POST['fFunCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyFuncao($oMnyFuncao)){
 					unset($_SESSION['oMnyFuncao']);
 					$_SESSION['sMsg'] = "Função inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyFuncao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Função!";
 					$sHeader = "?bErro=1&action=MnyFuncao.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyFuncao($oMnyFuncao)){
 					unset($_SESSION['oMnyFuncao']);
 					$_SESSION['sMsg'] = "Função alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyFuncao.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Função!";
 					$sHeader = "?bErro=1&action=MnyFuncao.preparaFormulario&sOP=".$sOP."&nIdMnyFuncao=".$_POST['fFunCodigo']."";
 				}
 			break;
 			case "Excluir":
			    $bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyFuncao']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyFuncao($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Função(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyFuncao.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Função!";
 					$sHeader = "?bErro=1&action=MnyFuncao.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
