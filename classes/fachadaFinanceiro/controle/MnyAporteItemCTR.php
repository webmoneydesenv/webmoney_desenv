<?php
 class MnyAporteItemCTR implements IControle{
 
 	public function MnyAporteItemCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyAporteItem = $oFachada->recuperarTodosMnyAporteItem();
 
 		$_REQUEST['voMnyAporteItem'] = $voMnyAporteItem;
 		$_REQUEST['voMnySolicitacaoAporte'] = $oFachada->recuperarTodosMnySolicitacaoAporte();

		$_REQUEST['voMnyMovimento'] = $oFachada->recuperarTodosMnyMovimento();
 		
 		include_once("view/financeiro/mny_aporte_item/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyAporteItem = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyAporteItem = ($_POST['fIdMnyAporteItem'][0]) ? $_POST['fIdMnyAporteItem'][0] : $_GET['nIdMnyAporteItem'];
 	
 			if($nIdMnyAporteItem){
 				$vIdMnyAporteItem = explode("||",$nIdMnyAporteItem);
 				$oMnyAporteItem = $oFachada->recuperarUmMnyAporteItem($vIdMnyAporteItem[0],$vIdMnyAporteItem[1]);
 			}
 		}
 		
 		$_REQUEST['oMnyAporteItem'] = ($_SESSION['oMnyAporteItem']) ? $_SESSION['oMnyAporteItem'] : $oMnyAporteItem;
 		unset($_SESSION['oMnyAporteItem']);
 
 		$_REQUEST['voMnySolicitacaoAporte'] = $oFachada->recuperarTodosMnySolicitacaoAporte();

		$_REQUEST['voMnyMovimento'] = $oFachada->recuperarTodosMnyMovimento();

		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_aporte_item/detalhe.php");
 		else
 			include_once("view/financeiro/mny_aporte_item/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyAporteItem = $oFachada->inicializarMnyAporteItem($_POST['fCodSolicitacaoAporte'],$_POST['fAporteItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fAssinado'],$_POST['fAtivo']);
 			$_SESSION['oMnyAporteItem'] = $oMnyAporteItem;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("CodSolicitacaoAporte", $oMnyAporteItem->getCodSolicitacaoAporte(), "number", "y");
			$oValidate->add_number_field("AporteItem", $oMnyAporteItem->getAporteItem(), "number", "y");
			//$oValidate->add_number_field("MovCodigo", $oMnyAporteItem->getMovCodigo(), "number", "y");
			//$oValidate->add_number_field("MovItem", $oMnyAporteItem->getMovItem(), "number", "y");
			//$oValidate->add_number_field("Ativo", $oMnyAporteItem->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyAporteItem.preparaFormulario&sOP=".$sOP."&nIdMnyAporteItem=".$_POST['fCodSolicitacaoAporte']."||".$_POST['fAporteItem']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyAporteItem($oMnyAporteItem)){
 					unset($_SESSION['oMnyAporteItem']);
 					$_SESSION['sMsg'] = "mny_aporte_item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyAporteItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o mny_aporte_item!";
 					$sHeader = "?bErro=1&action=MnyAporteItem.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyAporteItem($oMnyAporteItem)){
 					unset($_SESSION['oMnyAporteItem']);
 					$_SESSION['sMsg'] = "mny_aporte_item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyAporteItem.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o mny_aporte_item!";
 					$sHeader = "?bErro=1&action=MnyAporteItem.preparaFormulario&sOP=".$sOP."&nIdMnyAporteItem=".$_POST['fCodSolicitacaoAporte']."||".$_POST['fAporteItem']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyAporteItem']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyAporteItem($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "mny_aporte_item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyAporteItem.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) mny_aporte_item!";
 					$sHeader = "?bErro=1&action=MnyAporteItem.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}

 
 }
 
 
 ?>
