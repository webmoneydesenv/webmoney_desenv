<?php
 class MnyMovimentoCTR implements IControle{

 	public function MnyMovimentoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new FachadaViewBD();
        $_REQUEST['sInicio'] = 1;

		$sGrupo = '5';//Tipo de Lançamento
		$_REQUEST['voMnyTipoLancamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$voPessoa = $oFachadaView->recuperarTodosVPessoaFormatada();
 		$_REQUEST['voPessoa'] = $voPessoa;

		if($_REQUEST['fNome'] || $_REQUEST['fIdentificacao'] || $_REQUEST['fFichaAceite'] || $_REQUEST['fDataInicial'] || $_REQUEST['fMovTipo']) {
			$sComplemento = " WHERE emp_codigo=" .	$_SESSION['oEmpresa']->getEmpCodigo()." and " ;
             //Chefe de Obra , ADM Obra ou Financeiro Obra
			if($_SESSION['Perfil']['CodGrupoUsuario'] == 6 || $_SESSION['Perfil']['CodGrupoUsuario'] == 15 || $_SESSION['Perfil']['CodGrupoUsuario'] == 18){
                $sComplemento .= "uni_codigo=".$_SESSION['Perfil']['CodUnidade']." and ";
            }
			$sDescricao = " - Por:";
			if($_REQUEST['fMovTipo']){
				$sComplemento .= "mov_tipo = " . $_REQUEST['fMovTipo'] . " and " ;
				if($_REQUEST['fMovTipo'] == 188)
					$sTipoMov = "A Pagar";
				else
					$sTipoMov = "A Receber";
				$sDescricao .=  " Tipo ". $sTipoMov  ."; ";
			}

			if($_REQUEST['fNome']){
				$sComplemento .= "nome = '" . $_REQUEST['fNome'] . "' and "  ;
				$sDescricao .=  " Nome ". $_REQUEST['fNome'] ."; ";
			}
			if($_REQUEST['fIdentificacao']){
				$sComplemento .= "identificacao = '" . $_REQUEST['fIdentificacao'] . "' and "  ;
				$sDescricao .=  " CNPJ/CPF ". $_REQUEST['fIdentificacao'] ."; ";
			}
			if($_REQUEST['fFichaAceite']){
				$vFichaAceite = explode(".",$_REQUEST['fFichaAceite']);
				if($vFichaAceite[1]){
					if(!($vFichaAceite[1])){
						$vFichaAceite[1] = 1;
					}
					$sComplemento .= "mov_item = " . $vFichaAceite[1] . " and "  ;

				}
				$sComplemento .= "mov_codigo = " . $vFichaAceite[0] . " and "  ;
				$sDescricao .=  " F.A <strong>". $_REQUEST['fFichaAceite'] . "</strong>"."; ";
			}
			if($_REQUEST['fDataInicial']){
				if($_REQUEST['fDataFinal']){
					$sDataFinal = $_REQUEST['fDataFinal'];
				}else{
					$sDataFinal = date('d/m/Y');
				}
				$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataInicial']);
		        $sDataInicialBanco = $oData->format('Y-m-d') ;

				$oData = DateTime::createFromFormat('d/m/Y', $sDataFinal);
		        $sDataFinalBanco = $oData->format('Y-m-d') ;
				if($_REQUEST['fInclusao'] == 1){
					$sCampoData = "mov_data_emissao";
				}else{
					$sCampoData = "mov_data_vencto";
				}

               $sComplemento .= $sCampoData. " between '" . $sDataInicialBanco . "' and '"  . $sDataFinalBanco . "' and ";



				$sDescricao .=  " Periodo ". $_REQUEST['fDataInicial'] ." e ". $sDataFinal . " e empresa ".	$_SESSION['oEmpresa']->getEmpFantasia().";";
			}
				$sComplemento = substr($sComplemento,0,-4);

				$_REQUEST['sDescricao'] = $sDescricao;
				//$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplemento($sComplemento);

                $_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosVPesquisaPorComplemento($sComplemento);
                if(!$_REQUEST['voPesquisaMovimento']){
                    $_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($vFichaAceite[0]);
                    $_REQUEST['oMnyMovimentoItemAgrupador'] = $oFachada->recuperarTodosMnyMovimentoItemAgrupadorPorMovimentoItemAgrupador($vFichaAceite[0],'1');


                }

				//$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplementoProcedure($sComplemento);
                $_REQUEST['oMnyMovimentoItemAgrupador'] = $oFachada->recuperarTodosMnyMovimentoItemAgrupadorPorMovimentoItemAgrupador($vFichaAceite[0],1)[0];


		}

		if($_REQUEST['altEmp'] == 1){

			include_once("view/financeiro/mny_movimento/pesquisa2.php");
		}else{

			include_once("view/financeiro/mny_movimento/pesquisa.php");
		}
        exit();

 	}

 	public function preparaFormulario(){

 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
		$oFachadaView = new FachadaViewBD();
		$nIdPessoa = $_REQUEST['fPesCodigo'];

        switch($_REQUEST['sOP']){
            case 'Etapa1':
                $_REQUEST['voMnyContratoTipo'] = $oFachada->recuperarTodosMnyContratoTipo();
                include_once("view/financeiro/mny_movimento/etapa1.php");
                exit();
            break;
            case 'Etapa1_Ajax':
                $nIdPessoa = $_REQUEST['fPesCodigo'];
                $nTipoContrato = $_REQUEST['fContratoTipo'];
                $voMnyContratoPessoa = $oFachada->recuperarTodosMnyContratoPessoaPorPessoaPorTipoContrato($nIdPessoa,$nTipoContrato);
                $_REQUEST['voMnyContratoPessoa'] = $voMnyContratoPessoa;
                include_once('view/financeiro/mny_movimento/etapa1_ajax.php');
                exit();
            break;
            case 'Etapa0':
                $nTipoContrato = $_REQUEST['fContratoTipo'];

                if($nTipoContrato !=0 && $nTipoContrato !="" && $nTipoContrato !=4){
                    $_REQUEST['sTipoContrato'] =  $oFachada->recuperarUmMnyContratoTipo($nTipoContrato)->getDescricao();
                    $_REQUEST['voMnyPessoa']   =  $oFachadaView->recuperarTodosVPessoaGeralFormatadaPorTipoContratoVigentesPorEmpresa($nTipoContrato,$_SESSION['oEmpresa']->getEmpCodigo());
                }else{
                    $_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
                }
                include_once("view/financeiro/mny_movimento/etapa0_ajax.php");
                exit();
            break;
        }

        //$_REQUEST['sTipoLancamento'] = "Pagar";
		if($_REQUEST['sTipoLancamento'] == 'Pagar'){
			if($_REQUEST['sOP'] == 'Etapa2'){
				$_REQUEST['sOP'] = 'Cadastrar';
				$nContrato = $_REQUEST['fIdMnyContratoPessoa'];
				$oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($nContrato);
				if($oMnyContratoPessoa){
					$nTipoContrato = $oMnyContratoPessoa->getTipoContrato();
                    $_REQUEST['oTotaisContrato'] = $oFachadaView->recuperarUmVContratoAditivoValidadeValorPorContrato($oMnyContratoPessoa->getContratoCodigo());
                }
				else
					$nTipoContrato = 4;
				$_REQUEST['nCodPessoa'] = $_REQUEST['fPesCodigo'];
			}else{ // alterar contrato/despesa de caixa  ou  cadastrar despesa de caixa
				$_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['nIdMnyMovimento']);
				if($_REQUEST['oMnyMovimento']){
					if($oMnyContratoPessoa = $_REQUEST['oMnyMovimento']->getMnyContratoPessoa()){
						$nTipoContrato = $oMnyContratoPessoa->getTipoContrato();
                        $_REQUEST['oTotaisContrato'] = $oFachadaView->recuperarUmVContratoAditivoValidadeValorPorContrato($oMnyContratoPessoa->getContratoCodigo());
                    }else
						$nTipoContrato = 4;
				//}else{
				//	$oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($_REQUEST['fContratoCodigo']);
				}
				$_REQUEST['nCodPessoa'] = $_REQUEST['fPesCodigo'];
//				$_REQUEST['oMnyMovimentoItem'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['nIdMnyMovimento'],1);
			}
		}else{
			$_REQUEST['oMnyMovimentoItem'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['nIdMnyMovimento'],1);
		 	$_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['nIdMnyMovimento']);
			$nTipoContrato = 9;
		}

		$_REQUEST['oMnyContratoPessoa'] = $oMnyContratoPessoa;
		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($nTipoContrato,0);

		//lista de itens do movimento...
		$_REQUEST['voMnyMovimentoItem'] = $oFachada->recuperarTodosMnyMovimentoItemPorMovimento($_REQUEST['nIdMnyMovimento']);

		//contem o somatorio de valores...
		$_REQUEST['oMnyMovimentoValorGlobal'] = $oFachada->recuperarUmMnyMovimentoItemValorGlobal($_REQUEST['nIdMnyMovimento']);
		$_REQUEST['oMnyMovimentoValorGlobalLiquido'] = $oFachada->recuperarUmMnyMovimentoItemValorGlobalLiquido($_REQUEST['nIdMnyMovimento']);

		if($_REQUEST['sTipoLancamento'] == "Pagar"){
			$sGrupo = '2'; // Conta
		}else{
			$sGrupo = '1'; // Conta
		}

		$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '3'; // Tipo de aceite
		$_REQUEST['voMnyTipoAceite'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '4'; // Tipo de doc
		$_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '7'; // Centro de Negocio
		$_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		//$sGrupo = '8'; // Unidade
		//$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());

		$sGrupo = '10'; // Setor
		$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '11'; // Forma de Pag
		$_REQUEST['voMnyFormaPagamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '5';//Tipo de Lançamento
		$_REQUEST['voMnyTipoLancamento'] = $oFachada-> recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '6'; // Custo
		$_REQUEST['voMnyCusto'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = 2; // Centro de Custo
		$_REQUEST['voMnyCentroCusto'] = $oFachada->recuperarTodosMnyPlanoContasCabeca($sGrupo);

		if($_REQUEST['sOP'] == "Alterar"){

            if($oFachada->recuperarUmMnyAporteItemPorMovimento($_REQUEST['oMnyMovimento']->getMovCodigo())){
                $_REQUEST['nAporte']=1;
            }else{
               $_REQUEST['nAporte']=2;
            }

            //print_r($nTipoContrato);die();
            $_REQUEST['fContratoTipo'] = $nTipoContrato;
			$_REQUEST['oMnyMovimentoItem'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['nIdMnyMovimento'], 1);
			$oMnyConciliacao  = $oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($_REQUEST['nIdMnyMovimento'], $_REQUEST['nMovItem']);
			$_REQUEST['oMnyTransferencia']  = $oFachada->recuperarUmMnyTransferenciaPorMovimento($_REQUEST['nIdMnyMovimento']);
			if($oMnyConciliacao){
				$_REQUEST['Conciliado'] = 1;
			}else{
				$_REQUEST['Conciliado'] = 0;
			}
		}


        if($_REQUEST['sTipoLancamento'] == 'Pagar'){
		   $_REQUEST['nTipo'] = 188;
		}elseif($_REQUEST['sTipoLancamento'] == 'Receber'){
			$_REQUEST['nTipo'] = 189;
		}


            if($nTipoContrato != 4 && $_REQUEST['sTipoLancamento'] != "Receber"){
                // desabilitar Alteração
                $_REQUEST['sSomenteLeitura'] = " readonly='readonly' ";
            }else{
                $_REQUEST['sSomenteLeitura'] = "";
            }

            $_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			include_once("view/financeiro/mny_movimento/insere_altera_2.php");
			exit();


 	}

    public function preparaFormularioTransferencia(){
        $oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
		$oFachadaView = new FachadaViewBD();

        if($_REQUEST['sOP'] == "Transferir" || $_REQUEST['sOP'] == 'AlterarTransferencia'){
            $sGrupo = 2;
			$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasTransf($sGrupo,1);

		    $sGrupo = '7'; // Centro de Negocio
		    $_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
            $_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial(7,0);

            $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());

            $_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
			if($_REQUEST['nIdMnyMovimento'])
				$_REQUEST['oMnyMovimento'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['nIdMnyMovimento']);

            if($_REQUEST['sOP'] == 'AlterarTransferencia'){
              $nIdMnyTransferencia = ($_POST['fIdMnyTransferencia'][0]) ? $_POST['fIdMnyTransferencia'][0] : $_GET['nIdMnyTransferencia'];

                if($nIdMnyTransferencia){
                    $vIdMnyTransferencia = explode("||",$nIdMnyTransferencia);
                    $oMnyTransferencia = $oFachada->recuperarUmMnyTransferencia($vIdMnyTransferencia[0]);
                    $_REQUEST['oMnyTransferencia'] = $oMnyTransferencia;

                    $_REQUEST['oMnyMovimentoDe'] = $oFachada->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoOrigem());
                    $_REQUEST['oMnyMovimentoItemDe'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['oMnyMovimentoDe']->getMovCodigo(),1);
                    $_REQUEST['voMnyContaCorrenteDe'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoDe']->getUniCodigo());

                    $_REQUEST['oMnyMovimentoPara'] = $oFachada->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoDestino());
                    $_REQUEST['oMnyMovimentoItemPara'] = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['oMnyMovimentoPara']->getMovCodigo(),1);
                    $_REQUEST['voMnyContaCorrentePara'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoPara']->getUniCodigo());

                    $_REQUEST['voTipoAplicacao'] = 	$oFachada->recuperarTodosMnyPlanoContasPorGrupo('2.4.30');

                }
            }
            include_once("view/financeiro/mny_movimento/insere_altera_trans.php");
            exit();
        }
    }

  	public function preparaFormularioTrocaEmpresa(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new FachadaViewBD();

		$sGrupo = '5';//Tipo de Lançamento
		$_REQUEST['voMnyTipoLancamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$voPessoa = $oFachadaView->recuperarTodosVPessoaFormatada();
 		$_REQUEST['voPessoa'] = $voPessoa;

		if($_REQUEST['fNome'] || $_REQUEST['fIdentificacao'] || $_REQUEST['fFichaAceite'] || $_REQUEST['fDataInicial'] || $_REQUEST['fMovTipo']) {
			$sComplemento = " WHERE emp_codigo=" .	$_SESSION['oEmpresa']->getEmpCodigo()." and " ;

			if($_REQUEST['fFichaAceite']){
				$vFichaAceite = explode(".",$_REQUEST['fFichaAceite']);
				if($vFichaAceite[1]){
					if(!($vFichaAceite[1])){
						$vFichaAceite[1] = 1;
					}
					$sComplemento .= "mov_item = " . $vFichaAceite[1] . " and "  ;
				}
				$sComplemento .= "mov_codigo = " . $vFichaAceite[0]   ;
				$sDescricao .=  " F.A <strong>". $_REQUEST['fFichaAceite'] . "</strong>"."; ";
			}
				//$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplemento($sComplemento);
				$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosVPesquisaPorComplemento($sComplemento);

				//$_REQUEST['voPesquisaMovimento'] = $oFachadaView->recuperarTodosPesquisaMovimentoItemPorComplementoProcedure($sComplemento);
		}
			include_once("view/financeiro/mny_movimento/pesquisa2.php");
	 		exit();
	}

 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
			$sLogado = date('d/m/Y') ." ". $_SESSION['oUsuarioImoney']->getLogin();
			if($sOP =='Cadastrar' || $sOP =='Transferir'){
				$_POST['fMovDataInclusao']= date('d/m/Y');
			}

			//$_POST['fEmpCodigo']= $_SESSION['oEmpresa']->getEmpCodigo();

			//adiciona o contrato no movimento...
			if($sOP == 'Alterar' && $_REQUEST['Pagina'] == 3){
				$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);
				$oMnyMovimento->setMovContrato($_REQUEST['fIdMnyContratoPessoa']);
				$oMnyContratoPessoa = $oFachada->recuperarUmContratoPessoa($_REQUEST['fIdMnyContratoPessoa']);
			}else{
				if($sOP == 'Transferir' ){
					//Origem
					$_POST['fEmpCodigo'] = $_SESSION['oEmpresa']->getEmpCodigo();
					$_POST['fPesCodigoOrigem'] = $_SESSION['oEmpresa']->getEmpCodPessoa();
					$_POST['fMovParcelas'] = 'null';

					//$oMnyConciliacao = $oFachada->inicializarMnyConciliacao($_POST['fCodConciliacao'],$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fUniCodigo'],1,($_POST['fCodCartaoCredito']) ? $_POST['fCodCartaoCredito'] : "NULL",$_POST['fContaDe'],$_POST['fMovDataInclusao'],$_POST['fMovValorVale'],$_POST['fValorDocPendente'],$_POST['fMovValor'],mb_convert_case($_POST['fObservacao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fRealizadoPor'],MB_CASE_UPPER, "UTF-8"),($_POST['fConsolidado'])? $_POST['fConsolidado'] : 0 ,$_POST['fMovDataEmissao'], $_POST['fAtivo']);
					//$oMnyConciliacao->setValorTransferenciaBanco($_POST['fMovValor']);
					//$_SESSION['oMnyConciliacao'] = $oMnyConciliacao;
					$_POST['fMovTipo'] = 188;
                    $_POST['fMovContrato'] = 'NULL';
					$oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigoOrigem'],$_POST['fSetCodigo'],addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']);

				}else{
					$oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigo'],$_POST['fSetCodigo'],addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']);
//					$oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigo'],$_POST['fSetCodigo'],$_POST['fMovObs'],$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],$_POST['fMovDocumento'],$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],1,$_POST['fMovOutrosDesc']);
				}

			}



			$_SESSION['oMnyMovimento'] = $oMnyMovimento;

			if($sOP =='Cadastrar' || $sOP =='Transferir'){
				$oMnyMovimento->setMovInc($sLogado);
				//$oMnyConciliacao->setRealizadoPor($sLogado);
			}elseif($sOP =='Alterar'){
				$oMnyMovimento->setMovAlt($sLogado);
			}
			$nDias = $_REQUEST['fPeriodicidade'];
			$nParcelas = $_REQUEST['fMovParcelas'];

 			$_SESSION['oMnyMovimentoItem'] = $oMnyMovimentoItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 		//	$oValidate->add_date_field("Data de Inclus&atilde;o", $oMnyMovimento->getMovDataInclusao(), "date", "y");
			$oValidate->add_date_field("Data de Emiss&atilde;o", $oMnyMovimento->getMovDataEmissao(), "date", "y");
			$oValidate->add_number_field("Unid Neg&oaccute;cio", $oMnyMovimento->getNegCodigo(), "number", "y");

			if($_REQUEST['sTipoLancamento'] == 'Pagar'){
				if($sOP != 'Transferir'){
					$oValidate->add_number_field("Unidade Custo", $oMnyMovimento->getCusCodigo(), "number", "y");
					$oValidate->add_number_field("Centro", $oMnyMovimento->getCenCodigo(), "number", "y");
				}
			}

			$oValidate->add_number_field("Unidade", $oMnyMovimento->getUniCodigo(), "number", "y");
			$oValidate->add_number_field("Pessoa", $oMnyMovimento->getPesCodigo(), "number", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getConCodigo(), "number", "y");
			$oValidate->add_number_field("Setor", $oMnyMovimento->getSetCodigo(), "number", "y");
			//$oValidate->add_text_field("Obs", $oMnyMovimento->getMovObs(), "text", "y");
			$oValidate->add_text_field("Incluído por", $oMnyMovimento->getMovInc(), "text", "y");
			if($sOP == 'Alterar')
				$oValidate->add_text_field("MovAlt", $oMnyMovimento->getMovAlt(), "text", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getMovContrato(), "number", "y");
			$oValidate->add_text_field("Documento", $oMnyMovimento->getMovDocumento(), "text", "y");

			if($sOP == 'Cadastrar'){
				$oValidate->add_number_field("Parcelas", $oMnyMovimento->getMovParcelas(), "number", "y");
			}

			if($sOP != 'Transferir'){
				$oValidate->add_number_field("Tipo", $oMnyMovimento->getMovTipo(), "number", "y");

			}

			$oValidate->add_number_field("EmpCodigo", $oMnyMovimento->getEmpCodigo(), "number", "y");
			//$oValidate->add_number_field("Icms", $oMnyMovimento->getMovIcmsAliq(), "number", "y");
			//$oValidate->add_number_field("MovValorGlob", $oMnyMovimento->getMovValorGlob(), "number", "y");
			//$oValidate->add_number_field("Pis", $oMnyMovimento->getMovPis(), "number", "y");
			//$oValidate->add_number_field("Confins", $oMnyMovimento->getMovConfins(), "number", "y");
			//$oValidate->add_number_field("Csll", $oMnyMovimento->getMovCsll(), "number", "y");
			//$oValidate->add_number_field("Iss", $oMnyMovimento->getMovIss(), "number", "y");
			//$oValidate->add_number_field("Ir", $oMnyMovimento->getMovIr(), "number", "y");
			//$oValidate->add_number_field("Irrf", $oMnyMovimento->getMovIrrf(), "number", "y");
			//$oValidate->add_number_field("Inss", $oMnyMovimento->getMovInss(), "number", "y");
			//$oValidate->add_number_field("Ativo", $oMnyMovimento->getAtivo(), "number", "y");

 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				if($sOP == 'Transferir'){
                    $sHeader = "?bErro=1&action=MnyMovimento.preparaFormularioTransferencia&sOP=".$sOP."&nIdMnyMovimento=".$_POST['fMovCodigo']."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&fContratoTipo=".($oMnyContratoPessoa)? $oMnyContratoPessoa->getTipoContrato() : $_REQUEST['fContratoTipo']."&fContratoCodigo=".$_REQUEST['fContratoCodigo'];
                }else{
 				   $sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&nIdMnyMovimento=".$_POST['fMovCodigo']."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&fContratoTipo=".($oMnyContratoPessoa)? $oMnyContratoPessoa->getTipoContrato() : $_REQUEST['fContratoTipo']."&fContratoCodigo=".$_REQUEST['fContratoCodigo'];
                    header("Location: ".$sHeader);
                    die();
                }
 			}
 		}

		if($_REQUEST['fMovTipo']==188){
			$sPrefixo = "A_Pagar";
		}else{
			$sPrefixo = "A_Receber";
		}

		switch($sOP){
 			case "Cadastrar":
			//$_POST['fMovValor']  = round($_POST['fMovValor'] / $_REQUEST['fMovParcelas'],2) ;
			$oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($oMnyMovimento->getMovContrato());

			//		$sOrigem = array('.',',');
			//		$sDestino = array('','.');
			//		$MovValor = str_replace($sOrigem, $sDestino, $_REQUEST['fMovValor']);
			// 	if($oMnyContratoPessoa->getSaldoContrato() >= $MovValor){

		  if($nMovCodigo = $oFachada->inserirMnyMovimento($oMnyMovimento)){

				$nDias = $_REQUEST['fPeriodicidade'];
				$nParcelas = ($_REQUEST['fMovParcelas']) ? $_REQUEST['fMovParcelas']  : 1;

			/*
				$sOrigem = array('.',',');
				$sDestino = array('','.');
				$nValorLiquidoFormatadoBanco = str_replace($sOrigem, $sDestino, $_POST['fValorLiquido']);
				$nValorParcela = round(($nValorLiquidoFormatadoBanco / $nParcelas) ,2);
				$_POST['fValorLiquido'] = number_format($nParcela , 2, ',', '.');
			*/
				for ($i = 1; $i <= $nParcelas; $i++){
				$_POST['fMovItem'] = $i;
				//adicionar periodicidade para data de vencimento
				$dCompetencia = $_POST['fCompetencia'];
				$dMesAno = $_POST['fCompetencia'];

				if($i==1){
					$dDataVencimento = $_POST['fMovDataVencto'];
					//$dCompetencia = "01/" . $_POST['fCompetencia'];
//					$dCompetencia = $_POST['fCompetencia'];
//					$dMesAno = $_POST['fCompetencia'];
				}else{
					$oData = new Data($_POST['fMovDataVencto']);
					$dDataVencimento = $oData->avancaData($nDias * ($i-1));
//					$dMesAno = substr($dDataVencimento,3,15);//$oData->retornaProximoMesAno($dDataVencimento);
					//$dCompetencia = "01/" . $dMesAno;
//					$dCompetencia = $dMesAno;
				}
				$_POST['fMovDataPrev'] = date('d/m/Y');
                        $nCaixinha = ($_POST['fCaixinha']) ? $_POST['fCaixinha'] : '0';
                        $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                        $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
                        $sFaOrigem = 'NULL';
						$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$dDataVencimento,$_POST['fMovDataPrev'],$_POST['fMovValorParcela'],$_POST['fMovJuros'],$_POST['fValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'],$_POST['fTipAceCodigo'],$dCompetencia,1,0,1,$nCaixinha,$_POST['FaOrigem'],$nNf,$nConciliado);

                        //TRAMITAÇÂO...
                        $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],18,15,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Lançado.",MB_CASE_UPPER, "UTF-8"),1);

                        $oMnyMovimentoItem->setMovCodigo($nMovCodigo);
                        $oMnyMovimentoItemProtocolo->setMovCodigo($nMovCodigo);

                        if($oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem)){
                            $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                        }

 						unset($_SESSION['oMnyMovimentoItem']);
					}
 					unset($_SESSION['oMnyMovimento']);

 					$_SESSION['sMsg'] = "Movimento [".$nMovCodigo."] inserido com sucesso!";


					$oUplaod = UploadMultiplo::UploadBanco($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$nMovCodigo,$_REQUEST['fContratoTipo']);
					//if($oUplaod = 1)

					$sHeader = "?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$nMovCodigo."&fContratoTipo=".$_REQUEST['fContratoTipo'];

 			} else{
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
 			}
//			}else{
 //					$_SESSION['sMsg'] = "O saldo do contrato nao cobre o valor desse movimento!";
 //					$sHeader = "?bErro=2&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento']."&fContratoTipo=".$_REQUEST['fContratoTipo']."&fContratoCodigo=".$_REQUEST['fContratoCodigo'];
//			}
 			break;
 			case "Alterar":
                // FLAVIO SÓ ALTERA SE FOR DESPESA DE CAIXA
                // RESTO DO MUNDO ALTERA SE NAO TIVER NO APORTE

                //Financeiro OBRA , Nota Fiscal...



                //Usado no Caixinha...
                $nCaixinha = ($_POST['fCaixinha'] )?$_POST['fCaixinha']:'0';
                $voMnyMovimentoItem = $oFachada->recuperarTodosMnyMovimentoItemPorMovimento($_REQUEST['fMovCodigo']);

                if((!$oAporteItem = $oFachada->recuperarUmMnyAporteItemPorMovimento($oMnyMovimento->getMovCodigo())) || $_SESSION['Perfil']['CodGrupoUsuario'] == 8){

                    if($oFachada->alterarMnyMovimento($oMnyMovimento)){
                        //Caixinha...
                        foreach($voMnyMovimentoItem  As $oMnyMovimentoItem){
                            $oMnyMovimentoItem->setCaixinha($nCaixinha);
                            $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem);
                        }
                        unset($_SESSION['oMnyMovimento']);
                            $oUplaod = UploadMultiplo::AlteraUploadBanco($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodArquivo'],$oMnyMovimento->getMovCodigo());
                            $_SESSION['sMsg'] = "Movimento [".$oMnyMovimento->getMovCodigo()."] alterado com sucesso!";
                            $sHeader = "?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_REQUEST['fMovCodigo']."&fContratoTipo=".$_REQUEST['fContratoTipo'];
                    } else {
                            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Movimento!";
                            $sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_POST['fMovCodigo']."";
                    }

                }else{
                    $aDoc = array(4,16,18,23);
                    //print_r($_FILES['arquivo']);die();
                    if($_SESSION['Perfil']['CodGrupoUsuario']==1){
                        foreach($_REQUEST['fCodContratoDocTipo'] As $Chave=>$Valor){
                           //print_r($Valor);die();
                            if(in_array($Valor,$aDoc)){
                                $oUplaod = UploadMultiplo::AlteraUploadBanco($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodArquivo'],$oMnyMovimento->getMovCodigo());
                            }
                        }
                    }

                    $_SESSION['sMsg'] = 'Ficha de Aceite não pode ser alterada pois faz parte do aporte ['.$oAporteItem->getMnySolicitacaoAporte()->getNumeroAporteFormatado().']';
                    $sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_POST['fMovCodigo']."";
                }
 			break;
			case "Transferir":
                //cod Transferencia = 3476 usado para testar...
                $oMnyMovimento->setCusCodigo(320);
                $oMnyMovimento->setCenCodigo(320);
              //  $oMnyMovimento->setMovContrato('NULL');
                $voMnyContaCorrenteCaixinha = $oFachada->recuperarTodosMnyContaCorrenteCaixinha();
                foreach($voMnyContaCorrenteCaixinha As $oMnyContaCorrenteCaixinha){
                    $aCaixinha[] = $oMnyContaCorrenteCaixinha->getCcrCodigo();
                }

				if($nMovCodigoPagar = $oFachada->inserirMnyMovimento($oMnyMovimento)){



					$_POST['fMovCodigo'] = $nMovCodigoPagar;
					$_POST['fMovItem'] = 1;
                    //$nCaixinha = 0;
                    $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                    $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
                    //$sFaOrigem = 'NULL';
                     if(in_array($_POST['fContaPara'],$aCaixinha)){
                        $nCaixinha = 1 ;
                     }else{
                        $nCaixinha = 0;
                     }
					$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataEmissao'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,179,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'] = null,248,$_REQUEST['fCompetencia'],1,0,1,$nCaixinha,$_POST['FaOrigem'],0,$nConciliado);
					//$oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$_POST['fMovDataPrev'],$_POST['fMovValorParcela'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'],$_POST['fTipAceCodigo'],$_REQUEST['fCompetencia'],1);
					$nMovItemPagar = $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem);

                    //TRAMITAÇÂO...
                    $oMnyMovimentoItemProtocoloPagar = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],18,15,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Lançado.",MB_CASE_UPPER, "UTF-8"),1);
                    $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloPagar);

				    $oFachadaSys = new FachadaSysBD();
					$oSysEmpresa = $oFachadaSys->recuperarUmSysEmpresa($_POST['fEmpCodigoPara']);

					$oMnyMovimento->setEmpCodigo($oSysEmpresa->getEmpCodigo());
					$oMnyMovimento->setPesCodigo($oSysEmpresa->getEmpCodPessoa());
					$oMnyMovimento->setUniCodigo($_POST['fUniCodigoPara']);
					$oMnyMovimento->setMovTipo('189');
                    $oMnyMovimento->setCusCodigo('NULL');
                    $oMnyMovimento->setCenCodigo('NULL');
                  //  $oMnyMovimento->setMovContrato('NULL');

					       // $oUplaod = UploadMultiplo::upload($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$nMovCodigoPagar,7);
							if($nMovCodigoReceber = $oFachada->inserirMnyMovimento($oMnyMovimento)){

								$_POST['fMovCodigo'] = $nMovCodigoReceber;
								$_POST['fMovItem'] = 1;
                                //$nCaixinha = ($_POST['fCaixinha']) ? $_POST['fCaixinha'] : '0';
                                if(in_array($_POST['fContaPara'],$aCaixinha)){
                                    $nCaixinha = 1 ;
                                }else{
                                    $nCaixinha = 0;
                                }

                                $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                                $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
								$oMnyMovimentoItemDestino = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataEmissao'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,169,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],248,$_REQUEST['fCompetencia'],1,0,1,$nCaixinha,$_POST['FaOrigem'],$nNf,$nConciliado);
								$nMovItemReceber = $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItemDestino);
                                //TRAMITAÇÂO...
                                $oMnyMovimentoItemProtocoloReceber = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],18,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Lançado.",MB_CASE_UPPER, "UTF-8"),1);
                                $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloReceber);

								//unset($_SESSION['oMnyMovimento']);

								//$oUplaod = UploadMultiplo::upload($_FILES['arquivo'],7,$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$nMovCodigoReceber,$_REQUEST['fContratoTipo']);
                                $_POST['fCodSolicitacaoAporte'] = 'NULL';
								$oMnyTransferencia = $oFachada->inicializarMnyTransferencia($nCodTransferencia,$nMovCodigoPagar,$nMovItemPagar,$_POST['fContaDe'],$nMovCodigoReceber,$nMovItemReceber,$_POST['fContaPara'],$_POST['fTipoAplicacao'],$_POST['fCodSolicitacaoAporte'],$_POST['fOperacao'],1);

								$nCodTransferencia = $oFachada->inserirMnyTransferencia($oMnyTransferencia);

                                $arquivos= $_FILES['arquivo'];
                                for($i = 0 ; count($arquivos['name']) > $i  ;$i++){
                                   if($_FILES["arquivo"]["tmp_name"][$i]){
                                        $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                        $Tipo = $_FILES["arquivo"]["type"][$i];
                                        $Tamanho = $_FILES["arquivo"]["size"][$i];
                                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                                        $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                        $Anexo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                        unlink("c:\\wamp\\tmp\\arquivo.pdf");
                                        $sNome = 'TRANSF_'.$nCodTransferencia;
                                        $sArquivo = $Anexo;

                                        $sExtensao = $Tipo;
                                        $nCodTipo = $_REQUEST['fCodContratoDocTipo'][$i];
                                        $nTamanho = $Tamanho;
                                        $dDataHora= date('Y-m-d H:i:s');
                                        $sRealizadoPor = $_SESSION['Perfil']['Login'];
                                        $nAtivo = 1;
                                        $nContratoCodigo ='NULL';
                                        $nCodConciliacao = 'NULL';
                                        $nMovCodigo = $oMnyMovimentoItem->getMovCodigo();
                                        $nMovItem = $oMnyMovimentoItem->getMovItem();
                                        $oSysArquivo = $oFachadaSys->inicializarSysArquivo($nCodArquivo,$sNome,$sArquivo,$sExtensao,$nCodTipo,$nTamanho,$dDataHora,$sRealizadoPor,$nAtivo,$nContratoCodigo,$nCodConciliacao,$nMovCodigo,$nMovItem);
                                        $oSysArquivo->setArquivo($sArquivo);
                                        $oFachadaSys->inserirSysArquivo($oSysArquivo);
                                    }
                                }
								$_SESSION['sMsg'] = "Transfer&ecirc;ncia realizada com sucesso. <br> N&deg;: (".$nMovCodigoPagar . "/" . $nMovCodigoReceber .")";
								$sHeader = "?action=MnyTransferencia.preparaLista&nEmitirDoc=".$_REQUEST['fEmitirDoc']."&fMovCodigo=".$nMovCodigoPagar;

							}else{
								$oFachada->excluirMnyMovimento($nMovCodigoPagar);
								$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel realizar a tranfer&ecirc;ncia!";
								$sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
							}
				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel realizar a tranfer&ecirc;ncia!";
 					$sHeader = "?bErro=2&action=MnyMovimento.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
 				}
 			break;
 			case "Excluir":
			    $bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyMovimento']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyMovimento($sCampoChave);\n");
				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Movimento(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyMovimento.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Movimento!";
 					$sHeader = "?bErro=1&action=MnyMovimento.preparaLista";
 				}
 			break;
 		}

 		header("Location: ".$sHeader);
 	}

	function carregaTipo(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodCusto = $_REQUEST['fCenCodigo'];
			$oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($nCodCusto);
			$voMnyContas = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($oMnyPlanoContas->getCodigo());

			$_REQUEST['voMnyContas'] = $voMnyContas;
			include_once("view/financeiro/mny_movimento/centro_custo_ajax.php");
			exit();
	}

	function carregaTipoConta(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodUnidade = $_REQUEST['fUniCodigo'];
			$nCodUnidade1 = $_REQUEST['fUniCodigo1'];

			if($nCodUnidade){
				$_REQUEST['voMnyContaCorrenteUnidadeDe'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);
			}else{
				$_REQUEST['voMnyContaCorrenteUnidadePara'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade1);
			}
			include_once("view/financeiro/mny_movimento/tipo_conta_ajax.php");

			exit();
	}

	function carregaTypeApliction(){
		$oFachada = new FachadaFinanceiroBD();
		$sGrupo = '';
		$_REQUEST['voTipoAplicacao'] = 	$oFachada->recuperarTodosMnyPlanoContasPorGrupo('2.4.30');
		include_once('view/financeiro/mny_movimento/tipo_aplicacao_ajax.php');
	}

	function carregaRelatorioImposto(){
		if($_REQUEST['nRelatorio'] == 5){
			$oFachadaFinanceiro = new FachadaFinanceiroBD();

			$sComplemento = "
			INNER JOIN mny_plano_contas on mny_plano_contas.plano_contas_codigo = mny_movimento.con_codigo
			INNER JOIN v_pessoa_geral_formatada on v_pessoa_geral_formatada.pesg_codigo = mny_movimento.pes_codigo
			INNER JOIN mny_movimento_item on mny_movimento_item.mov_codigo = mny_movimento.mov_codigo
			WHERE mny_movimento.mov_data_inclusao  BETWEEN '" . $_REQUEST['fDataInicial'] . "' AND '" . $_REQUEST['fDataFinal'] . "' AND mny_movimento.ativo= 1";
			$sCampos = "
			mny_movimento.mov_codigo,
			mny_movimento_item.mov_item AS ativo,
			v_pessoa_geral_formatada.nome AS mov_devolucao,
			mny_movimento.mov_obs,
			mny_plano_contas.descricao AS mov_alt,
			mny_movimento.mov_documento,
			mny_movimento_item.mov_valor_parcela AS mov_parcelas,
			mny_movimento_item.mov_juros AS mov_contrato,
			mny_movimento_item.mov_retencao AS cen_codigo,
			mny_movimento_item.mov_data_vencto AS mov_data_emissao,
			";

			if($_REQUEST['fMovIcmsAliq'])
				$sCampos .= "mov_icms_aliq, ";
			if($_REQUEST['fMovPis'])
				$sCampos .= "mov_pis, ";
			if($_REQUEST['fMovConfins'])
				$sCampos .= "mov_confins, ";
			if($_REQUEST['fMovCsll'])
				$sCampos .= "mov_csll, ";
			if($_REQUEST['fMovIss'])
				$sCampos .= "mov_iss, ";
			if($_REQUEST['fMovIr'])
				$sCampos .= "mov_ir, ";
			if($_REQUEST['fMovIrrf'])
				$sCampos .= "mov_irrf, ";
			if($_REQUEST['fMovInss'])
				$sCampos .= "mov_inss, ";

			$sCampos = substr( $sCampos , 0, -2);
			$oDataInicial = new DateTime($_REQUEST['fDataInicial']);
			 $_REQUEST['dInicial']  = $oDataInicial->format("d/m/Y");
			$oDataFinal = new DateTime($_REQUEST['fDataFinal']);
			$_REQUEST['dFinal'] = $oDataFinal->format("d/m/Y");

			if($_REQUEST['voMnyMovimentoImposto'] = $oFachadaFinanceiro->recuperarTodosMnyMovimentoImpostoRelatorio($sCampos,$sComplemento)){
				$_SESSION['voMnyMovimentoImpostoXls'] = $_REQUEST['voMnyMovimentoImposto'];
			}

			if($_REQUEST['xls'] == 1){
				$_REQUEST['voImposto'] = $_SESSION['voMnyMovimentoImpostoXls'];
				include_once('view/financeiro/mny_movimento/imposto_xls.php');
			}else{
				include_once('view/financeiro/mny_movimento/tabela_imposto.php');
			}
			//header("Location: http://200.98.201.88/webmoney/relatorios/?rel=");
			exit();
		}
	}

	public function exportaXls(){
		$oFachadaView = new FachadaViewBD();
		switch($_REQUEST['nTipoRel']){
			case 1:
				$sOP = $_REQUEST['sOP'];
				$_REQUEST['voMnyMovimentoImposto'] = $oFachadaView->recuperarTodosMnyMovimentoImpostoRelatorio($sCampos,$sComplemento);
				include_once("view/financeiro/mny_conciliacao/reconciliacao_excel.php");
				exit();
			break;
			case 2:
				$_REQUEST['voVContasPagar'] = $oFachadaView->recuperarTodosVContasPagarPorPeriodoUnidade($_REQUEST['dDataInicial'], $_REQUEST['dDataFinal'],$_REQUEST['fUniCodigo'],$_SESSION['oEmpresa']->getEmpCodigo());

                if($_REQUEST['voVContasPagar']){
				    include_once("view/financeiro/mny_movimento/rel_contas_pagar_xls.php");
                }else{
                    $_SESSION['sMsg'] = "Nenhum resultado encontrado para a pesquisa";
                    include_once("view/index.php");
                }

				exit();
			break;
		}
 	}

	public function carregaEmpresa(){
		$oFachadaSys = new FachadaSysBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$_REQUEST['oMnyMovimento'] = $oFachadaFinanceiro->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);
		$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();

		include_once('view/financeiro/mny_movimento/altera_empresa_ajax.php');
		exit();
	}

	public function alteraEmpresa(){
		$oFachada = new FachadaFinanceiroBD();
		$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($_REQUEST['fMovCodigo']);

		$oMnyMovimento->setEmpCodigo($_REQUEST['fEmpCodigo']);

		//print_r($oMnyMovimento->getEmpCodigo());
		//die();
		if($_REQUEST['sOP']=='Alterar'){
			if($oFachada->alterarMnyMovimento($oMnyMovimento)){
				$_SESSION['sMsg'] = "Empresa altera com sucesso";
			}else{
				$_SESSION['sMsg'] = "N&atil;o foi possivel alterar a empresa";
			}
		}
		$sHeader = "?bErro=1&action=MnyMovimento.preparaLista";
 		header("Location: ".$sHeader);
/*		include_once('view/financeiro/mny_movimento/altera_empresa_ajax.php');
		exit();*/
	}

    public function processaFormularioReconciliacao(){

            $oFachada = new FachadaFinanceiroBD();
            $oFachadaSys = new FachadaSysBD();


          //print_r($_POST);die();


            $nErro=0;
            $vCampoChave = array();
            $arquivos = $_FILES["arquivo"];
            $aContaData = explode('||',$_POST['fSaldo']);
            $vFA = explode("||",$_REQUEST['fFA']);
            $_POST['fMovCodigo'] = $vFA[0];
            $_POST['fMovItem'] = $vFA[1];

            $nCodArquivo = $_REQUEST['fCodArquivo'];

            $oMnyMovimento = $oFachada->recuperarUmMnyMovimento($vFA[0]);
            $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($vFA[0],$vFA[1]);

            $oMnyMovimento->setMovDocumento($_POST['fMovDocumento']);
            $oMnyMovimentoItem->setTipDocCodigo($_POST['fTipDocCodigo']);

            //print_r($oMnyMovimento);die();
            $oFachada->alterarMnyMovimento($oMnyMovimento);
            $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem);

            $vIdArquivo = array();

            if($arquivos['name'] != ""){
                    $vFA = explode("||",$_REQUEST['fFA']);
                    $_POST['fMovCodigo'] = $vFA[0];
                    $_POST['fMovItem'] = $vFA[1];
                    $nCodContratoDocTipo = $_REQUEST['fCodContratoDocTipo'];//11;
                    $_POST['fDescricao'] = "Comprovante atualizado conforme orientação da Reconciliação";

                    $nomeArquivo = 'FA_9_'.$vFA[0];

                    $tipoArquivo = $arquivos['type'];
                    //$nCodContratoDocTipo = 9;

                    $tamanhoArquivo = $arquivos['size'];
                    $nContratoCodigo = 'NULL';
                    $nCodConciliacao = 'NULL';
                    $nMovCodigo = $vFA[0];
                    $nMovItem = $vFA[1];

                    $oSysArquivo =  $oFachadaSys->recuperarUmSysArquivo($_REQUEST['fCodArquivo']);
                    if($oSysArquivo){

                        $oSysArquivo->setTamanho($arquivos['size']);
                        $oSysArquivo->setRealizadoPor($_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i:s'));

                    }else{


                        $oSysArquivo = $oFachadaSys->inicializarSysArquivo($_POST['fCodArquivo'],$nomeArquivo,$blobArquivo,$tipoArquivo,$nCodContratoDocTipo,$tamanhoArquivo,date('Y-m-d H:i:s'),mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),1,'NULL','NULL', $_POST['fMovCodigo'], $_POST['fMovItem']);
                    }






                    $nomeTemporario = $arquivos['tmp_name'];
                   // $vPermissao = array("application/pdf");
                    //strtok($arquivos['type'], ";");




                    //print_r($oSysArquivo);
                    //die();


                    if($_FILES["arquivo"]["tmp_name"]){
                       $Arquivo = $_FILES["arquivo"]["tmp_name"];
                       $Tipo = $_FILES["arquivo"]["type"];

                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");

                        $blobArquivo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                        $blobArquivo = addslashes(fread($blobArquivo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                        $oSysArquivo->setArquivo($blobArquivo);
                        unlink("c:\\wamp\\tmp\\arquivo.pdf");

                        $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                        $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];

                        if($_POST['fCodArquivo']){
                            $oFachadaSys->alterarSysArquivo($oSysArquivo);
                             //$oMnyMovimentoItemProtocolo = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],18,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                             //$vIdMovimentoItemProtocolo = $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                        }else{
                            if(!$nIdArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo)){
                                $nErro++;
                                $_SESSION['sMsg'] = "Erro 6: não inserido no banco";
                            }
                        }
                    }
                }



        $_POST['fDescricao'] = "ENCAMINHADO PARA RECONCILIAÇÃO";
        $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],18,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
        $ndMovimentoItemProtocolo  = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);



        if($nErro ==0){
             $_SESSION['sMsg'] = "Comprovante atualizado com sucesso!";
        }else{
            $_SESSION['sMsg'] .= "Ocorreu [". $nErro ."] problema(s) na inserção do comprovante! Tente novamente!";
        }
        $sHeader = "?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=".$_REQUEST['fFA'];

        header("Location: ".$sHeader);
        exit();
    }


    public function preparaListaPendencias(){

            $oFachadaPermissao = new FachadaPermissaoBD;
            $oFachadaFinanceiro = new FachadaFinanceiroBD;


            $voTramitacao2 = $oFachadaPermissao->recuperarTodosAcessoGrupoTramitacao();
            foreach($voTramitacao2 as $oTramita){
                $voTramita[] = $oTramita->getCodGrupoUsuario();
            }

            if((in_array($_SESSION['Perfil']['CodGrupoUsuario'],$voTramita)) || $_SESSION['Perfil']['CodGrupoUsuario']==16){
                $vAportePagina = array(8,10,16);
                 if (in_array($_SESSION['Perfil']['CodGrupoUsuario'], $vAportePagina)){

                    $voSolicitacaoAportePendencias = $oFachadaFinanceiro->recuperarTodosMnySolicitacaoAportePorPendencia(0);
                    $voSolicitacaoAporteRealizados = $oFachadaFinanceiro->recuperarTodosMnySolicitacaoAportePorPendencia(1);
                   // echo "1";
                 }else{

                     $vVetudo = array(7,11,12,13);
                    if(in_array($_SESSION['Perfil']['CodGrupoUsuario'], $vVetudo)){
                        $voPendencias = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPendenciasPorGrupoUsuario($_SESSION['Perfil']['CodGrupoUsuario']);
                        //$voRealizados = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoRealizados();
                        //echo "2";
                    }else{

                        $nCodUnidade = $_SESSION['Perfil']['CodUnidade'];
                        $voPendencias = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoPorMovimentoItemPendenciasPorGrupoUsuarioUnidade($_SESSION['Perfil']['CodGrupoUsuario'],$nCodUnidade);
                        //$voRealizados = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemTramitacaoRealizadosPorUnidade($nCodUnidade);
                        //echo "3";
                    }
                }
            }

        $_REQUEST['voTramita'] = $voTramita;
        $_REQUEST['voSolicitacaoAportePendencias'] = $voSolicitacaoAportePendencias;
        $_REQUEST['voSolicitacaoAporteRealizados'] = $voSolicitacaoAporteRealizados;
        $_REQUEST['voPendencias'] = $voPendencias;
        $_REQUEST['$nCodUnidade'] = $nCodUnidade;
        $voPendencias = $_REQUEST['voPendencias'];

        include_once('view/financeiro/mny_movimento/pendencias.php');
        exit();

    }

 }
 ?>
