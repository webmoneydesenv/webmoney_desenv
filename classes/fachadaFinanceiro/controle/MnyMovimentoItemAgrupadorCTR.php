<?php
 class MnyMovimentoItemAgrupadorCTR implements IControle{

 	public function MnyMovimentoItemAgrupadorCTR(){

 	}

    public function preparaFormulario(){

        $oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();

 		$oMnyMovimento = false;
 		$oMnyMovimentoItem = false;


		if($_REQUEST['sOP'] != "Cadastrar" ){
            $oMovimentoItem = explode("||",$_REQUEST['nIdMnyMovimentoAgrupador']);
 			$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($oMovimentoItem[0]);
 			$oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($oMovimentoItem[0],$oMovimentoItem[1]);
            $_REQUEST['voMnyMovimentoItemAgrupado'] = $oFachada->recuperarTodosMnyMovimentoItemAgrupadorPorMovimentoItemAgrupador($oMovimentoItem[0],1);


 		}

 		$_REQUEST['oMnyMovimento'] = ($_SESSION['oMnyMovimento']) ? $_SESSION['oMnyMovimento'] : $oMnyMovimento;
 		$_REQUEST['oMnyMovimentoItem'] = ($_SESSION['oMnyMovimentoItem']) ? $_SESSION['oMnyMovimentoItem'] : $oMnyMovimentoItem;
 		unset($_SESSION['oMnyMovimento']);
 		unset($_SESSION['oMnyMovimentoItem']);

		$sGrupo = '7';//Centro de Negócio

		$_REQUEST['voCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

        //$_REQUEST['fConCodigo']=286;
        $sGrupo = '2.4';//Conta

		$_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		$_REQUEST['nCodPessoa'] = $_REQUEST['fPesCodigo'];
		$nTipoContrato = 4;
		$_REQUEST['nCodPessoa'] = $_REQUEST['fPesCodigo'];


		$sGrupo = '4'; // Tipo de doc
		$_REQUEST['voMnyTipoDoc'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '7'; // Centro de Negocio
		$_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		//$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());
        $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasCENTRALObjeto();

		$sGrupo = '10'; // Setor
		$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$sGrupo = '11'; // Forma de Pag
		$_REQUEST['voMnyFormaPagamento'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

	    $nConta = '156'; // Caixa
		$_REQUEST['oMnyConta'] = $oFachada->recuperarUmMnyPlanoContas($nConta);

		$nTipoAceite = '169'; // Tipo de aceite
        $_REQUEST['oTipoAceite'] = $oFachada->recuperarUmMnyPlanoContas($nTipoAceite);
        $nTipoLancamento = '188';//Tipo de Lançamento - Pagar
		$_REQUEST['oTipoLancamento'] = $oFachada-> recuperarUmMnyPlanoContas($nTipoLancamento);

		$sGrupo = '6'; // Custo
		$_REQUEST['voMnyCusto'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);

		$nCenCodigo = 156; // Centro de Custo
		$_REQUEST['oMnyCentroCusto'] = $oFachada->recuperarUmMnyPlanoContas($nCenCodigo);

        if($_REQUEST['sOP'] == "Alterar"){

            if($oFachada->recuperarUmMnyAporteItemPorMovimento($_REQUEST['oMnyMovimento']->getMovCodigo())){
                $_REQUEST['nAporte']=1;
            }else{
               $_REQUEST['nAporte']=2;
            }

            //print_r($nTipoContrato);die();
            $_REQUEST['fContratoTipo'] = $nTipoContrato;

			$oMnyConciliacao  = $oFachada->recuperarUmMnyConciliacaoPorMovimentoItem($_REQUEST['nIdMnyMovimento'], $_REQUEST['nMovItem']);

			if($oMnyConciliacao){
				$_REQUEST['Conciliado'] = 1;
			}else{
				$_REQUEST['Conciliado'] = 0;
			}
		}


		   $_REQUEST['nTipo'] = 188;

            if($nTipoContrato != 4 && $_REQUEST['sTipoLancamento'] != "Receber"){
                // desabilitar Alteração
                $_REQUEST['sSomenteLeitura'] = " readonly='readonly' ";
            }else{
                $_REQUEST['sSomenteLeitura'] = "";
            }

            $_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			include_once("view/financeiro/mny_movimento_item_agrupador/insere_altera.php");
			exit();


		//$_REQUEST['oVMovimentoConciliacaoTotal'] = $oFachada->recuperarTodosVMovimentoConciliacaoTotalPorData($dDataConciliacao);


 		if($_REQUEST['sOP'] == "Detalhar"){
 			include_once("view/financeiro/mny_movimento_item/detalhe.php");
		}
		exit();



    }

   public function gravaLinha(){
			$oFachadaFinanceiro = new FachadaFinanceiroBD();
			$oFachadaView = new FachadaViewBD();
			$nMovItem = explode(".", $_REQUEST['fMovimentoItem']);
			$_REQUEST['idCampo'] = $_GET['idCampo'];
			$nMovCodigo = $nMovItem[0];
			$nMovItem = $nMovItem[1];
            $nCodPessoa = $_GET['nCodPessoa'];
            unset($_SESSION['sMsg']);
             $_SESSION['sMsg'] = $oFachadaFinanceiro->recuperarUmMnyMovimentoItemAgrupadoVerificacao($nCodPessoa,$nMovCodigo,$nMovItem);

            if($_REQUEST['rep']==1)
                $_SESSION['sMsg'] ="Este item j&aacute; foi inserido.";

            $_REQUEST['oMovimentoItemLinha'] = $oFachadaView->recuperarUmVMovimentoItemAgrupadoLinha($nCodPessoa,$nMovCodigo,$nMovItem);
            include_once("view/financeiro/mny_movimento_item_agrupador/campo_ajax.php");

			exit();
	}


 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaView = new  FachadaViewBD();
 		//$voMnyConciliacao = $oFachada->recuperarTodosMnyConciliacao();

		$voDataConciliacao = $oFachada->recuperarTodosMnyConciliacaoDataConsolidado();

		if($voDataConciliacao){
			foreach($voDataConciliacao as $dDataConciliacao){
				 $voConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalConsolidadoPorData($dDataConciliacao->getDataConciliacao());
			}
		}
		$_REQUEST['voMnyConciliacao'] = $voConciliacao;
 		include_once("view/financeiro/mny_conciliacao/index_ajax.php");
 		exit();
 	}


  	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
			$sLogado = date('d/m/Y') ." ". $_SESSION['oUsuarioImoney']->getLogin();
			if($sOP =='Cadastrar'){
				$_POST['fMovDataInclusao']= date('d/m/Y');
				$_POST['fMovDataEmissao']= $_POST['fMovDataInclusao'];
			}

			//$_POST['fEmpCodigo']= $_SESSION['oEmpresa']->getEmpCodigo();

			//adiciona o contrato no movimento...
			if($sOP == 'Alterar')
				$oMnyMovimento = $oFachada->recuperarUmMnyMovimento($_REQUEST['fMovCodigoAgrupador']);
			else
                $oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigoAgrupador'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesgCodigo'],$_POST['fSetCodigo'],addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']);

			$_SESSION['oMnyMovimento'] = $oMnyMovimento;

			if($sOP =='Cadastrar'){
				$oMnyMovimento->setMovInc($sLogado);
	        }elseif($sOP =='Alterar'){
				$oMnyMovimento->setMovAlt($sLogado);
			}

            $nDias = $_REQUEST['fPeriodicidade'];
			$nParcelas = $_REQUEST['fMovParcelas'];

 			$_SESSION['oMnyMovimentoItem'] = $oMnyMovimentoItem;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 		//	$oValidate->add_date_field("Data de Inclus&atilde;o", $oMnyMovimento->getMovDataInclusao(), "date", "y");
			$oValidate->add_date_field("Data de Emiss&atilde;o", $oMnyMovimento->getMovDataEmissao(), "date", "y");
			$oValidate->add_number_field("Unid Neg&oaccute;cio", $oMnyMovimento->getNegCodigo(), "number", "y");
            $oValidate->add_number_field("Unidade Custo", $oMnyMovimento->getCusCodigo(), "number", "y");
			$oValidate->add_number_field("Centro", $oMnyMovimento->getCenCodigo(), "number", "y");
			$oValidate->add_number_field("Unidade", $oMnyMovimento->getUniCodigo(), "number", "y");
			$oValidate->add_number_field("Pessoa", $oMnyMovimento->getPesCodigo(), "number", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getConCodigo(), "number", "y");
			$oValidate->add_number_field("Setor", $oMnyMovimento->getSetCodigo(), "number", "y");
			//$oValidate->add_text_field("Obs", $oMnyMovimento->getMovObs(), "text", "y");
			$oValidate->add_text_field("Incluído por", $oMnyMovimento->getMovInc(), "text", "y");
			if($sOP == 'Alterar')
				$oValidate->add_text_field("MovAlt", $oMnyMovimento->getMovAlt(), "text", "y");
			//$oValidate->add_number_field("Contrato", $oMnyMovimento->getMovContrato(), "number", "y");
			$oValidate->add_text_field("Documento", $oMnyMovimento->getMovDocumento(), "text", "y");

			if($sOP == 'Cadastrar'){
				$oValidate->add_number_field("Parcelas", $oMnyMovimento->getMovParcelas(), "number", "y");
			}


			$oValidate->add_number_field("EmpCodigo", $oMnyMovimento->getEmpCodigo(), "number", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				  $sHeader = "?bErro=1&action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=".$sOP."&nIdMnyMovimentoAgrupador=".$_POST['fMovCodigoAgrupador']."&sTipoLancamento=".$_REQUEST['sTipoLancamento'];
                    header("Location: ".$sHeader);
                    die();

 			}
 		}


		$sPrefixo = "A_Pagar";

		switch($sOP){
 			case "Cadastrar":


		  if($nMovCodigo = $oFachada->inserirMnyMovimento($oMnyMovimento)){

				$nDias = $_REQUEST['fPeriodicidade'];
				$nParcelas = ($_REQUEST['fMovParcelas']) ? $_REQUEST['fMovParcelas']  : 1;


				$dCompetencia = $_POST['fCompetencia'];
				$dMesAno = $_POST['fCompetencia'];
				$dDataVencimento = $_POST['fMovDataVencto'];
                $nCaixinha = ($_POST['fCaixinha']) ? $_POST['fCaixinha'] : '0';
                $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
                $sFaOrigem = 'NULL';

                $nValorLiquidoFinal = 0;
              if($_REQUEST['fFA']){
                  foreach($_REQUEST['fFA'] as $key => $value){
                      $vFA = explode('||',$value);
                        $oMnyMovimentoItemAgrupador = $oFachada->inicializarMnyMovimentoItemAgrupador($nMovCodigo,1,$vFA[0],$vFA[1]);
                        if($oFachada->inserirMnyMovimentoItemAgrupador($oMnyMovimentoItemAgrupador)){
                          $nValorLiquidoFinal = $nValorLiquidoFinal + $vFA[2];
                            $oMnyMovimentoItemProtocoloFilho = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$vFA[0],$vFA[1],13,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Agrupado.[". $nMovCodigo .".1]",MB_CASE_UPPER, "UTF-8"),1);
                            $nCodProtocoloMovimentoItem1 = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloFilho);
                            $oMnyMovimentoItemProtocoloFilho->setDeCodTipoProtocolo('12');
                            $oMnyMovimentoItemProtocoloFilho->setDescricao("Movimento Agrupado [". $nMovCodigo .".1] finalizado");
                            $nCodProtocoloMovimentoItem2 = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloFilho);

                            $oMnyMovimentoItemFILHO = $oFachada->recuperarUmMnyMovimentoItem($vFA[0],$vFA[1]);
                            $oMnyMovimentoItemFILHO->setFinalizado('1');
                            $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemFILHO);


                        }else{
                            $oFachada->excluirTodosMnyMovimentoItemAgrupadorPorMovimentoItem($nMovCodigo,1);
                            $oFachada->excluirMnyMovimentoFisicamente($nMovCodigo);
                            $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
                            $sHeader = "?bErro=1&action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=" .$sOP;
                            exit();
                        }
                  }
                     $_POST['fMovValorParcela'] = number_format($nValorLiquidoFinal , 2, ',', '.');

                        $oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($nMovCodigo,1,$dDataVencimento,$_POST['fMovDataPrev'],$_POST['fMovValorParcela'],NULL,NULL,$_POST['fFpgCodigo'],$_POST['fTipDocCodigo'],NULL,$_POST['fMovDataInclusao'],NULL,$_POST['fTipAceCodigo'],$dCompetencia,1,0,1,$nCaixinha,$_POST['FaOrigem'],$nNf,$nConciliado);

                            //TRAMITAÇÂO...
                            $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$nMovCodigo,1,13,7,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Agrupado Lançado.",MB_CASE_UPPER, "UTF-8"),1);

                            $oMnyMovimentoItem->setMovCodigo($nMovCodigo);
                            $oMnyMovimentoItemProtocolo->setMovCodigo($nMovCodigo);

                            if($oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem)){
                                $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                            }else{
                                $oFachada->excluirTodosMnyMovimentoItemAgrupadorPorMovimentoItem($nMovCodigo,1);
                                $oFachada->excluirMnyMovimentoFisicamente($nMovCodigo);
                                $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
                                $sHeader = "?bErro=1&action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=" .$sOP;
                                exit();
                            }

                            unset($_SESSION['oMnyMovimentoItem']);

                        unset($_SESSION['oMnyMovimento']);

                        $_SESSION['sMsg'] = "Movimento [".$nMovCodigo."] inserido com sucesso!";

                        $sHeader = "?action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimentoAgrupador=".$nMovCodigo."||1";


                } else{
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
                        $sHeader = "?bErro=1&action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
                }
          }else{
              //apagar via procedure tudo que foi inserido equivocadamente
              $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Movimento!";
               $sHeader = "?bErro=1&action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=" .$sOP. "&sTipoLancamento=" . $_REQUEST['sTipoLancamento'];
          }

 			break;
 			case "Alterar":
                // FLAVIO SÓ ALTERA SE FOR DESPESA DE CAIXA
                // RESTO DO MUNDO ALTERA SE NAO TIVER NO APORTE

                //Financeiro OBRA , Nota Fiscal...
                //Usado no Caixinha...

                $nValorLiquidoParcial = 0;
                $nErro = 0;
                $nCaixinha = ($_POST['fCaixinha'] )?$_POST['fCaixinha']:'0';
                $oMnyMovimentoItemAgrupador = $oFachada->recuperarUmMnyMovimentoItemPorMovimento($_REQUEST['fMovCodigoAgrupador'])[0];


                $oMnyMovimentoItemAgrupador->setMovDataPrevBanco($_POST['fMovDataPrev']);
                $oMnyMovimentoItemAgrupador->setMovDataVenctoBanco($_POST['fMovDataVencto']);
                $oMnyMovimentoItemAgrupador->setCompetenciaBanco($_POST['fCompetencia']);
                $oMnyMovimentoItemAgrupador->setFpgCodigo($_POST['fFpgCodigo']);
                $oMnyMovimentoItemAgrupador->setTipDocCodigo($_POST['fTipDocCodigo']);


                //print_r(count($_REQUEST['fFA']));
                //die();


                for($i=0;count($_REQUEST['fFA']) >$i; $i++){

                        $nFA = explode('||',$_REQUEST['fFA'][$i]);


                        $oMnyMovimentoItemAgrupadorItem = $oFachada->inicializarMnyMovimentoItemAgrupador($_REQUEST['fMovCodigoAgrupador'],1,$nFA[0],$nFA[1]);
                        if($oFachada->inserirMnyMovimentoItemAgrupador($oMnyMovimentoItemAgrupadorItem)){

                            $nValorLiquidoParcial +=  $nFA[2];
                            $oMnyMovimentoItemProtocoloFilho = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$nFA[0],$nFA[1],13,12,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("Movimento Agrupado.[". $_REQUEST['fMovCodigoAgrupador'] .".1]",MB_CASE_UPPER, "UTF-8"),1);
                            $nCodProtocoloMovimentoItem1 = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloFilho);
                            $oMnyMovimentoItemProtocoloFilho->setDeCodTipoProtocolo('12');
                            $oMnyMovimentoItemProtocoloFilho->setDescricao("Movimento Agrupado [". $_REQUEST['fMovCodigoAgrupador'] .".1] finalizado");
                            $nCodProtocoloMovimentoItem2 = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloFilho);


                            $oMnyMovimentoItemFILHO = $oFachada->recuperarUmMnyMovimentoItem($nFA[0],$nFA[1]);
                            $oMnyMovimentoItemFILHO->setFinalizado('1');
                            $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemFILHO);


                        }else{

                            $nErro++;
                        }


                }

                //print_r( $oMnyMovimentoItemAgrupador);
                //die();

                $oMnyMovimentoItemAgrupador->setMovValorParcela($oMnyMovimentoItemAgrupador->getMovValorParcela() + $nValorLiquidoParcial);



                if($nErro > 0){

                    die('!funfou');
                }


                if($oFachada->alterarMnyMovimento($oMnyMovimentoItemAgrupador)){
                        $_SESSION['sMsg'] = "Movimento [".$oMnyMovimento->getMovCodigo()."] alterado com sucesso!";
                        $sHeader = "?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_REQUEST['fMovCodigo']."&fContratoTipo=".$_REQUEST['fContratoTipo'];
                } else {
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Movimento!";
                        $sHeader = "?bErro=1&action=MnyMovimento.preparaFormulario&sOP=".$sOP."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimento=".$_POST['fMovCodigo']."";
                }


                 $sHeader = "?action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimentoAgrupador=".$_REQUEST['fMovCodigoAgrupador']."||1";



 			break;

 			case "Excluir":
			    $bResultado = true;

                $nChave = explode("||",$_REQUEST['fIdMnyMovimento']);

                 $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($nChave[2],$nChave[3]);

                 $oMnyMovimentoItem->setFinalizado('0');



                 $oMnyMovimentoItemAgrupador = $oFachada->recuperarUmMnyMovimentoItem($nChave[0],$nChave[1]);
                 $oMnyMovimentoItemAgrupador->setMovValorParcela($oMnyMovimentoItemAgrupador->getMovValorParcela() - $oMnyMovimentoItem->getMovValorParcela());




                 $bResultado = $oFachada->excluirMnyMovimentoItemAgrupador($nChave[0],$nChave[1],$nChave[2],$nChave[3]);


 				if($bResultado){
 					$_SESSION['sMsg'] = "Movimento(s) exclu&iacute;do(s) com sucesso!";

                     $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemAgrupador);
                     if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){

                          $oFachada->excluirMnyMovimentoItemProtocoloAGRUPADO($nChave[2],$nChave[3],$nChave[0]);
                     }

 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Movimento!";
 				}
                $sHeader = "?action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimentoAgrupador=".$nChave[0]."||1";

 			break;
 		}

 		header("Location: ".$sHeader);
 	}



	function carregaTipo(){
		$oFachada = new FachadaFinanceiroBD();
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodTipoCaixa = $_REQUEST['fCenCodigo'];
			$nCodUnidade = $_REQUEST['fUniCodigo'];

			//usado na pagina para abrir a conciliacao
			$_REQUEST['nTipoOperacao'] = $_REQUEST['nTipoOperacao'];

			(isset($_REQUEST['fUniCodigo'])) ? $nCodTipoCaixa = 1 : "" ;

			$_REQUEST['fCenCodigo2'] = $nCodTipoCaixa;
			$_REQUEST['voMnyCartao'] = $oFachada->recuperarTodosMnyCartao();
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
			if($_REQUEST['fDataConciliacao']){
				$oData = DateTime::createFromFormat('d/m/Y', $_REQUEST['fDataConciliacao']);
				$dDataConciliacao = $oData->format('Y-m-d');
			}
			$_REQUEST['voMnyContaCorrenteUnidade'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);

			$_REQUEST['nCodContaCorrente'] = $_REQUEST['nCodContaCorrente'];

			$_REQUEST['dData'] = $_REQUEST['fDataConciliacao'];

			include_once("view/financeiro/mny_conciliacao/tipo_caixa_ajax.php");

			exit();
	}

	function carregaContaRelatorio(){
		$FachadaFinanceiroBD = new FachadaFinanceiroBD();
		$nCodUnidade = $_REQUEST['fUniCodigo'];
		$_REQUEST['voMnyContaCorrenteUnidade'] = $FachadaFinanceiroBD->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);
		include_once("view/financeiro/mny_conciliacao/conta_relatorio_ajax.php");
		exit();
	}

	/*
	function carregaContas(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nCodUnidade = $_REQUEST['fCodUnidade'];

			$_REQUEST['fCenCodigo2'] = $nCodTipoCaixa;
			$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($nCodUnidade);
			// parei aqui
			include_once("view/financeiro/mny_conciliacao/tipo_caixa_ajax.php");
			exit();
	}
	*/

	//carrega apenas os itens correspondentes ao movimento selecionado...
	function carregaItem(){
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

			$nMovCodigo = $_REQUEST['fMovCodigo'];

			$_REQUEST['voMnyMovimentoItem'] = $oFachada->recuperarTodosMnyMovimentoItemPorMovimento($nMovCodigo);
			include_once("view/financeiro/mny_conciliacao/item_ajax.php");
			exit();
	}

	function carregaTabela($dData="",$nConta="",$nCodUnidade=""){

		$oFachadaView = new FachadaViewBD();
		$oFachada = new FachadaFinanceiroBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
				$dDataBanco = ($dData) ? $dData : $_REQUEST['fDataConciliacao'];
				if($dDataBanco ){
					$oData = DateTime::createFromFormat('d/m/Y', $dDataBanco );
					$dDataConciliacaoBanco = $oData->format('Y-m-d');
				}

				$_REQUEST['fCodUnidade'] = ($nCodUnidade) ? $nCodUnidade : $_REQUEST['fCodUnidade'] ;
				$_REQUEST['fCodContaCorrente'] = ($nConta) ? $nConta : $_REQUEST['fCodContaCorrente'];

                $oMnyContaCorrente = $oFachada->recuperarUmMnyContaCorrente($_REQUEST['fCodContaCorrente']);

                $_REQUEST['nMaiorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMaiorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);
                $_REQUEST['nMenorOrdem'] = $oFachadaView->recuperarUmVMovimentoConciliacaoPorMenorOrdem(($dDataConciliacaoBanco) ? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente']);

                    $_REQUEST['voMnyVMovimentoConciliacao'] = $oFachadaView->recuperarTodosVMovimentoConciliacaoCaixinhaPorDataUnidadeConta(($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'), $_REQUEST['fCodUnidade'], $_REQUEST['fCodContaCorrente']);



				//$oFachada->recuperarUmMnySaldoDiarioPorUnidadeContaData($_REQUEST['fCodUnidade'],$_REQUEST['fCodContaCorrente'], ($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'));
				$oFachada->recuperarUmMnySaldoDiarioAnteriorPorContaData($_REQUEST['fCodContaCorrente'], ($dDataConciliacaoBanco)? $dDataConciliacaoBanco : date('Y-m-d'));
				if($_REQUEST['voMnyVMovimentoConciliacao']){
					if($_REQUEST['voMnyVMovimentoConciliacao'][0]->getConsolidado() == 1){
						$_REQUEST['consolidado'] = 1;
					}else{
						$_REQUEST['consolidado'] = 0;
					}
				}else{
						//dia nao util...
						$_REQUEST['consolidado'] = 0;
				}

				//if(!$_REQUEST['voMnyVMovimentoConciliacao']){
					$oSaldoConta = $oFachada->recuperarUmMnySaldoDiarioPorTipoConta($_REQUEST['fCodContaCorrente'], $dDataConciliacaoBanco);
					//print_r($oSaldoConta);
					//die();
					if($oSaldoConta){

						if($dDataConciliacaoBanco == $oSaldoConta->getData()){
							switch($oSaldoConta->getTipoSaldo()){
								case 1:
								  $_REQUEST['nTipoConciliacao'] = 1;
								break;
								case 2:
								  $_REQUEST['nTipoConciliacao'] = 2;
								break;
								case 3:
								  $_REQUEST['nTipoConciliacao'] = 3;
								break;
							}
						}else{
								  $_REQUEST['nTipoConciliacao'] = 0;

                                   $nAno = explode('/',$_REQUEST['fDataConciliacao']);
								  // $_REQUEST['dDataUltimaConciliacao'] = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrente($_REQUEST['fCodContaCorrente'])->getDataFormatado();
                                  $_REQUEST['dDataUltimaConciliacao'] = $oFachada->recuperarUmMnySaldoDiarioUltimaDataPorContaCorrentePorAno($_REQUEST['fCodContaCorrente'],$nAno[2])->getDataFormatado();
						}


					$_REQUEST['oMnySaldoDiario'] = $oSaldoConta;
				}
				//print_r($_REQUEST['nTipoConcialicao']);
				//die();
				$nTipoCaixa = 1;
			  	$_REQUEST['oSaldoConta']  = $oFachada->recuperarUmMnySaldoDiarioPorContaCorrente($dDataConciliacaoBanco, $_REQUEST['fCodContaCorrente']);


                //include_once("view/financeiro/mny_conciliacao/tabela_ajax.php");
				if($oMnyContaCorrente->getCcrConta() == 999){
                    $_REQUEST['nCaixinha']=1;
                    include_once("view/financeiro/mny_conciliacao/lista_ajax_caixinha.php");
                }else{
                    $_REQUEST['nCaixinha']=0;
                    include_once("view/financeiro/mny_conciliacao/lista_ajax.php");
                }
				exit();
	}

	public function carregaRelatorio(){
		$oFachadaView = new FachadaViewBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
		$_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo(8);
		$_REQUEST['nTipo'] = $_REQUEST['nTipo'];
		$_REQUEST['nCodEmpresa'] = $_SESSION['oEmpresa']->getEmpCodigo();
		if($_REQUEST['nTipo'] == 3){
			$voCredor = $oFachadaView->recuperarTodosVPessoaGeralFormatada();
			$_REQUEST['voCredor'] = $voCredor;
		}
		include_once('view/financeiro/mny_conciliacao/relatorio_ajax.php');
		exit();
	}




 }

 ?>
