<?php
 class MnyContratoDocTipoCTR implements IControle{
 
 	public function MnyContratoDocTipoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyContratoDocTipo = $oFachada->recuperarTodosMnyContratoDocTipo();
 
 		$_REQUEST['voMnyContratoDocTipo'] = $voMnyContratoDocTipo;
 		
 		
 		include_once("view/financeiro/mny_contrato_doc_tipo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyContratoDocTipo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContratoDocTipo = ($_POST['fIdMnyContratoDocTipo'][0]) ? $_POST['fIdMnyContratoDocTipo'][0] : $_GET['nIdMnyContratoDocTipo'];
 	
 			if($nIdMnyContratoDocTipo){
 				$vIdMnyContratoDocTipo = explode("||",$nIdMnyContratoDocTipo);
 				$oMnyContratoDocTipo = $oFachada->recuperarUmMnyContratoDocTipo($vIdMnyContratoDocTipo[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyContratoDocTipo'] = ($_SESSION['oMnyContratoDocTipo']) ? $_SESSION['oMnyContratoDocTipo'] : $oMnyContratoDocTipo;	
 		unset($_SESSION['oMnyContratoDocTipo']);
 		$_REQUEST['voMnyContratoTipo'] = $oFachada->recuperarTodosMnyContratoTipo();

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_contrato_doc_tipo/detalhe.php");
 		else
 			include_once("view/financeiro/mny_contrato_doc_tipo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyContratoDocTipo = $oFachada->inicializarMnyContratoDocTipo($_POST['fCodContratoDocTipo'],$_POST['fCodContratoTipo'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),$_POST['fObrigatorio'],$_POST['fAcaoInicial'],$_POST['fItem'],$_POST['fAtivo']);
 			
			
			$_SESSION['oMnyContratoDocTipo'] = $oMnyContratoDocTipo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Descricao", $oMnyContratoDocTipo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Obrigatorio", $oMnyContratoDocTipo->getObrigatorio(), "number", "y");
			$oValidate->add_number_field("Acao inicial", $oMnyContratoDocTipo->getAcaoInicial(), "number", "y");
			$oValidate->add_number_field("Tipo de Contrato", $oMnyContratoDocTipo->getCodContratoTipo(), "number", "y");
			$oValidate->add_number_field("Ativo", $oMnyContratoDocTipo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyContratoDocTipo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoDocTipo=".$_POST['fCodContratoDocTipo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyContratoDocTipo($oMnyContratoDocTipo)){
 					unset($_SESSION['oMnyContratoDocTipo']);
 					$_SESSION['sMsg'] = "Tipo de Documento do Contrato/Item inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoDocTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Tipo de Documento do Contrato/Item!";
 					$sHeader = "?bErro=1&action=MnyContratoDocTipo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyContratoDocTipo($oMnyContratoDocTipo)){
 					unset($_SESSION['oMnyContratoDocTipo']);
 					$_SESSION['sMsg'] = "Tipo de Documento do Contrato/Item alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoDocTipo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Documento do Contrato/Item!";
 					$sHeader = "?bErro=1&action=MnyContratoDocTipo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoDocTipo=".$_POST['fCodContratoDocTipo']."";
 				}
 			break;
 			case "Excluir":
                			$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContratoDocTipo']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyContratoDocTipo($sCampoChave);\n");
				}
 				if($bResultado){
 					$_SESSION['sMsg'] = "Tipo de Documento do Contrato/Item(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoDocTipo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Tipo de Documento do Contrato/Item!";
 					$sHeader = "?bErro=1&action=MnyContratoDocTipo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
