<?php
 class MnyContratoAditivoCTR implements IControle{
 
 	public function MnyContratoAditivoCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 		$voMnyContratoAditivo = $oFachada->recuperarTodosMnyContratoAditivo();
 		$_REQUEST['voMnyContratoAditivo'] = $voMnyContratoAditivo;
 		//$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();
 		include_once("view/financeiro/mny_contrato_aditivo/index.php");
 
 		exit();
 	
 	}
	
	public function upload($arquivo,$nCodContratoDocTipo,$nCodContrato2,$nCodContratoArquivo2 =''){
				$oFachada = new FachadaFinanceiroBD();
				$i=0;
				$nErro=0;

				$vCampoChave = array();
				$nTotalArquivos=0;
				$arquivos = $arquivo; 
				$nCodContrato = $_POST['fContratoCodigo'];				
				$tamanhoMaximo = 100000000;				
				$nCodContratoArquivo = $nCodContratoArquivo2;//$_REQUEST['fCodContratoArquivo'];
				$nCodContratoDocTipo = $nCodContratoDocTipo;
				$nCodContrato = $nCodContrato2;	
			
				
				for($i = 0 ; count($arquivos['name']) > $i  ;$i++){	
				
					if($arquivos['name'][$i] != ""){
											
					 	
						$oTipoDocumento = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i] );
						$oTipoDocumento = str_replace(" ","-",strtolower( preg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($oTipoDocumento->getDescricao())), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) ));
						// classUpload
						$nomeArquivo =  "Aditivo_" . $nCodContrato . "_" . $oTipoDocumento . "_" . $arquivos['name'][$i];
						
						$tamanhoArquivo = $arquivos['size'][$i];
						$nomeTemporario = $arquivos['tmp_name'][$i];
						$caminho = $_SERVER['DOCUMENT_ROOT'] . "webmoney/arquivos/";
						$vPermissao = array("application/pdf");
						$tipoArquivo = strtok($arquivos['type'][$i], ";");			
					
					//verificacao de erros	
						if($tamanhoArquivo <= 0){				
							$_SESSION['sMsg'] = "Erro 1: Erro no tamanho do arquivo: ".$tamanhoArquivo ." Kb. Tamanho Maximo é ".$tamanhoMaximo." !";
							$nErro++;
						}
						if($tamanhoArquivo > $tamanhoMaximo){
							$_SESSION['sMsg'] = "Erro 2: Tamanho máximo é: ".$tamanhoMaximo." KB!";
							$nErro++;
						}
						if(!is_uploaded_file($nomeTemporario)){
							$_SESSION['sMsg'] = "Erro 3: Ocorreu um erro na transfêrencia do arquivo";
							$nErro++;
						}
						
						
						if (!in_array($tipoArquivo, $vPermissao)){
							$_SESSION['sMsg'] = "Erro 4: Arquivo precisa ser PDF.";;
							$nErro++;
						}
		
						if(move_uploaded_file($nomeTemporario, ($caminho . $nomeArquivo))){
							$sNomeArquivoBD = "arquivos/" . $nomeArquivo;
					
							$oContratoDocTipoItem = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo[$i]);
			
					
					$_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i');
				
				   //   if($nCodContratoArquivo[$i] == "" ){$nCodContratoArquivo[$i] = $nCodContratoArquivo[$i -1];}
					
							if((!(empty($nCodContratoArquivo[$i]))) && ($oContratoDocTipoItem->getItem() == 0)){		
								
								
								$oMnyContratoArquivo = $oFachada->recuperarUmMnyContratoArquivo($nCodContratoArquivo[$i]);	
								
								$vArquivo = explode("/",$oMnyContratoArquivo->getNome());
								
								
								$sNomeCompleto = $_SERVER['DOCUMENT_ROOT'] . "webmoney/" . $oMnyContratoArquivo->getNome();
								unlink($sNomeCompleto);
								$oFachada->excluirMnyContratoArquivo($nCodContratoArquivo[$i]);
							}
							$nCodConciliacao = NULL;
							$oMnyContratoArquivo = $oFachada->inicializarMnyContratoArquivo($_POST['fCodContratoArquivo'],$nCodContrato,$nCodConciliacao, $_POST['fMovCodigo'],$_POST['fMovItem'],$nCodContratoDocTipo[$i],$sNomeArquivoBD,$_POST['fRealizadoPor'],1);
							if(!$nIdContratoArquivo = $oFachada->inserirMnyContratoArquivo($oMnyContratoArquivo)){
								$nErro++;							
							}						
					  }else{	
							$_SESSION['sMsg'] = "Erro 5: Ocorreu um erro na transfêrencia do arquivo";
							$nErro++;		
					   }						
					}
					
				}

	}//fim função	
 
 	public function DocumentoAjax(){
		$oFachada = new FachadaFinanceiroBD();
		$nTipoContrato = 8;			
		$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial($nTipoContrato,1);					
		include_once('view/financeiro/mny_contrato_aditivo/documentos_ajax.php');
	}
 	
	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
 
 		$oMnyContratoAditivo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyContratoAditivo = ($_POST['fIdMnyContratoAditivo'][0]) ? $_POST['fIdMnyContratoAditivo'][0] : $_GET['nIdMnyContratoAditivo'];
 	
 			if($nIdMnyContratoAditivo){
 				$vIdMnyContratoAditivo = explode("||",$nIdMnyContratoAditivo);
 				$oMnyContratoAditivo = $oFachada->recuperarUmMnyContratoAditivo($vIdMnyContratoAditivo[0]);
				$_REQUEST['oMnyContratoPessoa'] = $oMnyContratoAditivo->getMnyContratoPessoa();		
				$_REQUEST['voMnyContratoArquivo'] = $oFachada->recuperarTodosMnyContratoArquivoPorContrato($vIdMnyContratoAditivo[0]);
				$_REQUEST['voMnyContratoDocTipo'] = $oFachada->recuperarTodosMnyContratoDocTipoPorTipoContratoPorAcaoInicial(8,1);
			}
 		}
 		
 		$_REQUEST['oMnyContratoAditivo'] = ($_SESSION['oMnyContratoAditivo'])? $_SESSION['oMnyContratoAditivo'] : $oMnyContratoAditivo ;
 		
		//unset($_SESSION['oMnyContratoAditivo']);
		
		$_SESSION['oMnyContratoAditivo'] = $_SESSION['oMnyContratoAditivo'];
  		if(!$_REQUEST['oMnyContratoPessoa'])
			$_REQUEST['oMnyContratoPessoa'] = $oFachada->recuperarUmMnyContratoPessoa($_REQUEST['fContratoCodigo']);

 		$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
//		$_REQUEST['voMnyContratoPessoa'] = $oFachada->recuperarTodosMnyContratoPessoa();
     	$_REQUEST['oMnyPessoaGeral'] = $oFachada->recuperarUmMnyPessoaGeral($_REQUEST['fPesgCodigo']);
		
 		if($_REQUEST['sOP'] == "Detalhar"){
 			include_once("view/financeiro/mny_contrato_aditivo/detalhe.php");
		}else{	
			include_once("view/financeiro/mny_contrato_aditivo/insere_altera.php");
		}
		
 		exit();
 	
 	}
 
				public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
//anexo_antigo...			
/*			$sNomedoArquivo = $_FILES[fAnexo]['name'];
			
				$nUsuarioId  =  $_REQUEST['fPesCodigo']; 
				$nContratoId = $_POST['fAditivoCodigo'];				
				//$oMnyArquivo = $oFachada->recuperarUmConarquivo($nArquivoId);
				if($sOP == 'Cadastrar')
					$nArquivoId = "";
				
					if($sOP == 'Alterar' || $sOP='Cadastrar'){
					  if($_REQUEST['fAditivoCodigo']){	 
						//$_POST['fAditivoCodigo'] = 'null';
						$sPrefixo = "Aditivo_";						
					  }else{
						$sPrefixo = "Contrato_";						
					  }			  
					}			
				if ($sNomedoArquivo){
					
					$oUpload = new Upload(array("application/pdf"),$max_file = 12000000, $idioma="portugues");
					
					if($_REQUEST['fAditivoCodigo']){	
						
						$oArquivo = $oFachada->recuperarUmMnyContratoAditivo($_REQUEST['fAditivoCodigo']);
						$sArquivoBD = ($oArquivo->getAnexo())? $oArquivo->getAnexo(): "";						
						if(is_file($sArquivoBD)){
							unlink($sArquivoBD);
						}	
					}
					
					if (!($sArquivoBD = $oUpload->putFile($_FILES[fAnexo],$sPrefixo,$nUsuarioId))) {
						$_SESSION['sMsg'] = $oUpload->error;
						$sHeader = "?action=MnyContratoAditivo.preparaFormulario&sOP=Alterar&fPesgCodigo=".$nUsuarioId."&nIdMnyContratoAditivo=".$_POST['fAditivoCodigo'];
						header("Location: ".$sHeader);	
						die();
					}else{
						$dDataArquivo = date('Y-m-d');
					}
				}else {
					$sArquivoBD = $_REQUEST['fAnexo'];
				}	
*/		
			
 			$oMnyContratoAditivo = $oFachada->inicializarMnyContratoAditivo($_POST['fAditivoCodigo'],$_POST['fContratoCodigo'],$_POST['fCodStatus'],$_POST['fNumero'],$_POST['fMotivoAditivo'],$_POST['fDataAditivo'],$_POST['fDataValidade'],$_POST['fValorAditivo'],$sArquivoBD,$_POST['fAtivo']);
			
 			$_SESSION['oMnyContratoAditivo'] = $oMnyContratoAditivo;
			
			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
			 		
 			$oValidate->add_number_field("Contrato", $oMnyContratoAditivo->getContratoCodigo(), "number", "y");
			//$oValidate->add_number_field("CodStatus", $oMnyContratoAditivo->getCodStatus(), "number", "y");
			//$oValidate->add_text_field("Numero", $oMnyContratoAditivo->getNumero(), "text", "y");
			$oValidate->add_text_field("Motivo", $oMnyContratoAditivo->getMotivoAditivo(), "text", "y");
			//$oValidate->add_date_field("DataAditivo", $oMnyContratoAditivo->getDataAditivo(), "date", "y");
			//$oValidate->add_date_field("DataValidade", $oMnyContratoAditivo->getDataValidade(), "date", "y");
			//$oValidate->add_number_field("ValorAditivo", $oMnyContratoAditivo->getValorAditivo(), "number", "y");
			//$oValidate->add_text_field("Anexo", $oMnyContratoAditivo->getAnexo(), "text", "y");
			//$oValidate->add_number_field("Ativo", $oMnyContratoAditivo->getAtivo(), "number", "y");
 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();

 				$sHeader = "?bErro=1&action=MnyContratoAditivo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoAditivo=".$_POST['fAditivoCodigo'];
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 			
         switch($_REQUEST['fTipoContrato']){
            case 1: $sTipoContrato = "CONTRATO"; break;
            case 2: $sTipoContrato = "ASE"; break;
            case 3: $sTipoContrato = "OC"; break;
            case 10: $sTipoContrato = "ASEFRETE"; break;
        }
		switch($sOP){
 			case "Cadastrar":

                if($_FILES["arquivo"]["tmp_name"][0]){
                    $Arquivo = $_FILES["arquivo"]["tmp_name"][0];
                    $Tipo = $_FILES["arquivo"]["type"][0];
                    move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                    $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                    $AnexoAditivo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                    $oMnyContratoAditivo->setAnexo($AnexoAditivo);
                }

                if($nIdContrato = $oFachada->inserirMnyContratoAditivo($oMnyContratoAditivo)){
                    unset($_SESSION['oMnyContratoAditivo']);
                    $_SESSION['sMsg'] = "Aditivo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&sOP=Alterar&Pagina=2&nIdMnyContratoPessoa=".$oMnyContratoAditivo->getContratoCodigo()."&fTipoContrato=".$_REQUEST['fTipoContrato'];
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Aditivo!"; 					
					$sHeader = "?bErro=1&action=MnyContratoAditivo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":

                if($_FILES["arquivo"]["tmp_name"][0]){
                    $Arquivo = $_FILES["arquivo"]["tmp_name"][0];
                    $Tipo = $_FILES["arquivo"]["type"][0];
                    move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");
                    $OpenAnexo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                    $AnexoAditivo = addslashes(fread($OpenAnexo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                    $oMnyContratoAditivo->setAnexo($AnexoAditivo);

                }else{
                    // falta ajustar algo aqui...........
                    $oAditivo = $oFachada->recuperarUmMnyContratoAditivo($oMnyContratoAditivo->getAditivoCodigo());
                    $oMnyContratoAditivo->setAnexo(addslashes($oAditivo->getAnexo()));
                }

 				if($oFachada->alterarMnyContratoAditivo($oMnyContratoAditivo)){
					unset($_SESSION['oMnyContratoAditivo']);
 					$_SESSION['sMsg'] = "Aditivo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario".$sTipoContrato."&sOP=Alterar&Pagina=2&nIdMnyContratoPessoa=".$oMnyContratoAditivo->getContratoCodigo()."&fTipoContrato=".$_REQUEST['fTipoContrato'];
					//$sHeader = "?bErro=0&action=MnyContratoAditivo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Aditivo!";
 					$sHeader = "?bErro=1&action=MnyContratoAditivo.preparaFormulario&sOP=".$sOP."&nIdMnyContratoAditivo=".$_POST['fAditivoCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyContratoAditivo']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
                    $oMnyContratoAditivo = $oFachada->recuperarUmMnyContratoAditivo($voRegistros[0]);
					eval("\$bResultado &= \$oFachada->excluirMnyContratoAditivo($sCampoChave);\n");
				}
 				if($bResultado){
 					$_SESSION['sMsg'] = "Aditivo(s) exclu&iacute;do(s) com sucesso!";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Aditivo!";
 				}
				$sHeader = "?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Alterar&Pagina=2&nIdMnyContratoPessoa=".$oMnyContratoAditivo->getContratoCodigo()."&fTipoContrato=".$_REQUEST['fTipoContrato'];
 			break;
 		}
 
// die($sHeader);
 		header("Location: ".$sHeader);		
 	
 	}



 function preparaArquivo(){
         $nAditivoCodigo = $_GET['fAditivoCodigo'];
         $oFachadaFinanceiro = new FachadaFinanceiroBD();
         $_REQUEST['oArquivo'] = $oFachadaFinanceiro->recuperarUmMnyContratoAditivo($nAditivoCodigo)->getAnexo();
         include_once('view/financeiro/mny_contrato_arquivo/anexo.php');
         exit();
    }
 
 }
 
 
 ?>
