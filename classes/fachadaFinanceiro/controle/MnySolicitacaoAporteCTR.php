<?php
 class MnySolicitacaoAporteCTR implements IControle{
 
 	public function MnySolicitacaoAporteCTR(){
 	
 	}
 
 	public function preparaLista(){
		
		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();

        $nSituacao = ($_REQUEST['nSituacao']) ? $_REQUEST['nSituacao'] : 0;
 
 		$voMnySolicitacaoAporte = $oFachada->recuperarTodosMnySolicitacaoAportePorEmpresaPorSituacao($_SESSION['Perfil']['EmpCodigo'],$nSituacao);
 
 		$_REQUEST['voMnySolicitacaoAporte'] = $voMnySolicitacaoAporte;
 		//$_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
		//$_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
 		
       // $_REQUEST['nSituacao'] = $nSituacao;

 		include_once("view/financeiro/mny_solicitacao_aporte/index.php");
 
 		exit();
 	
 	}


    public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
		$oFachadaSys = new FachadaSysBD();
		$oFachadaView = new FachadaViewBD();
 
 		$oMnySolicitacaoAporte = false;

        if($_REQUEST['sOP'] != 'Detalhar' ){
            $_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
            $_REQUEST['voMnyPlanoContas'] = $oFachada->recuperarTodosMnyPlanoContas();
            //$sGrupo = 8; //Unidade
            //$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
            $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());
            $sGrupo = '1'; // Conta...
            $_REQUEST['voMnyConta'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
            $sGrupo = '7'; // Centro de Negocio
            $_REQUEST['voMnyCentroNegocio'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
        }
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP'] == "Pagamento" || $_REQUEST['sOP'] == "Assinar"){
 			$nIdMnySolicitacaoAporte = ($_POST['fIdMnySolicitacaoAporte'][0]) ? $_POST['fIdMnySolicitacaoAporte'][0] : $_GET['nIdMnySolicitacaoAporte'];


            unset($_SESSION['oMnySolicitacaoAporte']);

			if($nIdMnySolicitacaoAporte){
 				$vIdMnySolicitacaoAporte = explode("||",$nIdMnySolicitacaoAporte);
 				$oMnySolicitacaoAporte = $oFachada->recuperarUmMnySolicitacaoAporte($vIdMnySolicitacaoAporte[0]);

                if($oMnySolicitacaoAporte){
                    $_REQUEST['oMnyTransferencia'] = $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnySolicitacaoAporte->getCodSolicitacao());

                    if($_REQUEST['oMnyTransferencia']){
                        $_REQUEST['oMnyMovimentoOrigem'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['oMnyTransferencia']->getMovCodigoOrigem());
                        $_REQUEST['oMnyMovimentoDestino'] = $oFachada->recuperarUmMnyMovimento($_REQUEST['oMnyTransferencia']->getMovCodigoDestino());
                        $_REQUEST['voVMovimentoLiberado'] = $oFachada->recuperarTodosMnyAporteItemPorUnidade($_REQUEST['oMnyMovimentoDestino']->getUniCodigo(),$oMnySolicitacaoAporte->getCodSolicitacao());
                        $_REQUEST['voMnyContaCorrenteOrigem'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoOrigem']->getUniCodigo());
                        $_REQUEST['voMnyContaCorrenteDestino'] = $oFachada->recuperarTodosMnyContaCorrentePorUnidade($_REQUEST['oMnyMovimentoDestino']->getUniCodigo());

                    }
                }
 			}

 		}/*else{
             $_REQUEST['voVMovimentoLiberado'] = $oFachada->recuperarTodosMnyAporteItemPorUnidade($_REQUEST['oMnyMovimentoOrigem']->getCodUnidade());
        }*/

 		//$_REQUEST['voMnyContaCorrente'] = $oFachada->recuperarTodosMnyContaCorrente();
 		$_REQUEST['oMnySolicitacaoAporte'] = ($_SESSION['oMnySolicitacaoAporte']) ? $_SESSION['oMnySolicitacaoAporte'] : $oMnySolicitacaoAporte;


        //ESTORNO
        if($oMnySolicitacaoAporte){
            $voMnyMovimentoItem = $oFachada->recuperarTodosMnyMovimentoItemPorAporte($oMnySolicitacaoAporte->getCodSolicitacao());
                foreach($voMnyMovimentoItem as $oMnyMovimentoItem){
                    if($oMnyMovimentoItem->getFinalizado() == 2){
                        $aItemEstonado[] = $oMnyMovimentoItem->getMovCodigo() .".". $oMnyMovimentoItem->getMovItem();
                    }
                }
               // print_r($aItemEstonado);
               // die();


        }



            if(isset($aItemEstonado)){
                $_REQUEST['aItemEstonado'] = $aItemEstonado;

            }else{
                 $_REQUEST['aItemEstonado']= array('11111.1');
            }

            if($_REQUEST['sOP'] == "Detalhar"){
                $_REQUEST['voMnyAporteItem'] = $oFachadaView->recuperarTodosVAporteItemPorSolicitacaoAporteItemPROCEDURE($nIdMnySolicitacaoAporte);
                $_REQUEST['voMnyAporteItemEstorno'] = $oFachadaView->recuperarTodosVAporteItemPorSolicitacaoAporteItemPROCEDUREEstorno($nIdMnySolicitacaoAporte);

                include_once("view/financeiro/mny_solicitacao_aporte/detalhe2.php");

                exit();

            }elseif($_REQUEST['sOP'] == "Pagamento"){

                $_REQUEST['voVMovimentoLiberado'] = $oFachada->recuperarTodosMnyAporteItemPorUnidadePorSolicitacaoAporte($oMnySolicitacaoAporte->getUnidadeAporte(),$oMnySolicitacaoAporte->getCodSolicitacao());
                $nArquivoTransferencia = $oFachada->recuperarUmMnyAporteItemPorUnidadePorSolicitacaoAporte($oMnySolicitacaoAporte->getUnidadeAporte(),$oMnySolicitacaoAporte->getCodSolicitacao())->getCodSolicitacaoAporte();
                //usado na tela de pagamento
                $nCodContratoDocTipo = 31;
                $_REQUEST['oMnyContratoDocTipo'] = $oFachada->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo);
                if($nArquivoTransferencia > 0){
                    $_REQUEST['Anexado'] = 1;
                }else{
                    $_REQUEST['Anexado'] = $nArquivoTransferencia;
                }

                 include_once("view/financeiro/mny_solicitacao_aporte/insere_altera_pagamento.php");
                 exit();
            }else{
                if($_REQUEST['sOP'] == "Alterar"){
                    $_REQUEST['sSomenteLeitura'] = "readonly='readonly'";
                    $nCodGrupoUsuario = 13; // Financeiro CENTRAL...
                    //$nCodGrupoUsuario = $_SESSION['Perfil']['CodGrupoUsuario'];
                    //$_REQUEST['voVMovimentoLiberado'] = $oFachadaView->recuperarTodosVAporteItemPorSolicitacaoAporteItem(21);
                    //$_REQUEST['voVMovimentoTotais'] = $oFachadaView->recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuarioTotais($_SESSION['Perfil']['CodUnidade'],$nCodGrupoUsuario,$_REQUEST['oMnySolicitacaoAporte']->getNumeroSolicitacao());
                    //$_REQUEST['voVMovimentoDisponivel'] = $oFachadaView->recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuario($_SESSION['Perfil']['CodUnidade'],$nCodGrupoUsuario);
                    $_REQUEST['voVMovimentoDisponivel'] = $oFachada->recuperarTodosFADisponiveisAportePorUnidadeGrupoUsuario($_REQUEST['oMnyMovimentoDestino']->getUniCodigo(),$nCodGrupoUsuario,$_SESSION['oEmpresa']->getEmpCodigo());
                    //$_REQUEST['voVMovimentoDisponivel'] = $oFachada->recuperarTodosFADisponiveisAportePorUnidadeGrupoUsuario($_REQUEST['oMnyMovimentoDestino']->getUniCodigo(),$nCodGrupoUsuario);
                    //esta pegando da transferencia ...
                    // $_REQUEST['voVMovimentoLiberado'] = $oFachada->recuperarTodosMnyAporteItemPorUnidade($_SESSION['Perfil']['CodUnidade'],$_REQUEST['oMnySolicitacaoAporte']->getCodSolicitacao());

                }
            }
                include_once("view/financeiro/mny_solicitacao_aporte/insere_altera.php");

 		exit();
 	}



 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 		$oFachadaSys = new FachadaSysBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];


        if($sOP =='AlterarJuros'){

            $oMnyAporteItem =  $oFachada->recuperarUmMnyAporteItemPorFA($_REQUEST['fMovCodigo'],$_REQUEST['fMovItem']);
            $oMnyTransferencia =  $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnyAporteItem->getCodSolicitacaoAporte());
            $oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($_REQUEST['fMovCodigo'],$_REQUEST['fMovItem']);
            $oMnyMovimentoItem->setMovJurosBanco($_REQUEST['fMovJuros']);
            $oMnyMovimentoItem->setMovRetencaoBanco($_REQUEST['fMovRetencao']);
            if($oFachada->alterarMnyMovimentoItem($oMnyMovimentoItem)){
                if($oMnyTransferencia){
                    $oMnyMovimentoItemOrigem = $oFachada->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoOrigem(),$oMnyTransferencia->getMovItemOrigem());
                    $oMnyMovimentoItemDestino = $oFachada->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoDestino(),$oMnyTransferencia->getMovItemDestino());
                    $sOrigem = array('.',',');
                    $sDestino = array('','.');
                    $nMovJuros = str_replace($sOrigem, $sDestino, $_REQUEST['fMovJuros']);
                    $nMovRetencao = str_replace($sOrigem, $sDestino, $_REQUEST['fMovRetencao']);
                    $nJurosAtualizado = ($oMnyMovimentoItemOrigem->getMovJuros() + $nMovJuros);
                    $nRetencaoAtualizado = ($oMnyMovimentoItemOrigem->getMovJuros() + $nMovRetencao);
                    $oMnyMovimentoItemOrigem->setMovJurosBanco($nJurosAtualizado);
                    $oMnyMovimentoItemOrigem->setMovRetencaoBanco($nRetencaoAtualizado);
                    $oMnyMovimentoItemDestino->setMovJurosBanco($nJurosAtualizado);
                    $oMnyMovimentoItemDestino->setMovRetencaoBanco($nRetencaoAtualizado);
                    $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemOrigem);
                    $oFachada->alterarMnyMovimentoItem($oMnyMovimentoItemDestino);
                }
            }

            $sHeader = "?action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar&nIdMnySolicitacaoAporte=".$oMnyAporteItem->getCodSolicitacaoAporte();
            header("Location: ".$sHeader);
            exit();
        }else{
            if($sOP != "Excluir"){

                $_POST['fSolInc'] = $_SESSION['Perfil']['Login'] . "||" . date('Y-m-d');
                $_POST['fLiberado'] = 0;
                $oMnySolicitacaoAporte = $oFachada->inicializarMnySolicitacaoAporte($_POST['fCodSolicitacao'],$_POST['fDataSolicitacao'],$_POST['fNumeroSolicitacao'],$_POST['fNumeroAporte'],mb_convert_case($_POST['fSolInc'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fSolAlt'],MB_CASE_UPPER, "UTF-8"),$_POST['fAssinado'],$_POST['fLiberado'],$_POST['fAtivo']);

                $_SESSION['oMnySolicitacaoAporte'] = $oMnySolicitacaoAporte;

                $oValidate = FabricaUtilitario::getUtilitario("Validate");
                $oValidate->check_4html = true;

                //$oValidate->add_number_field("CodSolicitacao", $oMnySolicitacaoAporte->getCodSolicitacao(), "number", "y");
                //$oValidate->add_date_field("DataSolicitacao", $oMnySolicitacaoAporte->getDataSolicitacao(), "date", "y");
                //$oValidate->add_number_field("EmpCodigo", $oMnySolicitacaoAporte->getEmpCodigo(), "number", "y");
                //$oValidate->add_number_field("UniCodigo", $oMnySolicitacaoAporte->getUniCodigo(), "number", "y");
                //$oValidate->add_date_field("Periodo1", $oMnySolicitacaoAporte->getPeriodo1(), "date", "y");
                //$oValidate->add_date_field("Periodo2", $oMnySolicitacaoAporte->getPeriodo2(), "date", "y");
                //$oValidate->add_text_field("SolInc", $oMnySolicitacaoAporte->getSolInc(), "text", "y");
                //$oValidate->add_text_field("SolAlt", $oMnySolicitacaoAporte->getSolAlt(), "text", "y");
                //$oValidate->add_number_field("Ativo", $oMnySolicitacaoAporte->getAtivo(), "number", "y");


                if (!$oValidate->validation()) {
                    $_SESSION['sMsg'] = $oValidate->create_msg();
                    $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao']."";
                    header("Location: ".$sHeader);
                    die();
                }
            }

            switch($sOP){
                /*
                case "Cadastrar":
                    if($nCodMnySolicitacaoAporte = $oFachada->inserirMnySolicitacaoAporte($oMnySolicitacaoAporte)){
                    $num = 1;
                   // if($num == 1){
                        echo $cont = count($_REQUEST['fMovCodigo']);

                        $_POST['fCodSolicitacaoAporte'] = $nCodMnySolicitacaoAporte;
                        $item = 1;
                        for($i = 0; $cont > $i ; $i++ ){
                            $aMovimentoItem = explode(".",$_REQUEST['fMovCodigo'][$i]);
                            $_POST['fMovCodigo'] = $aMovimentoItem[0];
                            $_POST['fMovItem']   = $aMovimentoItem[1];
                            $oMnyAporteItem = $oFachada->inicializarMnyAporteItem($_POST['fCodSolicitacaoAporte'],$item,$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fAssinado'],1);
                            $oFachada->inserirMnyAporteItem($oMnyAporteItem);
                            //Financeiro Central -> 13
                            //Diretoria Financeira -> 8
                            $_POST['fDescricao'] = "Aguardando aprovação da diretoria.";
                            $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],13,8,mb_convert_case($_SESSION['Perfil']['Login'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                             $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                            $item++;
                        }
                        unset($_SESSION['oMnySolicitacaoAporte']);
                        unset($_SESSION['oMnyMov']);
                        $_SESSION['sMsg'] = "Solicita&ccedil;&atilde;o de Aporte [".$nCodMnySolicitacaoAporte."] inserida com sucesso!";
                        $sHeader = "?bErro=0&action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar&nIdMnySolicitacaoAporte=".$nCodMnySolicitacaoAporte;

                    }else{
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Solicita&ccedil;&atilde;o Aporte!";
                        $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP;
                    }
                break;
                */
                case "Cadastrar":

                    $nErro=0;

                    //Origem
                    $_POST['fEmpCodigo'] =          $_SESSION['oEmpresa']->getEmpCodigo();
                    $_POST['fPesCodigoOrigem'] = $_SESSION['oEmpresa']->getEmpCodPessoa();
                    $_POST['fMovParcelas'] = 'null';
                    $_POST['fMovDataInclusao'] = date('d/m/Y');
                    $_POST['fMovDataEmissao'] = date('d/m/Y');
                    $_POST['fMovTipo'] = 188;
                    $_POST['fSetCodigo'] =230;
                    $_POST['fMovInc'] = $_SESSION['Perfil']['Login'];
                    $_POST['fMovContrato'] = 'null';
                    $Comp = explode('/',$_POST['fDataSolicitacao']);
                    $_POST['fCompetencia'] = $Comp[1]."/".$Comp[2];



                    //print_r( $cont = count($_REQUEST['fMovCodigo']));
                    //die();

                    //inicializarMnySolicitacaoAporte();
                    $oMnySolicitacaoAporte = $oFachada->inicializarMnySolicitacaoAporte($_POST['fCodSolicitacao'],$_POST['fDataSolicitacao'],$_POST['fNumeroSolicitacao'],$_POST['fNumeroAporte'],mb_convert_case($_POST['fSolInc'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fSolAlt'],MB_CASE_UPPER, "UTF-8"),$_POST['fAssinado'],$_POST['fLiberado'],$_POST['fAtivo']);

                   // print_r($oMnySolicitacaoAporte );
                    //die();


                    if($nCodMnySolicitacaoAporte = $oFachada->inserirMnySolicitacaoAporte($oMnySolicitacaoAporte)){
                            $nValorAporte=0;
                            $cont = count($_REQUEST['fMovCodigo']);

                            $_POST['fCodSolicitacaoAporte'] = $nCodMnySolicitacaoAporte;

                            $item = 1;

                            for($i = 0; $cont > $i ; $i++ ){

                                $aMovimentoItem = explode(".",$_REQUEST['fMovCodigo'][$i]);

                                $_POST['fMovCodigo'] = $aMovimentoItem[0];

                                $_POST['fMovItem']   = $aMovimentoItem[1];

                                $_POST['fEstornado'] = ($_POST['fEstornado']) ? $_POST['fEstornado'] : 0;

                                $oMnyAporteItem = $oFachada->inicializarMnyAporteItem($_POST['fCodSolicitacaoAporte'],$item,$_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fAssinado'],$_POST['fEstornado'],1);

                                $nValorAporte = $nValorAporte + $oFachada->recuperarUmMnyMovimentoItemValorLiquido($_POST['fMovCodigo'],$_POST['fMovItem'])->getMovValorParcela();

                                if(!($oFachada->inserirMnyAporteItem($oMnyAporteItem))){
                                 echo "<br>erro ao inserir MnyAporteItem";
                                    $nErro=1;
                                }

                                //Financeiro Central -> 13
                                //Diretoria Financeira -> 8
                                $_POST['fDescricao'] = "Aguardando aprovação da diretoria.";

                                $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],13,8,mb_convert_case($_SESSION['Perfil']['Login'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);

                                if(!($oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo))){
                                  echo "<br>erro ao inserir MnyMovimentoItemProtocolo 1";
                                    $nErro=2;
                                }
                                $item++;
                             }


                    }else{

                        $nErro=3;
                        echo "<br>erro ao inserir mny_solicitacao_aporte";

                    }


                    $oMnyMovimento = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigoOrigem'],$_POST['fSetCodigo'],addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']);

                    $oMnyMovimento->setCusCodigo(320);
                    $oMnyMovimento->setCenCodigo(320);

                    if($nMovCodigoPagar = $oFachada->inserirMnyMovimento($oMnyMovimento)){

                        $_POST['fMovCodigo'] = $nMovCodigoPagar;
                        $_POST['fMovItem'] = 98;
                        $_POST['fMovValor'] = 1 ;

                        $nCaixinha = 0;
                        $sFaOrigem = NULL;
                        $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                        $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
                        $oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataEmissao'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,172,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorTarifaBanco'] = null,248,$_POST['fCompetencia'],1,0,1,$nCaixinha,$sFaOrigem,$nNf,$nConciliado);
                        $oMnyMovimentoItem->setMovValorParcela($nValorAporte);
                        if(!($nMovItemPagar = $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem)))
                            $nErro=1;

                        //TRAMITAÇÂO...
                        $oMnyMovimentoItemProtocoloPagar = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],8,10,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("LIBERADO PARA PAGAMENTO.",MB_CASE_UPPER, "UTF-8"),1);

                        if(!($oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloPagar))){
                            $nErro=4;
                            echo "<br>erro ao inserir MnyMovimentoItemProtocolo";
                        }
                        //destino
                       // $oFachadaSys = new FachadaSysBD();

                        $oSysEmpresa = $oFachadaSys->recuperarUmSysEmpresa($_POST['fEmpCodigoPara']);
                        $oMnyMovimento->setEmpCodigo($oSysEmpresa->getEmpCodigo());
                        $oMnyMovimento->setPesCodigo($oSysEmpresa->getEmpCodPessoa());
                        $oMnyMovimento->setUniCodigo($_POST['fUniCodigoPara']);
                        $oMnyMovimento->setMovTipo('189');
                        $oMnyMovimento->setCusCodigo(NULL);
                        $oMnyMovimento->setCenCodigo(NULL);

                        if($nMovCodigoReceber = $oFachada->inserirMnyMovimento($oMnyMovimento)){

                                $_POST['fMovCodigo'] = $nMovCodigoReceber;

                                $_POST['fMovItem'] = 99;

                                $nCaixinha = ($_POST['fCaixinha']) ? $_POST['fCaixinha'] : '0';
                                $sFaOrigem = NULL;
                                $nNf = ($_POST['fNf']) ? $_POST['fNf'] : '0';
                                $nConciliado = ($_POST['fConciliado']) ? $_POST['fConciliado'] : '0';
                                $oMnyMovimentoItem = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataEmissao'],$_POST['fMovDataPrev'],$_POST['fMovValor'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,169,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],248,$_POST['fCompetencia'],1,0,1,0,$sFaOrigem,$nNf,$nConciliado);
                                $oMnyMovimentoItem->setMovValorParcela($nValorAporte);
                                if(!($nMovItemReceber = $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItem))){
                                    $nErro=5;
                                    echo "<br>erro ao inserir MnyMovimentoItem 2";
                                }
                                //TRAMITAÇÂO...
                                $oMnyMovimentoItemProtocoloReceber = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],8,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case("ENCAMINHADO PARA CONCILIAÇÃO BANCÁRIA.",MB_CASE_UPPER, "UTF-8"),1);

                                if(!($oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloReceber))){
                                  echo "<br>erro ao inserir MnyMovimentoItemProtocolo 2";
                                    $nErro=6;
                                }

                                $_POST['fTipoAplicacao'] = "null";
                                $_POST['fOperacao'] = "null";
                                $_POST['fCodSolicitacaoAporte'] = "null";

                                $oMnyTransferencia = $oFachada->inicializarMnyTransferencia($nCodTransferencia,$nMovCodigoPagar,98,$_POST['fContaDe'],$nMovCodigoReceber,99,$_POST['fContaPara'],$_POST['fTipoAplicacao'],$nCodMnySolicitacaoAporte,$_POST['fOperacao'],1);

                                    if(! $oFachada->inserirMnyTransferencia($oMnyTransferencia)){
                                            $nErro=7;
                                        echo "erro ao inserir transferencia";
                                    }

                            }else{
                                echo "<br>erro ao inserir Movimento 2";
                                 $nErro=8;

                            }

                    } else {
                        echo "<br>erro ao inserir Movimento 1";
                        $nErro=9;

                    }

                    if($nErro >= 1){

                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir a Solicita&ccedil;&atilde;o Aporte!";

                        $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP;

                    }else{

                        unset($_SESSION['oMnySolicitacaoAporte']);

                        unset($_SESSION['oMnyMov']);

                        $_SESSION['sMsg'] = "Solicita&ccedil;&atilde;o de Aporte inserida com sucesso!";

                        $sHeader = "?bErro=0&action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar&nIdMnySolicitacaoAporte=".$nCodMnySolicitacaoAporte;

                    }

                break;
                case "Pagamento":
                    // gravar arquivos
                    //tramitar
                    $i=0;
                    $nErro=0;
                    $sOP = "Pagamento";
                    $vCampoChave = array();
                    $nTotalArquivos=0;
                    $arquivos = $_FILES["arquivo"];
                    $nCodSolicitacaoAporte = $_POST['fCodSolicitacao'];
                    $tamanhoMaximo = 100000000;
                    $nCodArquivo = $_REQUEST['fCodArquivo'];
                    $vIdArquivo = array();


                    if(!$arquivos['name']){
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir comprovante!";
                        $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                    }else{


                        for($i = 0 ; count($_REQUEST['fFA']) > $i  ;$i++){

                            if($arquivos['name'][$i] != ""){
                                $vFA = explode(".",$_REQUEST['fFA'][$i]);
                                $_POST['fMovCodigo'] = $vFA[0];
                                $_POST['fMovItem'] = $vFA[1];

                                $nomeArquivo =  "COMPROVANTE_" . $nCodSolicitacaoAporte . "_" . str_replace(".","_",$_REQUEST['fFA'][$i]);
                                $tamanhoArquivo = $arquivos['size'][$i];
                                $nomeTemporario = $arquivos['tmp_name'][$i];
                                $vPermissao = array("application/pdf");
                                $tipoArquivo = strtok($arquivos['type'][$i], ";");
                                $nCodContratoDocTipo = 11;// $_POST['fCodTipo'][$i];
                                $_POST['fRealizadoPor'] = $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i:s');

                                //verificacao de erros do upload;
                                if($tamanhoArquivo <= 0){
                                    $_SESSION['sMsg'] .= "Erro 1: Erro no tamanho do arquivo: ".$tamanhoArquivo ." Kb. Tamanho Maximo é ".$tamanhoMaximo." !";
                                    $nErro++;
                                }
                                if($tamanhoArquivo > $tamanhoMaximo){
                                    $_SESSION['sMsg'] .= "Erro 2: Tamanho máximo é: ".$tamanhoMaximo." KB!";
                                    $nErro++;
                                }
                                if (!in_array($tipoArquivo, $vPermissao)){
                                    $_SESSION['sMsg'] .= "Erro 4: Arquivo precisa ser PDF.";
                                    $nErro++;
                                }


                                if($nErro == 0 ){
                                    //  USADO PARA ARMAZENAR O ARQUIVO NO BANCO DE DADOS...
                                     $oSysArquivo = $oFachadaSys->inicializarSysArquivo($_POST['fCodArquivo'][$i],$nomeArquivo,$blobArquivo,$tipoArquivo,$nCodContratoDocTipo,$tamanhoArquivo,date('Y-m-d H:i:s'),mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),1,'NULL','NULL', $_POST['fMovCodigo'], $_POST['fMovItem']);

                                    if($_FILES["arquivo"]["tmp_name"][$i]){
                                       $Arquivo = $_FILES["arquivo"]["tmp_name"][$i];
                                       $Tipo = $_FILES["arquivo"]["type"][$i];

                                        move_uploaded_file($Arquivo, "c:\\wamp\\tmp\\arquivo.pdf");

                                        $blobArquivo = fopen("c:\\wamp\\tmp\\arquivo.pdf", "rb");
                                        $blobArquivo = addslashes(fread($blobArquivo, filesize("c:\\wamp\\tmp\\arquivo.pdf")));
                                        $oSysArquivo->setArquivo($blobArquivo);
                                        unlink("c:\\wamp\\tmp\\arquivo.pdf");


                                        $_REQUEST['fCodContratoDocTipo'] = $_REQUEST['fCodTipo'];
                                        $_REQUEST['fCodContratoArquivo'] = $_REQUEST['fCodArquivo'];

                                        //$oUplaod = UploadMultiplo::AlteraUpload($_FILES['arquivo'],$_REQUEST['fCodContratoDocTipo'],$_POST['fContratoCodigo'],$_REQUEST['fCodContratoArquivo'],$_POST['fMovCodigo']);

                                        if($_POST['fCodArquivo'][$i]){
                                            $oFachadaSys->alterarSysArquivo($oSysArquivo);
                                                $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],10,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                                $vIdMovimentoItemProtocolo[] = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);

                                        }else{
                                            if(!$nIdArquivo = $oFachadaSys->inserirSysArquivo($oSysArquivo)){
                                                $nErro++;
                                                $_SESSION['sMsg'] = "Erro 6: não inserido no banco";
                                            }else{
                                                $vIdArquivo[] = $nIdArquivo;
                                                $_POST['fDescricao'] = "ENCAMINHADO PARA CONCILIAÇÃO BANCÁRIA.";
                                                $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],10,11,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                                $vIdMovimentoItemProtocolo[] = $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                                            }
                                        }




                                    }
                                }
                            }
                        }
                }
                if($nErro ==0){
                     $_SESSION['sMsg'] = "Comprovante(s) inserido(s) com sucesso!";
                    // falta ver se esse link esta correto
                    $sHeader = "?bErro=0&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                }else{

                    $_SESSION['sMsg'] .= "Ocorreu [". $nErro ."] problema(s) na inserção do(s) comprovante(s)! Tente novamente!";
                    // falta ver se esse link esta correto

                    $sHeader = "?bErro=0&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                }


                break;
                case "Alterar":

                    if($oFachada->alterarMnySolicitacaoAporte($oMnySolicitacaoAporte)){
                        //$oFachada->exluirMnyAporteItemPorSolicitacaoAporte($oMnySolicitacaoAporte->getCodSolicitacao());

                        $oTransferencia = $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnySolicitacaoAporte->getCodSolicitacao());
                        $oTransferencia->setDe($_REQUEST['fContaDe']);
                        $oTransferencia->setPara($_REQUEST['fContaPara']);
                        $oFachada->alterarMnytransferencia($oTransferencia);


                        $cont = count($_REQUEST['fMovCodigo']);
                        $_POST['fCodSolicitacaoAporte'] = $oMnySolicitacaoAporte->getCodSolicitacao();

                        $item = $oMnySolicitacaoAporte->getMnyAporteItemPorMaxItem();
                        for($i = 0; $cont > $i ; $i++ ){
                            $item++;
                            $aMovimentoItem = explode(".",$_REQUEST['fMovCodigo'][$i]);
                            $_POST['fMovCodigo'] = $aMovimentoItem[0];
                            $_POST['fMovItem']   = $aMovimentoItem[1];

                            $oMnyAporteItem = $oFachada->inicializarMnyAporteItem($_POST['fCodSolicitacaoAporte'],$item,$_POST['fMovCodigo'],$_POST['fMovItem'],0,0,1);
                            if($oFachada->inserirMnyAporteItem($oMnyAporteItem)){
                                $_POST['fDescricao'] = "Aguardando aprovação da diretoria.";
                                $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],13,8,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                                $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                            }
                            $item++;

                        }
                        unset($_SESSION['oMnySolicitacaoAporte']);
                        $_SESSION['sMsg'] = "Solicita&ccedil;&atilde;o Aporte alterado com sucesso!";
                        $sHeader = "?action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                    } else {
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Solicita&ccedil;&atilde;o Aporte!";
                        $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaFormulario&sOP=".$sOP."&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                    }
                break;
                case "Estornar":
                    $aFaEstorno = $_REQUEST['fFaEstorno'];
                    //print_r($_REQUEST['fFaEstorno']);die();
                    //print_r(count($aFaEstorno));die();
                    $nValorTotalEstorno = 0.00;
                    $j=0;


                    for($i=0;$i<count($aFaEstorno);$i++){
                        $FA = explode('||',$_REQUEST['fFaEstorno'][$i]);
                        //print_r( $FA);die();

                        $oMnyAporteItem = $oFachada->recuperarUmMnyAporteItemPorFA($FA[0],$FA[1]);
                        $oMnySolicitacaoAporte = $oFachada->recuperarUmMnySolicitacaoAporte($oMnyAporteItem->getCodSolicitacaoAporte());
                       // print_r($oMnyAporteItem);die();
                        $oMnyAporteItem->setEstornado($oMnyAporteItem->getEstornado()+1);
                        $oFachada->alterarMnyAporteItem($oMnyAporteItem);
                        $_POST['fDescricao'] = "FA RETIRADA DO APORTE ". $oMnySolicitacaoAporte->getNumeroAporte();
                        $_POST['fResponsavel'] = $_SESSION['Perfil']['Login'];
                        $oMnyMovimentoItemProtocolo = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$oMnyAporteItem->getMovCodigo(),$oMnyAporteItem->getMovItem(),10,13,mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                        $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                        $nValorTotalEstorno += $FA[2];

                        $sMovimentosEstornados .= $oMnyAporteItem->getMovCodigo().'.'.$oMnyAporteItem->getMovItem().' ,';

                        $j++;
                    }


                    //print_r($nValorTotalEstorno);
                    //die();


                    $oMnyTransferencia = $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($_REQUEST['fCodSolicitacao']);

                    $oMnyMovimentoOrigem = $oFachada->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoOrigem());
                    $oMnyMovimentoItemOrigem = $oFachada->recuperarUmMnyMovimentoItemPorMovimento($oMnyTransferencia->getMovCodigoOrigem());


                    //Inicio Movimento
                    $_POST['fMovDataEmissao'] = $oMnyMovimentoOrigem->getMovDataEmissaoFormatado();
                    $_POST['fMovDataInclusao'] = date('d/m/Y');
                    $_POST['fCusCodigo'] = $oMnyMovimentoOrigem->getCusCodigo();
                    $_POST['fNegCodigo'] = $oMnyMovimentoOrigem->getNegCodigo();
                    $_POST['fCenCodigo'] = $oMnyMovimentoOrigem->getCenCodigo();
                    $_POST['fUniCodigo'] = $oMnyMovimentoOrigem->getUniCodigo();
                    $_POST['fPesCodigo'] = $oMnyMovimentoOrigem->getPesCodigo();
                    $_POST['fSetCodigo'] = $oMnyMovimentoOrigem->getSetCodigo();
                    $_POST['fConCodigo'] = 152; // ESTORNO
                    $_POST['fMovObs'] = "DEVOLUÇÃO DA SOLICITAÇÃO DE APORTE";
                    $_POST['fMovInc'] = $oMnyMovimentoOrigem->getMovInc();
                    $_POST['fMovAlt'] = $oMnyMovimentoOrigem->getMovAlt();
                    $_POST['fMovContrato'] = "null";
                    $_POST['fMovDocumento'] = $oMnyMovimentoOrigem->getMovDocumento();
                    $_POST['fMovParcelas'] = "null";
                    $_POST['fMovTipo'] = $oMnyMovimentoOrigem->getMovTipo();
                    $_POST['fEmpCodigo'] = $oMnyMovimentoOrigem->getEmpCodigo();
                    $_POST['fMovOutrosDesc'] = "null";

                    //Movimento Origem
                    $oMnyMovimentoOrigemClone = $oFachada->inicializarMnyMovimento($_POST['fMovCodigo'],$_POST['fMovDataInclusao'],$_POST['fMovDataEmissao'],$_POST['fCusCodigo'],$_POST['fNegCodigo'],$_POST['fCenCodigo'],$_POST['fUniCodigo'],$_POST['fConCodigo'],$_POST['fPesCodigo'],$_POST['fSetCodigo'],addslashes(mb_convert_case($_POST['fMovObs'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovInc'],$_POST['fMovAlt'],$_POST['fMovContrato'],addslashes(mb_convert_case($_POST['fMovDocumento'],MB_CASE_UPPER, "UTF-8")),$_POST['fMovParcelas'],$_POST['fMovTipo'],$_POST['fEmpCodigo'],$_POST['fMovIcmsAliq'],$_POST['fMovPis'],$_POST['fMovConfins'],$_POST['fMovCsll'],$_POST['fMovIss'],$_POST['fMovIr'],$_POST['fMovIrrf'],$_POST['fMovInss'],$_POST['fMovOutros'],$_POST['fMovDevolucao'],1,$_POST['fMovOutrosDesc']);

                    //Movimento Destino
                    $oMnyMovimentoDestinoClone = clone $oMnyMovimentoOrigemClone;

                    $nMovCodigoOrigemClone = $oFachada->inserirMnyMovimento($oMnyMovimentoOrigemClone);


                    //Inicio Item
                    $_POST['fMovCodigo'] = $nMovCodigoOrigemClone;
                    $_POST['fMovItem'] = 96;
                    $_POST['fMovDataVencto'] = $oMnyMovimentoItemOrigem[0]->getMovDataVenctoFormatado();
                   // $_POST['fMovDataPrev'] = $oMnyMovimentoItemOrigem[0]->getMovDataPrevFormatado();
                    $_POST['fMovValorParcela'] = (number_format($nValorTotalEstorno,2,',','.'));
                    $_POST['fMovJuros'] = $oMnyMovimentoItemOrigem[0]->getMovJurosFormatado();
                    $_POST['fMovValorPagar'] = $oMnyMovimentoItemOrigem[0]->getMovValorPagarFormatado();
                    $_POST['fFpgCodigo'] = $oMnyMovimentoItemOrigem[0]->getFpgCodigo();
                    $_POST['fTipDocCodigo'] = $oMnyMovimentoItemOrigem[0]->getTipDocCodigo();
                    $_POST['fMovRetencao'] = $oMnyMovimentoItemOrigem[0]->getMovRetencao();
                    $_POST['fMovDataInclusao'] = date('d/m/Y');
                    $_POST['fMovValorTarifaBanco'] = $oMnyMovimentoItemOrigem[0]->getMovValorTarifaBancoFormatado();
                    $_POST['fTipAceCodigo'] = $oMnyMovimentoItemOrigem[0]->getTipAceCodigo();
                    $nCompetencia = explode('-',$oMnyMovimentoItemOrigem[0]->getCompetencia());
                    $_POST['fCompetencia'] = $nCompetencia[1]."/".$nCompetencia[0];
                    $_POST['fContabil'] = 0;
                    $_POST['fNf'] = '0';
                    $_POST['fConciliado'] = '0';
                    $_POST['fFinalizado'] = 0;
                    $_POST['fAtivo'] = 1;
                    $_POST['fCaixinha'] = 0;
                    $_POST['fFaOrigem'] = "null";

                    //Item Origem...
                    $oMnyMovimentoItemOrigemClone = $oFachada->inicializarMnyMovimentoItem($_POST['fMovCodigo'],$_POST['fMovItem'],$_POST['fMovDataVencto'],$_POST['fMovDataPrev'],$_POST['fMovValorParcela'],$_POST['fMovJuros'],$_POST['fMovValorPagar'],248,169,$_POST['fMovRetencao'],$_POST['fMovDataInclusao'],$_POST['fMovValorPago'],248,$_POST['fCompetencia'],1,0,1,0,$_POST['fFaOrigem'],$_POST['fNf'],$_POST['fConciliado']);

                    //Item Destino...
                    $oMnyMovimentoItemDestinoClone = clone $oMnyMovimentoItemOrigemClone;


                    $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItemOrigemClone);

                    //Movimento Destino...
                    $oMnyMovimentoDestinoClone;
                    //Item Destino...
                    $oMnyMovimentoItemDestinoClone;

                    $oMnyMovimentoDestinoClone->setMovCodigo('null');
                    $oMnyMovimentoDestinoClone->setConCodigo(9); // DEVOLUÇÃO/ESTORNO
                    $oMnyMovimentoDestinoClone->setMovTipo(189); // DEVOLUÇÃO/ESTORNO
                    $nMovCodigoDestinoClone = $oFachada->inserirMnyMovimento($oMnyMovimentoDestinoClone);

                    $oMnyMovimentoItemDestinoClone->setMovCodigo($nMovCodigoDestinoClone);
                    $oMnyMovimentoItemDestinoClone->setMovItem(97);
                    $oFachada->inserirMnyMovimentoItem($oMnyMovimentoItemDestinoClone);


                    $_POST['fTipoAplicacao'] = "null";
                    $_POST['fOperacao'] = "null";
                    $_POST['fCodSolicitacaoAporte'] = "null";
                    $_POST['fContaDe'] = $oMnyTransferencia->getPara();
                    $_POST['fContaPara'] = $oMnyTransferencia->getDe();

                    $_POST['CodMnySolicitacaoAporte'] = $oMnyTransferencia->getCodSolicitacaoAporte();

                    $oMnyTransferenciaNova = $oFachada->inicializarMnyTransferencia($_post['CodTransferencia'],$nMovCodigoOrigemClone,96,$_POST['fContaDe'],$nMovCodigoDestinoClone,97,$_POST['fContaPara'],$_POST['fTipoAplicacao'],$_POST['CodMnySolicitacaoAporte'],$_POST['fOperacao'],1);

                    $nCodTransferencia = $oFachada->inserirMnyTransferencia($oMnyTransferenciaNova);


                    $oMnyAporteItem = $oFachada->recuperarUmMnyAporteItemPorMaxItem($oMnyTransferencia->getCodSolicitacaoAporte());



                    $oMnyAporteItemNovo = $oFachada->inicializarMnyAporteItem($oMnyTransferencia->getCodSolicitacaoAporte(),($oMnyAporteItem->getAporteItem() + 1),$nMovCodigoOrigemClone,96,0,0,1);
                    //print_r($oMnyAporteItemNovo);
                    $oFachada->inserirMnyAporteItem($oMnyAporteItemNovo);


                    $_POST['fResponsavel'] = $_SESSION['Perfil']['Login'];
                    $_POST['fDescricao'] = "Estorno do aporte: ". $oMnySolicitacaoAporte->getNumeroAporte().", itens estornados: ". substr($sMovimentosEstornados, 0,-1);


                    $oMnyMovimentoItemProtocoloOrigem = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$nMovCodigoOrigemClone,96,10,10,mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                    $oMnyMovimentoItemProtocoloDestino = $oFachada->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$nMovCodigoDestinoClone,97,10,11,mb_convert_case($_POST['fResponsavel'],MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);


                    $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloOrigem);
                    $oFachada->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocoloDestino);

                    $_SESSION['sMsg'] = "Itens estornados com sucesso!";
                    $sHeader = "?action=MnySolicitacaoAporte.preparaFormulario&sOP=Pagamento&nIdMnySolicitacaoAporte=".$_POST['fCodSolicitacao'];
                break;
                case "AbrirAporte":
                    $oFachadaFinanceiro = new FachadaFinanceiroBD();
                    $sRetorno = $oFachadaFinanceiro->abreUmMnySolicitacaoAporte($_REQUEST['fCodSolicitacao'])->getNumeroAporte();
                    $_SESSION['sMsg'] = $sRetorno;
                    $sHeader = "?action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar&nIdMnySolicitacaoAporte=".$_REQUEST['fCodSolicitacao'];
                break;
                case "Excluir":
                    $bResultado = true;
                    $voRegistros = explode("____",$_REQUEST['fIdMnySolicitacaoAporte']);
                    foreach($voRegistros as $oRegistros){
                        $sCampoChave = str_replace("||", ",", $oRegistros);
                        $oFachada->exluirMnyAporteItemPorSolicitacaoAporte($sCampoChave);
                        eval("\$bResultado &= \$oFachada->excluirMnySolicitacaoAporte($sCampoChave);\n");
                    }

                    if($bResultado){
                        $_SESSION['sMsg'] = "Solicita&ccedil;&atilde;o Aporte(s) exclu&iacute;do(s) com sucesso!";
                        $sHeader = "?bErro=0&action=MnySolicitacaoAporte.preparaLista";
                    } else {
                        $_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Solicita&ccedil;&atilde;o Aporte!";
                        $sHeader = "?bErro=1&action=MnySolicitacaoAporte.preparaLista";
                    }
                break;
            }
 		 header("Location: ".$sHeader);
        }
 	
 	}

    public function liberaMovimentoParaPagamento(){
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $nCodSolicitacao = ($_GET['fCodSolicitacao'])?$_GET['fCodSolicitacao']:$_REQUEST['nIdMnySolicitacaoAporte'];
        $oMnySolicitacaoAporte = $oFachadaFinanceiro->recuperarUmMnySolicitacaoAporte($nCodSolicitacao);

        if($oMnySolicitacaoAporte){
            $oMnySolicitacaoAporte->setLiberado(1);
            $oMnyTransferencia = $oFachadaFinanceiro->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnySolicitacaoAporte->getCodSolicitacao());

            $oMovimentoOrigem = $oFachadaFinanceiro->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoOrigem());
             /*
            $oMovimentoOrigem->setNegCodigo($_REQUEST['fNegCodigo']);
            $oMovimentoOrigem->setMovObs($_REQUEST['fMovObs']);
            $oMovimentoOrigem->setMovDocumento($_REQUEST['fMovDocumento']);
            $oMovimentoOrigem->setConCodigo($_REQUEST['fConCodigo']);
            $oMovimentoOrigem->setMovDataEmissaoBanco($_REQUEST['fDataSolicitacao']);
            $oFachadaFinanceiro->alterarMnyMovimento($oMovimentoOrigem);
            */
            $oMovimentoItemOrigem = $oFachadaFinanceiro->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoOrigem(),98);

            $oMovimentoItemOrigem->setMovValorParcela($_REQUEST['nTotalAporte']);
            $oFachadaFinanceiro->alterarMnyMovimentoItem($oMovimentoItemOrigem);
            /*
            $oMovimentoDestino = $oFachadaFinanceiro->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoDestino());
            $oMovimentoDestino->setNegCodigo($_REQUEST['fNegCodigo']);
            $oMovimentoDestino->setMovObs($_REQUEST['fMovObs']);
            $oMovimentoDestino->setMovDocumento($_REQUEST['fMovDocumento']);
            $oMovimentoDestino->setConCodigo($_REQUEST['fConCodigo']);
            $oMovimentoDestino->setMovDataEmissaoBanco($_REQUEST['fDataSolicitacao']);
            $oFachadaFinanceiro->alterarMnyMovimento($oMovimentoDestino);
            */
            $oMovimentoItemDestino = $oFachadaFinanceiro->recuperarUmMnyMovimentoItem($oMnyTransferencia->getMovCodigoDestino(),99);
            $oMovimentoItemDestino->setMovValorParcela($_REQUEST['nTotalAporte']);
            $oFachadaFinanceiro->alterarMnyMovimentoItem($oMovimentoItemDestino);

        }
        if($oFachadaFinanceiro->alterarMnySolicitacaoAporte($oMnySolicitacaoAporte)){
            //Financeiro Central -> 13
            //Pagamento -> 10
            //Diretoria Financeira -> 8


            $vMovimentoItem = explode("||",$_GET['vMovimentoItem']);

            for($i=0;count($vMovimentoItem)>$i;$i++){
                $vFA = explode("_",$vMovimentoItem[$i]);
                $_POST['fMovCodigo'] = $vFA[0];
                $_POST['fMovItem']   = $vFA[1];



                $_POST['fDescricao'] = "Liberado para pagamento";
                $oMnyMovimentoItemProtocolo = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_POST['fMovCodigo'],$_POST['fMovItem'],8,10,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
                $_REQUEST['oMnySolicitacaoAporte'] = $oMnySolicitacaoAporte;
                 $_REQUEST['voVMovimentoLiberado'] = $oFachadaFinanceiro->recuperarTodosMnyAporteItemPorUnidade($oMovimentoOrigem->getUniCodigo(),$oMnySolicitacaoAporte->getNumeroSolicitacao());

                $_REQUEST['Liberado']=1;

            }


        }else{
            $_REQUEST['Liberado']=0;
        }
        echo "liberaMovimentoParaPagamento";
        include_once('view/financeiro/mny_solicitacao_aporte/tabela_ajax.php');
        exit();
    }

    // recupera tabela contendo a informação de todos os itens disponiveis para pagamento.
    function recuperarItensPagamento(){
        $oFachadaView = new FachadaViewBD();
		$oFachadaFinanceiro = new FachadaFinanceiroBD();
        $nCodUnidade = $_REQUEST['nCodUnidade'];
        $nNumeroSolicitacaoAporte = $_REQUEST['nNumeroSolicitacaoAporte'];
        $sOP=$_GET['sOP'];

        $nCodGrupoUsuario = 13; //Financeiro CENTRAL...
        if($_REQUEST['MovCodigo']  != null && $_REQUEST['MovItem'] != null){
            //$oMnySolicitacaoAporte = $oFachadaFinanceiro->recuperarUmMnySolicitacaoAportePorMovimentoPorItem($_REQUEST['MovCodigo'],$_REQUEST['MovItem']);
            $oMnySolicitacaoAporte = $oFachadaFinanceiro->recuperarUmMnySolicitacaoAporte($nNumeroSolicitacaoAporte);
            $oMnyTransferencia = $oFachadaFinanceiro->recuperarUmMnytransferenciaPorSoliticatacaoAporte($oMnySolicitacaoAporte->getCodSolicitacao());
            $nCodUnidade =  $oFachadaFinanceiro->recuperarUmMnyMovimento($oMnyTransferencia->getMovCodigoDestino())->getUniCodigo();
            //$nNumeroSolicitacaoAporte = $oMnySolicitacaoAporte->getNumeroSolicitacao();
            //$_REQUEST['voVMovimentoTotais'] = $oFachadaView->recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuarioTotais($nCodUnidade,$nCodGrupoUsuario,$oMnySolicitacaoAporte->getNumeroSolicitacao());
            //verifica quantidade de itens do aporte

            if($oFachadaFinanceiro->excluirMnyAporteItemPorMovimentoPorItem($_REQUEST['MovCodigo'],$_REQUEST['MovItem'])){
                $_REQUEST['oMnySolicitacaoAporte'] = $oMnySolicitacaoAporte;
                //TRAMITAÇÂO
                if($_SESSION['Perfil']['CodGrupoUsuario'] == '8'){
                    $_POST['fDescricao'] = "Não liberado para pagamento.";
                }else{
                    $_POST['fDescricao'] = "Correção.";
                }
                $_POST['fDeCodTipoProtocolo'] = $_SESSION['Perfil']['CodGrupoUsuario'];
                $oMnyMovimentoItemProtocolo = $oFachadaFinanceiro->inicializarMnyMovimentoItemProtocolo($_POST['fCodProtocoloMovimentoItem'],$_REQUEST['MovCodigo'],$_REQUEST['MovItem'],$_POST['fDeCodTipoProtocolo'],13,mb_convert_case($_SESSION['oUsuarioImoney']->getLogin(),MB_CASE_UPPER, "UTF-8"),date('d/m/Y H:i:s'),mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),1);
                //print_r($oMnyMovimentoItemProtocolo);
                //die();
                $oFachadaFinanceiro->inserirMnyMovimentoItemProtocolo($oMnyMovimentoItemProtocolo);
            }
        }

        $nEmpcodigo = $_REQUEST['nEmpCodigo'];
        //$_REQUEST['voVMovimentoDisponivel'] = $oFachadaView->recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuario($nCodUnidade,$nCodGrupoUsuario);
        $_REQUEST['voVMovimentoDisponivel'] = $oFachadaFinanceiro->recuperarTodosFADisponiveisAportePorUnidadeGrupoUsuario($nCodUnidade,$nCodGrupoUsuario);
        $_REQUEST['voVMovimentoLiberado']   = $oFachadaFinanceiro->recuperarTodosMnyAporteItemPorUnidade($nCodUnidade,$nNumeroSolicitacaoAporte);


        include_once("view/financeiro/mny_solicitacao_aporte/tabela_ajax.php");
        //include_once("view/financeiro/mny_solicitacao_aporte/insere_altera.php");
		exit();
	}

    function assinaItensAporte(){
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaView = new FachadaViewBD();

        $nCodGrupoUsuario = 15;//aptos para pagamento

        $nNumeroSolicitacaoAporte = $_REQUEST['nNumeroSolicitacao'];

        $nCodSolicitacaoAporte = $_REQUEST['nCodSolicitacaoAporte'];
        $nCodUnidade =  $oFachadaFinanceiro->recuperarUmMnySolicitacaoAporte($nCodSolicitacaoAporte)->getUnidadeAporte();
        $oFachadaFinanceiro->assinaMnyAporteItenPorSolicitacaoAporte($nCodSolicitacaoAporte);

        $_REQUEST['voVMovimentoDisponivel'] = $oFachadaView->recuperarTodosVMovimentoPorItemProtocoloPorUnidadePorGrupoUsuario($nCodUnidade,$nCodGrupoUsuario);
        //$_REQUEST['voVMovimentoLiberado']   = $oFachadaFinanceiro->recuperarTodosMnyAporteItemPorUnidade($nCodUnidade,$nNumeroSolicitacaoAporte);
        $_REQUEST['voVMovimentoLiberado']   = $oFachadaFinanceiro->recuperarTodosMnyAporteItemPorUnidade($nCodUnidade,$nCodSolicitacaoAporte);
//echo "assinaItensAporte";
        include_once('view/financeiro/mny_solicitacao_aporte/tabela_ajax.php');

        exit();

    }


    function carregaUnidadeDestino(){
        $oFachada = new FachadaFinanceiroBD();
        $nEmpCodigo = $_REQUEST['nEmpCodigo'];

        $_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($nEmpCodigo);
        include_once('view/financeiro/mny_solicitacao_aporte/unidade_ajax.php');

        exit();

    }

	function gravaSessao(){	
		$oFachadaView = new FachadaViewBD();
		
		$nMovimentoItem = $_REQUEST['fMovimentoItem'];
		$nMovItem = explode(".", $nMovimentoItem);
		$_POST['fMovCodigo'] = $nMovItem[0];
		$_POST['fMovItem'] = $nMovItem[1];					 				
		
		$_REQUEST['oMnyMovimento'] =  $oFachadaView->recuperarUmVMovimento($_POST['fMovCodigo'],$_POST['fMovItem']);

		include_once("view/financeiro/mny_solicitacao_aporte/sessao.php");

	}
	function AlteraSessao(){
		$nCodSolicitacao = $_REQUEST['nCodSolicitacao'];	
		$oFachadaView = new FachadaViewBD();			
		$_REQUEST['oMnyMovimento'] =  $oFachadaView->recuperarTodosVAporteItemPorSolicitacaoAporteItem($nCodSolicitacao);						 		
		include_once("view/financeiro/mny_solicitacao_aporte/sessao.php");
		exit();
	}

	function deletaItemSessao(){
		$oFachadaView = new FachadaViewBD();						 				
	    
		$nMovimentoItem = $_REQUEST['fMovimentoItem'];
		$_REQUEST['Indice'] = $_REQUEST['Indice'];	
		$nMovItem = explode(".", $nMovimentoItem);
		$_REQUEST['fMovCodigo'] = $nMovItem[0];
		$_REQUEST['fMovItem'] = $nMovItem[1];		
		$_REQUEST['deletar'] = 1;						 		
		
		$_REQUEST['oMnyMovimento'] = $_REQUEST['fMovCodigo'] . $_REQUEST['fMovItem']  ;		
		
		include_once("view/financeiro/mny_solicitacao_aporte/sessao.php");
		exit();
	}

 	public function preparaFicha(){
		
		$oFachada = new FachadaFinanceiroBD();
		$oFachadaView = new FachadaViewBD();
		$sOP = $_REQUEST['sOP'];
 		
		$nCodAporte = $_REQUEST['fCodSolicitacaoAporte'];
		
 		$oAporte = $oFachada->recuperarUmMnySolicitacaoAporte($nCodAporte);

 		$voVAporteItem = $oFachadaView->recuperarTodosVAporteItemPorAporte($nCodAporte);

        $_REQUEST['oAporte'] = $oAporte;
 		$_REQUEST['voVAporteItem'] = $voVAporteItem;
 		include_once("view/financeiro/mny_solicitacao_aporte/solicitacao_excel.php");
 
 		exit();
 	
 	}


 	public function preparaProximoNumeroSolicitacao(){
		
		$oFachada = new FachadaFinanceiroBD();
		$sOP = $_REQUEST['sOP'];
 		
		$nUniCodigo = $_REQUEST['fUniCodigo'];
		$nAno = $_REQUEST['nAno'];

 		if($nAno == 'undefined'){
            echo "<span style='font-size: 16px;;font-weight: bold; color:red;'>Data do Aporte inválida!</span>";
        }else{
            $oSolicitacao = $oFachada->recuperarUmMnySolicitacaoAporteProximoNumeroSolicitacao($nUniCodigo,$nAno);
            $_REQUEST['nNumeroSolicitacao'] = $oSolicitacao->getNumeroSolicitacao();
            echo "<span style='font-size: 20px;;font-weight: bold;'>".$oSolicitacao->getLiberado()."</span>";
            echo "<input type='hidden' id='nSolicitacao2'  name='fNumeroSolicitacao' value='".$_REQUEST['nNumeroSolicitacao']."' class='form-control' placeholder='Codigo Solicita&ccedil;&atilde;o' readonly>";
            echo "<input type='hidden'  name='fNumeroAporte' value='".$oSolicitacao->getLiberado()."' readonly>";
            echo "<input type='hidden' id='nSolicitacao'id='fSolicitacao'  value='999999' >";
        }
        exit();
 	
 	}

    public function preparaRelatorioPorEmpresa(){

        $oFachadaSys = new FachadaSysBD();
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaView = new FachadaViewBD();
        $_REQUEST['voSysEmpresa'] = $oFachadaSys->recuperarTodosSysEmpresa();
        $_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo(8);
        //$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasUnidadePorEmpresa($_SESSION['oEmpresa']->getEmpCodigo());
        $_REQUEST['voMnyPessoa'] = $oFachadaView->recuperarTodosVPessoaGeralFormatada();

        if($_REQUEST['sOP'] == 'RelEmp'){

			 $dtInicio  = explode('/',$_REQUEST['dtInicio']);
             $dtInicio = $dtInicio[2]."-".$dtInicio[1]."-".$dtInicio[0];
             $dtFim  = explode('/',$_REQUEST['dtFim']);
             $dtFim = $dtFim[2]."-".$dtFim[1]."-".$dtFim[0];

            $sComplemento = "Where vencimento BETWEEN '" . $dtInicio . "' And '" . $dtFim ."'";

            if($_REQUEST['fEmpCodigo'] != null){
                $sComplemento .= " And emp_codigo=" . $_REQUEST['fEmpCodigo'];
            }
            if($_REQUEST['fUniCodigo'] != null){
                foreach($_REQUEST['fUniCodigo'] as $key => $value){
                    $nUnidadeFormatado .= $value.",";
                }
                $nUnidadeFormatado = substr($nUnidadeFormatado, 0, -1);
                $sComplemento .= " And uni_codigo in(" . $nUnidadeFormatado .") ";
            }
            if($_REQUEST['fPesgCodigo'] != null){
                $sComplemento .= " And pesg_codigo=" . $_REQUEST['fPesgCodigo'];
            }
            if($_REQUEST['fMovTipo'] != null){
                $sComplemento .= " And tipo=" . $_REQUEST['fMovTipo'];
            }
            $_REQUEST['voResultado'] = $oFachadaView->recuperarTodosVRelatorioPorEmpresaPorComplemento($sComplemento);
        }

        include_once("view/financeiro/mny_solicitacao_aporte/relatorio_por_empresa.php");
        exit();
    }


    public function preparaRelatorioAportePorUnidade(){

        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $_REQUEST['voMnyUnidade'] = $oFachadaFinanceiro->recuperarTodosMnyPlanoContasPorGrupo(8);
        $nUniCodigo = $_REQUEST['fUniCodigo'];
        if($_REQUEST['sOP'] == 'RelEmp'){

			 $dtInicio  = explode('/',$_REQUEST['dtInicio']);
             $dtInicio = $dtInicio[2]."-".$dtInicio[1]."-".$dtInicio[0];
             $dtFim  = explode('/',$_REQUEST['dtFim']);
             $dtFim = $dtFim[2]."-".$dtFim[1]."-".$dtFim[0];

             if($_REQUEST['fUniCodigo'] != null){
                foreach($_REQUEST['fUniCodigo'] as $key => $value){
                    $nUnidadeFormatado .= $value.",";
                }
                $nUnidadeFormatado = substr($nUnidadeFormatado, 0, -1);
                $sComplemento .= "mi.mov_data_vencto BETWEEN '" . $dtInicio . "' And '" . $dtFim ."' And m.uni_codigo  in(" . $nUnidadeFormatado .") ";
             }

             //print_r($sComplemento);die();

             //m.uni_codigo = ".$nUniCodigo."  And  mi.mov_data_vencto BETWEEN '".$dtInicio."' And '".$dtFim."'
             $_REQUEST['voResultado'] = $oFachadaFinanceiro->recuperarTodosMnyMovimentoPorAportePorUnidadePeriodo($sComplemento);
        }
        include_once("view/financeiro/mny_solicitacao_aporte/relatorio_aporte_por_unidade.php");
        exit();
    }

    public function processaFormularioExcluiItenDoAporte(){
        $oFachadaFinanceiro = new FachadaFinanceiroBD();
        $oFachadaView = new FachadaViewBD();

        $aFA = explode('||',$_REQUEST['FA']);
        $_REQUEST['nCodUnidade'] = $_GET['nUnidade'];

        //$_REQUEST['aItemEstonado'] = ;


        $_REQUEST['voVMovimentoLiberado'] = $oFachadaFinanceiro->recuperarTodosMnyAporteItemPorUnidadePorSolicitacaoAporte($_GET['nUnidade'],$_GET['nIdMnySolicitacaoAporte']);

        $nArquivoTransferencia = $oFachadaFinanceiro->recuperarUmMnyAporteItemPorUnidadePorSolicitacaoAporte($_GET['nUnidade'],$_GET['nIdMnySolicitacaoAporte'])->getCodSolicitacaoAporte();

        //usado na tela de pagamento
        $nCodContratoDocTipo = 31;
        $_REQUEST['oMnyContratoDocTipo'] = $oFachadaFinanceiro->recuperarUmMnyContratoDocTipo($nCodContratoDocTipo);
        if($nArquivoTransferencia > 0){
            $_REQUEST['Anexado'] = 1;
        }else{
            $_REQUEST['Anexado'] = $nArquivoTransferencia;
        }


        //print_r($_REQUEST['voVMovimentoLiberado']);
       // die();


        //ESTORNO
        $voMnyMovimentoItem = $oFachadaFinanceiro->recuperarTodosMnyMovimentoItemPorAporte($_GET['nIdMnySolicitacaoAporte']);
        foreach($voMnyMovimentoItem as $oMnyMovimentoItem){
            if($oMnyMovimentoItem->getFinalizado() == 2){
                $aItemEstonado[] = $oMnyMovimentoItem->getMovCodigo() .".". $oMnyMovimentoItem->getMovItem();
            }
        }

        if(isset($aItemEstonado)){
            $_REQUEST['aItemEstonado'] = $aItemEstonado;

        }else{
             $_REQUEST['aItemEstonado']= array('11111.1');
        }

        //$oFachadaFinanceiro->excluirMnyAporteItemPorMovimentoPorItem($aFA[0],$aFA[1]);

        include_once('view/financeiro/mny_solicitacao_aporte/tabela_ajax_pagamento_exclui_item.php');
        exit();


    }
 }
 

 ?>
 
