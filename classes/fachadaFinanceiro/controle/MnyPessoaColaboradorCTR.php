<?php
 class MnyPessoaColaboradorCTR implements IControle{
 
 	public function MnyPessoaColaboradorCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		exit();
 		
 		include_once("view/financeiro/mny_pessoa_colaborador/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
  		$oFachadaSys = new FachadaSysBD();

 		$oMnyPessoaColaborador = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyPessoaColaborador = ($_POST['fIdMnyPessoaColaborador'][0]) ? $_POST['fIdMnyPessoaColaborador'][0] : $_GET['nIdMnyPessoaColaborador'];
 	
 			if($nIdMnyPessoaColaborador){
 				$vIdMnyPessoaColaborador = explode("||",$nIdMnyPessoaColaborador);
 				$oMnyPessoaColaborador = $oFachada->recuperarUmMnyPessoaColaborador($vIdMnyPessoaColaborador[0]);
 				$oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($vIdMnyPessoaColaborador[0]);
 			}
 		}

 		$_REQUEST['oMnyPessoaGeral'] = ($_SESSION['oMnyPessoaGeral']) ? $_SESSION['oMnyPessoaGeral'] : $oMnyPessoaGeral;
 		unset($_SESSION['oMnyPessoaGeral']);

 		$_REQUEST['oMnyPessoaColaborador'] = ($_SESSION['oMnyPessoaColaborador']) ? $_SESSION['oMnyPessoaColaborador'] : $oMnyPessoaColaborador;
 		unset($_SESSION['oMnyPessoaColaborador']);
 
 		$_REQUEST['voSysStatus'] = $oFachadaSys->recuperarTodosSysStatus();
		$_REQUEST['voMnyFuncao'] = $oFachada->recuperarTodosMnyFuncao();
		//$_REQUEST['voMnyUnidadePlanocontas'] = $oFachada->recuperarTodosMnyUnidadePlanocontas();
		//$_REQUEST['voMnySetorPlanocontas'] = $oFachada->recuperarTodosMnySetorPlanocontas();

		$_REQUEST['voMnyPessoaGeral'] = $oFachada->recuperarTodosMnyPessoaGeral();

		//$sGrupo = 14;// banco
		$_REQUEST['voSysBanco'] = $oFachadaSys->recuperarTodosSysBanco();
		
		$sGrupo = 13;// tipo de conta
		$_REQUEST['voSysContaTipo'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = 8;// unidade
		$_REQUEST['voMnyUnidade'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
		$sGrupo = 10;// unidade
		$_REQUEST['voMnySetor'] = $oFachada->recuperarTodosMnyPlanoContasPorGrupo($sGrupo);
		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_pessoa_colaborador/detalhe.php");
 		else
 			include_once("view/financeiro/mny_pessoa_colaborador/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyPessoaColaborador = $oFachada->inicializarMnyPessoaColaborador($_POST['fPescCodigo'],mb_convert_case($_POST['fPescNome'],MB_CASE_UPPER, "UTF-8"),$_POST['fPescCpf'],$_POST['fPescMat'],$_POST['fFunCodigo'],$_POST['fUniCodigo'],$_POST['fPescSal'],$_POST['fSetCodigo'],$_POST['fAdmissao'],$_POST['fDemissao'],mb_convert_case($_POST['fValext'],MB_CASE_UPPER, "UTF-8"),$_POST['fMotivo']);
																															 																																																
			$_SESSION['oMnyPessoaColaborador'] = $oMnyPessoaColaborador;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("PescCodigo", $oMnyPessoaColaborador->getPescCodigo(), "number", "y");			
			$oValidate->add_text_field("PescNome", $oMnyPessoaColaborador->getPescNome(), "text", "y");
			$oValidate->add_text_field("PescCpf", $oMnyPessoaColaborador->getPescCpf(), "text", "y");
			$oValidate->add_text_field("PescMat", $oMnyPessoaColaborador->getPescMat(), "text", "y");
			$oValidate->add_number_field("FunCodigo", $oMnyPessoaColaborador->getFunCodigo(), "number", "y");
			$oValidate->add_number_field("UniCodigo", $oMnyPessoaColaborador->getUniCodigo(), "number", "y");
			$oValidate->add_text_field("PescSal", $oMnyPessoaColaborador->getPescSal(), "text", "y");
			$oValidate->add_number_field("SetCodigo", $oMnyPessoaColaborador->getSetCodigo(), "number", "y");
			$oValidate->add_date_field("Admissao", $oMnyPessoaColaborador->getAdmissao(), "date", "y");
			//$oValidate->add_date_field("Demissao", $oMnyPessoaColaborador->getDemissao(), "date", "y");
			//$oValidate->add_text_field("Valext", $oMnyPessoaColaborador->getValext(), "text", "y");
			//$oValidate->add_text_field("Motivo", $oMnyPessoaColaborador->getMotivo(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaColaborador=".$_POST['fPescCodigo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyPessoaColaborador($oMnyPessoaColaborador)){
 					unset($_SESSION['oMnyPessoaColaborador']);
 					$_SESSION['sMsg'] = "Colaborador inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaGeral.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Colaborador!";
 					$sHeader = "?bErro=1&action=MnyPessoaGeral.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyPessoaColaborador($oMnyPessoaColaborador)){
 					unset($_SESSION['oMnyPessoaColaborador']);
 					$_SESSION['sMsg'] = "Colaborador alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaColaborador.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Colaborador!";
 					$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaFormulario&sOP=".$sOP."&nIdMnyPessoaColaborador=".$_POST['fPescCodigo']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyPessoaColaborador']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
				eval("\$bResultado &= \$oFachada->excluirMnyPessoaColaborador($sCampoChave);\n");
				}
 				
  				if($bResultado){
 					$_SESSION['sMsg'] = "Colaborador(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyPessoaColaborador.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Colaborador!";
 					$sHeader = "?bErro=1&action=MnyPessoaColaborador.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
