<?php
 class MnyTipoCaixaCTR implements IControle{
 
 	public function MnyTipoCaixaCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$voMnyTipoCaixa = $oFachada->recuperarTodosMnyTipoCaixa();
 
 		$_REQUEST['voMnyTipoCaixa'] = $voMnyTipoCaixa;
 		
 		
 		include_once("view/financeiro/mny_tipo_caixa/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$oMnyTipoCaixa = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdMnyTipoCaixa = ($_POST['fIdMnyTipoCaixa'][0]) ? $_POST['fIdMnyTipoCaixa'][0] : $_GET['nIdMnyTipoCaixa'];
 	
 			if($nIdMnyTipoCaixa){
 				$vIdMnyTipoCaixa = explode("||",$nIdMnyTipoCaixa);
 				$oMnyTipoCaixa = $oFachada->recuperarUmMnyTipoCaixa($vIdMnyTipoCaixa[0]);
 			}
 		}
 		
 		$_REQUEST['oMnyTipoCaixa'] = ($_SESSION['oMnyTipoCaixa']) ? $_SESSION['oMnyTipoCaixa'] : $oMnyTipoCaixa;
 		unset($_SESSION['oMnyTipoCaixa']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("view/financeiro/mny_tipo_caixa/detalhe.php");
 		else
 			include_once("view/financeiro/mny_tipo_caixa/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaFinanceiroBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oMnyTipoCaixa = $oFachada->inicializarMnyTipoCaixa($_POST['fCodTipoCaixa'],mb_convert_case($_POST['fDescricao'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fInseridoPor'],MB_CASE_UPPER, "UTF-8"),mb_convert_case($_POST['fAlteradoPor'],MB_CASE_UPPER, "UTF-8"),$_POST['fAtivo']);
 																																							
			$_SESSION['oMnyTipoCaixa'] = $oMnyTipoCaixa;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Descricao", $oMnyTipoCaixa->getDescricao(), "text", "y");
			$oValidate->add_text_field("InseridoPor", $oMnyTipoCaixa->getInseridoPor(), "text", "y");
			//$oValidate->add_text_field("AlteradoPor", $oMnyTipoCaixa->getAlteradoPor(), "text", "y");
			$oValidate->add_number_field("Ativo", $oMnyTipoCaixa->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=MnyTipoCaixa.preparaFormulario&sOP=".$sOP."&nIdMnyTipoCaixa=".$_POST['fCodTipoCaixa']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirMnyTipoCaixa($oMnyTipoCaixa)){
 					unset($_SESSION['oMnyTipoCaixa']);
 					$_SESSION['sMsg'] = "Tipo de Caixa inserido com sucesso!";
 					$sHeader = "?bErro=0&action=MnyTipoCaixa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel inserir o Tipo de Caixa!";
 					$sHeader = "?bErro=1&action=MnyTipoCaixa.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarMnyTipoCaixa($oMnyTipoCaixa)){
 					unset($_SESSION['oMnyTipoCaixa']);
 					$_SESSION['sMsg'] = "Tipo de Caixa alterado com sucesso!";
 					$sHeader = "?bErro=0&action=MnyTipoCaixa.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel alterar o Tipo de Caixa!";
 					$sHeader = "?bErro=1&action=MnyTipoCaixa.preparaFormulario&sOP=".$sOP."&nIdMnyTipoCaixa=".$_POST['fCodTipoCaixa']."";
 				}
 			break;
 			case "Excluir":
				$bResultado = true;
				$voRegistros = explode("____",$_REQUEST['fIdMnyTipoCaixa']);
				foreach($voRegistros as $oRegistros){
					$sCampoChave = str_replace("||", ",", $oRegistros);
					eval("\$bResultado &= \$oFachada->excluirMnyTipoCaixa($sCampoChave);\n");
				}
 				
 				if($bResultado){
 					$_SESSION['sMsg'] = "Tipo de Caixa(s) exclu&iacute;do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=MnyTipoCaixa.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N&atilde;o foi poss&iacute;vel excluir o(s) Tipo de Caixa!";
 					$sHeader = "?bErro=1&action=MnyTipoCaixa.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>
