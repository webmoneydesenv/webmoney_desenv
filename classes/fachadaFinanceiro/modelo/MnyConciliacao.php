<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_conciliacao 
  */
 class MnyConciliacao{
 	/**
	* @campo cod_conciliacao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodConciliacao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo cod_unidade
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo tipo_caixa
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipoCaixa;
	/**
	* @campo cod_cartao_credito
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodCartaoCredito;
	/**
	* @campo cod_conta_corrente
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodContaCorrente;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo data_conciliacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataConciliacao;
	/**
	* @campo valor_vale
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorVale;
	/**
	* @campo valor_resgate_iof
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorResgateIof;
	/**
	* @campo valor_resgate_irrf
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorResgateIrrf;
	/**
	* @campo observacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sObservacao;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo consolidado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nConsolidado;
	/**
	* @campo ordem
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nOrdem;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;

	private $oMnyMovimentoItem;
	//private $oMnyMovimentoItem;
	private $oMnyCartao;
	private $oMnyContaCorrente;
	private $oMnyTipoCaixa;
	private $oMnyPlanoContas;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodConciliacao($nCodConciliacao){
		$this->nCodConciliacao = $nCodConciliacao;
	}
	public function getCodConciliacao(){
		return $this->nCodConciliacao;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setTipoCaixa($nTipoCaixa){
		$this->nTipoCaixa = $nTipoCaixa;
	}
	public function getTipoCaixa(){
		return $this->nTipoCaixa;
	}
	public function setCodCartaoCredito($nCodCartaoCredito){
		$this->nCodCartaoCredito = $nCodCartaoCredito;
	}
	public function getCodCartaoCredito(){
		return $this->nCodCartaoCredito;
	}
	public function setCodContaCorrente($nCodContaCorrente){
		$this->nCodContaCorrente = $nCodContaCorrente;
	}
	public function getCodContaCorrente(){
		return $this->nCodContaCorrente;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		 if($dData){
			 $oData = DateTime::createFromFormat('d/m/Y', $dData);
			 $this->dData = $oData->format('Y-m-d') ;
		}
	}
	public function setDataConciliacao($dDataConciliacao){
		$this->dDataConciliacao = $dDataConciliacao;
	}
	public function getDataConciliacao(){
		return $this->dDataConciliacao;
	}
	public function getDataConciliacaoFormatado(){
		$oData = new DateTime($this->dDataConciliacao);
		 return $oData->format("d/m/Y");
	}
	public function setDataConciliacaoBanco($dDataConciliacao){
		 if($dDataConciliacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataConciliacao);
			 $this->dDataConciliacao = $oData->format('Y-m-d') ;
		}
	}
	public function setValorVale($nValorVale){
		$this->nValorVale = $nValorVale;
	}
	public function getValorVale(){
		return $this->nValorVale;
	}
	public function getValorValeFormatado(){
		 $vRetorno = number_format($this->nValorVale , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorValeBanco($nValorVale){
		if($nValorVale){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorVale = str_replace($sOrigem, $sDestino, $nValorVale);
	
		}else{
		$this->nValorVale = 'null';
			}
		}
		
	public function setValorResgateIof($nValorResgateIof){
		$this->nValorResgateIof = $nValorResgateIof;
	}
	public function getValorResgateIof(){
		return $this->nValorResgateIof;
	}
	public function getValorResgateIofFormatado(){
		 $vRetorno = number_format($this->nValorResgateIof , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorResgateIofBanco($nValorResgateIof){
		if($nValorResgateIof){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorResgateIof = str_replace($sOrigem, $sDestino, $nValorResgateIof);
	
		}else{
		$this->nValorResgateIof = 'null';
			}
	}



	public function setValorResgateIrrf($nValorResgateIrrf){
		$this->nValorResgateIrrf = $nValorResgateIrrf;
	}
	public function getValorResgateIrrf(){
		return $this->nValorResgateIrrf;
	}
	public function getValorResgateIrrfFormatado(){
		 $vRetorno = number_format($this->nValorResgateIrrf , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorResgateIrrfBanco($nValorResgateIrrf){
		if($nValorResgateIrrf){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorResgateIrrf = str_replace($sOrigem, $sDestino, $nValorResgateIrrf);
	
		}else{
			$this->nValorResgateIrrf = 'null';
		}
	}


		
public function setObservacao($sObservacao){
		$this->sObservacao = $sObservacao;
	}
	public function getObservacao(){
		return $this->sObservacao;
	}
	public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setOrdem($nOrdem){
		$this->nOrdem = $nOrdem;
	}
	public function getOrdem(){
		return $this->nOrdem;
	}
	public function setConsolidado($nConsolidado){
		$this->nConsolidado = $nConsolidado;
	}
	public function getConsolidado(){
		return $this->nConsolidado;
	}
	public function setMnyMovimentoItem($oMnyMovimentoItem){
		$this->oMnyMovimentoItem = $oMnyMovimentoItem;
	}
	public function getMnyMovimentoItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovCodigo());
		return $this->oMnyMovimentoItem;
	}
	/*public function setMnyMovimentoItem($oMnyMovimentoItem){
		$this->oMnyMovimentoItem = $oMnyMovimentoItem;
	}
	public function getMnyMovimentoItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovItem());
		return $this->oMnyMovimentoItem;
	}*/
	public function setMnyCartao($oMnyCartao){
		$this->oMnyCartao = $oMnyCartao;
	}
	public function getMnyCartao(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyCartao = $oFachada->recuperarUmMnyCartao($this->getCodCartaoCredito());		
		return $this->oMnyCartao;
	}
	public function setMnyContaCorrente($oMnyContaCorrente){
		$this->oMnyContaCorrente = $oMnyContaCorrente;
	}
	public function getMnyContaCorrente(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContaCorrente = $oFachada->recuperarUmMnyContaCorrente($this->getCodContaCorrente());
		return $this->oMnyContaCorrente;
	}
	public function setMnyTipoCaixa($oMnyTipoCaixa){
		$this->oMnyTipoCaixa = $oMnyTipoCaixa;
	}
	public function getMnyTipoCaixa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyTipoCaixa = $oFachada->recuperarUmMnyTipoCaixa($this->getTipoCaixa());
		return $this->oMnyTipoCaixa;
	}
	public function setMnyPlanoContas($oMnyPlanoContas){
		$this->oMnyPlanoContas = $oMnyPlanoContas;
	}
	public function getMnyPlanoContas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($this->getCodUnidade());
		return $this->oMnyPlanoContas;
	}	
	public function getValorTotalTipoCaixaFormatado(){
		 $vRetorno = number_format($this->nValorResgateIof + $this->nValorVale  , 2, ',', '.');
		 return $vRetorno;
	}		
	
 }
 ?>
