<?php
 /**
  * @author Auto-Generated
  * @package fachadaFinanceiro
  * @SGBD mysql
  * @tabela mny_conta_corrente_unidade
  */
 class MnyContaCorrenteUnidade{
 	/**
	* @campo ccr_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCcrCodigo;
 	/**
	* @campo cod_unidade
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUnidade;

	private $oUnidade;
	private $oPlanoContas;


 	public function __construct(){

 	}

 	public function setCcrCodigo($nCcrCodigo){
		$this->nCcrCodigo = $nCcrCodigo;
	}
	public function getCcrCodigo(){
		return $this->nCcrCodigo;
	}
 	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}


    public function getUnidadeDescricao(){
        $oFachada = new FachadaFinanceiroBD();
        $this->oPlanoContas = $oFachada->recuperarUmMnyPlanoContas($this->getCodUnidade());
        return $this->oPlanoContas;

    }
	public function setUnidade($oUnidade){
		$this->oUnidade = $oUnidade;
	}
	public function getUnidade(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oUnidade = $oFachada->recuperarUmMnyPlanoContas($this->getCodUnidade());
		return $this->oUnidade;
	}

 }
 ?>
