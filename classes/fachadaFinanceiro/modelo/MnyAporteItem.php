<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinaceiro 
  * @SGBD mysql 
  * @tabela mny_aporte_item 
  */
 class MnyAporteItem{
 	/**
	* @campo cod_solicitacao_aporte
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodSolicitacaoAporte;
	/**
	* @campo aporte_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nAporteItem;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo assinado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAssinado;
 	/**
	* @campo estornado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEstornado;
     /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnySolicitacaoAporte;
	private $oMnyMovimento;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodSolicitacaoAporte($nCodSolicitacaoAporte){
		$this->nCodSolicitacaoAporte = $nCodSolicitacaoAporte;
	}
	public function getCodSolicitacaoAporte(){
		return $this->nCodSolicitacaoAporte;
	}
	public function setAporteItem($nAporteItem){
		$this->nAporteItem = $nAporteItem;
	}
	public function getAporteItem(){
		return $this->nAporteItem;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
 	public function setEstornado($nEstornado){
		$this->nEstornado = $nEstornado;
	}
	public function getEstornado(){
		return $this->nEstornado;
	}
    public function setAssinado($nAssinado){
		$this->nAssinado = $nAssinado;
	}
	public function getAssinado(){
		return $this->nAssinado;
	}
	public function setMnySolicitacaoAporte($oMnySolicitacaoAporte){
		$this->oMnySolicitacaoAporte = $oMnySolicitacaoAporte;
	}
	public function getMnySolicitacaoAporte(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnySolicitacaoAporte = $oFachada->recuperarUmMnySolicitacaoAporte($this->getCodSolicitacaoAporte());
		return $this->oMnySolicitacaoAporte;
	}
	public function setMnyMovimento($oMnyMovimento){
		$this->oMnyMovimento = $oMnyMovimento;
	}
	public function getMnyMovimento(){
		$oFachada = new FachadaFinaceiroBD();
		$this->oMnyMovimento = $oFachada->recuperarUmMnyMovimento($this->getMovCodigo());
		return $this->oMnyMovimento;
	}
	
 }
 ?>
