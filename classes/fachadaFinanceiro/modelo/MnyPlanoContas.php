<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_plano_contas 
  */
 class MnyPlanoContas{
 	/**
	* @campo plano_contas_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nPlanoContasCodigo;
	/**
	* @campo codigo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCodigo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo visivel
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nVisivel;
	
	/**
	* @campo usu_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sUsuInc;
	/**
	* @campo usu_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sUsuAlt;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPlanoContasCodigo($nPlanoContasCodigo){
		$this->nPlanoContasCodigo = $nPlanoContasCodigo;
	}
	public function getPlanoContasCodigo(){
		return $this->nPlanoContasCodigo;
	}
	public function setCodigo($sCodigo){
		$this->sCodigo = $sCodigo;
	}
	public function getCodigo(){
		return $this->sCodigo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setVisivel($nVisivel){
		$this->nVisivel = $nVisivel;
	}
	public function getVisivel(){
		return $this->nVisivel;
	}
	
	public function setUsuInc($sUsuInc){
		$this->sUsuInc = $sUsuInc;
	}
	public function getUsuInc(){
		return $this->sUsuInc;
	}
	public function setUsuAlt($sUsuAlt){
		$this->sUsuAlt = $sUsuAlt;
	}
	public function getUsuAlt(){
		return $this->sUsuAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
