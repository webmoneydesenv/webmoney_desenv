<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_movimento_doc 
  */
 class MnyMovimentoDoc{
 	/**
	* @campo mov_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo doc_seq
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nDocSeq;
	/**
	* @campo doc_path
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDocPath;
	/**
	* @campo doc_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDocNome;
	/**
	* @campo doc_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDocOrigem;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setDocSeq($nDocSeq){
		$this->nDocSeq = $nDocSeq;
	}
	public function getDocSeq(){
		return $this->nDocSeq;
	}
	public function setDocPath($sDocPath){
		$this->sDocPath = $sDocPath;
	}
	public function getDocPath(){
		return $this->sDocPath;
	}
	public function setDocNome($sDocNome){
		$this->sDocNome = $sDocNome;
	}
	public function getDocNome(){
		return $this->sDocNome;
	}
	public function setDocOrigem($sDocOrigem){
		$this->sDocOrigem = $sDocOrigem;
	}
	public function getDocOrigem(){
		return $this->sDocOrigem;
	}
	
 }
 ?>
