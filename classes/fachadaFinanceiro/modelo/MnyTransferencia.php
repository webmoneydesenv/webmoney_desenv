<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_transferencia 
  */
 class MnyTransferencia{
 	/**
	* @campo cod_transferencia
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodTransferencia;
	/**
	* @campo mov_codigo_origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigoOrigem;
	/**
	* @campo mov_item_origem
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItemOrigem;
	/**
	* @campo de
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDe;
	/**
	* @campo mov_codigo_destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigoDestino;
	/**
	* @campo mov_item_destino
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItemDestino;
	/**
	* @campo para
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPara;
	/**
	* @campo tipo_aplicacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTipoAplicacao;
	/**
	* @campo cod_solicitacao_aporte
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodSolicitacaoAporte;
     /**
	* @campo operacao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sOperacao;
		/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
	private $oMnyMovimentoOrigem;
	private $oMnyMovimentoItemOrigem;
	private $oMnyContaCorrenteOrigem;
	private $oMnyContaCorrenteDestino;
	private $oMnyMovimentoDestino;
	private $oMnyMovimentoItemDestino;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransferencia($nCodTransferencia){
		$this->nCodTransferencia = $nCodTransferencia;
	}
	public function getCodTransferencia(){
		return $this->nCodTransferencia;
	}
	public function setMovCodigoOrigem($nMovCodigoOrigem){
		$this->nMovCodigoOrigem = $nMovCodigoOrigem;
	}
	public function getMovCodigoOrigem(){
		return $this->nMovCodigoOrigem;
	}
	public function setMovItemOrigem($nMovItemOrigem){
		$this->nMovItemOrigem = $nMovItemOrigem;
	}
	public function getMovItemOrigem(){
		return $this->nMovItemOrigem;
	}
	public function setDe($nDe){
		$this->nDe = $nDe;
	}
	public function getDe(){
		return $this->nDe;
	}
	public function setMovCodigoDestino($nMovCodigoDestino){
		$this->nMovCodigoDestino = $nMovCodigoDestino;
	}
	public function getMovCodigoDestino(){
		return $this->nMovCodigoDestino;
	}
	public function setMovItemDestino($nMovItemDestino){
		$this->nMovItemDestino = $nMovItemDestino;
	}
	public function getMovItemDestino(){
		return $this->nMovItemDestino;
	}
	public function setPara($nPara){
		$this->nPara = $nPara;
	}
	public function getPara(){
		return $this->nPara;
	}
	public function setTipoAplicacao($nTipoAplicacao){
		$this->nTipoAplicacao = $nTipoAplicacao;
	}
	public function getTipoAplicacao(){
		return $this->nTipoAplicacao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}	
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setCodSolicitacaoAporte($nCodSolicitacaoAporte){
		$this->nCodSolicitacaoAporte = $nCodSolicitacaoAporte;
	}
	public function getCodSolicitacaoAporte(){
		return $this->nCodSolicitacaoAporte;
	}
    public function setOperacao($sOperacao){
		$this->sOperacao = $sOperacao;
	}	
	public function getOperacao(){
		return $this->sOperacao;
	}
	public function setMnyMovimentoDestino($oMnyMovimentoDestino){
		$this->oMnyMovimentoDestino = $oMnyMovimentoDestino;
	}
	public function getMnyMovimentoDestino(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoDestino = $oFachada->recuperarUmMnyMovimento($this->getMovCodigoDestino());
		return $this->oMnyMovimentoDestino;
	}
	public function setMnyMovimentoItemDestino($oMnyMovimentoItemDestino){
		$this->oMnyMovimentoItemDestino = $oMnyMovimentoItemDestino;
	}
	public function getMnyMovimentoItemDestino(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItemDestino = $oFachada->recuperarUmMnyMovimentoItem($this->getMovItemDestino());
		return $this->oMnyMovimentoItemDestino;
	}
	public function setMnyContaCorrenteDestino($oMnyContaCorrenteDestino){
		$this->oMnyContaCorrenteDestino = $oMnyContaCorrenteDestino;
	}
	public function getMnyContaCorrenteDestino(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContaCorrenteDestino = $oFachada->recuperarUmMnyContaCorrente($this->getPara());
		return $this->oMnyContaCorrenteDestino;
	}
	public function setMnyContaCorrenteOrigem($oMnyContaCorrenteOrigem){
		$this->oMnyContaCorrenteOrigem = $oMnyContaCorrenteOrigem;
	}
	public function getMnyContaCorrenteOrigem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContaCorrenteOrigem = $oFachada->recuperarUmMnyContaCorrente($this->getDe());
		return $this->oMnyContaCorrenteOrigem;
	}
	public function setMnyMovimentoOrigem($oMnyMovimentoOrigem){
		$this->oMnyMovimentoOrigem = $oMnyMovimentoOrigem;
	}
	public function getMnyMovimentoOrigem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoOrigem = $oFachada->recuperarUmMnyMovimento($this->getMovCodigoOrigem());
		return $this->oMnyMovimentoOrigem;
	}
	public function setMnyMovimentoItemOrigem($oMnyMovimentoItemOrigem){
		$this->oMnyMovimentoItemOrigem = $oMnyMovimentoItemOrigem;
	}
	public function getMnyMovimentoItemOrigem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItemOrigem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovCodigoOrigem(),$this->getMovItemOrigem());
		return $this->oMnyMovimentoItemOrigem;
	}

	
 }
 ?>
