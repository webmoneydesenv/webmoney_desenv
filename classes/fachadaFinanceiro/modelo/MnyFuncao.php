<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_funcao 
  */
 class MnyFuncao{
 	/**
	* @campo fun_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nFunCodigo;
	/**
	* @campo fun_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFunNome;
	/**
	* @campo fun_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFunInc;
	/**
	* @campo fun_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sFunAlt;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setFunCodigo($nFunCodigo){
		$this->nFunCodigo = $nFunCodigo;
	}
	public function getFunCodigo(){
		return $this->nFunCodigo;
	}
	public function setFunNome($sFunNome){
		$this->sFunNome = $sFunNome;
	}
	public function getFunNome(){
		return $this->sFunNome;
	}
	public function setFunInc($sFunInc){
		$this->sFunInc = $sFunInc;
	}
	public function getFunInc(){
		return $this->sFunInc;
	}
	public function setFunAlt($sFunAlt){
		$this->sFunAlt = $sFunAlt;
	}
	public function getFunAlt(){
		return $this->sFunAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
