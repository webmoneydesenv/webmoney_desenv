<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_conta_caixa 
  */
 class MnyContaCaixa{
 	/**
	* @campo cod_conta_caixa
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodContaCaixa;
	/**
	* @campo cod_unidade
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo data_conta
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataConta;
	/**
	* @campo credito_debito
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCreditoDebito;
		/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValor;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/	
	private $sDescricao;
	/**
	* @campo doc_pendente
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDocPendente;
	/**
	* @campo cod_arquivo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodArquivo;
	/**
	* @campo inserido_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sInseridoPor;
	/**
	* @campo alterado_por
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAlteradoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyPlanoContas;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodContaCaixa($nCodContaCaixa){
		$this->nCodContaCaixa = $nCodContaCaixa;
	}
	public function getCodContaCaixa(){
		return $this->nCodContaCaixa;
	}
	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	
	public function setDataConta($dDataConta){
		$this->dDataConta = $dDataConta;
	}
	public function getDataConta(){
		return $this->dDataConta;
	}
	
	public function getDataContaFormatado(){
		if($this->dDataConta){
		$oData = new DateTime($this->dDataConta);
		 return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	public function setDataContaBanco($dDataConta){
		if($dDataConta){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataConta);
			 $this->dDataConta = $oData->format('Y-m-d') ;
		}else{
			$this->dDataConta = 'NULL';	
		}
	}
	
	public function setCreditoDebito($sCreditoDebito){
		$this->sCreditoDebito = $sCreditoDebito;
	}
	public function getCreditoDebito(){
		return $this->sCreditoDebito;
	}
	public function setValor($nValor){
		$this->nValor= $nValor;
	}
	public function setValorBanco($nValor){
		if($nValor){	
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);
		}else{
			 $this->nValor = 'null';
		}	
	}


	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		 $vRetorno = number_format($this->nValor , 2, ',', '.');
		 return $vRetorno;	
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setDocPendente($sDocPendente){
		$this->sDocPendente = $sDocPendente;
	}
	public function getDocPendente(){
		return $this->sDocPendente;
	}
	public function setCodArquivo($nCodArquivo){
		$this->nCodArquivo = $nCodArquivo;
	}
	public function getCodArquivo(){
		return $this->nCodArquivo;
	}
	public function setInseridoPor($sInseridoPor){
		$this->sInseridoPor = $sInseridoPor;
	}
	public function getInseridoPor(){
		return $this->sInseridoPor;
	}
	public function setAlteradoPor($sAlteradoPor){
		$this->sAlteradoPor = $sAlteradoPor;
	}
	public function getAlteradoPor(){
		return $this->sAlteradoPor;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMnyPlanoContas($oMnyPlanoContas){
		$this->oMnyPlanoContas = $oMnyPlanoContas;
	}
	public function getMnyPlanoContas(){
		$oFachada = new FachadaFachadaFinanceiroBD();
		$this->oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($this->getCodUnidade());
		return $this->oMnyPlanoContas;
	}
	
 }
 ?>
