<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_contrato_ase 
  */
 class MnyContratoAse{
 	/**
	* @campo contrato_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nContratoCodigo;
	/**
	* @campo item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nItem;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyContratoPessoa;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setContratoCodigo($nContratoCodigo){
		$this->nContratoCodigo = $nContratoCodigo;
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
	public function setItem($nItem){
		$this->nItem = $nItem;
	}
	public function getItem(){
		return $this->nItem;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		 $vRetorno = number_format($this->nValor , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);
	
		}else{
		$this->nValor = 'null';
			}
		}
public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMnyContratoPessoa($oMnyContratoPessoa){
		$this->oMnyContratoPessoa = $oMnyContratoPessoa;
	}
	public function getMnyContratoPessoa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($this->getContratoCodigo());
		return $this->oMnyContratoPessoa;
	}
	
 }
 ?>
