<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_pessoa_colaborador 
  */
 class MnyPessoaColaborador{
 	/**
	* @campo pesc_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nPescCodigo;

	/**
	* @campo pesc_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPescNome;
	/**
	* @campo pesc_cpf
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPescCpf;
	/**
	* @campo pesc_mat
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPescMat;
	/**
	* @campo fun_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFunCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo pesc_sal
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPescSal;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo admissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dAdmissao;
	/**
	* @campo demissao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDemissao;
	/**
	* @campo valext
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sValext;
	/**
	* @campo motivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMotivo;
	private $oMnyFuncao;
	private $oMnyUnidadePlanocontas;
	private $oMnySetorPlanocontas;
	private $oMnyPessoaGeral;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPescCodigo($nPescCodigo){
		$this->nPescCodigo = $nPescCodigo;
	}
	public function getPescCodigo(){
		return $this->nPescCodigo;
	}
	public function setPescNome($sPescNome){
		$this->sPescNome = $sPescNome;
	}
	public function getPescNome(){
		return $this->sPescNome;
	}
	public function setPescCpf($sPescCpf){
		$this->sPescCpf = $sPescCpf;
	}
	public function getPescCpf(){
		return $this->sPescCpf;
	}
	public function setPescMat($sPescMat){
		$this->sPescMat = $sPescMat;
	}
	public function getPescMat(){
		return $this->sPescMat;
	}
	public function setFunCodigo($nFunCodigo){
		$this->nFunCodigo = $nFunCodigo;
	}
	public function getFunCodigo(){
		return $this->nFunCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		$this->nUniCodigo = $nUniCodigo;
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setPescSal($sPescSal){
		$this->sPescSal = $sPescSal;
	}
	public function getPescSal(){
		return $this->sPescSal;
	}
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}
	public function setAdmissao($dAdmissao){
		$this->dAdmissao = $dAdmissao;
	}
	public function getAdmissao(){
		return $this->dAdmissao;
	}
	public function getAdmissaoFormatado(){
		$oData = new DateTime($this->dAdmissao);
		 return $oData->format("d/m/Y");
	}
	public function setAdmissaoBanco($dAdmissao){
		 $oData = DateTime::createFromFormat('d/m/Y', $dAdmissao);
		 $this->dAdmissao = $oData->format('Y-m-d') ;
	}
	public function setDemissao($dDemissao){
		$this->dDemissao = $dDemissao;
	}
	public function getDemissao(){
		return $this->dDemissao;
	}
	public function getDemissaoFormatado(){
		if($this->dDemissao){
			$oData = new DateTime($this->dDemissao);
			return $oData->format("d/m/Y");
		}
	}
	public function setDemissaoBanco($dDemissao){
		 if($dDemissao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDemissao);
			 $this->dDemissao = $oData->format('Y-m-d') ;
		 }
		 
	}
	public function setValext($sValext){
		$this->sValext = $sValext;
	}
	public function getValext(){
		return $this->sValext;
	}
	public function setMotivo($sMotivo){
		$this->sMotivo = $sMotivo;
	}
	public function getMotivo(){
		return $this->sMotivo;
	}
	public function setMnyFuncao($oMnyFuncao){
		$this->oMnyFuncao = $oMnyFuncao;
	}
	public function getMnyFuncao(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyFuncao = $oFachada->recuperarUmMnyFuncao($this->getFunCodigo());
		return $this->oMnyFuncao;
	}
	public function setMnyUnidadePlanocontas($oMnyUnidadePlanocontas){
		$this->oMnyUnidadePlanocontas = $oMnyUnidadePlanocontas;
	}
	public function getMnyUnidadePlanocontas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyUnidadePlanocontas = $oFachada->recuperarUmMnyPlanoContas($this->getUniCodigo());
		return $this->oMnyUnidadePlanocontas;
	}
	public function setMnySetorPlanocontas($oMnySetorPlanocontas){
		$this->oMnySetorPlanocontas = $oMnySetorPlanocontas;
	}
	public function getMnySetorPlanocontas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnySetorPlanocontas = $oFachada->recuperarUmMnyPlanoContas($this->getSetCodigo());
		return $this->oMnySetorPlanocontas;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmMnyPessoaGeral($this->getPescCodigo());
		return $this->oMnyPessoaGeral;
	}
	
 }
 ?>
