<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_saldo_diario_aplicacao 
  */
 class MnySaldoDiarioAplicacao{
 	/**
	* @campo cod_saldo_diario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodSaldoDiario;
	/**
	* @campo cod_aplicacao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodAplicacao;
	/**
	* @campo valor_rendimento
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorRendimento;	
	/**
	* @campo valor
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValor;
	/**
	* @campo operacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sOperacao;
	/**
	* @campo valor_final
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorFinal;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodSaldoDiario($nCodSaldoDiario){
		$this->nCodSaldoDiario = $nCodSaldoDiario;
	}
	public function getCodSaldoDiario(){
		return $this->nCodSaldoDiario;
	}
	public function setCodAplicacao($nCodAplicacao){
		$this->nCodAplicacao = $nCodAplicacao;
	}
	public function getCodAplicacao(){
		return $this->nCodAplicacao;
	}
	public function setValorRendimento($nValorRendimento){
		$this->nValorRendimento = $nValorRendimento;
	}
	public function getValorRendimento(){
		return $this->nValorRendimento;
	}
	public function getValorRendimentoFormatado(){
		 $vRetorno = number_format($this->nValorRendimento , 2, ',', '.');
		 return $vRetorno;
	}	
	public function setValorRendimentoBanco($nValorRendimento){
		if($nValorRendimento){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorRendimento = str_replace($sOrigem, $sDestino, $nValorRendimento);
	
		}else{
		$this->nValorRendimento = 'null';
			}
		}	
	public function setValor($nValor){
		$this->nValor = $nValor;
	}
	public function getValor(){
		return $this->nValor;
	}
	public function getValorFormatado(){
		 $vRetorno = number_format($this->nValor , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorBanco($nValor){
		if($nValor){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValor = str_replace($sOrigem, $sDestino, $nValor);
	
		}else{
		$this->nValor = 'null';
			}
		}
    public function setOperacao($sOperacao){
		$this->sOperacao = $sOperacao;
	}
	public function getOperacao(){
		return $this->sOperacao;
	}
    public function setValorFinal($nValorFinal){
		$this->nValorFinal = $nValorFinal;
	}
	public function getValorFinal(){
		return $this->nValorFinal;
	}
	public function getValorFinalFormatado(){
		 $vRetorno = number_format($this->nValorFinal , 2, ',', '.');
		 return $vRetorno;
	}	
	public function setValorFinalBanco($nValorFinal){
		if($nValorFinal){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorFinal = str_replace($sOrigem, $sDestino, $nValorFinal);
	
		}else{
			$this->nValorFinal = 'null';
		}
	}	
	
 }
 ?>
