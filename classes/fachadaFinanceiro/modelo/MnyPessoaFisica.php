<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_pessoa_fisica 
  */
 class MnyPessoaFisica{
 	/**
	* @campo pesfis_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nPesfisCodigo;
	/**
	* @campo pesfis_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesfisNome;
	/**
	* @campo pesfis_cpf
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesfisCpf;

	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPesfisCodigo($nPesfisCodigo){
		$this->nPesfisCodigo = $nPesfisCodigo;
	}
	public function getPesfisCodigo(){
		return $this->nPesfisCodigo;
	}
	
	public function setPesfisNome($sPesfisNome){
		$this->sPesfisNome = $sPesfisNome;
	}
	public function getPesfisNome(){
		return $this->sPesfisNome;
	}
	public function setPesfisCpf($sPesfisCpf){
		$this->sPesfisCpf = $sPesfisCpf;
	}
	public function getPesfisCpf(){
		return $this->sPesfisCpf;
	}

	
 }
 ?>
