<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinaceiro 
  * @SGBD mysql 
  * @tabela mny_solicitacao_aporte 
  */
 class MnySolicitacaoAporte{
 	/**
	* @campo cod_solicitacao
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodSolicitacao;
	/**
	* @campo data_solicitacao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataSolicitacao;

	/**
	* @campo numero_solicitacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nNumeroSolicitacao;


	/**
	* @campo numero_aporte
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNumeroAporte;

	/**
	* @campo sol_inc
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSolInc;
	/**
	* @campo sol_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sSolAlt;
   /**
	* @campo liberado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nLiberado;
     /**
	* @campo assinado
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAssinado;
    /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;

	private $oSysEmpresa;
	private $oMnyPlanoContas;
	private $nAporteItem;
	private $oMnyTransferencia;
	private $oMnyAporte;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodSolicitacao($nCodSolicitacao){
		$this->nCodSolicitacao = $nCodSolicitacao;
	}
	public function getCodSolicitacao(){
		return $this->nCodSolicitacao;
	}
	public function setDataSolicitacao($dDataSolicitacao){
		$this->dDataSolicitacao = $dDataSolicitacao;
	}
	public function getDataSolicitacao(){
		return $this->dDataSolicitacao;
	}
	public function getDataSolicitacaoFormatado(){
		if($this->dDataSolicitacao){
            $oData = new DateTime($this->dDataSolicitacao);
		    return $oData->format("d/m/Y");
        }
	}
	public function setDataSolicitacaoBanco($dDataSolicitacao){
		 if($dDataSolicitacao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataSolicitacao);
			 $this->dDataSolicitacao = $oData->format('Y-m-d') ;
	}
		 }

	public function setNumeroSolicitacao($nNumeroSolicitacao){
		$this->nNumeroSolicitacao = $nNumeroSolicitacao;
	}
	public function getNumeroSolicitacao(){
		return $this->nNumeroSolicitacao;
	}

	public function setNumeroAporte($sNumeroAporte){
		$this->sNumeroAporte = $sNumeroAporte;
	}
	public function getNumeroAporte(){
		return $this->sNumeroAporte;
	}

	public function getNumeroAporteFormatado($nCodUnidade){
        $oFachada = new FachadaFinanceiroBD();
        if($nCodUnidade){
            $sUnidade = substr($oFachada->recuperarUmMnyPlanoContas($nCodUnidade)->getCodigo(),4);
            $vData = explode('-',$this->dDataSolicitacao);
            $sSolicitacao = "SA.".$vData[0].".". $sUnidade .".". str_pad((int) $this->nNumeroSolicitacao,3,"0",STR_PAD_LEFT);
            return $sSolicitacao;
        }
	}

	public function setSolInc($sSolInc){
		$this->sSolInc = $sSolInc;
	}
	public function getSolInc(){
		return $this->sSolInc;
	}
	public function setSolAlt($sSolAlt){
		$this->sSolAlt = $sSolAlt;
	}
	public function getSolAlt(){
		return $this->sSolAlt;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
    public function setLiberado($nLiberado){
		$this->nLiberado = $nLiberado;
	}
	public function getLiberado(){
		return $this->nLiberado;
	}
    public function setAssinado($nAssinado){
		$this->nAssinado = $nAssinado;
	}
	public function getAssinado(){
		return $this->nAssinado;
	}


	public function getMnyAporteItemPorMaxItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->nAporteItem = $oFachada->recuperarUmMnyAporteItemPorMaxItem($this->getCodSolicitacao())->getAporteItem();
		return $this->nAporteItem;
	}

    public function getUnidadeAporte(){

        $oFachada = new FachadaFinanceiroBD();
        //retorna a unidade em que o aporte faz parte...
        return ($this->getCodSolicitacao()) ? $oFachada->recuperarUmMnyMovimento($oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($this->getCodSolicitacao())->getMovCodigoDestino())->getUniCodigo() : "";
	}

    public function getTransferenciaAporte(){

        $oFachada = new FachadaFinanceiroBD();
        $this->oMnyTransferencia = $oFachada->recuperarUmMnytransferenciaPorSoliticatacaoAporte($this->getCodSolicitacao());
        return $this->oMnyTransferencia;
	}

  /*  public function getValorGlobalAporte(){

        $oFachada = new FachadaFinanceiroBD();
        $this->oMnyAporte = $oFachada->recuperarUmMnySolicitacaoAporteValorGlobal($this->getCodSolicitacao())->getSolAlt();
        return $this->oMnyAporte;
	}
	*/
 }
 ?>
