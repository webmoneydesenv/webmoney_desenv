<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_contrato_doc_tipo 
  */
 class MnyContratoDocTipo{
 	/**
	* @campo cod_contrato_doc_tipo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodContratoDocTipo;
 	/**
	* @campo cod_contrato_tipo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nCodContratoTipo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo obrigatorio
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nObrigatorio;
	/**
	* @campo acao_inicial
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAcaoInicial;
	/**
	* @campo item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nItem;
	/**
	* @campo tamanho_arquivo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTamanhoArquivo;
    /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyContratoTipo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodContratoDocTipo($nCodContratoDocTipo){
		$this->nCodContratoDocTipo = $nCodContratoDocTipo;
	}
	public function getCodContratoDocTipo(){
		return $this->nCodContratoDocTipo;
	}
 	public function setCodContratoTipo($nCodContratoTipo){
		$this->nCodContratoTipo = $nCodContratoTipo;
	}
	public function getCodContratoTipo(){
		return $this->nCodContratoTipo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setObrigatorio($nObrigatorio){
		$this->nObrigatorio = $nObrigatorio;
	}
	public function getObrigatorio(){
		return $this->nObrigatorio;
	}
	public function getObrigatorioFormatado(){
		if($this->nObrigatorio == '1')	
			$this->nObrigatorio = "SIM";
		else
			$this->nObrigatorio = "N&Atilde;O";
		return $this->nObrigatorio;
	}
	public function setAcaoInicial($nAcaoInicial){
		$this->nAcaoInicial = $nAcaoInicial;
	}
	public function getAcaoInicial(){
		return $this->nAcaoInicial;
	}	
	public function getAcaoInicialFormatado(){
		if($this->nAcaoInicial == '1')	
			$this->nAcaoInicial = "SIM";
		else
			$this->nAcaoInicial = "N&Atilde;O";
		return $this->nAcaoInicial;
	}
	public function setItem($nItem){
		$this->nItem = $nItem;
	}
	public function getItem(){
		return $this->nItem;
	}	
	public function getItemFormatado(){
		if($this->nItem == '1')	
			$this->nItem = "SIM";
		else
			$this->nItem = "N&Atilde;O";
		return $this->nItem;
	}
	
	public function setTamanhoArquivo($nTamanhoArquivo){
		$this->nTamanhoArquivo = $nTamanhoArquivo;
	}
	public function getTamanhoArquivo(){
		return $this->nTamanhoArquivo;
	}
    public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMnyContratoTipo($oMnyContratoTipo){
		$this->oMnyContratoTipo = $oMnyContratoTipo;
	}
	public function getMnyContratoTipo(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoTipo = $oFachada->recuperarUmMnyContratoTipo($this->getCodContratoTipo());
		return $this->oMnyContratoTipo;
	}
	
	
 }
 ?>
