<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_pessoa_geral 
  */
 class MnyPessoaGeral{
 	/**
	* @campo pesg_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nPesgCodigo;
	/**
	* @campo tip_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipCodigo;
	/**
	* @campo cod_status
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodStatus;
	/**
	* @campo pesg_end_logra
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEndLogra;
	/**
	* @campo pesg_end_bairro
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEndBairro;
	/**
	* @campo pesg_end_cep
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEndCep;
	/**
	* @campo cidade
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sCidade;
	/**
	* @campo estado
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sEstado;
	/**
	* @campo pesg_email
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgEmail;
	/**
	* @campo pes_fones
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesFones;
	/**
	* @campo pesg_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesgInc;
	/**
	* @campo pesg_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPesgAlt;
	/**
	* @campo bco_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nBcoCodigo;
	/**
	* @campo tipocon_cod
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nTipoconCod;
	/**
	* @campo pesg_bco_agencia
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPesgBcoAgencia;
	/**
	* @campo pesg_bco_conta
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPesgBcoConta;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyTipoPessoaPlanocontas;
	private $oSysBanco;
	private $oSysContaTipoPlanocontas;
	private $oVPessoaGeralFormatada;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPesgCodigo($nPesgCodigo){
		$this->nPesgCodigo = $nPesgCodigo;
	}
	public function getPesgCodigo(){
		return $this->nPesgCodigo;
	}
	public function setTipCodigo($nTipCodigo){
		$this->nTipCodigo = $nTipCodigo;
	}
	public function getTipCodigo(){
		return $this->nTipCodigo;
	}
	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setPesgEndLogra($sPesgEndLogra){
		$this->sPesgEndLogra = $sPesgEndLogra;
	}
	public function getPesgEndLogra(){
		return $this->sPesgEndLogra;
	}
	public function setPesgEndBairro($sPesgEndBairro){
		$this->sPesgEndBairro = $sPesgEndBairro;
	}
	public function getPesgEndBairro(){
		return $this->sPesgEndBairro;
	}
	public function setPesgEndCep($sPesgEndCep){
		$this->sPesgEndCep = $sPesgEndCep;
	}
	public function getPesgEndCep(){
		return $this->sPesgEndCep;
	}
	public function setCidade($sCidade){
		$this->sCidade = $sCidade;
	}
	public function getCidade(){
		return $this->sCidade;
	}
	public function setEstado($sEstado){
		$this->sEstado = $sEstado;
	}
	public function getEstado(){
		return $this->sEstado;
	}
	public function setPesgEmail($sPesgEmail){
		$this->sPesgEmail = $sPesgEmail;
	}
	public function getPesgEmail(){
		return $this->sPesgEmail;
	}
	public function setPesFones($sPesFones){
		$this->sPesFones = $sPesFones;
	}
	public function getPesFones(){
		return $this->sPesFones;
	}
	public function setPesgInc($sPesgInc){
		$this->sPesgInc = $sPesgInc;
	}
	public function getPesgInc(){
		return $this->sPesgInc;
	}
	public function setPesgAlt($sPesgAlt){
		$this->sPesgAlt = $sPesgAlt;
	}
	public function getPesgAlt(){
		return $this->sPesgAlt;
	}
	public function setBcoCodigo($nBcoCodigo){
		$this->nBcoCodigo = $nBcoCodigo;
	}
	public function getBcoCodigo(){
		return $this->nBcoCodigo;
	}
	public function setTipoconCod($nTipoconCod){
		$this->nTipoconCod = $nTipoconCod;
	}
	public function getTipoconCod(){
		return $this->nTipoconCod;
	}
	public function setPesgBcoAgencia($sPesgBcoAgencia){
		$this->sPesgBcoAgencia = $sPesgBcoAgencia;
	}
	public function getPesgBcoAgencia(){
		return $this->sPesgBcoAgencia;
	}
	public function setPesgBcoConta($sPesgBcoConta){
		$this->sPesgBcoConta = $sPesgBcoConta;
	}
	public function getPesgBcoConta(){
		return $this->sPesgBcoConta;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMnyTipoPessoaPlanocontas($oMnyTipoPessoaPlanocontas){
		$this->oMnyTipoPessoaPlanocontas = $oMnyTipoPessoaPlanocontas;
	}
	public function getMnyTipoPessoaPlanocontas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyTipoPessoaPlanocontas = $oFachada->recuperarUmMnyPlanoContas($this->getTipCodigo());
		return $this->oMnyTipoPessoaPlanocontas;
	}
	public function setSysBanco($oSysBanco){
		$this->oSysBanco = $oSysBanco;
	}
	public function getSysBanco(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oSysBanco = $oFachada->recuperarUmSysBanco($this->getBcoCodigo());
		return $this->oSysBanco;
	}
	public function setSysContaTipoPlanocontas($oSysContaTipoPlanocontas){
		$this->oSysContaTipoPlanocontas = $oSysContaTipoPlanocontas;
	}
	public function getSysContaTipoPlanocontas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oSysContaTipoPlanocontas = $oFachada->recuperarUmMnyPlanoContas($this->getTipoconCod());
		return $this->oSysContaTipoPlanocontas;
	}
	
	public function getVPessoaGeralFormatada(){
		$oFachada = new FachadaViewBD();
		$this->oVPessoaGeralFormatada = $oFachada->recuperarUmVPessoaGeralFormatada($this->getPesgCodigo());
		return $this->oVPessoaGeralFormatada;
	}
 }
 ?>
