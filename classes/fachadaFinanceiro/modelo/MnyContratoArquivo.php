<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_arquivo 
  */
 class MnyContratoArquivo{
 	/**
	* @campo cod_arquivo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodContratoArquivo;
	/**
	* @campo contrato_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nContratoCodigo;
	/**
	* @campo cod_conciliacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodConciliacao;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo cod_contrato_doc_tipo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodContratoDocTipo;
	/**
	* @campo nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sNome;

	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyContratoPessoa;
	private $oMnyConciliacao;
	private $oMnyMovimentoItem;
	private $oMnyContratoDocTipo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodContratoArquivo($nCodContratoArquivo){
		$this->nCodContratoArquivo = $nCodContratoArquivo;
	}
	public function getCodContratoArquivo(){
		return $this->nCodContratoArquivo;
	}
	public function setContratoCodigo($nContratoCodigo){
		if($nContratoCodigo)
			$this->nContratoCodigo = $nContratoCodigo;
		else
			$this->nContratoCodigo = "NULL";		
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
	public function setCodConciliacao($nCodConciliacao){
		if($nCodConciliacao)
			$this->nCodConciliacao = $nCodConciliacao;
		else
			$this->nCodConciliacao = "NULL";		
	}
	public function getCodConciliacao(){
		return $this->nCodConciliacao;
	}
	public function setMovCodigo($nMovCodigo){
		if($nMovCodigo)
			$this->nMovCodigo = $nMovCodigo;
		else
			$this->nMovCodigo = "NULL";		
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		if($nMovItem)
			$this->nMovItem = $nMovItem;
		else
			$this->nMovItem = "NULL";	

}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setCodContratoDocTipo($nCodContratoDocTipo){
		$this->nCodContratoDocTipo = $nCodContratoDocTipo;
	}
	public function getCodContratoDocTipo(){
		return $this->nCodContratoDocTipo;
	}
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	public function getNome(){
		return $this->sNome;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}

	public function setMnyContratoPessoa($oMnyContratoPessoa){
		$this->oMnyContratoPessoa = $oMnyContratoPessoa;
	}
	public function getMnyContratoPessoa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($this->getContratoCodigo());
		return $this->oMnyContratoPessoa;
	}
	public function setMnyMovimentoItem($oMnyMovimentoItem){
		$this->oMnyMovimentoItem = $oMnyMovimentoItem;
	}
	public function getMnyMovimentoItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovCodigo(),$this->getMovItem());
		return $this->oMnyMovimentoItem;
	}
	public function setMnyContratoDocTipo($oMnyContratoDocTipo){
		$this->oMnyContratoDocTipo = $oMnyContratoDocTipo;
	}
	public function getMnyContratoDocTipo(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoDocTipo = $oFachada->recuperarUmMnyContratoDocTipo($this->getCodContratoDocTipo());
		return $this->oMnyContratoDocTipo;
	}
	
	public function setMnyConciliacao($oMnyConciliacao){
		$this->oMnyConciliacao = $oMnyConciliacao;
	}
	public function getMnyConciliacao(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyConciliacao = $oFachada->recuperarUmMnyConciliacao($this->getConciliacao());
		return $this->oMnyConciliacao;
	}
 }
 ?>
