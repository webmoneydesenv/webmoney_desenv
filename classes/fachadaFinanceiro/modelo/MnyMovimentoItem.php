<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_movimento_item 
  */
 class MnyMovimentoItem{
 	/**
	* @campo mov_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo mov_data_vencto
	* @var data
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataVencto;
	/**
	* @campo mov_data_prev
	* @var data
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataPrev;
	/**
	* @campo mov_valor_parcela
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorParcela;
	/**
	* @campo mov_juros
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovJuros;
	/**
	* @campo mov_valor_pagar
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorPagar;
	/**
	* @campo fpg_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFpgCodigo;
	/**
	* @campo tip_doc_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipDocCodigo;
	/**
	* @campo mov_retencao
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovRetencao;
	/**
	* @campo mov_data_inclusao
	* @var data
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataInclusao;
	/**
	* @campo mov_valor_tarifa_banco
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovValorTarifaBanco;
	
	/**
	* @campo tip_ace_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipAceCodigo;
	/**
	* @campo competencia
	* @var data
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dCompetencia;
	
	/**
	* @campo contabil
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nContabil;
	/**
	* @campo nf
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nNf;
    /**
	* @campo finalizado
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nFinalizado;
   /**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
     /**
	* @campo caixinha
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCaixinha;
	 /**
	* @campo fa_origem
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sFaOrigem;
   /**
	* @campo conciliado
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nConciliado;
	
	private $oMnyMovimento;
	private $oMnyPlanoContasTipoDocumento;
	private $oMnyPlanoContasFormaPagamento;
	private $oMnyPlanoContasTipoAceite;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setMovDataVencto($dMovDataVencto){
		$this->dMovDataVencto = $dMovDataVencto;
	}
	public function getMovDataVencto(){
		return $this->dMovDataVencto;
	}
	public function getMovDataVenctoFormatado(){
		$oData = new DateTime($this->dMovDataVencto);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataVenctoBanco($dMovDataVencto){
		 if($dMovDataVencto){		
			 if($dMovDataVencto != date('Y-m-d')){ // data que vem da transferencia...
				 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataVencto);
				 $this->dMovDataVencto = $oData->format('Y-m-d');
			 }else{
				 $this->dMovDataVencto = date('Y-m-d');
			 }
		}
	}
	public function setMovDataPrev($dMovDataPrev){
		$this->dMovDataPrev = $dMovDataPrev;
	}
	public function getMovDataPrev(){
		return $this->dMovDataPrev;
	}
	public function getMovDataPrevFormatado(){
        if($this->dMovDataPrev){
		$oData = new DateTime($this->dMovDataPrev);
		 return $oData->format("d/m/Y");

         }else{
             return "";
         }
	}
	public function setMovDataPrevBanco($dMovDataPrev){
        if($dMovDataPrev){
             if($dMovDataPrev != date('Y-m-d')){ // data que vem da transferencia...
                 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataPrev);
                 $this->dMovDataPrev = $oData->format('Y-m-d');
             }else{
                 $this->dMovDataPrev = date('Y-m-d');
             }
        }
	}
	public function setMovValorParcela($nMovValorParcela){
		$this->nMovValorParcela = $nMovValorParcela;
	}
	public function getMovValorParcela(){
		return $this->nMovValorParcela;
	}
	public function getMovValorParcelaFormatado(){
		 $vRetorno = number_format($this->nMovValorParcela , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorParcelaBanco($nMovValorParcela){
		if($nMovValorParcela){	
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorParcela = str_replace($sOrigem, $sDestino, $nMovValorParcela);
		}else{
			 $this->nMovValorParcela = 'null';
		}
	}

	public function getMovValorParcelaLiquidaFormatado(){
		$oFachada = new FachadaFinanceiroBD();
		$vRetorno = number_format($oFachada->recuperarUmMnyMovimentoItemValorLiquido($this->getMovCodigo(),$this->getMovItem())->getMovValorParcela() , 2, ',', '.');
		 return $vRetorno;
	}

	public function setMovJuros($nMovJuros){
		$this->nMovJuros = $nMovJuros;
	}
	public function getMovJuros(){
		return $this->nMovJuros;
	}
	public function getMovJurosFormatado(){
		 $vRetorno = number_format($this->nMovJuros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovJurosBanco($nMovJuros){
		if($nMovJuros){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovJuros = str_replace($sOrigem, $sDestino, $nMovJuros);
		}else{
			 $this->nMovJuros = 'null';
		}
	}
	public function setMovValorPagar($nMovValorPagar){
		$this->nMovValorPagar = $nMovValorPagar;
	}
	public function getMovValorPagar(){
		return $this->nMovValorPagar;
	}
	public function getMovValorPagarFormatado(){
		 $vRetorno = number_format($this->nMovValorPagar , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorPagarBanco($nMovValorPagar){
		if($nMovValorPagar){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorPagar = str_replace($sOrigem, $sDestino, $nMovValorPagar);
		}else{
			 $this->nMovValorPagar = 'null';
		}
	}
	public function setFpgCodigo($nFpgCodigo){
		$this->nFpgCodigo = $nFpgCodigo;
	}
	public function getFpgCodigo(){
		return $this->nFpgCodigo;
	}
	public function setTipDocCodigo($nTipDocCodigo){
		$this->nTipDocCodigo = $nTipDocCodigo;
	}
	public function getTipDocCodigo(){
		return $this->nTipDocCodigo;
	}
	public function setMovRetencao($nMovRetencao){
		$this->nMovRetencao = $nMovRetencao;
	}
	public function getMovRetencao(){
		return $this->nMovRetencao;
	}
	public function getMovRetencaoFormatado(){
		 $vRetorno = number_format($this->nMovRetencao , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovRetencaoBanco($nMovRetencao){
		if($nMovRetencao){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovRetencao = str_replace($sOrigem, $sDestino, $nMovRetencao);
		}else{
			 $this->nMovRetencao = 'null';
		}
	}
	public function setMovDataInclusao($dMovDataInclusao){
		$this->dMovDataInclusao = $dMovDataInclusao;
	}
	public function getMovDataInclusao(){
		return $this->dMovDataInclusao;
	}
	public function getMovDataInclusaoFormatado(){
		 $oData = new DateTime($this->dMovDataInclusao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataInclusaoBanco($dMovDataInclusao){
		if($dMovDataInclusao){
			 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataInclusao);			
			 $this->dMovDataInclusao = $oData->format('Y-m-d') ;
		}
	}
	
	public function setMovValorTarifaBanco($nMovValorTarifaBanco){
		$this->nMovValorTarifaBanco = $nMovValorTarifaBanco;
	}
	public function getMovValorTarifaBanco(){
		return $this->nMovValorTarifaBanco;
	}
	public function getMovValorTarifaBancoFormatado(){
		 $vRetorno = number_format($this->nMovValorTarifaBanco , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovValorTarifaBancoBanco($nMovValorTarifaBanco){
		if($nMovValorTarifaBanco){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nMovValorTarifaBanco = str_replace($sOrigem, $sDestino, $nMovValorTarifaBanco);
		}else{
			 $this->nMovValorTarifaBanco = 'null';
		}
	}
	
	public function setTipAceCodigo($nTipAceCodigo){
		$this->nTipAceCodigo = $nTipAceCodigo;
	}
	public function getTipAceCodigo(){
		return $this->nTipAceCodigo;
	}
	
	public function setCompetencia($dCompetencia){
		$this->dCompetencia = $dCompetencia;
	}
	public function getCompetencia(){
		return $this->dCompetencia;
	}
	public function getCompetenciaFormatado(){
		if($this->dCompetencia){
			 $oData = new DateTime($this->dCompetencia);
			 return $oData->format("m/Y");
		}else{
			return "";
		}
	}
	public function setCompetenciaBanco($dCompetencia){		
		if($dCompetencia){
				$dCompetencia = "01/" .$dCompetencia; 
	
				$oData = new DateTime($dCompetencia);		  		    
				$oData = date('Y-d-m', strtotime($dCompetencia));	
				$this->dCompetencia = $oData;			
/*			if($dCompetencia != date('Y-m-d')){
	  		    $oData = DateTime::createFromFormat('d/m/Y', $dCompetencia);						
				$this->dCompetencia = $oData->format('Y-m-d');				
			}else{
				$this->dCompetencia = date('Y-m-d');				
			}
*/		
		}else{
			$this->dCompetencia  = 'null';
		}
	}
			
	public function setContabil($nContabil){
		$this->nContabil = $nContabil;
	}
	public function getContabil(){
		return $this->nContabil;
	}
	public function setNf($nNf){
		$this->nNf = $nNf;
	}
	public function getNf(){
		return $this->nNf;
	}
    public function setFinalizado($nFinalizado){
		$this->nFinalizado = $nFinalizado;
	}
	public function getFinalizado(){
		return $this->nFinalizado;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

     public function setCaixinha($nCaixinha){
		$this->nCaixinha = $nCaixinha;
	}
	public function getCaixinha(){
		return $this->nCaixinha;
	}

   public function setConciliado($nConciliado){
		$this->nConciliado = $nConciliado;
	}
	public function getConciliado(){
		return $this->nConciliado;
	}

     public function setFaOrigem($sFaOrigem){
		$this->sFaOrigem = $sFaOrigem;
	}
	public function getFaOrigem(){
		return $this->sFaOrigem;
	}
	public function setMnyMovimento($oMnyMovimento){
		$this->oMnyMovimento = $oMnyMovimento;
	}
	public function getMnyMovimento(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimento = $oFachada->recuperarUmMnyMovimento($this->getMovCodigo());
		return $this->oMnyMovimento;
	}
	public function setMnyPlanoContasTipoDocumento($oMnyPlanoContasTipoDocumento){
		if($oMnyPlanoContasTipoDocumento)
			$this->oMnyPlanoContasTipoDocumento = $oMnyPlanoContasTipoDocumento;			
		else
			$this->oMnyPlanoContasTipoDocumento = 'null';		
	}	
	public function getMnyPlanoContasTipoDocumento(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasTipoDocumento = $oFachada->recuperarUmMnyPlanoContas($this->getTipDocCodigo());
		return $this->oMnyPlanoContasTipoDocumento;
	}
	public function setMnyPlanoContasFormaPagamento($oMnyPlanoContasFormaPagamento){
		if($oMnyPlanoContasFormaPagamento)
 			$this->oMnyPlanoContasFormaPagamento = $oMnyPlanoContasFormaPagamento;
		else
			$this->oMnyPlanoContasFormaPagamento = 'null';
	}
	public function getMnyPlanoContasFormaPagamento(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasFormaPagamento = $oFachada->recuperarUmMnyPlanoContas($this->getFpgCodigo());
		return $this->oMnyPlanoContasFormaPagamento;
	}
		public function getMnyPlanoContasTipoAceite(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasTipoAceite = $oFachada->recuperarUmMnyPlanoContas($this->getTipAceCodigo());
		return $this->oMnyPlanoContasTipoAceite;
	}
	public function setMnyPlanoContasTipoAceite($oMnyPlanoContasTipoAceite){
		if($oMnyPlanoContasTipoAceite)
			$this->oMnyPlanoContasTipoAceite = $oMnyPlanoContasTipoAceite;
		else
			$this->oMnyPlanoContasTipoAceite = 'null';
	}
	
	
 }
 ?>
