<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_movimento_item_protocolo 
  */
 class MnyMovimentoItemProtocolo{
 	/**
	* @campo cod_protocolo_movimento_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodProtocoloMovimentoItem;
	/**
	* @campo mov_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;
	/**
	* @campo de_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nDeCodTipoProtocolo;
	/**
	* @campo para_cod_tipo_protocolo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nParaCodTipoProtocolo;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyMovimentoItem;
	private $oSysTipoProtocoloDe;
	private $oSysTipoProtocoloPara;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodProtocoloMovimentoItem($nCodProtocoloMovimentoItem){
		$this->nCodProtocoloMovimentoItem = $nCodProtocoloMovimentoItem;
	}
	public function getCodProtocoloMovimentoItem(){
		return $this->nCodProtocoloMovimentoItem;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}
	public function setDeCodTipoProtocolo($nDeCodTipoProtocolo){
		$this->nDeCodTipoProtocolo = $nDeCodTipoProtocolo;
	}
	public function getDeCodTipoProtocolo(){
		return $this->nDeCodTipoProtocolo;
	}
	public function setParaCodTipoProtocolo($nParaCodTipoProtocolo){
		$this->nParaCodTipoProtocolo = $nParaCodTipoProtocolo;
	}
	public function getParaCodTipoProtocolo(){
		return $this->nParaCodTipoProtocolo;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		if($dData){
		 $oData = DateTime::createFromFormat('d/m/Y H:i:s', $dData);
		 $this->dData = $oData->format('Y-m-d H:i:s') ;
		}
	}
	
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setMnyMovimentoItem($oMnyMovimentoItem){
		$this->oMnyMovimentoItem = $oMnyMovimentoItem;
	}
	public function getMnyMovimentoItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovCodigo(),$this->getMovItem());
		return $this->oMnyMovimentoItem;
	}
	
	public function setDeSysTipoProtocolo($oSysTipoProtocoloDe){
		$this->oSysTipoProtocoloDe = $oSysTipoProtocoloDe;
	}
	public function getDeSysTipoProtocolo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oSysTipoProtocoloDe = $oFachada->recuperarUmAcessoGrupoUsuario($this->getDeCodTipoProtocolo());
		return $this->oSysTipoProtocoloDe;
	}
	public function setParaSysTipoProtocolo($oSysTipoProtocoloPara){
		$this->oSysTipoProtocoloPara = $oSysTipoProtocoloPara;
	}
	public function getParaSysTipoProtocolo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oSysTipoProtocoloPara = $oFachada->recuperarUmAcessoGrupoUsuario($this->getParaCodTipoProtocolo());
		return $this->oSysTipoProtocoloPara;
	}
	
 }
 ?>
