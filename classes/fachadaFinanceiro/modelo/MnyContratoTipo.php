<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro
  * @SGBD mysql 
  * @tabela mny_contrato_tipo
  */
 class MnyContratoTipo{
 	/**
	* @campo cod_contrato_tipo
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodContratoTipo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodContratoTipo($nCodContratoTipo){
		$this->nCodContratoTipo = $nCodContratoTipo;
	}
	public function getCodContratoTipo(){
		return $this->nCodContratoTipo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
	
 }
 ?>
