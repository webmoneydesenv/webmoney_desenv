<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_conta_corrente 
  */
 class MnyContaCorrente{
 	/**
	* @campo ccr_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCcrCodigo;

	/**
	* @campo ccr_agencia
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrAgencia;
	/**
	* @campo ccr_agencia_dv
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrAgenciaDv;
	/**
	* @campo ccr_conta
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrConta;
	/**
	* @campo ccr_conta_dv
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrContaDv;
	/**
	* @campo ccr_tipo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCcrTipo;
	/**
	* @campo ban_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nBanCodigo;
	/**
	* @campo ccr_inc
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrInc;
	/**
	* @campo ccr_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sCcrAlt;
	/**
	* @campo classificacao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nClassificacao;
	/**
	* @campo data_encerramento
	* @var data
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataEncerramento;	
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpCodigo;
	
	private $oSysBanco;
	private $voUnidade;
	private $oUnidadeCaixinha;
	private $oPlanoContas;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCcrCodigo($nCcrCodigo){
		$this->nCcrCodigo = $nCcrCodigo;
	}
	public function getCcrCodigo(){
		return $this->nCcrCodigo;
	}

	public function setCcrAgencia($sCcrAgencia){
		$this->sCcrAgencia = $sCcrAgencia;
	}
	public function getCcrAgencia(){
		return $this->sCcrAgencia;
	}
	public function setCcrAgenciaDv($sCcrAgenciaDv){
		$this->sCcrAgenciaDv = $sCcrAgenciaDv;
	}
	public function getCcrAgenciaDv(){
		return $this->sCcrAgenciaDv;
	}
	public function setCcrConta($sCcrConta){
		$this->sCcrConta = $sCcrConta;
	}
	public function getCcrConta(){
		return $this->sCcrConta;
	}
	public function getCcrContaFormatada(){
		if($this->sCcrContaDv)
			return $this->sCcrConta . "-" . $this->sCcrContaDv;
		return $this->sCcrConta;
	}
	
	public function setCcrContaDv($sCcrContaDv){
		$this->sCcrContaDv = $sCcrContaDv;
	}
	public function getCcrContaDv(){
		return $this->sCcrContaDv;
	}
	public function setCcrTipo($nCcrTipo){
		$this->nCcrTipo = $nCcrTipo;
	}
	public function getCcrTipo(){
		return $this->nCcrTipo;
	}
	public function getCcrTipoFormatado(){
			switch($this->nCcrTipo)	{
				case 0: $this->nCcrTipo = "Conta Corrente"; break;
				case 1: $this->nCcrTipo = "Conta Sal&aacute;rio"; break;
				case 2: $this->nCcrTipo = "Conta Poupan&ccedil;a"; break;
				case 3: $this->nCcrTipo = "Outras Contas"; break;
			}
		return $this->nCcrTipo;
	}
	public function setBanCodigo($nBanCodigo){
		$this->nBanCodigo = $nBanCodigo;
	}
	public function getBanCodigo(){
		return $this->nBanCodigo;
	}
	public function setCcrInc($sCcrInc){
		$this->sCcrInc = $sCcrInc;
	}
	public function getCcrInc(){
		return $this->sCcrInc;
	}
	public function setCcrAlt($sCcrAlt){
		$this->sCcrAlt = $sCcrAlt;
	}
	public function getCcrAlt(){
		return $this->sCcrAlt;
	}
	public function setClassificacao($nClassificacao){
		$this->nClassificacao = $nClassificacao;
	}
	public function getClassificacao(){
		return $this->nClassificacao;
	}
	public function getClassificacaoFormatado(){
			switch($this->nClassificacao)	{
				case 0: $this->nClassificacao = "Recebedora"; break;
				case 1: $this->nClassificacao = "Pagadora"; break;
				case 2: $this->nClassificacao = "Caixa"; break;
			}
		return $this->nClassificacao;
	}
	public function setDataEncerramento($dDataEncerramento){
		$this->nAtivo = $dDataEncerramento;
	}
	public function getDataEncerramento(){
		return $this->dDataEncerramento;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setSysBanco($oSysBanco){
		$this->oSysBanco = $oSysBanco;
	}
	public function getSysBanco(){
		$oFachada = new FachadaSysBD();
		$this->oSysBanco = $oFachada->recuperarUmSysBanco($this->getBanCodigo());
		return $this->oSysBanco;
	}

	public function getUnidades(){
		$oFachada = new FachadaFinanceiroBD();
		$this->voUnidade = $oFachada->recuperarTodosMnyMnyContaCorrenteUnidadePorConta($this->getCcrCodigo());
		return $this->voUnidade;
	}
    public function getUnidadeCaixinha(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oUnidadeCaixinha = $oFachada->recuperarUmMnyPlanoContasPorContaCorrente($this->getCcrCodigo());

		return $this->oUnidadeCaixinha;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	
 }
 ?>
