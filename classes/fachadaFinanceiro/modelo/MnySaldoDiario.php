<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_saldo_diario 
  */
 class MnySaldoDiario{
 	/**
	* @campo cod_saldo_diario
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nCodSaldoDiario;
	/**
	* @campo cod_tipo_caixa
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodTipoCaixa;
	/**
	* @campo ccr_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCcrCodigo;
	/**
	* @campo cod_unidade
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCodUnidade;
	/**
	* @campo data
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dData;
	/**
	* @campo valor_final
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValorFinal;
	/**
	* @campo valor_inicial
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nValorInicial;
	/**
	* @campo realizado_por
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sRealizadoPor;
	/**
	* @campo tipo_saldo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipoSaldo;
    /**
	* @campo extrato_bancario
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $oExtratoBancario;

	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oMnyTipoCaixa;
	private $oMnyContaCorrente;
	private $oMnyPlanoContas;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodSaldoDiario($nCodSaldoDiario){
		$this->nCodSaldoDiario = $nCodSaldoDiario;
	}
	public function getCodSaldoDiario(){
		return $this->nCodSaldoDiario;
	}
	public function setCodTipoCaixa($nCodTipoCaixa){
		$this->nCodTipoCaixa = $nCodTipoCaixa;
	}
	public function getCodTipoCaixa(){
		return $this->nCodTipoCaixa;
	}
	public function setCcrCodigo($nCcrCodigo){
		$this->nCcrCodigo = $nCcrCodigo;
	}
	public function getCcrCodigo(){
		return $this->nCcrCodigo;
	}
	public function setCodUnidade($nCodUnidade){
		$this->nCodUnidade = $nCodUnidade;
	}
	public function getCodUnidade(){
		return $this->nCodUnidade;
	}
	public function setData($dData){
		$this->dData = $dData;
	}
	public function getData(){
		return $this->dData;
	}
	public function getDataFormatado(){
		$oData = new DateTime($this->dData);
		 return $oData->format("d/m/Y");
	}
	public function setDataBanco($dData){
		 if($dData){
			 $oData = DateTime::createFromFormat('d/m/Y', $dData);
			 $this->dData = $oData->format('Y-m-d') ;
		}else{
			 $this->dData = 'null' ;			
		}
	}
	public function setValorFinal($nValorFinal){
		$this->nValorFinal = $nValorFinal;
	}
	public function getValorFinal(){
		return $this->nValorFinal;
	}
	public function getValorFinalFormatado(){
		 $vRetorno = number_format($this->nValorFinal , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorFinalBanco($nValorFinal){
		if($nValorFinal){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorFinal = str_replace($sOrigem, $sDestino, $nValorFinal);
	
		}else{
			$this->nValorFinal = 'null';
		}
	}	
	public function setValorInicial($nValorInicial){
		$this->nValorInicial = $nValorInicial;
	}
	public function getValorInicial(){
		return $this->nValorInicial;
	}
	public function getValorInicialFormatado(){
		 $vRetorno = number_format($this->nValorInicial , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorInicialBanco($nValorInicial){
		if($nValorInicial){
			$sOrigem = array('.',',');
			$sDestino = array('','.');
			$this->nValorInicial = str_replace($sOrigem, $sDestino, $nValorInicial);
	
		}else{
			$this->nValorInicial = 'null';
		}
	}
    public function setRealizadoPor($sRealizadoPor){
		$this->sRealizadoPor = $sRealizadoPor;
	}
	public function getRealizadoPor(){
		return $this->sRealizadoPor;
	}
	public function setTipoSaldo($nTipoSaldo){
		$this->nTipoSaldo = $nTipoSaldo;
	}
	public function getTipoSaldo(){
		return $this->nTipoSaldo;
	}	
	public function setExtratoBancario($oExtratoBancario){
		$this->oExtratoBancario = $oExtratoBancario;
	}
	public function getExtratoBancario(){
		return $this->oExtratoBancario;
	}
    public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

	public function setMnyTipoCaixa($oMnyTipoCaixa){
		$this->oMnyTipoCaixa = $oMnyTipoCaixa;
	}
	public function getMnyTipoCaixa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyTipoCaixa = $oFachada->recuperarUmMnyTipoCaixa($this->getCodTipoCaixa());
		return $this->oMnyTipoCaixa;
	}
	public function getMnyContaCorrente(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContaCorrente = $oFachada->recuperarUmMnyContaCorrente($this->getCcrCodigo());		
		return $this->oMnyContaCorrente;
	}
	public function getMnyPlanoContas(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContas = $oFachada->recuperarUmMnyPlanoContas($this->getCodUnidade());		
		return $this->oMnyPlanoContas;
	}
	
 }
 ?>
