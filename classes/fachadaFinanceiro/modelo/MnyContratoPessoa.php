<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_contrato_pessoa 
  */
 class MnyContratoPessoa{
 	/**
	* @campo contrato_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nContratoCodigo;
 	/**
	* @campo tipo_contrato
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nTipoContrato;
	/**
	* @campo pes_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesCodigo;
	/**
	* @campo cod_status
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodStatus;
	/**
	* @campo numero
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNumero;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo data_inicio
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataInicio;
	/**
	* @campo data_contrato
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataContrato;
	/**
	* @campo data_validade
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataValidade;
	/**
	* @campo valor_contrato
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorContrato;
	/**
	* @campo cus_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCusCodigo;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oSysStatus;
	private $oMnyPlanoContasCusto;
	private $oMnyPlanoContasConta;
	private $oMnyPlanoContasNegocio;
	private $oMnyPlanoContasUnidade;
	private $oMnyPlanoContasCentro;
	private $oMnyPlanoContasSetor;
	private $oMnyPessoaGeral;
	private $oMnyContratoTipo;
	private $oMnyMovimentoContrato;
	private $oMnySaldoContrato;
	
	
 	public function __construct(){
 		
 	}
 	
 	public function setContratoCodigo($nContratoCodigo){
		$this->nContratoCodigo = $nContratoCodigo;
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
 	public function setTipoContrato($nTipoContrato){
		$this->nTipoContrato = $nTipoContrato;
	}
	public function getTipoContrato(){
		return $this->nTipoContrato;
	}
	public function setPesCodigo($nPesCodigo){
		$this->nPesCodigo = $nPesCodigo;
	}
	public function getPesCodigo(){
		return $this->nPesCodigo;
	}
	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setNumero($sNumero){
		$this->sNumero = $sNumero;
	}
	public function getNumero(){
		return $this->sNumero;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}

	public function setDataInicio($dDataInicio){
		$this->dDataInicio = $dDataInicio;
	}
	public function getDataInicio(){
		return $this->dDataInicio;
	}
	
	public function getDataInicioFormatado(){
		if($this->dDataInicio){
		$oData = new DateTime($this->dDataInicio);
		 return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	public function setDataInicioBanco($dDataInicio){
		if($dDataInicio){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataInicio);
			 $this->dDataInicio = $oData->format('Y-m-d');
		}else{
			$this->dDataInicio = 'NULL';	
		}
	}

	public function setDataContrato($dDataContrato){
		$this->dDataContrato = $dDataContrato;
	}
	public function getDataContrato(){
		return $this->dDataContrato;
	}
	public function getDataContratoFormatado(){
		if($this->dDataContrato != NULL){
			 $oData = new DateTime($this->dDataContrato);
			 return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	public function setDataContratoBanco($dDataContrato){
		if($dDataContrato){
			 $oData = DateTime::createFromFormat('d/m/Y', $dDataContrato);
			 $this->dDataContrato = $oData->format('Y-m-d') ;
		}else{
			$this->dDataContrato = 'NULL';	
		}
	}
	public function setDataValidade($dDataValidade){
		$this->dDataValidade = $dDataValidade;
	}
	public function getDataValidade(){
		return $this->dDataValidade;
	}
	public function getDataValidadeFormatado(){
		if($this->dDataValidade){
			$oData = new DateTime($this->dDataValidade);
		 	return $oData->format("d/m/Y");
		}else{
			return "";
		}
	}
	
	public function setDataValidadeBanco($dDataValidade){
		if($dDataValidade){
		 $oData = DateTime::createFromFormat('d/m/Y', $dDataValidade);
		 $this->dDataValidade = $oData->format('Y-m-d') ;
		}
	}
	public function setValorContrato($nValorContrato){
		$this->nValorContrato = $nValorContrato;
	}
	public function getValorContrato(){
		return $this->nValorContrato;
	}
	public function getValorContratoFormatado(){
		 if($this->nValorContrato)
			 $vRetorno = number_format($this->nValorContrato , 2, ',', '.');
		else
			$vRetorno = $this->nValorContrato;
		 return $vRetorno;
	}
	public function setValorContratoBanco($nValorContrato){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nValorContrato = str_replace($sOrigem, $sDestino, $nValorContrato);
	}

	public function setCusCodigo($nCusCodigo){
		if($nCusCodigo){
			$this->nCusCodigo = $nCusCodigo;	
		}else{
			$this->nCusCodigo = 'null';	
		}			
	}
	public function getCusCodigo(){
		return $this->nCusCodigo;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setCenCodigo($nCenCodigo){
		if($nCenCodigo){
			$this->nCenCodigo = $nCenCodigo;
		}else{
			$this->nCenCodigo = 'null';
		}
		
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		if($nUniCodigo){
			$this->nUniCodigo = $nUniCodigo;		
		}else{
			$this->nUniCodigo = 'null';	
		}
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setConCodigo($nConCodigo){
		if($nConCodigo){
			$this->nConCodigo = $nConCodigo;		
		}else{
			$this->nConCodigo = 'null';	
		}
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}

	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setSysStatus($oSysStatus){
		$this->oSysStatus = $oSysStatus;
	}
	public function getSysStatus(){
		$oFachada = new FachadaSysBD();
		$this->oSysStatus = $oFachada->recuperarUmSysStatus($this->getCodStatus());
		return $this->oSysStatus;
	}
	public function getMnyPlanoContasCusto(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCusto = $oFachada->recuperarUmMnyPlanoContas($this->getCusCodigo());
		return $this->oMnyPlanoContasCusto;
	}
	public function setMnyPlanoContasCusto($oMnyPlanoContasCusto){
		$this->oMnyPlanoContasCusto = $oMnyPlanoContasCusto;
	}
	public function getMnyPlanoContasNegocio(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasNegocio = $oFachada->recuperarUmMnyPlanoContas($this->getNegCodigo());
		return $this->oMnyPlanoContasNegocio;
	}
	public function setMnyPlanoContasNegocio($oMnyPlanoContasNegocio){
		$this->oMnyPlanoContasNegocio = $oMnyPlanoContasNegocio;
	}
	public function getMnyPlanoContasCentro(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCentro = $oFachada->recuperarUmMnyPlanoContas($this->getCenCodigo());
		return $this->oMnyPlanoContasCentro;
	}
	public function setMnyPlanoContasCentro($oMnyPlanoContasCentro){
		$this->oMnyPlanoContasCentro = $oMnyPlanoContasCentro;
	}
	public function getMnyPlanoContasUnidade(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasUnidade = $oFachada->recuperarUmMnyPlanoContas($this->getUniCodigo());
		return $this->oMnyPlanoContasUnidade;
	}
	public function setMnyPlanoContasUnidade($oMnyPlanoContasUnidade){
		$this->oMnyPlanoContasUnidade = $oMnyPlanoContasUnidade;
	}

	public function getMnyPlanoContasConta(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasConta = $oFachada->recuperarUmMnyPlanoContas($this->getConCodigo());
		return $this->oMnyPlanoContasConta;
	}
	public function setMnyPlanoContasConta($oMnyPlanoContasConta){
		$this->oMnyPlanoContasConta = $oMnyPlanoContasConta;
	}

	public function getMnyPlanoContasSetor(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasSetor = $oFachada->recuperarUmMnyPlanoContas($this->getSetCodigo());
		return $this->oMnyPlanoContasSetor;
	}
	public function setMnyPlanoContasSetor($oMnyPlanoContasSetor){
		$this->oMnyPlanoContasSetor = $oMnyPlanoContasSetor;
	}
	public function setMnyPessoaGeral($oMnyPessoaGeral){
		$this->oMnyPessoaGeral = $oMnyPessoaGeral;
	}
	public function getMnyPessoaGeral(){
		$oFachada = new FachadaViewBD();
		$this->oMnyPessoaGeral = $oFachada->recuperarUmVPessoaGeralFormatada($this->getPesCodigo());		
		return $this->oMnyPessoaGeral;
	}

	public function setMnyContratoTipo($oMnyContratoTipo){
		$this->oMnyContratoTipo = $oMnyContratoTipo;
	}
	public function getMnyContratoTipo(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoTipo = $oFachada->recuperarUmMnyContratoTipo($this->getTipoContrato());
		return $this->oMnyContratoTipo;
	}
	public function getMnyMovimentoContrato(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoContrato = $oFachada->recuperarUmMnyMovimentoPorContrato($this->getContratoCodigo());
		/*print_r($this->getContratoCodigo());
		die();*/
		return $this->oMnyMovimentoContrato;
	}	

	public function getSaldoContratoFormatado(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnySaldoContrato =  number_format($oFachada->recuperarUmMnyContratoPessoaSaldo($this->getContratoCodigo())->getAtivo() , 2, ',', '.');
		return $this->oMnySaldoContrato;
	}	

	public function getSaldoContrato(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnySaldoContrato =  $oFachada->recuperarUmMnyContratoPessoaSaldo($this->getContratoCodigo())->getAtivo();
		return $this->oMnySaldoContrato;
	}	

	
 }
 ?>
