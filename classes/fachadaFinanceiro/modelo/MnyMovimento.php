<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_movimento 
  */
 class MnyMovimento{
 	/**
	* @campo mov_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nMovCodigo;
	/**
	* @campo mov_data_inclusao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataInclusao;
	/**
	* @campo mov_data_emissao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $dMovDataEmissao;
	/**
	* @campo cus_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCusCodigo;
	/**
	* @campo neg_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nNegCodigo;
	/**
	* @campo cen_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nCenCodigo;
	/**
	* @campo uni_codigo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nUniCodigo;
	/**
	* @campo pes_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nPesCodigo;
	/**
	* @campo set_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nSetCodigo;
	/**
	* @campo con_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nConCodigo;
	/**
	* @campo mov_obs
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovObs;
	/**
	* @campo mov_inc
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovInc;
	/**
	* @campo mov_alt
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sMovAlt;
	/**
	* @campo mov_contrato
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovContrato;
	/**
	* @campo mov_documento
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMovDocumento;
	/**
	* @campo mov_parcelas
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovParcelas;
	/**
	* @campo mov_tipo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovTipo;
	/**
	* @campo emp_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nEmpCodigo;
	/**
	* @campo mov_icms_aliq
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nMovIcmsAliq;
	/**
	* @campo mov_pis
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovPis;
	/**
	* @campo mov_confins
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovConfins;
	/**
	* @campo mov_csll
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovCsll;
	/**
	* @campo mov_iss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIss;
	/**
	* @campo mov_ir
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIr;
	/**
	* @campo mov_irrf
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovIrrf;
	/**
	* @campo mov_inss
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovInss;
	/**
	* @campo mov_devolucao
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovDevolucao;
	/**
	* @campo mov_outros
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutros;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nAtivo;

	/**
	* @campo mov_outros_desc
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nMovOutrosDesc;

	private $oMnyPlanoContasCusto;
	private $oMnyPlanoContasConta;
	private $oMnyPlanoContasNegocio;
	private $oMnyPlanoContasUnidade;
	private $oMnyPlanoContasCentro;
	private $oMnyPlanoContasSetor;
	private $oMnyContratoPessoa;
	private $oSysEmpresa;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovDataInclusao($dMovDataInclusao){
		$this->dMovDataInclusao = $dMovDataInclusao;
	}
	public function getMovDataInclusao(){
		return $this->dMovDataInclusao;
	}
	public function getMovDataInclusaoFormatado(){
		$oData = new DateTime($this->dMovDataInclusao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataInclusaoBanco($dMovDataInclusao){
		if($dMovDataInclusao){
		 $oData = DateTime::createFromFormat('d/m/Y', $dMovDataInclusao);
		 $this->dMovDataInclusao = $oData->format('Y-m-d') ;
		 }else{
		 $this->dMovDataInclusao = 'null';
		 }
	}
	public function setMovDataEmissao($dMovDataEmissao){
		$this->dMovDataEmissao = $dMovDataEmissao;
	}
	public function getMovDataEmissao(){
		return $this->dMovDataEmissao;
	}
	public function getMovDataEmissaoFormatado(){
		$oData = new DateTime($this->dMovDataEmissao);
		 return $oData->format("d/m/Y");
	}
	public function setMovDataEmissaoBanco($dMovDataEmissao){
		if($dMovDataEmissao){
			$oData = DateTime::createFromFormat('d/m/Y', $dMovDataEmissao);
		 	$this->dMovDataEmissao = $oData->format('Y-m-d');
		 }else{
		 	$this->dMovDataEmissao = 'null';
		 }
	}
	public function setCusCodigo($nCusCodigo){
		if($nCusCodigo){
			$this->nCusCodigo = $nCusCodigo;	
		}else{
			$this->nCusCodigo = 'null';	
		}			
	}
	public function getCusCodigo(){
		return $this->nCusCodigo;
	}
	public function setNegCodigo($nNegCodigo){
		$this->nNegCodigo = $nNegCodigo;
	}
	public function getNegCodigo(){
		return $this->nNegCodigo;
	}
	public function setCenCodigo($nCenCodigo){
		if($nCenCodigo){
			$this->nCenCodigo = $nCenCodigo;
		}else{
			$this->nCenCodigo = 'null';
		}
		
	}
	public function getCenCodigo(){
		return $this->nCenCodigo;
	}
	public function setUniCodigo($nUniCodigo){
		if($nUniCodigo){
			$this->nUniCodigo = $nUniCodigo;		
		}else{
			$this->nUniCodigo = 'null';	
		}
	}
	public function getUniCodigo(){
		return $this->nUniCodigo;
	}
	public function setConCodigo($nConCodigo){
		if($nConCodigo){
			$this->nConCodigo = $nConCodigo;		
		}else{
			$this->nConCodigo = 'null';	
		}
	}
	public function getConCodigo(){
		return $this->nConCodigo;
	}
	public function setPesCodigo($nPesCodigo){
		$this->nPesCodigo = $nPesCodigo;
	}
	public function getPesCodigo(){
		return $this->nPesCodigo;
	}
	
	public function setSetCodigo($nSetCodigo){
		$this->nSetCodigo = $nSetCodigo;
	}
	public function getSetCodigo(){
		return $this->nSetCodigo;
	}	
	public function setMovObs($sMovObs){
		//grava em caixa alta...
		$this->sMovObs = strtoupper($sMovObs);
	}
	public function getMovObs(){
		return $this->sMovObs;
	}
	public function setMovInc($sMovInc){
		$this->sMovInc = $sMovInc;
	}
	public function getMovInc(){
		return $this->sMovInc;
	}
	public function setMovAlt($sMovAlt){
		$this->sMovAlt = $sMovAlt;
	}
	public function getMovAlt(){
		return $this->sMovAlt;
	}
	public function setMovContrato($nMovContrato){
		$this->nMovContrato = $nMovContrato;
	}
	public function getMovContrato(){
		return $this->nMovContrato;
	}
	public function setMovDocumento($sMovDocumento){
		$this->sMovDocumento = strtoupper($sMovDocumento);
	}
	public function getMovDocumento(){
		return $this->sMovDocumento;
	}
	public function setMovParcelas($nMovParcelas){
		$this->nMovParcelas = $nMovParcelas;
	}
	public function getMovParcelas(){
		return $this->nMovParcelas;
	}
	public function setMovTipo($nMovTipo){
		$this->nMovTipo = $nMovTipo;
	}
	public function getMovTipo(){
		return $this->nMovTipo;
	}
	public function setEmpCodigo($nEmpCodigo){
		$this->nEmpCodigo = $nEmpCodigo;
	}
	public function getEmpCodigo(){
		return $this->nEmpCodigo;
	}
	public function setMovIcmsAliq($nMovIcmsAliq){
		$this->nMovIcmsAliq = $nMovIcmsAliq;
	}
	public function getMovIcmsAliq(){
		return $this->nMovIcmsAliq;
	}
	public function getMovIcmsAliqFormatado(){
		 $vRetorno = number_format($this->nMovIcmsAliq , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIcmsAliqBanco($nMovIcmsAliq){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovIcmsAliq = str_replace($sOrigem, $sDestino, $nMovIcmsAliq);
	}

	public function setMovPis($nMovPis){
		$this->nMovPis = $nMovPis;
	}
	public function getMovPis(){
		return $this->nMovPis;
	}
	public function getMovPisFormatado(){
		 $vRetorno = number_format($this->nMovPis , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovPisBanco($nMovPis){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovPis = str_replace($sOrigem, $sDestino, $nMovPis);
	}
	public function setMovConfins($nMovConfins){
		$this->nMovConfins = $nMovConfins;
	}
	public function getMovConfins(){
		return $this->nMovConfins;
	}
	public function getMovConfinsFormatado(){
		 $vRetorno = number_format($this->nMovConfins , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovConfinsBanco($nMovConfins){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovConfins = str_replace($sOrigem, $sDestino, $nMovConfins);
	}
	public function setMovCsll($nMovCsll){
		$this->nMovCsll = $nMovCsll;
	}
	public function getMovCsll(){
		return $this->nMovCsll;
	}
	public function getMovCsllFormatado(){
		 $vRetorno = number_format($this->nMovCsll , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovCsllBanco($nMovCsll){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovCsll = str_replace($sOrigem, $sDestino, $nMovCsll);
	}
	public function setMovIss($nMovIss){
		$this->nMovIss = $nMovIss;
	}
	public function getMovIss(){
		return $this->nMovIss;
	}
	public function getMovIssFormatado(){
		 $vRetorno = number_format($this->nMovIss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIssBanco($nMovIss){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovIss = str_replace($sOrigem, $sDestino, $nMovIss);
	}
	public function setMovIr($nMovIr){
		$this->nMovIr = $nMovIr;
	}
	public function getMovIr(){
		return $this->nMovIr;
	}
	public function getMovIrFormatado(){
		 $vRetorno = number_format($this->nMovIr , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrBanco($nMovIr){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovIr = str_replace($sOrigem, $sDestino, $nMovIr);
	}
	public function setMovIrrf($nMovIrrf){
		$this->nMovIrrf = $nMovIrrf;
	}
	public function getMovIrrf(){
		return $this->nMovIrrf;
	}
	public function getMovIrrfFormatado(){
		 $vRetorno = number_format($this->nMovIrrf , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovIrrfBanco($nMovIrrf){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovIrrf = str_replace($sOrigem, $sDestino, $nMovIrrf);
	}
	public function setMovInss($nMovInss){
		$this->nMovInss = $nMovInss;
	}
	public function getMovInss(){
		return $this->nMovInss;
	}
	public function getMovInssFormatado(){
		 $vRetorno = number_format($this->nMovInss , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovInssBanco($nMovInss){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovInss = str_replace($sOrigem, $sDestino, $nMovInss);
	}

	public function setMovDevolucao($nMovDevolucao){
		$this->nMovDevolucao = $nMovDevolucao;
	}
	public function getMovDevolucao(){
		return $this->nMovDevolucao;
	}
	public function getMovDevolucaoFormatado(){
		 $vRetorno = number_format($this->nMovDevolucao , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovDevolucaoBanco($nMovDevolucao){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovDevolucao = str_replace($sOrigem, $sDestino, $nMovDevolucao);
	}
	public function setMovOutros($nMovOutros){
		$this->nMovOutros = $nMovOutros;
	}
	public function getMovOutros(){
		return $this->nMovOutros;
	}
	public function getMovOutrosFormatado(){
		 $vRetorno = number_format($this->nMovOutros , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosBanco($nMovOutros){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovOutros = str_replace($sOrigem, $sDestino, $nMovOutros);
	}
	
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

	public function setMovOutrosDesc($nMovOutrosDesc){
		$this->nMovOutrosDesc = $nMovOutrosDesc;
	}
	public function getMovOutrosDesc(){
		return $this->nMovOutrosDesc;
	}
	public function getMovOutrosDescFormatado(){
		 $vRetorno = number_format($this->nMovOutrosDesc , 2, ',', '.');
		 return $vRetorno;
	}
	public function setMovOutrosDescBanco($nMovOutrosDesc){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nMovOutrosDesc = str_replace($sOrigem, $sDestino, $nMovOutrosDesc);
	}	
	public function getMnyPlanoContasCusto(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCusto = $oFachada->recuperarUmMnyPlanoContas($this->getCusCodigo());
		return $this->oMnyPlanoContasCusto;
	}
	public function setMnyPlanoContasCusto($oMnyPlanoContasCusto){
		$this->oMnyPlanoContasCusto = $oMnyPlanoContasCusto;
	}
	public function getMnyPlanoContasNegocio(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasNegocio = $oFachada->recuperarUmMnyPlanoContas($this->getNegCodigo());
		return $this->oMnyPlanoContasNegocio;
	}
	public function setMnyPlanoContasNegocio($oMnyPlanoContasNegocio){
		$this->oMnyPlanoContasNegocio = $oMnyPlanoContasNegocio;
	}
	public function getMnyPlanoContasCentro(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasCentro = $oFachada->recuperarUmMnyPlanoContas($this->getCenCodigo());
		return $this->oMnyPlanoContasCentro;
	}
	public function setMnyPlanoContasCentro($oMnyPlanoContasCentro){
		$this->oMnyPlanoContasCentro = $oMnyPlanoContasCentro;
	}
	public function getMnyPlanoContasUnidade(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasUnidade = $oFachada->recuperarUmMnyPlanoContas($this->getUniCodigo());
		return $this->oMnyPlanoContasUnidade;
	}
	public function setMnyPlanoContasUnidade($oMnyPlanoContasUnidade){
		$this->oMnyPlanoContasUnidade = $oMnyPlanoContasUnidade;
	}

	public function getMnyPlanoContasConta(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasConta = $oFachada->recuperarUmMnyPlanoContas($this->getConCodigo());
		return $this->oMnyPlanoContasConta;
	}
	public function setMnyPlanoContasConta($oMnyPlanoContasConta){
		$this->oMnyPlanoContasConta = $oMnyPlanoContasConta;
	}

	public function getMnyPlanoContasSetor(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPlanoContasSetor = $oFachada->recuperarUmMnyPlanoContas($this->getSetCodigo());
		return $this->oMnyPlanoContasSetor;
	}
	public function setMnyPlanoContasSetor($oMnyPlanoContasSetor){
		$this->oMnyPlanoContasSetor = $oMnyPlanoContasSetor;
	}	
	
	public function getMnyContratoPessoa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($this->getMovContrato());
		return $this->oMnyContratoPessoa;
	}
	public function setMnyContratoPessoa($oMnyContratoPessoa){
		$this->oMnyContratoPessoa = $oMnyContratoPessoa;
	}

    public function getSysEmpresa(){
		$oFachada = new FachadaSysBD();
		$this->oSysEmpresa = $oFachada->recuperarUmSysEmpresa($this->getEmpCodigo());
		return $this->oSysEmpresa;
	}

 }
 ?>
