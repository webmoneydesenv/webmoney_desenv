<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_pessoa_juridica 
  */
 class MnyPessoaJuridica{
 	/**
	* @campo pesjur_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nPesjurCodigo;
	/**
	* @campo pesjur_razao
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesjurRazao;
	/**
	* @campo pesjur_nome
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesjurNome;
	/**
	* @campo pesjur_cnpj
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sPesjurCnpj;
	/**
	* @campo pesjur_insmun
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPesjurInsmun;
	/**
	* @campo pesjur_insest
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sPesjurInsest;
	/**
	* @campo cod_matriz
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodMatriz;
	private $oMnyPessoaJuridica;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setPesjurCodigo($nPesjurCodigo){
		$this->nPesjurCodigo = $nPesjurCodigo;
	}
	public function getPesjurCodigo(){
		return $this->nPesjurCodigo;
	}
	public function setPesjurRazao($sPesjurRazao){
		$this->sPesjurRazao = $sPesjurRazao;
	}
	public function getPesjurRazao(){
		return $this->sPesjurRazao;
	}
	public function setPesjurNome($sPesjurNome){
		$this->sPesjurNome = $sPesjurNome;
	}
	public function getPesjurNome(){
		return $this->sPesjurNome;
	}
	public function setPesjurCnpj($sPesjurCnpj){
		$this->sPesjurCnpj = $sPesjurCnpj;
	}
	public function getPesjurCnpj(){
		return $this->sPesjurCnpj;
	}
	public function setPesjurInsmun($sPesjurInsmun){
		$this->sPesjurInsmun = $sPesjurInsmun;
	}
	public function getPesjurInsmun(){
		return $this->sPesjurInsmun;
	}
	public function setPesjurInsest($sPesjurInsest){
		$this->sPesjurInsest = $sPesjurInsest;
	}
	public function getPesjurInsest(){
		return $this->sPesjurInsest;
	}
	public function setCodMatriz($nCodMatriz){
		$this->nCodMatriz = $nCodMatriz;
	}
	public function getCodMatriz(){
		return $this->nCodMatriz;
	}
	public function setMnyPessoaJuridica($oMnyPessoaJuridica){
		$this->oMnyPessoaJuridica = $oMnyPessoaJuridica;
	}
	public function getMnyPessoaJuridica(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyPessoaJuridica = $oFachada->recuperarUmMnyPessoaJuridica($this->getCodMatriz());
		return $this->oMnyPessoaJuridica;
	}
	
 }
 ?>
