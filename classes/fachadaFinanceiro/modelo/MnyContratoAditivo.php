<?php
 /**
  * @author Auto-Generated 
  * @package fachadaFinanceiro 
  * @SGBD mysql 
  * @tabela mny_contrato_aditivo 
  */
 class MnyContratoAditivo{
 	/**
	* @campo aditivo_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment true
	*/
	private $nAditivoCodigo;
	/**
	* @campo contrato_codigo
	* @var number
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $nContratoCodigo;
	/**
	* @campo cod_status
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nCodStatus;
	/**
	* @campo numero
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sNumero;
	/**
	* @campo motivo_aditivo
	* @var String
	* @primario false
	* @nulo false
	* @auto-increment false
	*/
	private $sMotivoAditivo;
	/**
	* @campo data_aditivo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataAditivo;
	/**
	* @campo data_validade
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $dDataValidade;
	/**
	* @campo valor_aditivo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nValorAditivo;
	/**
	* @campo anexo
	* @var String
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $sAnexo;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @nulo true
	* @auto-increment false
	*/
	private $nAtivo;
	private $oSysStatus;
	private $oMnyContratoPessoa;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setAditivoCodigo($nAditivoCodigo){
		$this->nAditivoCodigo = $nAditivoCodigo;
	}
	public function getAditivoCodigo(){
		return $this->nAditivoCodigo;
	}
	public function setContratoCodigo($nContratoCodigo){
		$this->nContratoCodigo = $nContratoCodigo;
	}
	public function getContratoCodigo(){
		return $this->nContratoCodigo;
	}
	public function setCodStatus($nCodStatus){
		$this->nCodStatus = $nCodStatus;
	}
	public function getCodStatus(){
		return $this->nCodStatus;
	}
	public function setNumero($sNumero){
		$this->sNumero = $sNumero;
	}
	public function getNumero(){
		return $this->sNumero;
	}
	public function setMotivoAditivo($sMotivoAditivo){
		$this->sMotivoAditivo = $sMotivoAditivo;
	}
	public function getMotivoAditivo(){
		return $this->sMotivoAditivo;
	}
	public function setDataAditivo($dDataAditivo){
		$this->dDataAditivo = $dDataAditivo;
	}
	public function getDataAditivo(){
		return $this->dDataAditivo;
	}
	public function getDataAditivoFormatado(){
		$oData = new DateTime($this->dDataAditivo);
		 return $oData->format("d/m/Y");
	}
	public function setDataAditivoBanco($dDataAditivo){
		 $oData = DateTime::createFromFormat('d/m/Y', $dDataAditivo);
		 $this->dDataAditivo = $oData->format('Y-m-d') ;
	}
	public function setDataValidade($dDataValidade){
		$this->dDataValidade = $dDataValidade;
	}
	public function getDataValidade(){
		return $this->dDataValidade;
	}
	public function getDataValidadeFormatado(){
		$oData = new DateTime($this->dDataValidade);
		 return $oData->format("d/m/Y");
	}
	public function setDataValidadeBanco($dDataValidade){
		 $oData = DateTime::createFromFormat('d/m/Y', $dDataValidade);
		 $this->dDataValidade = $oData->format('Y-m-d') ;
	}
	public function setValorAditivo($nValorAditivo){
		$this->nValorAditivo = $nValorAditivo;
	}
	public function getValorAditivo(){
		return $this->nValorAditivo;
	}
	public function getValorAditivoFormatado(){
		 $vRetorno = number_format($this->nValorAditivo , 2, ',', '.');
		 return $vRetorno;
	}
	public function setValorAditivoBanco($nValorAditivo){
		$sOrigem = array('.',',');
		$sDestino = array('','.');
		$this->nValorAditivo = str_replace($sOrigem, $sDestino, $nValorAditivo);
	}
	public function setAnexo($sAnexo){
		$this->sAnexo = $sAnexo;
	}
	public function getAnexo(){
		return $this->sAnexo;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setSysStatus($oSysStatus){
		$this->oSysStatus = $oSysStatus;
	}
	public function getSysStatus(){
		$oFachada = new FachadaSysBD();
		$this->oSysStatus = $oFachada->recuperarUmSysStatus($this->getCodStatus());
		return $this->oSysStatus;
	}
	public function setMnyContratoPessoa($oMnyContratoPessoa){
		$this->oMnyContratoPessoa = $oMnyContratoPessoa;
	}
	public function getMnyContratoPessoa(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyContratoPessoa = $oFachada->recuperarUmMnyContratoPessoa($this->getContratoCodigo());
		return $this->oMnyContratoPessoa;
	}
	
 }
 ?>
