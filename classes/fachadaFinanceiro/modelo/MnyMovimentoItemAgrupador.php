<?php
 /**
  * @author Auto-Generated
  * @package fachadaFinanceiro
  * @SGBD mysql
  * @tabela mny_movimento_item_agrupador
  */
 class MnyMovimentoItemAgrupador{
 	/**
	* @campo mov_codigo_agrupador
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigoAgrupador;
	/**
	* @campo mov_item_agrupador
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItemAgrupador;
     /**
	* @campo mov_codigo
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovCodigo;
	/**
	* @campo mov_item
	* @var number
	* @primario true
	* @nulo false
	* @auto-increment false
	*/
	private $nMovItem;


	private $oMnyMovimento;
	private $oMnyMovimentoItem;

 	public function __construct(){

 	}

 	public function setMovCodigoAgrupador($nMovCodigoAgrupador){
		$this->nMovCodigoAgrupador = $nMovCodigoAgrupador;
	}
	public function getMovCodigoAgrupador(){
		return $this->nMovCodigoAgrupador;
	}
	public function setMovItemAgrupador($nMovItemAgrupador){
		$this->nMovItemAgrupador = $nMovItemAgrupador;
	}
	public function getMovItemAgrupador(){
		return $this->nMovItemAgrupador;
	}
	public function setMovCodigo($nMovCodigo){
		$this->nMovCodigo = $nMovCodigo;
	}
	public function getMovCodigo(){
		return $this->nMovCodigo;
	}
	public function setMovItem($nMovItem){
		$this->nMovItem = $nMovItem;
	}
	public function getMovItem(){
		return $this->nMovItem;
	}


	public function setMnyMovimento($oMnyMovimento){
		$this->oMnyMovimento = $oMnyMovimento;
	}
	public function getMnyMovimento(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimento = $oFachada->recuperarUmMnyMovimento($this->getMovCodigo());
		return $this->oMnyMovimento;
	}

 	public function setMnyMovimentoItem($oMnyMovimentoItem){
		$this->oMnyMovimentoItem = $oMnyMovimentoItem;
	}
	public function getMnyMovimentoItem(){
		$oFachada = new FachadaFinanceiroBD();
		$this->oMnyMovimentoItem = $oFachada->recuperarUmMnyMovimentoItem($this->getMovCodigo(),$this->getMovItem());
		return $this->oMnyMovimentoItem;
	}

 }
 ?>
