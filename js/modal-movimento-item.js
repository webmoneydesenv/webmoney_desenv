function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);    
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
            	jQuery(function($){
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataPrev").mask("99/99/9999");
					$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	   
					$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	   
					$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	   
					$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	   
					$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	   
				  });		
    		}	
	});
}