<?php
 $sOP = $_REQUEST['sOP'];
 $oSysEmpresa = $_REQUEST['oSysEmpresa'];
 $voUnidade = $_REQUEST['voUnidade'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Empresa - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SysEmpresa.preparaLista">Gerenciar Empresas</a> &gt; <strong><?php echo $sOP?> Empresa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Empresa</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" enctype="multipart/form-data" class="form-horizontal" name="formSysEmpresa" action="?action=SysEmpresa.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type='hidden' name='fEmpInc' value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpInc() : date('d/m/Y H:i:s') . " " . $_SESSION['oUsuarioImoney']->getLogin()?>'/>
 		 <input type='hidden' name='fEmpAlt' value='<?= ($oSysEmpresa) ? date('d/m/Y H:i:s') . " " . $_SESSION['oUsuarioImoney']->getLogin() : ""?>'/>
 		 <input type='hidden' name='fAtivo' value='<?= ($oSysEmpresa) ? $oSysEmpresa->getAtivo() : 1?>'/>
         <input type="hidden" name="fEmpCodigo" value="<?=(is_object($oSysEmpresa)) ? $oSysEmpresa->getEmpCodigo() : ""?>" />
         <input type="hidden" name="fEmpCodPessoa" value="<?=(is_object($oSysEmpresa)) ? $oSysEmpresa->getEmpCodPessoa() : ""?>" />
         <?php if ($sOP == 'Alterar'){ ?>
        	<input type='hidden' name='fEmpImagem'  value='<?=($oSysEmpresa) ? $oSysEmpresa->getEmpImagem() : ""?>'/>
        <?php } ?>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Empresa">

		<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpRazaoSocial">Razão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Razão Social' name='fEmpRazaoSocial' required    value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpRazaoSocial() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFantasia">Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome Fantasia' name='fEmpFantasia' required   value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpFantasia() : ""?>'/>
				</div>
				</div>
				<br>
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Apelido">Apelido:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Apelido' name='fApelido' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getApelido() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CNPJ">CNPJ:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CNPJ' name='fEmpCnpj' id="cnpj"  onchange="validarCNPJ();" required   value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpCnpj() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Cep">Cep:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' placeholder='CEP' id="cep" name='fEmpEndCep'  required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndCep() : ""?>'/>
                    <div id="div123">&nbsp;</div>
				</div>
					<label class="col-sm-1 control-label"  style="text-align:left" for="Cidade">Cidade:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' placeholder='Cidade' id="cidade" name='fEmpEndCidade' required   value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndCidade() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="Bairro">Bairro:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' placeholder='Bairro' id="bairro" name='fEmpEndBairro' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndBairro() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="Uf">UF:</label>
					<div class="col-sm-1">
					<input class="form-control" type='text' placeholder='UF' id="estado" name='fEmpEndUf' required   value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndUf() : ""?>'/>
				</div>
				</div>
				<br>
				<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Endereço">Endereço:</label>
					<div class="col-sm-8">
					<input class="form-control" type='text' placeholder='Endereço' id="rua" name='fEmpEndLogra' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndLogra() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndFone">Fone:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' placeholder='Fone' name='fEmpEndFone' id="fone" required    value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpSite">Site:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' placeholder='Site' name='fEmpSite' required    value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpSite() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFtp">FTP:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' placeholder='FTP' name='fEmpFtp'  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpFtp() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEmail">Email:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' placeholder='Email' name='fEmpEmail' id="fEmpEmail" onchange="valida_email();"  required   value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEmail() : ""?>'/>
				</div>
				</div>
				<br>
             <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Imagem"> Logomarca: </label>
					<div class="col-sm-3">
                    <input class="filestyle" data-buttonName="btn-primary"  data-input="false" <?=($_REQUEST['sOP'] == 'Cadastrar') ? " required  " : "" ?> name="fEmpImagem" type="file" size="50"></div>
                    <?php if(($oSysEmpresa) && ($oSysEmpresa->getEmpImagem()) ){ ?>
                    	<div class="col-sm-3">
                        	<img src="view/sys/sys_empresa/logomarcas/<?=$oSysEmpresa->getEmpImagem()?>" class="img-responsive">
                        </div>
                    <?php }?>
				</div>

				<br>
				 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Aceite">Aceite:</label>
					<div class="col-sm-3">
                    <select name="fEmpAceite" class="form-control" required>
                    	<option value="0" <?=( ($oSysEmpresa) && ($oSysEmpresa->getEmpAceite() == 0)) ? " selected " : " " ?>>NO CADASTRO</option>
                        <option value="1" <?=( ($oSysEmpresa) && ($oSysEmpresa->getEmpAceite() == 1)) ? " selected " : " " ?>>SOMENTE AUTORIZADO</option>
					</select>
                    </div>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Aceite">Unidade:</label>
					<div class="col-sm-3">
                    <select name="fUniCodigo" class="form-control" required>
                        <? if($voUnidade){?>
                        <option value="">Selecione</option>
                            <? foreach($voUnidade as $oUnidade){
                                $sSelect = ($oSysEmpresa && $oSysEmpresa->getUniCodigo() == $oUnidade->getPlanoContasCodigo())? " selected " : "";
                            ?>
                            <option <?=$sSelect?> value="<?=$oUnidade->getPlanoContasCodigo()?>"><?=$oUnidade->getDescricao()?></option>
                            <? } ?>
                        <? } ?>
					</select>
                    </div>
				</div>
				<br>
				</fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2" id="botaoGravar">
       	<button type="submit"  class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>


        <div id="msgCNPJ" title="Mensagem" style="display:none"><p>CNPJ INVÁLIDO!</p></div>
	   <div id="msgEmail" title="Mensagem" style="display:none"><p>EMAIL INVÁLIDO!</p></div>

        <script src="js/jquery/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
		<script type='text/javascript' src='js/cep.js'></script>
 		<script type="text/javascript" language="javascript">

        jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

        jQuery(function($){
           $("#dataAdmissao").mask("99/99/9999");
           $("#cep").mask("99.999-999");
           $("#fone").mask("(99) 9999-9999");
           $("#dataDemissao").mask("99/99/9999");
           $("#cnpj").mask("99.999.999/9999-99");
        });

        function validarCNPJ(){
            valor = document.formSysEmpresa.fEmpCnpj;
            oDiv = document.getElementById('botaoGravar');

            if (validar_CNPJ(valor.value) === false){
                $(function() {
                    $( "#msgCNPJ" ).dialog();
                    oDiv.innerHTML = "<font color='red'><strong>Corrija o campo CNPJ</strong></font>";
                    document.getElementById("cnpj").style.backgroundColor = '#F89C8F';
                    return false;
                });
            }else{
                document.getElementById("cnpj").style.backgroundColor = '';
                oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";

            }
        }

        function valida_email(){
            valor = document.formSysEmpresa.fEmpEmail;
            oDiv = document.getElementById('botaoGravar');

            if (validacaoEmail(valor) === false){
                $(function() {
                    $( "#msgEmail" ).dialog();
                    oDiv.innerHTML = "<font color='red'><strong>Email inválido</strong></font>";

                    document.getElementById("fEmpEmail").style.backgroundColor = '#F89C8F';
                    return false;
                });
            }else{
                document.getElementById("fEmpEmail").style.backgroundColor = '';
                oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";

            }
        }

	</script>
       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
