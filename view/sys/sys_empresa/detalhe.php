﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSysEmpresa = $_REQUEST['oSysEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>SysEmpresa - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SysEmpresa.preparaLista">Gerenciar SysEmpresas</a> &gt; <strong><?php echo $sOP?> SysEmpresa</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> SysEmpresa</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSysEmpresa" action="?action=SysEmpresa.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do SysEmpresa" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">EmpCodigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpCodigo' name='fEmpCodigo' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpRazaoSocial">EmpRazaoSocial:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpRazaoSocial' name='fEmpRazaoSocial' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpRazaoSocial() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFantasia">EmpFantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpFantasia' name='fEmpFantasia' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpFantasia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCnpj">EmpCnpj:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpCnpj' name='fEmpCnpj' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpCnpj() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpImagem">EmpImagem:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpImagem' name='fEmpImagem' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpImagem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndLogra">EmpEndLogra:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndLogra' name='fEmpEndLogra' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndLogra() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndBairro">EmpEndBairro:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndBairro' name='fEmpEndBairro' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndBairro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndCidade">EmpEndCidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndCidade' name='fEmpEndCidade' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndCidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndCep">EmpEndCep:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndCep' name='fEmpEndCep' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndCep() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndUf">EmpEndUf:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndUf' name='fEmpEndUf' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndUf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndFone">EmpEndFone:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEndFone' name='fEmpEndFone' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEndFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpSite">EmpSite:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpSite' name='fEmpSite' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpSite() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFtp">EmpFtp:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpFtp' name='fEmpFtp' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpFtp() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEmail">EmpEmail:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpEmail' name='fEmpEmail' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpEmail() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpInc">EmpInc:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpInc' name='fEmpInc' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpAlt">EmpAlt:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpAlt' name='fEmpAlt' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpAlt() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpAceite">EmpAceite:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='EmpAceite' name='fEmpAceite' required  value='<?= ($oSysEmpresa) ? $oSysEmpresa->getEmpAceite() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=SysEmpresa.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
