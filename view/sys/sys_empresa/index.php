<?php
 $voSysEmpresa = $_REQUEST['voSysEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Empresas</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"></a> <strong><a href="?action=SysEmpresa.preparaLista">Gerenciar Empresas</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Empresas</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formSysEmpresa" id="formSysEmpresa" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesSysEmpresa" onChange="JavaScript: submeteForm('SysEmpresa')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=SysEmpresa.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar nova Empresa</option>
   						<option value="?action=SysEmpresa.preparaFormulario&sOP=Alterar" lang="1">Alterar Empresa selecionada</option>
   						<option value="?action=Login.processaFormulario&sOP=EscolheEmpresa" lang="1">Escolher Empresa selecionada</option>
   						<option value="?action=SysEmpresa.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Empresa selecionada</option>
   						<option value="?action=SysEmpresa.processaFormulario&sOP=Excluir" lang="2">Excluir Empresa(s) selecionada(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voSysEmpresa)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('SysEmpresa')" src="imagens/checkbox.gif" width="16" height="16"></th>
					<th class='Titulo'>Razão Social</th>
					<th class='Titulo'>Nome Fantasia</th>
					<th class='Titulo'>CNPJ</th>
					<th class='Titulo'>Endereço</th>
					<th class='Titulo'>Bairro</th>
					<th class='Titulo'>Cidade</th>
					<th class='Titulo'>Cep</th>
					<th class='Titulo'>Fone</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voSysEmpresa as $oSysEmpresa){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('SysEmpresa')" type="checkbox" value="<?=$oSysEmpresa->getEmpCodigo()?>" name="fIdSysEmpresa[]"/></td>
					<td><?= $oSysEmpresa->getEmpRazaoSocial()?></td>
					<td><?= $oSysEmpresa->getEmpFantasia()?></td>
					<td><?= $oSysEmpresa->getEmpCnpj()?></td>
					<td><?= $oSysEmpresa->getEmpEndLogra()?></td>
					<td><?= $oSysEmpresa->getEmpEndBairro()?></td>
					<td><?= $oSysEmpresa->getEmpEndCidade()?></td>
					<td><?= $oSysEmpresa->getEmpEndCep()?></td>
					<td><?= $oSysEmpresa->getEmpEndFone()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voSysEmpresa)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('SysEmpresa');
  	 </script>

       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
