﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupChamado = $_REQUEST['oSupChamado'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Aporte - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
 <meta name="msapplication-TileColor" content="#ffffff">
 <meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
 <meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

    <style>
        .pendente , .realizado, .analise{border-radius:20px;color:#FFF;size:22px;font-weight:bold;padding:10px;}
        .pendente {background-color: red !important;}
        .realizado {background-color: green !important;}
        .analise {background-color: yellow;color:black;}

    </style>


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("includes/topo.php")?>
   </header>
      <?php include_once("includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupChamado.preparaLista">Gerenciar Chamado </a> &gt; <strong><?php echo $sOP?> Aporte</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Chamado</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->

                    <div class="form-group">
                         <div class="col-sm-12 "><strong>Solicitante:&nbsp;</strong><?=$oSupChamado->getRealizadoPor()?></div>
                    </div>
                    <br>
                    <br>
                     <div class="form-group">
                           <div class="col-sm-12 "><strong>Desrição do Chamado:&nbsp;</strong><?=$oSupChamado->getDescricaoChamado()?></div>
                      </div>
                       <br>
                      <div class="form-group">
                            <div class="col-sm-12 "><strong>Situação:&nbsp;</strong>
                                <?
                                   switch($oSupChamado->getSituacao()){
                                       case 1: echo "<small class='label label-warning'><i class='fa fa-clock-o'></i>Em Análise</small>";break;
                                       case 2: echo "<small class='label label-success'><i class='fa fa-clock-o'></i>Realizado</small>";break;
                                       default: echo "<small class='label label-danger'><i class='fa fa-clock-o'></i>Pendente</small>";break;
                                   }
                                ?>
                            </div>
                      </div>
                      <br>
                      <div class="form-group">
                            <label class="col-sm-12 control-label" style="text-align:left" for="MnyMovimento">Motivo:</label>
                            <div class="col-sm-12">
                                <textarea class="form-control" disabled style="height:200px;"><?=(is_object($oSupChamado)) ? $oSupChamado->getMotivo():""?></textarea>
                            </div>
                      </div>
                      <br>

                   <div class="form-group" >
                        <div class="col-sm-offset-5 col-sm-2" ><a class="btn btn-primary" href="?action=SupChamado.preparaLista">Voltar</a></div>
                   </div>
        </section>

       <!-- InstanceEndEditable -->

   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
