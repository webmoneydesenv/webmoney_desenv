<?php
 $sOP = $_REQUEST['sOP'];
 $oSupChamado= $_REQUEST['oSupChamado'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Chamado - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupChamado.preparaLista">Gerenciar Chamado</a> &gt; <strong><?php echo $sOP?> Chamado</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Chamado</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupChamado" id='formSupChamado' action="?action=SupChamado.processaFormulario">
         <input type="hidden" name="sOP" id="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodChamado" value="<?=(is_object($oSupChamado)) ? $oSupChamado->getCodChamado() : ""?>" />
         <input type="hidden" name="fCodUsuario" value="<?=(is_object($oSupChamado)) ? $oSupChamado->getCodUsuario() : ""?>" />
		 <input type='hidden' name='fSituacao' value='<?= ($oSupChamado) ? $oSupChamado->getSituacao() : ""?>'/>
		 <input type='hidden' name='fRealizadoPor' value='<?= ($oSupChamado) ? $oSupChamado->getRealizadoPor() : $_SESSION['Perfil']['Login'].'||'.date('d/m/Y H:i:s') ?>'/>
		 <input type='hidden' name='fAtivo' value='<?= ($oSupChamado) ? $oSupChamado->getAtivo() : "1"?>'/>
         <div id="formulario" class="TabelaAdministracao">
             <fieldset title="Dados do Chamado">
                  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">Descrição do Chamado:</label>
                        <div class="col-sm-6">
                           <input type="text" name="fDescricaoChamado" class='form-control' value='<?=(is_object($oSupChamado)) ? $oSupChamado->getDescricaoChamado() : ""?>'>
                        </div>
                  </div>
                  <br>

                  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnySolicitacaoAporte">Motivo:</label>
                        <div class="col-sm-6">
                             <textarea name="fMotivo" class='form-control' value='<?=(is_object($oSupChamado)) ? $oSupChamado->getMotivo() : ""?>'><?=(is_object($oSupChamado)) ? $oSupChamado->getMotivo() : ""?></textarea>
                        </div>
                  </div>
                  <br>
                 <? if($_SESSION['Perfil']['CodGrupoUsuario']==1){?>
                      <div class="form-group">
                            <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">Situação:</label>
                            <div class="col-sm-6">
                               <select name='fSituacao'  class="form-control chosen"  required  >
                                   <option  <?= ($oSupChamado && $oSupChamado->getSituacao() == 0) ? "selected" : ""?> value='0'>Pendente</option>
                                   <option  <?= ($oSupChamado && $oSupChamado->getSituacao() == 1) ? "selected" : ""?> value='1'>Em Análise</option>
                                </select>
                            </div>
                      </div>
                         <? if($_REQUEST['sOP'] == "Alterar" && $oSupChamado->getSituacao() != 2){?>
                              <div class="form-group">
                                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">&nbsp;</label>
                                    <div class="col-sm-6">
                                           <input type="button" class="btn-success" onclick="finalizaChamado();" value='Finalizar Chamado'>
                                    </div>
                              </div>
                        <? } ?>

                 <?}?>
                  <br>

            </fieldset>
		</div>
		<br>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-2">
                 <button type="submit" class="btn btn-primary" ><?=$sOP?></button>
            </div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

            jQuery(function($){});

            function finalizaChamado(){
                document.getElementById('sOP').value='Finalizar';
                document.getElementById("formSupChamado").submit();
            }


 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
