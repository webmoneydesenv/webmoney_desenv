<?php
 $voSupChamado = $_REQUEST['voSupChamado'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>

 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista Chamados</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>

   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=SupChamado.preparaLista">Gerenciar Chamados</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Chamados</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formSupChamado" id="formSupChamado" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesSupChamado" onChange="JavaScript: submeteForm('SupChamado')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=SupChamado.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Chamado</option>
   						<option value="?action=SupChamado.preparaFormulario&sOP=Alterar" lang="1">Alterar Chamado selecionado</option>
   						<option value="?action=SupChamado.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Chamado selecionado</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voSupChamado)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('SupChamado')" src="imagens/checkbox.gif" width="16" height="16"></th>
					<th class='Titulo'>Descrição</th>
                    <th class='Titulo'>Motivo</th>
					<th class='Titulo'>Situação</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voSupChamado as $oSupChamado){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('SupChamado')" type="checkbox" value="<?=$oSupChamado->getCodChamado()?>" name="fIdSupChamado[]"/></td>
                    <td><?= $oSupChamado->getDescricaoChamado()?></td>
                    <td><?= $oSupChamado->getMotivo()?></td>
					<td><?
                         switch($oSupChamado->getSituacao()){
                             case 0:
                                 echo "<small class='label label-danger'><i class='fa fa-clock-o'></i>Pendente</small>";
                             break;
                             case 1:
                                 echo "<small class='label label-warning'><i class='fa fa-clock-o'></i>Em Análise</small>";
                             break;
                             case 2:
                                 echo "<small class='label label-success'><i class='fa fa-clock-o'></i>Realizado</small>";
                             break;
                         }
                    ?>
                    </td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voSupChamado)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function(){
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('SupChamado');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
