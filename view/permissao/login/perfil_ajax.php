<?php
header("Content-Type: text/html; text/css; charset=UTF-8",true);
$voVAcessoUsuarioUnidade = $_REQUEST['voVAcessoUsuarioUnidade'];
$voSysEmpresa = $_REQUEST['voSysEmpresa'];
?>


        <div class="col-sm-3">
            <? if($voSysEmpresa){ ?>
            <select name="fEmpCodigo" class="form-control" required>
                <option value="">Selecione</option>
                <?php foreach($voSysEmpresa as $oSysEmpresa){
                    if(($oEmpresaSelecionada) && $oSysEmpresa->getEmpCodigo() == $oEmpresaSelecionada->getEmpCodigo()){
                        $sSelect = " selected='selected' ";
                    }else{
                        $sSelect = " ";
                    }
                  ?>
                <option <?php echo $sSelect; ?> value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                <? } ?>
            </select>
            <? } ?>
        </div>
<div class="form-group">

<div class='col-sm-3'>
    <select name='fCodGrupoUsuario'  id='selPerfil' class="form-control chosen"  required onchange="document.getElementById('btn_submit').disabled = false">
          <? if($voVAcessoUsuarioUnidade){ ?>
            <option  value=''>Selecione</option>
            <?   foreach($voVAcessoUsuarioUnidade as $oVAcessoUsuarioUnidade){?>
                   <option value='<?= $oVAcessoUsuarioUnidade->getCodGrupoUsuario() ?>' > <?= $oVAcessoUsuarioUnidade->getGrupo()?> </option>
           <?  }
           } ?>
    </select>
</div>
</div>

<div class="col-sm-4">
    <?= (!$voVAcessoUsuarioUnidade) ? "<span style='color:red;'>*Você não possui perfil cadastrado para esta unidade</span>" : ""?>
</div>

<?php
if($voVAcessoUsuarioUnidade){

 $_SESSION['Perfil']['CodUnidade']  =  $voVAcessoUsuarioUnidade[0]->getPlanoContasCodigo();
 $_SESSION['Perfil']['Unidade']     = $voVAcessoUsuarioUnidade[0]->getUnidade();
 $_SESSION['Perfil']['CodUsuario']  =  $voVAcessoUsuarioUnidade[0]->getCodUsuario();
 $_SESSION['Perfil']['Nome']        =  $voVAcessoUsuarioUnidade[0]->getNome();
 $_SESSION['Perfil']['Login']       =  $voVAcessoUsuarioUnidade[0]->getLogin();
}
?>
