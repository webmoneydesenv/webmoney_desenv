<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
<title>Sistema WebMoney</title>
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet prefetch' href='css/foundation.min.css'>
<link href="css/style.css" rel="stylesheet" type="text/css">
    <style type="text/css" media="all">


        .login-box {
          background: #E7EEF2;
          border: 1px solid #ddd;
          margin: 100px 0;
          padding: 40px;
          border-radius:8px;
        }

    </style>


</head>

<body>

  <div class="large-3 large-centered columns">
  <div class="login-box">
  <div class="row">
  <div class="large-12 columns">
    <form  action="?action=Login.processaFormulario&amp;sOP=Logon" method="post">
       <div class="row">
         <div class="large-12 columns" style="padding-bottom:20px"><img src="imagens/logo.gif" class="img-responsive">
         </div>
       </div>
        <div class="row">
         <div class="large-12 columns">
             <input type="text" name="fLogin" placeholder="Login"  required autofocus/>
         </div>
       </div>
      <div class="row">
         <div class="large-12 columns">
             <input type="password" name="fSenha" placeholder="Senha"  required/>
         </div>
      </div>
      <div class="row">
        <div class="large-12 large-centered columns">
          <input type="submit" class="button expand" style="background-color:#449D44;font-weight:bold" value="Entrar"/>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>

	<script src="js/jquery/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
    <div align="center"><?php require_once("/view/includes/mensagem.php")?></div>

</body>

</html>
