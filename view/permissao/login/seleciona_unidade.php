<?php
 $sOP = $_REQUEST['sOP'];
 $voVAcessoUsuarioUnidade = $_REQUEST['voVAcessoUsuarioUnidade'];
 $oEmpresaSelecionada = $_REQUEST['oEmpresaSelecionada'];
//print_r($voVAcessoUsuarioUnidade);
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Empresa - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
 <meta name="msapplication-TileColor" content="#ffffff">
 <meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
 <meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("/view/includes/topo.php")?>
      <?php //include_once("/view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <!-- InstanceBeginEditable name="Migalha" --><a href="index.php">Home </a> &gt; <strong>Selecione Empresa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 style="margin-top:40px;"></h3>
       <!-- InstanceEndEditable --></h1>
     <section style="min-height:450px; margin-let:50px;">
 		<!-- InstanceBeginEditable name="conteudo" -->

        <div class="col-md-12">
			<div class="panel panel-success" style="border-color: #F5F5F5; background-color:#F5F5F5;">
				<div class="panel-heading" style="background-color:#00657F; color:#FFF; ">
					<h3 class="panel-title">Selecione Perfil</h3>
					<span class="pull-right clickable"></span>
				</div>
				<div class="panel-body">


		<form method="post" class="form-horizontal" name="formSysEmpresa" action="?action=Login.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="EscolheEmpresa2">
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados da Unidade">
           <div class="form-group">
           </div>
				<br>

            <div class="form-group">
					<div class="col-sm-8">
                        <? if($voSysEmpresa){ ?>
                        <!--<select name="fEmpCodigo" class="form-control" onchange="recuperaConteudoDinamico('index.php','action=Login.recuperaUnidadePorEmpresa&nEmpCodigo='+this.value,'divUnidade')"  required>-->
                        <select name="fEmpCodigo" class="form-control" required>
                            <option value="">Selecione</option>
                            <?php foreach($voSysEmpresa as $oSysEmpresa){
                                if(($oEmpresaSelecionada) && $oSysEmpresa->getEmpCodigo() == $oEmpresaSelecionada->getEmpCodigo()){
                                    $sSelect = " selected='selected' ";
                                }else{
                                    $sSelect = " ";
                                }
                              ?>
                            <option <?php echo $sSelect; ?> value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                            <? } ?>
                        </select>
                        <? } ?>
                    </div>

             </div>
             <?php if(!$_SESSION['Perfil']['Unidade']){?>
             <div class="form-group">
             <div class='col-sm-6'>
				   <select name='fUnidade'  class="form-control chosen"  required onchange="recuperaConteudoDinamico('index.php','action=Login.atualizaPerfil&nCodUsuario=<?= $_SESSION['oUsuarioImoney']->getCodUsuario()?>&nCodUnidade='+this.value,'divPerfil');document.getElementById('btn_submit').disabled = true;" >

                        <option value=''>Selecione</option>

                        <? if($voVAcessoUsuarioUnidade){
                                  foreach($voVAcessoUsuarioUnidade as $oVAcessoUsuarioUnidade){?>

                                       <option value='<?= $oVAcessoUsuarioUnidade->getPlanoContasCodigo() ?>' > <?= $oVAcessoUsuarioUnidade->getUnidade()?> </option>

                               <?  }

                           } ?>

                    </select>

                </div>

             </div>
             <?php } ?>

             <div id='divPerfil'>&nbsp;</div>

             <div class="form-group">
                 <div class="col-sm-8">
                    <button type="submit" class="btn btn-success"  id='btn_submit' >Selecionar <span class="glyphicon glyphicon-ok-circle"></span></button>
                    <a href="?action=Login.processaFormulario&sOP=Logoff" class="btn btn-danger" role="button">Sair  <span class="glyphicon glyphicon-off"></span></a>
                </div>
             </div>

         </fieldset>

       </form>

				</div>
			</div>



 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <script>
        document.getElementById('btn_submit').disabled = true;
           <?php if(!$_SESSION[Perfil]){?>
           function verificaUnidade(nEmpCodigo){
                recuperaConteudoDinamico('index.php','action=SysEmpresa.verificaUnidade&nCodUsuario=<?= $_SESSION['oUsuarioImoney']->getCodUsuario()?>&nEmpCodigo='+nEmpCodigo,'divUnidade');
                document.getElementById('divPerfil').innerHTML='';
                document.getElementById('btn_submit').disabled = true;
            }

            function verificaPerfil(){
                if(document.getElementById('selPerfil').value== ''){
                    document.getElementById('btn_submit').disabled = true;
                }else{
                    document.getElementById('btn_submit').disabled = false;
                }

            }

           <?php }else{ ?>
            document.getElementById('btn_submit').disabled = false;
           <?php }  ?>
        </script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("/view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("/view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
