﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoGrupoUsuario = $_REQUEST['oAcessoGrupoUsuario'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Grupo de Usuário - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoGrupoUsuario.preparaLista">Gerenciar Grupo de Usuários</a> &gt; <strong><?php echo $sOP?> Grupo de Usuário</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Grupo de Usuário</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoGrupoUsuario" action="?action=AcessoGrupoUsuario.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Grupo de Usuário" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodGrupoUsuario">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CodGrupoUsuario' name='fCodGrupoUsuario' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo CodGrupoUsuario")'  value='<?= ($oAcessoGrupoUsuario) ? $oAcessoGrupoUsuario->getCodGrupoUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descrição">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descrição' name='fDescricao' value='<?= ($oAcessoGrupoUsuario) ? $oAcessoGrupoUsuario->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oAcessoGrupoUsuario) ? $oAcessoGrupoUsuario->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=AcessoGrupoUsuario.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
