<?php
 $sOP = $_REQUEST['sOP'];
 $voAcessoUnidadeEmpresa = $_REQUEST['voAcessoUnidadeEmpresa'];


 $voUnidade = $_REQUEST['voUnidade'];
 $voSysEmpresa = $_REQUEST['voEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Unidade - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
     <link rel="stylesheet" href="css/bootstrap-select-min.css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoUnidadeEmpresa.preparaLista">Gerenciar Unidades</a> &gt; <strong><?php echo $sOP?> Unidade</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Unidade</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoUnidadeEmpresa" action="?action=AcessoUnidadeEmpresa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados da Unidade">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodUnidade">Unidade:</label>
					<div class="col-sm-10">

					<select name='fCodUnidade'  class="form-control chosen"  required  <?=($sOP=='Alterar')? " readonly='readonly' ":""?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voUnidade){
							   foreach($voUnidade as $oMnyPlanoContas){
								   if($voAcessoUnidadeEmpresa){
									   $sSelected = ($voAcessoUnidadeEmpresa[0]->getCodUnidade() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Empresa:</label>
					<div class="col-sm-10">
					<select name='fEmpCodigo[]'  class="form-control selectpicker"  required id='fEmpCodigo' multiple data-actions-box='true'
                            >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysEmpresa){
							   foreach($voSysEmpresa as $oSysEmpresa){
								   if($voAcessoUnidadeEmpresa){
                                       $sSelected  = (in_array($oSysEmpresa->getEmpCodigo(), $voAcessoUnidadeEmpresa[0]->getSysEmpresa())) ? "selected" : "";

								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysEmpresa->getEmpCodigo()?>'><?= $oSysEmpresa->getEmpFantasia()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
  			$('select[readonly=readonly] option:not(:selected)').prop('disabled', true);
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen();});
             jQuery(function($){$('.selectpicker').selectpicker();});
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
