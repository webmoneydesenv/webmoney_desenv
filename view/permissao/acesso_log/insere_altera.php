<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoLog = $_REQUEST['oAcessoLog'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>AcessoLog - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoLog.preparaLista">Gerenciar AcessoLogs</a> &gt; <strong><?php echo $sOP?> AcessoLog</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> AcessoLog</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoLog" action="?action=AcessoLog.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodAcesso" value="<?=(is_object($oAcessoLog)) ? $oAcessoLog->getCodAcesso() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do AcessoLog">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodAcesso">CodAcesso:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CodAcesso' name='fCodAcesso' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo CodAcesso")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getCodAcesso() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Usuario">Usuario:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Usuario' name='fUsuario' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Usuario")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Tabela">Tabela:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tabela' name='fTabela' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Tabela")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getTabela() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Operacao">Operacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Operacao' name='fOperacao' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Operacao")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getOperacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataLog">DataLog:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='DataLog' name='fDataLog' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo DataLog")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getDataLog() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fDescricao' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Descricao")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SqlDescricao">SqlDescricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='SqlDescricao' name='fSqlDescricao' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo SqlDescricao")'  value='<?= ($oAcessoLog) ? $oAcessoLog->getSqlDescricao() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oAcessoLog) ? $oAcessoLog->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
  		<script type="text/javascript" language="javascript">
  		 /*
  			jQuery(function($){
  			   $("#data").mask("99/99/9999");
  			   $("#cpf").mask("999.999.999-99");
  			   $("#fone").mask("(99) 9999-9999");
  			});
  		 */
  	 </script>

       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
