<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoLogExclusao = $_REQUEST['oAcessoLogExclusao'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>acesso_log_exclusao - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoLogExclusao.preparaLista">Gerenciar acesso_log_exclusaos</a> &gt; <strong><?php echo $sOP?> acesso_log_exclusao</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> acesso_log_exclusao</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoLogExclusao" action="?action=AcessoLogExclusao.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodExclusao" value="<?=(is_object($oAcessoLogExclusao)) ? $oAcessoLogExclusao->getCodExclusao() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do acesso_log_exclusao">

 							<input type='hidden' name='fCodExclusao' value='<?= ($oAcessoLogExclusao) ? $oAcessoLogExclusao->getCodExclusao() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Fa">Fa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Fa' placeholder='Fa' name='fFa'   value='<?= ($oAcessoLogExclusao) ? $oAcessoLogExclusao->getFa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="RealizadoPor">Realizado_por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='RealizadoPor' placeholder='Realizado_por' name='fRealizadoPor'   value='<?= ($oAcessoLogExclusao) ? $oAcessoLogExclusao->getRealizadoPor() : ""?>'/>
				</div>
				</div>
				<br>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Motivo">Motivo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Motivo' placeholder='Motivo' name='fMotivo'   value='<?= ($oAcessoLogExclusao) ? $oAcessoLogExclusao->getMotivo() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#Data").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
