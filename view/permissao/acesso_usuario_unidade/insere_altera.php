<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuarioUnidade = $_REQUEST['oAcessoUsuarioUnidade'];
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];

$voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];

$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>acesso_usuario_unidade - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoUsuarioUnidade.preparaLista">Gerenciar acesso_usuario_unidades</a> &gt; <strong><?php echo $sOP?> acesso_usuario_unidade</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> acesso_usuario_unidade</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoUsuarioUnidade" action="?action=AcessoUsuarioUnidade.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodUsuario" value="<?=(is_object($oAcessoUsuarioUnidade)) ? $oAcessoUsuarioUnidade->getCodUsuario() : ""?>" />
			<input type="hidden" name="fCodGrupoUsuario" value="<?=(is_object($oAcessoUsuarioUnidade)) ? $oAcessoUsuarioUnidade->getCodGrupoUsuario() : ""?>" />
			<input type="hidden" name="fCodPlanoContas" value="<?=(is_object($oAcessoUsuarioUnidade)) ? $oAcessoUsuarioUnidade->getCodPlanoContas() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do acesso_usuario_unidade">


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="AcessoUsuario">Cod_usuario:</label>
					<div class="col-sm-10">
					<select name='fCodUsuario'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoUsuario){
							   foreach($voAcessoUsuario as $oAcessoUsuario){
								   if($oAcessoUsuarioUnidade){
									   $sSelected = ($oAcessoUsuarioUnidade->getCodUsuario() == $oAcessoUsuario->getCodUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoUsuario->getCodUsuario()?>'><?= $oAcessoUsuario->getCodUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="AcessoGrupoUsuario">Cod_grupo_usuario:</label>
					<div class="col-sm-10">
					<select name='fCodGrupoUsuario'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoGrupoUsuario){
							   foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
								   if($oAcessoUsuarioUnidade){
									   $sSelected = ($oAcessoUsuarioUnidade->getCodGrupoUsuario() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Cod_plano_contas:</label>
					<div class="col-sm-10">
					<select name='fCodPlanoContas'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oAcessoUsuarioUnidade){
									   $sSelected = ($oAcessoUsuarioUnidade->getCodPlanoContas() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
