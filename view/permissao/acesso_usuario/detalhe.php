<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuario = $_REQUEST['oAcessoUsuario'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>AcessoUsuario - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoUsuario.preparaLista">Gerenciar Usuários</a> &gt; <strong><?php echo $sOP?> Usuário</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Usuário</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoUsuario" action="?action=AcessoUsuario.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do AcessoUsuario" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodUsuario">Cod:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CodUsuario' name='fCodUsuario' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo CodUsuario")'  value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getCodUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					Grupo
					  <label class="col-sm-2 control-label" style="text-align:left" for="AcessoGrupoUsuario">:</label>
					<div class="col-sm-10">
					<select name='fCodGrupoUsuario'  class="form-control" required  oninvalid=\'setCustomValidity("Por favor, preencha o campo AcessoGrupoUsuario")' >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoGrupoUsuario){
							   foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
								   if($oAcessoUsuario){
									   $sSelected = ($oAcessoUsuario->getCodGrupoUsuario() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
   <div class="col-sm-11"></div>
				</div>
 <div class="form-group">
			<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome' name='fNome' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Nome")'  value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Login">Login:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Login' name='fLogin' required  oninvalid=\'setCustomValidity("Por favor, preencha o campo Login")'  value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getLogin() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
   <div class="col-sm-11"></div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=AcessoUsuario.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>ar
