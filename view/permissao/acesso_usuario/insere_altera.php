<?php
 $sOP = $_REQUEST['sOP'];
 $oAcessoUsuario = $_REQUEST['oAcessoUsuario'];
  $voAcessoUsuarioUnidade = $_REQUEST['voAcessoUsuarioUnidade'];

$voVPessoaGeralFormatada = $_REQUEST['voVPessoaGeralFormatada'];
 $voAcessoGrupoUsuario  = $_REQUEST['voAcessoGrupoUsuario'] ;
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Usuário - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <link href="css/animate.css" rel="stylesheet" type="text/css">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=AcessoUsuario.preparaLista">Gerenciar Usuários</a> &gt; <strong><?php echo $sOP?> Usuário</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Usuário</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formAcessoUsuario" action="?action=AcessoUsuario.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodUsuario" value="<?=(is_object($oAcessoUsuario)) ? $oAcessoUsuario->getCodUsuario() : ""?>" />
         <input type="hidden" name="fAtivo" value="<?=(is_object($oAcessoUsuario)) ? $oAcessoUsuario->getAtivo() : "1"?>" />
         <input class="form-control" type='hidden' placeholder='Senha' name='fSenha' value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getSenha() : ""?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Usuário">



 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="AcessoGrupoUsuario">Nome:</label>
					<div class="col-sm-10">
					<select name='fPesgCodigo'  class="form-control chosen" required >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voVPessoaGeralFormatada){
							   foreach($voVPessoaGeralFormatada as $oPessoaFormatada){
								   if($oAcessoUsuario){
									   $sSelected = ($oPessoaFormatada->getPesgCodigo() == $oAcessoUsuario->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oPessoaFormatada->getPesgCodigo()?>'><?= $oPessoaFormatada->getNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Login">Login:</label>
					<div class="col-sm-10">
					<input class="form-control" type='text' placeholder='Login' name='fLogin' required  value='<?= ($oAcessoUsuario) ? $oAcessoUsuario->getLogin() : ""?>'/>
				</div>
				</div>
<? if($sOP == 'Cadastrar') {?>
 <br>
 <div class="form-group">
					<div class="col-sm-2"><strong>Senha inicial:</strong></div>
            <div class="col-sm-10" style="font-size:12px; color:red; font-weight:bold;">carmona123</div>
		   </div>
<? } ?>
		   <br>

       		<div class="form-group">
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary addButton" onClick="addCampos()">Associação de Perfis <i class="glyphicon glyphicon-plus"></i></button><br><br>
                <? if($sOP == 'Alterar' && $voAcessoUsuarioUnidade){ ?>
				<? foreach($voAcessoUsuarioUnidade as $oAcessoUsuarioUnidade){ ?>
                <div id="linha">
                    <div id="linha<?=($oAcessoUsuarioUnidade) ? $oAcessoUsuarioUnidade->getCodUsuario() ."||".$oAcessoUsuarioUnidade->getCodGrupoUsuario() ."||".$oAcessoUsuarioUnidade->getCodPlanoContas(): ""?>">
                          <div class='col-sm-4'>
                               <select name='fUnidade[]'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                                <? if($voMnyUnidade){
                                    foreach($voMnyUnidade as $oUnidade){
                                            if($oAcessoUsuarioUnidade->getCodPlanoContas() == $oUnidade->getPlanoContasCodigo())
                                                $sSelect = " selected ";
                                            else
                                                $sSelect = " ";
                                   ?>
                                       <option <?php echo $sSelect?> value='<?= $oUnidade->getPlanoContasCodigo() ?>' > <?= $oUnidade->getDescricao()?> </option>
                               <?   }
                                   } ?>
                               </select>
                          </div>
                         <div class='col-sm-4'>
                               <select name='fGrupoUsuario[]'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                                <? if($voAcessoGrupoUsuario){
                                    foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
                                            if($oAcessoUsuarioUnidade->getCodGrupoUsuario() == $oAcessoGrupoUsuario->getCodGrupoUsuario())
                                                $sSelect = " selected ";
                                            else
                                                $sSelect = " ";
                                   ?>
                                       <option <?php echo $sSelect?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario() ?>' > <?= $oAcessoGrupoUsuario->getDescricao()?> </option>
                               <?   }
                                   } ?>
                               </select>
                          </div>

                          <button  type='button' class='btn btn-danger removeButton' onClick="recuperaConteudoDinamico('index.php','action=AcessoUsuarioUnidade.processaFormulario&sOP=Excluir2&fIdAcessoUsuarioUnidade=<?=($oAcessoUsuarioUnidade) ? $oAcessoUsuarioUnidade->getCodUsuario() ."||".$oAcessoUsuarioUnidade->getCodGrupoUsuario() ."||".$oAcessoUsuarioUnidade->getCodPlanoContas(): ""?>','linha<?=($oAcessoUsuarioUnidade) ? $oAcessoUsuarioUnidade->getCodUsuario() ."||".$oAcessoUsuarioUnidade->getCodGrupoUsuario() ."||".$oAcessoUsuarioUnidade->getCodPlanoContas(): ""?>');" value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button><br><br>




                      </div>

                    </div>
                <? } ?>

                </div>
                <? }else{ ?>
                    <div class='form-group'>
                      <div class='col-xs-4'>
                           <select name='fUnidade[]'  class="form-control chosen" id='AcessoUsuarioUnidade'  required >
                                <option value=''>Selecione a Unidade</option>
                                <? if($voMnyUnidade){
                                    foreach($voMnyUnidade as $oMnyUnidade){?>
                                       <option value='<?= $oMnyUnidade->getPlanoContasCodigo() ?>' > <?= $oMnyUnidade->getDescricao()?> </option>
                               <?   }
                                   } ?>
                            </select>
                      </div>
                      <div class='col-xs-4'>
                           <select name='fGrupoUsuario[]'  class="form-control chosen" id='AcessoGrupoUsuario'  required >
                                <option value=''>Selecione Grupo de Usuário</option>
                                <? if($voAcessoGrupoUsuario){
                                    foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){?>
                                       <option value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario() ?>' > <?= $oAcessoGrupoUsuario->getDescricao()?> </option>
                               <?   }
                                   } ?>
                           </select>
                      </div>
                    </div>
                <? } ?>
            </div>

        	<div class="col-xs-12">
          		<div id="campoPai"></div>
          	</div>





         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
  		<script type="text/javascript" language="javascript">
  		jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

        var qtdeCampos = 0;
//te
        function addCampos(){
            var objPai = document.getElementById("campoPai");
            //Criando o elemento DIV;
            var objFilho = document.createElement("div");
            //Definindo atributos ao objFilho:
            objFilho.setAttribute("id","filho"+qtdeCampos);

            //Inserindo o elemento no pai:
            objPai.appendChild(objFilho);
            //Escrevendo algo no filho recém-criado:

             document.getElementById("filho"+qtdeCampos).innerHTML = "<div class='form-group'><div class='col-xs-4'><select name='fUnidade[]'  class='form-control chosen' id='AcessoUsuarioUnidade"+qtdeCampos+"'  required ><option value=''>Selecione a Unidade</option><? if($voMnyUnidade){ foreach($voMnyUnidade as $oMnyUnidade){?><option value='<?= $oMnyUnidade->getPlanoContasCodigo() ?>' > <?= $oMnyUnidade->getDescricao()?> </option><?  } } ?></select></div><div class='col-xs-4'><select name='fGrupoUsuario[]'  class='form-control chosen' id='AcessoGrupoUsuario"+qtdeCampos+"'  required ><option value=''>Selecione Grupo de Usuário</option><? if($voAcessoGrupoUsuario){foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){?><option value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario() ?>' > <?= $oAcessoGrupoUsuario->getDescricao()?> </option><? } } ?> </select></div><button  type='button' class='btn btn-danger removeButton' onclick='removerCampo("+qtdeCampos+")' value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>";


            var x = document.createElement("SCRIPT");
             qtdeCampos++;
        }

        function removerCampo(id){
            var objPai = document.getElementById("campoPai");
            var objFilho = document.getElementById("filho"+id);
            //Removendo o DIV com id específico do nó-pai:
            var removido = objPai.removeChild(objFilho);
        }

  	 </script>

       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
