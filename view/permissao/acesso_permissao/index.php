<?php
 $voAcessoPermissao = $_REQUEST['voAcessoPermissao'];
 $voAcessoModulo = $_REQUEST['voAcessoModulo'];
 $oGrupoUsuario = $_REQUEST['oGrupoUsuario'];
  set_time_limit(3600); // 30 minutos

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Permissões</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
      <?php include_once("view/includes/menu.php")?>
   </header>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=AcessoPermissao.preparaLista">Gerenciar Permissões</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Permissões - <?=$oGrupoUsuario->getDescricao()?></h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <style type="text/css">
			.list-group li {
				list-style:none;
				padding-bottom:3px;
				padding-left:1em;
				}
		</style>
 <form method="post" action="index.php?action=AcessoPermissao.processaFormulario" name="formAcessoPermissao" id="formAcessoPermissao" class="formulario">
	<input type="hidden" name="fCodGrupoUsuario" value="<?=$oGrupoUsuario->getCodGrupoUsuario()?>">
	<input type="hidden" name="sOP" value="Alterar">
<?php foreach($voAcessoModulo as $oModulo){?>
  <div id="row">
  <div class="list-group">
    <a href="#" onclick="MostraEsconde2('transacao_<?php echo $oModulo->getCodModulo()?>');" class="list-group-item"><?=$oModulo->getDescricao()?></a>
    	<div style="display:none" id="transacao_<?php echo $oModulo->getCodModulo()?>">
        	<?php if($oModulo->getTransacaoModulo()) {?>
            	<?php foreach($oModulo->getTransacaoModulo() as $oTransacaoModulo) {?>
	            	<li><input type="checkbox" value="<?= $oTransacaoModulo->getCodTransacaoModulo()?>" name="fCodTransacaoModulo[]" <?php echo($oGrupoUsuario->temPermissao($oTransacaoModulo->getCodTransacaoModulo())) ? "checked" : ""?>/><?=$oTransacaoModulo->getDescricao()?></li>
        	<?php  		}
			 ?>
             <? } ?>
        </div>

<? } ?>

  </div>
  </div>
	<div class="form-group">
   		<div class="col-sm-offset-5 col-sm-2">
       		<button type="submit" class="btn btn-primary" >Alterar</button>
     	</div>
   	</div>
<br/>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
         <script src="js/producao.js" type="text/javascript"></script>


       <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
       <script src=".js/ie10-viewport-bug-workaround.js"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
