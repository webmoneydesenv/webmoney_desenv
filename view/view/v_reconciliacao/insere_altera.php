<?php
 $sOP = $_REQUEST['sOP'];
 $oVReconciliacao = $_REQUEST['oVReconciliacao'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VReconciliacao.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVReconciliacao" action="?action=VReconciliacao.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Fa">Fa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Fa' placeholder='Fa' name='fFa'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getFa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPagar' placeholder='Valor' name='fMovValorPagar'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getMovValorPagarFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FpgCodigo">Forma de Pagamento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FpgCodigo' placeholder='Forma de Pagamento' name='fFpgCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getFpgCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaDePagamento">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FormaDePagamento' placeholder='Descricao' name='fFormaDePagamento'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getFormaDePagamento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipDocCodigo">Tipo de Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipDocCodigo' placeholder='Tipo de Documento' name='fTipDocCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getTipDocCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoDescricao' placeholder='Descricao' name='fTipoDescricao'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getTipoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Data">Data:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Data' placeholder='Data' name='fDataConciliacao'   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getDataFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpCodigo' placeholder='Empresa' name='fEmpCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovTipo">- Tipo Movimento:

0 - A Pagar

1 - A Receber

:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovTipo' placeholder='- Tipo Movimento:

0 - A Pagar

1 - A Receber

' name='fMovTipo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getMovTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovimentoTipo">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovimentoTipo' placeholder='Descricao' name='fMovimentoTipo'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getMovimentoTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ConCodigo">Con_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ConCodigo' placeholder='Con_codigo' name='fConCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getConCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaDescricao' placeholder='Descricao' name='fContaDescricao'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getContaDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UniCodigo">Unidade de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UniCodigo' placeholder='Unidade de Negócios' name='fUniCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getUniCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UnidadeDescricao' placeholder='Descricao' name='fUnidadeDescricao'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getUnidadeDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CenCodigo">Centro de Custo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CenCodigo' placeholder='Centro de Custo' name='fCenCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getCenCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CentroDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CentroDescricao' placeholder='Descricao' name='fCentroDescricao'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getCentroDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegCodigo">Centro de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='NegCodigo' placeholder='Centro de Negócios' name='fNegCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVReconciliacao) ? $oVReconciliacao->getNegCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeNegocioDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UnidadeNegocioDescricao' placeholder='Descricao' name='fUnidadeNegocioDescricao'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getUnidadeNegocioDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CcrConta">- Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CcrConta' placeholder='- Conta' name='fCcrConta'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getCcrConta() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Empresa">Razão Social:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Empresa' placeholder='Razão Social' name='fEmpresa'  required   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getEmpresa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovObs">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovObs' placeholder='Observação' name='fMovObs'   value='<?= ($oVReconciliacao) ? $oVReconciliacao->getMovObs() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#Data").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
