<?php
 $voVReconciliacao = $_REQUEST['voVReconciliacao'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>LISTA DE VIEWs</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=VReconciliacao.preparaLista">Gerenciar VIEWs</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar VIEWs</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formVReconciliacao" id="formVReconciliacao" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes" name="acoes" id="acoesVReconciliacao" onChange="JavaScript: submeteForm('VReconciliacao')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=VReconciliacao.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo VIEW</option>
   						<option value="?action=VReconciliacao.preparaFormulario&sOP=Alterar" lang="1">Alterar VIEW selecionado</option>
   						<option value="?action=VReconciliacao.preparaFormulario&sOP=Detalhar" lang="1">Detalhar VIEW selecionado</option>
   						<option value="?action=VReconciliacao.processaFormulario&sOP=Excluir" lang="2">Excluir VIEW(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voVReconciliacao)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('VReconciliacao')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Fa</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Forma de pagamento</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Tipo de documento</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Empresa</th>
					<th class='Titulo'>- tipo movimento:

0 - a pagar

1 - a receber

</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Documento</th>
					<th class='Titulo'>ConCodigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Unidade de negócios</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Centro de custo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Centro de negócios</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>- conta</th>
					<th class='Titulo'>Razão social</th>
					<th class='Titulo'>Observação</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voVReconciliacao as $oVReconciliacao){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('VReconciliacao')" type="checkbox" value="" name="fIdVReconciliacao[]"/></td>
  					<td><?= $oVReconciliacao->getFa()?></td>
					<td><?= $oVReconciliacao->getMovValorPagarFormatado()?></td>
					<td><?= $oVReconciliacao->getFpgCodigo()?></td>
					<td><?= $oVReconciliacao->getFormaDePagamento()?></td>
					<td><?= $oVReconciliacao->getTipDocCodigo()?></td>
					<td><?= $oVReconciliacao->getTipoDescricao()?></td>
					<td><?= $oVReconciliacao->getDataFormatado()?></td>
					<td><?= $oVReconciliacao->getEmpCodigo()?></td>
					<td><?= $oVReconciliacao->getMovTipo()?></td>
					<td><?= $oVReconciliacao->getMovimentoTipo()?></td>
					<td><?= $oVReconciliacao->getMovDocumento()?></td>
					<td><?= $oVReconciliacao->getConCodigo()?></td>
					<td><?= $oVReconciliacao->getContaDescricao()?></td>
					<td><?= $oVReconciliacao->getUniCodigo()?></td>
					<td><?= $oVReconciliacao->getUnidadeDescricao()?></td>
					<td><?= $oVReconciliacao->getCenCodigo()?></td>
					<td><?= $oVReconciliacao->getCentroDescricao()?></td>
					<td><?= $oVReconciliacao->getNegCodigo()?></td>
					<td><?= $oVReconciliacao->getUnidadeNegocioDescricao()?></td>
					<td><?= $oVReconciliacao->getCcrConta()?></td>
					<td><?= $oVReconciliacao->getEmpresa()?></td>
					<td><?= $oVReconciliacao->getMovObs()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voVReconciliacao)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('VReconciliacao');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
