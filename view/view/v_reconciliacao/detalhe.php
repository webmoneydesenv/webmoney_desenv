﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oVReconciliacao = $_REQUEST['oVReconciliacao'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VReconciliacao.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Fa:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getFa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getMovValorPagar() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Forma de Pagamento:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getFpgCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getFormaDePagamento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Documento:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getTipDocCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getTipoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getData() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getEmpCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Tipo Movimento:

0 - A Pagar

1 - A Receber

:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getMovTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getMovimentoTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Documento:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getMovDocumento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Con_codigo:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getConCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getContaDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Unidade de Negócios:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getUniCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getUnidadeDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Centro de Custo:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getCenCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getCentroDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Centro de Negócios:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getNegCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getUnidadeNegocioDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Conta:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getCcrConta() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Razão Social:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getEmpresa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oVReconciliacao) ? $oVReconciliacao->getMovObs() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VReconciliacao.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
