<?
	$voMnyTipoUnidade = $_REQUEST['voMnyTipoUnidade'];
	$voSysEmpresa = $_REQUEST['voSysEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Relatorios</title>
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
<body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyMovimento.preparaLista">Consultas</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Consultas</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" class="formulario" >
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Pesquisa">
 			<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="">Pesquisar por periodo:</label>
				</div>
				<br>
                </br>
            <div class="form-group">
	            <label class="col-sm-2 control-label" id="" style="text-align:left" for="Reconciliacao">Reconcilia&ccedil;&atilde;o</label>
            	<div class="col-sm-2">
					<input type="radio" id="rec" name="fTipoRelatorio" value="reconciliacao" onClick="MostraEscondeConta(1)">
                </div>
                <label class="col-sm-2 control-label" id="" style="text-align:left" for="ExtratoConciliacao" >Extrato Concilia&ccedil;&atilde;o</label>
            	<div class="col-sm-2">
					<input type="radio" id="ext" name="fTipoRelatorio" value="extrato" onClick="MostraEscondeConta(2)">
                </div>
            </div>
            <br><br>
           	<div class="form-group" id="divExtrato" style="display:block" >
	            <label class="col-sm-1 control-label" style="text-align:left" for="DataInicial">Data:</label>
            	<div class="col-sm-2">
                  <input class="form-control" type='text' id='data' placeholder='Data Inicial' name='fDataInicial'  />
                </div>
	            <label class="col-sm-1 control-label"  style="text-align:left" for="DataInicial">Unidade:</label>
              <div class="col-sm-3">
                                    <select name='fCodUnidade' id="CodUnidade" onChange="recuperaContaCorrente(this.value)" class="form-control chosen"  required  <?=$sDisabled?> >
                                        <option value=''>Selecione</option>
                                        <? $sSelected = "";
                                           if($voMnyTipoUnidade){
                                               foreach($voMnyTipoUnidade as $oMnyTipoUnidade){
                                                   if($_REQUEST['nCodUnidade']){
                                                       $sSelected = ($_REQUEST['nCodUnidade'] == $oMnyTipoUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                                   }
                                        ?>
                                                   <option  <?= $sSelected?> value='<?= $oMnyTipoUnidade->getPlanoContasCodigo()?>'><?= $oMnyTipoUnidade->getDescricao()?></option>
                                        <?
                                               }
                                           }
                                        ?>
                                    </select>
                              </div>
	            <label class="col-sm-1 control-label" style="text-align:left" for="ContaCorrente">Conta:</label>
               		<div id="divConta">
                         <div class="col-sm-4">
                            <input class="form-control" id="conta" type='text' placeholder='Conta Corrente' name='fDataInicial'  />
                        </div>
                    </div>
            </div>
            <br><br><br>
            <div class="form-group" id="divReconciliacao" style="display:block" >
                    <label class="col-sm-1 control-label" id="" style="text-align:left" for="Competencia">Comp:</label>
                    <div class="col-sm-2">
                      <input class="form-control" type='text' id='comp' placeholder='Competencia' name='fCompetencia'  />
                    </div>
                    <label class="col-sm-1 control-label" id="" style="text-align:left" for="Empresa">Empresa:</label>
                 	<div class="col-sm-5">
                        <select name='fEmpresa' class="form-control chosen"  >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voSysEmpresa){
                                   foreach($voSysEmpresa as $oSysEmpresa){ ?>
                                       <option  <?= $sSelected?> value='<?= $oSysEmpresa->getEmpCodigo()?>'><?= $oSysEmpresa->getEmpRazaoSocial()?></option>
                            <?
                                   }
                               }
                            ?>
                        </select>
                     </div>
            </div>
            <br><br><br>
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" onClick="geraRelatorio()">Gerar</button>
     	</div><br>
		<div class="">&nbsp;</div>
   		</div>
       </form>
       <hr>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
        <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
        <script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
        <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
        <script src="js/jquery/plugins/dataTables.colVis.js"></script>
        <script src="js/modal-movimento-item.js"></script>
        <script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript" charset="utf-8">

		 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

		  jQuery(function($){
  			   $("#comp").mask("99/9999");
			    $("#DataFinal").mask("99/99/9999");
			    $("#data").mask("99/99/9999");
				$("#MovDataVencto").mask("99/99/9999");
				$("#MovDataPrev").mask("99/99/9999");
  			 });

         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });

		function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
            	jQuery(function($){
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataPrev").mask("99/99/9999");
					$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				  });
				 $(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
					  numFiles = input.get(0).files ? input.get(0).files.length : 1,
					  label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});
				$(document).ready( function() {
					$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
						var input = $(this).parents('.input-group').find(':text'),
							log = numFiles > 1 ? numFiles + ' files selected' : label;
						if( input.length ) {
							input.val(log);
						} else {
							if( log ) alert(log);
						}
					});
				});
    		}
	});
}


	function loadValue(){
			var dMesAno = document.getElementById('Competencia').value;
			window.open("http://10.88.3.7:88/webmoney/?action=MnyConciliacao.preparaReconciliacao&dMesAno="+dMesAnor);
	}
   function recuperaContaCorrente(nUniCodigo){
	   recuperaConteudoDinamico('index.php','action=VReconciliacao.recuperaContaCorrente&nUniCodigo='+nUniCodigo,'divConta')
   }

   function MostraEscondeConta(nParamentro){
	   	if(nParamentro == 1){
			document.getElementById('divReconciliacao').style.display = "block";
			document.getElementById('divExtrato').style.display = "none";
			//document.getElementById('conta').disabled = true;
		}else{
			document.getElementById('divReconciliacao').style.display = "none";
			document.getElementById('divExtrato').style.display = "block";
		}
   }

   function geraRelatorio(){
		if(document.getElementById('rec').checked == true){

		}
		if(document.getElementById('ext').checked == true){
			nCodUnidade = document.getElementById('CodUnidade').value;
			nCodConta = document.getElementById('CodContaCorrenteAjax').value;
			dData = document.getElementById('data').value;

			window.open("http://200.98.201.88/webmoney/relatorios/?rel=extrato_conciliacao&cod_unidade=" + nCodUnidade + "&conta=" + nCodConta + "&data= "+dData);
		}
   }
</script>
           <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>

 <div class="modal fade" id="Objeto" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape" >
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
          </div>
    </div>
</div>

