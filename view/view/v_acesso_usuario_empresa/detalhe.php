<?php
 $sOP = $_REQUEST['sOP'];
 $oVAcessoUsuarioEmpresa = $_REQUEST['oVAcessoUsuarioEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VAcessoUsuarioEmpresa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Cod_usuario:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodUsuario() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Emp_codigo:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getEmpCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Cod_grupo_usuario:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodGrupoUsuario() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Cod_plano_contas:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodPlanoContas() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome Fantasia:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getEmpFantasia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getUnidade() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descrição:&nbsp;</strong><?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getPerfil() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VAcessoUsuarioEmpresa.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
