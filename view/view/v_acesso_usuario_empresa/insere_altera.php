<?php
 $sOP = $_REQUEST['sOP'];
 $oVAcessoUsuarioEmpresa = $_REQUEST['oVAcessoUsuarioEmpresa'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VAcessoUsuarioEmpresa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVAcessoUsuarioEmpresa" action="?action=VAcessoUsuarioEmpresa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodUsuario">Cod_usuario:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodUsuario' placeholder='Cod_usuario' name='fCodUsuario'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Emp_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpCodigo' placeholder='Emp_codigo' name='fEmpCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodGrupoUsuario">Cod_grupo_usuario:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodGrupoUsuario' placeholder='Cod_grupo_usuario' name='fCodGrupoUsuario'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodGrupoUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodPlanoContas">Cod_plano_contas:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodPlanoContas' placeholder='Cod_plano_contas' name='fCodPlanoContas'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getCodPlanoContas() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'  required   value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFantasia">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpFantasia' placeholder='Nome Fantasia' name='fEmpFantasia'  required   value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getEmpFantasia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Unidade">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Unidade' placeholder='Descricao' name='fUnidade'  required   value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getUnidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Perfil">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Perfil' placeholder='Descrição' name='fPerfil'   value='<?= ($oVAcessoUsuarioEmpresa) ? $oVAcessoUsuarioEmpresa->getPerfil() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
