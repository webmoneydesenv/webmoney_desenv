﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oVMovimentoContrato = $_REQUEST['oVMovimentoContrato'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contrato/Movimento- <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VMovimentoContrato.preparaLista">Gerenciar Contrato/Movimentos</a> &gt; <strong><?php echo $sOP?> Contrato/Movimento</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Contrato/Movimento</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">
           <div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais</legend>
               <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data de Emiss&atilde;o:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDataEmissaoFormatado() : ""?></div>
				</div>
				<br>
                <br>
                 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pessoa:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getPesCodigo() : ""?></div>
				</div>
				<br>
                <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Documento:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDocumento() : ""?></div>
				</div>
				<br>
                 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Global:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovValorGlobFormatado() : ""?></div>
				</div>
				<br>
 			</div>
           </div>

    <div class="form-group">
        <div class="col-sm-6">
          <legend>Apropria&ccedil;&otilde;es</legend>
             <div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>Custo:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCusCodigo() : ""?></div>
            </div>
            <br>
            <br>
             <div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>C.Negocio:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getNegocioDescricao() : ""?></div>
            </div>
            <br>
             <div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>C.Custo:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCenCodigo() : ""?></div>
            </div>
            <br>
			<div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getUniCodigo() : ""?></div>
            </div>
            <br>
 			<div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>Conta:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getConCodigo() : ""?></div>
            </div>
            <br>
			 <div class="form-group">
                 <div class="col-sm-12" style="text-align:left"><strong>Setor:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getSetCodigo() : ""?></div>
            </div>
            <br>
        </div>
   </div>

  <div class="form-group">
        <div class="col-sm-12">
          <legend>Valores</legend>
           <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Outros desc:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovOutrosDesc() : ""?></div>
				</div>
				<br>
                 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIcmsAliq() : ""?></div>
				</div>
				<br>
                 <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>PIS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovPis() : ""?></div>
                </div>
                <br>
                 <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>COFINS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovConfins() : ""?></div>
                </div>
                <br>
                 <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>CSLL:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovCsll() : ""?></div>
                </div>
                <br>
                <div class="form-group">
                        <div class="col-sm-12" style="text-align:left"><strong>ISS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIss() : ""?></div>
                </div>
                <br>
                 <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>IR:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIr() : ""?></div>
                </div>
                <br>
                 <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>IRRF:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIrrf() : ""?></div>
                </div>
                <br>
                <div class="form-group">
                     <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovInss() : ""?></div>
                </div>
                <br>
                </div>
  </div>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tip_nome:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_email:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getPesgEmail() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pes_fones:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getPesFones() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipocon_cod:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipoconCod() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_bco_agencia:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getPesgBcoAgencia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_bco_conta:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getPesgBcoConta() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Bco_nome:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getBcoNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Identificacao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getIdentificacao() : ""?></div>
				</div>
				<br>



 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Competência:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getComCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovObs() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Incluído por:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovInc() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Alterado por:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovAlt() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Contrato:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovContrato() : ""?></div>
				</div>
				<br>



 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Parcelas:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovParcelas() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Tipo Movimento:

0 - A Pagar

1 - A Receber

:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getEmpCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_outros:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovOutros() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_devolucao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDevolucao() : ""?></div>
				</div>
				<br>
				</div>
				<br>



 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Código:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Item:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovItem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Aceite:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipAceCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Vencimento:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDataVencto() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Previsão:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDataPrev() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovValor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong> Juros:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovJuros() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pagar:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovValorPagar() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Forma de Pagamento:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getFpgCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Documento:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipDocCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Desconto:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovRetencao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Inclusão:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovDataInclusao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Pago:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovValorPago() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCustoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCustoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getNegocioCodigo() : ""?></div>
				</div>
				<br>



 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCentroCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCentroDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getUnidadeCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getUnidadeDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getAceiteCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getAceiteDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getSetorCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getSetorDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipoDocumentoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipoDocumentoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipoMovimentoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getTipoMovimentoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getFormaPagamentoCodigo() : ""?></div>
				</div>
				<br>
 <div class="form-group">
     <div class="col-sm-12" style="text-align:left"><strong>Compet&ecirc;ncia:</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getCompetencia() : ""?></div>
</div>
<br>


 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?//= ($oVMovimentoContrato) ? $oVMovimentoContrato->getFormaPagamentoDescricao() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VMovimentoContrato.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
