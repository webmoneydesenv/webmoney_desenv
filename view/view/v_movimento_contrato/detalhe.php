<?php

$sOP = $_REQUEST['sOP'];
$voVPesquisaMovimentoItem = $_REQUEST['voVPesquisaMovimentoItem'];
$oVMovimentoContrato = $_REQUEST['oVMovimentoContrato'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contrato/Movimento - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VMovimentoContrato.preparaLista">Gerenciar Contrato/Movimentos</a> &gt; <strong><?php echo $sOP?> Contrato/Movimento</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Contrato/Movimento</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <div id="formulario" class="TabelaAdministracao">
          <div class="form-group">
            <div class="col-sm-4">
              <legend>Dados Gerais</legend>
              <div class="form-group">
                <div class="col-sm-12" style="text-align:left"><strong>Pessoa: </strong><?=$oVMovimentoContrato->getNome()?></div>
              </div>
              <br>
              <br>
              <div class="form-group">
                    <div class="col-sm-12" style="text-align:left" ><strong>Emissão:</strong><?= $oVMovimentoContrato->getMovDataEmissaoFormatado();?></div>
              </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Histórico: </strong><?= $oVMovimentoContrato->getMovObs()?></div>
                </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Documento: </strong><?= $oVMovimentoContrato->getMovDocumento()?></div>
                </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Valor Global:&nbsp;</strong><?= $oVMovimentoContrato->getMovValorGlobFormatado()?></div>
                </div>
                <br>
                </div>
                <div class="col-sm-4">
                <legend>Apropriações</legend>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Custo:&nbsp;</strong><?= $oVMovimentoContrato->getCustoCodigo() . " - " . $oVMovimentoContrato->getCustoDescricao() ?></div>
         	    </div>
                <br><br>
         		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>C.Negocio:&nbsp;</strong><?= $oVMovimentoContrato->getNegocioCodigo() . " - " . $oVMovimentoContrato->getNegocioDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Centro de Custo:&nbsp;</strong><?= $oVMovimentoContrato->getCentroCodigo() . " - " . $oVMovimentoContrato->getCentroDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= $oVMovimentoContrato->getUnidadeCodigo() . " - " . $oVMovimentoContrato->getUnidadeDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Conta:&nbsp;</strong><?= $oVMovimentoContrato->getPesgBcoConta() . " - " . $oVMovimentoContrato->getBcoNome() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Setor:&nbsp;</strong><?= $oVMovimentoContrato->getSetorCodigo() . " - " . $oVMovimentoContrato->getSetorDescricao() ?></div>
         	    </div>
          		</div>
                <div class="col-sm-4">
                <legend>Tributos</legend>
                        <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Outros desc:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovOutrosDesc() : ""?></div>
                        </div>
                        <br>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIcmsAliq() : ""?></div>
                        </div>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>PIS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovPis() : ""?></div>
                        </div>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>COFINS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovConfins() : ""?></div>
                        </div>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>CSLL:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovCsll() : ""?></div>
                        </div>
                        <br>
                        <div class="form-group">
                                <div class="col-sm-12" style="text-align:left"><strong>ISS:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIss() : ""?></div>
                        </div>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>IR:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIr() : ""?></div>
                        </div>
                        <br>
                         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>IRRF:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovIrrf() : ""?></div>
                        </div>
                        <br>
                        <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVMovimentoContrato) ? $oVMovimentoContrato->getMovInss() : ""?></div>
                        </div>
                        <br>
          		</div>
  <br>
       <div class="col-sm-12">
        <legend>Item</legend>
   			<?php if(is_array($voVPesquisaMovimentoItem)){?>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   				<thead>
   				<tr>
					<th class='Titulo'>Tipo</th>
					<th class='Titulo'>Nome</th>
					<th class='Titulo'>CPF/CNPJ</th>
					<th class='Titulo'>Emissão</th>
					<th class='Titulo'>Documento</th>
					<th class='Titulo'>F.A</th>
					<th class='Titulo'>Vencimento</th>
					<th class='Titulo'>Compet&ecirc;ncia</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voVPesquisaMovimentoItem as $oVPesquisaMovimento){ ?>
   				<tr>
  					<td><?= ($oVPesquisaMovimento->getMovTipo() == 188)? "PAGAR" : "RECEBER"?></td>
                    <td><?= $oVPesquisaMovimento->getNome()?></td>
					<td><?= $oVPesquisaMovimento->getIdentificacao()?></td>
					<td><?= $oVPesquisaMovimento->getMovDataEmissaoFormatado()?></td>
					<td><?= $oVPesquisaMovimento->getMovDocumento()?></td>
					<td><?= $oVPesquisaMovimento->getMovCodigo()?>.<?= $oVPesquisaMovimento->getMovItem()?></td>
					<td><?= $oVPesquisaMovimento->getMovDataVenctoFormatado()?></td>
					<td><?= $oVPesquisaMovimento->getCompetencia()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			</table>
  			<?php }//if(count($voVPessoaFormatada)){?>
</div>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd -->
 <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 </html>
