<?php
 $sOP = $_REQUEST['sOP'];
 $oVMovimentoConciliacao = $_REQUEST['oVMovimentoConciliacao'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VMovimentoConciliacao.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVMovimentoConciliacao" action="?action=VMovimentoConciliacao.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodConciliacao">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodConciliacao' placeholder='Código' name='fCodConciliacao'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getCodConciliacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigo">Movimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCodigo' placeholder='Movimento' name='fMovCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getMovCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItem' placeholder='Item' name='fMovItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodUnidade">Unidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodUnidade' placeholder='Unidade' name='fCodUnidade'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getCodUnidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoCaixa">Tipo de Caixa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoCaixa' placeholder='Tipo de Caixa' name='fTipoCaixa'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getTipoCaixa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataConciliacao">Data_conciliacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DataConciliacao' placeholder='Data_conciliacao' name='fDataConciliacao'   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getDataConciliacaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'  required   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValor' placeholder='Valor' name='fMovValor'  required   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getMovValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesCodigo">Pessoa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesCodigo' placeholder='Pessoa' name='fPesCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getPesCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Historico">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Historico' placeholder='Observação' name='fHistorico'   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getHistorico() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaCodigo">Conta_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaCodigo' placeholder='Conta_codigo' name='fContaCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getContaCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Agencia">- Agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Agencia' placeholder='- Agencia' name='fAgencia'  required   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getAgencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Conta">- Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Conta' placeholder='- Conta' name='fConta'  required   value='<?= ($oVMovimentoConciliacao) ? $oVMovimentoConciliacao->getConta() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#DataConciliacao").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
