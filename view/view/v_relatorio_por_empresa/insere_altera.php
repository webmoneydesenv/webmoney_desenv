<?php
 $sOP = $_REQUEST['sOP'];
 $oVRelatorioPorEmpresa = $_REQUEST['oVRelatorioPorEmpresa'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VRelatorioPorEmpresa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVRelatorioPorEmpresa" action="?action=VRelatorioPorEmpresa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Fa">Fa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Fa' placeholder='Fa' name='fFa'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getFa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Credor">Credor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Credor' placeholder='Credor' name='fCredor'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getCredor() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Empresa">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Empresa' placeholder='Nome Fantasia' name='fEmpresa'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getEmpresa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Unidade">Unidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Unidade' placeholder='Unidade' name='fUnidade'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getUnidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DescricaoDespesa">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DescricaoDespesa' placeholder='Observação' name='fDescricaoDespesa'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getDescricaoDespesa() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Aporte">Aporte:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Aporte' placeholder='Aporte' name='fAporte'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getAporte() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Parcela">Parcela:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Parcela' placeholder='Parcela' name='fParcela'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getParcela() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Competencia">Competencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Competencia' placeholder='Competencia' name='fCompetencia'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getCompetencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Vencimento">Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Vencimento' placeholder='Vencimento' name='fVencimento'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getVencimento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaPagamento">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FormaPagamento' placeholder='Descricao' name='fFormaPagamento'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getFormaPagamento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPagar' placeholder='Pagar' name='fMovValorPagar'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovValorPagarFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Acrescimo">Acrescimo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Acrescimo' placeholder='Acrescimo' name='fAcrescimo'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getAcrescimoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovJuros"> Juros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovJuros' placeholder=' Juros' name='fMovJuros'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovJurosFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Desconto">Desconto:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Desconto' placeholder='Desconto' name='fDesconto'   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getDescontoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Reprogramado">Reprogramado:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Reprogramado' placeholder='Reprogramado' name='fReprogramado'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getReprogramado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Pago">Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Pago' placeholder='Pago' name='fPago'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getPago() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Pagar">Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Pagar' placeholder='Pagar' name='fPagar'  required   value='<?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getPagar() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#Acrescimo").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#Desconto").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
