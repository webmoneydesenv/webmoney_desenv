<?php
 $sOP = $_REQUEST['sOP'];
 $oVRelatorioPorEmpresa = $_REQUEST['oVRelatorioPorEmpresa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VRelatorioPorEmpresa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Fa:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getFa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Credor:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getCredor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome Fantasia:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getEmpresa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getUnidade() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getDescricaoDespesa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Aporte:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getAporte() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Documento:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovDocumento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Parcela:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getParcela() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Competencia:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getCompetencia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Vencimento:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getVencimento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getFormaPagamento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pagar:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovValorPagar() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Acrescimo:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getAcrescimo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong> Juros:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getMovJuros() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Desconto:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getDesconto() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Reprogramado:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getReprogramado() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pago:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getPago() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pagar:&nbsp;</strong><?= ($oVRelatorioPorEmpresa) ? $oVRelatorioPorEmpresa->getPagar() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VRelatorioPorEmpresa.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
