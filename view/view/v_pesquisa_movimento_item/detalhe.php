﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oVPesquisaMovimentoItem = $_REQUEST['oVPesquisaMovimentoItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VPesquisaMovimentoItem.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descrição:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCompetencia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Razão Social:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpRazaoSocial() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome Fantasia:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpFantasia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>CNPJ:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpCnpj() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Logo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpImagem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data de Emissão:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataEmissao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tip_nome:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_email:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgEmail() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pes_fones:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesFones() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipocon_cod:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoconCod() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_bco_agencia:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgBcoAgencia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_bco_conta:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgBcoConta() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Bco_nome:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getBcoNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Identificacao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getIdentificacao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Custo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCusCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Centro de Negócios:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Centro de Custo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCenCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Unidade de Negócios:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUniCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pessoa:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Con_codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getConCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Setor:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovObs() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Incluído por:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovInc() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Alterado por:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovAlt() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Contrato:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovContrato() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Documento:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDocumento() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Parcelas:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovParcelas() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Tipo Movimento:

0 - A Pagar

1 - A Receber

:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIcmsAliq() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorGlob() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>PIS:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovPis() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>COFINS:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovConfins() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>CSLL:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovCsll() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>ISS:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIss() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>IR:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIr() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>IRRF:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIrrf() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>ICMS Aliq:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovInss() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_outros:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovOutros() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_devolucao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDevolucao() : ""?></div>
				</div>
				<br>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_outros_desc:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovOutrosDesc() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Com_codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getComCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCustoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCustoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegocioCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegocioDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCentroCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCentroDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUnidadeCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUnidadeDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getAceiteCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getAceiteDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetorCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetorDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoDocumentoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoDocumentoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoMovimentoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoMovimentoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getContaCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getContaDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFormaPagamentoCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFormaPagamentoDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Código:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Item:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovItem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Vencimento:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataVencto() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Previsão:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataPrev() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong> Juros:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovJuros() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pagar:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorPagar() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Forma de Pagamento:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFpgCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Documento:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipDocCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Desconto:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovRetencao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Inclusão:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataInclusao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Pago:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorPago() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo de Aceite:&nbsp;</strong><?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipAceCodigo() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VPesquisaMovimentoItem.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
