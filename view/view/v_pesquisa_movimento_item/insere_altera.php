<?php
 $sOP = $_REQUEST['sOP'];
 $oVPesquisaMovimentoItem = $_REQUEST['oVPesquisaMovimentoItem'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VPesquisaMovimentoItem.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVPesquisaMovimentoItem" action="?action=VPesquisaMovimentoItem.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Competencia">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Competencia' placeholder='Descrição' name='fCompetencia'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCompetencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpRazaoSocial">Razão Social:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpRazaoSocial' placeholder='Razão Social' name='fEmpRazaoSocial'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpRazaoSocial() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFantasia">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpFantasia' placeholder='Nome Fantasia' name='fEmpFantasia'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpFantasia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCnpj">CNPJ:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpCnpj' placeholder='CNPJ' name='fEmpCnpj'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpCnpj() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpImagem">Logo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpImagem' placeholder='Logo' name='fEmpImagem'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpImagem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataEmissao">Data de Emissão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataEmissaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipNome">Tip_nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipNome' placeholder='Tip_nome' name='fTipNome'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEmail">Pesg_email:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgEmail' placeholder='Pesg_email' name='fPesgEmail'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgEmail() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesFones">Pes_fones:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesFones' placeholder='Pes_fones' name='fPesFones'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesFones() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoconCod">Tipocon_cod:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoconCod' placeholder='Tipocon_cod' name='fTipoconCod'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoconCod() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoAgencia">Pesg_bco_agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgBcoAgencia' placeholder='Pesg_bco_agencia' name='fPesgBcoAgencia'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgBcoAgencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoConta">Pesg_bco_conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgBcoConta' placeholder='Pesg_bco_conta' name='fPesgBcoConta'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesgBcoConta() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="BcoNome">Bco_nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='BcoNome' placeholder='Bco_nome' name='fBcoNome'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getBcoNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Identificacao">Identificacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Identificacao' placeholder='Identificacao' name='fIdentificacao'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getIdentificacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CusCodigo">Tipo de Custo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CusCodigo' placeholder='Tipo de Custo' name='fCusCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCusCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegCodigo">Centro de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='NegCodigo' placeholder='Centro de Negócios' name='fNegCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CenCodigo">Centro de Custo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CenCodigo' placeholder='Centro de Custo' name='fCenCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCenCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UniCodigo">Unidade de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UniCodigo' placeholder='Unidade de Negócios' name='fUniCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUniCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesCodigo">Pessoa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesCodigo' placeholder='Pessoa' name='fPesCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getPesCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ConCodigo">Con_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ConCodigo' placeholder='Con_codigo' name='fConCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getConCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetCodigo">Setor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='SetCodigo' placeholder='Setor' name='fSetCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovObs">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovObs' placeholder='Observação' name='fMovObs'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovObs() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInc">Incluído por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovInc' placeholder='Incluído por' name='fMovInc'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAlt">Alterado por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovAlt' placeholder='Alterado por' name='fMovAlt'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovAlt() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovContrato">Contrato:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovContrato' placeholder='Contrato' name='fMovContrato'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovContrato() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParcelas">Parcelas:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovParcelas' placeholder='Parcelas' name='fMovParcelas'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovParcelas() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovTipo">- Tipo Movimento:

0 - A Pagar

1 - A Receber

:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovTipo' placeholder='- Tipo Movimento:

0 - A Pagar

1 - A Receber

' name='fMovTipo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpCodigo' placeholder='Empresa' name='fEmpCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIcmsAliq">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIcmsAliq' placeholder='ICMS Aliq' name='fMovIcmsAliq'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIcmsAliqFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorGlob">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorGlob' placeholder='Valor' name='fMovValorGlob'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorGlobFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovPis' placeholder='PIS' name='fMovPis'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovPisFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">COFINS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovConfins' placeholder='COFINS' name='fMovConfins'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovConfinsFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCsll">CSLL:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCsll' placeholder='CSLL' name='fMovCsll'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovCsllFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIss' placeholder='ISS' name='fMovIss'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIssFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIr' placeholder='IR' name='fMovIr'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIrFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIrrf">IRRF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIrrf' placeholder='IRRF' name='fMovIrrf'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovIrrfFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovInss' placeholder='ICMS Aliq' name='fMovInss'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovInssFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovOutros">Mov_outros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovOutros' placeholder='Mov_outros' name='fMovOutros'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovOutrosFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDevolucao">Mov_devolucao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDevolucao' placeholder='Mov_devolucao' name='fMovDevolucao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDevolucaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getAtivo() : "1"?>'/>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovOutrosDesc">Mov_outros_desc:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovOutrosDesc' placeholder='Mov_outros_desc' name='fMovOutrosDesc'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovOutrosDescFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ComCodigo">Com_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ComCodigo' placeholder='Com_codigo' name='fComCodigo'  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getComCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CustoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CustoCodigo' placeholder='Codigo' name='fCustoCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCustoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CustoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CustoDescricao' placeholder='Descricao' name='fCustoDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCustoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegocioCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='NegocioCodigo' placeholder='Codigo' name='fNegocioCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegocioCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegocioDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='NegocioDescricao' placeholder='Descricao' name='fNegocioDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getNegocioDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CentroCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CentroCodigo' placeholder='Codigo' name='fCentroCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCentroCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CentroDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CentroDescricao' placeholder='Descricao' name='fCentroDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getCentroDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UnidadeCodigo' placeholder='Codigo' name='fUnidadeCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUnidadeCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='UnidadeDescricao' placeholder='Descricao' name='fUnidadeDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getUnidadeDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AceiteCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='AceiteCodigo' placeholder='Codigo' name='fAceiteCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getAceiteCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AceiteDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='AceiteDescricao' placeholder='Descricao' name='fAceiteDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getAceiteDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetorCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='SetorCodigo' placeholder='Codigo' name='fSetorCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetorCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetorDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='SetorDescricao' placeholder='Descricao' name='fSetorDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getSetorDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoDocumentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoDocumentoCodigo' placeholder='Codigo' name='fTipoDocumentoCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoDocumentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoDocumentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoDocumentoDescricao' placeholder='Descricao' name='fTipoDocumentoDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoDocumentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoMovimentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoMovimentoCodigo' placeholder='Codigo' name='fTipoMovimentoCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoMovimentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoMovimentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoMovimentoDescricao' placeholder='Descricao' name='fTipoMovimentoDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipoMovimentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaCodigo' placeholder='Codigo' name='fContaCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getContaCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaDescricao' placeholder='Descricao' name='fContaDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getContaDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaPagamentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FormaPagamentoCodigo' placeholder='Codigo' name='fFormaPagamentoCodigo'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFormaPagamentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaPagamentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FormaPagamentoDescricao' placeholder='Descricao' name='fFormaPagamentoDescricao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFormaPagamentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigo">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCodigo' placeholder='Código' name='fMovCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItem' placeholder='Item' name='fMovItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataVencto">Data Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataVencto' placeholder='Data Vencimento' name='fMovDataVencto'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataVenctoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataPrev">Data Previsão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataPrev' placeholder='Data Previsão' name='fMovDataPrev'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataPrevFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValor' placeholder='Valor' name='fMovValor'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovJuros"> Juros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovJuros' placeholder=' Juros' name='fMovJuros'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovJurosFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPagar' placeholder='Pagar' name='fMovValorPagar'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorPagarFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FpgCodigo">Forma de Pagamento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FpgCodigo' placeholder='Forma de Pagamento' name='fFpgCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getFpgCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipDocCodigo">Tipo de Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipDocCodigo' placeholder='Tipo de Documento' name='fTipDocCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipDocCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovRetencao">Desconto:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovRetencao' placeholder='Desconto' name='fMovRetencao'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovRetencaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">Data Inclusão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataInclusao' placeholder='Data Inclusão' name='fMovDataInclusao'  required   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovDataInclusaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPago">Valor Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPago' placeholder='Valor Pago' name='fMovValorPago'   value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getMovValorPagoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipAceCodigo">Tipo de Aceite:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipAceCodigo' placeholder='Tipo de Aceite' name='fTipAceCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisaMovimentoItem) ? $oVPesquisaMovimentoItem->getTipAceCodigo() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#MovDataEmissao").mask("99/99/9999");
			$("#MovIcmsAliq").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorGlob").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovPis").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovConfins").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovCsll").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIr").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIrrf").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovInss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovOutros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDevolucao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovOutrosDesc").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataVencto").mask("99/99/9999");
			$("#MovDataPrev").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
