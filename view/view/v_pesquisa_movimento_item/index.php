<?php
 $voVPesquisaMovimentoItem = $_REQUEST['voVPesquisaMovimentoItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De VIEWs</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=VPesquisaMovimentoItem.preparaLista">Gerenciar VIEWs</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar VIEWs</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formVPesquisaMovimentoItem" id="formVPesquisaMovimentoItem" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesVPesquisaMovimentoItem" onChange="JavaScript: submeteForm('VPesquisaMovimentoItem')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=VPesquisaMovimentoItem.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo VIEW</option>
   						<option value="?action=VPesquisaMovimentoItem.preparaFormulario&sOP=Alterar" lang="1">Alterar VIEW selecionado</option>
   						<option value="?action=VPesquisaMovimentoItem.preparaFormulario&sOP=Detalhar" lang="1">Detalhar VIEW selecionado</option>
   						<option value="?action=VPesquisaMovimentoItem.processaFormulario&sOP=Excluir" lang="2">Excluir VIEW(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voVPesquisaMovimentoItem)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('VPesquisaMovimentoItem')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Descrição</th>
					<th class='Titulo'>Razão social</th>
					<th class='Titulo'>Nome fantasia</th>
					<th class='Titulo'>Cnpj</th>
					<th class='Titulo'>Logo</th>
					<th class='Titulo'>Data de emissão</th>
					<th class='Titulo'>TipNome</th>
					<th class='Titulo'>PesgEmail</th>
					<th class='Titulo'>PesFones</th>
					<th class='Titulo'>TipoconCod</th>
					<th class='Titulo'>PesgBcoAgencia</th>
					<th class='Titulo'>PesgBcoConta</th>
					<th class='Titulo'>BcoNome</th>
					<th class='Titulo'>Nome</th>
					<th class='Titulo'>Identificacao</th>
					<th class='Titulo'>Tipo de custo</th>
					<th class='Titulo'>Centro de negócios</th>
					<th class='Titulo'>Centro de custo</th>
					<th class='Titulo'>Unidade de negócios</th>
					<th class='Titulo'>Pessoa</th>
					<th class='Titulo'>ConCodigo</th>
					<th class='Titulo'>Setor</th>
					<th class='Titulo'>Observação</th>
					<th class='Titulo'>Incluído por</th>
					<th class='Titulo'>Alterado por</th>
					<th class='Titulo'>Contrato</th>
					<th class='Titulo'>Documento</th>
					<th class='Titulo'>Parcelas</th>
					<th class='Titulo'>- tipo movimento:

0 - a pagar

1 - a receber

</th>
					<th class='Titulo'>Empresa</th>
					<th class='Titulo'>Icms aliq</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Pis</th>
					<th class='Titulo'>Cofins</th>
					<th class='Titulo'>Csll</th>
					<th class='Titulo'>Iss</th>
					<th class='Titulo'>Ir</th>
					<th class='Titulo'>Irrf</th>
					<th class='Titulo'>Icms aliq</th>
					<th class='Titulo'>MovOutros</th>
					<th class='Titulo'>MovDevolucao</th>
					<th class='Titulo'>MovOutrosDesc</th>
					<th class='Titulo'>ComCodigo</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>Código</th>
					<th class='Titulo'>Item</th>
					<th class='Titulo'>Data vencimento</th>
					<th class='Titulo'>Data previsão</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'> juros</th>
					<th class='Titulo'>Pagar</th>
					<th class='Titulo'>Forma de pagamento</th>
					<th class='Titulo'>Tipo de documento</th>
					<th class='Titulo'>Desconto</th>
					<th class='Titulo'>Data inclusão</th>
					<th class='Titulo'>Valor pago</th>
					<th class='Titulo'>Tipo de aceite</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voVPesquisaMovimentoItem as $oVPesquisaMovimentoItem){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('VPesquisaMovimentoItem')" type="checkbox" value="" name="fIdVPesquisaMovimentoItem[]"/></td>
  					<td><?= $oVPesquisaMovimentoItem->getCompetencia()?></td>
					<td><?= $oVPesquisaMovimentoItem->getEmpRazaoSocial()?></td>
					<td><?= $oVPesquisaMovimentoItem->getEmpFantasia()?></td>
					<td><?= $oVPesquisaMovimentoItem->getEmpCnpj()?></td>
					<td><?= $oVPesquisaMovimentoItem->getEmpImagem()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDataEmissaoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipNome()?></td>
					<td><?= $oVPesquisaMovimentoItem->getPesgEmail()?></td>
					<td><?= $oVPesquisaMovimentoItem->getPesFones()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipoconCod()?></td>
					<td><?= $oVPesquisaMovimentoItem->getPesgBcoAgencia()?></td>
					<td><?= $oVPesquisaMovimentoItem->getPesgBcoConta()?></td>
					<td><?= $oVPesquisaMovimentoItem->getBcoNome()?></td>
					<td><?= $oVPesquisaMovimentoItem->getNome()?></td>
					<td><?= $oVPesquisaMovimentoItem->getIdentificacao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCusCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getNegCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCenCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getUniCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getPesCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getConCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getSetCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovObs()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovInc()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovAlt()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovContrato()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDocumento()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovParcelas()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovTipo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getEmpCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovIcmsAliqFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovValorGlobFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovPisFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovConfinsFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovCsllFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovIssFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovIrFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovIrrfFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovInssFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovOutrosFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDevolucaoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovOutrosDescFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getComCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCustoCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCustoDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getNegocioCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getNegocioDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCentroCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getCentroDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getUnidadeCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getUnidadeDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getAceiteCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getAceiteDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getSetorCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getSetorDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipoDocumentoCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipoDocumentoDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipoMovimentoCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipoMovimentoDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getContaCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getContaDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getFormaPagamentoCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getFormaPagamentoDescricao()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovItem()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDataVenctoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDataPrevFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovValorFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovJurosFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovValorPagarFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getFpgCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipDocCodigo()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovRetencaoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovDataInclusaoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getMovValorPagoFormatado()?></td>
					<td><?= $oVPesquisaMovimentoItem->getTipAceCodigo()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voVPesquisaMovimentoItem)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('VPesquisaMovimentoItem');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
