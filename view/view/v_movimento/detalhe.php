﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oVMovimento = $_REQUEST['oVMovimento'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VMovimento.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVMovimento" action="?action=VMovimento.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VMovimento" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Competencia">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descrição' name='fCompetencia' value='<?= ($oVMovimento) ? $oVMovimento->getCompetencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpRazaoSocial">Razão Social:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Razão Social' name='fEmpRazaoSocial' value='<?= ($oVMovimento) ? $oVMovimento->getEmpRazaoSocial() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFantasia">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome Fantasia' name='fEmpFantasia' value='<?= ($oVMovimento) ? $oVMovimento->getEmpFantasia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCnpj">CNPJ:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CNPJ' name='fEmpCnpj' value='<?= ($oVMovimento) ? $oVMovimento->getEmpCnpj() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpImagem">Logo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Logo' name='fEmpImagem' value='<?= ($oVMovimento) ? $oVMovimento->getEmpImagem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndLogra">Endereço:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Endereço' name='fEmpEndLogra' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndLogra() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndBairro">Bairro:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Bairro' name='fEmpEndBairro' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndBairro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndCidade">Cidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Cidade' name='fEmpEndCidade' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndCidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndCep">CEP:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CEP' name='fEmpEndCep' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndCep() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndUf">UF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='UF' name='fEmpEndUf' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndUf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEndFone">Fone:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Fone' name='fEmpEndFone' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEndFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpSite">Site:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Site' name='fEmpSite' value='<?= ($oVMovimento) ? $oVMovimento->getEmpSite() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFtp">FTP:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='FTP' name='fEmpFtp' value='<?= ($oVMovimento) ? $oVMovimento->getEmpFtp() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpEmail">email:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='email' name='fEmpEmail' value='<?= ($oVMovimento) ? $oVMovimento->getEmpEmail() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCustos">Custos:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Custos' name='fEmpCustos' value='<?= ($oVMovimento) ? $oVMovimento->getEmpCustos() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpFinanceiro">Financeiro:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Financeiro' name='fEmpFinanceiro' value='<?= ($oVMovimento) ? $oVMovimento->getEmpFinanceiro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpDiretor">Diretor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Diretor' name='fEmpDiretor' value='<?= ($oVMovimento) ? $oVMovimento->getEmpDiretor() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpAceite">Aceite:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Aceite' name='fEmpAceite' value='<?= ($oVMovimento) ? $oVMovimento->getEmpAceite() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataEmissao">Data de Emissão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data de Emissão' name='fMovDataEmissao' value='<?= ($oVMovimento) ? $oVMovimento->getMovDataEmissao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipNome">Tip_nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tip_nome' name='fTipNome' value='<?= ($oVMovimento) ? $oVMovimento->getTipNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEmail">Pesg_email:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pesg_email' name='fPesgEmail' value='<?= ($oVMovimento) ? $oVMovimento->getPesgEmail() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesFones">Pes_fones:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pes_fones' name='fPesFones' value='<?= ($oVMovimento) ? $oVMovimento->getPesFones() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoconCod">Tipocon_cod:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tipocon_cod' name='fTipoconCod' value='<?= ($oVMovimento) ? $oVMovimento->getTipoconCod() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoAgencia">Pesg_bco_agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pesg_bco_agencia' name='fPesgBcoAgencia' value='<?= ($oVMovimento) ? $oVMovimento->getPesgBcoAgencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoConta">Pesg_bco_conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pesg_bco_conta' name='fPesgBcoConta' value='<?= ($oVMovimento) ? $oVMovimento->getPesgBcoConta() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="BcoNome">Bco_nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Bco_nome' name='fBcoNome' value='<?= ($oVMovimento) ? $oVMovimento->getBcoNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome' name='fNome' value='<?= ($oVMovimento) ? $oVMovimento->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Identificacao">Identificacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Identificacao' name='fIdentificacao' value='<?= ($oVMovimento) ? $oVMovimento->getIdentificacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CusCodigo">Tipo de Custo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tipo de Custo' name='fCusCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getCusCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegCodigo">Centro de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Centro de Negócios' name='fNegCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getNegCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CenCodigo">Centro de Custo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Centro de Custo' name='fCenCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getCenCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UniCodigo">Unidade de Negócios:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Unidade de Negócios' name='fUniCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getUniCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesCodigo">Pessoa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pessoa' name='fPesCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getPesCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ConCodigo">Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Conta' name='fConCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getConCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetCodigo">Setor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Setor' name='fSetCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getSetCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ComCodigo">Competência:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Competência' name='fComCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getComCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovObs">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Observação' name='fMovObs' value='<?= ($oVMovimento) ? $oVMovimento->getMovObs() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInc">Incluído por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Incluído por' name='fMovInc' value='<?= ($oVMovimento) ? $oVMovimento->getMovInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAlt">Alterado por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Alterado por' name='fMovAlt' value='<?= ($oVMovimento) ? $oVMovimento->getMovAlt() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovContrato">Contrato:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Contrato' name='fMovContrato' value='<?= ($oVMovimento) ? $oVMovimento->getMovContrato() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipAceCodigo">Tipo de Aceite:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tipo de Aceite' name='fTipAceCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getTipAceCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Documento' name='fMovDocumento' value='<?= ($oVMovimento) ? $oVMovimento->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParcelas">Parcelas:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Parcelas' name='fMovParcelas' value='<?= ($oVMovimento) ? $oVMovimento->getMovParcelas() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovTipo">- Tipo Movimento:

0 - A Pagar

1 - A Receber

:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Tipo Movimento:

0 - A Pagar

1 - A Receber

' name='fMovTipo' value='<?= ($oVMovimento) ? $oVMovimento->getMovTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Empresa' name='fEmpCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIcmsAliq">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ICMS Aliq' name='fMovIcmsAliq' value='<?= ($oVMovimento) ? $oVMovimento->getMovIcmsAliq() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorGlob">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Valor' name='fMovValorGlob' value='<?= ($oVMovimento) ? $oVMovimento->getMovValorGlob() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='PIS' name='fMovPis' value='<?= ($oVMovimento) ? $oVMovimento->getMovPis() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">COFINS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='COFINS' name='fMovConfins' value='<?= ($oVMovimento) ? $oVMovimento->getMovConfins() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCsll">CSLL:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CSLL' name='fMovCsll' value='<?= ($oVMovimento) ? $oVMovimento->getMovCsll() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ISS' name='fMovIss' value='<?= ($oVMovimento) ? $oVMovimento->getMovIss() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IR' name='fMovIr' value='<?= ($oVMovimento) ? $oVMovimento->getMovIr() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIrrf">IRRF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IRRF' name='fMovIrrf' value='<?= ($oVMovimento) ? $oVMovimento->getMovIrrf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ICMS Aliq' name='fMovInss' value='<?= ($oVMovimento) ? $oVMovimento->getMovInss() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovOutros">Mov_outros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Mov_outros' name='fMovOutros' value='<?= ($oVMovimento) ? $oVMovimento->getMovOutros() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDevolucao">Mov_devolucao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Mov_devolucao' name='fMovDevolucao' value='<?= ($oVMovimento) ? $oVMovimento->getMovDevolucao() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oVMovimento) ? $oVMovimento->getAtivo() : "1"?>'/>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovOutrosDesc">Mov_outros_desc:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Mov_outros_desc' name='fMovOutrosDesc' value='<?= ($oVMovimento) ? $oVMovimento->getMovOutrosDesc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigo">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Código' name='fMovCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getMovCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Item' name='fMovItem' value='<?= ($oVMovimento) ? $oVMovimento->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataVencto">Data Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data Vencimento' name='fMovDataVencto' value='<?= ($oVMovimento) ? $oVMovimento->getMovDataVencto() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataPrev">Data Previsão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data Previsão' name='fMovDataPrev' value='<?= ($oVMovimento) ? $oVMovimento->getMovDataPrev() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Valor' name='fMovValor' value='<?= ($oVMovimento) ? $oVMovimento->getMovValor() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovJuros"> Juros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder=' Juros' name='fMovJuros' value='<?= ($oVMovimento) ? $oVMovimento->getMovJuros() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pagar' name='fMovValorPagar' value='<?= ($oVMovimento) ? $oVMovimento->getMovValorPagar() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FpgCodigo">Forma de Pagamento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Forma de Pagamento' name='fFpgCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getFpgCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipDocCodigo">Tipo de Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Tipo de Documento' name='fTipDocCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getTipDocCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovRetencao">Desconto:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Desconto' name='fMovRetencao' value='<?= ($oVMovimento) ? $oVMovimento->getMovRetencao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">Data Inclusão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data Inclusão' name='fMovDataInclusao' value='<?= ($oVMovimento) ? $oVMovimento->getMovDataInclusao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPago">Valor Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Valor Pago' name='fMovValorPago' value='<?= ($oVMovimento) ? $oVMovimento->getMovValorPago() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CustoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fCustoCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getCustoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CustoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fCustoDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getCustoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegocioCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fNegocioCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getNegocioCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="NegocioDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fNegocioDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getNegocioDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CentroCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fCentroCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getCentroCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CentroDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fCentroDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getCentroDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fUnidadeCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getUnidadeCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UnidadeDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fUnidadeDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getUnidadeDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AceiteCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fAceiteCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getAceiteCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AceiteDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fAceiteDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getAceiteDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetorCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fSetorCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getSetorCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SetorDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fSetorDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getSetorDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoDocumentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fTipoDocumentoCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getTipoDocumentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoDocumentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fTipoDocumentoDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getTipoDocumentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoMovimentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fTipoMovimentoCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getTipoMovimentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoMovimentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fTipoMovimentoDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getTipoMovimentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaPagamentoCodigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fFormaPagamentoCodigo' value='<?= ($oVMovimento) ? $oVMovimento->getFormaPagamentoCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FormaPagamentoDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fFormaPagamentoDescricao' value='<?= ($oVMovimento) ? $oVMovimento->getFormaPagamentoDescricao() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VMovimento.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
