<?php
 $sOP = $_REQUEST['sOP'];
 $oVTransferencia = $_REQUEST['oVTransferencia'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VTransferencia.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVTransferencia" action="?action=VTransferencia.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataEmissao">Data de Emissão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getMovDataEmissaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodTransferencia">Cod_transferencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodTransferencia' placeholder='Cod_transferencia' name='fCodTransferencia'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getCodTransferencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigoOrigem">Mov_codigo_origem:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCodigoOrigem' placeholder='Mov_codigo_origem' name='fMovCodigoOrigem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getMovCodigoOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItemOrigem">Mov_item_origem:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItemOrigem' placeholder='Mov_item_origem' name='fMovItemOrigem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getMovItemOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Origem">De:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Origem' placeholder='De' name='fOrigem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaOrigem">- Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaOrigem' placeholder='- Conta' name='fContaOrigem'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getContaOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AgenciaOrigem">Agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='AgenciaOrigem' placeholder='Agencia' name='fAgenciaOrigem'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getAgenciaOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="BancoNomeOrigem">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='BancoNomeOrigem' placeholder='Nome' name='fBancoNomeOrigem'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getBancoNomeOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodEmpresaOrigem">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodEmpresaOrigem' placeholder='Empresa' name='fCodEmpresaOrigem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getCodEmpresaOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpresaOrigem">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpresaOrigem' placeholder='Nome Fantasia' name='fEmpresaOrigem'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getEmpresaOrigem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigoDestino">Mov_codigo_destino:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCodigoDestino' placeholder='Mov_codigo_destino' name='fMovCodigoDestino'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getMovCodigoDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItemDestino">Mov_item_destino:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItemDestino' placeholder='Mov_item_destino' name='fMovItemDestino'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getMovItemDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Destino">De:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Destino' placeholder='De' name='fDestino'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ContaDestino">- Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ContaDestino' placeholder='- Conta' name='fContaDestino'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getContaDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="AgenciaDestino">Agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='AgenciaDestino' placeholder='Agencia' name='fAgenciaDestino'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getAgenciaDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="BancoNomeDestino">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='BancoNomeDestino' placeholder='Nome' name='fBancoNomeDestino'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getBancoNomeDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodEmpresaDestino">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodEmpresaDestino' placeholder='Empresa' name='fCodEmpresaDestino'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVTransferencia) ? $oVTransferencia->getCodEmpresaDestino() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpresaDestino">Nome Fantasia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='EmpresaDestino' placeholder='Nome Fantasia' name='fEmpresaDestino'  required   value='<?= ($oVTransferencia) ? $oVTransferencia->getEmpresaDestino() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#MovDataEmissao").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
