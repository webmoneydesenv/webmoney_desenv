<?php
 $sOP = $_REQUEST['sOP'];
 $oVSupriTramitacaoScm = $_REQUEST['oVSupriTramitacaoScm'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VSupriTramitacaoScm.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVSupriTramitacaoScm" action="?action=VSupriTramitacaoScm.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodProtocoloScm">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodProtocoloScm' placeholder='Código' name='fCodProtocoloScm'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getCodProtocoloScm() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodScm">Solicitacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodScm' placeholder='Solicitacao' name='fCodScm'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getCodScm() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TramDescricao">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TramDescricao' placeholder='Descrição' name='fTramDescricao'   value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getTramDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Usuario">Usuario:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Usuario' placeholder='Usuario' name='fUsuario'   value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getUsuario() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Data">Data:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Data' placeholder='Data' name='fData'  required   value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getDataFormatado() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getAtivo() : "1"?>'/>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodDe">Grupo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodDe' placeholder='Grupo' name='fCodDe'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getCodDe() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="De">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='De' placeholder='Descrição' name='fDe'   value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getDe() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodPara">Grupo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodPara' placeholder='Grupo' name='fCodPara'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getCodPara() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Para">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Para' placeholder='Descrição' name='fPara'   value='<?= ($oVSupriTramitacaoScm) ? $oVSupriTramitacaoScm->getPara() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#Data").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
