<?php
 $voVSupriTramitacaoScm = $_REQUEST['voVSupriTramitacaoScm'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>LISTA DE VIEWs</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=VSupriTramitacaoScm.preparaLista">Gerenciar VIEWs</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar VIEWs</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formVSupriTramitacaoScm" id="formVSupriTramitacaoScm" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes" name="acoes" id="acoesVSupriTramitacaoScm" onChange="JavaScript: submeteForm('VSupriTramitacaoScm')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=VSupriTramitacaoScm.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo VIEW</option>
   						<option value="?action=VSupriTramitacaoScm.preparaFormulario&sOP=Alterar" lang="1">Alterar VIEW selecionado</option>
   						<option value="?action=VSupriTramitacaoScm.preparaFormulario&sOP=Detalhar" lang="1">Detalhar VIEW selecionado</option>
   						<option value="?action=VSupriTramitacaoScm.processaFormulario&sOP=Excluir" lang="2">Excluir VIEW(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voVSupriTramitacaoScm)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('VSupriTramitacaoScm')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>C�digo</th>
					<th class='Titulo'>Solicitacao</th>
					<th class='Titulo'>Descri��o</th>
					<th class='Titulo'>Usuario</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Grupo</th>
					<th class='Titulo'>Descri��o</th>
					<th class='Titulo'>Grupo</th>
					<th class='Titulo'>Descri��o</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voVSupriTramitacaoScm as $oVSupriTramitacaoScm){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('VSupriTramitacaoScm')" type="checkbox" value="" name="fIdVSupriTramitacaoScm[]"/></td>
  					<td><?= $oVSupriTramitacaoScm->getCodProtocoloScm()?></td>
					<td><?= $oVSupriTramitacaoScm->getCodScm()?></td>
					<td><?= $oVSupriTramitacaoScm->getTramDescricao()?></td>
					<td><?= $oVSupriTramitacaoScm->getUsuario()?></td>
					<td><?= $oVSupriTramitacaoScm->getDataFormatado()?></td>
					<td><?= $oVSupriTramitacaoScm->getCodDe()?></td>
					<td><?= $oVSupriTramitacaoScm->getDe()?></td>
					<td><?= $oVSupriTramitacaoScm->getCodPara()?></td>
					<td><?= $oVSupriTramitacaoScm->getPara()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voVSupriTramitacaoScm)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('VSupriTramitacaoScm');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
