<?php
 $sOP = $_REQUEST['sOP'];
 $oVPesquisa = $_REQUEST['oVPesquisa'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VPesquisa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formVPesquisa" action="?action=VPesquisa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do VIEW">


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataVencto">Data Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataVencto' placeholder='Data Vencimento' name='fMovDataVencto'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovDataVenctoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Tipo">Tipo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Tipo' placeholder='Tipo' name='fTipo'   value='<?= ($oVPesquisa) ? $oVPesquisa->getTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovContrato">Mov_contrato:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovContrato' placeholder='Mov_contrato' name='fMovContrato'   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovContrato() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCodigo">Código:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCodigo' placeholder='Código' name='fMovCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisa) ? $oVPesquisa->getMovCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItem' placeholder='Item' name='fMovItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisa) ? $oVPesquisa->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Competencia">Competencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Competencia' placeholder='Competencia' name='fCompetencia'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getCompetenciaFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValor' placeholder='Valor' name='fMovValor'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovTipo">- Tipo Movimento:

0 - A Pagar

1 - A Receber

:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovTipo' placeholder='- Tipo Movimento:

0 - A Pagar

1 - A Receber

' name='fMovTipo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisa) ? $oVPesquisa->getMovTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Descricao' placeholder='Descricao' name='fDescricao'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipNome">Tip_nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipNome' placeholder='Tip_nome' name='fTipNome'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getTipNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgCodigo">Pesg_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgCodigo' placeholder='Pesg_codigo' name='fPesgCodigo'  required  onKeyPress="TodosNumero(event);" value='<?= ($oVPesquisa) ? $oVPesquisa->getPesgCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Identificacao">Identificacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Identificacao' placeholder='Identificacao' name='fIdentificacao'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getIdentificacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">Data Inclusão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataInclusao' placeholder='Data Inclusão' name='fMovDataInclusao'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovDataInclusaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataPrev">Data Previsão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataPrev' placeholder='Data Previsão' name='fMovDataPrev'   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovDataPrevFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPago">Valor Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPago' placeholder='Valor Pago' name='fMovValorPago'   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovValorPagoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataEmissao">Data de Emissão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovDataEmissaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oVPesquisa) ? $oVPesquisa->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
			$("#MovDataVencto").mask("99/99/9999");
			$("#Competencia").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovDataPrev").mask("99/99/9999");
			$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataEmissao").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
