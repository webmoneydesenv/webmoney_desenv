﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oVPesquisa = $_REQUEST['oVPesquisa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>VIEW - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=VPesquisa.preparaLista">Gerenciar VIEWs</a> &gt; <strong><?php echo $sOP?> VIEW</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> VIEW</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Vencimento:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovDataVencto() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Mov_contrato:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovContrato() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Código:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Item:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovItem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Competencia:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getCompetencia() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovValor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Tipo Movimento:

0 - A Pagar

1 - A Receber

:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovTipo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Descricao:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tip_nome:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getTipNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Pesg_codigo:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getPesgCodigo() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Nome:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getNome() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Identificacao:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getIdentificacao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Inclusão:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovDataInclusao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data Previsão:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovDataPrev() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Pago:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovValorPago() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Data de Emissão:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovDataEmissao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Documento:&nbsp;</strong><?= ($oVPesquisa) ? $oVPesquisa->getMovDocumento() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=VPesquisa.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
