<?php
 $voVPesquisa = $_REQUEST['voVPesquisa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De VIEWs</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=VPesquisa.preparaLista">Gerenciar VIEWs</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar VIEWs</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formVPesquisa" id="formVPesquisa" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesVPesquisa" onChange="JavaScript: submeteForm('VPesquisa')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=VPesquisa.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo VIEW</option>
   						<option value="?action=VPesquisa.preparaFormulario&sOP=Alterar" lang="1">Alterar VIEW selecionado</option>
   						<option value="?action=VPesquisa.preparaFormulario&sOP=Detalhar" lang="1">Detalhar VIEW selecionado</option>
   						<option value="?action=VPesquisa.processaFormulario&sOP=Excluir" lang="2">Excluir VIEW(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voVPesquisa)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('VPesquisa')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Data vencimento</th>
					<th class='Titulo'>Tipo</th>
					<th class='Titulo'>MovContrato</th>
					<th class='Titulo'>Código</th>
					<th class='Titulo'>Item</th>
					<th class='Titulo'>Competencia</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>- tipo movimento:

0 - a pagar

1 - a receber

</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>TipNome</th>
					<th class='Titulo'>PesgCodigo</th>
					<th class='Titulo'>Nome</th>
					<th class='Titulo'>Identificacao</th>
					<th class='Titulo'>Data inclusão</th>
					<th class='Titulo'>Data previsão</th>
					<th class='Titulo'>Valor pago</th>
					<th class='Titulo'>Data de emissão</th>
					<th class='Titulo'>Documento</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voVPesquisa as $oVPesquisa){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('VPesquisa')" type="checkbox" value="" name="fIdVPesquisa[]"/></td>
  					<td><?= $oVPesquisa->getMovDataVenctoFormatado()?></td>
					<td><?= $oVPesquisa->getTipo()?></td>
					<td><?= $oVPesquisa->getMovContrato()?></td>
					<td><?= $oVPesquisa->getMovCodigo()?></td>
					<td><?= $oVPesquisa->getMovItem()?></td>
					<td><?= $oVPesquisa->getCompetenciaFormatado()?></td>
					<td><?= $oVPesquisa->getMovValorFormatado()?></td>
					<td><?= $oVPesquisa->getMovTipo()?></td>
					<td><?= $oVPesquisa->getDescricao()?></td>
					<td><?= $oVPesquisa->getTipNome()?></td>
					<td><?= $oVPesquisa->getPesgCodigo()?></td>
					<td><?= $oVPesquisa->getNome()?></td>
					<td><?= $oVPesquisa->getIdentificacao()?></td>
					<td><?= $oVPesquisa->getMovDataInclusaoFormatado()?></td>
					<td><?= $oVPesquisa->getMovDataPrevFormatado()?></td>
					<td><?= $oVPesquisa->getMovValorPagoFormatado()?></td>
					<td><?= $oVPesquisa->getMovDataEmissaoFormatado()?></td>
					<td><?= $oVPesquisa->getMovDocumento()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voVPesquisa)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('VPesquisa');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
