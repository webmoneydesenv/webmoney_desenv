﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriMapaFornecedorFrete = $_REQUEST['oSupriMapaFornecedorFrete'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_mapa_fornecedor_frete - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriMapaFornecedorFrete.preparaLista">Gerenciar supri_mapa_fornecedor_fretes</a> &gt; <strong><?php echo $sOP?> supri_mapa_fornecedor_frete</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_mapa_fornecedor_frete</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Fornecedor:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getCodFornecedor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Forma:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getCodForma() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Transportadora:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getCodTransportadora() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Prazo de Entrega:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getPrazoEntrega() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getObs() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Contato:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getContato() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Forne:&nbsp;</strong><?= ($oSupriMapaFornecedorFrete) ? $oSupriMapaFornecedorFrete->getFone() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=SupriMapaFornecedorFrete.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
