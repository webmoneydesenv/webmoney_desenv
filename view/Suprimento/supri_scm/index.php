<?php
 $voSupriScm = $_REQUEST['voSupriScm'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>LISTA DE Solicitação de Compra de Material</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=SupriScm.preparaLista">Gerenciar Solicitação de Compra de Material</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Solicitação de Compra de Material</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formSupriScm" id="formSupriScm" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesSupriScm" onChange="JavaScript: submeteForm('SupriScm')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=SupriScm.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar nova Solicitação de Compra</option>
   						<option value="?action=SupriScm.preparaFormulario&sOP=Alterar" lang="1">Alterar Solicitação de Compra selecionada</option>
   						<option value="?action=SupriScm.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Solicitação de Compra selecionada</option>
   						<option value="?action=SupriScm.processaFormulario&sOP=Excluir" lang="2">Excluir Solicitação de Compra(s) selecionada(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voSupriScm)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('SupriScm')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Nº Solicitação</th>
					<th class='Titulo'>Descrição</th>
					<th class='Titulo'>Previsão entrega</th>
					<th class='Titulo'>Entrega</th>
					<th class='Titulo'>Aplicação</th>
					<th class='Titulo'>Observação</th>
					<th class='Titulo'>Emitente</th>
					<th class='Titulo'>Data da solicitação</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voSupriScm as $oSupriScm){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('SupriScm')" type="checkbox" value="<?=$oSupriScm->getCodScm()?>" name="fIdSupriScm[]"/></td>
                     <td><?= $oSupriScm->getCodScm()?></td>
                    <td><?= $oSupriScm->getNumeroSolicitacao()?></td>
					<td><?= $oSupriScm->getDescricao()?></td>
					<td><?//= $oSupriScm->getEntrega()?></td>
                    <td></td>
					<td><?= $oSupriScm->getAplicacao()?></td>
					<td><?= $oSupriScm->getObservacao()?></td>
					<td><?= $oSupriScm->getCodPessoaSolicitante()?></td>
					<td><?= $oSupriScm->getDataSolicitacaoFormatado()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voSupriScm)){?>
  			</table>
                    </form>
 	    <!--<script src="js/jquery/jquery.js"></script>-->
  		<script src="js/bootstrap.min.js"></script>
  		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {

               oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});


               /*
                oTable = $('#lista').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "index.php?action=SupriScm.preparaLista"


    			});
              */




   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('SupriScm');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
