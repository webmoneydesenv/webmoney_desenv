<?php
header("Content-Type: text/html; charset=UTF-8",true);

$oSupriProduto = $_REQUEST['oSupriProduto'];
$nQtde = $_REQUEST['nQtde'];

$sProjeto = $_REQUEST['sProjeto'];
$nRev = $_REQUEST['nRev'];
$nConsumoMensal = $_REQUEST['nConsumoMensal'];
$sAplicacao = $_REQUEST['sAplicacao'];
$dDataPrevEntrega = $_REQUEST['dDataPrevEntrega'];
$sTipo = $_REQUEST['sTipo'];

$DescTipo = ($sTipo == 1) ? "Unica" : "Fracionada";

?>

<input type="hidden" class='pdt' name='fCodProduto[]' value='<?= $oSupriProduto->getCodProduto() ?>'>
<input type="hidden" name='fQtde[]' value='<?= $nQtde ?>'>
<input type="hidden" name='fProjeto[]' value='<?= $sProjeto ?>'>
<input type="hidden" name='fRevisao[]' value='<?= $nRev ?>'>
<input type="hidden" name='fConsumoMensal[]' value='<?= $nConsumoMensal ?>'>
<input type="hidden" name='fAplicacaoItem[]' value='<?= $sAplicacao ?>'>
<input type="hidden" name='fDataPrevEntrega[]' value='<?= $dDataPrevEntrega ?>'>
<input type="hidden" name='fTipo' value='<?= $sTipo ?>'>

 <div class='form-group' style="border-bottom:#DDD 1px solid;padding-bottom:2px;margin-left:inherit;width:100%;background-color:#F5F5F5;">
    <div class='col-sm-1'><button  type='button' class='btn btn-danger removeButton' onclick='removerCampo(<?=$_REQUEST['idCampo']?>)' value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>
    <div class='col-sm-2'><?=$oSupriProduto->getDescricao()?></div>
    <div class='col-sm-1'><?=$dDataPrevEntrega?></div>
    <div class='col-sm-1'><?=$DescTipo?></div>
    <div class='col-sm-1' style='padding-left: 20px;'><?= $nQtde ?>(<?=$oSupriProduto->getSupriUnidade()->getDescricao()?>)</div>
    <div class='col-sm-2' style='padding-left: 30px;'><?= $sProjeto ?></div>
    <div class='col-sm-1' style='padding-left: 30px;'><?= $nRev ?></div>
    <div class='col-sm-1' style='text-align:center;'><?= $nConsumoMensal ?></div>
    <div class='col-sm-2' style='padding-left: 100px;'><?= $sAplicacao ?></div>
</div>

