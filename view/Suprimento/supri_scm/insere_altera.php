<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriScm = $_REQUEST['oSupriScm'];
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];

$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnyConta = $_REQUEST['voMnyConta'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];
$voSupriProduto = $_REQUEST['voSupriProduto'];

$voSysEmpresa = $_REQUEST['voSysEmpresa'];

$voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];

$voSupriScmItem = $_REQUEST['voSupriScmItem'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Solicitação de Compra - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriScm.preparaLista">Gerenciar Solicitação de Compra</a> &gt; <strong><?php echo $sOP?> Solicitação de Compra</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
           <h3 class="TituloPagina"><?= $sOP?> Solicitação de Compra </h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" enctype="multipart/form-data" class="form-horizontal" id="formSCM" name="formSupriScm" action="?action=SupriScm.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodScm" value="<?=(is_object($oSupriScm)) ? $oSupriScm->getCodScm() : ""?>" />
         <input type="hidden" name="fCodPessoaSolicitante" value="<?=(is_object($oSupriScm)) ? $oSupriScm->getCodPessoaSolicitante() : $_SESSION['Perfil']['CodUsuario']?>" />
         <input type="hidden" name="fEmpCodigo" value="<?=($oSupriScm) ? $oSupriScm->getEmpCodigo() : $_SESSION['oEmpresa']->getEmpCodigo()?>" />
         <input type='hidden' name='fDataSolicitacao'   value='<?= ($oSupriScm) ? $oSupriScm->getDataSolicitacaoFormatado() : date('d/m/Y H:i:s')?>'/>
         <input type='hidden' name='fNumeroScm'   value='<?= ($oSupriScm) ? $oSupriScm->getNumeroScm() : $_REQUEST['nNumeroScm'] ?>'/>

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados da Solicitação de Compra">

        <div class="form-group">
            <label class="col-sm-2 control-label" style="text-align:left" for="Numero Solicitacao">Nº Solicitação:</label>
            <div class="col-sm-2" id='divNumeroSolicitacao'>
                <?  if($oSupriScm){
                       echo "<span class = 'label label-primary' style='font-size:20px;'>".$oSupriScm->getNumeroSolicitacao()."</span>";
                        echo "<input type='hidden' name='fNumeroSolicitacao' value='".$oSupriScm->getNumeroSolicitacao()."'>";
                    }else{
                        echo "<span class = 'label label-primary'>".$_REQUEST['sNumeroSolicitacao']."</span>";
                    }
                ?>
            </div>
        </div>

<div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais</legend>

                <div class="form-group">
               <label class="col-sm-2 control-label" style="text-align:left" for="Entrega">Aplicação:</label>
                <div class="col-sm-5   ">
                    <select name="fAplicacao" class="form-control chosen">
                        <option value="">Selecione</option>
                        <option <?=($oSupriScm && $oSupriScm->getAplicacao() == 1) ? " selected " : ""?> value="1">Estoque</option>
                        <option <?=($oSupriScm && $oSupriScm->getAplicacao() == 2) ? " selected " : ""?> value="2">Aplicação Imediata</option>
                    </select>
				</div>

              </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Descri&ccedil;&atilde;o">Descri&ccedil;&atilde;o:</label>
                    <div class="col-sm-10">
                     <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oSupriScm) ? $oSupriScm->getDescricao() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Observação:</label>
					<div class="col-sm-10">
					<textarea rows="4" class="form-control" id='Observacao' placeholder='Observação' name='fObservacao'><?= ($oSupriScm) ? $oSupriScm->getObservacao() : ""?></textarea>
				</div>
				</div>
				<br>

                </div>

                <div class="col-sm-6">
                <legend>Apropriações </legend>

          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
            	<div class="col-sm-10">
				       <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                <option value="">Selecione</option>
                <? $sSelected = "";
						   if($voMnyCusto){
							   foreach($voMnyCusto as $oMnyCusto){
								   if($oSupriScm){
									   $sSelected = ($oSupriScm->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                <option  <?= $sSelected?> value="<?= $oMnyCusto->getPlanoContasCodigo()?>"> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                <?
							   }
						   }
						?>
              </select>
		    	</div>
         	    </div>

          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
           		<div class="col-sm-10">
				    <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
                           <option value="">Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroNegocio){
                                           foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                               if($oSupriScm){
									               $sSelected = ($oSupriScm->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
								                }
                                            ?>
                            <option  <?= $sSelected?> value="<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>"><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?></option>
                            <?
                                           }
                                       }
                                    ?>
                          </select>
                    </div>
                    </div>
                   <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                    <? $aUnidade = array(200,324,325,326,327,328);
                       if(!(in_array($_SESSION['Perfil']['CodUnidade'] ,$aUnidade))) {
                       echo "<strong>" . $_SESSION['Perfil']['Unidade'] ."</strong>"; ?>
                       <input type='hidden' name='fUniCodigo' value="<?= $_SESSION['Perfil']['CodUnidade']?>">
                <?   }else{ ?>
                           <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
                            <option value="">Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                          if($oSupriScm){
									           $sSelected = ($oSupriScm->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
								            }

                                ?>
                        <option  <?= $sSelected?> value="<?= $oMnyUnidade->getPlanoContasCodigo()?>"><?= $oMnyUnidade->getCodigo()?> -
                        <?= $oMnyUnidade->getDescricao()?>
                        </option>
                        <?
                                       }
                                   }
                                ?>
                      </select>

                  <? }?>
				</div>
          		</div>

      <? if($voMnyCentroCusto){ ?>
            <div class="form-group">
			<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">C. Custo:</label>
            <div class="col-sm-10">
              <select name='fCenCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  onChange="recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipo&sOP=1&fCenCodigo='+this.value,'divConta',1)">
                <option value="">Selecione</option>
                <? $sSelected = "";
						   if($voMnyCentroCusto){
							   foreach($voMnyCentroCusto as $oMnyCentroCusto){
								    if($oSupriScm){
									   $sSelected = ($oSupriScm->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
								   }

						?>
                <option  <?= $sSelected?> value="<?= $oMnyCentroCusto->getPlanoContasCodigo()?>"><?= $oMnyCentroCusto->getCodigo()?> -<?= $oMnyCentroCusto->getDescricao()?></option>
                <?
							 }
						   }
						?>
              </select>
            </div>

          </div>
			<? } ?>

		        <div class="form-group" >
        	    <label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                <div class="col-sm-10"  id="divConta">
			 		<select  name='fConCodigo'  class="form-control chosen"  required  >
						    <option  value="">Selecione...</option>
                		<? $sSelected = "";
						   if($voMnyConta){
							   foreach($voMnyConta as $oMnyConta){
								   if($oSupriScm){
									   $sSelected = ($oSupriScm->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
								   }

						?>
                        <option  <?= $sSelected?> value="<?= $oMnyConta->getPlanoContasCodigo()?>"><?= $oMnyConta->getCodigo()?> -<?= $oMnyConta->getDescricao()?></option>
                    	<?
                                }
                              }
                            ?>

                  </select>
				</div>
          		</div>
	           <div class="form-group">
     	       <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
        	   <div class="col-sm-10" id="divConta">
					<select name='fSetCodigo'  class="form-control chosen"  required  onchange=" recuperaConteudoDinamico('index','action=SupriScm.atualizaNumeroSolicitacao&nSetCodigo='+this.value,'divNumeroSolicitacao',1);">
						    <option value="">Selecione</option>
                		<? $sSelected = "";
						   if($voMnySetor){
							   foreach($voMnySetor as $oMnySetor){
						          if($oSupriScm){
									   $sSelected = ($oSupriScm->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                            <option  <?= $sSelected?> value="<?= $oMnySetor->getPlanoContasCodigo()?>"><?= $oMnySetor->getCodigo()?> -<?= $oMnySetor->getDescricao()?></option>
                            <?
							   }
						   }
						?>
              </select>
				</div>
          		</div>
            	</div>
                </div>
                <br>
                <div class="form-group">
                       <div class="col-sm-12">
                        <legend title="Documentos">Documentos</legend>
                       </div>
                          <div class="col-sm-12">
                                <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>

                                      <div class="form-group">
                                      <? $i = 0;
                                         $j = 0;?>

                                              <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                                                    <div>
                                                        <label class="col-sm-2 control-label"  style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao() ?>:</label>

                                                     <?
                                                           if($_REQUEST['sOP'] == "Alterar"){
                                                           $voMnyContratoPessoaArquivoTipo = $oFachada->recuperarTodosSupriArquivoPorContratoTipoArquivo($oSupriScm->getCodScm(),$oMnyContratoDocTipo->getCodContratoDocTipo());
                                                           //$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor();
                                                            } ?>
                                                            <div class="col-sm-10" >
                                                            <? if($voMnyContratoPessoaArquivoTipo){
                                                                    foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
                                                                           if($oMnyContratoPessoaArquivoTipo->getCodArquivo() && ($_REQUEST['sOP'] =='Alterar')){
                                                                                  // atualizar
                                                                                    echo "<div class=\"col-sm-1 \" ><a target='_blank' href='?action=SupriArquivo.preparaArquivo&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'><button type=\"button\" class=\"btn btn-success btn-sm \"  style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
                                                                                    <div class=\"col-sm-1\">";
                                                                                    if($oMnyContratoDocTipo->getObrigatorio() != 1){
                                                                                      echo "<div class=\"col-sm-1\"><a data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=SupriArquivo.processaFormulario&sOP=Excluir&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."&fCodScm=".$oSupriScm->getCodScm() ." \"><button type=\"button\" class=\"btn btn-danger \" style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div>";
                                                                                    }
                                                                                     echo"</div>
                                                                                          <div class=\"col-sm-3\"><input type=\"file\" id=\"file$i\"  onblur=\"verificaUpload('file$i',".$oMnyContratoDocTipo->getTamanhoArquivo().")\"  data-buttonText=\"Atualizar\" class=\"\" style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /></div>

                                                                                            <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'>".(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)."MB</span>


                                                                                     <input type='hidden' name='fCodContratoDocTipo[]' required value='". $oMnyContratoDocTipo->getCodContratoDocTipo().'||'.$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'>
                                                                                     <input type='hidden' name='fCodArquivo[]' required value='".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'>";

                                                                             } else{ ?>
                                                                                 <input type="file" id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/>
                                                                                 <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                                                                 <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                                                 <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
                                                                            <? }
                                                                    }?>
                                                            <? }else{ ?>
                                                                <div class="col-sm-3">
                                                                  <input type="file" id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                   <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                                                </div>
                                                                <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div class='col-sm-3' style=\"color:red;\" > * Arquivo Obrigat&oacute;rio. </div>" : ""?>
                                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                            <? } ?>
                                                            </div>

                                                          <? $i++ ?>
                                                    </div>
                                                     <p style='border-bottom: 1px solid #ccc;'>&nbsp;</p>
                                              <? } ?>

                                      </div>
                                              <div class="col-sm-2">
                                                   <div class="alert alert-danger" role="alert" style="text-align:center; font-weight:bold;" >
                                                       <a href='https://smallpdf.com/pt/compressor-de-pdf' target="_blank">COMPRESSOR DE PDF</a>
                                                    </div>
                                              </div>
                                       </div>
                                </div>
                 </div>

             <div class="form-group">
                 <div class="col-sm-12">
                     <legend>Produtos</legend>
                 </div>
                <div class="col-sm-12">
                     <div class="form-group">
                         <label class="col-sm-1 control-label" style="text-align:left" for="Produto">Produto:</label>
                         <div class="col-sm-7">
                             <select class="form-control chosen" id='CodProduto' onchange="recuperaConteudoDinamico('index.php','action=SupriProduto.exibeUnidade&nCodProduto='+this.value,'un','1')">
                                <option selected disabled>Selecione...</option>
                                 <? if($voSupriProduto){
                                        foreach($voSupriProduto as  $oSupriProduto){ ?>
                                            <option value='<?=$oSupriProduto->getCodProduto()?>'><?=$oSupriProduto->getDescricao()?></option>
                                      <?  }
                                    }?>
                             </select>
                         </div>
                         <label class="col-sm-1 control-label" style="text-align:left" for="Setor">Qtd:</label>
                         <div class="col-sm-1">
                             <input type='text' class='form-control' value='' id='Qtde'>
                         </div>
                         <div class="col-sm-1" style='font-weight:bold;' id='un'></div>
                    </div>
                    <div class="form-group">
                           <label class="col-sm-1 control-label" style="text-align:left" for="Projeto">Projeto:</label>
                         <div class="col-sm-2">
                             <input type='text' class='form-control' value='' id='sProjeto'>
                         </div>
                           <label class="col-sm-1 control-label" style="text-align:left" for="Rev">Revis&atilde;o:</label>
                         <div class="col-sm-1">
                             <input type='text' class='form-control' value='' id='nRev'>
                         </div>

                           <label class="col-sm-1 control-label" style="text-align:left" for="ConsumoMensal">Consumo Mensal:</label>
                         <div class="col-sm-1">
                             <input type='text' class='form-control' value='' id='nConsumoMensal'>
                         </div>
                        <label class="col-sm-1 control-label" style="text-align:left" for="Aplicacao">Aplica&ccedil;&atilde;o:</label>
                         <div class="col-sm-2">
                             <input type='text' class='form-control' value='' id='sAplicacao'>
                         </div>
<div class="col-sm-1" style='font-weight:bold;' id='un'></div>
                         <div class="col-sm-1">
                            <button type='button'  class='btn btn-success'  onclick='addCampos();'  value=''><i class='glyphicon glyphicon-plus'></i>Adicionar</button>
                             <input type='hidden' id='teste' value='0'>
                         </div>
                     </div>
                 </div>
             </div>

              <div class="form-group">
                   <label class="col-sm-1 control-label" style="text-align:left" for="DataPrevEntrega">Previsão:</label>
                <div class="col-sm-2">
                    <input class="form-control" type='text' id='DataPrevEntrega' placeholder='Previsão Entrega' name='fDataPrevEntrega'   value=''/>
				</div>
                   <label class="col-sm-1 control-label" style="text-align:left" for="Entrega">Entrega:</label>
                <div class="col-sm-1">
                    <select name="fEntrega" class="form-control chosen" id='tipo' >
                        <option value="1">Única</option>
                        <option value="2">Fracionada</option>
                    </select>
				</div>
              </div>


             <div class="form-group" >
                     <div class="col-sm-12" style="background-color:#E9F2F9;">
                     <label class="col-sm-3 control-label" style="text-align:left" for="Especificacao">Especifica&ccedil;&atilde;o</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Quantidade">Data Prev</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Quantidade">Entrega</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Quantidade">Qtde</label>
                     <label class="col-sm-2 control-label" style="text-align:left" for="Projeto">Projeto</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Rev">Revis&atilde;o</label>
                     <label class="col-sm-2 control-label" style="text-align:left" for="Consumo Mensal">Consumo Mensal</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Rev">Aplicação</label>
                     </div>
             </div>


             <? if($_REQUEST['sOP'] == "Alterar"){?>
                 <div class="form-group">
                     <div class="col-sm-12">
                         <? if($voSupriScmItem){
                                $i=0;
                                foreach($voSupriScmItem as $oSupriScmItem){ ?>
                                   <div class='form-group' id='div<?=$i?>' style="border-bottom:#DDD 1px solid;padding-bottom:2px;margin-left:inherit;width:100%;background-color:#F5F5F5;">
                                        <div class='col-sm-1'><button  type='button' class='btn btn-danger removeButton' onclick="recuperaConteudoDinamico('index.php','action=SupriScm.excluiItem&nCodScmItem=<?= $oSupriScmItem->cod_scm.'||'.$oSupriScmItem->cod_scm_item?>','div<?=$i?>','1');removeVerificaProduto(<?=$i?>)" value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>
                                        <div class='col-sm-2'><?= $oSupriScmItem->descricao?></div>
                                        <div class='col-sm-1'><?= $oSupriScmItem->data_entrega_formatada?></div>
                                        <div class='col-sm-1'><?= $oSupriScmItem->entrega_formatada?></div>
                                        <div class='col-sm-1' style='padding-left: 20px;'><?= $oSupriScmItem->qtde ?></div>
                                        <div class='col-sm-2' style='padding-left: 30px;'><?= $oSupriScmItem->projeto ?></div>
                                        <div class='col-sm-1' style='padding-left: 30px;'><?= $oSupriScmItem->revisao ?></div>
                                        <div class='col-sm-1' style='text-align:center;'><?= $oSupriScmItem->consumo_mensal ?></div>
                                        <div class='col-sm-2' style='padding-left: 100px;'><?= $oSupriScmItem->aplicacao ?></div>
                                    </div>
                                                    <? $i++; ?>
                              <?}?>
                         <? }?>
                     </div>
                 </div>
             <? } ?>





          <div class="form-group">
              <div class="col-sm-12" id="campoPai" ></div>
              <div id='pre' style='margin-left: 40%;width: 20%;'></div>
          </div>
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="button" class="btn btn-primary" onclick='validaItem();'><?=$sOP?></button>
            <input type="hidden" id='campos' value=''>
     	</div>
   		</div>
       </form>
        <div id="msg" title="Mensagem" style="display:none">&nbsp;</div>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">



			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })


             var qtdeCampos = 0;
             var cont=0;
             //var  nCodProduto =1;
             var produtos = [];
             var qtdeCampos=0;



			 jQuery(function($){
                $("#DataPrevEntrega").mask("99/99/9999");
                //$("#DataSolicitacao").mask("99/99/9999");
			 });
			 jQuery(function($){

                   <? if($voSupriScmItem){
                           foreach($voSupriScmItem as $oSupriScmItem){ ?>
                                produtos[qtdeCampos] = <?= $oSupriScmItem->cod_produto ?>;
                                qtdeCampos++;
                        <? }?>
                 <? }?>


			 });


        </script>
        <script type="text/javascript" language="javascript">

            function verificaEntrega(tipo){
                if(tipo == 1){
                    document.getElementById('QtdeEntrega').value = 1;
                    document.getElementById('QtdeEntrega').disabled = true;
                }else{
                    document.getElementById('QtdeEntrega').disabled = false;
                }

            }



            function addCampos(){

                var nCodProduto = document.getElementById('CodProduto').value;
                var qtde    = document.getElementById('Qtde').value;
                var sProjeto    = document.getElementById('sProjeto').value;
                var nRev    = document.getElementById('nRev').value;
                var nConsumoMensal    = document.getElementById('nConsumoMensal').value;
                var sAplicacao    = document.getElementById('sAplicacao').value;
                var dDataPrevEntrega    = document.getElementById('DataPrevEntrega').value;
                var sTipo  = document.getElementById('tipo').value;



                if(focus()){

				produtos[qtdeCampos]=nCodProduto;

				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:
                    if(nCodProduto > 0){
                        if(!verificaProduto(nCodProduto,produtos)){

                            document.getElementById('msg').innerHTML = 'Este produto já foi inserido!';
                            $( "#msg" ).dialog();

                            removeVerificaProduto(qtdeCampos);

                            return false;

                        }else{

                            document.getElementById("filho"+qtdeCampos).innerHTML="<div id='idSCM_"+qtdeCampos+"_"+nCodProduto+"'></div>";
                            document.getElementById("campos").value = (qtdeCampos + 1) ;

                            recuperaConteudoDinamico('index.php','action=SupriScm.insereProduto&idCampo='+qtdeCampos+'&nCodProduto='+nCodProduto+'&Qtde='+qtde+'&sProjeto='+sProjeto+'&nRev='+nRev+'&nConsumoMensal='+nConsumoMensal+'&sAplicacao='+sAplicacao+'&dDataPrevEntrega='+dDataPrevEntrega+'&sTipo='+sTipo,'idSCM_'+qtdeCampos+"_"+nCodProduto,2);
                            qtdeCampos++;


                            document.getElementById('CodProduto').value = "";
                            document.getElementById('Qtde').value = "";
                            document.getElementById('sProjeto').value = "";
                            document.getElementById('nRev').value = "";
                            document.getElementById('nConsumoMensal').value = "";
                            document.getElementById('sAplicacao').value = "";
                            document.getElementById('DataPrevEntrega').value = "";
                            //document.getElementById('tipo').selectedIndex = "-1";

                        }
                    }else{
                        document.getElementById( 'msg' ).innerHTML = 'Você precisa selecionar um produto!';
                        $( "#msg" ).dialog();
                    }
                }

			 }

             function focus(){
                   var qtde    = document.getElementById('Qtde').value;
                   if((!qtde) || (qtde == 0)){
                       document.getElementById( 'msg' ).innerHTML = 'Informe a quantidade desejada do produto!';
                       $( "#msg" ).dialog();
                        document.getElementById('Qtde').focus();
                        document.getElementById('Qtde').style.backgroundColor = '#F89C8F';
                        return false;
                   }else{
                         document.getElementById('Qtde').style.backgroundColor = '#FFF';
                        //document.getElementById('Qtde').onfocus();

                    return true;
                }
             }
             function removerCampo(id){
                 var qtde = document.getElementById("campos").value;
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);
                 produtos[id] ='';
                 qtdeCampos--;
                 document.getElementById("campos").value = (qtde - 1) ;
			 }
             function verificaProduto(nProduto,produtos){
                var cont = 0;
                var array_temp=[];
                for (var i = 0; i < produtos.length; i++){
                        if(produtos[i] == nProduto){
                            cont++;

                        }
                 }
                 if(cont > 1){
                    cont--;
                    return false;

                 }else{
                   return true;
                 }

              }
             function removeVerificaProduto(id){
				array_remove_temp = [];
				for (var i = 0; i < produtos.length; i++) {
						if( i == id){
							produtos[i] = '';
						}
				 }
			  }

            function validaItem(){

                <? if($_REQUEST['sOP'] == "Cadastrar"){?>

                        var produto = document.getElementById('campos').value;

                        if(produto > 0){
                           document.getElementById("formSCM").submit();
                        }else{
                             document.getElementById( 'msg' ).innerHTML = 'Você precisa inserir pelo menos 1 produto!';
                             $( "#msg" ).dialog();
                        }

                <?  }else{ ?>

                            document.getElementById("formSCM").submit();

                <? } ?>

                return false;
            }

            function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert,n){

                oDiv = document.getElementById(sIdDivInsert);
                pre = document.getElementById('pre');
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                    if(n == 1){
                                        oDiv.innerHTML ="<div class='col-sm-1' ><img  style='width:30px;' src='imagens/pre_loader.gif' title='img/ajax-loaders/pre_loader.gif'></img></div> ";
                                    }else{
                                        //oDiv.innerHTML ="<div class='col-sm-12' ><img  style='width:200px;' src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                        pre.innerHTML ="<div class='col-sm-12'><img  style='width:400px;' src='imagens/loader.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img></div> ";
                                    }

                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         pre.innerHTML = '';
                                         oDiv.innerHTML = data;
                        },
                        complete:function(){jQuery(function($){$('.chosen').chosen();});

                                           }
                });
            }

 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
