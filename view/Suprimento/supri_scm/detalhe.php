﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriScm = $_REQUEST['oSupriScm'];
 $voSupriScmItem  =  $_REQUEST['voSupriScmItem'];
 $voTramitacaoStatus = $_REQUEST['voTramitacaoStatus'];
 $sSTatusWizardBreadcrumb = $_REQUEST['sSTatusWizardBreadcrumb'];
 $voSupriProtocoloEncaminhamento = $_REQUEST['voSupriProtocoloEncaminhamento'];
 $voTramitacao = $_REQUEST['voTramitacao'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Soicitação - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->


     <style>

            /*body{margin:40px;}*/

            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 100%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;

            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
              width: 30px;
              height: 30px;
              text-align: center;
              padding: 6px 0;
              font-size: 12px;
              line-height: 1.428571429;
              border-radius: 15px;
            }

     </style>



 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriScm.preparaLista">Gerenciar Soicitações</a> &gt; <strong><?php echo $sOP?> Soicitação</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Soicitação de Compra</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Nº Solicitação:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getNumeroSolicitacao() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Solicitante:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getAcessoUsuario()->getLogin() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Descrição:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getDescricao() : ""?></div>
				</div>
				<br>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Data Prvisão Entrega:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getDataSolicitacaoFormatado() : ""?></div>

					 <!--<div class="col-sm-4" style="text-align:left"><strong>Quantidade:&nbsp;</strong><?//= ($oSupriScm) ? $oSupriScm->getQtdeEntrega() : ""?></div>-->
                        <div class="col-sm-4" style="text-align:left"><strong>Data da Solicitação:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getDataSolicitacaoFormatado() : ""?></div>
					 <div class="col-sm-4" style="text-align:left"><strong>Aplicação :&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getAplicacao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Custo:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasCusto()->getDescricao() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Centro de Negocio:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasNegocio()->getDescricao() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasUnidade()->getDescricao() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Centro de Custo:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasCusto()->getDescricao() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Setor:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasSetor()->getDescricao()  : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Conta:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getMnyPlanoContasConta()->getDescricao()  : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getSysEmpresa()->getEmpFantasia() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getObservacao() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Emitente:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getAcessoUsuario()->getLogin() : ""?></div>
				</div>
				<br>


            <div class="form-group">
                <div class="col-sm-12">
                <legend>Produtos</legend>


           <table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voSupriScmItem)){?>
   				<thead>
   				<tr>
   					<th class='Titulo'>Produto</th>
					<th class='Titulo'>Data Previsão</th>
					<th class='Titulo'>Entrega</th>
					<th class='Titulo'>Qtde</th>
					<th class='Titulo'>Qtde Antendida</th>
					<th class='Titulo'>Projeto</th>
					<th class='Titulo'>Revisão</th>
					<th class='Titulo'>Consumo Mensal</th>
					<th class='Titulo'>Aplicação</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voSupriScmItem as $oSupriScmItem){ ?>
   				<tr>
                    <td><a href='?action=SupriScmItem.preparaFormulario&sOP=Detalhar&nCodScm=<?=$oSupriScmItem->cod_scm?>&nItem=<?=$oSupriScmItem->cod_scm_item?>' target="_blank"><?=$oSupriScmItem->descricao?></a></td>
					<td><?=$oSupriScmItem->data_entrega_formatada?></td>
					<td><?=$oSupriScmItem->entrega_formatada?></td>
					<td><?=$oSupriScmItem->qtde ?></td>
					<td><?=$oSupriScmItem->qtde_atendida ?></td>
					<td><?=$oSupriScmItem->projeto ?></td>
					<td><?=$oSupriScmItem->revisao ?></td>
					<td><?=$oSupriScmItem->consumo_mensal ?></td>
					<td><?=$oSupriScmItem->aplicacao ?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voSupriArquivo)){?>
  			</table>
             </div>
          </div>


                      <form method="post" enctype="multipart/form-data"  class="form-horizontal" name="formSupriScmProtocolo" action="?action=SupriScmProtocolo.processaFormulario">
                      <input type="hidden" name="sTipoLancamento" value="<?=$_REQUEST['sTipoLancamento']?>">
                      <input type="hidden" name="sOP" value="Cadastrar" />
                      <input type="hidden" name="fCodScm" value="<?= ($oSupriScm) ? $oSupriScm->getCodScm() : "" ?>" />
                      <input type='hidden' name='fData' required value='<?=date('d/m/Y H:i:s')?>'/>
                      <input type='hidden' name='fAtivo' value='1'/>

           <!--
              <div class="form-group">
                 <div class="col-sm-12">
                 <div class="row" style="background:#CCC;margin-right:0px; margin-left:0px;">

                    <div class="col-sm-1"><strong>Detalhar</strong></div>
                    <div class="col-sm-1"><strong>DATA</strong></div>
                    <div class="col-sm-4"><strong>DE</strong></div>
                    <div class="col-sm-4"><strong>PARA</strong></div>
                    <div class="col-sm-2"><strong>QUEM</strong></div>
                 </div>
                 </div>
             </div>
            -->



                <div class="form-group">
                    <div class="col-sm-12">
                        <legend>Hist&oacute;rico de Tramita&ccedil;&otilde;es</legend>
                    </div>
                </div>
       <? if($voTramitacao){ ?>
                <div class="row" style="background:#CCC;margin-right:0px; margin-left:0px;">
                    <div class="col-sm-1"><strong>Detalhar</strong></div>
                    <div class="col-sm-1"><strong>DATA</strong></div>
                    <div class="col-sm-4"><strong>DE</strong></div>
                    <div class="col-sm-4"><strong>PARA</strong></div>
                    <div class="col-sm-2"><strong>QUEM</strong></div>
                </div>
      <!--id="tram" class="collapse"-->
            <? foreach($voTramitacao as $oTramitacao){
                $nUltimaPosicaoPara = $oTramitacao->getParaCodTipoProtocolo();
                $nUltimaPosicaoDe = $oTramitacao->getDeCodTipoProtocolo();
           ?>

                    <div class="row">
                        <?// if($oTramitacao->getDeCodTipoProtocolo() != $oTramitacao->getParaCodTipoProtocolo()){ ?>
                        <div class="form-group" style="padding:10px 0px">
                            <div class="col-sm-1" align="center"><a onclick="MostraEsconde2('Detalhe<?php echo $i?>');"><span class="glyphicon glyphicon-info-sign large" aria-hidden="true" style="font-size: 20px;"></span></a></div>
                            <div class="col-sm-1"><?php echo $oTramitacao->getDataFormatado()?></div>
                            <div class="col-sm-4"><?php echo $oTramitacao->getDeSysTipoProtocolo()->getDescricao() ?></div>
                            <div class="col-sm-4"><?php echo $oTramitacao->getParaSysTipoProtocolo()->getDescricao() ?></div>
                            <div class="col-sm-2"><?php echo $oTramitacao->getResponsavel() ?></div>
                        </div>

                    </div>
                    <div class="row" style="display:none;margin-bottom:10px;margin-top:10px; " id="Detalhe<?php echo $i;?>">
                    <div class="col-sm-1"><strong>Descrição:</strong></div>
                    <div class="col-sm-11"><textarea readonly class="form-control"><?php echo $oTramitacao->getDescricao()?></textarea></div>

                    </div>

            <hr style="margin-top:0px; margin-bottom:0px">
            <? $i++;
                } ?>

      <? }  else {?>
      	<div class="col-sm-12">N&atilde; h&aacute; tramita&ccedil;&otilde;es para esta solicitação</div>
      <? } ?>
        <br><br>


             <div class="form-group">
                 <div class="col-sm-12" style='padding: 50px;'>
                    <div class="stepwizard">
                        <div class="stepwizard-row">
                                <?
                                 if($voTramitacaoStatus){

                                    foreach($voTramitacaoStatus as $oTramitacaoStatus){?>
                                          <div class="stepwizard-step">
                                            <button type="button" <?= ($sSTatusWizardBreadcrumb == $oTramitacaoStatus->getDe())? 'class="btn btn-success btn-circle"  ' : 'class="btn btn-default btn-circle" '?>></button>
                                            <p><?= $oTramitacaoStatus->getAtivo()?></p>
                                          </div>
                                <?  }
                                   }?>

                                <div class="stepwizard-step">
                                    <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
                                    <p>Finalizado</p>
                                </div>
                        </div>
                    </div>
                 </div>
             </div>

    <?//php if($sSTatusWizardBreadcrumb == $_SESSION['Perfil']['CodGrupoUsuario'] ){ ?>

                 <div class="form-group">
                 <div class="col-sm-12">
                    <div class="row" style="padding-bottom:20px;magin:0px;">

                        <label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">De:</label>
                        <div class="col-sm-2">
                            <input class="form-control" type='text' readonly  value="<?= $_SESSION['Perfil']['GrupoUsuario']?>">
                           <input type='hidden' name='fDeCodTipoProtocolo' value="<?= $_SESSION['Perfil']['CodGrupoUsuario']?>">

                        </div>
                        <label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">Para:</label>
                        <div class="col-sm-2" id="divPara">
                             <select name='fParaCodTipoProtocolo'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                                <? $sSelected = "";
                                   if($voSupriProtocoloEncaminhamento){
                                       foreach($voSupriProtocoloEncaminhamento as $oSupriProtocoloEncaminhamento){

                                ?>
                                           <option  <?= $sSelected?> value='<?= $oSupriProtocoloEncaminhamento->getPara()?>'><?= $oSupriProtocoloEncaminhamento->getAtivo()?></option>
                                <?
                                       }
                                   }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label" style="text-align:left" for="Responsavel">Responsavel:</label>
                        <div class="col-sm-4">
                            <input class="form-control" type='text' name='fResponsavel' required readonly value='<?= $_SESSION['oUsuarioImoney']->getLogin()?>'/>
                        </div>
                    </div>
                  </div>
               </div>



                <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descrição:</label>
					<div class="col-sm-11">
                         <textarea class="form-control" rows="3" placeholder='Descrição' name='fDescricao'></textarea>
				    </div>

				</div>
                <div class="form-group">&nbsp;</div>


                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-2"><button type="submit" class="btn btn-primary" >Tramitar</button></div>
                 </div>
        </form>

        <?// } ?>

 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/producao.js" type="text/javascript"></script>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
