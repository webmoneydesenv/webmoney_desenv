     <div class="navbar-default">
 		<div class="navbar-header">
                 <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                     <span class="sr-only">Navegação Responsiva</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
             </div>
         <div id="navbar-collapse-1" class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
 		    <li class="nav nav-pills btn-primary" ><a href="?">INÍCIO</a></li>
             <!-- Classic dropdown -->
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">CADASTROS<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                 	<li><a tabindex="-1" href="?action=SupriMapaCompras.preparaLista">supri_mapa_compras</a></li>
	<li><a tabindex="-1" href="?action=SupriMapaFornecedor.preparaLista">supri_mapa_fornecedor</a></li>
	<li><a tabindex="-1" href="?action=SupriMapaFornecedorFrete.preparaLista">supri_mapa_fornecedor_frete</a></li>
	<li><a tabindex="-1" href="?action=SupriMapaFornecedorItem.preparaLista">supri_mapa_fornecedor_item</a></li>
	<li><a tabindex="-1" href="?action=SupriOm.preparaLista">supri_om</a></li>
	<li><a tabindex="-1" href="?action=SupriOmItem.preparaLista">supri_om_item</a></li>
	<li><a tabindex="-1" href="?action=SupriProduto.preparaLista">supri_produto</a></li>
	<li><a tabindex="-1" href="?action=SupriScm.preparaLista">supri_scm</a></li>
	<li><a tabindex="-1" href="?action=SupriScmItem.preparaLista">supri_scm_item</a></li>
	<li><a tabindex="-1" href="?action=SupriScmItemOmItem.preparaLista">supri_scm_item_om_item</a></li>
	<li><a tabindex="-1" href="?action=SupriUnidade.preparaLista">supri_unidade</a></li>

                 <li class="divider"></li>
               </ul>
             </li>
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">CONSULTAS<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                 <li><a tabindex="-1" href="#"> Consulta 01 </a></li>
                 <li><a tabindex="-1" href="#"> Consulta 02 </a></li>
                 <li><a tabindex="-1" href="#"> Consulta 03 </a></li>
               </ul>
             </li>
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">SEGURANÇA<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                 <li><a tabindex="-1" href="?action=AcessoModulo.preparaLista"> Módulos </a></li>
                 <li><a tabindex="-1" href="?action=AcessoGrupoUsuario.preparaLista"> Grupos de Usuário </a></li>
 				 <li><a tabindex="-1" href="?action=AcessoUsuario.preparaLista"> Usuários </a></li>
                 <li class="divider"></li>
                 <li><a tabindex="-1" href="?action=AcessoUsuario.preparaFormulario&sOP=AlterarSenha"> Alterar Senha </a></li>
               </ul>
             </li>
              <li class="nav nav-pills btn-danger"><a href="?action=Login.processaFormulario&sOP=Logoff" data-toggle="dropdown" class="dropdown-toggle">SAIR</a>
           </ul>
         </div>
     </div>
