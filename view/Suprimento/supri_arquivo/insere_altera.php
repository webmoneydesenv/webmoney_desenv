<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriArquivo = $_REQUEST['oSupriArquivo'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
$voSupriScm = $_REQUEST['voSupriScm'];
$voSupriMapaFornecedor = $_REQUEST['voSupriMapaFornecedor'];
$voSupriMapaFornecedor = $_REQUEST['voSupriMapaFornecedor'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_arquivo - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriArquivo.preparaLista">Gerenciar supri_arquivos</a> &gt; <strong><?php echo $sOP?> supri_arquivo</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_arquivo</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriArquivo" action="?action=SupriArquivo.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodArquivo" value="<?=(is_object($oSupriArquivo)) ? $oSupriArquivo->getCodArquivo() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_arquivo">

 							<input type='hidden' name='fCodArquivo' value='<?= ($oSupriArquivo) ? $oSupriArquivo->getCodArquivo() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Nome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Nome' placeholder='Nome' name='fNome'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Arquivo">Binário:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Arquivo' placeholder='Binário' name='fArquivo'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getArquivo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Extensao">Extensão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Extensao' placeholder='Extensão' name='fExtensao'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getExtensao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyContratoDocTipo">Cod_tipo:</label>
					<div class="col-sm-10">
					<select name='fCodTipo'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyContratoDocTipo){
							   foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){
								   if($oSupriArquivo){
									   $sSelected = ($oSupriArquivo->getCodTipo() == $oMnyContratoDocTipo->getCodContratoDocTipo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>'><?= $oMnyContratoDocTipo->getCodContratoDocTipo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Tamanho">Tamanho:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Tamanho' placeholder='Tamanho' name='fTamanho'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getTamanho() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataHora">Data:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DataHora' placeholder='Data' name='fDataHora'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getDataHora() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="RealizadoPor">Realizado Por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='RealizadoPor' placeholder='Realizado Por' name='fRealizadoPor'   value='<?= ($oSupriArquivo) ? $oSupriArquivo->getRealizadoPor() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oSupriArquivo) ? $oSupriArquivo->getAtivo() : "1"?>'/>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriScm">Solicitação de Compra:</label>
					<div class="col-sm-10">
					<select name='fCodScm'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriScm){
							   foreach($voSupriScm as $oSupriScm){
								   if($oSupriArquivo){
									   $sSelected = ($oSupriArquivo->getCodScm() == $oSupriScm->getCodScm()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriScm->getCodScm()?>'><?= $oSupriScm->getCodScm()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriMapaFornecedor">MAPA:</label>
					<div class="col-sm-10">
					<select name='fCodMapa'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriMapaFornecedor){
							   foreach($voSupriMapaFornecedor as $oSupriMapaFornecedor){
								   if($oSupriArquivo){
									   $sSelected = ($oSupriArquivo->getCodMapa() == $oSupriMapaFornecedor->getCodMapa()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriMapaFornecedor->getCodMapa()?>'><?= $oSupriMapaFornecedor->getCodMapa()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriMapaFornecedor">Fornecedor:</label>
					<div class="col-sm-10">
					<select name='fCodFornecedor'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriMapaFornecedor){
							   foreach($voSupriMapaFornecedor as $oSupriMapaFornecedor){
								   if($oSupriArquivo){
									   $sSelected = ($oSupriArquivo->getCodFornecedor() == $oSupriMapaFornecedor->getCodFornecedor()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriMapaFornecedor->getCodFornecedor()?>'><?= $oSupriMapaFornecedor->getCodFornecedor()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
