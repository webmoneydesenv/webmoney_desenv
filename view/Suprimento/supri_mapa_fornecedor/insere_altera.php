<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriMapaFornecedor = $_REQUEST['oSupriMapaFornecedor'];
 $voSupriMapaCompras = $_REQUEST['voSupriMapaCompras'];
$voMnyPessoaGeral = $_REQUEST['voMnyPessoaGeral'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_mapa_fornecedor - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriMapaFornecedor.preparaLista">Gerenciar supri_mapa_fornecedors</a> &gt; <strong><?php echo $sOP?> supri_mapa_fornecedor</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_mapa_fornecedor</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriMapaFornecedor" action="?action=SupriMapaFornecedor.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodMapa" value="<?=(is_object($oSupriMapaFornecedor)) ? $oSupriMapaFornecedor->getCodMapa() : ""?>" />
			<input type="hidden" name="fCodFornecedor" value="<?=(is_object($oSupriMapaFornecedor)) ? $oSupriMapaFornecedor->getCodFornecedor() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_mapa_fornecedor">

 							<input type='hidden' name='fCodMapa' value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getCodMapa() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaGeral">Fornecedor:</label>
					<div class="col-sm-10">
					<select name='fCodFornecedor'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPessoaGeral){
							   foreach($voMnyPessoaGeral as $oMnyPessoaGeral){
								   if($oSupriMapaFornecedor){
									   $sSelected = ($oSupriMapaFornecedor->getCodFornecedor() == $oMnyPessoaGeral->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPessoaGeral->getPesgCodigo()?>'><?= $oMnyPessoaGeral->getPesgCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Contato">Contato:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Contato' placeholder='Contato' name='fContato'   value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getContato() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Fone">Fone:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Fone' placeholder='Fone' name='fFone'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CondicaoPagamento">Condição de Pagamento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CondicaoPagamento' placeholder='Condição de Pagamento' name='fCondicaoPagamento'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getCondicaoPagamento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DescPercentual">Desconto Percentual:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DescPercentual' placeholder='Desconto Percentual' name='fDescPercentual'   value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getDescPercentualFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DescValor">Desconto Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DescValor' placeholder='Desconto Valor' name='fDescValor'   value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getDescValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="FreteCifFob">Frete CIF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='FreteCifFob' placeholder='Frete CIF' name='fFreteCifFob'   value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getFreteCifFobFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Obs">Observação :</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Obs' placeholder='Observação ' name='fObs'   value='<?= ($oSupriMapaFornecedor) ? $oSupriMapaFornecedor->getObs() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#DescPercentual").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#DescValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#FreteCifFob").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
