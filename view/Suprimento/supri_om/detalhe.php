﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOm = $_REQUEST['oSupriOm'];
 //print_r($oSupriOm);
 //die();
 $voSupriOmItem =  $_REQUEST['voSupriOmItem'];
 $oSupriMapaCompras = $_REQUEST['oSupriMapaCompras'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Orçamento - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOm.preparaLista">Gerenciar Orçamento</a> &gt; <strong><?php echo $sOP?> Orçamento</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Orçamento</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getSysEmpresa()->getEmpFantasia() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Comprador:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getAcessoUsuario()->getLogin() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Endereço de Entrega:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getEntregaEndereco() : ""?></div>
				</div>
				<br>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Contato para entrega:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getEntregaContato() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Fone para entrega:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getEntregaFone() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Fornecedor:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getCodFornecedor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Resposta:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getRespostaFormatado() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Fechamento:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getFechamentoFormatado() : ""?></div>

					 <div class="col-sm-4" style="text-align:left"><strong>Previsão:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getPrevisaoFormatado() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-4" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getObservacao() : ""?></div>
				</div>
				<br>
 <div class="form-group">
     <div class="col-sm-4" style="text-align:left"><strong>Nº Cotação:&nbsp;</strong><a href="#"><?= ($oSupriMapaCompras) ? $oSupriMapaCompras->getCodMapa() : ""?></a></div>
				</div>
				<br>


               <div class="form-group">
                            <div class="col-sm-12">
                            <legend>Produtos</legend>


                       <table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <?php if(is_array($voSupriOmItem)){?>
                            <thead>
                            <tr>
                                <th class='Titulo'>Nº Solicitação</th>
                                <th class='Titulo'>Produto</th>
                                <th class='Titulo'>Qtde Orçada</th>
                            </tr>
                            </thead>
                            <tbody>
                               <?php foreach($voSupriOmItem as $oSupriOmItem){ ?>
                            <tr>
                                <td><a href="?action=SupriScm.preparaFormulario&sOP=Detalhar&nIdSupriScm=<?=$oSupriOmItem->cod_scm?>"><?=$oSupriOmItem->numero_solicitacao?></a></td>
                                <td><a href="?action=SupriOmItem.preparaFormulario&sOP=Detalhar&nIdSupriOmItem=<?=$oSupriOmItem->cod_om."||".$oSupriOmItem->om_item?>" target="_blank"><?=$oSupriOmItem->descricao?></a></td>
                                <td><?=$oSupriOmItem->qtde?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td></td>
                            </tr>
                            </tfoot>
                        <?php }//if(count($voSupriArquivo)){?>
                        </table>
                         </div>
                </div>


         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=SupriOm.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
            		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
