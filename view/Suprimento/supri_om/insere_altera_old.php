<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOm = $_REQUEST['oSupriOm'];
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];

$voVPessoaGeralFormatada = $_REQUEST['voVPessoaGeralFormatada'];

$voSysEmpresa = $_REQUEST['voSysEmpresa'];

$voSupriScm = $_REQUEST['voSupriScm'];
$voSupriScmItem = $_REQUEST['voSupriScmItem'];

/*
print_r(count($voSupriScmItem));
die();
*/

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_om - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

    <style>
        .pickListButtons {
          padding: 10px;
          text-align: center;
        }

        .pickListButtons button {
          margin-bottom: 5px;
        }

        .pickListSelect {
          height: 200px !important;
        }

        .botao{
            padding-bottom: 10px;
        }

        .overlay {
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background: rgba(70,20,15,0.3);
            z-index: 2;
            background-image: url(https://i.stack.imgur.com/BNGOI.gif);
            background-color: white;
            opacity: 0.5;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: 100px;
        }

     </style>

 </head>
 <body>
 <div class="container">
<div id="teste" ></div>
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOm.preparaLista">Gerenciar supri_oms</a> &gt; <strong><?php echo $sOP?> supri_om</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Orçamento</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriOm" action="?action=SupriOm.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodOm" id='codOm' value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodOm() : ""?>" />
         <input type="hidden" name="fEmpCodigo" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getEmpCodigo() : ""?>" />
         <input type="hidden" name="fCodPessoaComprador" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodPessoaComprador() : ""?>" />
         <input type='hidden' name='fCodOm' value='<?= ($oSupriOm) ? $oSupriOm->getCodOm() : ""?>'/>


         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_om">
 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaEndereco">Endereço de Entrega:</label>
					<div class="col-sm-10">
					<input class="form-control" type='text' id='EntregaEndereco' placeholder='Endereço de Entrega' name='fEntregaEndereco'   value='<?= ($oSupriOm) ? $oSupriOm->getEntregaEndereco() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaContato">Contato para entrega:</label>
					<div class="col-sm-4">
					<input class="form-control" type='text' id='EntregaContato' placeholder='Contato para entrega' name='fEntregaContato'   value='<?= ($oSupriOm) ? $oSupriOm->getEntregaContato() : ""?>'/>
				</div>

					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaFone">Fone para entrega:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='EntregaFone' placeholder='Fone para entrega' name='fEntregaFone'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriOm) ? $oSupriOm->getEntregaFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaGeral">Fornecedor:</label>
					<div class="col-sm-10">
					<select name='fCodFornecedor'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voVPessoaGeralFormatada){
							   foreach($voVPessoaGeralFormatada as $oVPessoaGeralFormatada){
								   if($oSupriOm){
									   $sSelected = ($oSupriOm->getCodFornecedor() == $oVPessoaGeralFormatada->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oVPessoaGeralFormatada->getPesgCodigo()?>'><?= $oVPessoaGeralFormatada->getNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>



 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Previsao">Previsão:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Previsao' placeholder='Previsão' name='fPrevisao'   value='<?= ($oSupriOm) ? $oSupriOm->getPrevisaoFormatado() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Resposta">Resposta:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Resposta' placeholder='Resposta' name='fResposta'   value='<?= ($oSupriOm) ? $oSupriOm->getRespostaFormatado() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Fechamento">Fechamento:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Fechamento' placeholder='Fechamento' name='fFechamento'   value='<?= ($oSupriOm) ? $oSupriOm->getFechamentoFormatado() : ""?>'/>
				</div>
				</div>
				<br>


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Observação:</label>
					<div class="col-sm-10">
					<textarea class="form-control"  id='Observacao' placeholder='Observação' name='fObservacao'    value='<?= ($oSupriOm) ? $oSupriOm->getObservacao() : ""?>'/></textarea>
				</div>
				</div>
				<br>


             <br>

             <? if($_REQUEST['sOP'] == "Alterar"){?>

           <div class="form-group">
              <label class="col-sm-2 control-label" style="text-align:left" for="Solicitacoes">Solicitação:</label>
             <div class="col-sm-4">
                <? if($voSupriScm){?>
                 <!--<select class="form-control chosen" onchange="recuperaConteudoDinamico('index.php','action=SupriOm.preparaProdutosSolicitacao&nCodScm='+this.value,'divResult');">-->
                 <!--<select class="form-control chosen" onchange="this.form.submit();">                                      -->
                 <select class="form-control chosen" onchange="recuperaProduto(this.value)">
                      <option>Selecione</option>
                      <? foreach($voSupriScm as $oSupriScm){?>
                     <option value="<?= $oSupriScm->getCodScm()?>"><?= $oSupriScm->getNumeroSolicitacao()?></option>
                      <? }?>
                 </select>
                <? }?>

              </div>
           </div>



           <div class="form-group">
              <div class="col-sm-12" >
                     <div class="container">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">Lista de Produtos</h3>
                        </div>
                        <div class="panel-body">
                          <div id="pickList"></div>

                          <br>
                          <br>
                          <input type="button" class="btn btn-primary" id="getSelected" value="Gravar itens selecionados">
                        </div>
                      </div>
                    </div>
               </div>
             </div>

            <div class="form-group" >
                     <div class="col-sm-12" style="background-color:#E9F2F9;">
                     <label class="col-sm-1 control-label" style="text-align:left" for="Especificacao">Opções</label>
                     <label class="col-sm-3 control-label" style="text-align:left" for="Rev">Numero Solicitação</label>
                     <label class="col-sm-3 control-label" style="text-align:left" for="Quantidade">Prduto</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Quantidade">Qtde Solicitada</label>
                     <label class="col-sm-1 control-label" style="text-align:left" for="Quantidade">Qtde Orçada</label>
                     <label class="col-sm-3 control-label" style="text-align:left" for="Projeto"></label>


                     </div>
             </div>


             <div id='divTeste'> </div>

             <? } ?>

         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>

         <form method="post" class="form-horizontal" name="formProduto" id="formProduto" action="?action=SupriOm.preparaFormulario&sOP=Alterar">
             <input type='hidden' value="Alterar" name="sOP">
             <input type="hidden" name="fIdSupriOm" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodOm() : ""?>" />
         </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
            function recuperaProduto($nCodProduto){
                var nCodOrcamento = document.getElementById('codOm').value;
                window.location.replace("?action=SupriOm.preparaFormulario&sOP=Alterar&nIdSupriOm="+nCodOrcamento+"&nCodProduto="+$nCodProduto);
                //document.getElementById('formProduto').submit()
            }

 		</script>
 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#Resposta").mask("99/99/9999");
			$("#Fechamento").mask("99/99/9999");
			$("#Previsao").mask("99/99/9999");
			$("#EntregaFone").mask("99/99/9999");
				 });
 		</script>
         <script type="text/javascript" language="javascript">
             (function($) {

                  $.fn.pickList = function(options) {

                    var opts = $.extend({}, $.fn.pickList.defaults, options);

                    this.fill = function() {
                      var option = '';

                      $.each(opts.data, function(key, val) {
                        option += '<option data-id=' + val.id + '>' + val.text + '</option>';
                      });
                      this.find('.pickData').append(option);
                    };
                    this.controll = function() {
                      var pickThis = this;

                      pickThis.find(".pAdd").on('click', function() {
                        var p = pickThis.find(".pickData option:selected");
                        p.clone().appendTo(pickThis.find(".pickListResult"));
                        p.remove();
                      });

                      pickThis.find(".pAddAll").on('click', function() {
                        var p = pickThis.find(".pickData option");
                        p.clone().appendTo(pickThis.find(".pickListResult"));
                        p.remove();
                      });

                      pickThis.find(".pRemove").on('click', function() {
                        var p = pickThis.find(".pickListResult option:selected");
                        p.clone().appendTo(pickThis.find(".pickData"));
                        p.remove();
                      });

                      pickThis.find(".pRemoveAll").on('click', function() {
                        var p = pickThis.find(".pickListResult option");
                        p.clone().appendTo(pickThis.find(".pickData"));
                        p.remove();
                      });
                      pickThis.find(".pRemoveInsert").on('click', function() {
                        var p = pickThis.find(".pickListResult option");
                        p.remove();
                      });
                    };

                    this.getValues = function() {
                      var objResult = [];
                      this.find(".pickListResult option").each(function() {
                        objResult.push({
                          id: $(this).data('id'),
                          text: this.text
                        });
                      });
                      return objResult;
                    };

                    this.init = function() {
                      var pickListHtml =
                        "<div class='row'>" +
                        "<div class='col-sm-7'>" +
                        " <label class='col-sm-2 control-label' style='text-align:left' for='Itens Solicitacoes'>SOLICITAÇÃO:</label>" +
                        "</div>" +
                        "<div class='col-sm-5'>" +
                        " <label class='col-sm-2 control-label' style='text-align:left' for='Itens Solicitacoes'>ORÇAMENTO:</label>" +
                        "</div>" +
                        "</div>" +
                        "<div class='row'>" +
                        "  <div class='col-sm-5' id='Produtos'>" +
                        "	 <select class='form-control pickListSelect pickData' name='todos[]' multiple></select>" +
                        " </div>" +
                        " <div class='col-sm-2 pickListButtons'>" +
                        " <div class='row botao'>" +
                        "	<input type='button'  class='pAdd btn btn-primary btn-sm' value='Adicionar'>" +
                        " </div>" +
                        " <div class='row botao'>" +
                        "      <input type='button'  class='pAddAll btn btn-primary btn-sm' value='Adicionar Todos'>" +
                        " </div>" +
                        " <div class='row botao'>" +
                        "	<input type='button'  class='pRemove btn btn-primary btn-sm' value='Remover'>" +
                        " </div>" +
                        " <div class='row botao'>" +
                        "	<input type='button'  class='pRemoveAll btn btn-primary btn-sm' value='Remover Todos'>" +
                        " </div>" +
                        " <div class='row botao' style='display:none;'>" +
                        "	<input type='button'  class='pRemoveInsert btn btn-primary btn-sm' value='Remover Inseridos'>" +
                        " </div>" +
                        " </div>" +
                        " <div class='col-sm-5'>" +
                        "    <select class='form-control pickListSelect pickListResult' name='fItem[]' multiple></select>" +
                        " </div>" +
                        "</div>";

                      this.append(pickListHtml);

                      this.fill();
                      this.controll();
                    };

                    this.init();
                    return this;
                  };

                  $.fn.pickList.defaults = {
                    add: 'Add',
                    addAll: 'Add All',
                    remove: 'Remove',
                    removeAll: 'Remove All'
                  };


                }(jQuery));



                var val = {

                    <?
                            $nTotal = count($voSupriScmItem);
                            $i = 1;
                            foreach($voSupriScmItem as $oSupriScmItem){
                                if($i != $nTotal){
                                       echo  $oSupriScmItem->cod_scm.".".$oSupriScmItem->cod_scm_item.": {
                                        id: ".$oSupriScmItem->cod_scm.".".$oSupriScmItem->cod_scm_item .",
                                        text: '"."  ITEM:  " . $oSupriScmItem->descricao."   QTDE: ". $oSupriScmItem->qtde   ."'
                                      },";
                                }else{
                                      echo  $oSupriScmItem->cod_scm.".".$oSupriScmItem->cod_scm_item.": {
                                        id: ".$oSupriScmItem->cod_scm.".".$oSupriScmItem->cod_scm_item .",
                                        text: '"."  ITEM:  ". $oSupriScmItem->descricao."    QTDE: ". $oSupriScmItem->qtde   ."'
                                      }";
                                }
                                $i++;
                            }

                    ?>
                };

                var pick = $("#pickList").pickList({
                  data: val
                });

                $("#getSelected").click(function() {
                    var v =  pick.getValues();
                    var itens = [];
                    for (var i in v) {

                      //console.log(v[i].id);
                      itens[i] = v[i].id;

                    }

                    $.ajax({
                            dataType: "html",
                            type: "GET",
                            beforeSend: function(oXMLrequest){
                                        document.getElementById('teste').innerHTML ="<div id='test' class='overlay'></div>";
                                    },
                            url: "?action=SupriOmItem.preparaItem&Teste="+itens,
                            error: function(oXMLRequest,sErrorType){
                                                    alert(oXMLRequest.responseText);
                                                    alert(oXMLRequest.status+' , '+sErrorType);
                                               },
                            success: function(data){
                                             document.getElementById('divTeste').innerHTML = data;
                                             document.getElementById('teste').innerHTML ="<div id='teste'></div>";
                            },
                            complete:function(){
                                var btn = $('.pRemoveInsert');
                                btn.click();

                            }
                    });


                });


             function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

                oDiv  = document.getElementById(sIdDivInsert);
                oDiv2 = document.getElementById('teste');
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                    oDiv2.innerHTML ="<div id='test' class='overlay'></div>";
                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         oDiv.innerHTML = data;
                                        oDiv2.innerHTML ="<div id='teste'></div>";
                        },
                        complete:function(){
                         jQuery(function($){


                                 });
                        }
                });
            }



         </script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
