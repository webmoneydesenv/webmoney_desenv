<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOm = $_REQUEST['oSupriOm'];
 $voAcessoUsuario = $_REQUEST['voAcessoUsuario'];

$voVPessoaGeralFormatada = $_REQUEST['voVPessoaGeralFormatada'];

$voSysEmpresa = $_REQUEST['voSysEmpresa'];

//$voSupriScm = $_REQUEST['voSupriScm'];

$voSupriScmItem = $_REQUEST['voSupriScmItem'];
$oSupriScm = $_REQUEST['oSupriScm'];
$voSupriScm = $_REQUEST['voSupriScm'];
$voSupriOmItem = $_REQUEST['voSupriOmItem']
/*
print_r(count($voSupriScmItem));
die();
*/

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Orçamento- <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

    <style>
        .pickListButtons {
          padding: 10px;
          text-align: center;
        }

        .pickListButtons button {
          margin-bottom: 5px;
        }

        .pickListSelect {
          height: 200px !important;
        }

        .botao{
            padding-bottom: 10px;
        }

        .overlay {
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background: rgba(70,20,15,0.3);
            z-index: 2;
            background-image: url(https://i.stack.imgur.com/BNGOI.gif);
            background-color: white;
            opacity: 0.5;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: 100px;
        }

     </style>

 </head>
 <body>
 <div class="container">
<div id="teste" ></div>
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOm.preparaLista">Gerenciar Orçamento</a> &gt; <strong><?php echo $sOP?> Orçamento</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Orçamento</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriOm" action="?action=SupriOm.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodOm" id='codOm' value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodOm() : ""?>" />
         <input type="hidden" name="fEmpCodigo" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getEmpCodigo() : ""?>" />
         <input type="hidden" name="fCodPessoaComprador" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodPessoaComprador() : ""?>" />
         <input type='hidden' name='fCodOm' value='<?= ($oSupriOm) ? $oSupriOm->getCodOm() : ""?>'/>


         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_om">
 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaEndereco">Endereço de Entrega:</label>
					<div class="col-sm-10">
					<input class="form-control" type='text' id='EntregaEndereco' placeholder='Endereço de Entrega' name='fEntregaEndereco'   value='<?= ($oSupriOm) ? $oSupriOm->getEntregaEndereco() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaContato">Contato para entrega:</label>
					<div class="col-sm-4">
					<input class="form-control" type='text' id='EntregaContato' placeholder='Contato para entrega' name='fEntregaContato'   value='<?= ($oSupriOm) ? $oSupriOm->getEntregaContato() : ""?>'/>
				</div>

					<label class="col-sm-2 control-label" style="text-align:left" for="EntregaFone">Fone para entrega:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='EntregaFone' placeholder='Fone para entrega' name='fEntregaFone'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriOm) ? $oSupriOm->getEntregaFone() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaGeral">Fornecedor:</label>
					<div class="col-sm-10">
					<select name='fCodFornecedor'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voVPessoaGeralFormatada){
							   foreach($voVPessoaGeralFormatada as $oVPessoaGeralFormatada){
								   if($oSupriOm){
									   $sSelected = ($oSupriOm->getCodFornecedor() == $oVPessoaGeralFormatada->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oVPessoaGeralFormatada->getPesgCodigo()?>'><?= $oVPessoaGeralFormatada->getNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>



 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Previsao">Previsão:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Previsao' placeholder='Previsão' name='fPrevisao'   value='<?= ($oSupriOm) ? $oSupriOm->getPrevisaoFormatado() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Resposta">Resposta:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Resposta' placeholder='Resposta' name='fResposta'   value='<?= ($oSupriOm) ? $oSupriOm->getRespostaFormatado() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Fechamento">Fechamento:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='Fechamento' placeholder='Fechamento' name='fFechamento'   value='<?= ($oSupriOm) ? $oSupriOm->getFechamentoFormatado() : ""?>'/>
				</div>
				</div>
				<br>


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Observação:</label>
					<div class="col-sm-10">
					<textarea class="form-control"  id='Observacao' placeholder='Observação' name='fObservacao'    value='<?= ($oSupriOm) ? $oSupriOm->getObservacao() : ""?>'/><?= ($oSupriOm) ? $oSupriOm->getObservacao() : ""?></textarea>
				</div>
				</div>
				<br>


             <br>

             <?// if($_REQUEST['sOP'] == "Alterar"){?>

                   <div class="form-group">
                      <label class="col-sm-2 control-label" style="text-align:left" for="Solicitacoes">Solicitação:</label>
                     <div class="col-sm-4">
                        <? if($voSupriScm){?>
                         <!--<select class="form-control chosen" onchange="recuperaConteudoDinamico('index.php','action=SupriOm.preparaProdutosSolicitacao&nCodScm='+this.value,'divResult');">-->
                         <!--<select class="form-control chosen" onchange="this.form.submit();">                                      -->
                         <select class="form-control chosen" id='SupriScm'>
                              <option>Selecione</option>
                              <? foreach($voSupriScm as $oSupriScm){?>
                             <option value="<?= $oSupriScm->getCodScm()?>"><?= $oSupriScm->getNumeroSolicitacao()?></option>
                              <? }?>
                         </select>
                        <? }?>

                      </div>
                      <input type='button' class='btn-primary' value='Pesquisar' onclick="recuperaProduto()">
                   </div>

             <?// } ?>

             <hr>

              <? include_once("view/Suprimento/supri_om/produtos.php"); ?>

         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>

         <form method="post" class="form-horizontal" name="formProduto" id="formProduto" action="?action=SupriOm.preparaFormulario&sOP=Alterar">
             <input type='hidden' value="Alterar" name="sOP">
             <input type="hidden" name="fIdSupriOm" value="<?=(is_object($oSupriOm)) ? $oSupriOm->getCodOm() : ""?>" />
         </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
            function recuperaProduto(){
                var nCodOrcamento = document.getElementById('codOm').value;
                var nCodScm = document.getElementById('SupriScm').value;
                //window.location.replace("?action=SupriOm.preparaFormulario&sOP=Alterar&nIdSupriOm="+nCodOrcamento+"&nCodScm="+nCodScm);
                //document.getElementById('formProduto').submit()
                recuperaConteudoDinamico('index.php','action=SupriOm.preparaProdutosSolicitacao&nCodScm='+nCodScm,'divProdScm');
                return false;
            }

 		</script>
 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#Resposta").mask("99/99/9999");
			$("#Fechamento").mask("99/99/9999");
			$("#Previsao").mask("99/99/9999");
			$("#EntregaFone").mask("(99) 99999-9999");
             });



            function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                   // oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                    document.getElementById('teste').innerHTML ="<div id='test' class='overlay'></div>";
                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         oDiv.innerHTML = data;
                                         document.getElementById('teste').innerHTML ="<div id='teste'></div>";
                        },
                        complete:function(){
                         jQuery(function($){
                                    $("#MovDataPrev").mask("99/99/9999");
                                    $('.mascaraMoeda').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    //usados no modal relatorio...
                                    $("#DataInicial").mask("99/99/9999");
                                    $("#DataFinal").mask("99/99/9999");
                                    $("#Competencia").mask("99/9999");

                                    $("#ValorVale").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    $("#ValorDocPendente").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    $('.chosen').chosen();

                                 });
                        }
                });
            }



 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
