﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOm = $_REQUEST['oSupriOm'];
 $oSupriScm = $_REQUEST['oSupriScm'];
 $oSupriScmItem= $_REQUEST['oSupriScmItem'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Detalhe do Processo</title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

     <style>

            /*body{margin:40px;}*/

            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 100%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;

            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
              width: 30px;
              height: 30px;
              text-align: center;
              padding: 6px 0;
              font-size: 12px;
              line-height: 1.428571429;
              border-radius: 15px;
            }

     </style>


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <strong> Detalhe do Processo</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Detalhe do Processo de Compra</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Solicitação:&nbsp;</strong><?= ($oSupriScm) ? $oSupriScm->getNumeroSolicitacao() : ""?></div>
			</div>
			<br>
			<br>
            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Produto:&nbsp;</strong><?= $oSupriScmItem->descricao ?></div>
			</div>
			<br>

            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Quantidade:&nbsp;</strong><?= $oSupriScmItem->qtde ?></div>
			</div>
			<br>
			<br>
			<br>


             <div class="form-group">
               <legend>Hist&oacute;rico de Tramita&ccedil;&otilde;es</legend>
             </div>
             <div class="form-group" style='padding-bottom: 100px;'>
                 <div class="row" style="background:#CCC;margin-right:0px; margin-left:0px;padding:10px">
                    <div class="col-sm-1"><strong>Detalhar</strong></div>
                    <div class="col-sm-1"><strong>DATA</strong></div>
                    <div class="col-sm-4"><strong>DE</strong></div>
                    <div class="col-sm-4"><strong>PARA</strong></div>
                    <div class="col-sm-2"><strong>QUEM</strong></div>
                 </div>
             </div>
             <div class="form-group">
                 <div class="col-sm-12">
                    <div class="stepwizard">
                        <div class="stepwizard-row">
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle">1</button>
                                <p>Adm. Obra</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle">2</button>
                                <p>Chefe de Obra</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-primary btn-circle" disabled="disabled">3</button>
                                <p>Suprimento</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>
                                <p>Diretoria Financeira</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled">5</button>
                                <p>Suprimento</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled" style="background:#A6D34A;"></button>
                                <p>Finalizado</p>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>



                <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oSupriOm) ? $oSupriOm->getEmpCodigo() : ""?></div>
				</div>
				<br>

          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=SupriOm.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
