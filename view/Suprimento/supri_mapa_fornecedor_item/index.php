<?php
 $voSupriMapaFornecedorItem = $_REQUEST['voSupriMapaFornecedorItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>LISTA DE supri_mapa_fornecedor_items</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=SupriMapaFornecedorItem.preparaLista">Gerenciar supri_mapa_fornecedor_items</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar supri_mapa_fornecedor_items</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formSupriMapaFornecedorItem" id="formSupriMapaFornecedorItem" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes" name="acoes" id="acoesSupriMapaFornecedorItem" onChange="JavaScript: submeteForm('SupriMapaFornecedorItem')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=SupriMapaFornecedorItem.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo supri_mapa_fornecedor_item</option>
   						<option value="?action=SupriMapaFornecedorItem.preparaFormulario&sOP=Alterar" lang="1">Alterar supri_mapa_fornecedor_item selecionado</option>
   						<option value="?action=SupriMapaFornecedorItem.preparaFormulario&sOP=Detalhar" lang="1">Detalhar supri_mapa_fornecedor_item selecionado</option>
   						<option value="?action=SupriMapaFornecedorItem.processaFormulario&sOP=Excluir" lang="2">Excluir supri_mapa_fornecedor_item(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voSupriMapaFornecedorItem)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('SupriMapaFornecedorItem')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Fornecedor</th>
					<th class='Titulo'>Orçamento</th>
					<th class='Titulo'>Orçamento item</th>
					<th class='Titulo'>Valor origem</th>
					<th class='Titulo'>Dif aliquota</th>
					<th class='Titulo'>Dif valor</th>
					<th class='Titulo'>Valor final</th>
					<th class='Titulo'>Observação</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voSupriMapaFornecedorItem as $oSupriMapaFornecedorItem){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('SupriMapaFornecedorItem')" type="checkbox" value="<?=$oSupriMapaFornecedorItem->getCodMapa()?>||<?=$oSupriMapaFornecedorItem->getCodFornecedor()?>||<?=$oSupriMapaFornecedorItem->getCodOm()?>||<?=$oSupriMapaFornecedorItem->getCodOmItem()?>" name="fIdSupriMapaFornecedorItem[]"/></td>
  					<td><?= $oSupriMapaFornecedorItem->getCodMapa()?></td>
					<td><?= $oSupriMapaFornecedorItem->getCodFornecedor()?></td>
					<td><?= $oSupriMapaFornecedorItem->getCodOm()?></td>
					<td><?= $oSupriMapaFornecedorItem->getCodOmItem()?></td>
					<td><?= $oSupriMapaFornecedorItem->getValorOrigemFormatado()?></td>
					<td><?= $oSupriMapaFornecedorItem->getDifAliquotaFormatado()?></td>
					<td><?= $oSupriMapaFornecedorItem->getDifValorFormatado()?></td>
					<td><?= $oSupriMapaFornecedorItem->getValorFinalFormatado()?></td>
					<td><?= $oSupriMapaFornecedorItem->getObs()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voSupriMapaFornecedorItem)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('SupriMapaFornecedorItem');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
