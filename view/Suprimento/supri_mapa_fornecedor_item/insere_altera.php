<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriMapaFornecedorItem = $_REQUEST['oSupriMapaFornecedorItem'];
 $voSupriMapaCompras = $_REQUEST['voSupriMapaCompras'];
$voMnyPessoaGeral = $_REQUEST['voMnyPessoaGeral'];
$voSupriOmItem = $_REQUEST['voSupriOmItem'];
$voSupriOmItem = $_REQUEST['voSupriOmItem'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_mapa_fornecedor_item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriMapaFornecedorItem.preparaLista">Gerenciar supri_mapa_fornecedor_items</a> &gt; <strong><?php echo $sOP?> supri_mapa_fornecedor_item</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_mapa_fornecedor_item</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriMapaFornecedorItem" action="?action=SupriMapaFornecedorItem.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodMapa" value="<?=(is_object($oSupriMapaFornecedorItem)) ? $oSupriMapaFornecedorItem->getCodMapa() : ""?>" />
			<input type="hidden" name="fCodFornecedor" value="<?=(is_object($oSupriMapaFornecedorItem)) ? $oSupriMapaFornecedorItem->getCodFornecedor() : ""?>" />
			<input type="hidden" name="fCodOm" value="<?=(is_object($oSupriMapaFornecedorItem)) ? $oSupriMapaFornecedorItem->getCodOm() : ""?>" />
			<input type="hidden" name="fCodOmItem" value="<?=(is_object($oSupriMapaFornecedorItem)) ? $oSupriMapaFornecedorItem->getCodOmItem() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_mapa_fornecedor_item">


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriMapaCompras">Codigo:</label>
					<div class="col-sm-10">
					<select name='fCodMapa'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriMapaCompras){
							   foreach($voSupriMapaCompras as $oSupriMapaCompras){
								   if($oSupriMapaFornecedorItem){
									   $sSelected = ($oSupriMapaFornecedorItem->getCodMapa() == $oSupriMapaCompras->getCodMapa()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriMapaCompras->getCodMapa()?>'><?= $oSupriMapaCompras->getCodMapa()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaGeral">Fornecedor:</label>
					<div class="col-sm-10">
					<select name='fCodFornecedor'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPessoaGeral){
							   foreach($voMnyPessoaGeral as $oMnyPessoaGeral){
								   if($oSupriMapaFornecedorItem){
									   $sSelected = ($oSupriMapaFornecedorItem->getCodFornecedor() == $oMnyPessoaGeral->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPessoaGeral->getPesgCodigo()?>'><?= $oMnyPessoaGeral->getPesgCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriOmItem">Orçamento:</label>
					<div class="col-sm-10">
					<select name='fCodOm'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriOmItem){
							   foreach($voSupriOmItem as $oSupriOmItem){
								   if($oSupriMapaFornecedorItem){
									   $sSelected = ($oSupriMapaFornecedorItem->getCodOm() == $oSupriOmItem->getCodOm()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriOmItem->getCodOm()?>'><?= $oSupriOmItem->getCodOm()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriOmItem">Orçamento Item:</label>
					<div class="col-sm-10">
					<select name='fCodOmItem'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriOmItem){
							   foreach($voSupriOmItem as $oSupriOmItem){
								   if($oSupriMapaFornecedorItem){
									   $sSelected = ($oSupriMapaFornecedorItem->getCodOmItem() == $oSupriOmItem->getCodOmItem()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriOmItem->getCodOmItem()?>'><?= $oSupriOmItem->getCodOmItem()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ValorOrigem">Valor Origem:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ValorOrigem' placeholder='Valor Origem' name='fValorOrigem'   value='<?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getValorOrigemFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DifAliquota">DIF Aliquota:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DifAliquota' placeholder='DIF Aliquota' name='fDifAliquota'   value='<?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getDifAliquotaFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DifValor">DIF Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='DifValor' placeholder='DIF Valor' name='fDifValor'   value='<?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getDifValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ValorFinal">Valor Final:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ValorFinal' placeholder='Valor Final' name='fValorFinal'   value='<?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getValorFinalFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Obs">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Obs' placeholder='Observação' name='fObs'   value='<?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getObs() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#ValorOrigem").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#DifAliquota").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#DifValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#ValorFinal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
