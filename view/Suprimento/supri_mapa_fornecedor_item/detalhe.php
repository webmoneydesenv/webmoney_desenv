﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriMapaFornecedorItem = $_REQUEST['oSupriMapaFornecedorItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_mapa_fornecedor_item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriMapaFornecedorItem.preparaLista">Gerenciar supri_mapa_fornecedor_items</a> &gt; <strong><?php echo $sOP?> supri_mapa_fornecedor_item</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_mapa_fornecedor_item</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Codigo:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getCodMapa() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Fornecedor:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getCodFornecedor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Orçamento:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getCodOm() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Orçamento Item:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getCodOmItem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Origem:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getValorOrigem() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>DIF Aliquota:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getDifAliquota() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>DIF Valor:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getDifValor() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Valor Final:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getValorFinal() : ""?></div>
				</div>
				<br>

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Observação:&nbsp;</strong><?= ($oSupriMapaFornecedorItem) ? $oSupriMapaFornecedorItem->getObs() : ""?></div>
				</div>
				<br>

         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=SupriMapaFornecedorItem.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
