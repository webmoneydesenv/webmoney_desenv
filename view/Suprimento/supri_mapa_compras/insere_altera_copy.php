<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriMapaCompras = $_REQUEST['oSupriMapaCompras'];
 $voSupriOm = $_REQUEST['voSupriOm'];

 $oSupriOm = $_REQUEST['oSupriOm'];
 $voSupriOmItem = $_REQUEST['voSupriOmItem'];
 $voVPessoaGeralFormatada = $_REQUEST['voVPessoaGeralFormatada'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Mapa de Compras - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

  <style>
      .nav-tabs {border-bottom: 1px solid #e9e9e9 !important;}

      .overlay {
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background: rgba(70,20,15,0.3);
            z-index: 2;
            background-image: url(https://i.stack.imgur.com/BNGOI.gif);
            background-color: white;
            opacity: 0.5;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: 100px;
      }

      .btn_exlcuir{
        font-size: 15px;
        color: #f8f3f3;
        background: #C9302C;
        border-radius: 20px;
        padding: 10px;
      }

  </style>

 </head>
 <body>
 <div class="container">
 <div id="teste" ></div>
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriMapaCompras.preparaLista">Gerenciar Mapa de Compras</a> &gt; <strong><?php echo $sOP?> Mapa de Compras</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Mapa de Análise de Compras <?=  ($oSupriOm)? "para o orçamento: ".$oSupriOm->getCodOm() : ""?></h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
          <form method="post" class="form-horizontal" name="formSupriOm" id="formSupriOm" action="?action=SupriMapaCompras.preparaOm">
               <input type="hidden" name="fCodOm" id='codOm' value="" />
         </form>
       <form method="post" class="form-horizontal" name="formSupriMapaCompras" action="?action=SupriMapaCompras.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodMapa" value="<?=(is_object($oSupriMapaCompras)) ? $oSupriMapaCompras->getCodMapa() : ""?>" />
         <input type='hidden' name='fAprovadorPor' value='<?= ($oSupriMapaCompras) ? $oSupriMapaCompras->getAprovadorPor() : ""?>'/>
         <input type='hidden' name='fDataFechamento'  value='<?= ($oSupriMapaCompras) ? $oSupriMapaCompras->getDataFechamentoFormatado() : ""?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_mapa_compras">

 		 <input type='hidden' name='fCodMapa' value='<?= ($oSupriMapaCompras) ? $oSupriMapaCompras->getCodMapa() : ""?>'/>

                <div class="form-group">
					<label class="col-sm-1  control-label" style="text-align:left" for="SupriOm">Orçamento:</label>
					<div class="col-sm-4">
					<!--<select name='fCodOm'  class="form-control chosen" onchange="recuperaConteudoDinamico('index.php','action=SupriMapaCompras.preparaOm&fCodOm='+this.value,'divOm');">-->
					<select name='fCodOm' id='fCodOm'  class="form-control chosen" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriOm){
							   foreach($voSupriOm as $oSupriOm1){
								   if($oSupriOm2){
									   $sSelected = ($oSupriOm1->getCodOm() == $oSupriOm1->getCodOm()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriOm1->getCodOm()?>'><?= $oSupriOm1->getCodOm()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                <div class="col-sm-2">
                    <input type='button' class="btn btn-primary" value='Buscar' onclick="preparaOm();">
                </div>
               </div>
  		       <br>

             <div id="divOm">&nbsp;</div>

    <ul class="nav nav-tabs">
    	<li class="active"><a href="#1" data-toggle="tab">Basica</a></li>
        <li><a href="#2" data-toggle="tab">Fornecedores</a></li>
    </ul>

    <div class="tab-content">
    	<div class="tab-pane active in" id="1" style="padding-bottom:100px;">

                    <legend style="padding: 30px;">Dados do Básicos</legend>
                   <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>
                                <div class="form-group">

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Custo:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name=''   value=''/>
                                    </div>

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">C. Custo:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name=''   value=''/>
                                    </div>

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Cotado Por:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name=''   value=''/>
                                    </div>

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Valor Aprovado:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text' id='ValorAprovado' placeholder='Valor Aprovado' name='fValorAprovado'   value='<?= ($oSupriMapaCompras) ? $oSupriMapaCompras->getValorAprovadoFormatado() : ""?>'/>
                                    </div>


                             </div>
                             <hr>
                            <div class="form-group">
                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Almoxarifado Origem:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name=''   value=''/>
                                    </div>

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Setor/Destino:</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name=''   value='' />
                                    </div>
                                    <label class="col-sm-1 control-label" style="text-align:left" for="">COMPRADOR</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name='' value='<?= $_SESSION['Perfil']['Login']?>' readonly/>
                                    </div>

                                    <label class="col-sm-1 control-label" style="text-align:left" for="">Coordenação de Compras</label>
                                    <div class="col-sm-2">
                                       <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                    </div>
                             </div>
                             <br>
                             <div class="form-group">
                                <label class="col-sm-1 control-label" style="text-align:left" for="">Data da Solicitação</label>
                                <div class="col-sm-2">
                                   <input class="form-control" type='text' id="DataSolicitacao" placeholder='' name=''   value=''/>
                                </div>

                                <label class="col-sm-1 control-label" style="text-align:left" for="">Data da Cotação</label>
                                <div class="col-sm-2">
                                   <input class="form-control" type='text'  id="DataCotacao" placeholder='' name=''   value=''/>
                                </div>

                                <label class="col-sm-1 control-label" style="text-align:left" for="">Data Prev Compra</label>
                                <div class="col-sm-2">
                                   <input class="form-control" type='text'  id="DataPrevCompra" placeholder='' name='' value='<?= ($oSupriOm) ? $oSupriOm->getPrevisaoFormatado() : "" ?>' readonly />
                                </div>
                                <label class="col-sm-1 control-label" style="text-align:left" for="">Dias do Processo</label>
                                <div class="col-sm-2">
                                   <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                </div>

                            </div>
             </div>
			<br>


    	</div>

    	<div class="tab-pane " id="2" style="padding-bottom:100px;">
    	<div class="form-group" style="padding: 30px;">
            <div class="col-sm-12">
                <input type="button" name="" class="btn btn-primary" value="+ Fornecedor" id="mais">
            </div>
        </div>
        <div style="overflow:auto; max-height:900px;">
            <div class=" item" id="conteudo_engloba">
             <div class="engloba">

             <div class="form-group">
                 <div class="col-sm-12">
                    <legend>Dados do Item</legend>
                 </div>
             </div>

            <div class="form-group" style="padding:15px;">
                <table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <?php if(is_array($voSupriOmItem)){?>
                            <thead>
                            <tr>
                                <th colspan="6"  style="text-align:center;font-size:20px;font-weight:bold;">Produtos</th>
                                <th  colspan="7" style="text-align:center;font-size:20px;font-weight:bold;">Melhor Compra</th>

                            </tr>
                            <tr>
                                <th class='Titulo'>Selecionar</th>
                                <th class='Titulo'>Item</th>
                                <th class='Titulo'>Cod</th>
                                <th class='Titulo'>Especificação</th>
                                <th class='Titulo'>UN</th>
                                <th class='Titulo'>QUANT</th>
                                <th class='Titulo'>P. Unit c/ (ICMS/IPI)</th>
                                <th class='Titulo'>V. Total c/ (ICMS/IPI)</th>
                                <th class='Titulo'>Ultimo Preço Comprado</th>
                                <th class='Titulo'>Difrença Compra</th>
                                <th class='Titulo'>%</th>

                            </tr>
                            </thead>
                            <tbody>
                                <? $i =1;?>
                               <?php foreach($voSupriOmItem as $oSupriOmItem){ ?>
                            <tr>
                                <td><input type="checkbox" value="<?= $oSupriOmItem->cod_om.'||'.$oSupriOmItem->om_item ?>"></td>
                                <td><?= $i++; ?></td>
                                <td><?= $oSupriOmItem->numero_solicitacao ?></td>
                                <td><?= $oSupriOmItem->descricao ?></td>
                                <td><?//= $oSupriOmItem-> ?></td>
                                <td><?= $oSupriOmItem->qtde ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php }?>
                            </tbody>
                        <?php }//if(count($voMnyPlanoContas)){?>
                </table>
			 </div>
			<br>
                 <div class="col-sm-1 fechar"><icon class="glyphicon glyphicon-trash btn_exlcuir" >Excluir </icon></div>

                 <div class="form-group">
                     <div class="col-sm-12">
                        <legend>Dados do Fornecedor</legend>
                     </div>
                 </div>

                              <div class="col-sm-12">
                                   <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>
                            <div class="col-sm-6">
                            <hr>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Nome:</label>
                                        <div class="col-sm-4">
                                           <select class="form-control ">
                                               <option>Selecione</option>
                                               <? if($voVPessoaGeralFormatada){?>
                                                    <? foreach($voVPessoaGeralFormatada as $oVPessoaGeralFormatada){?>
                                                        <option value="<?= $oVPessoaGeralFormatada->getPesgCodigo()?>"><?= $oVPessoaGeralFormatada->getNome() ?></option>
                                                    <? }?>
                                               <? } ?>
                                           </select>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Endereço:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Valor Unitario C/IPI:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control ValorUnitarioCIPI"  type='text'  id="" placeholder='' name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">VLR C/DIF:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control VLRCDIF" type='text'  placeholder='' id="" name='' value=''/>
                                        </div>
                                     </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Valor Total:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control ValorTotal" type='text' id="" placeholder='' name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Total dos Produtos:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control TotalProdutos" type='text'  placeholder='' id="" name='' value=''/>
                                        </div>
                                     </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Itens Cotados:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">% Diferença Melhor Compra:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control diferenca" type='text'  placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Valor com Frete:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control frete" type='text'  id="" placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                            </div>


                            <div class="col-sm-6">
                            <hr>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Fone:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control Fone" type='text'  id="" placeholder='' name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Contato:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">ICMS:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control ICMS" type='text'  placeholder='' id="" name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Cond. Pagamento:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control" type='text'  placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Prazo de Entrega:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control PrazoEntrega" type='text'  placeholder='' name='' id="" value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">DESC(%):</label>
                                        <div class="col-sm-4">
                                           <input class="form-control DESC" type='text'  placeholder='' id='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">DESC(R$):</label>
                                        <div class="col-sm-4">
                                           <input class="form-control DESC" type='text'   placeholder='' name='' value=''/>
                                        </div>

                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Frete do Fornecedor:</label>
                                        <div class="col-sm-4">
                                           <input class="form-control FreteFornecedor" type='text' id="" placeholder='' name='' value=''/>
                                        </div>
                                     </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align:left" for="">Frete (CIF / FOB):</label>
                                        <div class="col-sm-4">
                                           <input class="form-control FreteCIFFOB" type='text'  id="" placeholder='' name='' value=''/>
                                        </div>

                                    <label class="col-sm-2 control-label" style="text-align:left" for="">Valor Compra C/Frete:</label>
                                    <div class="col-sm-4">
                                       <input class="form-control ValorCompraFrete" type='text'  placeholder='' id="" name='' value=''/>
                                    </div>
                                 </div>
                                 <br>

                            </div>
                         </div>
                      </div>

                       <legend style="border-width: 20px;">&nbsp;</legend>

           	    </div>
           	</div>
            <div class="nova"></div>
        </div>
        </div>
            <div class="tab-pane" id="3" style="padding-bottom:100px;">
    		   3

        	</div>



    </div>


            <div class="form-goup">
                <label class="col-sm-3 control-label" style="text-align:left" for="">Observações do Comprador:</label>
                <div class="col-sm-12">
                    <textarea class="form-control" name="" value=""></textarea>
                </div>
            </div>

         </fieldset>
          <br>
          <br>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
                $("#DataFechamento").mask("99/99/9999");
                $("#DataSolicitacao").mask("99/99/9999");
                $("#DataCotacao").mask("99/99/9999");
                $(".PrazoEntrega").mask("99/99/9999");
                $("#DataPrevCompra").mask("99/99/9999");

                $(".Fone").mask("99999-9999");


                $(".ValorAprovado").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".ValorUnitarioCIPI").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".VLRCDIF").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".ValorTotal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".ValorFrete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".ICMS").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".DESC").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".FreteFornecedor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".FreteCIFFOB").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".ValorCompraFrete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".TotalProdutos").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".diferenca").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $(".frete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

                 //$(".DESC").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			});
 		</script>

        <script type="text/javascript" language="javascript">
            function preparaOm(){
                var codOm = document.getElementById("fCodOm").value;
                document.getElementById("codOm").value = codOm;
                document.getElementById("formSupriOm").submit();

            }
        </script>
        <script type="text/javascript" language="javascript">

             function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

                    oDiv = document.getElementById(sIdDivInsert);
                    sArquivo = sArquivo+'?'+sParametros;
                    $.ajax({
                            dataType: "html",
                            type: "GET",
                            beforeSend: function(oXMLrequest){
                                       // oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                        document.getElementById('teste').innerHTML ="<div id='test' class='overlay'></div>";
                                    },
                            url: sArquivo,
                            error: function(oXMLRequest,sErrorType){
                                                    alert(oXMLRequest.responseText);
                                                    alert(oXMLRequest.status+' , '+sErrorType);
                                               },
                            success: function(data){
                                             oDiv.innerHTML = data;
                                             document.getElementById('teste').innerHTML ="<div id='teste'></div>";
                            },
                            complete:function(){
                             jQuery(function($){
                                        $("#MovDataPrev").mask("99/99/9999");
                                        $('.mascaraMoeda').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                        //usados no modal relatorio...
                                        $("#DataInicial").mask("99/99/9999");
                                        $("#DataFinal").mask("99/99/9999");
                                        $("#Competencia").mask("99/9999");

                                        $("#ValorVale").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                        $("#ValorDocPendente").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                        $('.chosen').chosen();

                                     });
                            }
                    });
                }


        </script>
        <script type="text/javascript" language="javascript">

              $(document).ready(function () {

            /*
                $(".item").on('click', '.fechar', function() {
                   $(this).parent().remove();
                });


                $('.exe').on('click', function() {
                    var opt = '';
                  //  $('.anoExeOrc option').remove();

                    var ul = $('.fornecedor');
                    var linha =$('.item').html();


                    //$('.labelAno').css('display', 'inline');

                    //var textPpa = $('.ppaExeOrc :selected').text();
                    //textPpa = textPpa.replace("PPA ", "");

                   // var arrAnos = textPpa.split("-");
                   // var arrayOf = arrAnos.map(parseFloat);
                    for (var i = arrayOf[0]; i <= arrayOf[1]; i++) {
                        opt += '<option value="' + i + '">' + i + '</option>';
                    }


                    //linha.prop('id', '4');
                    opt += '<li><a href="#4" data-toggle="tab">Fornecedor 3</a></li>';
                    ul.append(opt);
                    $(".nova").append(linha);

                    */


                        var linha = $(".engloba:first").clone();
                        $("#mais").click(function() {
                           $("#conteudo_engloba").append(linha.clone());

                                $(".PrazoEntrega").mask("99/99/9999");
                                $(".Fone").mask("99999-9999");
                                $(".ValorAprovado").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".ValorUnitarioCIPI").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".VLRCDIF").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".ValorTotal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".ValorFrete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".ICMS").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".DESC").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".FreteFornecedor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".FreteCIFFOB").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".ValorCompraFrete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".TotalProdutos").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".diferenca").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                $(".frete").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

                        });
                        $("#conteudo_engloba").on('click', '.fechar', function() {
                           $(this).parent().remove();
                        });





                });


        </script>
        <script type="text/javascript" language="javascript">
        	 var qtdeCampos = 0;

			function addCampos(){
				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:
				document.getElementById("filho"+qtdeCampos).innerHTML =
                    "<div class='form-group'><div class='col-xs-4'><input type='text' class='form-control' required id='AseItemDescricao"+qtdeCampos+"' name='AseItemDescricao[]' placeholder='Descri&ccedil;&atilde;o' /></div><div class='col-xs-2'><input type='text' class='form-control' required id='AseItemValor"+qtdeCampos+"' name='AseItemValor[]' placeholder='Valor' /></div><button  type='button' class='btn btn-danger removeButton' onclick='removerCampo("+qtdeCampos+")' value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>"+
                    "<div class='row'>" +

				var x = document.createElement("SCRIPT");
				var t = document.createTextNode("jQuery(function($){ $('#AseItemValor"+qtdeCampos+"').maskMoney(	{symbol:'R$', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	});");
				x.appendChild(t);
				document.body.appendChild(x);
				qtdeCampos++;
			}

			function removerCampo(id){
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);
			}
 		</script>



       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
