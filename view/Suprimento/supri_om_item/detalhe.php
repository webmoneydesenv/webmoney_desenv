﻿<?
 $sOP = $_REQUEST['sOP'];
 $oSupriOmItem = $_REQUEST['oSupriOmItem'];
 ?>
<!doctype html>
 <html>
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Item do Orçamento - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

     <style>

            /*body{margin:40px;}*/

            .stepwizard-step p {
                margin-top: 10px;
            }

            .stepwizard-row {
                display: table-row;
            }

            .stepwizard {
                display: table;
                width: 100%;
                position: relative;
            }

            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }

            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;

            }

            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }

            .btn-circle {
              width: 30px;
              height: 30px;
              text-align: center;
              padding: 6px 0;
              font-size: 12px;
              line-height: 1.428571429;
              border-radius: 15px;
            }

     </style>


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOmItem.preparaLista">Gerenciar Item do Orçamento</a> &gt; <strong><?php echo $sOP?> Item do Orçamento</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Item do Orçamento</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Solicitação:&nbsp;</strong><?= $oSupriOmItem->numero_solicitacao ?></div>
			</div>
			<br>
			<br>
            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Produto:&nbsp;</strong><?= $oSupriOmItem->descricao ?></div>
			</div>
			<br>
			<div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Quantidade Solicitada:&nbsp;</strong><?= $oSupriOmItem->qtde_solicitada ?></div>
			</div>
			<br>
			<div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Quantidade Orçada:&nbsp;</strong><?= $oSupriOmItem->qtde_orcada ?></div>
			</div>
			<br>
			<br>
			<br>

            <div class="form-group" style='padding-bottom: 100px;'>
                 <div class="row" style="background:#CCC;margin-right:0px; margin-left:0px;padding:10px">
                    <div class="col-sm-1"><strong>Detalhar</strong></div>
                    <div class="col-sm-1"><strong>DATA</strong></div>
                    <div class="col-sm-4"><strong>DE</strong></div>
                    <div class="col-sm-4"><strong>PARA</strong></div>
                    <div class="col-sm-2"><strong>QUEM</strong></div>
                 </div>
             </div>
             <div class="form-group">
                 <div class="col-sm-12" style='padding-bottom: 50px;'>
                    <div class="stepwizard">
                        <div class="stepwizard-row">
                           <div class="stepwizard-step">
                                <button type="button" class="btn btn-primary btn-circle" disabled="disabled"></button>
                                <p>Cotação</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled"  ></button>
                                <p>Diretoria</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled" ></button>
                                <p>Dep. de Compras</p>
                            </div>
                            <div class="stepwizard-step">
                                <button type="button" class="btn btn-default btn-circle" disabled="disabled" ></button>
                                <p>Finalizado</p>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
             <br>
             <br>
             <br>

                 <div class="form-group" style="padding:30px;">
                        <label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">De:</label>
                        <div class="col-sm-2">
                            <input class="form-control" type='text' readonly  value="<?= $_SESSION['Perfil']['GrupoUsuario']?>">
                           <input type='hidden' name='fDeCodTipoProtocolo' value="<?= $_SESSION['Perfil']['CodGrupoUsuario']?>">

                        </div>
                        <label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">Para:</label>
                        <div class="col-sm-2" id="divPara">
                             <select name='fParaCodTipoProtocolo'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                                <? $sSelected = "";
                                   if($voSysProtocoloEncaminhamento){
                                       foreach($voSysProtocoloEncaminhamento as $oSysProtocoloEncaminhamento){
                                           if($oMnyMovimentoItemProtocolo){
                                               $sSelected = ($oMnyMovimentoItemProtocolo->getDeCodTipoProtocolo() == $oSysTipoProtocolo->getCodGrupoUsuario()) ? "selected" : "";
                                           }
                                ?>
                                           <option  <?= $sSelected?> value='<?= $oSysProtocoloEncaminhamento->getPara()?>'><?= $oSysProtocoloEncaminhamento->getAtivo()?></option>
                                <?
                                       }
                                   }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label" style="text-align:left" for="Responsavel">Responsavel:</label>
                        <div class="col-sm-4">
                        <input class="form-control" type='text' name='fResponsavel' required readonly value='<?= $_SESSION['oUsuarioImoney']->getLogin()?>'/>
                        </div>
               </div>
				<br>
				<br>
				<br>
				<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descrição:</label>
					<div class="col-sm-11">
                    <textarea class="form-control" rows="3" placeholder='Descrição' name='fDescricao'></textarea>
				</div>
				</div>

          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2" style="padding-top: 20px;"><a class="btn btn-primary" href="?action=SupriOmItem.preparaLista" >Voltar</a></div>
   		 </div>
             <script src="js/bootstrap.min.js"></script>
 		</div>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
       		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
