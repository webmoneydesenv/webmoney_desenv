<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOmItem = $_REQUEST['oSupriOmItem'];
 $voSupriOm = $_REQUEST['voSupriOm'];

$voSupriProduto = $_REQUEST['voSupriProduto'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_om_item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOmItem.preparaLista">Gerenciar supri_om_items</a> &gt; <strong><?php echo $sOP?> supri_om_item</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_om_item</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriOmItem" action="?action=SupriOmItem.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodOm" value="<?=(is_object($oSupriOmItem)) ? $oSupriOmItem->getCodOm() : ""?>" />
			<input type="hidden" name="fCodOmItem" value="<?=(is_object($oSupriOmItem)) ? $oSupriOmItem->getCodOmItem() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_om_item">

 							<input type='hidden' name='fCodOm' value='<?= ($oSupriOmItem) ? $oSupriOmItem->getCodOm() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodOmItem">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodOmItem' placeholder='Codigo' name='fCodOmItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oSupriOmItem) ? $oSupriOmItem->getCodOmItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="TipoMaterial">Tipo Material:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='TipoMaterial' placeholder='Tipo Material' name='fTipoMaterial'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriOmItem) ? $oSupriOmItem->getTipoMaterial() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Aplicacao">Aplicacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Aplicacao' placeholder='Aplicacao' name='fAplicacao'   value='<?= ($oSupriOmItem) ? $oSupriOmItem->getAplicacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Obeservacao">Observacao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Obeservacao' placeholder='Observacao' name='fObeservacao'   value='<?= ($oSupriOmItem) ? $oSupriOmItem->getObeservacao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriProduto">Produto:</label>
					<div class="col-sm-10">
					<select name='fCodProduto'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriProduto){
							   foreach($voSupriProduto as $oSupriProduto){
								   if($oSupriOmItem){
									   $sSelected = ($oSupriOmItem->getCodProduto() == $oSupriProduto->getCodProduto()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriProduto->getCodProduto()?>'><?= $oSupriProduto->getCodProduto()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Qtde">Quantidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Qtde' placeholder='Quantidade' name='fQtde'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriOmItem) ? $oSupriOmItem->getQtde() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
