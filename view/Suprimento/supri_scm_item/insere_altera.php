<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriScmItem = $_REQUEST['oSupriScmItem'];
 $voSupriScm = $_REQUEST['voSupriScm'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_scm_item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriScmItem.preparaLista">Gerenciar supri_scm_items</a> &gt; <strong><?php echo $sOP?> supri_scm_item</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_scm_item</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriScmItem" action="?action=SupriScmItem.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodScm" value="<?=(is_object($oSupriScmItem)) ? $oSupriScmItem->getCodScm() : ""?>" />
			<input type="hidden" name="fCodScmItem" value="<?=(is_object($oSupriScmItem)) ? $oSupriScmItem->getCodScmItem() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_scm_item">


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriScm">Codigo Orçamento:</label>
					<div class="col-sm-10">
					<select name='fCodScm'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriScm){
							   foreach($voSupriScm as $oSupriScm){
								   if($oSupriScmItem){
									   $sSelected = ($oSupriScmItem->getCodScm() == $oSupriScm->getCodScm()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriScm->getCodScm()?>'><?= $oSupriScm->getCodScm()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodScmItem">Codigo Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodScmItem' placeholder='Codigo Item' name='fCodScmItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oSupriScmItem) ? $oSupriScmItem->getCodScmItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodProduto">Produto:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodProduto' placeholder='Produto' name='fCodProduto'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriScmItem) ? $oSupriScmItem->getCodProduto() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Qtde">Quantidade:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Qtde' placeholder='Quantidade' name='fQtde'   value='<?= ($oSupriScmItem) ? $oSupriScmItem->getQtdeFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Projeto">Projeto:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Projeto' placeholder='Projeto' name='fProjeto'   value='<?= ($oSupriScmItem) ? $oSupriScmItem->getProjeto() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Revisao">Revisão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Revisao' placeholder='Revisão' name='fRevisao'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriScmItem) ? $oSupriScmItem->getRevisao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ConsumoMensal">Consumo Mensal:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='ConsumoMensal' placeholder='Consumo Mensal' name='fConsumoMensal'  onKeyPress="TodosNumero(event);" value='<?= ($oSupriScmItem) ? $oSupriScmItem->getConsumoMensal() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Aplicacao">Aplicação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Aplicacao' placeholder='Aplicação' name='fAplicacao'   value='<?= ($oSupriScmItem) ? $oSupriScmItem->getAplicacao() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#Qtde").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
