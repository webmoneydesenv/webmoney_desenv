﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriScmItem = $_REQUEST['oSupriScmItem'];
 $voSupriProtocoloEncaminhamento  = $_REQUEST['voSupriProtocoloEncaminhamento'] ;
 $voSupriOmItem = $_REQUEST['voSupriOmItem'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Item da Solicitação - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriScmItem.preparaLista">Gerenciar Item Solicitação</a> &gt; <strong><?php echo $sOP?> supri_scm_item</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Item Solicitação</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Solicitação:&nbsp;</strong><?= ($oSupriScmItem ) ? $oSupriScmItem->numero_solicitacao : ""?></div>
			</div>
			<br>
			<br>
            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Produto:&nbsp;</strong><?= $oSupriScmItem->descricao ?></div>
			</div>
			<br>

            <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Quantidade Solicitada:&nbsp;</strong><?= $oSupriScmItem->qtde ?></div>
			</div>
             <br>
            <div class="form-group">
				 <div class="col-sm-12" style="text-align:left"><strong>Quantidade Atendida:&nbsp;</strong><?= $oSupriScmItem->qtde_atendida ?></div>
			</div>
			<br>
             <div class="form-group">
                  <div class="col-sm-12">
                    <legend>Orçamento</legend>
				  </div>
                  <div class="form-group">
                     <? if($voSupriOmItem){ ?>
                         <? foreach($voSupriOmItem as $oSupriOmItem){ ?>
                            <div class="col-sm-12"><strong>Nº: </strong> <a href="?action=SupriOm.preparaFormulario&sOP=Detalhar&nIdSupriOm=<?= $oSupriOmItem->getCodOm() ?>"><?= $oSupriOmItem->getCodOm() ?></a> &nbsp;Qtde:<?= $oSupriOmItem->getQtde() ?></div>
                            <p>
                         <? } ?>
                     <? }?>
			      </div>

			</div>
			<br>
			<br>
			<br>



          <div class="form-group" >
 	    	<div class="col-sm-offset-5 col-sm-2" style="padding-top: 20px;"><a class="btn btn-primary" href="?action=SupriScmItem.preparaLista" >Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
            		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
