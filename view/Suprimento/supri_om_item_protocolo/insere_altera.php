<?php
 $sOP = $_REQUEST['sOP'];
 $oSupriOmItemProtocolo = $_REQUEST['oSupriOmItemProtocolo'];
 $voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];
$voAcessoGrupoUsuario = $_REQUEST['voAcessoGrupoUsuario'];
$voSupriScmItem = $_REQUEST['voSupriScmItem'];
$voSupriScmItem = $_REQUEST['voSupriScmItem'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>supri_om_item_protocolo - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=SupriOmItemProtocolo.preparaLista">Gerenciar supri_om_item_protocolos</a> &gt; <strong><?php echo $sOP?> supri_om_item_protocolo</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> supri_om_item_protocolo</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formSupriOmItemProtocolo" action="?action=SupriOmItemProtocolo.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodProtocoloSuprimentoItem" value="<?=(is_object($oSupriOmItemProtocolo)) ? $oSupriOmItemProtocolo->getCodProtocoloSuprimentoItem() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do supri_om_item_protocolo">

 							<input type='hidden' name='fCodProtocoloSuprimentoItem' value='<?= ($oSupriOmItemProtocolo) ? $oSupriOmItemProtocolo->getCodProtocoloSuprimentoItem() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriScmItem">Solicitacao:</label>
					<div class="col-sm-10">
					<select name='fCodOm'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriScmItem){
							   foreach($voSupriScmItem as $oSupriScmItem){
								   if($oSupriOmItemProtocolo){
									   $sSelected = ($oSupriOmItemProtocolo->getCodOm() == $oSupriScmItem->getCodScm()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriScmItem->getCodScm()?>'><?= $oSupriScmItem->getCodScm()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SupriScmItem">Item:</label>
					<div class="col-sm-10">
					<select name='fCodOmItem'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSupriScmItem){
							   foreach($voSupriScmItem as $oSupriScmItem){
								   if($oSupriOmItemProtocolo){
									   $sSelected = ($oSupriOmItemProtocolo->getCodOmItem() == $oSupriScmItem->getCodScmItem()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSupriScmItem->getCodScmItem()?>'><?= $oSupriScmItem->getCodScmItem()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="AcessoGrupoUsuario">Tipo:</label>
					<div class="col-sm-10">
					<select name='fDeCodTipoProtocolo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoGrupoUsuario){
							   foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
								   if($oSupriOmItemProtocolo){
									   $sSelected = ($oSupriOmItemProtocolo->getDeCodTipoProtocolo() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="AcessoGrupoUsuario">Tipo:</label>
					<div class="col-sm-10">
					<select name='fParaCodTipoProtocolo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voAcessoGrupoUsuario){
							   foreach($voAcessoGrupoUsuario as $oAcessoGrupoUsuario){
								   if($oSupriOmItemProtocolo){
									   $sSelected = ($oSupriOmItemProtocolo->getParaCodTipoProtocolo() == $oAcessoGrupoUsuario->getCodGrupoUsuario()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?>'><?= $oAcessoGrupoUsuario->getCodGrupoUsuario()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Responsavel">Responsavel:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Responsavel' placeholder='Responsavel' name='fResponsavel'   value='<?= ($oSupriOmItemProtocolo) ? $oSupriOmItemProtocolo->getResponsavel() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Data">Data:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Data' placeholder='Data' name='fData'  required   value='<?= ($oSupriOmItemProtocolo) ? $oSupriOmItemProtocolo->getDataFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descrição:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oSupriOmItemProtocolo) ? $oSupriOmItemProtocolo->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oSupriOmItemProtocolo) ? $oSupriOmItemProtocolo->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#Data").mask("99/99/9999");
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
