<div style="padding:10px;border-top:#DCDCDC  1px solid; ">
<p align="center" style="color:#5D5D5D;">Desenvolvido por <strong>Fábio Cavalcante</strong>.<br></p>
</div>

<!--usados na chamada do relatorio... -->
<div class="modal fade" id="Relatorio" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="form-group" id="divConteudo">&nbsp;</div><br><br>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
                </div>
          </div>
    </div>
</div>

<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
<script language="javascript" src="js/moments/moment.js"></script>
<script language="javascript" src="js/moments/moment-with-locales.js"></script>
<script language="javascript" src="js/formatDate.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" language="javascript">

	$(document).ready(function(){
	  $('.dropdown-submenu a.test').on("click", function(e){
		$(this).next('ul').toggle();
		e.stopPropagation();
		e.preventDefault();
	  });
	});

	function relatorio(nTipo, nCodEmpresa){
		/*
		  var dtInicio =  (new Date(document.getElementById('DataInicial').value)).toISOString().substring(0, 10) ;
		  if(data2.getMonth().toString() == 0 && data1.getDate().toString() < 10 ){
				 dtInicio =  data1.getFullYear().toString() + '-0' +  data1.getDate() + '-' + (data1.getMonth().toString() + 1);
		  }
		*/

		var dInicio = document.getElementById('DataInicial').value.toString().split('/');
		var dFinal  = document.getElementById('DataFinal').value.toString().split('/');
		dtInicio    = dInicio[2]+'-'+dInicio[1]+'-'+dInicio[0];
		dtFinal     = dFinal[2]+'-'+dFinal[1]+'-'+dFinal[0];
		var unidade = document.getElementById('fUniCodigo').value;

		if(nTipo == 4){
			var nConta = document.getElementById('CodContaCorrenteRelatorioAjax').value;
		}
		switch(nTipo){
				case 1 :
                    recuperaConteudoDinamico('index.php','action=MnyPlanoContas.preparaUnidade&UniCodigo'+unidade,'divFeedBack');
					window.open("http://200.98.201.88/webmoney/relatorios/?rel=pagar&inicio="+dtInicio+"&fim="+dtFinal+"&empresa="+nCodEmpresa+"&tipo=188&unidade="+unidade);
				break;
				case 2 :
					window.open("http://200.98.201.88/webmoney/relatorios/?rel=pagas&inicio="+dtInicio+"&fim="+dtFinal+"&empresa="+nCodEmpresa+"&tipo=188&unidade="+unidade);
				break;
				case 3 :
					var nCodPessoa = document.getElementById('credor').value;
					window.open("http://200.98.201.88/webmoney/relatorios/?rel=credor&inicio="+dtInicio+"&fim="+dtFinal+"&empresa="+nCodEmpresa+"&tipo=188&pessoa="+nCodPessoa+"&unidade="+unidade);
				break;
				case 4 :
					 //window.open("http://200.98.201.88/webmoney/?action=MnyConciliacao.preparaReconciliacao&dMesAno="+dCompetencia);
					window.open("http://200.98.201.88/webmoney/?action=MnyConciliacao.preparaReconciliacao&sOP=Reconciliacao&unidade="+unidade+"&conta="+nConta+"&dInicio="+dtInicio+"&dFim="+dtFinal);
				break;
				case 5 :

				   var  dDataInicio  = document.getElementById('DataInicial').value;
				   var  dDataFim     = document.getElementById('DataFinal').value;
				   var  bMovIrrf     = (document.getElementById('fMovIrrf').checked == true ? true : '');
                   var  bMovPis      = (document.getElementById('fMovPis').checked == true ? true : '');
				   var  bMovIss      = (document.getElementById('fMovIss').checked == true ? true : '');
				   var  bMovIcmsAliq = (document.getElementById('fMovIcmsAliq').checked == true ? true : '');
				   var  bMovCsll     = (document.getElementById('fMovCsll').checked == true ? true : '');
				   var  bMovIr       = (document.getElementById('fMovIr').checked == true ? true : '');
				   var  bMovInss     = (document.getElementById('fMovInss').checked == true ? true : '');
				   var  bMovConfins  = (document.getElementById('fMovConfins').checked == true ? true : '');
				   window.location.href = "http://200.98.201.88/webmoney/?action=MnyMovimento.carregaRelatorioImposto&nRelatorio=5&fMovIrrf="+bMovIrrf+"&fMovPis="+bMovPis+
				   "&fMovIss="+bMovIss+"&fMovIss="+bMovIss+"&fMovIcmsAliq="+bMovIcmsAliq+"&fMovCsll="+bMovCsll+"&fMovIr="+bMovIr+"&fMovInss="+bMovInss+"&fMovConfins="+bMovConfins+"&fDataInicial="+dtInicio +"&fDataFinal="+dtFinal+"&unidade="+unidade;
/*				    window.open("http://10.88.3.7:88/webmoney/?action=MnyMovimento.carregaRelatorioImposto&nRelatorio=5&fMovOutrosDesc="+bMovOutrosDesc+"&fMovIrrf="+bMovIrrf+"&fMovPis="+bMovPis+
				   "&fMovIss="+bMovIss+"&fMovIss="+bMovIss+"&fMovIcmsAliq="+bMovIcmsAliq+"&fMovCsll="+bMovCsll+"&fMovIr="+bMovIr+"&bMovInss="+bMovInss+"&fMovConfins="+bMovConfins+"&fDataInicial="+dtInicio +"&fDataFinal="+dtFinal);
*/				break;
				case 6 :
					//window.open("http://200.98.201.88/webmoney/?action=MnyMovimento.exportaXls&nTipoRel=2&dDataInicial="+dtInicio+"&dDataFinal="+dtFinal+"&fUniCodigo="+unidade);
					window.open("?action=MnyMovimento.exportaXls&nTipoRel=2&dDataInicial="+dtInicio+"&dDataFinal="+dtFinal+"&fUniCodigo="+unidade);
				break;
		}
	}
</script>
