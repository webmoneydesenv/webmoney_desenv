<div class="col-md-12">
    <div class="panel with-nav-tabs panel-default">
      <div class="panel-heading">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#taPendencias" data-toggle="tab">Pendências</a></li>
          <li id="MenuReg"><a href="#tabRealizados" data-toggle="tab" onClick="recuperaConteudoDinamicoPendencias('index.php','action=MnyMovimentoItemProtocolo.ListaEmAndamento&fCodUnidade=<?=($nCodUnidade) ? $nCodUnidade:''?>','divRealizados')">Em Andamento</a></li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
             <div class="tab-pane fade in active " id="taPendencias">
              <div class="form-group">
                <div class="col-md-12"  style="background-color: #fff; padding:10px  20px 10px;">
                <h4 style="color:#0175E9;  font-weight: bold;">Pendências...</h4>
                <hr style="border-bottom:1px solid #FEB13D">
                        <table id="lista" class="table table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0">
                            <?php if($voPendencias){?>
                                <thead>
                                <tr>
                                    <th class='Titulo'>F.A </th>
                                    <th class='Titulo'>Identifica&ccedil;&atilde;o </th>
                                    <th class='Titulo'>Respons&aacute;vel </th>
                                     <th class="Titulo">Descri&ccedil;&atilde;o </th>
                                    <th class='Titulo'>Data</th>
                                    <th class='Titulo'>Valor</th>

                                    <? if($_SESSION['Perfil']['CodGrupoUsuario'] ==12 ){?>
                                    <th class='Titulo'>Data do Pagamento</th>
                                    <th class='Titulo'>Agencia / Conta</th>
                                    <th class='Titulo'>Unidade de Negócio</th>
                                    <? } ?>

                                </tr>
                                </thead>
                                <tbody>
                                    <? foreach($voPendencias as $oPendencias){ ?>
                                <tr>
                                    <td>
                                    <a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?=($oPendencias->mov_tipo == 188) ? "Pagar":"Receber"?>&nIdMnyMovimentoItem=<?=str_replace(".","||",$oPendencias->mov_codigo); ?>&MovContrato=<?= $oPendencias->cod_protocolo_movimento_item?>&nTipoContrato=<?= $oPendencias->mov_item?>" target="_blank">
                                        <?= $oPendencias->mov_codigo;
                                        $aPendencias[] = $oPendencias->mov_codigo;
                                        ?>
                                    </a>
                                    </td>
                                    <td><?= $oPendencias->para_cod_tipo_protocolo?></td>
                                    <td><?= $oPendencias->responsavel?></td>
                                    <td><?= $oPendencias->descricao ?></td>
                                    <td><?= $oPendencias->data_formatada ?></td>
                                    <td><?= $oPendencias->ativo ?></td>

                                    <? if($_SESSION['Perfil']['CodGrupoUsuario'] ==12 ){?>
                                    <td><?= $oPendencias->data_conciliacao_formatada ?></td>
                                    <td><?= $oPendencias->agencia . " " . $oPendencias->conta ?></td>
                                    <td><?= $oPendencias->unidade_descricao ?></td>
                                    <? } ?>

                                </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                </tr>
                                </tfoot>
                            <?php } ?>
                        </table>


               </div>
            </div>
            </div>  <!-- tabRecursos end style="display: block;"-->
             <div class="tab-pane fade active" id="tabRealizados" >
                <div class="form-group">
                <div class="col-md-12"  style="background-color: #fff; padding:10px  20px 10px;">
                <h4 style="color:#0175E9;  font-weight: bold;">Em Andamento...</h4>
                <hr style="border-bottom:1px solid #FEB13D">
                <br>
                    <div id="divRealizados">


               </div>
                    </div>
              </div>
            </div>  <!-- tabRecursos end -->
        </div>

        </div>
    </div>
</div>
