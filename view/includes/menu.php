        <?php
/*
1	Desenvolvedor
2	Administrador
3	Jurídico
6	Chefe de Obra
7	Auditoria
8	Diretoria Financeira
10	Pagamento
11	Conciliação
12	Reconciliação Auditoria
13	Financeiro Central
14	COMUM a TODOS
15	Financeiro Obra
16	ADM Central
17	Suprimentos
18	Financeiro Obra
19	Master
*/
    //MENU CADASTROS
    $vCadastro = array(1,3,13,16,17,19);
    $vPessoas = array(1,3,19,13);
    $vPlanoContas = array(1,13,16,19);
    $vBancos = array(1,19);
    $vContasCorrente = array(1,19);
    $vEmpresas = array(1,19);
    $vFuncoes = array(1,19);
    $vUnidadeEmpresa = array(1,8,19);

// MENU FINANCEIRO
    $vContasPagarReceber = array(1,19,13,16,18);
    $vConciliacao = array(1,19,11);
    $vConciliacaoCaixinha = array(1,19,18);
    $vAporte = array(1,19,8,10,13,16);
    $vTransferencia = array(1,19,13,16,7,18);
    $vReConciliacao = array(1,19,12);

   // MENU CONTRATOS
    $vsContrato = array(1,19,3,13,16,17,15);
    $vAse = array(1,19,13,15,16,17);
    $vAseFrete = array(1,19,13,15,16,17);
    $vOC = array(1,19,13,15,16,17);

// MENU RELATÓRIOS

 $vRelatorio = array(1,19,7,8,10,11,12,13,16);

   // MENU SEGURANÇA
    $vUsuario = array(1,19,3,13,16);

//MENU SUPRIMENTOS
$vSolicitacaoCompra = array(1,17);

?>

<div class="navbar-default">
 		<div class="navbar-header">
                 <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                     <span class="sr-only">Navegação Responsiva</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
         </div>

         <div id="navbarCollapse" class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
 		    <li class="nav nav-pills " ><a href="?"><span class="glyphicon glyphicon-home"></span> INÍCIO </a></li>
             <!-- Classic dropdown -->
               <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vCadastro)){ ?>

             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-list-alt"></span> CADASTROS<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                   <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vPessoas)){ ?>
                    <li><a tabindex="-1" href="?action=MnyPessoaGeral.preparaLista">Pessoa</a></li>
                    <li class="divider"></li>
                   <?php } ?>
                   <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vPlanoContas)){ ?>
                    <li><a tabindex="-1" href="?action=MnyPlanoContas.preparaLista">Plano de Contas</a></li>
                 	<li class="divider"></li>
                   <?php } ?>
                   <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vBancos)){ ?>
                    <li><a tabindex="-1" href="?action=SysBanco.preparaLista">Bancos</a></li>
					<li class="divider"></li>
                   <?php } ?>
                   <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vContasCorrente)){ ?>
                    <li><a tabindex="-1" href="?action=MnyContaCorrente.preparaLista">Conta Corrente</a></li>
					<li class="divider"></li>
                   <?php } ?>
                    <li><a tabindex="-1" href="?action=SysEmpresa.preparaLista">Empresa</a></li>
                    <li class="divider"></li>
                   <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vFuncoes)){ ?>
                    <li><a tabindex="-1" href="?action=MnyFuncao.preparaLista">Fun&ccedil;&atilde;o</a></li>
                   <?php } ?>
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vUnidadeEmpresa)){ ?>
                   <li class="divider"></li>
                    <li><a tabindex="-1" href="?action=AcessoUnidadeEmpresa.preparaLista">Unidade x Empresa</a></li>
                   <?php } ?>
               </ul>
             </li>
               <? } ?>
           <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-file"></span> CONTRATOS<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vsContrato)){ ?>
                              <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaLista">CONTRATO</a></li>
                              <li class="divider"></li>
                    <? } ?>
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vAse)){ ?>
                              <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaASE">ASE</a></li>
                              <li class="divider"></li>
                    <? } ?>

                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vAseFrete)){ ?>
                              <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaASEFRETE">ASE/FRETE</a></li>
                              <li class="divider"></li>
                    <? } ?>


                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vOC)){ ?>
                              <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaOC">Ordem de Compra</a></li>
                    <? } ?>
                </ul>
            </li>
               <!--
           <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-file"></span> SUPRIMENTOS<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vSolicitacaoCompra)){ ?>
                              <li><a tabindex="-1" href="?action=SupriScm.preparaLista">Solic. Compra</a></li>
                              <li class="divider"></li>
                    <? } ?>

                </ul>
            </li>
-->
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-usd"></span> FINANCEIRO<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="?action=MnyMovimento.preparaLista"> Pesquisar Contas </a></li>
                    <? if(($_SESSION['Perfil']['CodGrupoUsuario'] ==1) || ($_SESSION['Perfil']['CodGrupoUsuario'] ==19)) {?>
                    <li class="divider"></li>
                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Ajustes<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="?action=MnyMovimento.preparaFormularioTrocaEmpresa">Movimento/Empresa</a></li>
                      <!--<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyMovimentoItem.preparaFormularioReativarFA','divConteudo')" >Reativar F.A</a></li>-->
                      <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyMovimentoItem.preparaFormularioExcluirFA','divConteudo')" >Excluir F.A</a></li>
                      <li><a tabindex="-1" href="?action=MnyConciliacao.preparaFormularioAbreConciliacao">Abrir Concilia&ccedil;&atilde;o</a></li>
                    </ul>
                  </li>
                    <?php } ?>
                 <li class="divider"></li>
                 <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vContasPagarReceber)){ ?>
               <!--  <li><a tabindex="-1" href="?action=MnyMovimento.preparaFormulario&sOP=Etapa1&sTipoLancamento=Pagar">Contas a Pagar</a></li>-->


                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Contas a Pagar<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="?action=MnyMovimento.preparaFormulario&sOP=Etapa1&sTipoLancamento=Pagar">Comum</a></li>
                      <li><a tabindex="-1" href="?action=MnyMovimentoItemAgrupador.preparaFormulario&sOP=Cadastrar">Agrupado</a></li>
                    </ul>
                  </li>



                 <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=MnyMovimento.preparaFormulario&sOP=Cadastrar&sTipoLancamento=Receber&fContratoTipo=9">Contas a Receber</a></li>
                  <li class="divider"></li>
                    <?php } ?>

                 <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Conciliação<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vConciliacao)){ ?>
                          <li><a href="?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Concilia&ccedil;&atilde;o</a></li>
                       <? } ?>
                        <? if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vConciliacaoCaixinha)){?>
                          <li class="divider"></li>
                          <li><a href="?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar&nCx=1">Concilia&ccedil;&atilde;o Caixinha</a></li>
                    <? } ?>

                    </ul>
                  </li>
                  <li class="divider"></li>
                  <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vReConciliacao)){ ?>
                  <li><a href="?action=MnyConciliacao.preparaFormulario&sOP=Alterar">Reconcilia&ccedil;&atilde;o</a></li>
                  <li class="divider"></li>
                    <? } ?>
                  <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vTransferencia)){ ?>
                    <li><a href="?action=MnyTransferencia.preparaLista">Transfer&ecirc;ncia</a></li>
                    <?php }?>
                    <?php if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vAporte)){ ?>
                  <li class="divider"></li>
                  <li><a href="?action=MnySolicitacaoAporte.preparaLista">Solicita&ccedil;&atilde;o de Aporte</a></li>
                    <?php } ?>

                </ul>
      		 </li>

<?         if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vRelatorio)){ ?>
             <li class="dropdown"><a href="#"    data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-pencil"></span> RELATORIOS <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Contas a Pagar<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=Pagar&nTipo=1','divConteudo')">PDF</a></li>
                      <li><a tabindex="-1" href="#" data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=Pagar&nTipo=6','divConteudo')">XLS</a></li>
                      <li><a tabindex="-1" href="?action=MnySolicitacaoAporte.preparaRelatorioPorEmpresa">Por Empresa</a></li>
                      <li><a tabindex="-1" href="?action=MnySolicitacaoAporte.preparaRelatorioAportePorUnidade&sOP=RelEmp">Aporte Por Unidade</a></li>
                    </ul>
                  </li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=Pagas&nTipo=2','divConteudo')" > Contas  Pagas </a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=Credor&nTipo=3','divConteudo')" > Credor</a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=Reconciliacao&nTipo=4','divConteudo')" > Reconcilia&ccedil;&atilde;o</a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&sOP=PagarXls&nTipo=5','divConteudo')" > Impostos Pagos</a></li>
                </ul>
      		 </li>
            <? } ?>



			<? if(($_SESSION['Perfil']['CodGrupoUsuario'] ==1)) {?>

              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-user"></span> DESENVOLVEDOR<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="?action=SysStatus.preparaLista">Status</a></li>
                  <li class="divider"></li>
				  <li><a tabindex="-1" href="?action=AcessoModulo.preparaLista"> Módulos </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=AcessoGrupoUsuario.preparaLista"> Grupos de Usuário </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=MnyContratoDocTipo.preparaLista">Tipos de Documento Contrato </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=AcessoUsuario.preparaLista">Usu&aacute;rios </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=AcessoLog.preparaLista">Log de Acesso</a></li>
                </ul>
      		 </li>
			<? } ?>
               <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-lock"></span> SEGURANÇA<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="?action=AcessoUsuario.preparaFormulario&sOP=AlterarSenha&nIdAcessoUsuario=<?=$_SESSION['oUsuarioImoney']->getCodUsuario()?>"> Alterar Senha </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="?action=Login.processaFormulario&sOP=EscolheEmpresa">Alterar Perfil</a></li>
                  <li class="divider"></li>

              <?      if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$vUsuario)){ ?>
                   <li><a tabindex="-1" href="?action=AcessoUsuario.preparaLista">Usu&aacute;rios </a></li>
                  <li class="divider"></li>
				  <li style="text-align:left;"><a tabindex="-1" href='?action=AcessoUsuario.preparaFormulario&sOP=LimparSenha'/>Limpar Senha</a></li>
				  <li class="divider"></li>
                    <? } ?>

                </ul>
              </li>
    <!--
            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-search"></span> PENDENCIAS<b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a tabindex="-1" href="?action=MnyMovimento.preparaListaPendencias">Ficha de Aceite</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="?action=SupChamado.preparaLista">Suporte</a></li>
                        <li class="divider"></li>
                    </ul>

                </li>
-->
                <li class="nav nav-pills"><a href="?action=MnyMovimento.preparaListaPendencias"> <span class="glyphicon glyphicon-list-alt"></span> PENDÊNCIAS</a></li>
                <li class="nav nav-pills "><a href="?action=SupChamado.preparaLista"> <span class="glyphicon glyphicon-cog"></span> SUPORTE</a></li>
                <li class="nav nav-pills btn-danger"><a href="?action=Login.processaFormulario&sOP=Logoff"> <span class="glyphicon glyphicon-off"></span> SAIR</a></li>
           </ul>
         </div>
     </div>
