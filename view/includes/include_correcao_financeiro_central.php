<?
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];

?>
<form  method="post" enctype="multipart/form-data"  class="form-horizontal" action="?action=MnyMovimento.processaFormularioReconciliacao" name='formCorrecao 'id='formCorrecao'>
    <input type="hidden" name="fCodArquivo" value="<?=$nCodArquivo?>">
    <input type="hidden" name="fCodContratoDocTipo" value="<?=$nCodContratoDocTipo?>">
    <input type="hidden" name="fFA" value="<?= $oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>">
    <input type="hidden" name="fTamanhoArquivo" value="<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getAssinado():""?>">

    <div class="modal-header">
      <div class="alert alert-danger">
          <h4>Atualize as Informações Necessárias</h4>
      </div>
    </div>
    <div class="modal-body">
     <div class="form-group">
        <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
        <div class="col-sm-10">
            <input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value="<?= ($oVMovimento) ? $oVMovimento->getMovDocumento() : ""?>"/>
        </div>
    </div>


    <div class="form-group">
            <label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo Doc:</label>
            <div class="col-sm-10">
                 <select name='fTipDocCodigo'  class="form-control chosen">
                   <option value="">SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyTipoDoc){
                       foreach($voMnyTipoDoc as $oMnyTipoDoc){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyTipoDoc->getPlanoContasCodigo()) ? "selected" : "";
                           }  ?>
                    <option  <?= $sSelected?> value="<?= $oMnyTipoDoc->getPlanoContasCodigo()?>"><?= $oMnyTipoDoc->getDescricao()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
           </div>
    </div>
    <div class='jumbotron form-group'>
        <div class="col-sm-10">
           <input type='file' id='file0'  onblur="verificaUpload('file0',1048576)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo">
        </div>
        <div class="col-sm-1">
            <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamanho maximo para este tipo de documento!'><?=((1048576/1024)/1024)?>MB</span>
        </div>
        <div class="col-sm-1"><span style="color:red;">*Obrigatório</span></div>
    </div>
    </div>
    <? //} ?>
    <div class="modal-footer" id="rodape">
      <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
      <input type="button" class="btn btn-success"    id='btnEnviar'value="Corrigir" onclick="submeteCorrecao()">
    </div>
</form>
