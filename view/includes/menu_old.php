     <div class="navbar-default">
 		<div class="navbar-header">
                 <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                     <span class="sr-only">Navegação Responsiva</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
             </div>

         <div id="navbarCollapse" class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
 		    <li class="nav nav-pills " ><a href="../../controle/includes/?"><span class="glyphicon glyphicon-home"></span> INÍCIO </a></li>
             <!-- Classic dropdown -->
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-list-alt"></span> CADASTROS<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                    <li><a tabindex="-1" href="../../controle/includes/?action=MnyPessoaGeral.preparaLista">Pessoa</a></li>
                    <li class="divider"></li>
                    <?
						$vPermitidos = array(1,2,13);
					//if(in_array($_SESSION['oUsuarioImoney']->getCodGrupoUsuario(),$vPermitidos)){
					 ?>
                    <li><a tabindex="-1" href="../../controle/includes/?action=MnyPlanoContas.preparaLista">Plano de Contas</a></li>
                 	<li class="divider"></li>
                    <li><a tabindex="-1" href="../../controle/includes/?action=SysBanco.preparaLista">Bancos</a></li>
					<li class="divider"></li>
                    <li><a tabindex="-1" href="../../controle/includes/?action=MnyCartao.preparaLista">Cart&atilde;o de Cr&eacute;dito</a></li>
					<li class="divider"></li>
                    <li><a tabindex="-1" href="../../controle/includes/?action=MnyContaCorrente.preparaLista">Conta Corrente</a></li>
					<li class="divider"></li>
                    <?// } ?>
                    <li><a tabindex="-1" href="../../controle/includes/?action=SysEmpresa.preparaLista">Empresa</a></li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="../../controle/includes/?action=MnyFuncao.preparaLista">Fun&ccedil;&atilde;o</a></li>

               </ul>
             </li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-usd"></span> FINANCEIRO<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="../../controle/includes/?action=MnyMovimento.preparaLista"> Pesquisar Contas </a></li>
                   <li class="divider"></li>
                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Ajustes<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="../../controle/includes/?action=MnyMovimento.preparaFormularioTrocaEmpresa">Movimento/Empresa</a></li>
                      <li><a tabindex="-1" href="../../controle/includes/?action=MnyConciliacao.preparaFormularioAbreConciliacao">Abrir Concilia&ccedil;&atilde;o</a></li>
                    </ul>
                  </li>
                 <li class="divider"></li>
                 <li><a tabindex="-1" href="../../controle/includes/?action=MnyContratoPessoa.preparaFormulario&sOP=Etapa1&sTipoLancamento=Pagar&fContratoTipo=1">Contas a Pagar</a></li>
                 <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=MnyMovimento.preparaFormulario&sOP=Cadastrar&sTipoLancamento=Receber&fContratoTipo=9">Contas a Receber</a></li>
                  <li class="divider"></li>
                  <li><a href="../../controle/includes/?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Concilia&ccedil;&atilde;o</a></li>
                  <li class="divider"></li>
                  <li><a href="../../controle/includes/?action=MnyTransferencia.preparaLista">Transfer&ecirc;ncia</a></li>
                  <li class="divider"></li>
                  <li><a href="../../controle/includes/?action=MnySolicitacaoAporte.preparaLista">Solicita&ccedil;&atilde;o de Aporte</a></li>
                </ul>
      		 </li>
             <li class="dropdown"><a href="#"    data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-pencil"></span> RELATORIOS <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">Contas a Pagar<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=1','divConteudo')" > PDF </a></li>
                      <li><a tabindex="-1" href="#" data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=6','divConteudo')"> XLS </a></li>
                    </ul>
                  </li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=2','divConteudo')" > Contas  Pagas </a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=3','divConteudo')" > Credor</a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=4','divConteudo')" > Reconcilia&ccedil;&atilde;o</a></li>
                   <li class="divider"></li>
                  <li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#Relatorio" onclick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRelatorio&nTipo=5','divConteudo')" > Impostos Pagos</a></li>
                </ul>
      		 </li>

             <?
             switch($_SESSION['oUsuarioImoney']->getCodGrupoUsuario()){
             	case 3:
                	$sDescricaoTopoMenu = "JURIDICO";
					$sItemMenu =
						'
							<ul role="menu" class="dropdown-menu">
							  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaLista&fTipoContrato=1">Contrato</a></li>
							</ul>

						';
                break;
             	case 6:
             	case 13:
                	$sDescricaoTopoMenu = "OBRA";
					$sItemMenu =
						'
							<ul role="menu" class="dropdown-menu">
							  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaLista&fTipoContrato=3">ASE</a></li>
							</ul>

						';
                break;
             	case 2:
                	$sDescricaoTopoMenu = "SUPRMIMENTOS";
                break;
					$sItemMenu =
						'
							<ul role="menu" class="dropdown-menu">
							  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaLista&fTipoContrato=2">Ordem de Compra</a></li>
							</ul>

						';
				default:
					$sDescricaoTopoMenu = "<span class='glyphicon glyphicon-file'></span> CONTRATOS";
					$sItemMenu =
						'
							<ul role="menu" class="dropdown-menu">
							  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaLista">CONTRATO</a></li>
			                <li class="divider"></li>
							   	  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaASE">ASE</a></li>
			                <li class="divider"></li>
							   	  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaASEFRETE">ASE/FRETE</a></li>
			                  <li class="divider"></li>
								  <li><a tabindex="-1" href="?action=MnyContratoPessoa.preparaListaOC">Ordem de Compra</a></li>
							</ul>

						';

				break;

             }
			 ?>
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><?=$sDescricaoTopoMenu?><b class="caret"></b></a>

                <?=$sItemMenu;?>
      		 </li>
			<? if(($_SESSION['oUsuarioImoney']) && ($_SESSION['oUsuarioImoney']->getCodGrupoUSuario() == 1 )) {?>
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-user"></span> DESENVOLVEDOR<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="../../controle/includes/?action=SysStatus.preparaLista">Status</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=MnySaldoDiario.preparaLista">Saldo Diário</a></li>
                  <li class="divider"></li>
                  <li><a href="../../controle/includes/?action=MnyContratoDocTipo.PreparaLista">Gerenciar Tipo de Documentos</a></li>
                  <li class="divider"></li>
				  <li><a tabindex="-1" href="../../controle/includes/?action=AcessoModulo.preparaLista"> Módulos </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=AcessoGrupoUsuario.preparaLista"> Grupos de Usuário </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=MnyContratoDocTipo.preparaLista">Tipos de Documento Contrato </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=AcessoUsuario.preparaLista">Usu&aacute;rios </a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="../../controle/includes/?action=AcessoLog.preparaLista">Log de Acesso</a></li>
                </ul>
      		 </li>
			<? } ?>
			 <!--
             <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">CONSULTAS<b class="caret"></b></a>
               <ul role="menu" class="dropdown-menu">
                 <li><a tabindex="-1" href="#"> Consulta 01 </a></li>
                 <li><a tabindex="-1" href="#"> Consulta 02 </a></li>
                 <li><a tabindex="-1" href="#"> Consulta 03 </a></li>
               </ul>
             </li>-->
               <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-lock"></span> SEGURANÇA<b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a tabindex="-1" href="../../controle/includes/?action=AcessoUsuario.preparaFormulario&sOP=AlterarSenha&nIdAcessoUsuario=<?=$_SESSION['oUsuarioImoney']->getCodUsuario()?>"> Alterar Senha </a></li>
                </ul>
              </li>
              <li class="nav nav-pills btn-danger"><a href="../../controle/includes/?action=Login.processaFormulario&sOP=Logoff"> <span class="glyphicon glyphicon-off"></span> SAIR</a>
           </ul>
         </div>
     </div>
