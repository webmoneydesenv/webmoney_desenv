<? if($_SESSION['Perfil']['CodGrupoUsuario'] == 10){
    $sOPLink="Pagamento";
}else{
    $sOPLink="Alterar";
}

?>
<div class="col-md-12">
    <div class="panel with-nav-tabs panel-default">
      <div class="panel-heading">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#taPendencias" data-toggle="tab">Pendências</a></li>
          <li id="MenuReg"><a href="#tabRealizados" data-toggle="tab">Realizados</a></li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
             <div class="tab-pane fade in active " id="taPendencias">
              <div class="form-group">
                <div class="col-md-12"  style="background-color: #fff; padding:10px  20px 10px;">
                <h4 style="color:#0175E9;  font-weight: bold;">Pendências...</h4>
                <hr style="border-bottom:1px solid #FEB13D">
                <br>
                    <table id="lista" class="table table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0">
                            <?php if($voSolicitacaoAportePendencias){?>

                                <thead>
                                <tr>
                                    <th class='Titulo'>Nº Aporte </th>
                                    <th class='Titulo'>Data</th>
                                     <th class="Titulo">Unidade</th>
                                    <th class='Titulo'>Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <? foreach($voSolicitacaoAportePendencias as $oSolicitacaoAportePendencias){ ?>
                                    <tr>
                                    <td >
                                        <? $sOP= ($_SESSION['Perfil']['CodGrupoUsuario'] != 10)? 'Alterar': 'Pagamento'?>
                                        <a style="margin-left: 50%;" href="?action=MnySolicitacaoAporte.preparaFormulario&sOP=<?= $sOP ?>&nIdMnySolicitacaoAporte=<?=$oSolicitacaoAportePendencias->getcodSolicitacao()?>" target="_blank">
                                            <?= $oSolicitacaoAportePendencias->getNumeroSolicitacao() ?>
                                        </a>
                                    </td>
                                    <td><?= $oSolicitacaoAportePendencias->getDataSolicitacaoFormatado()?></td>
                                    <td><?= $oSolicitacaoAportePendencias->getSolInc() ?></td>
                                    <td><?= ($oSolicitacaoAportePendencias->getAtivo()) ? number_format($oSolicitacaoAportePendencias->getAtivo(),2,",",".") : ""?></td>
                                </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                </tr>
                                </tfoot>
                            <?php }?>
                        </table>

               </div>
            </div>
            </div>  <!-- tabRealizados end style="display: block;"-->
             <div class="tab-pane fade active" id="tabRealizados" >
                <div class="form-group">
                <div class="col-md-12"  style="background-color: #fff; padding:10px  20px 10px;">
                <h4 style="color:#0175E9;  font-weight: bold;">Realizados...</h4>
                <hr style="border-bottom:1px solid #FEB13D">
                <br>

                        <table id="lista2" class="table table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0">
                            <?php if(is_array($voSolicitacaoAporteRealizados)){?>
                                <thead>
                                <tr>
                                    <th class='Titulo'>Nº Aporte </th>
                                    <th class='Titulo'>Data</th>
                                     <th class="Titulo">Unidade</th>
                                    <th class='Titulo'>Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                     <? foreach($voSolicitacaoAporteRealizados as $oSolicitacaoAporteRealizados){ ?>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-sm-1" >
                                            <a href="?rel=aporte&cod_aporte=<?= $oSolicitacaoAporteRealizados->getCodSolicitacao()?>" target="_blank" data-toggle="tooltip" title="Solicita&ccedil;&atilde;o PDF"><i class="glyphicon glyphicon-duplicate" style="font-size: 150%;"></i></a>&nbsp;
                                            </div>
                                            <div class="col-sm-2">
                                            <a style="margin-left: 50%;" href="?action=MnySolicitacaoAporte.preparaFormulario&sOP=<?=$sOPLink?>&nIdMnySolicitacaoAporte=<?=$oSolicitacaoAporteRealizados->getcodSolicitacao()?>" target="_blank">
                                                <?= $oSolicitacaoAporteRealizados->getNumeroSolicitacao() ?>
                                            </a>
                                            </div>
                                        </div>

                                    </td>
                                    <td><?= $oSolicitacaoAporteRealizados->getDataSolicitacaoFormatado() ?>  </td>
                                    <td><?= $oSolicitacaoAporteRealizados->getSolInc() ?></td>
                                    <td><?= ($oSolicitacaoAporteRealizados->getAtivo()) ? number_format($oSolicitacaoAporteRealizados->getAtivo(),2,",",".") : ""?></td>
                                </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                </tr>
                                </tfoot>
                            <?php }//if(count($voMnySaldoDiario)){?>
                        </table>

               </div>
              </div>
            </div>  <!-- tabRealizados end -->
        </div>



        </div>
    </div>
</div>
