<form  method="post" enctype="multipart/form-data"  class="form-horizontal" action="?action=SysArquivo.processaFormulario" name='formCorrecao 'id='formCorrecao'>
    <input type="hidden" name="fCodArquivo" value="<?=($oMnySolicitacaoAporte)?$oMnySolicitacaoAporte->getSolInc() : ""?>">
    <input type="hidden" name="fCodContratoDocTipo" value="11">
    <input type="hidden" name="fFA" value="<?= $oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>">
    <input type="hidden" name="fTamanhoArquivo" value="<?=($oMnySolicitacaoAporte)? $oMnySolicitacaoAporte->getAssinado() : ""?>">
    <input type="hidden" name="sOP" value="Alterar">

  <div class="modal-header">
      <div class="alert alert-danger">
          <h4>Atualize o Comprovante de Pagamento</h4>
      </div>
  </div>
  <div class="modal-body">
    <div class='form-group'>
         <div class="col-sm-8">
           <input type='file' id='file0'  onblur="verificaUpload('file0',1048576)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo">
        </div>
        <div class="col-sm-1">
            <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamanho maximo para este tipo de documento!'><?=((1048576/1024)/1024)?>MB</span>
        </div>
        <div class="col-sm-3"><span style="color:red;"> *Obrigatório</span></div>
    </div>

  </div>
 <? //} ?>
    <div class="modal-footer" id="rodape">
      <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
      <input type="button" class="btn btn-success"  disabled  id='btnEnviar'value="Corrigir" onclick="submeteCorrecao()">
    </div>
  </form>
