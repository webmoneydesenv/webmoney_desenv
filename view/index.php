<?php
//print_r($_SESSION);
//die();

$oFachadaFinanceiro = new FachadaFinanceiroBD();
$oFachadaPermissao  = new FachadaPermissaoBD();
if(!$_SESSION['oUsuarioImoney']){
    header("Location: ?action=Login.preparaFormulario&sOP=Login");
}


?>

<!doctype html>
<html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sistema WebMoney</title>
<!-- InstanceEndEditable -->

<!-- InstanceBeginEditable name="head" -->
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap-theme.css" rel="stylesheet">
<!--- Css dataTables--->
<link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="css/bootstrap-select-min.css">


<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">



<style>
	.dropdown-submenu {
		position: relative;
	}

	.dropdown-submenu .dropdown-menu {
		top: 0;
		left: 100%;
		margin-top: -1px;
	}
    #exTab1 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

#exTab2 h3 {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}

/* remove border radius for the tab */

#exTab1 .nav-pills > li > a {
  border-radius: 0;
}

/* change border radius for the tab , apply corners on top*/

#exTab3 .nav-pills > li > a {
  border-radius: 4px 4px 0 0 ;
}

#exTab3 .tab-content {
  color : white;
  background-color: #428bca;
  padding : 5px 15px;
}




</style>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">

<meta name="theme-color" content="#ffffff">

<!-- InstanceEndEditable -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body onLoad="recuperaConteudoDinamico('index.php','action=MnyMovimentoItemProtocolo.notificaTramitacao','divTramitacao')">
<div class="container">
  <header>
	 <?php include_once("view/includes/topo.php")?>

     <?php include_once("view/includes/menu.php")?>
  </header>
  	<div class="content">
      <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt;
    <!-- InstanceBeginEditable name="Migalha" --> <!-- InstanceEndEditable --></div>
    <h1>
		<!-- InstanceBeginEditable name="titulo" --><!-- InstanceEndEditable --></h1>
    <section style="min-height:500px;  margin-left:50px;">
		<!-- InstanceBeginEditable name="conteudo" -->
        	<h4>Bem vindo ao Sistema Web Money...</h4>
            <div id="formulario" class="TabelaAdministracao">
            <div id="corpoformulario">
              <!--  <div class="form-group">
                    <div class="col-sm-4">
                        <div class="alert alert-success">
                          <strong>
                              <icon class='glyphicon glyphicon-search' style='font-size:20px;'></icon>
                              <a href="#">
                                  Clique aqui para visualizar as suas pendências!
                              </a>
                          </strong>
                        </div>
                    </div>
                </div> -->
            </div>
                <!--<div id="divTramitacao" style="margin-top:50px;">&nbsp;</div>-->
            </div>

	<!-- InstanceEndEditable -->
        <!-- InstanceEnd -->

<!-- Bootstrap core JavaScript-->
	<script src="js/jquery/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
	<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
	<script src="js/producao.js" type="text/javascript"></script>
	<!-- Usados na chamada do relatorio-->
	<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
	<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
	<script language="javascript" src="js/moments/moment.js"></script>
	<script language="javascript" src="js/moments/moment-with-locales.js"></script>
    <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
    <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
    <script src="js/jquery/plugins/dataTables.colVis.js"></script>
	<script language="javascript" src="js/formatDate.js"></script>

    </section>
  <!-- end .content -->
  </div>
    <div align="center"><?php require_once("view/includes/mensagem.php")?></div>
    <footer>
	<?php require_once("view/includes/rodape.php")?>
  </footer>
<!-- end .container --></div>

</body>

</html>
