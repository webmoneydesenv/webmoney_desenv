<?php
	$oMnyMovimento = $_REQUEST['oMnyMovimento'];
	$voMnyMovimento = $_REQUEST['voMnyMovimento'];
	$voPessoa = $_REQUEST['voPessoa'];
	$voPesquisaMovimento = $_REQUEST['voPesquisaMovimento'] ;
	$sDescricao = $_REQUEST['sDescricao'] ;
	$voMnyTipoLancamento =  $_REQUEST['voMnyTipoLancamento'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Pesquisa</title>
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
<body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyMovimento.preparaLista&altEmp=1">Movimento Empresa</a></strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Trocar empresa do movimento</h3><br><br>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" class="formulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Pesquisa">


             <div class="col-sm-12">
<!--                <legend>Pesquisar Movimento</legend>-->
                 <div class="form-group">
                    <div class="col-sm-4" style="text-align:left" >&nbsp;</div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-4" style="text-align:left" ><strong>Pesquisar F.A</strong><input class="form-control" type='text' id='' placeholder='' name='fFichaAceite'  value=''/>
</div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-4" style="text-align:left">&nbsp;</div>
                  </div>
            </div>
         <div class="col-sm-12">
             </div>
             <div class="form-group"><div class="col-sm-12">&nbsp;</div></div>
            </div>
				<br>
				<br>
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" >Pesquisar</button>
     	</div><br>
		<div class="">&nbsp;</div>
   		</div>
       </form>
       <hr>

	   <?php if(is_array($voPesquisaMovimento)){?>

   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   				<thead>
   				<tr>
                	<th class='Titulo'>A&ccedil;&atilde;o</th>
					<th class='Titulo'>F.A</th>
                    <th class='Titulo'>Tipo</th>
					<th class='Titulo'>Nome</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Documento</th>
   					<th class='Titulo'>Hist&oacute;rico</th>
					<th class='Titulo'>Vencimento</th>
					<th class='Titulo'>Compet&ecirc;ncia</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voPesquisaMovimento as $oVPesquisaMovimento){ ?>
   				<tr>
               		<td>
                    <a href="#Objeto"  data-toggle="modal"  onClick="recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaEmpresa&fEmpCodigo=<?=$_SESSION['oEmpresa']->getEmpCodigo()?>&fMovCodigo=<?= $oVPesquisaMovimento->getMovCodigo()?>','conteudo')" data-toggle="tooltip" title="Alterar Empresa"><i class=" glyphicon glyphicon-edit"></i></a>
					<td><?= $oVPesquisaMovimento->getMovCodigo()?>.<?= $oVPesquisaMovimento->getMovItem()?></td>
  					<td><?= ($oVPesquisaMovimento->getMovTipo() == 188)? "PAGAR" : "RECEBER"?></td>
                    <td><?= $oVPesquisaMovimento->getNome()?></td>
					<td><?= $oVPesquisaMovimento->getMovValorParcelaFormatado()?></td>
					<td><?= $oVPesquisaMovimento->getMovDocumento()?></td>
					<td><?= $oVPesquisaMovimento->getMovObs()?></td>
					<td><?= $oVPesquisaMovimento->getMovDataVenctoFormatado()?></td>
					<td><?= $oVPesquisaMovimento->getCompetencia()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>

   					<td></td>
   				</tr>
   				</tfoot>
  			</table>
  			<?php }//if(count($voVPessoaFormatada)){?>

 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
        <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
        <script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
        <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
        <script src="js/jquery/plugins/dataTables.colVis.js"></script>
        <script src="js/modal-movimento-item.js"></script>
        <script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript" charset="utf-8">

		 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

		  jQuery(function($){
  			   $("#DataInicial").mask("99/99/9999");
			    $("#DataFinal").mask("99/99/9999");
				$("#MovDataVencto").mask("99/99/9999");
				$("#MovDataPrev").mask("99/99/9999");
  			 });

         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });

		function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
            	jQuery(function($){
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataVencto").mask("99/99/9999");
					$("#MovDataPrev").mask("99/99/9999");
					$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				  });
				 $(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
					  numFiles = input.get(0).files ? input.get(0).files.length : 1,
					  label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});

				$(document).ready( function() {
					$('.btn-file :file').on('fileselect', function(event, numFiles, label) {

						var input = $(this).parents('.input-group').find(':text'),
							log = numFiles > 1 ? numFiles + ' files selected' : label;

						if( input.length ) {
							input.val(log);
						} else {
							if( log ) alert(log);
						}

					});
				});
    		}
	});
}
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>

 <div class="modal fade" id="Objeto" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Alterar para</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
          </div>
    </div>
</div>

