<?php


$sOP = $_REQUEST['sOP'];
$oMnyMovimento = $_REQUEST['oMnyMovimento'];

$oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];
$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
$voMnyTipoAceite = $_REQUEST['voMnyTipoAceite'];

$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnyConta = $_REQUEST['voMnyConta'];
$voMnySetor = $_REQUEST['voMnySetor'];

$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
$voSysCompetencia = $_REQUEST['voSysCompetencia'];
$voMnyFormaPagamento = $_REQUEST['voMnyFormaPagamento'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];

$voSysEmpresa = $_REQUEST['voSysEmpresa'];
$voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
$oMnyTransferencia = $_REQUEST['oMnyTransferencia'];

//Documentos
$voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
$oMnyMovimentoDe = $_REQUEST['oMnyMovimentoDe'];
$oMnyMovimentoItemDe = $_REQUEST['oMnyMovimentoItemDe'] ;

$oMnyMovimentoPara = $_REQUEST['oMnyMovimentoPara'];
$oMnyMovimentoItemPara = $_REQUEST['oMnyMovimentoItemPara'] ;


$voMnyContaCorrenteDe  = $_REQUEST['voMnyContaCorrenteDe'] ;
$voMnyContaCorrentePara = $_REQUEST['voMnyContaCorrentePara'] ;

$voTipoAplicacao = $_REQUEST['voTipoAplicacao'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Transfer&ecirc;ncia</title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <style>
     .file{
         size:60px;
     }
</style>
 <!-- InstanceBeginEditable name="head" -->
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

 </head>
 <body >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyTransferencia.preparaLista">Gerenciar Movimentos</a> &gt; <strong>Transferência</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

       <h3 class="TituloPagina">Transferência</h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <?
            $sAction = ($_REQUEST['sOP']=="Transferir") ? "?action=MnyMovimento.processaFormulario" : "?action=MnyTransferencia.processaFormulario";


         ?>
        <form method="post" class="form-horizontal" name="formMnyMovimento" enctype="multipart/form-data" action="<?=$sAction?>" onSubmit="JavaScript: return validaForm(this), verificaContrato();" >
        <!-- <input type="hidden" name="sOP" value="Transferir" />-->
         <input type="hidden" name="sOP" value="<?=$_REQUEST['sOP']?>" />
          <input type='hidden' name='sTipoLancamento' value='Transferir'/>


         <input type='hidden' name='fMovDataInclusao' value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovDataInclusaoFormatado() : date('d/m/Y')?>'/>
         <input type='hidden' name='fMovInc' value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovInc() : ""?>'/>
         <input type='hidden' name='fMovAlt'value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovAlt() : ""?>'/>
         <input type='hidden' name='fMovTipo' required  value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovTipo() : $_REQUEST['nTipo']; ?>'/>
         <input type='hidden' name='fEmpCodigo' required  value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getEmpCodigo() : $_SESSION['oEmpresa']->getEmpCodPessoa()?>'/>

         <input type="hidden" name="fSetCodigo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getSetCodigo() : "230"?>'/>
         <input type="hidden" name="fMovDataEmissao"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getMovDataEmissao() : ""?>'/>
         <input type="hidden" name="fPesCodigo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getPesCodigo() : ""?>'/>
         <input type="hidden" name="fMovObs"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getMovObs() : "2"?>'/>
         <input type="hidden" name="fMovDocumento"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getMovDocumento() : ""?>'/>
         <input type="hidden" name="fNegCodigo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getNegCodigo() : ""?>'/>
         <input type="hidden" name="fConCodigo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getConCodigo() : ""?>'/>
         <input type="hidden" name="fUniCodigo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getUniCodigo() : ""?>'/>
         <input type="hidden" name="fAtivo"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getAtivo() : 1?>'/>

         <input type="hidden" name="fUniCodigoPara"value='<?= ($oMnyMovimentoPara) ? $oMnyMovimentoPara->getUniCodigo() : ""?>'/>

         <input type="hidden" name="fCodTransferencia"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getCodTransferencia() : ""?>'/>
         <input type="hidden" name="fMovCodigoOrigem"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getMovCodigoOrigem() : ""?>'/>
         <input type="hidden" name="fMovItemOrigem"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getMovItemOrigem() : ""?>'/>
         <input type="hidden" name="fUniCodigo"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getDe() : ""?>'/>
         <input type="hidden" name="fMovCodigoDestino"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getMovCodigoDestino() : ""?>'/>
         <input type="hidden" name="fMovItemDestino"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getMovItemDestino() : ""?>'/>
         <input type="hidden" name="fUniCodigoPara"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getPara() : ""?>'/>
         <input type="hidden" name="fAtivo"value='<?= ($oMnyTransferencia) ? $oMnyTransferencia->getAtivo() : 1 ?>'/>


        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Dados Gerais">
          <div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais</legend>
              <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Competencia">Comp:</label>
                    <div class="col-sm-4">
                    <input class="form-control" type='text' id='fCompetencia' placeholder='Competencia' name='fCompetencia'  required   value='<?=($oMnyMovimentoItemDe) ? $oMnyMovimentoItemDe->getCompetenciaFormatado() : ""?>'/>
                    </div>
                <label class="col-sm-2 control-label" style="text-align:left" for="Data Emissão">Emissão:</label>
                <div class="col-sm-4">
                <input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovDataEmissaoFormatado() : ""?>'/>
				</div>
              </div>

                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Histórico">Histórico:</label>
                <div class="col-sm-10">
      			 <input class="form-control"  type='text' placeholder='Histórico' name='fMovObs' value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovObs() : ""?>'/>
                </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                <div class="col-sm-10">
				<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oMnyMovimentoDe) ? $oMnyMovimentoDe->getMovDocumento() : ""?>'/>
				</div>
                </div>
                <br>
       		 </div>
                <div class="col-sm-6">
       		    <legend>Apropriações </legend>
   				<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
           		<div class="col-sm-10">
				    <select name='fNegCodigo'  class="form-control chosen" required   >
               <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyCentroNegocio){
							   foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
								   if($oMnyMovimentoDe){
									   $sSelected = ($oMnyMovimentoDe->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
            </div>
            </div>
			<div class="form-group" >
        	    <label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                <div class="col-sm-10" >
                  <select  name='fConCodigo'  class="form-control chosen"  required >
                    <option  value=''>Selecione...</option>
                    <? $sSelected = "";
						   if($voMnyConta){
							   foreach($voMnyConta as $oMnyConta){
								   if($oMnyMovimentoDe){
									   $sSelected = ($oMnyMovimentoDe->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                    <option  <?= $sSelected?> value='<?= $oMnyConta->getPlanoContasCodigo()?>'>
                      <?= $oMnyConta->getCodigo()?>
                      -
 				 <?= $oMnyConta->getDescricao()?>
                    </option>
                    <?
                                   }
                               }
                            ?>
                  </select>
                </div>
          		</div>
            	</div>
                </div>
	       		<div class="form-group">
            	<div class="col-sm-12">


                <div class="col-sm-6">
                    <legend>Origem</legend>
                 </div>
                 <div class="col-sm-6">
                     <legend>Destino</legend>
                 </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Empresa:</label>
                    <div class="col-sm-4">
                    <select name='fEmpCodigoDe' class="form-control chosen" required readonly='readonly'>
                        <option value=''>Selecione</option>
                        <?php foreach($voSysEmpresa as $oSysEmpresa){
                             if($_REQUEST['sOP'] == "Alterar"){
                                $sSelected = (($oMnyMovimentoDe) && ($oMnyMovimentoDe->getEmpCodigo() == $oSysEmpresa->getEmpCodigo())) ? " selected " : "";
                             }else{
                                 $sSelected = ($_SESSION[oEmpresa]->getEmpCodigo() == $oSysEmpresa->getEmpCodigo()) ? " selected " : "";
                             }
                        ?>
                            <option <?=$sSelected?> value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                            <? } ?>
                    </select>
                    </div>
                   <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Empresa:</label>
                    <div class="col-sm-4">
                    <select name='fEmpCodigoPara' class="form-control chosen" required onChange="recuperaConteudoDinamico('index.php','action=MnyTransferencia.carregaUnidadeDestino&nEmpCodigo='+this.value,'divUnidadeDestino')">
                        <option value=''>Selecione</option>
                        <?php foreach($voSysEmpresa as $oSysEmpresa){
                                $sSelected = (($oMnyMovimentoPara) && ($oMnyMovimentoPara->getEmpCodigo() == $oSysEmpresa->getEmpCodigo())) ? " selected " : "";
                        ?>
                            <option <?=$sSelected?> value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                            <? } ?>
                    </select>
                    </div>
                </div>

                <div class="form-group">
           		<label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-4">
                   <select name='fUniCodigo' class="form-control chosen"  required  onChange="recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipoConta&fUniCodigo='+this.value,'divContaDe')">
				    <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyUnidade){
							   foreach($voMnyUnidade as $oMnyUnidade){
								   if($oMnyMovimentoDe){
									   $sSelected = ($oMnyMovimentoDe->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> -
                <?= $oMnyUnidade->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
			  </div>
              <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
              <div class="col-sm-4"
                  <div id='divUnidadeDestino'>
                       <select name='fUniCodigoPara'  class="form-control chosen"  required onChange="recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipoConta&fUniCodigo1='+this.value,'divContaPara')">
                        <option value=''>Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                           if($oMnyMovimentoPara){
									           $sSelected = ($oMnyMovimentoPara->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                           }
                                ?>
                                <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?>
                                </option>
                                <?
                                       }
                                   }
                                ?>
                      </select>
                </div>
			  </div>

              <div class="form-group">
             	<label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">De:</label>
                <div class="col-sm-4">
                <div id="divContaDe">
                <select name='fContaDe'  class="form-control chosen" required >
                    <option value=''>Selecione</option>
                	<?php foreach($voMnyContaCorrenteDe as $oMnyContaCorrente){
                     if($oMnyTransferencia){
                           $sSelected = ($oMnyTransferencia->getDe() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
					  	  }
					   ?>
                    	<option <?= $sSelected?>  value="<?= $oMnyContaCorrente->getCcrCodigo()?>"><?php echo $oMnyContaCorrente->getCcrConta()?> - <?= $oMnyContaCorrente->getBanCodigo()?></option>
                        <? } ?>
				</select>
                </div>
 				</div>
                <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Para:</label>
                <div class="col-sm-4">
                <div id="divContaPara" >
                <select name='fContaPara' class="form-control chosen" required >
                    <option value=''>Selecione</option>
                	<?php foreach($voMnyContaCorrentePara as $oMnyContaCorrente){
		                     if($oMnyTransferencia){
    	                       $sSelected = ($oMnyTransferencia->getPara() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
						  	 }
					   ?>
                    	<option <?= $sSelected?> value="<?= $oMnyContaCorrente->getCcrCodigo()?>"><?php echo $oMnyContaCorrente->getCcrConta()?> - <?= $oMnyContaCorrente->getBanCodigo()?></option>
                        <? } ?>
				</select>
                </div>
                </div>
              </div>
              <div class="form-group" id="divAplicacao"  style="display:block">
             	<label class="col-sm-2 control-label" style="text-align:left" for="Resgatar/Aplicar">Resgatar/Aplicar:</label>
                <div class="col-sm-4">
                    <select name='fOperacao'  id='resAp' class="form-control chosen"  >
                        <option value='' >Selecione</option>
                        <option value='C' <?=($oMnyTransferencia && $oMnyTransferencia->getOperacao()=='C') ? " selected " : ""?>>APLICA&Ccedil;&Atilde;O</option>
                        <option value='D'  <?=($oMnyTransferencia && $oMnyTransferencia->getOperacao()=='D') ? " selected " : ""?>>RESGATE</option>
                    </select>
 				</div>
                <label class="col-sm-2 control-label" style="text-align:left" for="TipoAplicacao">Tipo Aplica&ccedil;&atilde;o:</label>
                <div class="col-sm-4" id="divTypeAplication">
                    <select name='fTipoAplicacao'  id='tipoAp' class="form-control chosen" >
                        <? if(($_REQUEST['sOP'] == "AlterarTransferencia") && ($oMnyTransferencia->getTipoAplicacao())){?>
                            <? foreach($voTipoAplicacao as $oTipoAplicacao){ ?>
                                <option  value="<?=$oTipoAplicacao->getPlanoContasCodigo()?>"><?=$oTipoAplicacao->getDescricao()?></option>
                            <? } ?>
                        <? }else{?>
                                <option value='' >Selecione</option>
                        <? }?>
                    </select>
                </div>
                </div>
                <div class="form-group">
                <div id="divValorGlobal">
                <label class="col-sm-2 control-label" style="text-align:left" for="Parcelas">Valor:</label>
                <div class="col-sm-2">
                <input class="form-control" type='text'  placeholder='Valor' id="fMovValor" name='fMovValor' required  value='<?= ($oMnyMovimentoItemDe) ? $oMnyMovimentoItemDe->getMovValorParcelaFormatado() : ""?>' />
                </div>
                <label class="col-sm-2 control-label" style="text-align:left" for="Tipo Caixa">Tipo Caixa:</label>
                <div class="col-sm-2">
                    <select name='fTipoCaixa' class="form-control chosen" required >
                        <option value='1' <?=($_REQUEST['fTipoCaixa']<>2) ? " selected " : "" ?> >CONTA</option>
                        <option value='2'  <?=($_REQUEST['fTipoCaixa']==2) ? " selected " : "" ?> >CAIXA</option>
                    </select>
                </div>
                <label class="col-sm-3 control-label" style="text-align:left" for="Parcelas">Gerar Ficha de Transfer&ecirc;ncia:</label>
                <div class="col-sm-1">
                	<input class="form-control" type='checkbox'  name='fEmitirDoc' value='1' />
                </div>
                </div>
                </div>

  <? if($voMnyContratoDocTipo){ ?>

          <div class="form-group">
              <div class="col-sm-12">
                <legend title="Documentos">Documentos </legend>
              </div>
            </div>
                    <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>
          <? $i = 0;
		     $j =0; ?>


				<?  foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                <div class="form-group">

							<? if($oMnyMovimentoDe)
								$voMnyContratoPessoaArquivoTipo = $oFachadaSys->recuperarTodosSysArquivoPorMovimentoPorTipoDocumento($oMnyMovimentoDe->getMovCodigo(), $oMnyContratoDocTipo->getCodContratoDocTipo());

							?>
                                <div class="col-sm-12">
                                     <label class="col-sm-2 control-label"  style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao() ?>:</label>
                                <?
                                    if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){


                                               if($oMnyContratoPessoaArquivoTipo->getCodArquivo() ){
                                                        echo "
                                                         <div class=\"col-sm-1 \" ><a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'><button type=\"button\" class=\"btn btn-success btn-sm \"  style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
                                                         <div class=\"col-sm-1\"><a data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=SysArquivo.processaFormulario&sOP=Excluir&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."&nCodTransferencia=".$oMnyTransferencia->getCodTransferencia() ." \"><button type=\"button\" class=\"btn btn-danger \" style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div>
                                                         <div class=\"col-sm-3\"><input type=\"file\" id=\"file$j\"  onblur=\"verificaUpload('file$j',".$oMnyContratoDocTipo->getTamanhoArquivo().")\"  data-buttonText=\"Atualizar\" class=\"\" style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /></div>
                                                         <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'>".(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)."MB</span>
                                                         "; ?>

                                                         <input type="hidden" name='fCodArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()."||".$oMnyContratoPessoaArquivoTipo->getCodArquivo()?>">
                                                <? } else{ ?>
                                                     <!--<input type="file"  id='file<?//=$j?>'  onblur="upload('file<?//=$j?>')"  class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?//= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?//= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Campo Obrigat&oacute;rio. </div>" : ""?>-->
                                                     <input type="file" id='file<?=$j?>'  onblur="verificaUpload('file<?=$j?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Campo Obrigat&oacute;rio. </div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodArquivo() : ""?>">
												<? }
                                        }?>
                                <? }else{ ?>

                                            <div class='col-sm-4'>
                                             <input type="file" id='file<?=$j?>'  onblur="verificaUpload('file<?=$j?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/>
                                             </div>
                                            <div class="col-sm-1">
                                             <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                            </div>
                                           <div class="col-sm-2">
                                             <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<span style=\"color:red;\" > * Arquivo Obrigat&oacute;rio. </span>" : ""?>
                                            </div>

                                            <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">



                                <? } ?>
                                </div>
                                <p style='border-bottom: 1px solid #ccc;'>&nbsp;</p>
                              <? $i++ ?>
                              <? $j++ ?>

                            </div>
		  		  <? } ?>

                  <div class="col-sm-2">
                       <div class="alert alert-danger" role="alert" style="text-align:center; font-weight:bold;" >
                           <a href='https://smallpdf.com/pt/compressor-de-pdf' target="_blank">COMPRESSOR DE PDF</a>
                        </div>
                  </div>
 </div>
            </div>
<? }?>
   <!--fim documentos-->
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2" >
            <button id='btnEnviar' type="submit" class="btn btn-primary" ><?=($sOP="AlterarTransferencia") ? "Alterar" : $sOP?></button>
            <div class='col-sm-12' id='divEnviar'></div>
     	</div>
   		</div>
       </form>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script src="js/modal-movimento-item.js"></script>

 		<script type="text/javascript" language="javascript">
  			 $('select[readonly=readonly] option:not(:selected)').prop('disabled', true);
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
                $("#MovDataInclusao").mask("99/99/9999");
                $("#MovDataEmissao").mask("99/99/9999");
                $("#MovDataVencto").mask("99/99/9999");
                $("#fCompetencia").mask("99/9999");
                $("#fMovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#fMovCsll").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#fMovIcmsAliq").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
            });

			jQuery(document).ready(function(){
 				jQuery(".chosen").data("placeholder","Selecione").chosen();
 			});

		function verificaNulo(valor){
			if(valor == null || valor == undefined)
				return 0;
			else
				return valor;
		}

		function habilita() {
				document.getElementById("avancar").disabled = false;
			}

  function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                           oDiv.innerHTML = "Aguarde... carregando...";

				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
					jQuery(function($){
						$("#MovDataVencto").mask("99/99/9999");
						$("#MovDataPrev").mask("99/99/9999");
						$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					  });
    		}
	});
}

	function redirect(nCod, nEmitir){
		var nMovCodigo = nCod;
		var nEmitirDoc = nEmitir;
		if(nEmitirDoc == 1){
			window.open("http://200.98.201.88/webmoney/relatorios/?rel=transferencia&movimento="+nMovCodigo);
		}
	}

	function habilitaAplicacao(){
		if(document.getElementById('para').value == document.getElementById('de').value){
			recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTypeApliction','divTypeAplication');
			document.getElementById("resAp").required = true
			document.getElementById('divAplicacao').style.display = "block";
		}else {
			document.getElementById('divAplicacao').style.display = "none";
			document.getElementById("resAp").required = false;
			document.getElementById('resAp').value = "";
			document.getElementById('tipoAp').value = "";
			document.getElementById('divAplicacao').style.display = "none";

		}
	}

 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>

  <div class="modal fade" id="Objeto" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
          </div>
    </div>
</div>
