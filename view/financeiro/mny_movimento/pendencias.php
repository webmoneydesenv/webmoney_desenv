<?php

    $voTramita = $_REQUEST['voTramita'];
    $voSolicitacaoAportePendencias = $_REQUEST['voSolicitacaoAportePendencias'];
    $voSolicitacaoAporteRealizados = $_REQUEST['voSolicitacaoAporteRealizados'];
    $voPendencias = $_REQUEST['voPendencias'];
    $nCodUnidade = $_REQUEST['$nCodUnidade'];
    $voPendencias = $_REQUEST['voPendencias'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a <? echo $_REQUEST['sTipoLancamento'] ?> </title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="calculaValorLiquido();">
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a>
     <!-- InstanceBeginEditable name="Migalha" -->

     <?=$sLink?> &gt; <strong><?php echo $sOP?> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

       <h3 class="TituloPagina">Visualizar Pendências</h3>

       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->

        <div id="formulario" class="TabelaAdministracao">
                <?
                if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$voTramita)){

                    if(in_array($_SESSION['Perfil']['CodGrupoUsuario'], $vAportePagina)){

                        include_once('view/includes/pendencias_diretoria.php');
                    }else{

                        include_once('view/includes/pendencias_geral.php');
                    }
                }
                ?>
        </div>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
        <script language="javascript" src="js/moments/moment.js"></script>
	    <script language="javascript" src="js/moments/moment-with-locales.js"></script>
        <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
        <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
        <script src="js/jquery/plugins/dataTables.colVis.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

        <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable();
        		oTable = $('#lista2').dataTable();
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });

  	 </script>
     <script type="text/javascript" charset="utf-8">
         function recuperaConteudoDinamicoPendencias(sArquivo,sParametros,sIdDivInsert){

                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                    oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         oDiv.innerHTML = data;
                        },
                        complete:function(){
                         jQuery(function($){
                                           var asInitVals = new Array();
                                           $(document).ready(function() {
                                                oTable2 = $('#lista2').dataTable({
                                                    "scrollX": true,
                                                    dom: 'C<"clear">lfrtip',
                                                        colVis: {
                                                      exclude: [ 0 ]
                                                    },
                                                    "aoColumnDefs": [
                                                    {
                                                        "bSortable": true, "aTargets": [ 0 ],
                                                        "bSearchable": true, "aTargets": [ 0 ]
                                                    }
                                                ]
                                                });
                                           } );

                                 });
                        }
                });
            }
     </script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>

 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
