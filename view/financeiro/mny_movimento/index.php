<?php
 $voMnyMovimento = $_REQUEST['voMnyMovimento'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Movimentos</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyMovimento.preparaLista">Gerenciar Movimentos</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Movimentos</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnyMovimento" id="formMnyMovimento" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyMovimento" onChange="JavaScript: submeteForm('MnyMovimento')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyMovimento.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Movimento</option>
   						<option value="?action=MnyMovimento.preparaFormulario&sOP=Alterar" lang="1">Alterar Movimento selecionado</option>
   						<option value="?action=MnyMovimento.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Movimento selecionado</option>
   						<option value="?action=MnyMovimento.processaFormulario&sOP=Excluir" lang="2">Excluir Movimento(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyMovimento)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyMovimento')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Data de inclusão</th>
					<th class='Titulo'>Data de emissão</th>
					<th class='Titulo'>Tipo de custo</th>
					<th class='Titulo'>Centro de negócios</th>
					<th class='Titulo'>Centro de custo</th>
					<th class='Titulo'>Unidade de negócios</th>
					<th class='Titulo'>Pessoa</th>
					<th class='Titulo'>Conta</th>
					<th class='Titulo'>Setor</th>
					<th class='Titulo'>Competência</th>
					<th class='Titulo'>Observação</th>
					<th class='Titulo'>Incluído por</th>
					<th class='Titulo'>Alterado por</th>
					<th class='Titulo'>Contrato</th>
					<th class='Titulo'>Tipo de aceite</th>
					<th class='Titulo'>Documento</th>
					<th class='Titulo'>Parcelas</th>
					<th class='Titulo'>- tipo movimento:

0 - a pagar

1 - a receber

</th>
					<th class='Titulo'>Empresa</th>
					<th class='Titulo'>MovContabil</th>
					<th class='Titulo'>MovParecer</th>
					<th class='Titulo'>MovStatusPri</th>
					<th class='Titulo'>Icms aliq</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Pis</th>
					<th class='Titulo'>Cofins</th>
					<th class='Titulo'>Csll</th>
					<th class='Titulo'>Iss</th>
					<th class='Titulo'>Ir</th>
					<th class='Titulo'>Irrf</th>
					<th class='Titulo'>Icms aliq</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyMovimento as $oMnyMovimento){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyMovimento')" type="checkbox" value="<?=$oMnyMovimento->getMovCodigo()?>" name="fIdMnyMovimento[]"/></td>
  					<td><?= $oMnyMovimento->getMovCodigo()?></td>
					<td><?= $oMnyMovimento->getMovDataInclusaoFormatado()?></td>
					<td><?= $oMnyMovimento->getMovDataEmissaoFormatado()?></td>
					<td><?= $oMnyMovimento->getCusCodigo()?></td>
					<td><?= $oMnyMovimento->getNegCodigo()?></td>
					<td><?= $oMnyMovimento->getCenCodigo()?></td>
					<td><?= $oMnyMovimento->getUniCodigo()?></td>
					<td><?= $oMnyMovimento->getPesCodigo()?></td>
					<td><?= $oMnyMovimento->getConCodigo()?></td>
					<td><?= $oMnyMovimento->getSetCodigo()?></td>
					<td><?= $oMnyMovimento->getComCodigo()?></td>
					<td><?= $oMnyMovimento->getMovObs()?></td>
					<td><?= $oMnyMovimento->getMovInc()?></td>
					<td><?= $oMnyMovimento->getMovAlt()?></td>
					<td><?= $oMnyMovimento->getMovContrato()?></td>
					<td><?= $oMnyMovimento->getTipAceCodigo()?></td>
					<td><?= $oMnyMovimento->getMovDocumento()?></td>
					<td><?= $oMnyMovimento->getMovParcelas()?></td>
					<td><?= $oMnyMovimento->getMovTipo()?></td>
					<td><?= $oMnyMovimento->getEmpCodigo()?></td>
					<td><?= $oMnyMovimento->getMovContabil()?></td>
					<td><?= $oMnyMovimento->getMovParecer()?></td>
					<td><?= $oMnyMovimento->getMovStatusPri()?></td>
					<td><?= $oMnyMovimento->getMovIcmsAliqFormatado()?></td>
					<td><?= $oMnyMovimento->getMovValorGlobFormatado()?></td>
					<td><?= $oMnyMovimento->getMovPisFormatado()?></td>
					<td><?= $oMnyMovimento->getMovConfinsFormatado()?></td>
					<td><?= $oMnyMovimento->getMovCsllFormatado()?></td>
					<td><?= $oMnyMovimento->getMovIssFormatado()?></td>
					<td><?= $oMnyMovimento->getMovIrFormatado()?></td>
					<td><?= $oMnyMovimento->getMovIrrfFormatado()?></td>
					<td><?= $oMnyMovimento->getMovInssFormatado()?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyMovimento)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyMovimento');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
