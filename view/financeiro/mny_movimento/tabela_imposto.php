<?php
 $voMnyMovimentoImposto = $_REQUEST['voMnyMovimentoImposto'];

 $nSomaIcmsAliq = 0;
 $nTotalPis = 0;
 $nTotalConfins = 0;
 $nTotalCsll = 0;
 $nTotalIss = 0;
 $nTotalIr = 0;
 $nTotalInss = 0;
 $nTotalIrrf = 0;
 $nTotal = 0;
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Movimentos</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyMovimento.preparaLista">Gerenciar Movimentos</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Movimentos de  <?= $_REQUEST['dInicial']?> &agrave; <?= $_REQUEST['dFinal']?></h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnyMovimento" id="formMnyMovimento" class="formulario">
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyMovimentoImposto)){?>
   				<thead>
   				<tr>
   					<th class='Titulo'>Movimento</th>
					<th class='Titulo'>Pessoa</th>
					<th class='Titulo'>Historico</th>
    				<th class='Titulo'>Conta</th>
					<th class='Titulo'>Documento</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Juros</th>
					<th class='Titulo'>Reten&ccedil;&otilde;es</th>
					<th class='Titulo'>Data Vencimento</th>
                    <? if($voMnyMovimentoImposto[0]->getMovIcmsAliq()){ echo "<th class='Titulo'>IcmsAliq</th>";}?>
                    <? if($voMnyMovimentoImposto[0]->getMovPis()){ echo "<th class='Titulo'>Pis</th>";}?>
                    <? if($voMnyMovimentoImposto[0]->getMovConfins()){echo "<th class='Titulo'>Cofins</th>";}?>
                    <? if($voMnyMovimentoImposto[0]->getMovCsll()){echo "<th class='Titulo'>Csll</th>";}?>
                    <? if($voMnyMovimentoImposto[0]->getMovIss()){echo"	<th class='Titulo'>Iss</th>";}?>
                    <? if($voMnyMovimentoImposto[0]->getMovIr()){ echo "<th class='Titulo'>Ir</th>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovInss()){ echo "<th class='Titulo'>Inss</th>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovIrrf()){echo "<th class='Titulo'>Irrf</th>";}?>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyMovimentoImposto as $oMnyMovimentoImposto){?>
   				<tr>
  					<td><?= $oMnyMovimentoImposto->getMovCodigo().".".$oMnyMovimentoImposto->getAtivo()?></td>
                   	<td><?= $oMnyMovimentoImposto->getMovDevolucao()?></td>
					<td><?= ($oMnyMovimentoImposto->getMovObs())?$oMnyMovimentoImposto->getMovObs() : "-"?></td>
   					<td><?= ($oMnyMovimentoImposto->getMovAlt())?$oMnyMovimentoImposto->getMovAlt() : "-"?></td>
					<td><?= ($oMnyMovimentoImposto->getMovDocumento())?$oMnyMovimentoImposto->getMovDocumento():"-"?></td>
					<td><?= ($oMnyMovimentoImposto->getMovParcelas())?$oMnyMovimentoImposto->getMovParcelas():"-"?></td>
					<td><?= ($oMnyMovimentoImposto->getMovContrato())?$oMnyMovimentoImposto->getMovContrato():"0.00"?></td>
					<td><?= (is_null($oMnyMovimentoImposto->getCenCodigo()))?$oMnyMovimentoImposto->getCenCodigo():"0.00"?></td>
					<td><?= ($oMnyMovimentoImposto->getMovDataEmissao())?$oMnyMovimentoImposto->getMovDataEmissaoFormatado() : "-"?></td>
                    <? if($oMnyMovimentoImposto->getMovIcmsAliq()){echo "<td>". $oMnyMovimentoImposto->getMovIcmsAliqFormatado() ."</td>"; $nTotalIcmsAliq = $nTotalIcmsAliq +$oMnyMovimentoImposto->getMovIcmsAliq; $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovIcmsAliq()); }?>
                    <? if($oMnyMovimentoImposto->getMovPis()){echo "<td>". $oMnyMovimentoImposto->getMovPisFormatado() ."</td>";  $nTotalPis = $nTotalPis + $oMnyMovimentoImposto->getMovPis(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovPis()); }?>
                    <? if($oMnyMovimentoImposto->getMovConfins()){ echo "<td>". $oMnyMovimentoImposto->getMovConfinsFormatado() ."</td>"; $nTotalConfins = $nTaotalConfins + $oMnyMovimentoImposto->getMovConfins(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovConfins());}?>
                    <? if($oMnyMovimentoImposto->getMovCsll()){ echo "<td>". $oMnyMovimentoImposto->getMovCsllFormatado() ."</td>"; $nTotalCsll = $nTotalCsll + $oMnyMovimentoImposto->getMovCsll(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovCsll());}?>
                    <? if($oMnyMovimentoImposto->getMovIss()){ echo "<td>".  $oMnyMovimentoImposto->getMovIssFormatado() ."</td>"; $nTotalIss = $nTotalIss + $oMnyMovimentoImposto->getMovIss(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovIss());}?>
                    <? if($oMnyMovimentoImposto->getMovIr()){ echo "<td>".  $oMnyMovimentoImposto->getMovIrFormatado()."</td>"; $nTotalIr = $nTotalIr + $oMnyMovimentoImposto->getMovIrFormatado(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovIr());}?>
                    <? if($oMnyMovimentoImposto->getMovInss()){ echo "<td>".  $oMnyMovimentoImposto->getMovInssFormatado()."</td>"; $nTotalInss = $nTotalInss + $oMnyMovimentoImposto->getMovInss(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovInss());}?>
                    <? if($oMnyMovimentoImposto->getMovIrrf()){ echo "<td>".  $oMnyMovimentoImposto->getMovIrrfFormatado() ."</td>" ; $nTotalIrrf = $nTotalIrrf + $oMnyMovimentoImposto->getMovIrrf(); $nTotal = ($nTotal + $oMnyMovimentoImposto->getMovIrrf());}?>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
                	<tr><td><a href="?action=MnyMovimento.carregaRelatorioImposto&xls=1&nRelatorio=5"   target="_blank" data-toggle="tooltip" title="Exportar Para XLS"><i class="glyphicon glyphicon-file"></i>Exportar Para XLS</a></td></tr>
					<? if($voMnyMovimentoImposto[0]->getMovIcmsAliq()){ echo "<tr><td><strong>Total IcmsAliq</strong>: " . number_format($nSomaIcmsAliq, 2, ',', '.') . "</td></tr>" ;}?>
                    <? if($voMnyMovimentoImposto[0]->getMovPis()){ echo "<tr><td><strong>Total Pis</strong>: " . number_format($nTotalPis, 2, ',', '.') . "</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovConfins()){ echo "<tr><td><strong>Total Confins</strong>: " .  number_format($nTotalConfins, 2, ',', '.') . "</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovCsll()){ echo "<tr><td><strong>Total Csll</strong>: " . number_format($nTotalCsll, 2, ',', '.') ."</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovIss()){ echo "<tr><td><strong>Total Iss</strong>: " . number_format($nTotalIss, 2, ',', '.') ."</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovIr()){ echo "<tr><td><strong>Total Ir</strong>: " . number_format($nTotalIr, 2, ',', '.') . "</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovInss()){ echo "<tr><td><strong>Total Inss</strong>: " . number_format($nTotalInss, 2, ',', '.') . "</td></tr>"; }?>
                    <? if($voMnyMovimentoImposto[0]->getMovIrrf()){ echo "<tr><td><strong>Total Irrf</strong>: " . number_format($nTotalIrrf, 2, ',', '.') . "</td></tr>"; }?>
					<?
						$nTotal = $nSomaIcmsAliq
						+$nTotalPis
						+$nTotalConfins
						+$nTotalCsll
						+$nTotalIss
						+$nTotalIr
						+$nTotalInss
						+$nTotalIrrf;

					echo "<tr><td><strong>Total Pago no Per&iacute;odo </strong>: " . number_format($nTotal, 2, ',', '.') . "</td></tr>";?>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyMovimento)){?>
  			</table>
            <br><br>
        </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyMovimento');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
