<?php

 $sOP = $_REQUEST['sOP'];
 $voMnyContratoTipo =  $_REQUEST['voMnyContratoTipo'];
 $voMnyPessoa = $_REQUEST['voMnyPessoa'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a Pagar</title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body <?=($_GET['fPesCodigo'] && $_GET['fContratoTipo']) ? "onLoad='carregaAjax(".$_GET['fPesCodigo'].")'" :""?>>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <strong> Contas a Pagar - Etapa 01</a></Strong> <!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
        <? if($_REQUEST['Pagina'] == 3){?>
		     <h3 class="TituloPagina">Selecionar  Contrato</h3>
       <? }else{ ?>
		   <h3 class="TituloPagina">Contas a Pagar - Etapa 01</h3>
	   <? }?>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->

       <form method="post" class="form-horizontal" name="formMnyContratoPessoa" action="?action=MnyMovimento.preparaFormulario" >
         <input type="hidden" name="sOP" value="<?= $sOP?>" />
		 <input type='hidden' name='sTipoLancamento' value='<?= $_REQUEST['sTipoLancamento']?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Pré Cadastro">
 		<div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left">Tipo:</label>
					<div class="col-sm-2">
                    		<select name='fContratoTipo' id="tipoContrato" onChange="despesaCaixa(this.value)"  class="form-control chosen"  >
                                <option value='0'>Selecione</option>
                                <? $sSelected = "";
                                   if($voMnyContratoTipo){
                                       foreach($voMnyContratoTipo as $oMnyContratoTipo){
                                           if($oMnyContratoPessoa){
                                               		$sSelected = ($oMnyContratoPessoa->getTipoContrato() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
										   }
										   if($_GET['fContratoTipo']){
                                               $sSelected = ($_GET['fContratoTipo'] == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
										   }
                                ?>
                                           <option  <?= $sSelected ?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>
                                <?
                                       }
                                   }
                                ?>
                            </select>
                     </div>
        </div>
        <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Contratado">Contratado:</label>
                    <div class="col-sm-8" id="divPessoa"></div>
				</div>
                <div id="divContrato">&nbsp;</div>
                  <div class="form-group">
                    <div id="button" class="col-sm-offset-5 col-sm-2" style="display:none">
                    	<input type="hidden" name="sOP" value="Etapa2">
                    	<button type="submit" class="btn btn-primary" id="botao"  >Selecionar</button>
                    </div>
   				  </div>
         </fieldset>

       </form>
      <div>&nbsp;</div><br>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>

  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">
            jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })


			 function voltar() {
				window.history.back()
			 }




			 function carregaAjax(nPessoa){
				 var nTipoContrato;
				 nTipoContrato = document.getElementById('tipoContrato').value;
			 	 recuperaConteudoDinamico('index.php','action=MnyMovimento.preparaFormulario&sOP=Etapa1_Ajax&fPesCodigo='+nPessoa+'&fContratoTipo='+nTipoContrato, 'divContrato');
				 if(nTipoContrato != 4 && nTipoContrato !=""){
					 document.getElementById('divContrato').style.display = 'block';
					 document.getElementById('button').style.display = 'none';
				 }
			 }

			 function despesaCaixa(el){

				 if(el == 4) {
					document.getElementById('divContrato').style.display = 'none';
					document.getElementById('button').style.display = 'block';
				 }else if(el != 4){
				 	 document.getElementById('button').style.display = 'none';
				 }
                 recuperaConteudoDinamico('index.php','action=MnyMovimento.preparaFormulario&sOP=Etapa0&fContratoTipo='+el, 'divPessoa');
			 }

 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


   <!--Contrato-->
  <div class="modal fade" id="anexo" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
          </div>
    </div>
</div>

