<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyMovimento = $_REQUEST['oMnyMovimento'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Movimento - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
     <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>

   </header>
      <?php include_once("view/includes/menu.php") ?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyMovimento.preparaLista">Gerenciar Movimentos</a> &gt; <strong><?php echo $sOP?> Movimento</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Movimento</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimento" action="?action=MnyMovimento.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyMovimento" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">Data de Inclusão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data de Inclusão' name='fMovDataInclusao' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataInclusao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataEmissao">Data de Emissão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Data de Emissão' name='fMovDataEmissao' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataEmissao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Custo:</label>
					<div class="col-sm-10">
					<select name='fCusCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Centro de Negócios:</label>
					<div class="col-sm-10">
					<select name='fNegCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Centro de Custo:</label>
					<div class="col-sm-10">
					<select name='fCenCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCenCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Unidade de Negócios:</label>
					<div class="col-sm-10">
					<select name='fUniCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesCodigo">Pessoa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pessoa' name='fPesCodigo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getPesCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ConCodigo">Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Conta' name='fConCodigo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getConCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Setor:</label>
					<div class="col-sm-10">
					<select name='fSetCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="ComCodigo">Competência:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Competência' name='fComCodigo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getComCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovObs">Observação:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Observação' name='fMovObs' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInc">Incluído por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Incluído por' name='fMovInc' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAlt">Alterado por:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Alterado por' name='fMovAlt' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovAlt() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovContrato">Contrato:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Contrato' name='fMovContrato' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovContrato() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Aceite:</label>
					<div class="col-sm-10">
					<select name='fTipAceCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getTipAceCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDocumento">Documento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Documento' name='fMovDocumento' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDocumento() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParcelas">Parcelas:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Parcelas' name='fMovParcelas' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovParcelas() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovTipo">- Tipo Movimento:

0 - A Pagar

1 - A Receber

:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Tipo Movimento:

0 - A Pagar

1 - A Receber

' name='fMovTipo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovTipo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="EmpCodigo">Empresa:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Empresa' name='fEmpCodigo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getEmpCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovContabil">Mov_contabil:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Mov_contabil' name='fMovContabil' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovContabil() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParecer">___mov_parecer:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_parecer' name='fMovParecer' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovParecer() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatusPri">Mov_status_pri:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Mov_status_pri' name='fMovStatusPri' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovStatusPri() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIcmsAliq">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ICMS Aliq' name='fMovIcmsAliq' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovIcmsAliq() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorGlob">Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Valor' name='fMovValorGlob' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovValorGlob() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='PIS' name='fMovPis' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovPis() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">COFINS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='COFINS' name='fMovConfins' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovConfins() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCsll">CSLL:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CSLL' name='fMovCsll' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovCsll() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ISS' name='fMovIss' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovIss() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IR' name='fMovIr' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovIr() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIrrf">IRRF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IRRF' name='fMovIrrf' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovIrrf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ICMS Aliq' name='fMovInss' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovInss() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyMovimento.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
