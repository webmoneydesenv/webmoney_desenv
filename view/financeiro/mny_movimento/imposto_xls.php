<?php  set_time_limit(3600); // 30 minutos
 $voMnyMovimentoImpostoXsl = $_REQUEST['voImposto'];
 $nSomaIcmsAliq = 0;
 $nTotalPis = 0;
 $nTotalConfins = 0;
 $nTotalCsll = 0;
 $nTotalIss = 0;
 $nTotalIr = 0;
 $nTotalIrrf = 0;
// definimos o tipo de arquivo
header("Content-type: application/msexcel");
$sNomeArquivo = "Impostos.xls";
// Como será gravado o arquivo
header("Content-Disposition: attachment; filename=$sNomeArquivo"); ?>
<table border="1" cellpadding="2" cellspacing="2">
     <thead>
          <tr>
            <th height="93" colspan="2"><img src="http://200.98.201.88/webmoney/view/sys/sys_empresa/logomarcas/<?=$_SESSION['oEmpresa']->getEmpImagem()?>"></th>
            <th  colspan="4"><h3>Impostos</h3></th>
          </tr>
    <? if($voMnyMovimentoImpostoXsl){ ?>
        <th class='Titulo'>Movimento</th>
        <th class='Titulo' colspan="5">Historico</th>
        <th class='Titulo'>Documento</th>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIcmsAliq()){ echo "<th class='Titulo'>IcmsAliq</th>";}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovPis()){ echo "<th class='Titulo'>Pis</th>";}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovConfins()){echo "<th class='Titulo'>Cofins</th>";}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovCsll()){echo "<th class='Titulo'>Csll</th>";}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIss()){echo"	<th class='Titulo'>Iss</th>";}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIr()){ echo "<th class='Titulo'>Ir</th>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovInss()){ echo "<th class='Titulo'>Inss</th>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIrrf()){echo "<th class='Titulo'>Irrf</th>";}?>
      </tr>
        </thead>
    <tbody>
    <? foreach($voMnyMovimentoImpostoXsl as $oMnyMovimentoImpostoXsl){  ?>
    <tr>
        <td><?= $oMnyMovimentoImpostoXsl->getMovCodigo()?></td>
        <td  colspan="5"><?= $oMnyMovimentoImpostoXsl->getMovObs()?></td>
        <td><?= $oMnyMovimentoImpostoXsl->getMovDocumento()?></td>
        <? if($oMnyMovimentoImpostoXsl->getMovIcmsAliq()){echo "<td>". number_format($oMnyMovimentoImpostoXsl->getMovIcmsAliq() , 2, ',', '.') ."</td>"; $nTotalIcmsAliq = $nTotalIcmsAliq +$oMnyMovimentoImpostoXsl->getMovIcmsAliq; }?>
        <? if($oMnyMovimentoImpostoXsl->getMovPis()){echo "<td>".  number_format($oMnyMovimentoImpostoXsl->getMovPis(), 2, ',', '.') ."</td>";  $nTotalPis = $nTotalPis + $oMnyMovimentoImpostoXsl->getMovPis();}?>
        <? if($oMnyMovimentoImpostoXsl->getMovConfins()){ echo "<td>". number_format($oMnyMovimentoImpostoXsl->getMovConfins(), 2, ',', '.') ."</td>"; $nTotalConfins = $nTaotalConfins + $oMnyMovimentoImpostoXsl->getMovConfinsFormatado();}?>
        <? if($oMnyMovimentoImpostoXsl->getMovCsll()){ echo "<td>".  number_format($oMnyMovimentoImpostoXsl->getMovCsll(), 2, ',', '.') ."</td>"; $nTotalCsll = $nTotalCsll + $oMnyMovimentoImpostoXsl->getMovCsllFormatado();}?>
        <? if($oMnyMovimentoImpostoXsl->getMovIss()){ echo "<td>".  number_format($oMnyMovimentoImpostoXsl->getMovIss(), 2, ',', '.') ."</td>"; $nTotalIss = $nTotalIss + $oMnyMovimentoImpostoXsl->getMovIssFormatado();}?>
        <? if($oMnyMovimentoImpostoXsl->getMovIr()){ echo "<td>".   number_format($oMnyMovimentoImpostoXsl->getMovIr(), 2, ',', '.') ."</td>"; $nTotalIr = $nTotalIr + $oMnyMovimentoImpostoXsl->getMovIrFormatado() ;}?>
        <? if($oMnyMovimentoImpostoXsl->getMovInss()){ echo "<td>".  $oMnyMovimentoImpostoXsl->getMovInssFormatado()."</td>"; $nTotalInss = $nTotalInss + $oMnyMovimentoImpostoXsl->getMovInss();}?>
        <? if($oMnyMovimentoImpostoXsl->getMovIrrf()){ echo "<td>". number_format($oMnyMovimentoImpostoXsl->getMovIrrf(), 2, ',', '.') ."</td>"; $nTotalIrrf = $nTotalIrrf + $oMnyMovimentoImpostoXsl->getMovIrrfFormatado();}?>
    </tr>
  <? } ?>
    </tbody>
    <tfoot>
    <tr>
		<? if($voMnyMovimentoImpostoXsl[0]->getMovIcmsAliq()){  echo "<tr><td><strong>Total IcmsAliq</strong>: " . number_format($nSomaIcmsAliq, 2, ',', '.') . "</td></tr>" ;}?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovPis()){ echo "<tr><td><strong>Total Pis</strong>: " . number_format($nTotalPis, 2, ',', '.') . "</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovConfins()){ echo "<tr><td><strong>Total Confins</strong>: " .  number_format($nTotalConfins, 2, ',', '.') . "</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovCsll()){ echo "<tr><td><strong>Total Csll</strong>: " . number_format($nTotalCsll, 2, ',', '.') ."</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIss()){ echo "<tr><td><strong>Total Iss</strong>: " . number_format($nTotalIss, 2, ',', '.') ."</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIr()){ echo "<tr><td><strong>Total Ir</strong>: " . number_format($nTotalIr, 2, ',', '.') . "</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovInss()){ echo "<tr><td><strong>Total Inss</strong>: " . number_format($nTotalInss, 2, ',', '.') . "</td></tr>"; }?>
        <? if($voMnyMovimentoImpostoXsl[0]->getMovIrrf()){ echo "<tr><td><strong>Total Irrf</strong>: " . number_format($nTotalIrrf, 2, ',', '.') . "</td></tr>"; }?>
		<?
            $nTotal = $nSomaIcmsAliq
            +$nTotalPis
            +$nTotalConfins
            +$nTotalCsll
            +$nTotalIss
            +$nTotalIr
            +$nTotalInss
            +$nTotalIrrf;

        echo "<tr><td><strong>Total Pago no Per&iacute;odo </strong>: " . number_format($nTotal, 2, ',', '.') . "</td></tr>";
        ?>
    </tr>
    </tfoot>
<? }?>
</table>
