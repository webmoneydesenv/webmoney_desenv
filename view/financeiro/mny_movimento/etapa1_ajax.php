<?
header("Content-Type: text/html; charset=utf-8",true);
$voMnyContratoPessoa = $_REQUEST['voMnyContratoPessoa'];
if($voMnyContratoPessoa){
?>
<input type="hidden" name="sOP" value="Cadastrar">
<label class="col-sm-2 control-label" style="text-align:left" for="Contrato">Contrato:</label>
   <div class="col-sm-10">
	<table id="lista1" class="table table-striped table-bordered" align="left">
		<thead>
          <tr>
            <th width="1%"><img src="controle/imagens/checkbox.gif" width="16" height="16"></th>
            <th class='Titulo'>N&uacute;mero</th>
            <th class='Titulo'>Descri&ccedil;&atilde;o</th>
            <th class='Titulo'>Inicio</th>
            <th class='Titulo'>Vig&ecirc;ncia</th>
            <th class='Titulo'>Valor</th>
            <th class='Titulo'>Saldo</th>
          </tr>
        </thead>
        <tbody>
           <?php foreach($voMnyContratoPessoa as $oMnyContratoPessoa){ ?>
          <? if($oMnyContratoPessoa->getSaldoContrato() > 0 ){?>
            <tr>

            	<td ><input type="radio" value="<?=$oMnyContratoPessoa->getContratoCodigo()?>" name="fIdMnyContratoPessoa" onClick="if(document.getElementById('botao').disabled==true){document.getElementById('botao').disabled=false}" /></td>

            <td><?= $oMnyContratoPessoa->getNumero()?></td>
            <td><?= $oMnyContratoPessoa->getDescricao()?></td>
            <td><?= $oMnyContratoPessoa->getDataInicioFormatado()?></td>
            <td><?= $oMnyContratoPessoa->getDataContratoFormatado()?> <?= ($oMnyContratoPessoa->getDataValidadeFormatado()) ? " A ". $oMnyContratoPessoa->getDataValidadeFormatado() : "" ?></td>
            <td>R$<?= $oMnyContratoPessoa->getValorContratoFormatado()?></td>
            <td>R$<?= $oMnyContratoPessoa->getSaldoContratoFormatado()?></td>
        </tr>
                 <? }?>
        <?php }?>
        </tbody>
  			</table>
            	  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-2" >
                    	<input type="hidden" name="sOP" value="Etapa2">
                    	<button type="submit" class="btn btn-primary" id="botao" disabled >Selecionar</button>
                    </div>
   				  </div>
        </div>
<?php }else{ ?>
	<div align="center">N&atilde;o existe contrato para esta pessoa.
    <!--
    <br><a href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=<?= $_REQUEST['sTipoLancamento']?>&Pagina=2&fContratoTipo=1&fPesCodigo=<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo']  ?>&fMovCodigo=<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>" class="btn btn-primary">Adicionar Contrato</a>
    -->
    </div>
<?php } ?>

</div>
