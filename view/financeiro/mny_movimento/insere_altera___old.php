<?php
$sOP = $_REQUEST['sOP'];
if($_REQUEST['fContratoTipo'] == 1 && $sOP=='Cadastrar'){
	$sLink = "<a href='?action=MnyContratoPessoa.preparaFormulario&sOP=Etapa1&fContratoTipo=".$_REQUEST['fContratoTipo']."&fPesCodigo=".$_REQUEST['fPesCodigo']."'>".$_REQUEST['sTipoLancamento']." Etapa 01</a>";
}else{
	$sLink = "<a href='?action=MnyMovimento.preparaLista'>Gerenciar Movimentos</a>";
}


$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
$sSomenteLeitura = $_REQUEST['sSomenteLeitura'];

$oMnyMovimento = $_REQUEST['oMnyMovimento'];
$voMnyMovimentoItem = $_REQUEST['voMnyMovimentoItem'];

$voMnyTipoAceite = $_REQUEST['voMnyTipoAceite'];
$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnyConta = $_REQUEST['voMnyConta'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
$voMnyFormaPagamento = $_REQUEST['voMnyFormaPagamento'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];

$sPagina = $_REQUEST['sPagina'];

//Documentos
$voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];

//contrato
$oContrato = $_REQUEST['oContrato'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a Pagar - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php">

     <?=$sLink?> &gt; <strong><?php echo $sOP?> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

       <h3 class="TituloPagina"><?php echo $sOP?> Contas a <?=$_REQUEST['sTipoLancamento']?>  <?= ($oMnyMovimento ) ? "&mdash; Nº ".$oMnyMovimento->getMovCodigo() : ""?></h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimento" enctype="multipart/form-data" action="?action=MnyMovimento.processaFormulario" onSubmit="JavaScript: return validaForm(this), verificaContrato();" >
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fMovCodigo" value="<?=(is_object($oMnyMovimento)) ? $oMnyMovimento->getMovCodigo() : ""?>" />
         <input type='hidden' name='fMovDataInclusao' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataInclusaoFormatado() : date('Y-m-d')?>'/>
         <input type='hidden' name='fMovInc' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovInc() : ""?>'/>
         <input type='hidden' name='fMovAlt'value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovAlt() : ""?>'/>
         <input type='hidden' name='fMovTipo' required  value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovTipo() : $_REQUEST['nTipo'] ?>'/>
         <input type='hidden' name='fEmpCodigo' required  value='<?= ($oMnyMovimento) ? $oMnyMovimento->getEmpCodigo() : $_SESSION['oEmpresa']->getEmpCodigo()?>'/>
         <input type="hidden" name="fMovObs" value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : "2"?>" />
         <input type='hidden' name='fAtivo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getAtivo() : "1" ?>'/>
         <input type='hidden' name='fMovContrato' value='<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>
         <input type='hidden' name='sTipoLancamento' value='<?= $_REQUEST['sTipoLancamento']?>'/>
         <input type='hidden' name='fContratoCodigo' value='<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>' />
         <input type='hidden' name='fCodContratoDocTipo' value='<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getTipoContrato() : ""?>' />
         <input type='hidden' name='fContratoTipo' value='<?=$_REQUEST['fContratoTipo']?>' />



        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Contrato">
        <div class="col-sm-12">
        	<legend>Dados Contratuais</legend>
              <div class="form-group">
                <div class="col-sm-12" style="text-align:left" ><strong>Descrição: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?>  </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3" style="text-align:left" ><strong>Tipo: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyContratoTipo()->getDescricao() : ""?>  </div>
                <div class="col-sm-3" style="text-align:left" ><strong>Numero: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : "" ?>  </div>
                <div class="col-sm-3" style="text-align:left" ><strong>Valor: </strong>R$ <?= ($oMnyContratoPessoa) ?  $oMnyContratoPessoa->getValorContratoFormatado() : ""?>  </div>
                <div class="col-sm-3" style="text-align:left" ><strong>Saldo: </strong>R$ <?= $oMnyContratoPessoa->getSaldoContratoFormatado()?></div>
              </div>
              <div class="form-group">
                <div class="col-sm-3" style="text-align:left" ><strong>Data Contrato: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?>  </div>
                <div class="col-sm-3" style="text-align:left" ><strong>Data Inicio: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataInicioFormatado() : ""?>  </div>
                <div class="col-sm-3" style="text-align:left" ><strong>Data Validade: </strong><?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?>  </div>
              </div>

        </div>
        </fieldset>
        <fieldset title="Dados Gerais">
          <div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais</legend>
              <div class="form-group">
                <? if($sOP == "Cadastrar"){?>
                    <label class="col-sm-2 control-label" style="text-align:left" for="Competencia">Comp:</label>
                    <div class="col-sm-4">
                    <input class="form-control" type='text' id='fCompetencia' placeholder='Competencia' name='fCompetencia'  required   value=''/>
                    </div>
  				<? }?>
                <label class="col-sm-2 control-label" style="text-align:left" for="Data Emissão">Emissão:</label>
                <div class="col-sm-4">
                <input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataEmissaoFormatado() : ""?>'/>
				</div>
              </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Pessoa:</label>
                <div class="col-sm-10" >
                <select name='fPesCodigo' id="PesCodigo" required class='form-control chosen' <?=$sSomenteLeitura ?>>
                    <option value=''>SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyPessoa){
                       foreach($voMnyPessoa as $oMnyPessoa){
                           if($oMnyMovimento){
                               $sSelected = ($oMnyMovimento->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
                           }elseif($oMnyContratoPessoa){
                               $sSelected = ($oMnyContratoPessoa->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
							}
                ?>
                    <option  <?= $sSelected?> value='<?= $oMnyPessoa->getPesgCodigo()?>'><?= $oMnyPessoa->getIdentificacao()?> - <?= $oMnyPessoa->getNome()?> </option>
                    <?
                       }
                   }
                ?>
                  </select>
 				</div>
                </div>
                <? if($sOP=='Alterar' && $oMnyMovimento->getMovContrato()){?>
                <div class="form-group">
	                <label class="col-sm-2" >Contrato:</label>
   	           	 	<div id="divContrato" class="col-sm-10">
						<input class=' form-control'type="text" name='descContrato' value='<?=($oMnyMovimento->getMovContrato()) ? $oMnyContratoPessoa->getDescricao() : "" ?>' >
                    </div>
                </div>
                <? }?>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Descri&ccedil;&atilde;o">Descri&ccedil;&atilde;o:</label>
                <div class="col-sm-10">
      			 <input class="form-control"  type='text' placeholder='Descrição' name='fMovObs' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : ""?>'/>
                </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                <div class="col-sm-10">
				<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDocumento() : ""?>'/>
				</div>
                </div>
                <br>
                </div>
                <div class="col-sm-6">
                <legend>Apropriações </legend>

				<? if ($voMnyCusto){ ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
            	<div class="col-sm-10">
				       <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyCusto){
							   foreach($voMnyCusto as $oMnyCusto){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value='<?= $oMnyCusto->getPlanoContasCodigo()?>'> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                <?
							   }
						   }
						?>
              </select>
		    	</div>
         	    </div>
          		<? } ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
           		<div class="col-sm-10">
				    <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
               <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyCentroNegocio){
							   foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
                    </div>
                    </div>
                   <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                   <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
				    <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyUnidade){
							   foreach($voMnyUnidade as $oMnyUnidade){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> -
                <?= $oMnyUnidade->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
				</div>
          		</div>
                    <?		if($voMnyCentroCusto){ ?>
                         <div class="form-group">
<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">C. Custo:</label>
            <div class="col-sm-10">
              <select name='fCenCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  <?= ($_REQUEST['sTipoLancamento'] == 'Pagar') ? "onChange=\"recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipo&sOP=1&fCenCodigo='+this.value,'divConta')\"": ""?> >
                <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyCentroCusto){
							   foreach($voMnyCentroCusto as $oMnyCentroCusto){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value='<?= $oMnyCentroCusto->getPlanoContasCodigo()?>'><?= $oMnyCentroCusto->getCodigo()?> -
                <?= $oMnyCentroCusto->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
            </div>
          </div>
					<? } ?>

		        <div class="form-group" >
        	    <label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                <div class="col-sm-10"  id="divConta">
			 		<select  name='fConCodigo'  class="form-control chosen"  required <?=$sSomenteLeitura?> >
						    <option  value=''>Selecione...</option>
                		<? $sSelected = "";
						   if($voMnyConta){
							   foreach($voMnyConta as $oMnyConta){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                        <option  <?= $sSelected?> value='<?= $oMnyConta->getPlanoContasCodigo()?>'><?= $oMnyConta->getCodigo()?> -
                        <?= $oMnyConta->getDescricao()?>
                        </option>
                    	<?
                                   }
                               }
                            ?>

                  </select>
				</div>
          		</div>
	           <div class="form-group">
     	       <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
        	   <div class="col-sm-10" id="divConta">
					<select name='fSetCodigo'  class="form-control chosen"  required >
						    <option value=''>Selecione</option>
                		<? $sSelected = "";
						   if($voMnySetor){
							   foreach($voMnySetor as $oMnySetor){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value='<?= $oMnySetor->getPlanoContasCodigo()?>'><?= $oMnySetor->getCodigo()?> -
                <?= $oMnySetor->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
				</div>
          		</div>
            	</div>
          		</div>
        		<br>
          		<div class="form-group">
<fieldset <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>>
            		<div class="col-sm-12">
	            		<legend>Valores</legend>
                		<div class="form-group">
                	<label class="col-sm-1 control-label" style="text-align:left" for="Valor">Valor:</label>
					<div class="col-sm-2">

						<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Valor' id="fMovValor" name='fMovValor' required value='' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
				   </div>
					    <label class="col-sm-1 control-label" style="text-align:left" for="MovOutrosDesc">OUTROS DESC:</label>
            		<div class="col-sm-2">
              			<input class="form-control" type='text' onChange="calculaValorLiquido()" placeholder='Outros Desc' id="fMovOutrosDesc" name='fMovOutrosDesc'   value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovOutrosDesc() > 0) ? $oMnyMovimento->getMovOutrosDescFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
                   	</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="IRRF">IRRF:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Irrf' name='fMovIrrf' id='fMovIrrf'   value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovIrrf() > 0) ? $oMnyMovimento->getMovIrrfFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
					</div>
                    <label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-2">
					<input class="form-control" onChange="calculaValorLiquido()" type='text' placeholder='Pis' name='fMovPis'  id='fMovPis'    value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovPis() > 0) ? $oMnyMovimento->getMovPisFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
				</div>

                </div>
    			<div class="form-group">
 					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-2">
						<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Iss' name='fMovIss' id="fMovIss"  value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovIss() > 0) ? $oMnyMovimento->getMovIssFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
					</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">INSS:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Inss' name='fMovInss' id='fMovInss'  value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovInss() > 0) ? $oMnyMovimento->getMovInssFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
					</div>
                <label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">CONFINS:</label>
					<div class="col-sm-2">
						<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Confins' id='fMovConfins' name='fMovConfins'  value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovConfins() > 0) ? $oMnyMovimento->getMovConfinsFormatado() :  "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
					</div>
                     <label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-2">
						<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='Ir' name='fMovIr' id='fMovIr'   value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovIr() > 0) ? $oMnyMovimento->getMovIrFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
                    </div>
 			</div>
                </div>
 <label class="col-sm-1 control-label" style="text-align:left" for="MovOutros">OUTROS:</label>
            		<div class="col-sm-2">
              			<input class="form-control" type='text' onChange="calculaValorLiquido()" placeholder='Outros' id="fMovOutros" name='fMovOutros'   value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovOutros() > 0) ? $oMnyMovimento->getMovOutrosFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
                    </div>
                   <label class="col-sm-1 control-label" style="text-align:left" for="Csll">CSLL:</label>
					<div class="col-sm-2">
						<input class="form-control" type='text' onChange="calculaValorLiquido()"  placeholder='csll'  id='fMovCsll' name='fMovCsll'  value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovCsll() > 0) ? $oMnyMovimento->getMovCsllFormatado() : "0.00") : "0.00" ?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/>
					 </div>
               <label class="col-sm-1 control-label" style="text-align:left" for="MovIcmsAliq">ICMS:</label>
            		<div class="col-sm-2">
              			<input class="form-control" type='text' placeholder='% Icms' id="fMovIcmsAliq" name='fMovIcmsAliq'   value='<?= ($oMnyMovimento) ? (($oMnyMovimento->getMovIcmsAliq() > 0 ) ? $oMnyMovimento->getMovIcmsAliqFormatado() : "0.00") : "0.00" ?>'<?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?> />
                     </div>
               <label class="col-sm-1 control-label" style="text-align:left" for="Liquido">Liquido:</label>
            		<div class="col-sm-2">
              			<div id="valorLiquido"></div>
                    </div>
            </div>
	   <? //if($sPagina != 1){ ?>
</fieldset>
          <div class="form-group">
<fieldset <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly = readonly" : ""?>>
            <div class="col-sm-12">
              	  <legend>Parcelas Automáticas</legend>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-1 control-label" style="text-align:left" for="Contabil">Parcelas:</label>
            <div class="col-sm-2"><input class="form-control" type='text' name='fMovParcelas' required  value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovParcelas() : ""?>' onKeyPress="TodosNumero(event);" maxlength="2" <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/></div>

            <label class="col-sm-1 control-label" style="text-align:left" for="Contabil">1º Vencto:</label>
            <div class="col-sm-2"><input class="form-control"  id="MovDataVencto" type='text' placeholder='Vencimento' name='fMovDataVencto'  <?=($_REQUEST['sOP'] == 'Alterar')?  "" :  'required' ?>   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado(): ""?>' <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/></div>

            <label class="col-sm-2 control-label" style="text-align:left" for="Contabil">Periodicidade:</label>
            <div class="col-sm-1"><input class="form-control" type='text' placeholder='Em dias' name='fPeriodicidade' value='' onKeyPress="TodosNumero(event);" maxlength="2" <?= ($_REQUEST['sOP'] == 'Alterar') ? "readonly " : ""?>/></div>

                <label class="col-sm-1 control-label" style="text-align:left" for="Forma Pagamento">PGTO:</label>
                <div class="col-sm-2">

         		<select name='fFpgCodigo'  class="form-control chosen" <?=($_REQUEST['sOP'] == 'Alterar')?  "" :  'required' ?>   >
                   <option value=''>SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyFormaPagamento){
                       foreach($voMnyFormaPagamento as $oMnyFormaPagamento){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipAceCodigo() == $oMnyFormaPagamento->getPlanoContasCodigo()) ? "selected" : "";
                           }
                ?>
                    <option  <?= $sSelected?> value='<?= $oMnyFormaPagamento->getPlanoContasCodigo()?>'><?= $oMnyFormaPagamento->getDescricao()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
               </div>
          </div>
 <div class="form-group">
 <label class="col-sm-2 control-label" style="text-align:left" for="Tipo de Aceite">Tipo de Aceite:</label>
                <div class="col-sm-4">
                <select name='fTipAceCodigo'  class="form-control chosen" <?=($_REQUEST['sOP'] == 'Alterar')?  "" :  'required' ?>  >
                    <option value=''>SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyTipoAceite){
                       foreach($voMnyTipoAceite as $oMnyTipoAceite){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipAceCodigo() == $oMnyTipoAceite->getPlanoContasCodigo()) ? "selected" : "";
                           }
                ?>
                    <option  <?= $sSelected?> value='<?= $oMnyTipoAceite->getPlanoContasCodigo()?>'><?= $oMnyTipoAceite->getDescricao()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
                       </div>

					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Documento:</label>
					<div class="col-sm-4">
					<select name='fTipDocCodigo'  class="form-control chosen"  <?=($_REQUEST['sOP'] == 'Alterar')?  "" :  'required' ?>   >
			       <option value=''>SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyTipoDoc){
                       foreach($voMnyTipoDoc as $oMnyTipoDoc){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyTipoDoc->getPlanoContasCodigo()) ? "selected" : "";
                           }
                ?>
                    <option  <?= $sSelected?> value='<?= $oMnyTipoDoc->getPlanoContasCodigo()?>'><?= $oMnyTipoDoc->getDescricao()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
               </div>
</fieldset>
     <?// }?>

          	   <div class="col-sm-2">
<!-- <button type="submit" class="btn btn-primary" onClick="habilita();" >Somar</button>-->
				<br>
  		<?php /*?>     <div class="form-group"><?php */?>
   		</div>
		   <table width="100%" cellpadding="2" cellspacing="2" border="0">
			   <tr>
				   <td align="left">                   </td>
                     <td align="right"></td>
			   </tr>
		   </table>
		   <br/>
           <? if($sOP != 'Cadastrar'){?>
           <div style="padding: 0 0 0.5em 1em"><a  class="btn btn-primary"href="#Objeto" data-toggle="modal" onClick="recuperaConteudoDinamico2('index.php','action=MnyMovimentoItem.preparaFormulario&sOP=Cadastrar&AddItem=1&nIdMnyMovimento=<?=$oMnyMovimento->getMovCodigo()?>&nCodContrato=<?=$oMnyContratoPessoa->getContratoCodigo()?>','conteudo')" >+ Parcela</a></div>
           <? }?>
           <br>
		   <table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyMovimentoItem)){?>
			   <thead>
   				<tr>
                	<th class='Titulo'>ACOES</th>
   					<th class='Titulo'>Parcela</th>
                    <th class='Titulo'>Vencto</th>
                     <th class='Titulo'>Compet&ecirc;ncia</th>
                    <th class='Titulo'>Valor</th>
					<th class='Titulo'>Forma Pgto</th>
                    <th class='Titulo'>Tipo Doc.</th>
   				</tr>
			   </thead>
			   <tbody>
                  <?php foreach($voMnyMovimentoItem as $oMnyMovimentoItem){ ?>
   				<tr>
                	<td>
					<a href="relatorios/?rel=ficha_aceite&mov=<?= $oMnyMovimento->getMovCodigo()?>&item=<?= $oMnyMovimentoItem->getMovItem()?>" target="_blank" data-toggle="tooltip" title="Gerar Ficha de Aceite"><i class="glyphicon glyphicon-file"></i></a>
                    <!--<a href="#Objeto" data-toggle="modal" onClick="recuperaConteudoDinamico('index.php','action=MnyMovimentoItem.preparaFormulario&sOP=Alterar&nIdMnyMovimentoItem=<?=$oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>','conteudo')" >Alterar</a>-->
                    <a href="#Objeto" data-toggle="modal" onClick="recuperaConteudoDinamico2('index.php','action=MnyMovimentoItem.preparaFormulario&sOP=Alterar&nIdMnyMovimentoItem=<?=$oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>','conteudo')" ><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#"  onclick="confirmacao('?action=MnyMovimentoItem.processaFormulario&sOP=Excluir&sOrigem=insere_altera&fIdMnyMovimentoItem=<?=$oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>')"><i class="glyphicon glyphicon-trash"></i></a></td>
                    <td><?= $oMnyMovimentoItem->getMovItem()?></td>
					<td><?= $oMnyMovimentoItem->getMovDataVenctoFormatado()?></td>
                    <td><?= $oMnyMovimentoItem->getCompetenciaFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovValorFormatado()?></td>
                    <td><?= $oMnyMovimentoItem->getMnyPlanoContasFormaPagamento()->getDescricao()?></td>
					<td><?= $oMnyMovimentoItem->getMnyPlanoContasTipoDocumento()->getDescricao()?></td>
                </tr>
  				<?php }?>
			   </tbody>
  			<?php }//if(count($voCondesembolso)){?>
		   </table>

           </div>
           </div>
      <!--inicio documentos-->
   <? if($oMnyContratoPessoa){?>
          <legend title="Documentos">Documentos</legend>
          <div class="form-group">
          <? $i = 0;
		     $j =0;
				     foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                        <div>
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                          <? if($oMnyContratoPessoa){
						  		if($sOP == "Alterar"){
						 		 	$voMnyContratoPessoaArquivoTipo = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoMovimento($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo(), $oMnyMovimento->getMovCodigo() );
								}
								 //print_r($voMnyContratoPessoaArquivoTipo);
                           		 $nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor();
						     } ?>
                                <div class="col-sm-12">
                                <? if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
                                                  if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
                                                            <input type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><br>
                                                            <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                            <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
                                               <? }
                                                  if($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() && $sOP=='Alterar'){
                                                        echo "
                                                         <div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
                                                         <div class=\"col-sm-2\"><input type=\"file\" data-buttonText=\"Atualizar\" class=\"filestyle \" data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /> </div>
                                                         <div class=\"col-sm-9\"><a   data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=MnyContratoArquivo.processaFormulario&sOP=Excluir&fCodContratoArquivo=".$oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()."
														 &nMovCodigo=".$oMnyMovimento->getMovCodigo()."&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&fContratoTipo=".$_REQUEST['fContratoTipo']." \"><button type=\"button\" class=\"btn btn-danger \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div><hr><br>"; ?>
                                                         <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                <? } else{ ?>
                                                     <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Campo Obrigat&oacute;rio. </div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
												<? }
                                        }?>
                                <? }else{ ?>
                                         <div style="position:relative;" class="col-sm-3">
                                            <a class='btn btn-primary' href='javascript:;'>
                                                Selecione o Arquivo...
                                                  <input type="file"  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' onchange='$("#<?=$j?>").html($(this).val());' data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/>
                                            </a>
                                            &nbsp;
                                        </div>
                                          <input type="hidden" name='fCodContratoArquivo[]' required value="">
                                          <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                          <span class='label label-info' id="<?=$j?>"></span>
                                       <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\" > * Arquivo Obrigat&oacute;rio. </div>" : ""?>
                                <? } ?>
                                </div>
                              <? $i++ ?>
                              <? $j++?>
                        </div>

		  		  <? } ?>
                  <br>
		  </div>

   <!--fim documentos-->
  <? }?>

         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
        		<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  	    <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script src="js/modal-movimento-item.js"></script>

 		<script type="text/javascript" language="javascript">

		$('select[readonly=readonly] option:not(:selected)').prop('disabled', true);

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovDataEmissao").mask("99/99/9999");
			$("#MovDataVencto").mask("99/99/9999");
			$("#fCompetencia").mask("99/9999");
			$("#fMovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovCsll").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIcmsAliq").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovValorGlob").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovPis").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovConfins").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIr").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIrrf").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovInss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovOutrosDesc").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovOutros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});


			//$("#MovDataVencto").mask("99/99/9999");
			$("#MovDataPrev").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovValorPago").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

		});

			jQuery(document).ready(function(){
 				jQuery(".chosen").data("placeholder","Selecione").chosen();
 			});

		function verificaNulo(valor){
			if(valor == null || valor == undefined)
				return 0;
			else
				return valor;
		}

		function calculaValorLiquido(){
//			nValorTotal = document.formFormaPagamento.fValorTotal.value;
			nValor = verificaNulo(document.getElementById('fMovValor').value.replace(".","").replace(",","."));
			nValorPIS = verificaNulo(document.getElementById('fMovPis').value.replace(".","").replace(",","."));
			nValorCONFINS = verificaNulo(document.getElementById('fMovConfins').value.replace(".","").replace(",","."));
			nValorCSLL = verificaNulo(document.getElementById('fMovCsll').value.replace(".","").replace(",","."));
			nValorISS = verificaNulo(document.getElementById('fMovIss').value.replace(".","").replace(",","."));
			nValorIR = verificaNulo(document.getElementById('fMovIr').value.replace(".","").replace(",","."));
			nValorIRRF = verificaNulo(document.getElementById('fMovIrrf').value.replace(".","").replace(",","."));
			nValorINSS = verificaNulo(document.getElementById('fMovInss').value.replace(".","").replace(",","."));
			nValorOUTROS = verificaNulo(document.getElementById('fMovOutros').value.replace(".","").replace(",","."));
			nValorOUTROSDESC = verificaNulo(document.getElementById('fMovOutrosDesc').value.replace(".","").replace(",","."));


			nSoma = nValorPIS*1 + nValorCONFINS*1 + nValorCSLL*1 + nValorISS*1 + nValorIR*1 + nValorIRRF*1 + nValorINSS*1 - nValorOUTROS*1 + nValorOUTROSDESC*1 ;
			//alert (nSoma);
			nValorLiquido = (nValor - nSoma);

	  		nValorLiquidoFormatado = nValorLiquido.toFixed(2);
	   		nValorLiquidoFinal = nValorLiquidoFormatado.replace(".",",");
	  		document.getElementById('valorLiquido').innerHTML = "<strong style='color: red; font-size: 16px;'>" + nValorLiquidoFinal +"</strong><input type='hidden' name='fValorLiquido' id='fValorLiquido' value='"+ nValorLiquidoFormatado +"'>";
			document.getElementById('fMovValor').value = nValorLiquidoFinal;
		}


		function habilita() {
				document.getElementById("avancar").disabled = false;
			}

  function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
					jQuery(function($){
						$("#MovDataVencto").mask("99/99/9999");
						$("#MovDataPrev").mask("99/99/9999");
						$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
						$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					  });
    		}
	});
}




 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>

  <div class="modal fade" id="Objeto" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
          </div>
    </div>
</div>


