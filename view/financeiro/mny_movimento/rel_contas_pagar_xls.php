<?php  set_time_limit(3600); // 30 minutos
 $voVContasPagarXsl = $_REQUEST['voVContasPagar'];

 $nTotal = 0;

// definimos o tipo de arquivo
//header("Content-type: application/msexcel");
header ('Cache-Control: no-cache, must-revalidate');
header ('Pragma: no-cache');
header('Content-Type: application/x-msexcel');

$sNomeArquivo = "Contas_Pagar_".$voVContasPagarXsl[0]->getEmpCodigo()."_".$voVContasPagarXsl[0]->getUniCodigo().".xls";
// Como será gravado o arquivo
header("Content-Disposition: attachment; filename=$sNomeArquivo");

?>
<table border="1" cellpadding="2" cellspacing="2">
     <thead>
          <tr>
            <th height="93" colspan="2"><img src="http://200.98.201.88/webmoney/view/sys/sys_empresa/logomarcas/<?=$_SESSION['oEmpresa']->getEmpImagem()?>"></th>
            <th colspan="12" ><h3>CONTAS A PAGAR</h3></th>
          </tr>
    <? if($voVContasPagarXsl){ ?>
        <th class='Titulo'>F.A</th>
        <th class='Titulo' >FORNECEDOR</th>
        <th class='Titulo'>UNIDADE</th>
        <th class='Titulo'>DESCRI&Ccedil;&Atilde;O DA DESPESA</th>
        <th class='Titulo'>TIPO DOC</th>
        <th class='Titulo'>DOC FISCAL</th>
        <th class='Titulo'>PARC</th>
        <th class='Titulo'>COMPET</th>
        <th class='Titulo'>VENCIMENTO</th>
        <th class='Titulo'>FORMA PGTO</th>
        <th class='Titulo'>VALOR PRINCIPAL</th>
        <th class='Titulo'>JUROS/MULTA</th>
        <th class='Titulo'>DESCONTO</th>
        <th class='Titulo'>VALOR</th>
      </tr>
        </thead>
    <tbody>
    <? foreach($voVContasPagarXsl as $oVContasPagarXsl){  ?>
    <tr>
        <td><?= $oVContasPagarXsl->getMovCodigo() . $oVContasPagarXsl->getMovItem()?></td>
        <td ><?= $oVContasPagarXsl->getPessoa()?></td>
        <td><?= $oVContasPagarXsl->getUnidade()?></td>
        <td><?= $oVContasPagarXsl->getHistorico()?></td>
        <td><?= $oVContasPagarXsl->getTipoDoc()?></td>
        <td><?= $oVContasPagarXsl->getNumeroDocumento()?></td>
        <td><?= $oVContasPagarXsl->getParcela()?></td>
        <td><?= $oVContasPagarXsl->getCompetencia()."."?></td>
        <td><?= $oVContasPagarXsl->getVencimento()?></td>
        <td><?= $oVContasPagarXsl->getFormaPagamento()?></td>
        <td><?= $oVContasPagarXsl->getValorPrincipalFormatado()?></td>
        <td><?= $oVContasPagarXsl->getJurosFormatado()?></td>
        <td><?= $oVContasPagarXsl->getRetencaoDescontoFormatado()?></td>
        <td><? $nTotal += $oVContasPagarXsl->getValorPrincipal() + $oVContasPagarXsl->getJuros() - $oVContasPagarXsl->getRetencaoDesconto();
					echo number_format($oVContasPagarXsl->getValorPrincipal() + $oVContasPagarXsl->getJuros() - $oVContasPagarXsl->getRetencaoDesconto(),2,',','.');
			?></td>
    </tr>
  <? } ?>
    </tbody>
    <tfoot>
    <tr>
		<? echo "<tr><td><strong>TOTAL PER&Iacute;ODO: VALOR </strong>: " . number_format($nTotal, 2, ',', '.') . "</td></tr>";?>
    </tr>
    </tfoot>
<? }?>
</table>
