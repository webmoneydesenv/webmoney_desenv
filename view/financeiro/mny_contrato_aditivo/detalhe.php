﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoAditivo = $_REQUEST['oMnyContratoAditivo'];
 $voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Aditivo - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->
  <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoPessoa.preparaFormulario&sOP=Alterar&Pagina=2&fTipoContrato=1&nIdMnyContratoPessoa=<?= $oMnyContratoAditivo->getContratoCodigo()?>.">Gerenciar Contrato</a> &gt; <strong><?php echo $sOP?> Aditivo</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Aditivo</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyContratoAditivo" action="?action=MnyContratoAditivo.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
		 <input type='hidden' name='fAtivo' value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getAtivo() : "1"?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyContratoAditivo" disabled>
         <div class="form-group">
            <div class="col-sm-6">
            <legend>Dados Gerais</legend>
              <div class="form-group">
                 <div class="col-sm-6">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Numero:</strong><strong style='color: red;'><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getNumero() : ""?></strong></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Motivo: </strong><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getMotivoAditivo() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Contrato: </strong><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getDataAditivo() : ""?></div></div>
				</div>
                <div class="col-sm-6">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data de validade: </strong><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getDataValidade() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Valor: </strong><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getValorAditivo() : ""?></div></div>
				      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Anexo:: </strong><?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getAnexo() : ""?></div></div>
				</div>
             </div>
             <br>
             <div class="col-sm-12">
	<legend title="Documentos">Documentos</legend>
    <? if($_REQUEST['sOP'] == 'Alterar' || $_REQUEST['sOP'] == 'Detalhar'){?>
          <div class="form-group">
          <? $i = 0; ?>
				  <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                        <div>
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                         <? $voMnyContratoPessoaArquivoTipo = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivo($oMnyContratoAditivo->getAditivoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
							$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor(); ?>
                                <div class="col-sm-12">
                                <? if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
											  if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
                                                    <input type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><br>
                                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
										     <? }
											   if($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() && ($_REQUEST['sOP'] =='Alterar' || $_REQUEST['sOP'] =='Detalhar')){
                                                      // atualizar
                                                      if($_REQUEST['sOP'] == 'Detalhar'){
  													    echo"<div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>   ";
													  }else{
  													    echo "
                                                         <div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>"; ?>
                                                         <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
												   <? }
                                                 } else{ ?>
                                                     <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Arquivo Obrigat&oacute;rio.</div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
                                                <? }
                                        }?>
                                <? }else{ ?>
                                             <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Arquivo Obrigat&oacute;rio. </div>" : ""?>
                                             <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                             <input type="hidden" name='fCodContratoArquivo[]' required value="">
                                <? } ?>
                                </div>
                              <? $i++ ?>
                        </div>
                        <p>
                        <p>
		  		  <? } ?>
               <br>
		  </div>
          <? }else{?>
          		<div class="form-group">
	          		<div class="col-sm-10" id="divDocumentos">Selecione o tipo de contrato para carregar os documentos necess&aacute;rios.</div>
    			</div>
          <? }?>
  </div>
</div>

         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyContratoAditivo.preparaLista">Voltar</a></div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
