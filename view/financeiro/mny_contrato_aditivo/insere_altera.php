<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoAditivo = $_REQUEST['oMnyContratoAditivo'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
 $voSysStatus = $_REQUEST['voSysStatus'];
 $voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
 //print_r($voMnyContratoDocTipo);
// die();


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Aditivo - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="recuperaConteudoDinamico('index.php','action=MnyContratoAditivo.DocumentoAjax&nTipoContrato=8','divDocumentos')">
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoPessoa.preparaFormulario&sOP=Alterar&Pagina=2&fTipoContrato=1&nIdMnyContratoPessoa=<?= $oMnyContratoPessoa->getContratoCodigo()?>.">Gerenciar Contrato </a> &gt; <strong><?php echo $sOP?> Aditivo</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Aditivo</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyContratoAditivo"  enctype="multipart/form-data" action="?action=MnyContratoAditivo.processaFormulario">
       	<input type="hidden" name="Pagina" value="2" />
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fAditivoCodigo" value="<?=(is_object($oMnyContratoAditivo)) ? $oMnyContratoAditivo->getAditivoCodigo() : ""?>" />
         <input type="hidden" name='fPesCodigo' required value="<?=(is_object($oMnyContratoAditivo)) ? $oMnyContratoAditivo->getAditivoCodigo() : $oMnyContratoPessoa->getPesCodigo() ?>">
         <input type='hidden' name='fAtivo' value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getAtivo() : "1"?>'/>
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getContratoCodigo() : $oMnyContratoPessoa->getContratoCodigo() ?>'/>
         <input type='hidden' name='fTipoContrato' value='<?= $_REQUEST['fTipoContrato'] ?>'/>


           <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Aditivo">
             <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SysStatus">Status:</label>
					<div class="col-sm-2">
					<select name='fCodStatus'  class="form-control chosen" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysStatus){
							   foreach($voSysStatus as $oSysStatus){
								   if($oMnyContratoAditivo){
									   $sSelected = ($oMnyContratoAditivo->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysStatus->getCodStatus() ?>'><?=  $oSysStatus->getDescStatus()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Numero">Numero:</label>
					<div class="col-sm-5">
					<input class="form-control" type='text' id='Numero' placeholder='Numero' name='fNumero'   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getNumero() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Motivo">Motivo:</label>
					<div class="col-sm-5">
					<input class="form-control" type='text' id='MotivoAditivo' placeholder='Motivo' name='fMotivoAditivo'  required   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getMotivoAditivo() : ""?>'/>
				</div>
				</div>
				<br>
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="DataAditivo">Data:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='DataAditivo' placeholder='Data' name='fDataAditivo'   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getDataAditivoFormatado() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="DataValidade">validade:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='DataValidade' placeholder='Validade' name='fDataValidade'   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getDataValidadeFormatado() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="ValorAditivo">Valor:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='ValorAditivo' placeholder='Valor' name='fValorAditivo'   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getValorAditivoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
			<!--		<label class="col-sm-1 control-label" style="text-align:left" for="Anexo">Anexo:</label>
					<div class="col-sm-3">
					<input  type='file'  class="filestyle" data-buttonName="btn-primary"  data-input="false"  id='Anexo'  name='fAnexo'   value='<?= ($oMnyContratoAditivo) ? $oMnyContratoAditivo->getAnexo() : ""?>' required/>
					</div>-->

<div class="col-sm-12">
 <legend title="Documentos">Documentos</legend>
    <? if($_REQUEST['sOP'] == 'Alterar' || $_REQUEST['sOP'] == 'Detalhar'){?>
          <div class="form-group">
          <? $i = 0; ?>
				  <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                        <div>
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                         <? $voMnyContratoPessoaArquivoTipo = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivo($oMnyContratoAditivo->getAditivoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
							$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor(); ?>
                                <div class="col-sm-12">
                                <? if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
											  if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
                                                    <input type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><br>
                                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
										     <? }
											   if($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() && ($_REQUEST['sOP'] =='Alterar' || $_REQUEST['sOP'] =='Detalhar')){
                                                      // atualizar
                                                      if($_REQUEST['sOP'] == 'Detalhar'){
  													    echo"<div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>   ";
													  }else{
  													    echo "
                                                         <div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
                                                         <div class=\"col-sm-2\"><input type=\"file\" data-buttonText=\"Atualizar\" class=\"filestyle \" data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /> </div>
                                                         <div class=\"col-sm-9\"><a   data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=MnyContratoArquivo.processaFormulario&sOP=Excluir&fCodContratoArquivo=".$oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()."\"><button type=\"button\" class=\"btn btn-danger \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div><hr><br>"; ?>
                                                         <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
												   <? }
                                                 } else{ ?>
                                                     <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Arquivo Obrigat&oacute;rio.</div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
                                                <? }

                                        }?>
                                <? }else{ ?>
                                            <div class="col-sm-12">
                                                <? if($sOP=='Alterar' && ($oMnyContratoAditivo->getAnexo())){ ?>
	                                           <div class="col-sm-1 " ><a target='_blank' href='?action=MnyContratoAditivo.preparaArquivo&fAditivoCodigo=<?=$oMnyContratoAditivo->getAditivoCodigo()?>'><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button>	 </a> </div>
                                                <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">

                                                <? } else{?>
                                                    <div class="col-sm-3">
                                                       <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                                    </div>

                                                <?= ((!($oMnyContratoAditivo->getAnexo())) && $oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div class='col-sm-3' style=\"color:red;\"> * Arquivo Obrigat&oacute;rio. </div>" : ""?>


                                                <? } ?>



                                             <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                             <input type="hidden" name='fCodContratoArquivo[]' required value="">
                                <? } ?>
                                </div>
                              <? $i++ ?>
                        </div>
                        <p>
                        <p>
		  		  <? } ?>
               <br>
		  </div>
          <? }else{?>

	          		<div id="divDocumentos">Selecione o tipo de contrato para carregar os documentos necess&aacute;rios.</div>

          <? }?>
  </div>
</div>




</div>
				<br>

				<br>


         </fieldset>
          <div class="form-group">
                <div class="col-sm-offset-5 col-sm-6">
                <button type="submit" id="btnEnviar" class="btn btn-primary" ><?=$sOP?></button>
                     <div class='col-sm-12' id='divEnviar'></div>
                </div>
   		  </div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#DataAditivo").mask("99/99/9999");
			$("#DataValidade").mask("99/99/9999");
			$("#ValorAditivo").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
