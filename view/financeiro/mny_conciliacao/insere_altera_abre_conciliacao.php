<?php
$voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Concilia&ccedil;&atilde;o - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <link href="css/animate.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Gerenciar Conciliações</a> &gt; <a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Conta Caixa</a>&gt; <strong><?php echo $sOP?> Conciliação</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Abrir Concilia&ccedil;&atilde;o</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyConciliacao"  enctype="multipart/form-data" action="?action=MnyConciliacao.processaFormulario">
<!--   <input type="hidden" name="sOP" id="sOP" value="Consolidar">   -->
       <input type="hidden" id="sOP" name="sOP" value="AbrirConciliacao" />
  <div id="formulario" class="TabelaAdministracao">
   	 <legend>Dados da Concilia&ccedil;&atilde;o</legend>
     <div class="form-group">
        <label class="col-sm-1 control-label" style="text-align:left" for="Observacao">Data:</label>
        <div class="col-sm-2">
            <input class="form-control" type='text' id='fDataConciliacao' placeholder='Data Concilia&ccedil;&atilde;o' name='fDataConciliacao'   value='<?= ($_REQUEST['dData']) ? $_REQUEST['dData'] : ""?>'  />

        </div>
    </div>
    <div class="form-group">
         <fieldset title="Dados da Conciliacao">
            <label class="col-sm-1 control-label" style="text-align:left" for="MnyPlanoContas">Conta:</label>
            <div class="col-sm-5">
                <select name='fCodContaCorrente' id="CodContaCorrente" class="form-control chosen"  required >
                    <option value=''>Selecione</option>
                    <? $sSelected = "";
                       if($voMnyContaCorrente){
                           foreach($voMnyContaCorrente as $oMnyContaCorrente){
                               $sSelected = ($_REQUEST['nCodContaCorrente'] == $oMnyContaCorrente->getCcrCodigo()) ? " selected " : "";
                    ?>
                               <option <?=$sSelected?> value='<?= $oMnyContaCorrente->getCcrCodigo()?>'><?= $oMnyContaCorrente->getCcrConta()?><?= ($oMnyContaCorrente->getCcrConta() == "999") ? " - " . $oMnyContaCorrente->getCcrTipo() : " - " . $oMnyContaCorrente->getCcrContaDv() ?></option>
                    <?
                           }
                       }
                    ?>
                </select>
             </div>
	     </fieldset>
     </div>
        <div class="form-group" style="margin-top:100px;">
            <div class="col-sm-offset-5 col-sm-2">
                <button type="button" id="ok" class="btn btn-primary" onClick="confirmaAbreConciliacao('dsds',1);">Abrir Concilia&ccedil;&atilde;o</button>
            </div>
        </div>
    </div>
      </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#fDataConciliacao").mask("99/99/9999");
			    //$("#ValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });

				function confirmaAbreConciliacao(sLink,n) {
					$("#ModalConciliacao").on("shown.bs.modal", function () {
						//$(this).find('#ok').attr('href', "javascript:document.formMnyConciliacao.submit()");
						document.getElementById('msg').innerHTML = "Deseja mesmo realizar esta opera&ccedil;&atilde;o ?";
                        $(this).find('#ok').attr('href', "javascript:document.formMnyConciliacao.submit()");
					});
					$('#ModalConciliacao').modal('show');
				}



 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalConciliacao" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body" id="msg"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalConciliacao').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
    </div>
    </div>
  </div>
</div>

<div id="ModalExtrato" class="modal fade modal-admin" role="dialog">
  <div class="modal-admin">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Extrato Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body"id="divExtrato">
      	<div >&nbsp;</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
