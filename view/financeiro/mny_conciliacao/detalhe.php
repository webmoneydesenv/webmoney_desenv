<?php
 $voMnyConciliacao = $_REQUEST['voMnyConciliacaoData'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Concilia&ccedil;&atilde;o - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaLista">Gerenciar Concilia&ccedil;&otilde;es</a> &gt; <strong>Detalhar Concilia&ccedil;&atilde;o</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Detalhar Concilia&ccedil;&atilde;o</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnyConciliacao" id="formMnyConciliacao" class="formulario">

   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyConciliacao)){?>
   				<thead>
   				<tr>
   					<th class='Titulo'>Movimento&nbsp;</th>
					<th class='Titulo'>Item</th>
					<th class='Titulo'>Unidade&nbsp;</th>
                    <th class="TItulo">Tipo Caixa</th>
					<th class='Titulo'>Cart&atilde;o de Cr&eacute;dito</th>
                	<th class='Titulo'>Conta&nbsp;</th>
                    <th class="TItulo">Valor Pago&nbsp;</th>
                    <th class="TItulo">N&ordm; Cheque</th>
                    <th class="TItulo">Realizado Por&nbsp;</th>
   				</tr>
   				</thead>
   				<tbody>
                <? foreach($voMnyConciliacao as $oMnyConciliacao){?>
   				<tr>
				    <td><?=  $oMnyConciliacao->getMovCodigo()?></td>
                    <td><?=  $oMnyConciliacao->getMovItem()?></td>
                    <td><?=  $oMnyConciliacao->getMnyPlanoContas()->getDescricao()?></td>
                    <td><?=  $oMnyConciliacao->getMnyTipoCaixa()->getDescricao() ?></td>
                    <td><?=  ($oMnyConciliacao->getCodCartaoCredito()) ? $oMnyConciliacao->getCodCartaoCredito() : "&mdash;"?></td>
                    <td><?=  ($oMnyConciliacao->getCodContaCorrente()) ? $oMnyConciliacao->getMnyContaCorrente()->getCcrConta() : "&mdash;" ?></td>
                    <td><?=  $oMnyConciliacao->getValorTotalTipoCaixaFormatado()?></td>
                    <td><?=  ($oMnyConciliacao->getObservacao()) ?$oMnyConciliacao->getObservacao() : "&mdash;" ?></td>
                    <td><?=  $oMnyConciliacao->getRealizadoPor() ?></td>
  				</tr>
               <? }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyConciliacao)){?>
  			</table>
                    </form>
         <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyConciliacao.preparaLista">Voltar</a></div>
   		 </div>

 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyConciliacao');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
