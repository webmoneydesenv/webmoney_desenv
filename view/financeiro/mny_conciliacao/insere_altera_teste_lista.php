<?php

$voMnyVMovimentoConciliacao = $_REQUEST['voMnyConciliacao'];

$sOP = $_REQUEST['sOP'];
$oMnyConciliacao = $_REQUEST['oMnyConciliacao'];
$voMnyMovimentoItem = $_REQUEST['voMnyMovimentoItem'];
$voMnyMovimento = $_REQUEST['voMnyMovimento'];
$voMnyCartao = $_REQUEST['voMnyCartao'];
$voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
$voMnyTipoCaixa = $_REQUEST['voMnyTipoCaixa'];
$voMnyTipoUnidade = $_REQUEST['voMnyTipoUnidade'];
$voConciliacao =  $_REQUEST['voMnyVMovimentoConciliacao'];
$oFachadaView = new FachadaViewBD();
	if($sOP == "Alterar"){
		if(($oMnyConciliacao) && ($oMnyConciliacao->getConsolidado() == 1 )){
			$sDisabled = "disabled";
		 }else{
			$sDisabled = "";
		 }
	}

//CONTAS DO CAIXINHA
$aCaixinha = array(271,275,276,277,290,291,292,293);


?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Concilia&ccedil;&atilde;o - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <link href="css/animate.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

   <style>
      .placeholder {
        border: 1px solid green;
        background-color: white;
        -webkit-box-shadow: 0px 0px 10px #888;
        -moz-box-shadow: 0px 0px 10px #888;
        box-shadow: 0px 0px 10px #888;
      }

      .grid {
        margin-top: 1em;
      }

      .well {
        min-height: 20px;
        margin-bottom: 20px;
        background-color: #f8f8f8;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      .row {
        margin-right: -15px;
        margin-left: -15px;
      }

     .list-group-item {
        position: relative;
        display: block;
        padding: 10px 60px;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid #ddd;
        display: inline;
        width: 50%;
    }
    .well {
        min-height: 20px;
        padding: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    }
    .row {
        margin: 0px;
    }
    .rowAjax {
        padding: 30px;
    }
    .table-sortable tbody tr {
        cursor: move;
    }
    .ui-dialog, .ui-widget, .ui-widget-content, .ui-corner-all, .ui-front, .ui-draggable, .ui-resizable{z-index: 1055 !important;}

 </style>


 </head>
 <body <?=($_REQUEST['nTipoCaixa']) ? "onLoad=\"retornoConciliacao(". $_REQUEST['nTipoCaixa'] .");verificaConteudoDinamico(". $_REQUEST['nTipoCaixa'] ."); setTimeout( function() {carregavalor()}, 500);\""  : "" ?>   >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Gerenciar Conciliações</a> &gt; <a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Conta Caixa</a>&gt; <strong><?php echo $sOP?> Conciliação</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Concilia&ccedil;&atilde;o</h3>
           <!-- InstanceEndEditable -->  <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post"  class="form-horizontal" name="formMnyConciliacao"  id="formMnyConciliacao" enctype="multipart/form-data" action="?action=MnyConciliacao.processaFormulario">
           <input type="hidden" id="sOP" name="sOP"     value="" />
           <input type="hidden" name="fCodConciliacao"  value="<?=(is_object($oMnyConciliacao)) ? $oMnyConciliacao->getCodConciliacao() : ""?>" />
           <input type='hidden' name='fAtivo'           value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getAtivo() : "1"?>'/>
           <input type='hidden' name='fData'            value='<?=date('d/m/Y')?>'/>
           <input type='hidden' name='fRealizadoPor'    value='<?= $_SESSION['oUsuarioImoney']->getLogin() . " ||" .   date('Y-m-d H:i') ?>'/>
  <div id="formulario" class="TabelaAdministracao">
   <div class="form-group">
      <div class="col-sm-6">
         <fieldset title="Dados da Conciliacao">
         <legend>Dados da Concilia&ccedil;&atilde;o</legend>
            <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Data:</label>
                        <div class="col-sm-4">
                            <input class="form-control" type='text' id='fDataConciliacao' placeholder='Data Concilia&ccedil;&atilde;o' name='fDataConciliacao'   value='<?= ($_REQUEST['dData']) ? $_REQUEST['dData'] : ""?>'  />

                        </div>

                     <div class="box-content">
                          <div class="col-lg-3">
                            <div class="input-group" style="width: 50%;">
                              <span class="input-group-addon">
                                    <input  type='checkbox' id='naoUtil'  name='naoUtil'   value='' onClick="MostraEsconde();"/>
                              </span>
                              <span class="form-control" style="border:#ccc 1px solid"><b>N/&uacute;til</b></span>
                            </div><!-- /input-group -->
                          </div><!-- /.col-lg-6 -->
                     </div>
                     <div class="box-content">
                          <div class="col-lg-3">
                            <div class="input-group" style="width: 50%;">
                              <span class="input-group-addon">
                                    <input  type='checkbox' id='semMovimento'  name='semMovimento'   value='' onClick="MostraEsconde();"/>
                              </span>
                              <span class="form-control" style="border:#ccc 1px solid"><b>S/Movimento</b></span>
                            </div><!-- /input-group -->
                          </div><!-- /.col-lg-6 -->
                     </div>
                </div>
             <div class="form-group">
              		<label class="col-sm-2 control-label"  style="text-align:left" for="MnyContaCorrente">Conta:</label>
					<div class="col-sm-10" >
					<select name='fCodContaCorrente'  id = 'CodContaCorrenteAjax' class="form-control chosen"  required <?=$sDisabled?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyContaCorrente){
							   foreach($voMnyContaCorrente as $oMnyContaCorrente){
								   if($oMnyContaCorrente){
									   $sSelected = ($_GET['nCodContaCorrente'] == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyContaCorrente->getCcrCodigo()?>'><?= $oMnyContaCorrente->getCcrConta() . " -" .  $oMnyContaCorrente->getCcrContaDv()?>  <?= (in_array($oMnyContaCorrente->getCcrCodigo(),$aCaixinha)) ?  $oMnyContaCorrente->getUnidadeCaixinha()->getDescricao() : "" ?>   </option>
						<?
							   }
						   }
						?>
					</select>
                    </div>

             </div>
                <div class='form-group'>
                    <div id='botaoGravar' class="col-sm-offset-5 col-sm-2" >
                        <a href='#' class="btn btn-primary" onClick="carregavalor();"  ><icon class='glyphicon glyphicon-search'></icon>&nbsp;Pesquisar</a>
                     </div>
                </div>

     </div>


        </fieldset>
     </div>
     <!-- <div id="divRendimento">&nbsp;</div><p> -->
    <? if($sOP == "Cadastrar"){?>

    <div class="col-sm-12">
       <div class="form-group">
         <legend>Itens de Pagamento</legend>
       </div>
    <div>

    </div>
    <br>
   </div>
    <br>

       <? }?>

<? if($voConciliacao){ ?>

                 <div class="col-sm-12" id="divTabela">
                    <table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
							<th>A&ccedil;&otilde;es</th>
                            <th>Mov.Item</th>
                            <th>Favorecido</th>
                            <th>Hist&oacute;rico</th>
                            <th>Conta</th>
                            <th>Valor</th>
                            <th>&nbsp;</th>
                        </tr>
                    <?
					  $nValorTotal = 0;
					  foreach($voConciliacao as $oConciliacao){
   					  $nValorTotal = $oConciliacao->getMovValorParcela() + $nValorTotal;
					 ?>
                       <tr>
                            <td><a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oConciliacao->getCodConciliacao()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>
                                <a href="?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimento=<?= $oConciliacao->getMovCodigo()?>" target="_blank" ><i class="glyphicon glyphicon-eye-open"></i></a>
                                <span class="btn btn-primary btn-file btn-xs">
                                    <i class='glyphicon glyphicon-file'></i><input type="file" name="fArquivo" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   >
                                </span>
                            </td>
                            <td><?= $oConciliacao->getMovCodigo()?>.<?= $oConciliacao->getMovItem()?></td>
                            <td><?= $oConciliacao->getNome()?></td>
                            <td><?= $oConciliacao->getHistorico()?></td>
                            <td><?= $oConciliacao->getConta()?></td>
                            <td><?= $oConciliacao->getMovValorParcelaFormatado()?></td>
                            <td><a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oConciliacao->getCodConciliacao()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a></td>
                        </tr>
                    <? } ?>
                    </table>
                    <? $oVMovimentoConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalPorData($oConciliacao->getDataConciliacao()) ?>
                 </div>

              <? }  else {?>

		           		<? if($voMnyVMovimentoConciliacao){ ?>


<div class="form-group">
	<div class="row">
        <div class="table-responsive col-md-12">
        <table id="sort2" class="grid table table-bordered table-sortable">
              <thead>
                <tr>
                    <th width="5%">A&ccedil;&otilde;es</th>
                    <th width="5%">Mov.Item</th>
                    <th width="25%">Favorecido</th>
                    <th width="35%">Hist&oacute;rico</th>
                    <th width="10%">Conta</th>
                    <th width="20%">Valor</th>
                </tr>
             </thead>
            <tbody>
                <? if($voMnyVMovimentoConciliacao){?>
                    <input type="hidden" name="consolidado" id="consolidado" value="<?=$_REQUEST['consolidado']?>">
                    <input type="hidden" name="fDataConciliacao" value="<?= $voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>"/>
                    <? foreach($voMnyVMovimentoConciliacao as $oMnyVMovimentoConciliacao){

                if($oMnyVMovimentoConciliacao->getFinalizado() != 2){
                if($oMnyVMovimentoConciliacao->getMovTipo() == '188'){
                            $nTotalC = $nTotalC + $oMnyVMovimentoConciliacao->getMovValorParcela();
                            $sSinal = '<button type="button" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-minus"></i></button>';
                            $sTipoLancamento="Pagar";
                }else{
                            $nTotalD = $nTotalD + $oMnyVMovimentoConciliacao->getMovValorParcela();
                            $sSinal = '<button type="button" class="btn btn-success btn-circle"><i class="glyphicon glyphicon-plus"></i></button>';
                            $sTipoLancamento="Receber";
                        }
                }else{
                     $sSinal = '<button type="button" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-remove-sign"></i></button>';
                } ?>
                        <tr>
                            <td>
                             <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
                                <!-- <a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oMnyVMovimentoConciliacao->getCodConciliacao()?>&req=1&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>-->
                               <a href="#" onclick="confirmaConciliacao('action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oMnyVMovimentoConciliacao->getCodConciliacao()?>&req=1&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>&dData=<?=$oMnyVMovimentoConciliacao->getDataConciliacaoFormatado()?>&nCodUnidade=<?=$oMnyVMovimentoConciliacao->getCodUnidade()?>&nCodContaCorrente=<?=$oMnyVMovimentoConciliacao->getContaCodigoConc()?>',2)" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash" style='color: red;font-size: large;'></i></a>
                             <? }?>
                                <a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?=$sTipoLancamento?>&nIdMnyMovimentoItem=<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>||<?= $oMnyVMovimentoConciliacao->getMovItem()?>" target="_blank" title="Detalhar Movimento"><i class="glyphicon glyphicon-eye-open" style='color: #0783BB;font-size: large;'></i></a>
                                <a href="?action=SysArquivo.preparaArquivo&sOP=Visualizar&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>" target="_blank" title="Visualizar Arquivo"><i class='glyphicon glyphicon-file' style='color: #8B8989;font-size: large;'></i></a>
                                <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
                                    <a href="#" onclick='confirmaConciliacao("?action=MnyMovimentoItem.extornaItemMovimento&nCodSolicitacao=<?= $oMnyVMovimentoConciliacao->getCodSolicitacaoAporte()?>&fa=<?=$oMnyVMovimentoConciliacao->getMovCodigo()?>||<?= $oMnyVMovimentoConciliacao->getMovItem() ?>&fDataConciliacao=<?= $oMnyVMovimentoConciliacao->getDataConciliacao() ?>&fCodUnidade=<?= $oMnyVMovimentoConciliacao->getCodUnidade() ?>&fCodContaCorrente=<?= $oMnyVMovimentoConciliacao->getCodContaCorrente() ?>",3)'  title="Estornar Item"><i class="glyphicon glyphicon-remove-sign" style='color: #FF8C00;font-size: large;'></i></a>
                                <? } ?>
                            </td>
                            <td>
                                <input type="hidden" name="fFichaAceite[]" value="<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?>"/>
                                <?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?>
                            </td>
                            <td><?= $oMnyVMovimentoConciliacao->getNome()?></td>
                            <td><?= $oMnyVMovimentoConciliacao->getHistorico()?></td>
                            <td><?= $oMnyVMovimentoConciliacao->getConta()?></td>
                            <td><?= $sSinal . " " .$oMnyVMovimentoConciliacao->getMovValorParcelaFormatado()?></td>
                        </tr>
                     <? $i++;
                        } ?>
                <? }?>
      </tbody>
      <tfoot>
          <tr>
              <? if(($voMnyVMovimentoConciliacao && $voMnyVMovimentoConciliacao[0]->getConsolidado() == 0)){?>
                    <td colspan="6">
                        <label class="col-sm-2 control-label" style="text-align:left; font-size:18px;">Saldo Inicial:  </label>
                        <div class="col-sm-10" align="left" style='font-size:18px;'>
                            <?= 'R$ ' . number_format(($oSaldoConta) ? $oSaldoConta->getValorInicial() : 00.00, 2, ',', '.');


                             ?>
                        </div>
                    </td>
              <? } ?>
          </tr>
      </tfoot>
        </table>
        </div>
    </div>


        <? if($_REQUEST['nTipoConciliacao'] == 2 || $_REQUEST['nTipoConciliacao'] == 3){
                if($oMnySaldoDiario){?>
                    <a href="relatorios/?rel=extrato_sm&cod_unidade=<?= ($voMnyVMovimentoConciliacao)? $voMnyVMovimentoConciliacao[0]->getCodUnidade() :  $oMnySaldoDiario->getCodUnidade()?>&conta=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getContaCodigoConc():  $oMnySaldoDiario->getCcrCodigo() ?>&data=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getDataConciliacao() : $oMnySaldoDiario->getData()?>&empresa=<?= $_SESSION['oEmpresa']->getEmpCodigo()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato"><i class="glyphicon glyphicon-duplicate "> Gerar Extrato </i></a>
             <? }
        }elseif($voMnyVMovimentoConciliacao){ ?>
        <td><strong>[<a href="relatorios/?rel=extrato_conciliacao&cod_unidade=<?= ($voMnyVMovimentoConciliacao)? $voMnyVMovimentoConciliacao[0]->getCodUnidade() :  $oMnySaldoDiario->getCodUnidade()?>&conta=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getContaCodigoConc():  $oMnySaldoDiario->getCcrCodigo() ?>&data=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getDataConciliacao() : $oMnySaldoDiario->getData()?>&empresa=<?= $_SESSION['oEmpresa']->getEmpCodigo()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato">Gerar Extrato</a>
            <? if($voMnyVMovimentoConciliacao[0]->getConsolidado() == 1){?>
            | <a href="?action=MnyConciliacao.preparaExtratoBancario&fCodContaCorrente=<?=$voMnyVMovimentoConciliacao[0]->getContaCodigoConc()?>&fCodUnidade=<?=$voMnyVMovimentoConciliacao[0]->getCodUnidade()?>&fData=<?=$voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>" target="_blank">Anexo Bancário</a>]
            <? } ?>
            </strong>
        </td>
     <? }?>

</div>

<?// }?>



              <? } ?>



              <? }

      ?>

        <div class="col-sm-12" id="campoPai"></div>
         <br><br><br><br>
        <div class="form-group">
     		<label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimentoItem">Movimento/Item:</label>
            <div class="col-sm-2">
                    <input type="text" class="form-control" name="fMovimentoItem" value="<?= $nMovimentoItem?>" id="fMovimentoITem" required>
             </div>
             <div class="col-sm-2">
					<button type='button'  onBlur="" class='btn btn-success' onclick='addCampos()' value=''><i class='glyphicon glyphicon-plus'></i></button>
             </div>
             </div>
             <br><br><br>


      <? if($sOP == "Cadastrar"){?>
           	<div class="form-group" style="align:center;">
                 <div class="col-sm-4">&nbsp;</div>
                 <div class=" col-sm-5">
                   <button type="button"  class="btn btn-primary" value="AlterarOrdem" onClick="alteraOrdem()" >Alterar Ordem</button>
                   <button type="submit"  id="btnSalvar" class="btn btn-primary" value="Cadastrar" onClick="muda_sop(this.value);" >Salvar</button>
               	   <button type="button"  class="btn btn-primary" value="Consolidar"  onClick="muda_sop(this.value,);"  data-toggle="modal" data-target="#Conciliacao">Fechar</button>
                </div>
            </div>
      <? } ?>
           <div class="modal fade" id="Conciliacao" role="dialog">
                <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                              <div class="alert alert-danger">
                                  <h4>Encerramento do dia</h4>
                              </div>
                          </div>
                          <br>
                          <br>
                          <? // if($nCaixinha!=1){ //conta <> caixinha ?>
                                    <div id="divAnexoCaixa">
                                    <label class="col-sm-2 control-label" style="text-align:left" for="ExtratoBancario">Anexo:</label>
                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <div style="position:relative;" class="col-sm-12" >
                                             <!-- <a class='btn btn-primary' href='javascript:;' data-toggle="tooltip" title="Anexar Comprovante de Pagamento">Extrato Bancário<input type="file"  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'  onchange='$("#ExtratoBancario").html("Ok");' data-input="false"  id='arquivo' name='arquivo' oninput="document.getElementById('btnRealiza').disabled=false;document.getElementById('fMovimentoITem').required=false;" /></a>&nbsp;<span class='label label-success animated rubberBand' id='ExtratoBancario'></span>-->
                                                <div class='form-group'>
                                                     <div class="col-sm-11">
                                                       <input type='file' id='arquivo'   onblur="verificaUploadConciliacao('arquivo',1048576)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" onchange='$("#ExtratoBancario").html("Ok");'  name='arquivo' oninput="document.getElementById('btnRealiza').disabled=false;document.getElementById('fMovimentoITem').required=false;">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=((1048576/1024)/1024)?>MB</span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        &nbsp;<span style="color:red;"> *Obrigatório para realizar a conciliação bancária</span>
                                                    </div>
                                            </div>

                                        </div>
                                        </div>
                                    </div>
                                </div>
                         <? //} ?>
                            <div class="modal-footer" id="rodape">
                              <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
                              <input type="submit" class="btn btn-success"  disabled  id='btnRealiza'value="Realizar Conciliação Bancaria">
                            </div>
                      </div>
                </div>
            </div>
     </form>
 </div>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">


			 jQuery(document).ready(function(){
				 jQuery(".chosen").data("placeholder","Selecione").chosen();
			 })
			 jQuery(function($){
			   $("#fDataConciliacao").mask("99/99/9999");
			  // $("#iof_0").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });
			 function rendimento(nRendimento){
				document.getElementById('fRendimento').value = nRendimento;
				return true;
			 }
			 function veirifica_ultimo_dia(){
				var nCodUnidade = document.getElementById('CodUnidade').value;
				var nCodContaCorrente = document.getElementById('CodContaCorrenteAjax').value;
				//recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRendimento&nCodContaCorrente=' + nCodContaCorrente  +'&nCodUnidade=' + nCodUnidade ,'divRendimento');
				return true;
			 }

            function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                    oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         oDiv.innerHTML = data;
                        },
                        complete:function(){
                         jQuery(function($){
                                    $("#MovDataPrev").mask("99/99/9999");
                                    $('.mascaraMoeda').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    //usados no modal relatorio...
                                    $("#DataInicial").mask("99/99/9999");
                                    $("#DataFinal").mask("99/99/9999");
                                    $("#Competencia").mask("99/9999");

                                    $("#ValorVale").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    $("#ValorDocPendente").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});


                                    $(".grid").sortable({
                                        tolerance: 'pointer',
                                        revert: 'invalid',
                                        placeholder: 'span2 well placeholder tile',
                                        forceHelperSize: true
                                    });

                                    $(document).ready(function() {
                                        var fixHelperModified = function(e, tr) {
                                            var $originals = tr.children();
                                            var $helper = tr.clone();

                                            $helper.children().each(function(index){
                                                $(this).width($originals.eq(index).width())
                                            });

                                            return $helper;
                                        };

                                        $(".table-sortable tbody").sortable({
                                            helper: fixHelperModified
                                        }).disableSelection();

                                        $(".table-sortable thead").disableSelection();
                                    });

                                 });
                        }
                });
            }
			 function confirmaConciliacao(sLink,n){
				$("#ModalConciliacao").on("shown.bs.modal", function () {
					switch(n){
						case 1:
							$(this).find('#ok').attr('href', "javascript:document.formMnyConciliacao.submit()");
							document.getElementById('msg').innerHTML = "Deseja mesmo realizar esta opera&ccedil;&atilde;o ?";
						break;
						case 2:
							$(this).find('#ok').attr('href', "javascript:recuperaConteudoDinamico('index.php','"+sLink+"','divTabela')");
							document.getElementById('msg').innerHTML = "Deseja mesmo excluir este item ?";
						break;
                        case 3:
							$(this).find('#ok').attr('href', sLink);
							document.getElementById('msg').innerHTML = "Deseja mesmo estornar este item ?";
						break;
					}
				});
				$('#ModalConciliacao').modal('show');
			 }
			 function muda_sop(sOP){
				 if(sOP){
					document.getElementById('sOP').value=sOP;


                    sCaixinha = document.getElementById('caixinha').value;
                    if(sCaixinha==1  ){
                         document.getElementById("divAnexoCaixa").style.display = "none";
                        document.getElementById('btnRealiza').disabled=false;   //ok
                        document.getElementById('fMovimentoITem').required=false;
                        document.getElementById('arquivo').required=false;
                          document.getElementById('anexo').style.display = "none";
                     }
                     return false;
                     //if(document.getElementById('caixinha').value ==1){

                    // }
				 }

			 }

             function alteraOrdem(){
				 document.getElementById('fMovimentoITem').require = false;
                 document.getElementById('sOP').value='AlterarOrdem';
                 //document.formMnyConciliacao().submit();
                  document.getElementById("formMnyConciliacao").submit();
			 }

            function exibeAnexo(val){
                if(document.getElementById('naoUtil').checked == true){
                    document.getElementById('arquivo').required = false;
                    document.getElementById("formMnyConciliacao").submit();
                    //document.formMnyConciliacao().submit();
                }else{
                    document.getElementById('anexo').style.display = "block";
                }

                return false;
            }


			 var qtdeCampos = 0;
			 var cont=0;
			 nMovimentoItem=0;
			 var movimentos = [];

			 function verificaMovimento(nMovimentoItem,movimentos){
				var cont = 0;
				var array_temp=[];
				for (var i = 0; i < movimentos.length; i++){
						if(movimentos[i] == nMovimentoItem){
							cont++;
						}
				 }
				 if(cont > 1){
					cont--;
					return false;

				 }else{
				   return true;
				 }

			 }

			 function removeVerificaMovimento(id){
				array_remove_temp = [];
				for (var i = 0; i < movimentos.length; i++) {
						if( i == id){
							movimentos[i] = '';
						}
				 }
			 }

			 function addCampos(){
				nMovimentoItem = document.getElementById('fMovimentoITem').value;
				movimentos[qtdeCampos]= nMovimentoItem;
				var nCodContaCorrente = document.getElementById('CodContaCorrenteAjax').value;
				//var nCodUnidade = document.getElementById('CodUnidade').value;
				var dConciliacao = document.getElementById('fDataConciliacao').value;
				var rep=0;
				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:

				document.getElementById("filho"+qtdeCampos).innerHTML="<div id='idMovimento_"+qtdeCampos+"_"+nMovimentoItem+"'></div>";
				if(!verificaMovimento(nMovimentoItem,movimentos)){
					rep=1;
				}
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.gravaLinha&rep='+rep+'&idCampo='+qtdeCampos+'&fMovimentoItem=' + nMovimentoItem + '&fCodContaCorrente=' + nCodContaCorrente + '&fDataConciliacao=' + dConciliacao  ,'idMovimento_'+qtdeCampos+"_"+nMovimentoItem);
				qtdeCampos++;

			 }

			 function removerCampo(id){
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);

				removeVerificaMovimento(id);
			 }

			 function carregavalor(){

				// recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaResumo&fDataConciliacao='+document.getElementById("fDataConciliacao").value,'divResumo');
				// setTimeout( function() {
					// if(document.getElementById("CodContaCorrenteAjax").value==''){
					// document.getElementById("CodContaCorrenteAjax").value =;
					// }

					//var nCodContaCorrente = <?= ($_REQUEST['nCodContaCorrente']) ? $_REQUEST['nCodContaCorrente'] : 0 ?>;
					//if(nCodContaCorrente == 0){
						nCodContaCorrente = document.getElementById("CodContaCorrenteAjax").value;
					//}



					 recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTabela&fDataConciliacao='+document.getElementById("fDataConciliacao").value+'&fCodContaCorrente='+nCodContaCorrente,'divTabela');
				// }, 1000);
					// document.getElementById('fDataConciliacao2').value = document.getElementById('fDataConciliacao').value;
					 //document.getElementById('fCodUnidade2').value = document.getElementById('CodUnidade').value;
					 //document.getElementById('CodContaCorrente2').value = document.getElementById('CodContaCorrenteAjax').value;
					 //document.getElementById('TipoCaixa2').value = document.getElementById('TipoCaixa').value;

			  }

			  function verificaConteudoDinamico(valor){
					if(document.getElementById("TipoCaixa").value == 1){ //conciliacao
						var nCodContaCorrente = <?= ($_REQUEST['nCodContaCorrente']) ? $_REQUEST['nCodContaCorrente'] : 0 ?>;
						if(nCodContaCorrente == 0){
							recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value+'&fDataConciliacao='+document.getElementById("fDataConciliacao").value ,'divComplemento');
						}else{
							recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value+'&fDataConciliacao='+document.getElementById("fDataConciliacao").value +'&nCodContaCorrente='+ nCodContaCorrente ,'divComplemento');
						}

						//usado rendimento da aplicacao...
						var data = document.getElementById('fDataConciliacao').value.split('/');
						var dia          = data[0];
						var mes          = data[1];
						var ano          = data[2];
						var ultimoDiaMes = (new Date(ano, mes, 0)).getDate();
						/*
						if(dia == ultimoDiaMes){
							setTimeout( function() {
								recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRendimento&nCodContaCorrente=' + nCodContaCorrente  +'&nCodUnidade=' + document.getElementById("CodUnidade").value ,'divRendimento');
							}, 6000);
						}
				 		*/
						document.getElementById("divCheque").innerHTML="&nbsp;";
					}else if(document.getElementById("TipoCaixa").value == 3){ // cheque
						recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value ,'divCheque');
					}else{
						document.getElementById("divCheque").innerHTML="&nbsp;";
					}
			  }

			  function habilitaCampo(){
				if(document.getElementById('semMovimento').value == "sim"){
					document.getElementById('fDataConcilicao').disabled = true ;
					document.getElementById('CodUnidade').disabled = true;
				}
			  }

			  function MostraEsconde(){
				if(document.getElementById('semMovimento').checked == true){
					document.getElementById('semMovimento').value = "sim";
					document.getElementById('fMovimentoITem').disabled = true;
					document.getElementById('naoUtil').disabled = true;
					document.getElementById('btnSalvar').disabled = true;
				}else if(document.getElementById('naoUtil').checked == true){
					document.getElementById('naoUtil').value = "sim";
					document.getElementById('fMovimentoITem').disabled = true;
					document.getElementById('semMovimento').disabled = true;
					document.getElementById('btnSalvar').disabled = true;

                    document.getElementById('divAnexoCaixa').style.display = "none";
                    document.getElementById('arquivo').required = false;
                    document.getElementById('btnRealiza').disabled=false;
				}else{
					document.getElementById('fMovimentoITem').disabled = false;
					document.getElementById('btnSalvar').disabled = false;
					document.getElementById('semMovimento').disabled = false;
					document.getElementById('naoUtil').disabled = false;

                    document.getElementById('divAnexoCaixa').style.display = "block";
                    document.getElementById('arquivo').required = true;
                    document.getElementById('btnRealiza').disabled=true;
				}
			 }

			 function retornoConciliacao(nTipoCaixa){
				var fCenCodigo = nTipoCaixa;
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fCenCodigo='+fCenCodigo,'divComplemento');
				document.getElementById('divCheque').innerHTML='&nbsp';
				//verificaConteudoDinamico(this.value);

			 }

            function verificaUploadConciliacao(id,limite){
                var fileInput = $('#'+id);

                var maxSize = limite;
                console.log(fileInput.get(0).files[0].size);

                //aqui a sua função normal
                if (fileInput.get(0).files.length) {
                    var fileSize = fileInput.get(0).files[0].size; // in bytes

                    if (fileSize > maxSize) {
                        var b = fileInput.get(0).files[0].size;
                        var msg = ((b/1024) / 1024).toFixed(2);
                        var msg_limite = ((limite/1024) / 1024);
                        document.getElementById('mensagem2').innerHTML='O tamanho maximo permitido para este arquivo e de '+msg_limite+'MB , o seu arquivo contem '+msg+'MB';

                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });

                        document.getElementById('btnRealiza').disabled = true;
                        document.getElementById(id).value="";
                        return false;
                    }else if(fileInput.get(0).files[0].type != 'application/pdf'){
                        document.getElementById('mensagem2').innerHTML='O arquivo tem que ser do tipo PDF!';
                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });
                        document.getElementById(id).value="";
                        document.getElementById('btnRealiza').disabled = true;
                        btnRealiza
                    } else {
                         document.getElementById('btnRealiza').disabled = false;
                       btnRealiza
                    }
                } else {
                    alert('Selecione um arquivo');
                    return false;
                }
            }

</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalConciliacao" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body" id="msg"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalConciliacao').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
    </div>
    </div>
  </div>
</div>

<div id="ModalExtrato" class="modal fade modal-admin" role="dialog">
  <div class="modal-admin">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Extrato Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body"id="divExtrato">
      	<div >&nbsp;</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
