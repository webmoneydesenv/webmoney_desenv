<?php
header("Content-Type: text/html; charset=UTF-8",true);

$oSaldoDiarioAplicacao = $_REQUEST['oSaldoDiarioAplicacao'];
?>
 <legend>Rendimento / Aplica&ccedil;&atilde;o </legend>

 <? if($oSaldoDiarioAplicacao->getValorFormatado() != 0 ){?>
 <div class="form-group">
     <div class="col-sm-6">
        <span style="color: rgb(255, 0, 0);"><marquee>No fechamento do dia é obrigatório Informar o valor do rendimento mensal da aplicacao/conta. </marquee></span>
    </div>
 </div>
 <div class="form-group" >
    <label class="col-sm-2 control-label" style="text-align:left" for="Rendimento">Saldo Aplica&ccedil;&atilde;o:</label>
    <div class="col-sm-2">
       <input type="text" name="fRendimento" id="rendimento" class="form-control" value='<?= ($oSaldoDiarioAplicacao) ? $oSaldoDiarioAplicacao->getValorFormatado() : '0,00'?>' readonly>
    </div>
 </div>
 <div class="form-group" >
    <label class="col-sm-2 control-label" style="text-align:left" for="Rendimento">Rendimento:</label>
    <div class="col-sm-2">
       <input type="text" name="fRendimento"  class="form-control mascaraMoeda" value='' <?=$sRequired?>>
    </div>
 </div>
<? }else{?>
 <div class="form-group">
     <div class="col-sm-6">
        <span style="color: rgb(255, 0, 0);"><marquee>Esta conta n&atilde;o possui aplica&ccedil;&atilde;o. </marquee></span>
    </div>
       <input type="hidden" name="fRendimento" id="rendimento" class="form-control" value='<?= 0 ?>' readonly>
 </div> <? } ?>
