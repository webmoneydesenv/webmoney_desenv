<?php
header("Content-Type: text/html; charset=UTF-8",true);
$voResumo = $_REQUEST['voResumo'];
if($voResumo){
  $nValorTotal = $voResumo[0]->getMovValor() + $voResumo[0]->getContaCodigo() + $voResumo[0]->getPesCodigo();
}
?>

<label class="col-sm-6 control-label" style="text-align:left" >Total Pago</label>
<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <th width="20%">Caixa</th>
            <th width="20%">Cheque</th>
            <th width="20%">Cart&atilde;o </th>
            <th width="20%">C. Corrente</th>
          <th width="20%">Total</th>
        </tr>
        <tr>
            <td><?= ($voResumo[0] && $voResumo[0]->getMovValor()) ? $voResumo[0]->getMovValorFormatado() : "0,00"?></td>
            <td><?= ($voResumo[0] && $voResumo[0]->getContaCodigo()) ? $voResumo[0]->getContaCodigoFormatado() : "0,00"?></td>
            <td> <?= ($voResumo[0] && $voResumo[0]->getPesCodigo()) ? $voResumo[0]->getPesCodigoFormatado() : "0,00"?></td>
            <td> <?= ($voResumo[0] && $voResumo[0]->getCodUnidade()) ? $voResumo[0]->getCodUnidadeFormatado() : "0,00"?></td>
            <td> <?= ($voResumo[0] && $nValorTotal) ? number_format($voResumo[0]->getMovValor() + $voResumo[0]->getContaCodigo() + $voResumo[0]->getPesCodigo() + $voResumo[0]->getCodUnidade(), 2, ',', '.') : "0,00"?></td>
        </tr>
</table>
