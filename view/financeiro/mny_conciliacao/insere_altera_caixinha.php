<?php
$sOP = $_REQUEST['sOP'];
$oMnyConciliacao = $_REQUEST['oMnyConciliacao'];
$voMnyMovimentoItem = $_REQUEST['voMnyMovimentoItem'];
$voMnyMovimento = $_REQUEST['voMnyMovimento'];
$voMnyCartao = $_REQUEST['voMnyCartao'];
$voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
$voMnyTipoCaixa = $_REQUEST['voMnyTipoCaixa'];
$voMnyTipoUnidade = $_REQUEST['voMnyTipoUnidade'];
$voConciliacao =  $_REQUEST['voMnyVMovimentoConciliacao'];
$oFachadaView = new FachadaViewBD();
	if($sOP == "Alterar"){
		if(($oMnyConciliacao) && ($oMnyConciliacao->getConsolidado() == 1 )){
			$sDisabled = "disabled";
		 }else{
			$sDisabled = "";
		 }
	}

//CONTAS DO CAIXINHA
$aCaixinha = array(271,275,276,277,290,291,292,293);

$oSaldoCaixinha = $_REQUEST['oSaldoCaixinha'];


?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Concilia&ccedil;&atilde;o Caixinha - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <link href="css/animate.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

   <style>
      .placeholder {
        border: 1px solid green;
        background-color: white;
        -webkit-box-shadow: 0px 0px 10px #888;
        -moz-box-shadow: 0px 0px 10px #888;
        box-shadow: 0px 0px 10px #888;
      }

      .grid {
        margin-top: 1em;
      }

      .well {
        min-height: 20px;
        margin-bottom: 20px;
        background-color: #f8f8f8;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      .row {
        margin-right: -15px;
        margin-left: -15px;
      }

     .list-group-item {
        position: relative;
        display: block;
        padding: 10px 60px;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid #ddd;
        display: inline;
        width: 50%;
    }
    .well {
        min-height: 20px;
        padding: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    }
    .row {
        margin: 0px;
    }
    .rowAjax {
        padding: 30px;
    }
    .table-sortable tbody tr {
        cursor: move;
    }
    .ui-dialog, .ui-widget, .ui-widget-content, .ui-corner-all, .ui-front, .ui-draggable, .ui-resizable{z-index: 1055 !important;}

 </style>


 </head>
 <body <?=($_REQUEST['nTipoCaixa']) ? "onLoad=\"retornoConciliacao(". $_REQUEST['nTipoCaixa'] .");verificaConteudoDinamico(". $_REQUEST['nTipoCaixa'] ."); setTimeout( function() {carregavalor()}, 500);\""  : "" ?>   >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacaoCaixinha.preparaFormularioCaixinha&sOP=Cadastrar">Gerenciar Conciliações</a> &gt; <a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Conta Caixa</a>&gt; <strong><?php echo $sOP?> Conciliação</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Concilia&ccedil;&atilde;o</h3>
           <!-- InstanceEndEditable -->  <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post"  class="form-horizontal" name="formMnyConciliacao"  id="formMnyConciliacao" enctype="multipart/form-data" action="?action=MnyConciliacaoCaixinha.processaFormularioCaixinha">
           <input type="hidden" id="sOP" name="sOP"     value="" />
           <input type="hidden" name="fCodConciliacao"  value="<?=(is_object($oMnyConciliacao)) ? $oMnyConciliacao->getCodConciliacao() : ""?>" />
           <input type='hidden' name='fAtivo'           value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getAtivo() : "1"?>'/>
           <input type='hidden' name='fData'            value='<?=date('d/m/Y')?>'/>
           <input type='hidden' name='fRealizadoPor'    value='<?= $_SESSION['oUsuarioImoney']->getLogin() . " ||" .   date('Y-m-d H:i') ?>'/>
  <div id="formulario" class="TabelaAdministracao">
   <div class="form-group" >
      <div class="col-sm-12" >
         <fieldset title="Dados da Conciliacao">
         <legend>Dados da Concilia&ccedil;&atilde;o Caixinha</legend>
            <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Data:</label>
                        <div class="col-sm-5">
                            <input class="form-control" type='text' id='fDataConciliacao' placeholder='Data Concilia&ccedil;&atilde;o' name='fDataConciliacao'   value='<?= ($_REQUEST['dData']) ? $_REQUEST['dData'] : ""?>'  />

                        </div>
             </div>
             <div class="form-group">
                 <label class="col-sm-2 control-label" style="text-align:left" for="Observacao">Não Util / Sem Movimentação:</label>
                 <div class="col-sm-4">
                     <div class="box-content">
                          <div class="col-lg-3">
                            <div class="input-group" style="width: 50%;">
                              <span class="input-group-addon">
                                    <input  type='checkbox' id='naoUtil'  name='naoUtil'   value='' onClick="MostraEsconde();"/>
                              </span>
                              <span class="form-control" style="border:#ccc 1px solid"><b>N/&uacute;til</b></span>
                            </div>
                          </div>

                     </div>
                                <div class="box-content">
                          <div class="col-lg-3">
                            <div class="input-group" style="width: 50%;">
                              <span class="input-group-addon">
                                    <input  type='checkbox' id='semMovimento'  name='semMovimento'   value='' onClick="MostraEsconde();"/>
                              </span>
                              <span class="form-control" style="border:#ccc 1px solid"><b>S/Movimento</b></span>
                            </div>
                          </div>
                     </div>
                 </div>
             </div>
             <div class="form-group" style="margin-bottom:40px;">

              		<label class="col-sm-2 control-label"  style="text-align:left" for="MnyContaCorrente">Conta:</label>
					<div class="col-sm-5" >
					<select name='fCodContaCorrente'  id = 'CodContaCorrenteAjax' class="form-control chosen"  required <?=$sDisabled?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyContaCorrente){
							   foreach($voMnyContaCorrente as $oMnyContaCorrente){
								   if($oMnyContaCorrente){
									   $sSelected = ($_GET['nCodContaCorrente'] == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyContaCorrente->getCcrCodigo()?>'><?= $oMnyContaCorrente->getCcrConta() . " -" .  $oMnyContaCorrente->getCcrContaDv()?>  <?= (in_array($oMnyContaCorrente->getCcrCodigo(),$aCaixinha)) ?  $oMnyContaCorrente->getUnidadeCaixinha()->getDescricao() : "" ?>   </option>
						<?
							   }
						   }
						?>
					</select>
                    </div>

             </div>
             <div class='form-group'>

     		<label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimentoItem">Movimento/Item:</label>
            <div class="col-sm-2">
                    <input type="text" class="form-control" name="fMovimentoItem" value="<?= $nMovimentoItem?>" id="fMovimentoITem" >
             </div>
             <div class="col-sm-1">
					<button type='button'  onBlur="" class='btn btn-success' onclick='addCampos()' value=''><i class='glyphicon glyphicon-plus'></i></button>
             </div>
             <div id='botaoGravar' class="col-sm-1" >
                <a href='#' class="btn btn-primary" onClick="carregavalor();" style='width: 220px;' ><icon class='glyphicon glyphicon-search'></icon>&nbsp;Pesquisar</a>
             </div>


                </div>

     </div>


        </fieldset>
     </div>
     <!-- <div id="divRendimento">&nbsp;</div><p> -->
    <? if($sOP == "Cadastrar"){?>

    <div class="col-sm-12">
       <div class="form-group">
         <legend>Itens de Pagamento</legend>
       </div>
    <div>

    </div>
    <br>
   </div>
    <br>

       <? }?>

<? if($voConciliacao){ ?>

                 <div class="col-sm-12" id="divTabela">
                    <table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
							<th>A&ccedil;&otilde;es</th>
                            <th>Mov.Item</th>
                            <th>Favorecido</th>
                            <th>Hist&oacute;rico</th>
                            <th>Conta</th>
                            <th>Valor</th>
                            <th>&nbsp;</th>
                        </tr>
                    <?
					  $nValorTotal = 0;
					  foreach($voConciliacao as $oConciliacao){
   					  $nValorTotal = $oConciliacao->getMovValorParcela() + $nValorTotal;
					 ?>
                       <tr>
                            <td><a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oConciliacao->getCodConciliacao()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>
                                <a href="?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimento=<?= $oConciliacao->getMovCodigo()?>" target="_blank" ><i class="glyphicon glyphicon-eye-open"></i></a>
                                <span class="btn btn-primary btn-file btn-xs">
                                    <i class='glyphicon glyphicon-file'></i><input type="file" name="fArquivo" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   >
                                </span>
                            </td>
                            <td><?= $oConciliacao->getMovCodigo()?>.<?= $oConciliacao->getMovItem()?></td>
                            <td><?= $oConciliacao->getNome()?></td>
                            <td><?= $oConciliacao->getHistorico()?></td>
                            <td><?= $oConciliacao->getConta()?></td>
                            <td><?= $oConciliacao->getMovValorParcelaFormatado()?></td>
                            <td><a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oConciliacao->getCodConciliacao()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a></td>
                        </tr>
                    <? } ?>
                    </table>
                    <? $oVMovimentoConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalPorData($oConciliacao->getDataConciliacao()) ?>
                 </div>

              <? }  else {?>

		           		<div  id="divTabela">&nbsp;</div>


              <? }

      ?>

        <div class="col-sm-12" id="campoPai"></div>

        <div class="form-group">
            <div class='col-sm-12'>
                <span style="background:#dad9d8;">
                <label class="col-sm-2 control-label" style="text-align:left;margin-left: 53%;" for="MnyMovimentoItem">Caixa: <span style='font-size: 20px;color: red;font-weight: bold;'><?= ($oSaldoCaixinha) ? $oSaldoCaixinha->getValorFinalFormatado() : '0,00'?></span></label>
                <input type='hidden' id='saldo' value="<?= ($oSaldoCaixinha) ? $oSaldoCaixinha->getValorFinal() : '0.00'?> ">
                <label class="col-sm-2 control-label" style="text-align:left;" for="MnyMovimentoItem">Disponivel:&nbsp;<span  id='CalcTotal' style='font-size: 20px;color: red;font-weight: bold;'>&nbsp;</span></label>
                </span>
            </div>
        </div>

      <? if($sOP == "Cadastrar"){?>
           	<div class="form-group" style="align:center;">
                 <div class="col-sm-5">&nbsp;</div>

                 <div class=" col-sm-5">
                  <!-- <button type="button"  class="btn btn-primary" value="AlterarOrdem" onClick="alteraOrdem()" >Alterar Ordem</button>-->
                   <button type="submit"  id="btnSalvar" class="btn btn-primary" value="Cadastrar" onClick="muda_sop(this.value);" >Salvar</button>
               	   <button type="button"  class="btn btn-primary" value="Consolidar"  onClick="muda_sop(this.value,);"  data-toggle="modal" data-target="#Conciliacao">Fechar</button>
                </div>
            </div>
      <? } ?>
           <div class="modal fade" id="Conciliacao" role="dialog">
                 <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                          <div class="modal-header">
                              <div class="alert alert-success">
                                  <h4>Fechar conciliação bancária</h4>
                              </div>
                          </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimentoItem"></label>
                                <div class="col-sm-10" style='font-weight:bold;'>Deseja mesmo realizar esta operaçao ?</div>
                            </div>
                            <div class="modal-footer" id="rodape">
                              <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
                              <input type="submit" class="btn btn-primary" id='btnRealiza'value="sim">
                            </div>
                      </div>
                </div>
            </div>
     </form>
 </div>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">


			 jQuery(document).ready(function(){
				 jQuery(".chosen").data("placeholder","Selecione").chosen();
			 })
			 jQuery(function($){
			   $("#fDataConciliacao").mask("99/99/9999");
			  // $("#iof_0").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });
			 function rendimento(nRendimento){
				document.getElementById('fRendimento').value = nRendimento;
				return true;
			 }
			 function veirifica_ultimo_dia(){
				var nCodUnidade = document.getElementById('CodUnidade').value;
				var nCodContaCorrente = document.getElementById('CodContaCorrenteAjax').value;
				//recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaRendimento&nCodContaCorrente=' + nCodContaCorrente  +'&nCodUnidade=' + nCodUnidade ,'divRendimento');
				return true;
			 }

            function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){
                                    oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";
                                },
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                                         oDiv.innerHTML = data;
                        },
                        complete:function(){
                         jQuery(function($){
                                    $("#MovDataPrev").mask("99/99/9999");
                                    $('.mascaraMoeda').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    //usados no modal relatorio...
                                    $("#DataInicial").mask("99/99/9999");
                                    $("#DataFinal").mask("99/99/9999");
                                    $("#Competencia").mask("99/9999");

                                    $("#ValorVale").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                                    $("#ValorDocPendente").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});


                                    $(".grid").sortable({
                                        tolerance: 'pointer',
                                        revert: 'invalid',
                                        placeholder: 'span2 well placeholder tile',
                                        forceHelperSize: true
                                    });

                                    $(document).ready(function() {
                                        var fixHelperModified = function(e, tr) {
                                            var $originals = tr.children();
                                            var $helper = tr.clone();

                                            $helper.children().each(function(index){
                                                $(this).width($originals.eq(index).width())
                                            });

                                            return $helper;
                                        };

                                        $(".table-sortable tbody").sortable({
                                            helper: fixHelperModified
                                        }).disableSelection();

                                        $(".table-sortable thead").disableSelection();
                                    });

                                 });
                        }
                });
            }
			 function confirmaConciliacao(sLink,n){
				$("#ModalConciliacao").on("shown.bs.modal", function () {
					switch(n){
						case 1:
							$(this).find('#ok').attr('href', "javascript:document.formMnyConciliacao.submit()");
							document.getElementById('msg').innerHTML = "Deseja mesmo realizar esta opera&ccedil;&atilde;o ?";
						break;
						case 2:
							$(this).find('#ok').attr('href', "javascript:recuperaConteudoDinamico('index.php','"+sLink+"','divTabela')");
							document.getElementById('msg').innerHTML = "Deseja mesmo excluir este item ?";
						break;
                        case 3:
							$(this).find('#ok').attr('href', sLink);
							document.getElementById('msg').innerHTML = "Deseja mesmo estornar este item ?";
						break;
					}
				});
				$('#ModalConciliacao').modal('show');
			 }
			 function muda_sop(sOP){
				 if(sOP){
					document.getElementById('sOP').value=sOP;


                    sCaixinha = document.getElementById('caixinha').value;
                    if(sCaixinha==1  ){
                         document.getElementById("divAnexoCaixa").style.display = "none";
                        document.getElementById('btnRealiza').disabled=false;   //ok
                        document.getElementById('fMovimentoITem').required=false;
                        document.getElementById('arquivo').required=false;
                          document.getElementById('anexo').style.display = "none";
                     }
                     return false;
                     //if(document.getElementById('caixinha').value ==1){

                    // }
				 }

			 }

             function alteraOrdem(){
				 document.getElementById('fMovimentoITem').require = false;
                 document.getElementById('sOP').value='AlterarOrdem';
                 //document.formMnyConciliacao().submit();
                  document.getElementById("formMnyConciliacao").submit();
			 }

            function exibeAnexo(val){
                if(document.getElementById('naoUtil').checked == true){
                    document.getElementById('arquivo').required = false;
                    document.getElementById("formMnyConciliacao").submit();
                    //document.formMnyConciliacao().submit();
                }else{
                    document.getElementById('anexo').style.display = "block";
                }

                return false;
            }


			 var qtdeCampos = 0;
			 var cont=0;
			 nMovimentoItem=0;
			 var movimentos = [];

			 function verificaMovimento(nMovimentoItem,movimentos){
				var cont = 0;
				var array_temp=[];
				for (var i = 0; i < movimentos.length; i++){
						if(movimentos[i] == nMovimentoItem){
							cont++;
						}
				 }
				 if(cont > 1){
					cont--;
					return false;

				 }else{
				   return true;
				 }

			 }

			 function removeVerificaMovimento(id){
				array_remove_temp = [];
				for (var i = 0; i < movimentos.length; i++) {
						if( i == id){
							movimentos[i] = '';
						}
				 }
			 }

			 function addCampos(){
				nMovimentoItem = document.getElementById('fMovimentoITem').value;
				movimentos[qtdeCampos]= nMovimentoItem;
				var nCodContaCorrente = document.getElementById('CodContaCorrenteAjax').value;
				//var nCodUnidade = document.getElementById('CodUnidade').value;
				var dConciliacao = document.getElementById('fDataConciliacao').value;
				var rep=0;
				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:

				document.getElementById("filho"+qtdeCampos).innerHTML="<div id='idMovimento_"+qtdeCampos+"_"+nMovimentoItem+"'></div>";
				if(!verificaMovimento(nMovimentoItem,movimentos)){
					rep=1;
				}
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.gravaLinha&rep='+rep+'&idCampo='+qtdeCampos+'&fMovimentoItem=' + nMovimentoItem + '&fCodContaCorrente=' + nCodContaCorrente + '&fDataConciliacao=' + dConciliacao  ,'idMovimento_'+qtdeCampos+"_"+nMovimentoItem);
				qtdeCampos++;


                 setTimeout(function(){ CalculaTotal(); }, 1000);

			 }

			 function removerCampo(id){
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);

				removeVerificaMovimento(id);

                CalculaTotal();
			 }

			 function carregavalor(){

				// recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaResumo&fDataConciliacao='+document.getElementById("fDataConciliacao").value,'divResumo');
				// setTimeout( function() {
					// if(document.getElementById("CodContaCorrenteAjax").value==''){
					// document.getElementById("CodContaCorrenteAjax").value =;
					// }

					//var nCodContaCorrente = <?= ($_REQUEST['nCodContaCorrente']) ? $_REQUEST['nCodContaCorrente'] : 0 ?>;
					//if(nCodContaCorrente == 0){
						nCodContaCorrente = document.getElementById("CodContaCorrenteAjax").value;
					//}



					 recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTabela&fDataConciliacao='+document.getElementById("fDataConciliacao").value+'&fCodContaCorrente='+nCodContaCorrente,'divTabela');
				// }, 1000);
					// document.getElementById('fDataConciliacao2').value = document.getElementById('fDataConciliacao').value;
					 //document.getElementById('fCodUnidade2').value = document.getElementById('CodUnidade').value;
					 //document.getElementById('CodContaCorrente2').value = document.getElementById('CodContaCorrenteAjax').value;
					 //document.getElementById('TipoCaixa2').value = document.getElementById('TipoCaixa').value;

			  }

			  function verificaConteudoDinamico(valor){
					if(document.getElementById("TipoCaixa").value == 1){ //conciliacao
						var nCodContaCorrente = <?= ($_REQUEST['nCodContaCorrente']) ? $_REQUEST['nCodContaCorrente'] : 0 ?>;
						if(nCodContaCorrente == 0){
							recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value+'&fDataConciliacao='+document.getElementById("fDataConciliacao").value ,'divComplemento');
						}else{
							recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value+'&fDataConciliacao='+document.getElementById("fDataConciliacao").value +'&nCodContaCorrente='+ nCodContaCorrente ,'divComplemento');
						}

						//usado rendimento da aplicacao...
						var data = document.getElementById('fDataConciliacao').value.split('/');
						var dia          = data[0];
						var mes          = data[1];
						var ano          = data[2];
						var ultimoDiaMes = (new Date(ano, mes, 0)).getDate();
						/*
						if(dia == ultimoDiaMes){
							setTimeout( function() {
								recuperaConteudoDinamico('index.php','action=MnyConciliacaoCaixinha.carregaRendimento&nCodContaCorrente=' + nCodContaCorrente  +'&nCodUnidade=' + document.getElementById("CodUnidade").value ,'divRendimento');
							}, 6000);
						}
				 		*/
						document.getElementById("divCheque").innerHTML="&nbsp;";
					}else if(document.getElementById("TipoCaixa").value == 3){ // cheque
						recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value ,'divCheque');
					}else{
						document.getElementById("divCheque").innerHTML="&nbsp;";
					}
			  }

			  function habilitaCampo(){
				if(document.getElementById('semMovimento').value == "sim"){
					document.getElementById('fDataConcilicao').disabled = true ;
					document.getElementById('CodUnidade').disabled = true;
				}
			  }

			  function MostraEsconde(){
				if(document.getElementById('semMovimento').checked == true){
					document.getElementById('semMovimento').value = "sim";
					document.getElementById('fMovimentoITem').disabled = true;
					document.getElementById('naoUtil').disabled = true;
					document.getElementById('btnSalvar').disabled = true;
				}else if(document.getElementById('naoUtil').checked == true){
					document.getElementById('naoUtil').value = "sim";
					document.getElementById('fMovimentoITem').disabled = true;
					document.getElementById('semMovimento').disabled = true;
					document.getElementById('btnSalvar').disabled = true;

                    document.getElementById('divAnexoCaixa').style.display = "none";
                    document.getElementById('arquivo').required = false;
                    document.getElementById('btnRealiza').disabled=false;
				}else{
					document.getElementById('fMovimentoITem').disabled = false;
					document.getElementById('btnSalvar').disabled = false;
					document.getElementById('semMovimento').disabled = false;
					document.getElementById('naoUtil').disabled = false;

                    document.getElementById('divAnexoCaixa').style.display = "block";
                    document.getElementById('arquivo').required = true;
                    document.getElementById('btnRealiza').disabled=true;
				}
			 }

			 function retornoConciliacao(nTipoCaixa){
				var fCenCodigo = nTipoCaixa;
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fCenCodigo='+fCenCodigo,'divComplemento');
				document.getElementById('divCheque').innerHTML='&nbsp';
				//verificaConteudoDinamico(this.value);

			 }

            function verificaUploadConciliacao(id,limite){
                var fileInput = $('#'+id);

                var maxSize = limite;
                console.log(fileInput.get(0).files[0].size);

                //aqui a sua função normal
                if (fileInput.get(0).files.length) {
                    var fileSize = fileInput.get(0).files[0].size; // in bytes

                    if (fileSize > maxSize) {
                        var b = fileInput.get(0).files[0].size;
                        var msg = ((b/1024) / 1024).toFixed(2);
                        var msg_limite = ((limite/1024) / 1024);
                        document.getElementById('mensagem2').innerHTML='O tamanho maximo permitido para este arquivo e de '+msg_limite+'MB , o seu arquivo contem '+msg+'MB';

                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });

                        document.getElementById('btnRealiza').disabled = true;
                        document.getElementById(id).value="";
                        return false;
                    }else if(fileInput.get(0).files[0].type != 'application/pdf'){
                        document.getElementById('mensagem2').innerHTML='O arquivo tem que ser do tipo PDF!';
                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });
                        document.getElementById(id).value="";
                        document.getElementById('btnRealiza').disabled = true;
                        btnRealiza
                    } else {
                         document.getElementById('btnRealiza').disabled = false;
                       btnRealiza
                    }
                } else {
                    alert('Selecione um arquivo');
                    return false;
                }
            }



           function CalculaTotal() {

                  var v = 0;
                  var atual = 0;

                  $('.soma').each(function(i,e){

                        if ($(e).val()) {

                          var i = $(e).val().replace(/\,/g,'.');

                          if (isNaN(i)) { $(e).val(''); return; }

                          v += parseFloat(i);

                        }

                    });

                    // $('#CalcTotal').text(v.toFixed(2).replace(".",","));
                    atual = ($('#saldo').val() - v);
                    $('#CalcTotal').text(atual.toLocaleString('pt-br', {minimumFractionDigits: 2}));

            }


</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalConciliacao" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body" id="msg"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalConciliacao').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
    </div>
    </div>
  </div>
</div>

<div id="ModalExtrato" class="modal fade modal-admin" role="dialog">
  <div class="modal-admin">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Extrato Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body"id="divExtrato">
      	<div >&nbsp;</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
