<?php
 $sOP = $_REQUEST['sOP'];
 $voMnyConciliacao = $_REQUEST['voMnyConciliacaoData'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Concilia&ccedil;&atilde;o - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaLista">Gerenciar Concilia&ccedil;&otilde;es</a> &gt; <strong><?php echo $sOP?> Concilia&ccedil;&atilde;o</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Concilia&ccedil;&atilde;o</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->

 <? foreach ($voMnyConciliacao  as $oMnyConciliacao){ ?>



   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   				<thead>
   				<tr>
   					<th class='Titulo'>Movimento:&nbsp;</th>
					<th class='Titulo'>Item</th>
					<th class='Titulo'>Unidade:&nbsp;</th>
					<th class='Titulo'>Cart&atilde;o de Cr&eacute;dito</th>
                	<th class='Titulo'>Conta:&nbsp;</th>
                    <th class="TItulo">Data:&nbsp;</th>
                    <th class="TItulo">Valor Pago:&nbsp;</th>
                    <th class="TItulo">Observa&ccedil;&atilde;o:&nbsp;</th>
                    <th class="TItulo">Realizado Por:&nbsp;</th>
   				</tr>
   				</thead>
   				<tbody>
   				<tr>
                    <td><?=  $oMnyConciliacao->getMovCodigo()?></td>
                    <td><?=  $oMnyConciliacao->getMnyPlanoContas()->getDescricao()?></td>
                    <td><?=  $oMnyConciliacao->getMnyTipoCaixa()->getDescricao() ?></td>
                    <td><?=  $oMnyConciliacao->getCodCartaoCredito()?></td>
                    <td><?=  $oMnyConciliacao->getCodContaCorrente()?></td>
                    <td><?=  $oMnyConciliacao->getDataFormatado() ?></td>
                    <td><?=  $oMnyConciliacao->getValorPagoFormatado()?></td>
                    <td><?=  $oMnyConciliacao->getObservacao() ?></td>
                    <td><?=  $oMnyConciliacao->getRealizadoPor() ?></td>
  				</tr>

  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			</table>
			<?php }?>
         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyConciliacao.preparaLista">Voltar</a></div>
   		 </div>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
