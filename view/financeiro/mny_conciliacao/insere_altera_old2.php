<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyConciliacao = $_REQUEST['oMnyConciliacao'];
 $voMnyMovimentoItem = $_REQUEST['voMnyMovimentoItem'];
 $voMnyMovimento = $_REQUEST['voMnyMovimento'];
 $voMnyCartao = $_REQUEST['voMnyCartao'];
 $voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
 $voMnyTipoCaixa = $_REQUEST['voMnyTipoCaixa'];
 $voMnyTipoUnidade = $_REQUEST['voMnyTipoUnidade'];
 $voConciliacao =  $_REQUEST['voConciliacao'];
 $oFachadaView = new FachadaViewBD();

	if($sOP == "Alterar"){
		if($oMnyConciliacao->getConsolidado() == 1 ){
			$sDisabled = "disabled";
		 }else{
			$sDisabled = "";
		 }
	}
?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Conciliação - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaLista">Gerenciar Conciliações</a> &gt; <strong><?php echo $sOP?> Conciliação</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Conciliação</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyConciliacao" action="?action=MnyConciliacao.processaFormulario">
<!--   <input type="hidden" name="sOP" id="sOP" value="Consolidar">   -->
       <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
       <input type="hidden" name="fCodConciliacao" value="<?=(is_object($oMnyConciliacao)) ? $oMnyConciliacao->getCodConciliacao() : ""?>" />
       <input type='hidden' name='fAtivo' value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getAtivo() : "1"?>'/>
	   <input type='hidden' name='fData' required value='<?=date('d/m/Y')?>'/>
	   <input type='hidden' name='fRealizadoPor'  required readonly value='<?= $_SESSION['oUsuarioImoney']->getNome()?>'/>

         <div id="formulario" class="TabelaAdministracao">
   <div class="form-group">
      <div class="col-sm-6">
         <fieldset title="Dados da Conciliacao">
         <legend>Dados da Concilia&ccedil;&atilde;o</legend>

         <? if($sOP == "Alterar"){?>
         	<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:left" for="MovimentoItem">Movimento:</label>
					<div class="col-sm-9">
					<select name='fMovCodigo'  class="form-control chosen"  required  onChange="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaItem&fMovCodigo='+this.value,'divItem')" <?=$sDisabled?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyMovimento){
							   foreach($voMnyMovimento as $oMnyMovimento){
								   if($oMnyConciliacao){
									   $sSelected = ($oMnyConciliacao->getMovCodigo() == $oMnyMovimento->getMovCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyMovimento->getMovCodigo()?>'><?= $oMnyMovimento->getMovCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

               <div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:left" for="MnyMovimentoItem">Item:</label>
					<div class="col-sm-9" id="divItem">
					<select name='fMovItem'  class="form-control chosen"  required <?=$sDisabled?> >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyMovimentoItem){
							   foreach($voMnyMovimentoItem as $oMnyMovimentoItem){
								   if($oMnyConciliacao){
									   $sSelected = ($oMnyConciliacao->getMovItem() == $oMnyMovimentoItem->getMovItem()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyMovimentoItem->getMovItem()?>'><?= $oMnyMovimentoItem->getMovItem()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>
         <? }?>
             <div class="form-group">
                    <label class="col-sm-3 control-label" style="text-align:left" for="Observacao">Data:</label>
                    <div class="col-sm-4">
                        <input class="form-control" type='text' id='fDataConciliacao' placeholder='Data Concilia&ccedil;&atilde;o' name='fDataConciliacao'   value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getDataConciliacaoFormatado() : ""?>'  <?=$sDisabled?>/>
                    </div>
                    <div class="col-sm-4">
                        <input type='button' class="btn btn-primary btn-sm" value="Listar" onClick="carregavalor();">
                    </div>
                </div>
           <div id="esconde" style="display:block">
               <div class="form-group">
                    <label class="col-sm-3 control-label" style="text-align:left" for="MnyPlanoContas">Unidade:</label>
                    <div class="col-sm-9">
                        <select name='fCodUnidade' id="CodUnidade" onChange="verificaConteudoDinamico(this.value)" class="form-control chosen"  required  <?=$sDisabled?> >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyTipoUnidade){
                                   foreach($voMnyTipoUnidade as $oMnyTipoUnidade){
                                       if($oMnyConciliacao){
                                           $sSelected = ($oMnyConciliacao->getCodUnidade() == $oMnyTipoUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                       }
                            ?>
                                       <option  <?= $sSelected?> value='<?= $oMnyTipoUnidade->getPlanoContasCodigo()?>'><?= $oMnyTipoUnidade->getDescricao()?></option>
                            <?
                                   }
                               }
                            ?>
                        </select>
                  </div>
                </div>
                  <div id="esconde2" style="display:block">
                           <div class="form-group">
                                <label class="col-sm-3 control-label"  style="text-align:left" for="MnyTipoCaixa">Tipo de Caixa:</label>
                                <div class="col-sm-9">
                                <select name='fTipoCaixa' id="TipoCaixa" class="form-control chosen"  required    onChange="recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fCenCodigo='+this.value,'divComplemento');document.getElementById('divCheque').innerHTML='&nbsp;';"   \>
                                    <option  value=''>Selecione</option>
                                    <? $sSelected = "";
                                       if($voMnyTipoCaixa){
                                           foreach($voMnyTipoCaixa as $oMnyTipoCaixa){
                                               if($oMnyConciliacao){
                                                   $sSelected = ($oMnyConciliacao->getTipoCaixa() == $oMnyTipoCaixa->getCodTipoCaixa()) ? "selected" : "";
                                               }
                                    ?>
                                               <option  <?= $sSelected?> value='<?= $oMnyTipoCaixa->getCodTipoCaixa()?>'><?= $oMnyTipoCaixa->getDescricao()?></option>
                                    <?
                                           }
                                       }
                                    ?>
                                </select>
                            </div>
                            </div>
                  </div>
				<br>
            </div>
                 <div class="form-group">
                 <? if( $OP=="Alterar" ) {?>
					<label class="col-sm-3 control-label" style="text-align:left" for="MnyCartao">Número Cartão: </label>
					<div class="col-sm-9">
					<select name='fCodCartaoCredito' class="form-control chosen"  required <?=$sDisabled?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyCartao){
							   foreach($voMnyCartao as $oMnyCartao){
								   if($oMnyConciliacao){
									   $sSelected = ($oMnyConciliacao->getCodCartaoCredito() == $oMnyCartao->getCodCartao()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyCartao->getCodCartao()?>'><?= $oMnyCartao->getCodCartao()?></option>
						<?
							   }
						   }
						?>
					</select>
                    </div>
					<label class="col-sm-3 control-label"  style="text-align:left" for="MnyContaCorrente">Conta:</label>
					<div class="col-sm-9" >
					<select name='fCodContaCorrente'  class="form-control chosen"  required <?=$sDisabled?>>
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyContaCorrente){
							   foreach($voMnyContaCorrente as $oMnyContaCorrente){
								   if($oMnyConciliacao){
									   $sSelected = ($oMnyConciliacao->getCodContaCorrente() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyContaCorrente->getCcrCodigo()?>'><?= $oMnyContaCorrente->getCcrConta()?></option>
						<?
							   }
						   }
						?>
					</select>
                    </div>

					<label class="col-sm-3 control-label" style="text-align:left" for="ValorPago">Valor Pago:</label>
					<div class="col-sm-9" >
					<input class="form-control" type='text' id='ValorPago' placeholder='Valor Pago' name='fValorPago'  required   value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getValorPagoFormatado() : ""?>' <?=$sDisabled?>/>
				</div>
					<label class="col-sm-3 control-label" style="text-align:left" for="Observacao">Numero do Cheque:</label>
					<div class="col-sm-9">
					<input class="form-control" type='text' id='Observacao' placeholder='N&deg; do Cheque' name='fObservacao'   value='<?= ($oMnyConciliacao) ? $oMnyConciliacao->getObservacao() : ""?>' <?=$sDisabled?>/>
				</div>
                   <? } else{ ?>
                   <div id="divComplemento">&nbsp;</div>
                   <div>&nbsp;</div>
                   <div id="divCheque">&nbsp;</div>
                   <? } ?>
                 </div>
       </div>
          <div class="form-group col-sm-6">
          <legend>Resumo de Pagamento/Dia</legend>
          <? if(!(isset($voConciliacao))){?>
              <? $oVMovimentoConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalPorData($voConciliacao[0]->getDataConciliacao()) ?>
          <? }?>
           <div id="divResumo">
              <label class="col-sm-6 control-label" style="text-align:left" >Total Pago</label>
                <table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Caixa</th>
                            <th>Cheque</th>
                            <th>Cart&atilde;o de Credito</th>
                            <th>Total</th>
                        </tr>
                        <tr>
                            <td><?= ($oVMovimentoConciliacao[0]) ? $oVMovimentoConciliacao[0]->getMovValor() : "R$ 0,00"?></td>
                            <td><?= ($oVMovimentoConciliacao[0]) ? $oVMovimentoConciliacao[0]->getContaCodigo() : "R$ 0,00" ?></td>
                            <td> <?= ($oVMovimentoConciliacao[0]) ? $oVMovimentoConciliacao[0]->getPesCodigo() : "R$ 0,00"?></td>
                            <td> <?= ($oVMovimentoConciliacao) ? number_format($oVMovimentoConciliacao[0]->getMovValor() + $oVMovimentoConciliacao[0]->getContaCodigo() + $oVMovimentoConciliacao[0]->getPesCodigo(), 2, ',', '.') : "0,00"?></td>
                        </tr>
                </table>
           </div>
           </div>
        </fieldset>
     </div>
       <div class="form-group">
                <div class=" col-sm-1">
                	<button type="submit" class="btn btn-primary" >Salvar</button>
                </div>
                <div class=" col-sm-2">
              <!-- <button type="submit"  class="btn btn-primary" onClick="consolidar();">Fechamento\Extrato</button>-->
                   <a href="#" data-toggle="modal" data-target="#ModalConciliacao"   class="btn btn-primary">Fechamento/Extrato</a>
                </div>
                <div class="box-content">
                      <div class="col-lg-2">
                        <div class="input-group">
                          <span class="input-group-addon">
             					<input  type='checkbox' id='naoUtil'  name='naoUtil'   value='' onClick="MostraEsconde2('esconde');"/>
                          </span>
                          <span class="form-control" style="border:#ccc 1px solid"><b>N&atilde;o &uacute;til</b></span>
                        </div><!-- /input-group -->
                      </div><!-- /.col-lg-6 -->
                 </div>
                 <div class="box-content">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <span class="input-group-addon">
               					<input  type='checkbox' id='semMovimento'  name='semMovimento'   value='' onClick="MostraEsconde3('esconde2');"/>
                          </span>
                          <span class="form-control" style="border:#ccc 1px solid"><b>Sem Movimento</b></span>
                        </div><!-- /input-group -->
                      </div><!-- /.col-lg-6 -->
                 </div>
	   </div>
      <? if($sOP == "Cadastrar"){?>
           <div class="col-sm-12">
               <div class="form-group">
                 <legend>Itens de Pagamento</legend>
               </div>
               <div>
			   <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimentoItem">Movimento/Item:</label>
					<div class="col-sm-2" id="divItem">
						<input type="text" class="form-control" name="fMovimentoItem" value="<?= $nMovimentoItem?>" id="fMovimentoITem" required>
				</div>
				</div>
				<br>
               </div>
                 <br>
       <? }?>
        </form>

  <form method="post" class="form-horizontal" name="formConciliacao" id="formConciliacao" action="?action=MnyConciliacao.processaFormulario">
  <input type="hidden" name="sOP"  value="Consolidar"/>
  <input type="hidden" id="semMovimento2" name="semMovimento"  value=""/>
  <input type="hidden" id="fCodUnidade2" name="fCodUnidade" value=""/>
  <input type="hidden" id="naoUtil2" name="naoUtil"  value=""/>
  <input type="hidden" id="fDataConciliacao2" name="fDataConciliacao"  value=""/>
<? if($voConciliacao){ ?>

                 <div id="divTabela">
                    <table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
							<th>N/A</th>
                            <th>Mov.Item</th>
                            <th>Favorecido</th>
                            <th>Hist&oacute;rico</th>
                            <th>Conta</th>
                            <th>Valor</th>
                            <th>&nbsp;</th>
                        </tr>
                    <?
					  $nValorTotal = 0;
					  foreach($voConciliacao as $oConciliacao){
   					  $nValorTotal = $oConciliacao->getMovValor() + $nValorTotal;
					 ?>
                       <tr>
                            <td>N/A</td>
                            <td><?= $oConciliacao->getMovCodigo()?>.<?= $oConciliacao->getMovItem()?></td>
                            <td><?= $oConciliacao->getNome()?></td>
                            <td><?= $oConciliacao->getHistorico()?></td>
                            <td><?= $oConciliacao->getConta()?></td>
                            <td><?= $oConciliacao->getMovValorFormatado()?></td>
                            <td><a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oConciliacao->getCodConciliacao()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a></td>
                        </tr>
                    <? } ?>
                    </table>
                    <? $oVMovimentoConciliacao = $oFachadaView->recuperarTodosVMovimentoConciliacaoTotalPorData($oConciliacao->getDataConciliacao()) ?>
                 </div>
              <? }  else {?>
		           <div id="divTabela">&nbsp;</div>
              <? } ?>
      </form>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#fDataConciliacao").mask("99/99/9999");
			    //$("#ValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });

				function confirmaConciliacao(sLink) {
					$("#ModalConciliacao").on("shown.bs.modal", function () {
					  //$(this).data('href', $(this).attr('href')).attr('href', '#');
						$(this).find('#ok').attr('href', sLink);
					});
					$('#ModalConciliacao').modal('show');
				}

				function carregavalor(){
					     recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaResumo&fDataConciliacao='+document.getElementById("fDataConciliacao").value,'divResumo');
					 setTimeout( function() {
						 recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTabela&fDataConciliacao='+document.getElementById("fDataConciliacao").value+'&fCodUnidade='+document.getElementById("CodUnidade").value,'divTabela');
				      }, 1000);
				 }




		function verificaConteudoDinamico(valor){
			if(document.getElementById("TipoCaixa").value == 1){ // conciliacao
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value+'&fDataConciliacao='+document.getElementById("fDataConciliacao").value ,'divComplemento');
				document.getElementById("divCheque").innerHTML="&nbsp;";
			}else if(document.getElementById("TipoCaixa").value == 3){ // cheque
				recuperaConteudoDinamico('index.php','action=MnyConciliacao.carregaTipo&fUniCodigo='+document.getElementById("CodUnidade").value ,'divCheque');
			}else{
				document.getElementById("divCheque").innerHTML="&nbsp;";
			}
	    }
	function habilitaCampo(){
		if(document.getElementById('semMovimento').value == "sim"){
			document.getElementById('fDataConcilicao').disabled = true ;
			document.getElementById('CodUnidade').disabled = true;
		}
	}

	function MostraEsconde2(div){
		if(document.getElementById('semMovimento').checked == true){
			document.getElementById('semMovimento2').value = "sim";
			document.getElementById('fDataConciliacao2').value = document.getElementById('fDataConciliacao').value;
			document.getElementById('fCodUnidade2').value = document.getElementById('CodUnidade').value;
			document.getElementById('naoUtil').disabled = true;
		}else{
			document.getElementById('semMovimento2').value = "nao";
			document.getElementById('fDataConciliacao2').value = "";
			document.getElementById('naoUtil').disabled = false;
		}
		if(document.getElementById('naoUtil').checked == true){
			document.getElementById('naoUtil2').value = "sim";
			document.getElementById('fDataConciliacao2').value = document.getElementById('fDataConciliacao').value;
			document.getElementById('semMovimento').disabled = true;
		}else{
			document.getElementById('naoUtil2').value = "nao";
			document.getElementById('fDataConciliacao2').value = "";
			document.getElementById('semMovimento').disabled = false;
		}
		if(document.getElementById(div).style.display == "none"){
			document.getElementById(div).style.display = "block";
		}else if(document.getElementById(div).style.display == "block"){
			document.getElementById(div).style.display = "none";;
		}
}

	function MostraEsconde3(div){
		if(document.getElementById('semMovimento').checked == true){
			document.getElementById('semMovimento2').value = "sim";
			document.getElementById('fDataConciliacao2').value = document.getElementById('fDataConciliacao').value;
			document.getElementById('fCodUnidade2').value = document.getElementById('CodUnidade').value;
			document.getElementById('naoUtil').disabled = true;
		}else{
			document.getElementById('semMovimento2').value = "nao";
			document.getElementById('fDataConciliacao2').value = "";
			document.getElementById('naoUtil').disabled = false;
		}

		if(document.getElementById(div).style.display == "none"){
			document.getElementById(div).style.display = "block";
		}else if(document.getElementById(div).style.display == "block"){
			document.getElementById(div).style.display = "none";;
		}
	}



	</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalConciliacao" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body">
        <p>Deseja mesmo realizar esta concilia&ccedil;&atilde;o ?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="javascript:document.formConciliacao.submit();" class="btn btn-primary btn-ok" id="ok">Sim</a>
      </div>
    </div>
  </div>
</div>
