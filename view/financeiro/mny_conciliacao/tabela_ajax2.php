<?php
header("Content-Type: text/html; charset=UTF-8",true);
// $voMnyVMovimentoConciliacao = $_REQUEST['voMnyVMovimentoConciliacao'];

  $voMnyVMovimentoConciliacao = $_REQUEST['voMnyVMovimentoConciliacao'];
  $oMnySaldoDiario = $_REQUEST['oMnySaldoDiario'];

 $oSaldoConta = $_REQUEST['oSaldoConta'];

 $Max = $_REQUEST['nMaiorOrdem'];
 $Min = $_REQUEST['nMenorOrdem'];

?>
<? if($voMnyVMovimentoConciliacao){?>
	<? if($voMnyVMovimentoConciliacao[0]->getConsolidado() == 1){?>
        <div class="cols-sm-12">
            <h4 style="color:red;font-weight:bold;">*Já foi realizada a concilia&ccedil;&atilde;o para esta data/unidade/tipo caixa</h4>
        </div>
    <? }?>
<input type="hidden" name="fDataConciliacao" value="<?= $voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>"/>
<input type="hidden" name="sOP"  value="Consolidar"/>
<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
			<th width="19%">A&ccedil;&otilde;es</th>
            <th width="15%">Mov.Item</th>
            <th width="16%">Favorecido</th>
            <th width="13%">Hist&oacute;rico</th>
            <th width="9%">Conta</th>
            <th width="8%">Valor</th>
		<? if($voMnyVMovimentoConciliacao[0]->getConsolidado() == 0){?>
        	   <th width="12%">Ordenar</th>
        <? }?>
        </tr>
     </thead>
 <? $total = 0;
	$i = 0;?>
	<tbody>
	<? foreach($voMnyVMovimentoConciliacao as $oMnyVMovimentoConciliacao){
		if($oMnyVMovimentoConciliacao->getMovTipo == 188){
			$nTotalC = $oMnyVMovimentoConciliacao->getMovValorParcela() + $nTotalC;
		}else{
			$nTotalD = $oMnyVMovimentoConciliacao->getMovValorParcela() + $nTotalD;
		}?>
        <tr>
            <td>
			 <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
            	<a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oMnyVMovimentoConciliacao->getCodConciliacao()?>&req=1&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>
             <? }?>
                <a href="?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimento=<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>" target="_blank" title="Detalhar Movimento"><i class="glyphicon glyphicon-eye-open"></i></a>
	            <a href="<?= $oMnyVMovimentoConciliacao->getCaminhoArquivo()?>" target="_blank" title="Visualizar Arquivo"><i class='glyphicon glyphicon-file'></i></a>
            </td>
            <td><?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?></td>
            <td><?= $oMnyVMovimentoConciliacao->getNome()?></td>
            <td><?= $oMnyVMovimentoConciliacao->getHistorico()?></td>
            <td><?= $oMnyVMovimentoConciliacao->getConta()?></td>
            <td><?= $oMnyVMovimentoConciliacao->getMovValorParcelaFormatado()?></td>
	   <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
			 <? if ($Max->getOrdem() == $oMnyVMovimentoConciliacao->getOrdem()) {?>
                <td align="center"><i class="glyphicon glyphicon-arrow-up"></i><a href='?action=MnyConciliacao.processaFormulario&sOP=AlterarOrdem&ordem=<?=$oMnyVMovimentoConciliacao->getOrdem()?>&sentido=baixo&nId=<?=$oMnyVMovimentoConciliacao->getCodConciliacao()?>'><i class="glyphicon  glyphicon-arrow-down"></i></a></td>
             <? }else if($Min->getOrdem() == $oMnyVMovimentoConciliacao->getOrdem()){ ?>
                <td width="1%" align="center"><a href='?action=MnyConciliacao.processaFormulario&sOP=AlterarOrdem&ordem=<?=$oMnyVMovimentoConciliacao->getOrdem()?>&sentido=cima&nId=<?=$oMnyVMovimentoConciliacao->getCodConciliacao()?>'><i class="glyphicon glyphicon-arrow-up"></i></a><i class="glyphicon  glyphicon-arrow-down"></i></td>
             <? }else{ ?>
                <td width="1%" align="center"><a href='?action=MnyConciliacao.processaFormulario&sOP=AlterarOrdem&ordem=<?=$oMnyVMovimentoConciliacao->getOrdem()?>&sentido=cima&nId=<?=$oMnyVMovimentoConciliacao->getCodConciliacao()?>'><i class="glyphicon glyphicon-arrow-up"></i></a>
                 <a href='?action=MnyConciliacao.processaFormulario&sOP=AlterarOrdem&ordem=<?=$oMnyVMovimentoConciliacao->getOrdem()?>&sentido=baixo&nId=<?=$oMnyVMovimentoConciliacao->getCodConciliacao()?>'><i class="glyphicon  glyphicon-arrow-down"></i></a></td>
             <? }
	   	   }?>
        </tr>
	 <? $i++;
        } ?>
      </tbody>
      <tfoot>
      	<tr>
          <td><a href="relatorios/?rel=extrato_conciliacao&cod_unidade=<?=  $voMnyVMovimentoConciliacao[0]->getCodUnidade()?>&conta=<?=  $voMnyVMovimentoConciliacao[0]->getContaCodigoConc() ?>&data=<?= $voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato"><i class="glyphicon glyphicon-duplicate "> Gerar Extrato </i></a></td>
        </tr>
      </tfoot>

</table>
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Total Itens:  </label>
		<div class="col-sm-10" align="left" ><?= 'R$ ' . number_format($nTotalC + $nTotalD, 2, ',', '.'); ?></div>
    </div>
<!--<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Total Itens Credito:  </label>
		<div class="col-sm-10" align="left"  style="color:blue;"><?//= 'R$ ' . number_format($nTotalC, 2, ',', '.'); ?></div>
    </div>
    	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Total Itens Debito:  </label>
		<div class="col-sm-10" align="left" style="color:red;"><?//= 'R$ ' . number_format($nTotalD, 2, ',', '.'); ?></div>
    </div>  -->
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Saldo Conta:  </label>
		<div class="col-sm-10" align="left" ><?= 'R$ ' . number_format($oSaldoConta->getValorInicial(), 2, ',', '.'); ?></div>
    </div>
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Saldo Final Conta:  </label>
		<div class="col-sm-10" align="left" ><?= 'R$ ' . number_format($oSaldoConta->getValorFinal() , 2, ',', '.'); ?></div>
    </div>
<? }else{?>
<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
			<th width="19%">A&ccedil;&otilde;es</th>
            <th width="15%">Mov.Item</th>
            <th width="16%">Favorecido</th>
            <th width="13%">Hist&oacute;rico</th>
            <th width="9%">Conta</th>
            <th width="8%">Valor</th>
        	<th width="12%">Ordenar</th>
        </tr>
     </thead>
	 <tbody>
      </tbody>
      <tfoot>
    	<? if($oMnySaldoDiario){ // continuar aqui...?>
        <tr>
          <td colspan="7" style="font-weight:bold;color:red;font-size:18px;">Dia <?=($oMnySaldoDiario->getTipoSaldo() == 2)?"sem movimento.":"n&atilde;o util."?></td>
        </tr>
      	<tr>
          <td><a href="relatorios/?rel=extrato_sm&cod_unidade=<?=  $oMnySaldoDiario->getCodUnidade()?>&conta=<?=  $oMnySaldoDiario->getCcrCodigo() ?>&data=<?= $oMnySaldoDiario->getData()?>&emp_codigo=<?=$_SESSION['oEmpresa']->getEmpCodigo()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato"><i class="glyphicon glyphicon-duplicate "> Gerar Extrato </i></a></td>
		<? }else{?>
           <td colspan="7">N&atilde;o h&aacute; concilia&ccedil;&otilde;es para esta data.</td>
        <? }?>
        </tr>
      </tfoot>
</table>
</form>
<? }?>

<script src="js/bootstrap-filestyle.min.js"></script>
