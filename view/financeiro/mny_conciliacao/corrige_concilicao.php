<?php

$voMnyVMovimentoConciliacao = $_REQUEST['voMnyConciliacao'];

$oMnyContaCorrente =
$voMnyTipoCaixa = $_REQUEST['voMnyTipoCaixa'];
$voMnyTipoUnidade = $_REQUEST['voMnyTipoUnidade'];
$voConciliacao =  $_REQUEST['voMnyVMovimentoConciliacao'];
$oMnySaldoDiario = $_REQUEST['oMnySaldoDiario'];



?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Reconciliação></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/style_checkbox.css" rel="stylesheet" type="text/css">
 <link href="css/animate.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

   <style>

      .btn-circle {
          width: 20px;
          height: 20px;
          text-align: center;
          padding: 2px 0;
          font-size: 10px;
          line-height: 1.428571429;
          border-radius: 15px;
        }

      .placeholder {
        border: 1px solid green;
        background-color: white;
        -webkit-box-shadow: 0px 0px 10px #888;
        -moz-box-shadow: 0px 0px 10px #888;
        box-shadow: 0px 0px 10px #888;
      }

      .grid {
        margin-top: 1em;
      }

      .well {
        min-height: 20px;
        margin-bottom: 20px;
        background-color: #f8f8f8;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }

      .row {
        margin-right: -15px;
        margin-left: -15px;
      }

      .list-group-item {
        position: relative;
        display: block;
        padding: 10px 60px;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid #ddd;
        display: inline;
        width: 50%;
    }

      .well {
        min-height: 20px;
        padding: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    }

      .row {
        margin: 0px;
    }

      .rowAjax {
        padding: 30px;
      }

      .table-sortable tbody tr {
        cursor: move;
    }

      .ui-dialog, .ui-widget, .ui-widget-content, .ui-corner-all, .ui-front, .ui-draggable, .ui-resizable{z-index: 1055 !important;}

      .bold{
           font-weight: bold;
       }

 </style>


 </head>
 <body >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Gerenciar Conciliações</a> &gt; <a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Conta Caixa</a>&gt; <strong><?php echo $sOP?> Conciliação</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
           <h3 class="TituloPagina"><?php echo $sOP?> Concilia&ccedil;&atilde;o da conta <span class='bold'><?= $oMnySaldoDiario->getMnyContaCorrente()->getCcrConta() ."-". $oMnySaldoDiario->getMnyContaCorrente()->getCcrContaDv()?></span> no dia <span class="bold"><?=$oMnySaldoDiario->getDataFormatado();?></span></h3>
           <!-- InstanceEndEditable -->  <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post"  class="form-horizontal" name="formMnyConciliacao"  id="formMnyConciliacao" enctype="multipart/form-data" action="?action=MnyConciliacao.processaFormularioReconciliacao">
           <input type="hidden" id="sOP" name="sOP"     value="Alterar" />
           <input type="hidden" name="fCodConciliacao"  value="<?=(is_object($oMnyConciliacao)) ? $oMnyConciliacao->getCodConciliacao() : ""?>" />
           <input type='hidden' name='fFA'           value='<?= $_REQUEST['nIdMnyMovimentoItem'] ?>'/>
           <input type='hidden' name='fSaldo'           value='<?=$voMnyVMovimentoConciliacao[0]->getContaCodigoConc() .'||'.$voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>'/>
           <input type='hidden' name='fRealizadoPor'    value='<?= $_SESSION['oUsuarioImoney']->getLogin() . " ||" .   date('Y-m-d H:i') ?>'/>
  <div id="formulario" class="TabelaAdministracao">
     <div class="form-group">
          <div class="col-sm-12">
             <fieldset title="Dados da Conciliacao">
                <legend>Reordenar Movimentos</legend>
             </fieldset>
          </div>
     </div>


<? if($voMnyVMovimentoConciliacao){ ?>


<div class="form-group">
	<div class="row">
        <div class="table-responsive col-md-12">
        <table id="sort2" class="grid table table-bordered table-sortable">
              <thead>
                <tr>
                    <th width="5%">A&ccedil;&otilde;es</th>
                    <th width="5%">Mov.Item</th>
                    <th width="25%">Favorecido</th>
                    <th width="35%">Hist&oacute;rico</th>
                    <th width="10%">Conta</th>
                    <th width="20%">Valor</th>
                </tr>
             </thead>
            <tbody>
                <? if($voMnyVMovimentoConciliacao){?>
                    <input type="hidden" name="consolidado" id="consolidado" value="<?=$_REQUEST['consolidado']?>">
                    <input type="hidden" name="fDataConciliacao" value="<?= $voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>"/>
                    <? foreach($voMnyVMovimentoConciliacao as $oMnyVMovimentoConciliacao){

                if($oMnyVMovimentoConciliacao->getFinalizado() != 2){
                if($oMnyVMovimentoConciliacao->getMovTipo() == '188'){
                            $nTotalC = $nTotalC + $oMnyVMovimentoConciliacao->getMovValorParcela();
                            $sSinal = '<button type="button" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-minus"></i></button>';
                            $sTipoLancamento="Pagar";
                }else{
                            $nTotalD = $nTotalD + $oMnyVMovimentoConciliacao->getMovValorParcela();
                            $sSinal = '<button type="button" class="btn btn-success btn-circle"><i class="glyphicon glyphicon-plus"></i></button>';
                            $sTipoLancamento="Receber";
                        }
                }else{
                     $sSinal = '<button type="button" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-remove-sign"></i></button>';
                } ?>
                        <tr>
                            <td>
                             <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
                                <!-- <a href="#" onClick="confirmacao('?action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oMnyVMovimentoConciliacao->getCodConciliacao()?>&req=1&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>-->
                               <a href="#" onclick="confirmaConciliacao('action=MnyConciliacao.processaFormulario&sOP=Excluir&fIdMnyConciliacao=<?= $oMnyVMovimentoConciliacao->getCodConciliacao()?>&req=1&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>&dData=<?=$oMnyVMovimentoConciliacao->getDataConciliacaoFormatado()?>&nCodUnidade=<?=$oMnyVMovimentoConciliacao->getCodUnidade()?>&nCodContaCorrente=<?=$oMnyVMovimentoConciliacao->getContaCodigoConc()?>',2)" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash" style='color: red;font-size: large;'></i></a>
                             <? }?>
                                <a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?=$sTipoLancamento?>&nIdMnyMovimentoItem=<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>||<?= $oMnyVMovimentoConciliacao->getMovItem()?>" target="_blank" title="Detalhar Movimento"><i class="glyphicon glyphicon-eye-open" style='color: #0783BB;font-size: large;'></i></a>
                                <a href="?action=SysArquivo.preparaArquivo&sOP=Visualizar&fCodArquivo=<?= $oMnyVMovimentoConciliacao->getCodArquivo()?>" target="_blank" title="Visualizar Arquivo"><i class='glyphicon glyphicon-file' style='color: #8B8989;font-size: large;'></i></a>
                                <? if($oMnyVMovimentoConciliacao->getConsolidado() == 0){?>
                                    <a href="#" onclick='confirmaConciliacao("?action=MnyMovimentoItem.extornaItemMovimento&nCodSolicitacao=<?= $oMnyVMovimentoConciliacao->getCodSolicitacaoAporte()?>&fa=<?=$oMnyVMovimentoConciliacao->getMovCodigo()?>||<?= $oMnyVMovimentoConciliacao->getMovItem() ?>&fDataConciliacao=<?= $oMnyVMovimentoConciliacao->getDataConciliacao() ?>&fCodUnidade=<?= $oMnyVMovimentoConciliacao->getCodUnidade() ?>&fCodContaCorrente=<?= $oMnyVMovimentoConciliacao->getCodContaCorrente() ?>",3)'  title="Estornar Item"><i class="glyphicon glyphicon-remove-sign" style='color: #FF8C00;font-size: large;'></i></a>
                                <? } ?>
                            </td>
                            <td>
                                <input type="hidden" name="fFichaAceite[]" value="<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?>"/>
                                <?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?>
                            </td>
                            <td><?= $oMnyVMovimentoConciliacao->getNome()?></td>
                            <td><?= $oMnyVMovimentoConciliacao->getHistorico()?></td>
                            <td><?= $oMnyVMovimentoConciliacao->getConta()?></td>
                            <td><?= $sSinal . " " .$oMnyVMovimentoConciliacao->getMovValorParcelaFormatado()?></td>
                        </tr>
                     <? $i++;
                        } ?>
                <? }?>
      </tbody>
      <tfoot>
         <!--
        <?// if($oMnySaldoDiario ){?>
              <tr>
                    <td colspan="4">
                        <label class="col-sm-2 control-label" style="text-align:left; font-size:18px;">Saldo Inicial:  </label>
                        <div class="col-sm-10" align="left" style='font-size:18px;'><?//= 'R$ ' . number_format($oMnySaldoDiario->getValorInicial(),2, ',', '.');?></div>
                    </td>
                  <td colspan="2" rowspan="2" style='background: #E2EEF8;'>
                      <div class="form-group" style='margin: 50px 200px;'>
                        <div class="col-sm-10">
                            <strong><a href="relatorios/?rel=extrato_conciliacao&cod_unidade=<?//= ($voMnyVMovimentoConciliacao)? $voMnyVMovimentoConciliacao[0]->getCodUnidade() :  $oMnySaldoDiario->getCodUnidade()?>&conta=<?//= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getContaCodigoConc():  $oMnySaldoDiario->getCcrCodigo() ?>&data=<?//= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getDataConciliacao() : $oMnySaldoDiario->getData()?>&empresa=<?//= $_SESSION['oEmpresa']->getEmpCodigo()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato"><button type="button" class="btn btn-primary btn-sm"  style="font-size:12px;"><i class="glyphicon glyphicon-file">&nbsp;Gerar Extrato</i></button></a></strong>
                        </div>
                      </div>
                  </td>
             </tr>
             <tr>
                    <td colspan="4">
                        <label class="col-sm-2 control-label" style="text-align:left; font-size:18px;">Saldo Final:  </label>
                        <div class="col-sm-4" align="left" style='font-size:18px;'><?//= 'R$ ' . number_format($oMnySaldoDiario->getValorFinal(),2, ',', '.');?></div>
                    </td>
             </tr>
         <?// } ?>
        -->
      </tfoot>
        </table>
        </div>
    </div>

</div>

    <div class="form-group">
        <div class="col-sm-12">
           <strong><a href="relatorios/?rel=extrato_conciliacao&cod_unidade=<?= ($voMnyVMovimentoConciliacao)? $voMnyVMovimentoConciliacao[0]->getCodUnidade() :  $oMnySaldoDiario->getCodUnidade()?>&conta=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getContaCodigoConc():  $oMnySaldoDiario->getCcrCodigo() ?>&data=<?= ($voMnyVMovimentoConciliacao) ? $voMnyVMovimentoConciliacao[0]->getDataConciliacao() : $oMnySaldoDiario->getData()?>&empresa=<?= $_SESSION['oEmpresa']->getEmpCodigo()?>"  target="_blank" data-toggle="tooltip" title="Gerar Extrato"><button type="button" class="btn btn-success btn-sm"  style="font-size:6px;"><i class="glyphicon glyphicon-file">&nbsp;Gerar Extrato</i></button></a></strong>
        </div>
    </div>
   <? } ?>

         <div class="form-group">
              <div class="col-sm-12">
                 <fieldset title="Dados da Conciliacao">
                    <legend>Atualizar Comprovante Bancário</legend>
                 </fieldset>
              </div>
         </div>

             <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>
                <div class='form-group'>
                         <div class="col-sm-2" ><a href="?action=MnyConciliacao.preparaExtratoBancario&fCodContaCorrente=<?=$voMnyVMovimentoConciliacao[0]->getContaCodigoConc()?>&fCodUnidade=<?=$voMnyVMovimentoConciliacao[0]->getCodUnidade()?>&fData=<?=$voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>" target="_blank"><button type="button" class="btn btn-success btn-sm"  style="font-size:6px;"><i class="glyphicon glyphicon-folder-open">&nbsp;Visualizar Comprovante</i></button></a> </div>
                         <div class="col-sm-3">
                           <input type='file' id='arquivo'   onblur="verificaUploadConciliacao('arquivo',1048576)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false"   name='arquivo' >
                        </div>
                        <div class="col-sm-1">
                            <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=((1048576/1024)/1024)?>MB</span>
                        </div>
                </div>
            </div>


           	<div class="form-group" >
                 <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-2">
                         <button type="button"  class="btn btn-primary" onclick="confirmaConciliacao();" value="">Corrigir   </button>
                    </div>
                </div>
            </div>


     </form>
 </div>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script type="text/javascript" language="javascript">



			 jQuery(document).ready(function(){ jQuery(".chosen").data("placeholder","Selecione").chosen();})

              $(".grid").sortable({
                    tolerance: 'pointer',
                    revert: 'invalid',
                    placeholder: 'span2 well placeholder tile',
                    forceHelperSize: true
                });

                $(document).ready(function() {
                    var fixHelperModified = function(e, tr) {
                        var $originals = tr.children();
                        var $helper = tr.clone();

                        $helper.children().each(function(index){
                            $(this).width($originals.eq(index).width())
                        });

                        return $helper;
                    };

                    $(".table-sortable tbody").sortable({
                        helper: fixHelperModified
                    }).disableSelection();

                    $(".table-sortable thead").disableSelection();
                });


			 function confirmaConciliacao(){
				$("#ModalConciliacao").on("shown.bs.modal", function () {
                    $(this).find('#ok').attr('href', "javascript:document.formMnyConciliacao.submit()");
                    document.getElementById('msg').innerHTML = "Ao enviar o formulário você tramitará automaticamente para a reconciliação! \n\nDeseja mesmo realizar esta opera&ccedil;&atilde;o ?";
				});
				$('#ModalConciliacao').modal('show');
			 }

            function verificaUploadConciliacao(id,limite){
                var fileInput = $('#'+id);

                var maxSize = limite;
                console.log(fileInput.get(0).files[0].size);

                //aqui a sua função normal
                if (fileInput.get(0).files.length) {
                    var fileSize = fileInput.get(0).files[0].size; // in bytes

                    if (fileSize > maxSize) {
                        var b = fileInput.get(0).files[0].size;
                        var msg = ((b/1024) / 1024).toFixed(2);
                        var msg_limite = ((limite/1024) / 1024);
                        document.getElementById('mensagem2').innerHTML='O tamanho maximo permitido para este arquivo e de '+msg_limite+'MB , o seu arquivo contem '+msg+'MB';

                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });

                        document.getElementById('btnRealiza').disabled = true;
                        document.getElementById(id).value="";
                        return false;
                    }else if(fileInput.get(0).files[0].type != 'application/pdf'){
                        document.getElementById('mensagem2').innerHTML='O arquivo tem que ser do tipo PDF!';
                        $(function() {
                            $( "#mensagem2" ).dialog();
                        });
                        document.getElementById(id).value="";
                        document.getElementById('btnRealiza').disabled = true;
                        btnRealiza
                    } else {
                         document.getElementById('btnRealiza').disabled = false;
                       btnRealiza
                    }
                } else {
                    alert('Selecione um arquivo');
                    return false;
                }
            }

</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalConciliacao" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Concilia&ccedil;&atilde;o </h4>
      </div>
      <div class="modal-body" id="msg"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalConciliacao').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
    </div>
    </div>
  </div>
</div>
