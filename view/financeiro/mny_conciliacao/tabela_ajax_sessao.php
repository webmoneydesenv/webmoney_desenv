<?php
header("Content-Type: text/html; charset=UTF-8",true);
session_start();

 $oSaldoConta = $_REQUEST['oSaldoConta'];
 $Max = $_REQUEST['nMaiorOrdem'];
 $Min = $_REQUEST['nMenorOrdem'];

  if( $_REQUEST['oMnyVMovimentoConciliacao'] || $_REQUEST['deletar'] ){
	if($_REQUEST['deletar'] == 1){
		if($_REQUEST['Indice']){
	 		unset( $_SESSION['oMnyMovConciliacao'][$_REQUEST['Indice']]);
		}
	 	unset( $_SESSION['oMnyMovConciliacao'][$_REQUEST['fMovCodigo'].$_REQUEST['fMovItem']]);
	}else{
		if(is_array($_REQUEST['oMnyVMovimentoConciliacao'] )){
		   	$_SESSION['oMnyMovConciliacao'] = $_REQUEST['oMnyVMovimentoConciliacao'];
		}else{
		   	$_SESSION['oMnyMovConciliacao'][$_REQUEST['oMnyVMovimentoConciliacao']->getMovCodigo().$_REQUEST['oMnyVMovimentoConciliacao']->getMovItem()] = $_REQUEST['oMnyVMovimentoConciliacao'];
		}
	}
  }

  $voMnyVMovimentoConciliacao = $_SESSION['oMnyMovConciliacao'] ;


?>
<? if($voMnyVMovimentoConciliacao){
  $voIndice = array_keys($voMnyVMovimentoConciliacao);
?>

<input type="hidden" name="fDataConciliacao" value="<?//= $voMnyVMovimentoConciliacao[0]->getDataConciliacao()?>"/>
<input type="hidden" name="sOP"  value="Consolidar"/>
<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
        <tr>
			<th>A&ccedil;&otilde;es</th>
            <th>Mov.Item</th>
            <th>Favorecido</th>
            <th>Hist&oacute;rico</th>
            <th>Valor</th>
			<!--<th>Anexo</th>-->
            <th>Ordenar</th>
        </tr>
 <? $total = 0;
	$i = 0;
	foreach($voMnyVMovimentoConciliacao as $oMnyVMovimentoConciliacao){
		if(($oMnyVMovimentoConciliacao->getMovCodigo()."".$oMnyVMovimentoConciliacao->getMovItem()) == $voIndice[$i]){
			$total = $oMnyVMovimentoConciliacao->getMovValorParcela() + $total; ?>
			<tr>
				<td><button class="btn btn-primary btn-xs" onClick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.deletaItemSessao&fMovCodigo=<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>&fMovItem=<?= $oMnyVMovimentoConciliacao->getMovItem()?>','divTabela');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></button>
					<a href="?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=Pagar&nIdMnyMovimento=<?= $oMnyVMovimentoConciliacao->getMovCodigo()?>" target="_blank" title="Detalhar Movimento"><i class="glyphicon glyphicon-eye-open"></i></a>
				</td>
				<td><?= $oMnyVMovimentoConciliacao->getMovCodigo()?>.<?= $oMnyVMovimentoConciliacao->getMovItem()?></td>
				<td><?= $oMnyVMovimentoConciliacao->getNome()?></td>
				<td><?= $oMnyVMovimentoConciliacao->getHistorico()?></td>
				<td><?= $oMnyVMovimentoConciliacao->getMovValorParcelaFormatado()?></td>
				<!--
                <td>   <div style="position:relative;" class="col-sm-3" >
                          <a class='btn btn-primary' href='javascript:;'>
                              <i class='glyphicon glyphicon-file'></i>
                                <input type="file"  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' onchange='$("#nomeArquivo").html($(this).val());uplaod();' data-input="false" id="arquivo" name="arquivo[]" required />
                          </a>
                          &nbsp;
                           <span class='label label-info' id="nomeArquivo"></span>
                      </div>
                </td>
                -->
				<td align="center">
                    <button class="btn btn-primary btn-xs" onClick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.alteraOrdem&nIndice=<?=$voIndice[$i]?>&nProximoIndice=<?=$voIndice[$i+1]?>','divTabela');" data-toggle="tooltip" title="Trocar Posi&ccedil;&atilde;o"><i class="glyphicon glyphicon-arrow-up"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="recuperaConteudoDinamico('index.php','action=MnyConciliacao.alteraOrdem&nIndice=<?=$voIndice[$i]?>&nProximoIndice=<?=$voIndice[$i+1]?>','divTabela');" data-toggle="tooltip" title="Trocar Prosi&ccedil;&atilde;o!"><i class="glyphicon glyphicon-arrow-down"></i></button>
				</td>
			</tr>
 <? 	}
 	$i++;
   } ?>
</table>
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Total Itens:  </label>
		<div class="col-sm-10" align="left" ><?= 'R$ ' . number_format($total, 2, ',', '.'); ?></div>
    </div>
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Saldo Conta:  </label>
		<div class="col-sm-10" align="left" ><?//= 'R$ ' . number_format($oSaldoConta->getValorInicial(), 2, ',', '.'); ?></div>
    </div>
	<div class="form-group">
    	<label class="col-sm-2 control-label" style="text-align:left" >Saldo Final Conta:  </label>
		<div class="col-sm-10" align="left" ><?//= 'R$ ' . number_format($oSaldoConta->getValorFinal() , 2, ',', '.'); ?></div>
    </div>
<? }else{?>
<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <th>C/D</th>
            <th>Mov</th>
            <th>Item</th>
            <th>C&oacute;digo</th>
            <th>Favorecido</th>
            <th>Hist&oacute;rico</th>
            <th>Conta</th>
            <th>Valor</th>
        </tr>
        <tr>
            <td colspan="7">N&atilde;o h&aacute; concilia&ccedil;&otilde;es para esta data.</td>
        </tr>
</table>
</form>
<? }?>

<script src="js/bootstrap-filestyle.min.js"></script>
