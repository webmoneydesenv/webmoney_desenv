<?
header("Content-Type: text/html; text/css; charset=UTF-8",true);
 $voMnyConciliacao = $_REQUEST['voMnyConciliacao'];

 ?>
   			<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyConciliacao)){?>
   				<thead>
   				<tr>
   					<th class='Titulo'>&nbsp;</th>
					<th class='Titulo'>Data</th>
                    <th class='Titulo'>Uniade</th>
					<th class='Titulo'>Conta Corrente</th>
					<th class='Titulo'>Agencia</th>
                    <th class="TItulo">Banco</th>
   				</tr>
   				</thead>
   				<tbody>
                <? foreach($voMnyConciliacao as $oMnyConciliacao){?>
   				<tr>
                    <td ><a href="relatorios/?rel=extrato_conciliacao&cod_unidade=<?=  $oMnyConciliacao->getCodUnidade()?>&conta=<?=  $oMnyConciliacao->getContaCodigoConc() ?>&data=<?= $oMnyConciliacao->getDataConciliacao()?>" target="_blank" data-toggle="tooltip" title="Gerar Extrato"><i class="glyphicon glyphicon-duplicate  "></i></a></td>
                    <td><?= ($oMnyConciliacao->getDataConciliacao()) ? $oMnyConciliacao->getDataConciliacaoFormatado() : "&mdash;"?></td>
                    <td><?= ($oMnyConciliacao->getUnidadeDescricao())? $oMnyConciliacao->getUnidadeDescricao() : "&mdash;"?></td>
					<td><?= ($oMnyConciliacao->nContaCorrenteConc())? $oMnyConciliacao->nContaCorrenteConc() : "&mdash;"?></td>
					<td><?= ($oMnyConciliacao->getAgenciaContaConc()) ? $oMnyConciliacao->getAgenciaContaConc() : "&mdash;"?></td>
                    <td><?= ($oMnyConciliacao->getBcoNome()) ? $oMnyConciliacao->getBcoNome() : "&mdash;"?></td>
  				</tr>
               <? }?>
  				</tbody>

  			<?php }else{?>
              	<tfoot>
  				<tr>
   					<td>Nenhum extrato disponivel!</td>
   				</tr>
   				</tfoot>
            <? }?>
  			</table>
