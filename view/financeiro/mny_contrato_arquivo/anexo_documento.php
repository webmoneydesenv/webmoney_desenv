<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
 $oMnyMovimento = $_REQUEST['oMnyMovimento'];
 $voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];


 ?>
<!doctype html>
<html>
<!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Contrato - <?php echo $sOP ?></title>
<!-- InstanceEndEditable -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
</head>
<body>
<div class="container">
  <header>
    <?php include_once("view/includes/topo.php")?>
  </header>
  <?php include_once("view/includes/menu.php")?>
  <div class="content">
    <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt; <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoPessoa.preparaLista">Gerenciar Contratos</a> &gt; <strong><?php echo $sOP?> Contrato</strong><!-- InstanceEndEditable --></div>
    <!-- InstanceBeginEditable name="titulo" -->
    <h3 class="TituloPagina"><?= ($_REQUEST['sOP'] == 'VincularDocumento') ? "Vincular Documento" : "Anexar Documento"?></h3>
    <!-- InstanceEndEditable -->
    <section> <!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoArquivo.processaFormulario" >
		 <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fContratoCodigo" value="<?=(is_object($oMnyContratoPessoa)) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>" />
		 <input type="hidden" name='fMovCodigo' required value="<?= ($oMnyMovimento)? $oMnyMovimento->getMovCodigo() : $_REQUEST['fMovCodigo']?>">
         <input type="hidden" name='fAtivo' required value="1">
         <input type="hidden" name='sTipoLancamento' required value="<?= ($oMnyMovimento)? (($oMnyMovimento->getMovTipo() == 188) ? "Pagar" : "Receber" ): $_REQUEST['sTipoLancamento']?>">

     <!--<input type='hidden' name='fPesCodigo'  required value='<?//= $_REQUEST['fPesCodigo'] ?>'/>-->
        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Dados do MnyContratoPessoa" disabled>
        <legend>Dados do Contrato</legend>
          <div class="form-group">
            <div class="col-sm-4">
	          <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nº do Contrato:</strong><strong style='color: red;'><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?></strong></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Contratado: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyPessoaGeral()->getNome() :  ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Status: </strong><?= ($oMnyContratoPessoa->getSysStatus()) ? $oMnyContratoPessoa->getSysStatus()->getDescStatus() : ""?></div></div>
           </div>
            <div class="col-sm-4">
	          <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Descri&ccedil;&atilde;o: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Contrato: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Validade: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?></div></div>
           </div>
            <div class="col-sm-4">
	          <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Unidade:</strong><?= ($oMnyContratoPessoa) ?  $oMnyContratoPessoa->getMnyUnidadePlanocontas()->getDescricao()  : ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Setor: </strong><?= ($oMnyContratoPessoa) ?  $oMnyContratoPessoa->getMnySetorPlanocontas()->getDescricao() :  ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Status: </strong><?= ($oMnyContratoPessoa->getSysStatus()) ? $oMnyContratoPessoa->getSysStatus()->getDescStatus() : ""?></div></div>
           </div>
           </div>
          </div>
          <br>
          <legend>Documentos</legend>
          <div class="class="col-sm-4"">
            <button type="button" class="btn btn-primary " data-toggle="collapse" data-target="#demo">Abrir</button>
		  </div>
          <div class="form-group">
          <? $i = 0;?>

            <div id="demo" class="collapse">

		  <?php foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
            <div>

                <label class="control-label col-sm-2" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br>
                <?
					$voMnyContratoPessoaArquivoTipo = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivo($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
					$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor();
				?>
                </label>
                <div class="col-sm-12">
                <?php
					if($voMnyContratoPessoaArquivoTipo){
						$j=0;
						foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
							$j++;

							if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($j == 1)){ ?>

                                    <input type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($i == 0 || $i == 1 ||$i == 2) ? ' required="required"' : ""?>/><br>
                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">

						<?	}
							if($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()){
								// atualizar
								echo "
								 <div class=\"col-sm-1 \" ><a target='_blank' href='".$oMnyContratoPessoaArquivoTipo->getNome()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
 								 <div class=\"col-sm-2\"><input type=\"file\" data-buttonText=\"Atualizar\" class=\"filestyle \" data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /> </div>
								 <div class=\"col-sm-9\"><a   data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=MnyContratoArquivo.processaFormulario&sOP=Excluir&fCodContratoArquivo=".$oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()."\"><button type=\"button\" class=\"btn btn-danger \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div><hr><br>"; ?>
                                 <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()?>">
                                 <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">

					<?	}else{
							?>
                                 <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($i == 0 || $i == 1 ||$i == 2) ? ' required="required"' : ""?>/><br><br>
                                 <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                      <? 	}

						}
						?>

                	<? }else{ ?>
                             <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($i == 0 || $i == 1 ||$i == 2) ? ' required="required"' : ""?>/> <?= ($i == 0 || $i == 1 ||$i == 2) ? "<div style=\"color:red;\"> * Campo Obrigat&aacute;rio. </div>" : ""?>
                             <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
					<? } ?>


                </div>
                <? $i++ ?>
            </div>
            <p>
            <p>

		  <? } ?>
          </div>
          </div>

          </div>
          </div>
          <br>
          <br>
        </fieldset>
        <br/>
       <div class="form-group">
        <div class="col-sm-offset-5 col-sm-2">
        	<input type="hidden" name="fIndice" value="<?=$i?>">
            <button type="submit" class="btn btn-primary">Vincular</button>
        </div>
      </div>
      </form>
      <script src="js/jquery/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/bootstrap-filestyle.min.js"></script>
      <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
      <script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
      <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
      <script src="js/producao.js" type="text/javascript"></script>
      <script type="text/javascript">
		   $(function(){
				$(".btn-toggle").click(function(e){
					e.preventDefault();
					el = $(this).data('element');
					$(el).toggle();
				});
			});



			function ativa(){
				var div = document.getElementById('div')
				/* se conteúdo está escondido, mostra e troca o valor do botão para: esconde */
				if (div.style.display == 'none') {
					document.getElementById("botao").value='esconde'
					div.style.display = 'block'
				} else {
					/* se conteúdo está a mostra, esconde o conteúdo e troca o valor do botão para: mostra */
					div.style.display = 'none'
					document.getElementById("botao").value='mostra'
				}
			}
	  </script>
      <!-- InstanceEndEditable -->
      </h1>
    </section>
    <!-- end .content -->
  </div>
  <?php include_once("view/includes/mensagem.php")?>
  <footer>
    <?php require_once("view/includes/rodape.php")?>
  </footer>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd -->
	</html>



  <div class="modal fade" id="file" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Atualizar Arquivo</h4>
                </div>
                    <div class="modal-title" id="conteudo">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoArquivo.processaFormulario">
                      	 <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
                         <input type="hidden" name="fContratoCodigo" value="<?=(is_object($oMnyContratoPessoa)) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>" />
                         <input type="hidden" name='fAtivo' required value="1">
                         <input type="hidden" name='fCodContratoDocTipo_<?=$i?>' required value="<?= $oMnyContratoPessoa->getTipoContrato()?>">
                       	<div>
	                           <input type="file" class="filestyle" data-buttonName="btn-primary"  data-input="true"  name="arquivo_<?=$i?>" >
                           </div>
                          </form>
                    </div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-primary" id="ok">Anexar Arquivo</button>
                  <button type="file" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>

          </div>
    </div>
</div>
