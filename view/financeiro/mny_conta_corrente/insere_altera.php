<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContaCorrente = $_REQUEST['oMnyContaCorrente'];
 $voSysBanco = $_REQUEST['voSysBanco'];
 $voUnidade = $_REQUEST['voUnidade'];
 $voTipoConta = $_REQUEST['voTipoConta'];
 $voSysEmpresa = $_REQUEST['voSysEmpresa'];
 $vUnidadeConta = $_REQUEST['vUnidadeConta'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Conta Corrente - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/bootstrap-select-min.css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContaCorrente.preparaLista">Gerenciar Conta Correntes</a> &gt; <strong><?php echo $sOP?> Conta Corrente</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Conta Corrente</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyContaCorrente" action="?action=MnyContaCorrente.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCcrCodigo" value="<?=(is_object($oMnyContaCorrente)) ? $oMnyContaCorrente->getCcrCodigo() : ""?>" />
 		 <input type='hidden' name='fAtivo' value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getAtivo() : "1"?>'/>
         <input type='hidden' id='CcrInc' name='fCcrInc' value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrInc() : ""?>'/>
		 <input type='hidden' id='CcrAlt' name='fCcrAlt' value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAlt() : ""?>'/>


         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Conta Corrente">
 		 <input type='hidden' name='fCcrCodigo' value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrCodigo() : ""?>'/>
 		<div class="form-group">
        <label class="col-sm-1 control-label" style="text-align:left" for="Pessoa">Empresa:</label>
            <div class="col-sm-5">
                <select name='fEmpCodigo' class="form-control chosen" required >
                    <option value=''>Selecione</option>
                	<?php foreach($voSysEmpresa as $oSysEmpresa){
                     		if($oMnyContaCorrente){
							   $sSelected = ($oMnyContaCorrente->getEmpCodigo() == $oSysEmpresa->getEmpCodigo()) ? "selected" : "";
								   }?>
                    	<option <?= $sSelected?>  value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                        <? } ?>
		          </select>
 			</div>

            <label class="col-sm-1 control-label" style="text-align:left" for="Unidade">Unidade:</label>
			<div class="col-sm-5">
				<select class="selectpicker col-sm-5" name='fCodUnidade[]'  multiple data-actions-box='true'>
					<option value=''>Selecione</option>
					<? $sSelected = "";
                    if($voUnidade){
					  foreach($voUnidade as $oUnidade){
					   if($oMnyContaCorrente){
						   $sSelected = (in_array($oUnidade->getPlanoContasCodigo(), $vUnidadeConta)) ? "selected" : "";
					   }
						?>
                    <option  <?= $sSelected?> value='<?= $oUnidade->getPlanoContasCodigo()?>'><?= $oUnidade->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
                   </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label" style="text-align:left" for="DataEncerramento">Encerramento:</label>
					<div class="col-sm-1">
						<input class="form-control" id="DataEncerramento" type='text'  placeholder='Data de Encerramento' name='fDataEncerramento'   value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getDataEncerramento() : ""?>'/>
					</div>
                 	<label class="col-sm-1 control-label" style="text-align:left" for="CcrAgencia">Ag&ecirc;ncia:</label>
					<div class="col-sm-1">
						<input class="form-control" type='text' id='CcrAgencia' placeholder='Agencia' name='fCcrAgencia'   value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAgencia() : ""?>'/>
					</div>
                    <label class="col-sm-1 control-label" style="text-align:left" for="CcrAgenciaDv">Digito:</label>
					<div class="col-sm-1">
					<input class="form-control" type='text' id='CcrAgenciaDv' placeholder='- Digito Verificador Agencia' name='fCcrAgenciaDv'   value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAgenciaDv() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="CcrConta">Conta:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='CcrConta' placeholder='Conta' name='fCcrConta'   value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrConta() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="CcrContaDv">Digito:</label>
					<div class="col-sm-1">
					<input class="form-control" type='text' id='CcrContaDv' placeholder='Digito Conta' name='fCcrContaDv'   value='<?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrContaDv() : ""?>'/>
				</div>
				</div>
				<br>

				<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CcrTipo">Tipo:</label>
					<div class="col-sm-3">
					<select id='CcrTipo' name='fCcrTipo'  class="form-control chosen"  >
						<option value=''>Selecione</option>
                        <option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getCcrTipo()==0) ? " selected " : ""?> value='0'>Conta Corrente</option>
						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getCcrTipo()==1) ? " selected " : ""?> value='1'>Conta Sal&aacute;rio</option>
						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getCcrTipo()==2) ? " selected " : ""?> value='2'>Conta Poupan&ccedil;a</option>
						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getCcrTipo()==3) ? " selected " : ""?> value='3'>Outras Contas</option>
                    </select>
					</div>

				  <label class="col-sm-1 control-label" style="text-align:left" for="SysBanco">Banco:</label>
					<div class="col-sm-3">
					<select name='fBanCodigo'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysBanco){
							   foreach($voSysBanco as $oSysBanco){
								   if($oMnyContaCorrente){
									   $sSelected = ($oMnyContaCorrente->getBanCodigo() == $oSysBanco->getBcoCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysBanco->getBcoCodigo()?>'><?= $oSysBanco->getBcoNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>

                <label class="col-sm-2 control-label" style="text-align:left" for="Classificacao">Classifica&ccedil;&atilde;o:</label>
                <div class="col-sm-2">
					<select id='Classificacao' name='fClassificacao'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getClassificacao()==0) ? " selected " : ""?> value='0'>Recebedora</option>
						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getClassificacao()==1) ? " selected " : ""?> value='1'>Pagadora</option>
   						<option  <?= ($oMnyContaCorrente && $oMnyContaCorrente->getClassificacao()==2) ? " selected " : ""?> value='2'>Caixa</option>
                    </select>

				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
 				$("#DataEncerramento").mask("99/99/9999");
			 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
