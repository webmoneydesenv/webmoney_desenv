﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContaCorrente = $_REQUEST['oMnyContaCorrente'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Conta Corrente - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContaCorrente.preparaLista">Gerenciar Contas Correntes</a> &gt; <strong><?php echo $sOP?> Conta Corrente</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Conta Corrente</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Ag&ecirc;ncia:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAgencia() : "" ?>
                     - <?=($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAgenciaDv() : ""?> </div>
				</div>
				<br>
                <br>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getUnidade()->getDescricao()  : "" ?>
                     </div>
				</div>
				<br>
                <br>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong> Conta:&nbsp;</strong><?= ($oMnyContaCorrente) ?  $oMnyContaCorrente->getCcrConta() : ""?>
                     -  <?=($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrContaDv() : ""?></div>
				</div>
				<br>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Tipo:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrTipoFormatado() : ""?></div>
				</div>
				<br>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Banco:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getSysBanco()->getBcoNome() : ""?></div>
				</div>
				<br>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>- Inclus&atilde;o:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrInc() : ""?></div>
				</div>
				<br>
 <? if( $oMnyContaCorrente->getCcrAlt()){?>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong> Altera&ccedil;&atilde;o:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getCcrAlt() : ""?></div>
				</div>
				<br>
 <? }?>
 <div class="form-group">
					 <div class="col-sm-12" style="text-align:left"><strong>Classifica&ccedil;&atilde;o:&nbsp;</strong><?= ($oMnyContaCorrente) ? $oMnyContaCorrente->getClassificacaoFormatado() : ""?></div>
				</div>
				<br>
				</div>
				<br>
         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyContaCorrente.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd -->
 <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 </html>
