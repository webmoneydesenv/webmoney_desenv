<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPlanoContas = $_REQUEST['oMnyPlanoContas'];

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Plano de Contas - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPlanoContas.preparaLista">Gerenciar Plano de Contas</a> &gt; <strong><?php echo $sOP?> Plano de Contas</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Plano de Contas</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPlanoContas" action="?action=MnyPlanoContas.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fPlanoContasCodigo" value="<?=(is_object($oMnyPlanoContas)) ? $oMnyPlanoContas->getPlanoContasCodigo() : ""?>" />
         <input type="hidden" name="fUsuInc" value="<?=(is_object($oMnyPlanoContas)) ? $oMnyPlanoContas->getUsuInc() : date('d/m/Y') ." || ". $_SESSION['oUsuarioImoney']->getLogin();?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Plano de Contas">

 							<input type='hidden' name='fPlanoContasCodigo' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getPlanoContasCodigo() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="C&oacute;digo">C&oacute;digo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Codigo' placeholder='C&oacute;digo' name='fCodigo'  required   value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descri&ccedil;&atilde;o">Descri&ccedil;&atilde;o:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Descricao' placeholder='Descri&ccedil;&atilde;o' name='fDescricao'  required   value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Vis&iacute;vel">Vis&iacute;vel:</label>
					<div class="col-sm-2">
                    <select name="fVisivel" class="form-control">
                    	<option value="">Selecione</option>
						<option value="1" <?= ($oMnyPlanoContas && $oMnyPlanoContas->getVisivel() == 1 ) ? " selected='selected'" : ""?> >Sim</option>
                        <option value="0" <?= ($oMnyPlanoContas && $oMnyPlanoContas->getVisivel() == 0 ) ? " selected='selected'" : ""?>>N&atilde;o</option>
                    </select>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="view/js/jquery/jquery.js"></script>
  		<script src="view/js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="view/js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="view/js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="view/js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="view/js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="view/js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
