﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPlanoContas = $_REQUEST['oMnyPlanoContas'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Plano de Contas - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="view/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="view/css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="view/css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="view/imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="view/imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="view/imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="view/imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="view/imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="view/imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="view/imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="view/imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="view/imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="view/imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="view/imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="view/imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="view/imagens/ico/favicon-16x16.png">
<link rel="manifest" href="view/imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPlanoContas.preparaLista">Gerenciar Plano de Contas</a> &gt; <strong><?php echo $sOP?> Plano de Contas</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Plano de Contas</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPlanoContas" action="view/?action=MnyPlanoContas.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyPlanoContas" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Codigo">Codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Codigo' name='fCodigo' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fDescricao' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UsuInc">Inclusão:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Inclusao' name='fUsuInc' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getUsuInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="UsuAlt">Alteração:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Alteracao' name='fUsuAlt' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getUsuAlt() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyPlanoContas) ? $oMnyPlanoContas->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="view/?action=MnyPlanoContas.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="view/js/jquery/jquery.js"></script>
  		<script src="view/js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="view/js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="view/js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="view/js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
