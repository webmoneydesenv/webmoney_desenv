<?php
 $voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Plano de Contas</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
     <link rel="apple-touch-icon" sizes="57x57" href="view/imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="view/imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="view/imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="view/imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="view/imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="view/imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="view/imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="view/imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="view/imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="view/imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="view/imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="view/imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="view/imagens/ico/favicon-16x16.png">
<link rel="manifest" href="view/imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyPlanoContas.preparaLista">Gerenciar Plano de Contas</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Plano de Contas</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnyPlanoContas" id="formMnyPlanoContas" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyPlanoContas" onChange="JavaScript: submeteForm('MnyPlanoContas')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyPlanoContas.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Plano de Contas</option>
   						<option value="?action=MnyPlanoContas.preparaFormulario&sOP=Alterar" lang="1">Alterar Plano de Contas selecionado</option>
   						<option value="?action=MnyPlanoContas.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Plano de Contas selecionado</option>
   						<option value="?action=MnyPlanoContas.processaFormulario&sOP=Excluir" lang="2">Excluir Plano de Contas(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="view/imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyPlanoContas)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyPlanoContas')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Id</th>
					<th class='Titulo'>C&oacute;digo</th>
					<th class='Titulo'>Descri&ccedil;&atilde;o</th>
					<th class='Titulo'>Vis&iacute;vel</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyPlanoContas as $oMnyPlanoContas){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyPlanoContas')" type="checkbox" value="<?=$oMnyPlanoContas->getPlanoContasCodigo()?>" name="fIdMnyPlanoContas[]"/></td>
  					<td><?= $oMnyPlanoContas->getPlanoContasCodigo()?></td>
					<td><?= $oMnyPlanoContas->getCodigo()?></td>
					<td><?= $oMnyPlanoContas->getDescricao()?></td>
					<td><?= ($oMnyPlanoContas->getVisivel() == 1) ? "Sim" : "N&atilde;o"?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyPlanoContas)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyPlanoContas');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
