<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voSysStatus = $_REQUEST['voSysStatus'];
 $voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
 $voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
 $oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
 $voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 $voMnySetor = $_REQUEST['voMnySetor'];
 $voMnyContratoAditivo = $_REQUEST['voMnyContratoAditivo'];
 $voMnyContratoTipo =  $_REQUEST['voMnyContratoTipo'];
 ?>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
       <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoPessoa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fContratoCodigo" value="<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>" />
         <input type="hidden" name='fPesCodigo' required value="<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getPesCodigo() : ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>">
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>
		 <?php if ($sOP == 'Alterar'){ ?>
            <input type='hidden' name='fAnexo'  value='<?=($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAnexo() : ""?>'/>
            <?php } ?>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Contrato">

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SysStatus">Status:</label>
					<div class="col-sm-2">
					<select name='fCodStatus'  class="form-control chosen" required >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysStatus){
							   foreach($voSysStatus as $oSysStatus){
								   if($oMnyContratoPessoa){
									   $sSelected = ($oMnyContratoPessoa->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected ?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getDescStatus()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                <label class="col-sm-2 control-label" style="text-align:left">Tipo de Contrato:</label>
					<div class="col-sm-2">
					<select name='fContratoTipo'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyContratoTipo){
							   foreach($voMnyContratoTipo as $oMnyContratoTipo){
								   if($oMnyContratoPessoa){
									   $sSelected = ($oMnyContratoPessoa->getTipoContrato() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected ?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div><div class="form-group">
				  <label class="col-sm-1 control-label" style="text-align:left" for="Numero">Numero:</label>
					<div class="col-sm-2">
					<input required class="form-control" type='text' id='Numero' placeholder='Numero' name='fNumero'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descri&ccedil;&atilde;o:</label>
					<div class="col-sm-8">
					<input required class="form-control" type='text' id='Descricao' placeholder='Descricao' name='fDescricao'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="DataContrato">Data do Contrato:</label>
					<div class="col-sm-2">
					<input required class="form-control" type='text' id='DataContrato' placeholder='Data_contrato' name='fDataContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="DataValidade">Validade:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='DataValidade' placeholder='Data_validade' name='fDataValidade'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?>'/>
				</div>
					<label  class="col-sm-1 control-label" style="text-align:left" for="ValorContrato">Valor:</label>
					<div class="col-sm-3">
					<input required class="form-control" type='text' id='ValorContrato' placeholder='Valor_contrato' name='fValorContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getValorContratoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MnySetorPlanocontas">Setor:</label>
					<div class="col-sm-5">
					<select name='fSetCodigo'  class="form-control chosen"  required>
						<option value=''>Selecione</option>
							<? $sSelected = "";
							   if($voMnySetor){
								   foreach($voMnySetor as $oMnySetor){
									   if($oMnyContratoPessoa){
										   $sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
									   }
							?>
									   <option  <?= $sSelected?> value='<?= $oMnySetor->getPlanoContasCodigo()?>'><?= $oMnySetor->getDescricao()?></option>
							<?
								   }
							   }
							?>
						</select>

				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="MnyUnidadePlanocontas">Unidade:</label>
					<div class="col-sm-5">
					<select name='fUniCodigo'  class="form-control chosen"  required>
						<option value=''>Selecione</option>
                        	<? $sSelected = "";
						   if($voMnyUnidade){
							   foreach($voMnyUnidade as $oMnyUnidade){
								   if($oMnyContratoPessoa){
									   $sSelected = ($oMnyContratoPessoa->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                </div>
                <br>
                 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Anexo">Anexo:</label>
					<div class="col-sm-3">
<!--					<input class="form-control" type='file' id='Anexo' placeholder='Anexo' name='fAnexo'   value='<?// ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAnexo() : ""?>'/>-->
						<input type="file" class="filestyle" data-buttonName="btn-primary"  data-input="false"  id ="fAnexo" name="fAnexo" />
				</div>
                <div class="col-sm-5">
                <?
						if(($_REQUEST['sOP'] == 'Alterar') && ($oMnyContratoPessoa && $oMnyContratoPessoa->getAnexo())) {
							echo "<a href='".$oMnyContratoPessoa->getAnexo()."' target='_blank'> Visualizar Anexo</a>";
						}
						?>
                 </div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAtivo() : "1"?>'/>
				</div>
				<br>
         </fieldset>
          <div class="form-group">
                <div class="col-sm-offset-5 col-sm-2">
                <button type="submit" class="btn btn-primary" ><?=$sOP?></button>
                </div>
   		  </div>
       </form>
       <div>&nbsp;</div><br>
       <legend>Contrato Aditivo</legend>
       <form method="post" action="" name="formMnyContratoAditivo" id="formMnyContratoAditivo" class="formulario">
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>

   			<table>
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyContratoAditivo" onChange="JavaScript: submeteForm('MnyContratoAditivo')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Aditivo</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Alterar" lang="1">Alterar Aditivo selecionado</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Aditivo selecionado</option>
   						<option value="?action=MnyContratoAditivo.processaFormulario&sOP=Excluir" lang="2">Excluir Aditivo(s) selecionado(s)</option>
   						</select>
                    </td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyContratoAditivo)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyContratoAditivo')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
					<th class='Titulo'>Numero</th>
					<th class='Titulo'>Motivo</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Validade</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Anexo</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyContratoAditivo as $oMnyContratoAditivo){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyContratoAditivo')" type="checkbox" value="<?=$oMnyContratoAditivo->getAditivoCodigo()?>" name="fIdMnyContratoAditivo[]"/></td>
					<td><?= $oMnyContratoAditivo->getNumero()?></td>
					<td><?= $oMnyContratoAditivo->getMotivoAditivo()?></td>
					<td><?= $oMnyContratoAditivo->getDataAditivoFormatado()?></td>
					<td><?= $oMnyContratoAditivo->getDataValidadeFormatado()?></td>
					<td><?= $oMnyContratoAditivo->getValorAditivoFormatado()?></td>
					<td><? if($oMnyContratoAditivo->getAnexo()){?>
                    <a target="_blank" href='<?=$oMnyContratoAditivo->getAnexo()?>'>Visualizar Anexo</a>
                    <? }?>
                    </td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyContratoAditivo)){?>
  			</table>
          </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="js/bootstrap-filestyle.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#DataContrato").mask("99/99/9999");
			$("#DataValidade").mask("99/99/9999");
			$("#ValorContrato").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
