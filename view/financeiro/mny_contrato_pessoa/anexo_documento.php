<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];

 ?>
<!doctype html>
<html>
<!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Contrato - <?php echo $sOP ?></title>
<!-- InstanceEndEditable -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
</head>
<body>
<div class="container">
  <header>
    <?php include_once("view/includes/topo.php")?>
  </header>
  <?php include_once("view/includes/menu.php")?>
  <div class="content">
    <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt; <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoPessoa.preparaLista">Gerenciar Contratos</a> &gt; <strong><?php echo $sOP?> Contrato</strong><!-- InstanceEndEditable --></div>
    <!-- InstanceBeginEditable name="titulo" -->
    <h3 class="TituloPagina">Vincular Documento</h3>
    <!-- InstanceEndEditable -->
    <section> <!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoArquivo.processaFormulario" onSubmit="return teste()" >
		 <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fContratoCodigo" value="<?=(is_object($oMnyContratoPessoa)) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>" />
		 <input type="hidden" name='fMovCodigo' required value="<?= $_REQUEST['fMovCodigo']?>">
   		 <input type="hidden" name='fCodContratoDocTipo' required value="<?= $oMnyContratoPessoa->getTipoContrato()?>">
         <input type="hidden" name='fAtivo' required value="1">
     <!--<input type='hidden' name='fPesCodigo'  required value='<?//= $_REQUEST['fPesCodigo'] ?>'/>-->
        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Dados do MnyContratoPessoa" disabled>
          <div class="form-group">
            <div class="col-sm-12">
	          <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nº:</strong><strong style='color: red;'><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?></strong></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nome: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyPessoaGeral()->getNome() :  ""?></div></div>
              <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Status: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getSysStatus()->getDescStatus() : ""?></div></div>
           </div>
           </div>
          </div>
          <div class="form-group">
                  <?php foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
				<div>
                        <label class="control-label col-sm-2" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:</label>
                        <div class="input-group col-sm-4">
                        <input type="file" class="filestyle" data-buttonName="btn-primary"  data-input="false"  id ="fAnexo" name="arquivo[]" multiple />

                        </div>
                    </div>
                    <p>
                    <p>
		  <? } ?>
          </div>
          <br>
          <br>


        </fieldset>
        <br/>

       <div class="form-group">
        <div class="col-sm-offset-5 col-sm-2">
            <input type="submit" class="btn btn-primary" onClick="comprova_extensao('formMnyContratoPessoa', formMnyContratoPessoa.arquivo.value)" ><?= $_REQUEST['sOP']?></button>
        </div>
      </div>
      </form>
      <script src="js/jquery/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/bootstrap-filestyle.min.js"></script>
      <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
      <script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
      <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
      <script src="js/producao.js" type="text/javascript"></script>
        <script type="text/javascript">

		function teste(){
			alert('chegou')	;
			return false;
		}


      	function comprova_extensao(formulario, arquivo) {
			alert ('chegou');
				return false;
		   extensoes_permitidas = new Array(".gif", ".jpg", ".doc", ".pdf");
		   meuerro = "";
		   if (!arquivo) {

			   meuerro = "Não foi selecionado nenhum arquivo";
		   }else{

			  extensao = (arquivo.substring(arquivo.lastIndexOf("."))).toLowerCase();

			  permitida = false;
			  for (var i = 0; i < extensoes_permitidas.length; i++) {
				 if (extensoes_permitidas[i] == extensao) {
				 permitida = true;
				 break;
				 }
			  }
			  if (!permitida) {
				 meuerro = "Comprova a extensão dos arquivos a subir. \nSó se podem subir arquivos com extensões: " + extensoes_permitidas.join();
			   }else{

				 alert ("Tudo correto. Vou submeter o formulário.");
				 formulario.submit();
				 return 1;
			   }
		   }

		   alert (meuerro);
		   return 0;
		}
   	 </script>
      <!-- InstanceEndEditable -->
      </h1>
    </section>
    <!-- end .content -->
  </div>
  <?php include_once("view/includes/mensagem.php")?>
  <footer>
    <?php require_once("view/includes/rodape.php")?>
  </footer>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd -->
</html>
