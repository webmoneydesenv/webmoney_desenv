<?php
$sOP = $_REQUEST['sOP'];
$voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
$voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyConta = $_REQUEST['voMnyConta'];
$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
$voMnyContratoDocTipo  = $_REQUEST['voMnyContratoDocTipo'];
$voMnyContratoPessoaPorId = $_REQUEST['voMnyContratoPessoaPorId'];
$voSysStatus = $_REQUEST['voSysStatus'];
$oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
$voMnyContratoAditivo = $_REQUEST['voMnyContratoAditivo'];

if($_REQUEST['oMnyContratoTipo'])
	$oContratoTipo = $_REQUEST['oMnyContratoTipo'];
else
	$voMnyContratoTipo =  $_REQUEST['voMnyContratoTipo'];

$oMnyContratoDocTipo = $_REQUEST['fContratoTipo'];
$voMnyContratoPessoa = $_REQUEST['voMnyContratoPessoa'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];
$oMnyMovimento =$_REQUEST['oMnyMovimento'];
$voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];
$oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title><?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?> - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->
 <link rel="stylesheet" href="css/formValidation.css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&nTipoContrato='+<?=($oContratoTipo) ? $oContratoTipo->getCodContratoTipo() : ""?>,'divDocumentos')" >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <!--&gt; <strong><?//php echo $sOP?> Contrato</strong>--><a href='?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>'> Gerenciar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></a>&gt;  <Strong>Cadastrar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></Strong> <!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

		   <h3 class="TituloPagina"><?= $sOP ?>  <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
     <form id="AseItemForm" method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-1 control-label">AseItem</label>
        <div class="col-xs-4">
            <input type="text" class="form-control" name="AseItem[0].descricao" placeholder="Descrição" />
        </div>
        <div class="col-xs-2">
            <input type="text" class="form-control" name="AseItem[0].valor" placeholder="Valor" />
        </div>
        <div class="col-xs-1">
            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <!-- The template for adding new field -->
    <div class="form-group hide" id="AseItemTemplate">
        <div class="col-xs-4 col-xs-offset-1">
            <input type="text" class="form-control" name="descricao" placeholder="descricao" />
        </div>

        <div class="col-xs-2">
            <input type="text" class="form-control" name="valor" placeholder="valor" />
        </div>
        <div class="col-xs-1">
            <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
        </div>
    </div>

</form>

</script><script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/formValidation.js"></script>
<script>
$(document).ready(function() {
    var descricaoValidators = {
            row: '.col-xs-4',   // The descricao is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Descrição obrigatória!'
                }
            }
        },
        valorValidators = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'Valor Obrigatório'
                },
                numeric: {
                    message: 'Valor precisa ser numérico'
                }
            }
        },
        AseItemIndex = 0;

    $('#AseItemForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'AseItem[0].descricao': descricaoValidators,
                'AseItem[0].valor': valorValidators
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            AseItemIndex++;
            var $template = $('#AseItemTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-AseItem-index', AseItemIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="descricao"]').attr('name', 'AseItem[' + AseItemIndex + '].descricao').end()
                .find('[name="valor"]').attr('name', 'AseItem[' + AseItemIndex + '].valor').end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#AseItemForm')
                .formValidation('addField', 'AseItem[' + AseItemIndex + '].descricao', descricaoValidators)
                .formValidation('addField', 'AseItem[' + AseItemIndex + '].valor', valorValidators);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-AseItem-index');

            // Remove fields
            $('#AseItemForm')
                .formValidation('removeField', $row.find('[name="AseItem[' + index + '].descricao"]'))
                .formValidation('removeField', $row.find('[name="AseItem[' + index + '].valor"]'));

            // Remove element containing the fields
            $row.remove();
        });
});
</script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
</div>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


   <!--Contrato-->
  <div class="modal fade" id="anexo" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
          </div>
    </div>
</div>

