<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voMnyContratoPessoaPorId = $_REQUEST['voMnyContratoPessoaPorId'];
 $voSysStatus = $_REQUEST['voSysStatus'];
 $voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
 $voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
 $oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
  $oMnyMovimento = $_REQUEST['oMnyMovimento'];
 $voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 $voMnySetor = $_REQUEST['voMnySetor'];
 $voMnyContratoAditivo = $_REQUEST['voMnyContratoAditivo'];
 $voMnyContratoTipo =  $_REQUEST['voMnyContratoTipo'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contrato - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="modal();">
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> &gt; <strong><?php echo $sOP?> Contrato</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">IMONEY</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
<input type='hidden' name='fPesCodigo'  required value='<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo'] ?>'/>
<input type="hidden" name='fMovCodigo' required value="<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>">
<div class="modal fade" id="Contrato" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><strong>Selecione a op&ccedil;&atilde;o desejada!</strong></h4>
                </div>
					<div><h4>&nbsp;</h4></div>
                <div class="modal-footer" id="rodape">
                <div style=" margin: 0 auto; width: 100%;">
                    <a href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=<?= $_REQUEST['sTipoLancamento']?>&Pagina=2&fContratoTipo=2&fPesCodigo=<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo']  ?>&fMovCodigo=<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>" class="btn btn-primary">Adicionar ASE</a>
                    <a href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=<?= $_REQUEST['sTipoLancamento']?>&Pagina=2&fContratoTipo=3&fPesCodigo=<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo']  ?>&fMovCodigo=<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>" class="btn btn-primary">Adicionar OC</a>
                    <a href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=<?= $_REQUEST['sTipoLancamento']?>&Pagina=3&fPesCodigo=<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo']  ?>&fMovCodigo=<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>"class="btn btn-primary">Selecionar Contrato</a>
                    <a href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&sTipoLancamento=<?= $_REQUEST['sTipoLancamento']?>&Pagina=2&fContratoTipo=1&fPesCodigo=<?= ( $oMnyPessoaGeral)?  $_REQUEST['oMnyPessoaGeral']->getPesgCodigo() : $_REQUEST['fPesgCodigo']  ?>&fMovCodigo=<?= ($_REQUEST['oMnyMovimento'])? $_REQUEST['oMnyMovimento']->getMovCodigo() : $_REQUEST['fMovCodigo'] ?>" class="btn btn-primary">Adicionar Contrato</a>
                </div>
                </div>
          </div>
    </div>
</div>
        <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">
			function modal(){
				$('#Contrato').modal('show');
/*				  $('#existente').click(function (event) {
						  alert('existente');
						  return false;
				  });
				   $('#novo').click(function (event) {
						  location.href="?bErro=0&action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&fPesCodigo=";
				  });*/

			}
			document.execCommand('Refresh')
			 function voltar() {
				window.history.back()
			 }
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


   <!--Contrato-->
