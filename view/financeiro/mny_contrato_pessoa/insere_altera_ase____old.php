<?php
unset($_SESSION['oMnyContratoPessoa']);
$sOP = $_REQUEST['sOP'];
$voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
$voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyConta = $_REQUEST['voMnyConta'];
$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
$voSysStatus = $_REQUEST['voSysStatus'];
$oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
$voMnyContratoAse = $_REQUEST['voMnyContratoAse'];

if($_REQUEST['oMnyContratoTipo'])
$oContratoTipo = $_REQUEST['oMnyContratoTipo'];

$oMnyContratoDocTipo = $_REQUEST['fContratoTipo'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];

//print_r($_REQUEST['oMnyContratoPessoa']);

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title><?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?> - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->
 <link rel="stylesheet" href="css/formValidation.css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&nTipoContrato='+<?=($oContratoTipo) ? $oContratoTipo->getCodContratoTipo() : ""?>,'divDocumentos')" >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <!--&gt; <strong><?//php echo $sOP?> Contrato</strong>--><a href='?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>'> Gerenciar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></a>&gt;  <Strong>Cadastrar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></Strong> <!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

		   <h3 class="TituloPagina"><?= $sOP ?>  <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <form method="post" id="AseItemForm" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoPessoa.processaFormulario" >
         <input type="hidden" name="sOP" value="<?= $sOP?>" />
		 <input type='hidden' name='sTipoLancamento' value='<?= $_REQUEST['sTipoLancamento']?> '/>
		 <input type='hidden' name='fTipoContrato' value='<?= $_REQUEST['fTipoContrato']?> '/>
		 <input type='hidden' name='fAtivo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAtivo() : "1"?>'/>
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ''?>'/>
         <input type='hidden' name='fTipoContrato' value='2'/>

<fieldset title="Dados de <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?>">
         <div id="formulario" class="TabelaAdministracao">
<legend title="Documentos">Dados Gerais</legend>
         <div class="col-sm-6">
		<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="SysStatus">Status:</label>
                    <div class="col-sm-3">
                        <select name='fCodStatus'  class="form-control chosen"  required>
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voSysStatus){
                                   foreach($voSysStatus as $oSysStatus){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
                                       }
                            ?>
                                       <option  <?= $sSelected ?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getDescStatus()?></option>
                            <?
                                   }
                               }
                            ?>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label" style="text-align:left">Tipo:</label>
                    <div class="col-sm-5">
                    	<? if($oContratoTipo) { ?>
						<input type='hidden' name='fContratoTipo'  required value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getTipoContrato() : $oContratoTipo->getCodContratoTipo()?>'/>
                        <? echo $oContratoTipo->getDescricao();
                          }else{ ?>
                        <select name='fContratoTipo'  class="form-control chosen" onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&nTipoContrato='+this.value,'divDocumentos')" >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyContratoTipo){
                                   foreach($voMnyContratoTipo as $oMnyContratoTipo){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getTipoContrato() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
                                       }  ?>
                                       <option  <?= $sSelected ?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>
                               <?  }
                               }
                            ?>
                        </select>
                     <? } ?>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Contratado">Pessoa:</label>
                <div class="col-sm-10">
                    <select name='fPesCodigo'  class="form-control chosen"  >
                        <option value=''>Selecione</option>
                        <? $sSelected = "";
                           if($voMnyPessoa){
                               foreach($voMnyPessoa as $oMnyPessoa){
                                   if($oMnyContratoPessoa){
                                       $sSelected = ($oMnyContratoPessoa->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
                                   }?>
                                   <option  <?= $sSelected ?> value='<?= $oMnyPessoa->getPesgCodigo()?>'><?= $oMnyPessoa->getNome()?></option>
                            <? }
                           }?>
                    </select>
                </div>
                </div>
                             <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Numero">N&uacute;mero:</label>
                    <div class="col-sm-4">
                         <input class="form-control" type='text' id='Numero' placeholder='Nro' name='fNumero'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?>'/>
                    </div>
                    <label class="col-sm-2 control-label" style="text-align:left" for="ValorContrato">Valor:</label>
                    <div class="col-sm-4">
                    <input class="form-control" type='text' id='ValorContrato' placeholder='Valor' name='fValorContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getValorContratoFormatado() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Descricao">Descri&ccedil;&atilde;o:</label>
                    <div class="col-sm-10">
                         <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataContrato">Data:</label>
                    <div class="col-sm-4">
                        <input class="form-control" type='text' id='DataContrato' placeholder='Inicio' name='fDataContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?>'/>
                    </div>
                    <? if($_REQUEST['fTipoContrato'] != 2){?>
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataValidade">Validade:</label>
                    <div class="col-sm-4">
                        <input class="form-control" type='text'  id='DataValidade' placeholder='Validade' name='fDataValidade'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?>'/>
                    </div>
                    <? } ?>
                </div>
</div>
<div class="col-sm-6">
               	<? if ($voMnyCusto){ ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
                        <div class="col-sm-10">
                         <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCusto){
                                           foreach($voMnyCusto as $oMnyCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
					                            <option  <?= $sSelected?> value='<?= $oMnyCusto->getPlanoContasCodigo()?>'> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                                    <?     }
                                       }
                                    ?>
                          </select>
                        </div>
                    </div>
          		<? } ?>
          		<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
                    <div class="col-sm-10">
                        <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
                           <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroNegocio){
                                           foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                } ?>
                                              <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?></option>
                                        <?  }
                                         }?>

                             </select>
                   </div>
                </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                     <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
                        <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyUnidade){
                                   foreach($voMnyUnidade as $oMnyUnidade){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                             <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?> </option>
                              <?   }
                               }?>
                      </select>
                  </div>
               </div>
              <? if($voMnyCentroCusto){ ?>
                   <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">C. Custo:</label>
                        <div class="col-sm-10">
                          <select name='fCenCodigo'  class="form-control chosen" required  onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.carregaTipo&sOP=1&fCenCodigo='+this.value,'divConta')" />
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroCusto){
                                           foreach($voMnyCentroCusto as $oMnyCentroCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
                            					<option  <?= $sSelected?> value='<?= $oMnyCentroCusto->getPlanoContasCodigo()?>'><?= $oMnyCentroCusto->getCodigo()?> - <?= $oMnyCentroCusto->getDescricao()?></option>
                                      <?   }
                                       } ?>
                          </select>
                        </div>
                    </div>
             <? } ?>
		        <div class="form-group" >
        	   		<label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                    <div class="col-sm-10"  id="divConta">
                        <select  name='fConCodigo'  class="form-control chosen"  required <?=$sSomenteLeitura?> >
                                <option  value=''>Selecione...</option>
                            <? $sSelected = "";
                               if($voMnyConta){
                                   foreach($voMnyConta as $oMnyConta){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnyConta->getPlanoContasCodigo()?>'><?= $oMnyConta->getCodigo()?> - <?= $oMnyConta->getDescricao()?></option>
                               <?  }
                              }?>
                        </select>
                    </div>
          	    </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
                   <div class="col-sm-10" id="divConta">
                        <select name='fSetCodigo'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnySetor){
                                   foreach($voMnySetor as $oMnySetor){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnySetor->getPlanoContasCodigo()?>'><?= $oMnySetor->getCodigo()?> -  <?= $oMnySetor->getDescricao()?>  </option>
                                <? }
                               } ?>
                      </select>
                    </div>
          		</div>
             </div>
   </div>
   </fieldset>
   <legend>Itens da ASE</legend>
  <? if($_REQUEST['sOP'] == "Alterar"){ ?>
<form role="form" action="/wohoo" method="POST">

    <div class="multi-field-wrapper">
      <div class="multi-fields">
        <div class="multi-field">
		<label>Produto:</label>
          <input type="text" name="descricao[]">
		  <label>Valor:</label>
		   <input type="text" name="valor[]">
          <button type="button" class="add-field">+</button>
		  <button type="button" class="remove-field">-</button>

        </div>
      </div>

  </div>
</form>
    <? }else{?>
        <div class="form-group">
            <div class="col-xs-4">
                <input type="text" class="form-control" required name="AseItem[].descricao" placeholder="Descrição" />
            </div>
            <div class="col-xs-2">
                <input type="text" class="form-control" required name="AseItem[].valor" placeholder="Valor" id="ValorItem"/>
            </div>
            <div class="col-xs-1">
                <button type="button" class="btn btn-default addButton"><i class="glyphicon glyphicon-plus"></i></button>
            </div>
    	</div>
   <? }?>

    <!-- The template for adding new field -->
    <div class="form-group hide" id="AseItemTemplate">
        <div class="col-xs-4">
            <input type="text" class="form-control" required name="AseItemDescricao[]" placeholder="descricao" />
        </div>
        <div class="col-xs-2">
            <input type="text" class="form-control" required name="AseItemValor[]" placeholder="valor" />
        </div>
        <div class="col-xs-1">
            <button type="button" class="btn btn-default removeButton"><i class="glyphicon glyphicon-minus"></i></button>
        </div>
    </div>
    <div class="form-group">
                <div class="col-sm-offset-5 col-sm-6" id="botaoGravar">
                <button type="submit" class="btn btn-primary" ><?=$sOP?></button>
                </div>
   		  </div>
</form>

</script><script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/formValidation.js"></script>



	  		   <div id="msgValidade" title="Mensagem" style="display:none"><p>Esta data de validade &eacute; invalida!</p></div>


         <script src="js/bootstrap-filestyle.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataContrato").mask("99/99/9999");
				$("#DataValidade").mask("99/99/9999");
				$("#ValorContrato").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				$('#ValorItem').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

			 });

			 function voltar() {
				window.history.back()
			 }

			 function verificaData(){
 				oDiv = document.getElementById('botaoGravar');
				var inicio = document.getElementById('DataContrato');
				var fim = document.getElementById('DataValidade');
				var dInicio = inicio.value.split("/");
				var dFim = fim.value.split("/");
				var nDataInicio;
				var nDataFim;
				var i = 0;
					while(i < 3){
						nDataInicio = dInicio[i] + nDataInicio;
						nDataFim = dFim[i] + nDataFim;
						i ++;
					}
					if(nDataInicio < nDataFim ){
						document.getElementById("DataValidade").style.backgroundColor = '';
						oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";
						return true;
					}else{
						$(function() {
							$( "#msgValidade" ).dialog();
							oDiv.innerHTML = "<font color='red'><strong>Data de validade inválida</strong></font>";

							document.getElementById("email").style.backgroundColor = '#F89C8F';
							return false;
						});
						return false;
					}
			 }

 		</script>



<script>
$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {

        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
</div>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


