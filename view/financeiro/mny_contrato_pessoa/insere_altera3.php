<?php


$sOP = $_REQUEST['sOP'];
$voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
$voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyConta = $_REQUEST['voMnyConta'];

$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
$voMnyContratoDocTipo  = $_REQUEST['voMnyContratoDocTipo'];

$voMnyContratoPessoaPorId = $_REQUEST['voMnyContratoPessoaPorId'];
$voSysStatus = $_REQUEST['voSysStatus'];
$oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
$voMnyContratoAditivo = $_REQUEST['voMnyContratoAditivo'];

if($_REQUEST['oMnyContratoTipo'])
	$oContratoTipo = $_REQUEST['oMnyContratoTipo'];
else
	$voMnyContratoTipo =  $_REQUEST['voMnyContratoTipo'];

$oMnyContratoDocTipo = $_REQUEST['fContratoTipo'];
$voMnyContratoPessoa = $_REQUEST['voMnyContratoPessoa'];

$voMnyPessoa = $_REQUEST['voMnyPessoa'];
$oMnyMovimento =$_REQUEST['oMnyMovimento'];

$voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];
$oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title><?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?> - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaDocumentosAjax&sOP=Visualizar&nTipoContrato='+<?=($oContratoTipo) ? $oContratoTipo->getCodContratoTipo() : ""?>,'divDocumentos')" >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <!--&gt; <strong><?//php echo $sOP?> Contrato</strong>--><a href='?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>'> Gerenciar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></a>&gt;  <Strong>Cadastrar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></Strong> <!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

		   <h3 class="TituloPagina"><?= $sOP ?>  <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoPessoa.processaFormulario">
         <input type="hidden" name="sOP" value="<?= $sOP?>" />
         <input type="hidden" name="fContratoCodigo" value="<?=(is_object($oMnyContratoPessoa)) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>" />
		 <input type="hidden" name='fMovCodigo' required value="<?= ($oMnyMovimento)? $oMnyMovimento->getMovCodigo() : $_REQUEST['fMovCodigo']?>">
		 <input type="hidden" name='fMovItem' required value="<?= ($oMnyMovimento)? $oMnyMovimento->getMovItem() : $_REQUEST['fMovItem']?>">
         <input type='hidden' name='fPesCodigo'  required value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getPesCodigo() : ""?>'/>
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>
		 <input type='hidden' name='fAtivo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAtivo() : "1"?>'/>
		 <input type='hidden' name='sTipoLancamento' value='<?= $_REQUEST['sTipoLancamento']?> '/>
		 <input type='hidden' name='fTipoContrato' value='<?= $_REQUEST['fTipoContrato']?> '/>

<fieldset title="Dados de <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?>">
         <div id="formulario" class="TabelaAdministracao">
<legend title="Documentos">Dados Gerais</legend>
         <div class="form-group">
         	 <div class="col-sm-6">
             	<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="SysStatus">Status:</label>
                    <div class="col-sm-3">
                        <select name='fCodStatus'  class="form-control chosen"   required >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voSysStatus){
                                   foreach($voSysStatus as $oSysStatus){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
                                       }
                            ?>
                                       <option  <?= $sSelected ?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getDescStatus()?></option>
                            <?
                                   }
                               }
                            ?>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label" style="text-align:left">Tipo:</label>
                    <div class="col-sm-5">
                    	<? if($oContratoTipo) { ?>
						<input required  type='hidden' name='fContratoTipo'  required value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getTipoContrato() : $oContratoTipo->getCodContratoTipo()?>'>
                        <? echo $oContratoTipo->getDescricao();
                          }else{ ?>
                        <select name='fContratoTipo'  class="form-control chosen" onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&nTipoContrato='+this.value,'divDocumentos')" >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyContratoTipo){
                                   foreach($voMnyContratoTipo as $oMnyContratoTipo){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getTipoContrato() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
                                       }  ?>
                                       <option  <?= $sSelected ?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>
                               <?  }
                               }
                            ?>
                        </select>
                     <? } ?>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Contratado">Pessoa:</label>
                <div class="col-sm-10">
                    <select name='fPesCodigo'  class="form-control chosen"  required  >
                        <option value=''>Selecione</option>
                        <? $sSelected = "";
                           if($voMnyPessoa){
                               foreach($voMnyPessoa as $oMnyPessoa){
                                   if($oMnyContratoPessoa){
                                       $sSelected = ($oMnyContratoPessoa->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
                                   }?>
                                   <option  <?= $sSelected ?> value='<?= $oMnyPessoa->getPesgCodigo()?>'><?= $oMnyPessoa->getIdentificacao() . " - ". $oMnyPessoa->getNome()?></option>
                            <? }
                           }?>
                    </select>
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Numero">N&uacute;mero:</label>
                    <div class="col-sm-4">
                         <input required  class="form-control" type='text' id='Numero' placeholder='Nro' name='fNumero'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?>'/>
                    </div>
                    <label class="col-sm-2 control-label" style="text-align:left" for="ValorContrato">Valor:</label>
                    <div class="col-sm-4">
                    <input  required class="form-control" type='text' id='ValorContrato' placeholder='Valor' name='fValorContrato' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getValorContratoFormatado() : (( $oMnyMovimento) ?  $oMnyMovimento->getMovValorGlobFormatado() : " " )?>' <?=(($_REQUEST['fTipoContrato']) == 3 && ($_SESSION['Perfil']['CodUnidade'] == 200) && ($_SESSION['Perfil']['CodGrupoUsuario'] == 15))? "onBlur='validaValor()'" : "";?>/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Descricao">Descri&ccedil;&atilde;o:</label>
                    <div class="col-sm-10">
                         <input  required class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataContrato">Data:</label>
                    <div class="col-sm-4">
                        <input  required class="form-control" type='text'   id='DataContrato' placeholder='Contrato' name='fDataContrato'   value='<?= ($oMnyContratoPessoa && $oMnyContratoPessoa->getDataContrato()) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?>'/>
                    </div>
                    <? if($_REQUEST['fTipoContrato'] != 2){?>
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataValidade">Validade:</label>
                    <div class="col-sm-4">
                        <input  required class="form-control" type='text'   id='DataValidade' placeholder='Validade' name='fDataValidade'   value='<?= ($oMnyContratoPessoa && $oMnyContratoPessoa->getDataValidade()) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?>' >
                    </div>
                    <? } ?>
                </div>
                <div class="form-group">
				  <? if($_REQUEST['fTipoContrato'] == 1){?>
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataInicio">Inicio:</label>
                    <div class="col-sm-4">
                        <input  required class="form-control"  type='text'  id='DataInicio' placeholder='Inicio' name='fDataInicio'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataInicioFormatado() : ""?>' >
                    </div>
                 <? }?>
                </div>

             </div>
             <div class="col-sm-6">
               	<? if ($voMnyCusto){ ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
                        <div class="col-sm-10">
                         <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCusto){
                                           foreach($voMnyCusto as $oMnyCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
					                            <option  <?= $sSelected?> value='<?= $oMnyCusto->getPlanoContasCodigo()?>'> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                                    <?     }
                                       }
                                    ?>
                          </select>
                        </div>
                    </div>
          		<? } ?>
          		<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
                    <div class="col-sm-10">
                        <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
                           <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroNegocio){
                                           foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                } ?>
                                              <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?></option>
                                        <?  }
                                         }?>

                             </select>
                   </div>
                </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                     <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
                        <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyUnidade){
                                   foreach($voMnyUnidade as $oMnyUnidade){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                             <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?> </option>
                              <?   }
                               }?>
                      </select>
                  </div>
               </div>
              <? if($voMnyCentroCusto){ ?>
                   <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">C. Custo:</label>
                        <div class="col-sm-10">
                          <select name='fCenCodigo'  class="form-control chosen" required  onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.carregaTipo&sOP=1&fCenCodigo='+this.value,'divConta')" />
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroCusto){
                                           foreach($voMnyCentroCusto as $oMnyCentroCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
                            					<option  <?= $sSelected?> value='<?= $oMnyCentroCusto->getPlanoContasCodigo()?>'><?= $oMnyCentroCusto->getCodigo()?> - <?= $oMnyCentroCusto->getDescricao()?></option>
                                      <?   }
                                       } ?>
                          </select>
                        </div>
                    </div>
             <? } ?>
		        <div class="form-group" >
        	   		<label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                    <div class="col-sm-10"  id="divConta">
                        <select  name='fConCodigo'  class="form-control chosen"  required <?=$sSomenteLeitura?> >
                                <option  value=''>Selecione...</option>
                            <? $sSelected = "";
                               if($voMnyConta){
                                   foreach($voMnyConta as $oMnyConta){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnyConta->getPlanoContasCodigo()?>'><?= $oMnyConta->getCodigo()?> - <?= $oMnyConta->getDescricao()?></option>
                               <?  }
                              }?>
                        </select>
                    </div>
          	    </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
                   <div class="col-sm-10" id="divConta">
                        <select name='fSetCodigo'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnySetor){
                                   foreach($voMnySetor as $oMnySetor){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnySetor->getPlanoContasCodigo()?>'><?= $oMnySetor->getCodigo()?> -  <?= $oMnySetor->getDescricao()?>  </option>
                                <? }
                               } ?>
                      </select>
                    </div>
          		</div>
             </div>
         </div>
<? if($_REQUEST['fTipoContrato'] <> 2){?>
 <legend title="Documentos">Documentos</legend>
    <? if($_REQUEST['sOP'] == 'Alterar'){?>
          <div class="form-group">
          <? $i = 0;?>
				  <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                        <div>
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                         <? $voMnyContratoPessoaArquivoTipo = $oFachadaSys->recuperarTodosSysArquivoPoContratoPorTipoDocumento($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
                                                                            print_r($voMnyContratoPessoaArquivoTipo);
							$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor(); ?>
                                <div class="col-sm-12">
                                <? if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
											  if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
                                                    <input type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><br>
                                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
										     <? }
											   if($oMnyContratoPessoaArquivoTipo->getCodContratoCodigo() && ($_REQUEST['sOP'] =='Alterar' )){ ?>

                                                         <div class="col-sm-1" ><a target='_blank' href='<?=$oMnyContratoPessoaArquivoTipo->getNome()?>'><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button>	 </a> </div>
                                                         <div class="col-sm-2"><input type="file" data-buttonText="Atualizar" class="filestyle" data-iconName="glyphicon glyphicon-file" data-buttonName="btn-primary"  data-input="true"  name='arquivo[]'> </div>
                                                         <div class="col-sm-9"><a   data-toggle="tooltip" title="Excluir Documento" href="?action=MnyContratoArquivo.processaFormulario&sOP=Excluir&fCodContratoArquivo=<?=$oMnyContratoPessoaArquivoTipo->getCodContratoCodigo()?>&nIdMnyContratoPessoa=<?=$oMnyContratoPessoa->getContratoCodigo()?>&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>&nTipo=1"><button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></a> </div><hr><br>

                                                         <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodContratoCodigo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
											<?
                                                 } else{ ?>
                                                     <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Arquivo Obrigat&oacute;rio.</div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoCodigo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoCodigo() : ""?>">
                                                <? }
                                        }?>
                                <? }else{ ?>
                                             <input type="file" class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Arquivo Obrigat&oacute;rio. </div>" : ""?>
                                             <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                             <input type="hidden" name='fCodContratoArquivo[]' required value="">
                                <? } ?>
                                </div>
                              <? $i++ ?>
                        </div>
                        <p>
                        <p>
		  		  <? } ?>
               <br>
		  </div>
          <? }else{?>
          		<div class="form-group">
	          		<div class="col-sm-10" id="divDocumentos">Selecione o tipo de contrato para carregar os documentos necess&aacute;rios.</div>
    			</div>
          <? }?>
   <? }?>
  </div>
</div>
<br>
</fieldset>
          <div class="form-group">
                <div class="col-sm-offset-5 col-sm-6" id="botaoGravar">
                <button type="submit" id="btnGravar" class="btn btn-primary" ><?=$sOP?></button>
                </div>
   		  </div>
        <div class="form-group">
            <div id="divInfo"  class="col-sm-offset-5 col-sm-6">&nbsp;</div>
       </div>
       </form>
       <div>&nbsp;</div><br>
       <? if ($_REQUEST['sOP'] == 'Alterar' && $_REQUEST['fTipoContrato'] == 1){?>
       <legend>Contrato Aditivo</legend>
       <form method="post" action="" name="formMnyContratoAditivo" id="formMnyContratoAditivo" class="formulario">
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>
   			<table>
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyContratoAditivo" onChange="JavaScript: submeteForm('MnyContratoAditivo')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Cadastrar&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>" lang="0">Cadastrar novo Aditivo</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Alterar&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>" lang="1">Alterar Aditivo selecionado</option>
   						<option value="?action=MnyContratoAditivo.preparaFormulario&sOP=Detalhar&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>" lang="1">Detalhar Aditivo selecionado</option>
   						<option value="?action=MnyContratoAditivo.processaFormulario&sOP=Excluir&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>" lang="2">Excluir Aditivo(s) selecionado(s)</option>
   						</select>
                    </td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyContratoAditivo)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyContratoAditivo')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
					<th class='Titulo'>Numero</th>
					<th class='Titulo'>Motivo</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Validade</th>
					<th class='Titulo'>Valor</th>
					<th class='Titulo'>Anexo</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyContratoAditivo as $oMnyContratoAditivo){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyContratoAditivo')" type="checkbox" value="<?=$oMnyContratoAditivo->getAditivoCodigo()?>" name="fIdMnyContratoAditivo[]"/></td>
					<td><?= $oMnyContratoAditivo->getNumero()?></td>
					<td><?= $oMnyContratoAditivo->getMotivoAditivo()?></td>
					<td><?= $oMnyContratoAditivo->getDataAditivoFormatado()?></td>
					<td><?= $oMnyContratoAditivo->getDataValidadeFormatado()?></td>
					<td><?= $oMnyContratoAditivo->getValorAditivoFormatado()?></td>
					<td><? if($oMnyContratoAditivo->getAnexo()){?>
                    <a target="_blank" href='?action=MnyContratoAditivo.preparaArquivo&fAditivoCodigo=<?=$oMnyContratoAditivo->getAditivoCodigo()?>'>Visualizar Anexo</a>
                    <? }?>
                    </td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyContratoAditivo)){?>
  			</table>
          </form>
       <? } ?>



 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="js/bootstrap-filestyle.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataContrato").mask("99/99/9999");
				$("#DataValidade").mask("99/99/9999");
				$("#DataInicio").mask("99/99/9999");
				$("#ValorContrato").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });

			 function voltar() {
				window.history.back()
			 }

            function validaValor(){
                valorMaximoPermitido = 3000;
                movValor = document.getElementById('ValorContrato').value.replace(".","").replace(",",".")
                if(movValor > valorMaximoPermitido){
                    document.getElementById('divInfo').innerHTML = "<strong style='color: red; font-size: 12px;'> O valor máximo permitido é:" +valorMaximoPermitido+"</strong>";
                   document.getElementById('btnGravar').disabled = true;
                }else{
                    document.getElementById('divInfo').innerHTML = "";
                    document.getElementById('btnGravar').disabled = false;
                }


            }

/*			 function verificaData(){
					var inicio = document.getElementById('DataContrato');
					var fim = document.getElementById('DataValidade');
					var dInicio = inicio.value.split("/");
					var dFim = fim.value.split("/");
					var nDataInicio
					var nDataFim;

					nDataInicio = dInicio[0] + dInicio[1] + dInicio[2];
					nDataFim = dFim[0] + dFim[1] + dFim[2];
					if(nDataInicio >= nDataFim){
					<?//=  $_SESSION['sMsg'] = "Data de validade invalida !";	?>
						//document.getElementById('DataValidade').focus();
						document.formMnyContratoPessoa.fDescricao.focus()
						$( "#mensagem" ).dialog();


						 $("#formMnyContratoPessoa").submit(function(){
							return false;
						  });

						return false;
					}
			}*/

 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


   <!--Contrato-->
  <div class="modal fade" id="anexo" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
          </div>
    </div>
</div>

