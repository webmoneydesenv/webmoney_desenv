<?php
unset($_SESSION['oMnyContratoPessoa']);
$sOP = $_REQUEST['sOP'];
$voMnyUnidadePlanocontas = $_REQUEST['voMnyUnidadePlanocontas'];
$voMnySetorPlanocontas = $_REQUEST['voMnySetorPlanocontas'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];
$voMnySetor = $_REQUEST['voMnySetor'];
$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyConta = $_REQUEST['voMnyConta'];
$oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
if($oMnyContratoPessoa && ($oMnyContratoPessoa->getTipoContrato() == 2 || $oMnyContratoPessoa->getTipoContrato() == 10))
	$voMnyContratoAse = $_REQUEST['voMnyContratoAse'];
$voSysStatus = $_REQUEST['voSysStatus'];
$oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];

if($_REQUEST['oMnyContratoTipo'])
	$oContratoTipo = $_REQUEST['oMnyContratoTipo'];

$oMnyContratoDocTipo = $_REQUEST['fContratoTipo'];
$voMnyPessoa = $_REQUEST['voMnyPessoa'];

 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title><?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?> - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->
 <link rel="stylesheet" href="css/formValidation.css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body onLoad="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaDocumentosAjax&sOP=Visualizar&nTipoContrato='+<?=($oContratoTipo) ? $oContratoTipo->getCodContratoTipo() : ""?>,'divDocumentos')" >

 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <!--&gt; <strong><? //php echo $sOP?> Contrato</strong>--><a href='?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>'> Gerenciar <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></a>&gt;  <Strong><?=($oContratoTipo) ? $sOP . " " . $oContratoTipo->getDescricao() : "Contrato"?></Strong> <!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

		   <h3 class="TituloPagina"><?= $sOP ?>  <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?></h3>

       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <form method="post" id="AseItemForm" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyContratoPessoa.processaFormulario" >
         <input type="hidden" name="sOP" value="<?= $sOP?>" />
		 <input type='hidden' name='sTipoLancamento' value='<?= $_REQUEST['sTipoLancamento']?> '/>
		 <input type='hidden' name='fTipoContrato' value='<?= $_REQUEST['fTipoContrato']?> '/>
		 <input type='hidden' name='fAtivo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getAtivo() : "1"?>'/>
         <input type='hidden' name='fContratoCodigo' value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getContratoCodigo() : ""?>'/>
<fieldset title="Dados de <?=($oContratoTipo) ? $oContratoTipo->getDescricao() : "Contrato"?>">
         <div id="formulario" class="TabelaAdministracao">
<legend title="Documentos">Dados Gerais</legend>
         <div class="col-sm-6">
		<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="SysStatus">Status:</label>
                    <div class="col-sm-4">
                        <select name='fCodStatus'  class="form-control chosen"  required>
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voSysStatus){
                                   foreach($voSysStatus as $oSysStatus){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
                                       }
                            ?>
                                       <option  <?= $sSelected ?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getDescStatus()?></option>
                            <?
                                   }
                               }
                            ?>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label" style="text-align:left">Tipo:</label>
                    <div class="col-sm-3">
                    	<? if($oContratoTipo) { ?>
						<input type='hidden' name='fContratoTipo'  required value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getTipoContrato() : $oContratoTipo->getCodContratoTipo()?>'/>
                        <? echo $oContratoTipo->getDescricao();
                          }else{ ?>
                        <select name='fContratoTipo'  class="form-control chosen" onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar&nTipoContrato='+this.value,'divDocumentos')" >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyContratoTipo){
                                   foreach($voMnyContratoTipo as $oMnyContratoTipo){
                                       if($oMnyContratoPessoa){
                                           $sSelected = ($oMnyContratoPessoa->getTipoContrato() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
                                       }  ?>
                                       <option  <?= $sSelected ?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>
                               <?  }
                               }
                            ?>
                        </select>
                     <? } ?>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Contratado">Pessoa:</label>
                <div class="col-sm-10">
                    <select name='fPesCodigo'  class="form-control chosen"  >
                        <option value=''>Selecione</option>
                        <? $sSelected = "";
                           if($voMnyPessoa){
                               foreach($voMnyPessoa as $oMnyPessoa){
                                   if($oMnyContratoPessoa){
                                       $sSelected = ($oMnyContratoPessoa->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
                                   }?>
                                   <option  <?= $sSelected ?> value='<?= $oMnyPessoa->getPesgCodigo()?>'><?= $oMnyPessoa->getIdentificacao() . " - ". $oMnyPessoa->getNome()?></option>
                            <? }
                           }?>
                    </select>
                </div>
                </div>
                             <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Numero">N&uacute;mero:</label>
                    <div class="col-sm-4">
                         <input class="form-control" type='text' id='Numero' placeholder='Nº' name='fNumero'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?>'/>
                    </div>
                    <label class="col-sm-2 control-label" style="text-align:left" for="ValorContrato">Valor:</label>
                    <div class="col-sm-4">
                    <input class="form-control" type='text' id='ValorContrato' placeholder='Valor' name='fValorContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getValorContratoFormatado() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Descricao">Descri&ccedil;&atilde;o:</label>
                    <div class="col-sm-6">
                         <input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?>'/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataContrato">Data:</label>
                    <div class="col-sm-3">
                        <input class="form-control" type='text' id='DataContrato' placeholder='Inicio' name='fDataContrato'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?>'/>
                    </div>
                    <? if($_REQUEST['fTipoContrato'] != 2){?>
                    <label class="col-sm-2 control-label" style="text-align:left" for="DataValidade">Validade:</label>
                    <div class="col-sm-4">
                        <input class="form-control" type='text'  id='DataValidade' placeholder='Validade' name='fDataValidade'   value='<?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?>'/>
                    </div>
                    <? } ?>
                </div>
</div>
<div class="col-sm-6">
               	<? if ($voMnyCusto){ ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
                        <div class="col-sm-6">
                         <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCusto){
                                           foreach($voMnyCusto as $oMnyCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
					                            <option  <?= $sSelected?> value='<?= $oMnyCusto->getPlanoContasCodigo()?>'> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                                    <?     }
                                       }
                                    ?>
                          </select>
                        </div>
                    </div>
          		<? } ?>
          		<div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
                    <div class="col-sm-6">
                        <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
                           <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroNegocio){
                                           foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                } ?>
                                              <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?></option>
                                        <?  }
                                         }?>

                             </select>
                   </div>
                </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-6">
                     <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
                        <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyUnidade){
                                   foreach($voMnyUnidade as $oMnyUnidade){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                             <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?> </option>
                              <?   }
                               }?>
                      </select>
                  </div>
               </div>
              <? if($voMnyCentroCusto){ ?>
                   <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">C. Custo:</label>
                        <div class="col-sm-6">
                          <select name='fCenCodigo'  class="form-control chosen" required  onChange="recuperaConteudoDinamico('index.php','action=MnyContratoPessoa.carregaTipo&sOP=1&fCenCodigo='+this.value,'divConta')" />
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroCusto){
                                           foreach($voMnyCentroCusto as $oMnyCentroCusto){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getCenCodigo() == $oMnyCentroCusto->getPlanoContasCodigo()) ? "selected" : "";
                                                }?>
                            					<option  <?= $sSelected?> value='<?= $oMnyCentroCusto->getPlanoContasCodigo()?>'><?= $oMnyCentroCusto->getCodigo()?> - <?= $oMnyCentroCusto->getDescricao()?></option>
                                      <?   }
                                       } ?>
                          </select>
                        </div>
                    </div>
             <? } ?>
		        <div class="form-group" >
        	   		<label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                    <div class="col-sm-6"  id="divConta">
                        <select  name='fConCodigo'  class="form-control chosen"  required <?=$sSomenteLeitura?> >
                                <option  value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnyConta){
                                   foreach($voMnyConta as $oMnyConta){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnyConta->getPlanoContasCodigo()?>'><?= $oMnyConta->getCodigo()?> - <?= $oMnyConta->getDescricao()?></option>
                               <?  }
                              }?>
                        </select>
                    </div>
          	    </div>
               <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
                   <div class="col-sm-6" id="divConta">
                        <select name='fSetCodigo'  class="form-control chosen"  required >
                                <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voMnySetor){
                                   foreach($voMnySetor as $oMnySetor){
                                       if($oMnyMovimento){
                                           $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        }elseif($oMnyContratoPessoa){
                                            $sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                                        } ?>
                                        <option  <?= $sSelected?> value='<?= $oMnySetor->getPlanoContasCodigo()?>'><?= $oMnySetor->getCodigo()?> -  <?= $oMnySetor->getDescricao()?>  </option>
                                <? }
                               } ?>
                      </select>
                    </div>
          		</div>
             </div>
   </div>
   </fieldset>

   <? //if($_REQUEST['fTipoContrato'] == 10){?>
  <? if($voMnyContratoDocTipo){ ?>

          <div class="form-group">
              <div class="col-sm-12">
                <legend title="Documentos">Documentos</legend>
              </div>
            </div>
                    <div class="jumbotron col-sm-12" style='background: #e9f2f9;'>
          <? $i = 0;
		     $j =0; ?>


				<?  foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                <div class="form-group">

							<? if($oMnyContratoPessoa)
                                $voMnyContratoPessoaArquivoTipo = $oFachadaSys->recuperarTodosSysArquivoPorContratoTipoArquivo($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());

							?>
                                <div class="col-sm-12">
                                     <label class="col-sm-2 control-label"  style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao() ?>:</label>
                                <?
                                    if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){


                                               if($oMnyContratoPessoaArquivoTipo->getCodArquivo() ){
                                                        echo "
                                                         <div class=\"col-sm-1 \" ><a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'><button type=\"button\" class=\"btn btn-success btn-sm \"  style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button>	 </a> </div>
                                                         <div class=\"col-sm-1\"><a data-toggle=\"tooltip\" title=\"Excluir Documento\" href=\"?action=SysArquivo.processaFormulario&sOP=Excluir&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."&fContratoCodigo=".$oMnyContratoPessoaArquivoTipo->getContratoCodigo() ." \"><button type=\"button\" class=\"btn btn-danger \" style=\"font-size:6px; \"><i class=\"glyphicon glyphicon-trash\"></i></button></a> </div>
                                                         <div class=\"col-sm-3\"><input type=\"file\" id=\"file$j\"  onblur=\"verificaUpload('file$j',".$oMnyContratoDocTipo->getTamanhoArquivo().")\"  data-buttonText=\"Atualizar\" class=\"\" style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" /></div>
                                                         <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'>".(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)."MB</span>
                                                         "; ?>

                                                         <input type="hidden" name='fCodArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()."||".$oMnyContratoPessoaArquivoTipo->getCodArquivo()?>">
                                                <? } else{ ?>
                                                     <!--<input type="file"  id='file<?//=$j?>'  onblur="upload('file<?//=$j?>')"  class="filestyle" data-buttonName="btn-primary"  data-iconName="glyphicon glyphicon-inbox" data-input="false"   name="arquivo[]" <?//= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/> <?//= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Campo Obrigat&oacute;rio. </div>" : ""?>-->
                                                     <input type="file" id='file<?=$j?>'  onblur="verificaUpload('file<?=$j?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<div style=\"color:red;\"> * Campo Obrigat&oacute;rio. </div>" : ""?>
                                                     <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                     <input type="hidden" name='fCodArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodArquivo() : ""?>">
												<? }
                                        }?>
                                <? }else{ ?>

                                            <div class='col-sm-4'>
                                             <input type="file" id='file<?=$j?>'  onblur="verificaUpload('file<?=$j?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)"  class=""  style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/>
                                             </div>
                                            <div class="col-sm-1">
                                             <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                            </div>
                                           <div class="col-sm-2">
                                             <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? "<span style=\"color:red;\" > * Arquivo Obrigat&oacute;rio. </span>" : ""?>
                                            </div>

                                            <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">



                                <? } ?>
                                </div>
                                <p style='border-bottom: 1px solid #ccc;'>&nbsp;</p>
                              <? $i++ ?>
                              <? $j++ ?>

                            </div>
		  		  <? } ?>

                  <div class="col-sm-2">
                       <div class="alert alert-danger" role="alert" style="text-align:center; font-weight:bold;" >
                           <a href='https://smallpdf.com/pt/compressor-de-pdf' target="_blank">COMPRESSOR DE PDF</a>
                        </div>
                  </div>
 </div>

<? }?>
   <!--fim documentos-->

   <legend>Itens da ASE</legend>
   		<div class="form-group">
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary addButton" onClick="addCampos()">Adicionar Campos <i class="glyphicon glyphicon-plus"></i></button><br><br>
                <? if($sOP == 'Alterar' && $voMnyContratoAse){ ?>
				<div id="linha">
                	<? foreach($voMnyContratoAse as $oMnyContratoAse){ ?>
                    <div id="linha<?=($oMnyContratoAse) ? $oMnyContratoAse->getContratoCodigo() ."||".$oMnyContratoAse->getItem(): ""?>">
                          <div class='col-sm-4'>
                            <input type='text' class='form-control' required id='AseItemDescricao<?=($oMnyContratoAse) ? $oMnyContratoAse->getContratoCodigo() ."||".$oMnyContratoAse->getItem(): ""?>' name='AseItemDescricao[]' value="<?=($oMnyContratoAse) ? $oMnyContratoAse->getDescricao(): ""?>" placeholder='Descri&ccedil;&atilde;o' />
                          </div>
                          <div class='col-sm-2'>
                            <input type='text' class='form-control' required id='AseItemValor<?=($oMnyContratoAse) ? $oMnyContratoAse->getContratoCodigo() ."||".$oMnyContratoAse->getItem(): ""?>' name='AseItemValor[]' placeholder='Valor' value="<?=($oMnyContratoAse) ? $oMnyContratoAse->getValorFormatado(): ""?>" />
                          </div>
                          <button  type='button' class='btn btn-danger removeButton' onClick="recuperaConteudoDinamico('index.php','action=MnyContratoAse.processaFormulario&sOP=Excluir&fIdMnyContratoAse=<?=($oMnyContratoAse) ? $oMnyContratoAse->getContratoCodigo() ."||".$oMnyContratoAse->getItem(): ""?>','linha');" value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button><br><br>
                      </div>
                      <? } ?>
                   </div>
                </div>
                <? }else{ ?>
                    <div class='form-group'>
                      <div class='col-xs-4'>
                        <input type='text' class='form-control' required id='AseItemDescricao' name='AseItemDescricao[]' value="" placeholder='Descri&ccedil;&atilde;o' />
                      </div>
                      <div class='col-xs-2'>
                        <input type='text' class='form-control' required id='AseItemValor' name='AseItemValor[]' placeholder='Valor' value="" />
                      </div>
                    </div>
                <? } ?>
            </div>

        	<div class="col-xs-12">
          		<div id="campoPai"></div>
          	</div>

	    <div class="form-group">
        <div class="col-sm-offset-5 col-sm-6" id="botaoGravar">
            <button type="submit" id="btnEnviar" class="btn btn-primary" ><?=$sOP?></button>
             <div class='col-sm-12' id='divEnviar'></div>
        </div>
     </div>
</form>
	    <div id="msgValidade" title="Mensagem" style="display:none"><p>Data de validade &eacute; inv&aacute;lida!</p></div>

        <script src="js/jquery/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/formValidation.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataContrato").mask("99/99/9999");
				$("#DataValidade").mask("99/99/9999");
				$("#ValorContrato").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				$('#AseItemValor').maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

			 });

			 function voltar() {
				window.history.back()
			 }

			 function verificaData(){
 				oDiv = document.getElementById('botaoGravar');
				var inicio = document.getElementById('DataContrato');
				var fim = document.getElementById('DataValidade');
				var dInicio = inicio.value.split("/");
				var dFim = fim.value.split("/");
				var nDataInicio;
				var nDataFim;
				var i = 0;
					while(i < 3){
						nDataInicio = dInicio[i] + nDataInicio;
						nDataFim = dFim[i] + nDataFim;
						i ++;
					}
					if(nDataInicio < nDataFim ){
						document.getElementById("DataValidade").style.backgroundColor = '';
						oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";
						return true;
					}else{
						$(function() {
							$( "#msgValidade" ).dialog();
							oDiv.innerHTML = "<font color='red'><strong>Data de validade inválida</strong></font>";

							document.getElementById("email").style.backgroundColor = '#F89C8F';
							return false;
						});
						return false;
					}
			 }

			 var qtdeCampos = 0;

			function addCampos(){
				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:
				document.getElementById("filho"+qtdeCampos).innerHTML = "<div class='form-group'><div class='col-xs-4'><input type='text' class='form-control' required id='AseItemDescricao"+qtdeCampos+"' name='AseItemDescricao[]' placeholder='Descri&ccedil;&atilde;o' /></div><div class='col-xs-2'><input type='text' class='form-control' required id='AseItemValor"+qtdeCampos+"' name='AseItemValor[]' placeholder='Valor' /></div><button  type='button' class='btn btn-danger removeButton' onclick='removerCampo("+qtdeCampos+")' value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>";

				var x = document.createElement("SCRIPT");
				var t = document.createTextNode("jQuery(function($){ $('#AseItemValor"+qtdeCampos+"').maskMoney(	{symbol:'R$', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});	});");
				x.appendChild(t);
				document.body.appendChild(x);
				qtdeCampos++;
			}

			function removerCampo(id){
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);
			}
 		</script>

</div>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>


