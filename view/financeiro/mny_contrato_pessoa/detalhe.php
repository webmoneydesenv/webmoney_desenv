<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voMnyContratoArquivo = $_REQUEST['voMnyContratoArquivo'];
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
 $oMnyContratoPessoa = $_REQUEST['oMnyContratoPessoa'];
 $voMnyMovimento = $_REQUEST['voMnyMovimento'];
 $voAditivo = $_REQUEST['voAditivo'];
?>
<!doctype html>
<html>
<!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Contrato -<?php echo $sOP ?></title>
<!-- InstanceEndEditable -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
</head>
<body>
<div class="container">
  <header>
    <?php include_once("view/includes/topo.php")?>
  </header>
  <?php include_once("view/includes/menu.php")?>
  <div class="content">
    <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt; <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>">Gerenciar Contratos</a> &gt; <strong><?php echo $sOP?> Contrato</strong><!-- InstanceEndEditable --></div>
    <!-- InstanceBeginEditable name="titulo" -->
    <h3 class="TituloPagina"><?php echo $sOP?> Contrato</h3>
    <!-- InstanceEndEditable -->
    <section> <!-- InstanceBeginEditable name="conteudo" -->
      <form method="post" class="form-horizontal" name="formMnyContratoPessoa" action="?action=MnyContratoPessoa.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
        <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Dados do MnyContratoPessoa" >
          <div class="form-group">
            <div class="col-sm-12">
            <legend>Dados Gerais</legend>
              <div class="form-group">
                 <div class="col-sm-8">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nº:</strong><strong style='color: red;'><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getNumero() : ""?></strong></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nome: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyPessoaGeral()->getNome() :  ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Status: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getSysStatus()->getDescStatus() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Descri&ccedil;&atilde;o: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDescricao() : ""?></div></div>
                     <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Saldo Contrato: </strong><strong style='color: red;'><?= ($_REQUEST['nSaldoContrato']) ? "R$".number_format($_REQUEST['nSaldoContrato']->getAtivo(), 2, ',', '.') : ""?></strong></div></div>
				</div>
                <div class="col-sm-4">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Vencimento: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataValidadeFormatado() : ""?></div></div>
					  <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Contrato: </strong><?=  ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getDataContratoFormatado() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Valor: </strong><?= ($oMnyContratoPessoa) ? "R$".$oMnyContratoPessoa->getValorContratoFormatado(): "" ?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Unidade: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyPlanoContasUnidade()->getDescricao() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Setor: </strong><?= ($oMnyContratoPessoa) ? $oMnyContratoPessoa->getMnyPlanoContasSetor()->getDescricao() : ""?></div></div>
				</div>
             </div>
            <? if($voAditivo){?>
          <div class="form-group">
            <div class="col-sm-12">
            <legend>Aditivos</legend>
              <div class="form-group">
                 <div class="col-sm-12">
                     <div class="col-sm-2"><strong>Numero</strong></div>
                         <div class="col-sm-3"><strong>Motivo</strong></div>
                         <div class="col-sm-2"><strong>Data</strong></div>
                         <div class="col-sm-2"><strong>Validade</strong></div>
                         <div class="col-sm-2"><strong>Valor</strong></div>
                         <div class="col-sm-1"><strong>Anexo</strong></div>
                  </div>
                  <? foreach($voAditivo as $oAditivo){?>
                <div class="col-sm-12">
                      <div class="col-sm-2"><?=$oAditivo->getNumero()?></div>
                     <div class="col-sm-3"><?=$oAditivo->getMotivoAditivo()?></div>
                     <div class="col-sm-2"><?=$oAditivo->getDataAditivoFormatado()?></div>
                     <div class="col-sm-2"><?=$oAditivo->getDataValidadeFormatado()?></div>
                     <div class="col-sm-2"><?=$oAditivo->getValorAditivoFormatado()?></div>
                     <div class="col-sm-1"><?=($oAditivo->getAnexo()) ? "<a target='_blank' href='?action=MnyContratoAditivo.preparaArquivo&fAditivoCodigo=".$oAditivo->getAditivoCodigo()."'><i class=\"glyphicon glyphicon-folder-open btn  btn-sm btn-success\">&nbsp;Abrir</i></a>" : "S/Anexo"?></div>
				</div>
                 <? } ?>
             </div>

                <? } ?>

            <? if($voMnyMovimento){?>
             <div class="form-group">
             	<div class="col-sm-12">
                <legend>Movimento Relacionados</legend>
                </div>
             	<? foreach($voMnyMovimento as $oMnyMovimento){?>
                  <div class="col-sm-12">
                  	<div class="col-sm-2">
                  <a href="?action=MnyMovimento.preparaFormulario&sOP=Alterar&sTipoLancamento=<?= ($oMnyMovimento->getMovTipo() == 188)? "Pagar" : "Receber"?>&nMovItem=1&nIdMnyMovimento=<?= $oMnyMovimento->getMovCodigo()?>&fContratoTipo=<?=$oMnyContratoPessoa->getTipoContrato()?>"   data-toggle="tooltip" title="Visualizar Movimento" target="_blank"><i class=" glyphicon glyphicon-edit"></i></a>
					<strong>Movimento:&nbsp;&nbsp;</strong><?=$oMnyMovimento->getMovCodigo() .".". $oMnyMovimento->getMovItem() ?>
                    </div>
                    <div class="col-sm-3">
                 		<strong>Valor:&nbsp;&nbsp;</strong><?= number_format($oMnyMovimento->getValorLiquido() , 2, ',', '.');?>
                    </div>
                                       <div class="col-sm-7">
                    <strong>Documento:&nbsp;&nbsp;</strong><?=$oMnyMovimento->getMovDocumento()?>
                    </div>
                    </div>
                <? }?>
             </div>
             <? }else{?>
					<div class="col-sm-12"><strong>N&atilde;o h&aacute; movimento para este contrato.</strong><?//=$oMnyMovimento->getMovDocumento()?></div>
             <? }?>
  <? if($_REQUEST['sOP'] == 'Detalhar' && $_REQUEST['fTipoContrato'] != 2){?>
      <legend title="Documentos">Documentos</legend>
          <div class="form-group">
          <? $i = 0;?>
				  <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                        <div>
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                         <? $voMnyContratoPessoaArquivoTipo = $oFachadaSys->recuperarTodosSysArquivoPorContratoTipoArquivo($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
							$nRegistros = $oFachada->recuperarTodosMnyContratoArquivoPorContratoTipoArquivoContador($oMnyContratoPessoa->getContratoCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo())->getRealizadoPor(); ?>
                                <div class="col-sm-12">
                               <?     if($voMnyContratoPessoaArquivoTipo){
                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){

											   if($oMnyContratoPessoaArquivoTipo->getCodArquivo()){
  													    echo"<div class=\"col-sm-6 \" ><a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'><i class=\"glyphicon glyphicon-folder-open btn  btn-sm btn-success\">&nbsp;Abrir</i></a></div>   ";

												 }
                                        }?>
                                <? } ?>
                                </div>
                              <? $i++ ?>
                        </div>
                        <p>
                        <p>
		  		  <? } ?>
               <br>
		  </div>
          <? }else{?>
          		<div class="form-group">
					<!-- <div class="col-sm-10" id="divDocumentos">Selecione o tipo de contrato para carregar os documentos necess&aacute;rios.</div>-->
   			</div>
          <? }?>
  </div>




        </fieldset>
        <br/>
        <div class="form-group">
          <div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyContratoPessoa.preparaLista&fTipoContrato=<?=$_REQUEST['fTipoContrato']?>">Voltar</a></div>
        </div>
        </div>
      </form>
      <script src="js/jquery/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
      <script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
      <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
      <script src="js/producao.js" type="text/javascript"></script>
      <!-- InstanceEndEditable -->
      </h1>
    </section>
    <!-- end .content -->
  </div>
  <?php include_once("view/includes/mensagem.php")?>
  <footer>
    <?php require_once("view/includes/rodape.php")?>
  </footer>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd -->
</html>
