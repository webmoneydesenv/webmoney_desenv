<?php
 $sOP = $_REQUEST['sOP'];
 $oMnySaldoDiario = $_REQUEST['oMnySaldoDiario'];
 $voMnyTipoCaixa = $_REQUEST['voMnyTipoCaixa'];
 $voMnyContaCorrente = $_REQUEST['voMnyContaCorrente'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Saldo Diário - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnySaldoDiario.preparaLista">Gerenciar Saldo Diários</a> &gt; <strong><?php echo $sOP?> Saldo Diário</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Saldo Diário</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnySaldoDiario" action="?action=MnySaldoDiario.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodSaldoDiario" value="<?=(is_object($oMnySaldoDiario)) ? $oMnySaldoDiario->getCodSaldoDiario() : ""?>" />
         <input type='hidden' name='fRealizadoPor'   value='<?= ($oMnySaldoDiario) ? $oMnySaldoDiario->getRealizadoPor() : $_SESSION['oUsuarioImoney']->getNome()?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Saldo Diário">

 							<input type='hidden' name='fCodSaldoDiario' value='<?= ($oMnySaldoDiario) ? $oMnySaldoDiario->getCodSaldoDiario() : ""?>'/>

 <div class="form-group">
                <label class="col-sm-1 control-label" style="text-align:left" >Unidade:</label>
                <div class="col-sm-3">
                      <select name='fCodUnidade'  class="form-control chosen"  onChange="recuperaConteudoDinamico('index.php','action=MnySaldoDiario.carregaConta&fCodUnidade='+this.value,'divComplemento');" required  >
                            <option value=''>Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                           if($oMnySaldoDiario){
                                               $sSelected = ($oMnySaldoDiario->getCodUnidade() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                           }
                                ?>
                        <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?>
                        </option>
                        <?
                                       }
                                   }
                                ?>
                      </select>
                 </div>
					<label class="col-sm-1 control-label" style="text-align:left" for="MnyTipoCaixa">Conta:</label>
                    <div class="col-sm-3">
                        <div id="divComplemento">
                            <select name='fCcrCodigo'  class="form-control chosen"  required  >
                                <option value=''>Selecione</option>
                                <? $sSelected = "";
                                   if($voMnyContaCorrente){
                                       foreach($voMnyContaCorrente as $oMnyContaCorrente){
                                           if($oMnySaldoDiario){
                                               $sSelected = ($oMnySaldoDiario->getCcrCodigo() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
                                           }
                                ?>
                                           <option  <?= $sSelected?> value='<?= $oMnyContaCorrente->getCcrCodigo()?>'><?= $oMnyContaCorrente->getCcrConta()?></option>
                                <?
                                       }
                                   }
                                ?>
                            </select>
                         </div>
                     </div>
                    <label class="col-sm-1 control-label" style="text-align:left" for="MnyContaCorrente">Tipo:</label>
					<div class="col-sm-3">
					<select name='fCodTipoCaixa'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyTipoCaixa){
							   foreach($voMnyTipoCaixa as $oMnyTipoCaixa){
								   if($oMnySaldoDiario){
									   $sSelected = ($oMnySaldoDiario->getCodTipoCaixa() == $oMnyTipoCaixa->getCodTipoCaixa()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyTipoCaixa->getCodTipoCaixa()?>'><?= $oMnyTipoCaixa->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                </div>
    <div class="form-group">
                    <label class="col-sm-1 control-label" style="text-align:left" for="Data">Data:</label>
					<div class="col-sm-3">
					  <input class="form-control" type='text' id='Data' placeholder='Data' name='fData'  required   value='<?= ($oMnySaldoDiario) ? $oMnySaldoDiario->getDataFormatado() : ""?>'/>
				    </div>
					<label class="col-sm-1 control-label" style="text-align:left" for="Valor">Valor:</label>
					<div class="col-sm-3">
					  <input class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'  required   value='<?= ($oMnySaldoDiario) ? $oMnySaldoDiario->getValorFormatado() : ""?>'/>
				    </div>
				</div>
				<br>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnySaldoDiario) ? $oMnySaldoDiario->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#Data").mask("99/99/9999");
				$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
