﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPessoaFisica = $_REQUEST['oMnyPessoaFisica'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Pesssoa Física - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPessoaFisica.preparaLista">Gerenciar Pesssoa Físicas</a> &gt; <strong><?php echo $sOP?> Pesssoa Física</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Pesssoa Física</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPessoaFisica" action="?action=MnyPessoaFisica.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyPessoaFisica" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesfisCodigo">Pesfis_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pesfis_codigo' name='fPesfisCodigo' value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SysStatus">Status:</label>
					<div class="col-sm-2">
					<select name='fCodStatus'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysStatus){
							   foreach($voSysStatus as $oSysStatus){
								   if($oMnyPessoaFisica){
									   $sSelected = ($oMnyPessoaFisica->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getCodStatus()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesfisNome">Nome:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' placeholder='Nome' name='fPesfisNome' value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisNome() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="PesfisCpf">Cpf:</label>
					<div class="col-sm-7">
					<input class="form-control" type='text' placeholder='Cpf' name='fPesfisCpf' value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisCpf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesfisDescricao">Descricao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Descricao' name='fPesfisDescricao' value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisDescricao() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyPessoaFisica.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
