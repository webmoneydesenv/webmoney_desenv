<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPessoaFisica = $_REQUEST['oMnyPessoaFisica'];
 $oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];
 $voSysStatus = $_REQUEST['voSysStatus'];


 $voMnySetor = $_REQUEST['voMnySetor'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 $voSysBanco = $_REQUEST['voSysBanco'];
 $voSysContaTipo = $_REQUEST['voSysContaTipo'];
 $voMnyContratoPessoa = $_REQUEST['voMnyContratoPessoa'];

# print_r($_REQUEST);

 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Pessoa Física - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPessoaGeral.preparaLista">Gerenciar Pessoas</a> &gt; <strong><?php echo $sOP?> Pesssoa Física</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Pessoa Física</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPessoaFisica" action="?action=MnyPessoaGeral.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
           <input type="hidden" name="PesfisCodigo" value="<?=(is_object($oMnyPessoaFisica)) ? $oMnyPessoaFisica->getPesfisCodigo() : ""?>" />
         <input type='hidden' name='fAtivo' value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getAtivo() : "1"?>'/>
         <input type="hidden" name="fPesgCodigo" value="<?=(is_object($oMnyPessoaGeral)) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>" />
         <input type="hidden" name='fPesgAlt' required  value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgAlt() : ""?>'/>
		 <input type="hidden" name='fPesgInc' required  value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgInc() : ""?>'/>
		 <input type="hidden" name='fTipCodigo' value="<?=($oMnyPessoaGeral) ? $oMnyPessoaGeral->getTipCodigo():256?>">
         <div id="formulario" class="TabelaAdministracao">
           <fieldset title="Dados do Pessoa">
           <legend>Dados Gerais</legend>

 							<input type='hidden' name='fPesgCodigo' value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>'/>



	<div class="form-group">
                <label class="col-sm-1 control-label" style="text-align:left" for="SysStatus">STATUS:</label>
				<div class="col-sm-2">
					<select name='fCodStatus'  class="form-control" required >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysStatus){
							   foreach($voSysStatus as $oSysStatus){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getDescStatus()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                </div>
				<br>
				<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndCep">CEP:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id="cep" placeholder='CEP' name='fPesgEndCep'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndCep() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndLogra">Endere&ccedil;o:</label>
					<div class="col-sm-7">
					<input class="form-control" type='text' name='fPesgEndLogra' placeholder='Endere&ccedil;o' id='rua'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndLogra() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndBairro">Bairro:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='bairro' placeholder='Bairro' name='fPesgEndBairro'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndBairro() : ""?>'/>
				</div>

					<label class="col-sm-1 control-label" style="text-align:left" for="Cidade">Cidade:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='cidade' placeholder='Cidade' name='fCidade'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getCidade() : ""?>'/>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="Estado">UF:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='estado' placeholder='UF' name='fEstado'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getEstado() : ""?>'/>
				</div>
				</div>
				<br>
<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEmail">E-mail:</label>
					<div class="col-sm-4">
						<input class="form-control" type='text' id='email'     placeholder='E-mail' name='fPesgEmail'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEmail() : ""?>'/>
					</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="PesFones">Telefone:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='fone' placeholder='Telefone' name='fPesFones'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesFones() : ""?>'/>
				</div>
                	<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoAgencia">Ag&ecirc;ncia:</label>
					<div class="col-sm-2">
					<input class="form-control" type='text' id='PesgBcoAgencia' placeholder='Ag&ecirc;ncia' name='fPesgBcoAgencia'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgBcoAgencia() : ""?>'/>
				</div>
				</div>
				<br>
 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="SysBanco">Banco:</label>
					<div class="col-sm-3">
					<select name='fBcoCodigo'  class="form-control chosen"    >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysBanco){
							   foreach($voSysBanco as $oSysBanco){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getBcoCodigo() == $oSysBanco->getBcoCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysBanco->getBcoCodigo()?>'><?= $oSysBanco->getBcoNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="SysContaTipoPlanocontas">Tipo:</label>
					<div class="col-sm-3">
					<select name='fTipoconCod'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysContaTipo){
							   foreach($voSysContaTipo as $oSysContaTipo){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getTipoconCod() == $oSysContaTipo->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysContaTipo->getPlanoContasCodigo()?>'><?= $oSysContaTipo->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoConta">Conta:</label>
					<div class="col-sm-3">
					<input class="form-control" type='text' id='PesgBcoConta' placeholder='Conta' name='fPesgBcoConta'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgBcoConta() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getAtivo() : "1"?>'/>
				</div>
				<br>


         <!---#############################--->
         <fieldset title="Dados do Pesssoa Física">
         <legend>Dados da Pesssoa Física</legend>
 			<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesfisNome">Nome:</label>
					<div class="col-sm-6">
					<input class="form-control" type='text' id='PesfisNome' placeholder='Nome' name='fPesfisNome'  required   value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisNome() : ""?>'/>
				</div>
					<label   class="col-sm-1 control-label" style="text-align:left" for="PesfisCpf">Cpf:</label>
					<div  class="col-sm-4">
					<input class="form-control" type='text' id='cpf' placeholder='Cpf' name='fPesfisCpf' onblur='valida_CPF()' required   value='<?= ($oMnyPessoaFisica) ? $oMnyPessoaFisica->getPesfisCpf() : ""?>'/>
				</div>
				</div>
				<br>
         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2" id="botaoGravar">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
         <?php if ($sOP == 'Alterar'){ ?>

         <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <thead>
            <tr>
                <td height="28"><h2 class="TituloPagina">Gerenciar Contratos de Pessoa</h2></td>
            </tr>
            </thead>
        <tbody>
 		<tr>
 			<td>
 			<form method="post" action="" name="formMnyContratoPessoa" id="formMnyContratoPessoa" class="formulario">
            <input type="hidden" name="fPesgCodigo" value="<?=(is_object($oMnyPessoaGeral)) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>" />
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyContratoPessoa" onChange="JavaScript: submeteForm('MnyContratoPessoa')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyContratoPessoa.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Contrato</option>
   						<option value="?action=MnyContratoPessoa.preparaFormulario&sOP=Alterar" lang="1">Alterar Contrato selecionado</option>
   						<option value="?action=MnyContratoPessoa.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Contrato selecionado</option>
   						<option value="?action=MnyContratoPessoa.processaFormulario&sOP=Excluir" lang="2">Excluir Contrato(s) selecionado(s)</option>
   					</select></td>

   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyContratoPessoa)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyContratoPessoa')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Tipo</th>
					<th class='Titulo'>Numero</th>
					<th class='Titulo'>Descrição</th>
					<th class='Titulo'>Data</th>
					<th class='Titulo'>Validade</th>
					<th class='Titulo'>Valor</th>
<!--                    <th class='Titulo'>Anexo</th>
-->   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyContratoPessoa as $oMnyContratoPessoa){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyContratoPessoa')" type="checkbox" value="<?=$oMnyContratoPessoa->getContratoCodigo()?>" name="fIdMnyContratoPessoa[]"/></td>
  					<td><?= $oMnyContratoPessoa->getContratoCodigo()?></td>
					<td><?= $oMnyContratoPessoa->getMnyContratoTipo()->getDescricao()?></td>
					<td><?= $oMnyContratoPessoa->getNumero()?></td>
					<td><?= $oMnyContratoPessoa->getDescricao()?></td>
					<td><?= $oMnyContratoPessoa->getDataContratoFormatada()?></td>
					<td><?= $oMnyContratoPessoa->getDataValidadeFormatado()?></td>
					<td>R$<?= $oMnyContratoPessoa->getValorContratoFormatado()?></td>
<!--                    <td><a target='_blank' href='#<?//= ($oMnyContratoPessoa->getAnexo())? $oMnyContratoPessoa->getAnexo() : ""?>'>Visualizar Anexo</a></td>
-->  				</tr>
  				<?php }?>
  				</tbody>
  			<?php }//if(count($voMnyContratoPessoa)){?>
  			</table>
            </form>
			<?php } ?>
         	   <div id="msgCPF" title="Mensagem" style="display:none"><p>CPF INVÁLIDO!</p></div>
	  		   <div id="msgEMAIL" title="Mensagem" style="display:none"><p>EMAIL INVÁLIDO!</p></div>

 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

  		<script type='text/javascript' src='js/cep_2.js'></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#Admissao").mask("99/99/9999");
				$("#Demissao").mask("99/99/9999");
			 });

			 jQuery(function($){
  			   $("#dataAdmissao").mask("99/99/9999");
  			   $("#cep").mask("99.999-999");
  			   $("#fone").mask("(99) 9999-9999");
			   $("#dataDemissao").mask("99/99/9999");
  			   $("#cpf").mask("999.999.999-99");

			   $("#PescSal").maskMoney({symbol:'R$ ',
				showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

  			});

			function valida_CPF(){
				valor = document.formMnyPessoaFisica.fPesfisCpf;
				oDiv = document.getElementById('botaoGravar');

				if (Verifica_campo_CPF(valor) === false){
					$(function() {
						$( "#msgCPF" ).dialog();
						oDiv.innerHTML = "<font color='red'><strong>Corrija o campo CPF</strong></font>";
						document.getElementById("cpf").style.backgroundColor = '#F89C8F';
						return false;
					});
				}else{
					document.getElementById("cpf").style.backgroundColor = '';
					oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";

				}
			}

			function valida_email(){
				valor = document.formMnyPessoaFisica.fPesgEmail ;
				oDiv = document.getElementById('botaoGravar');

				if (validacaoEmail(valor) === false){
					$(function() {
						$( "#msgEMAIL" ).dialog();
						//oDiv.innerHTML = "<font color='red'><strong>Email inválido</strong></font>";

						document.getElementById("email").style.backgroundColor = '#F89C8F';
						return false;
					});
				}else{
					document.getElementById("email").style.backgroundColor = '';
					oDiv.innerHTML = "<button type='submit' class='btn btn-primary'><?=$sOP?></button>";

				}
			}

/*	  	$(document).ready(function(){
		//Preenche os campos na a&#231;&#227;o "Blur" (mudar de campo)
    	    $("#cep").blur(function(){
				$("#rua").val("...")
				$("#bairro").val("...")
				$("#cidade").val("...")
				$("#estado").val("...")

				// seta a variavel requisitada no campo cep
				consulta = $("#cep").val()

				var str = consulta;
				str = str.replace(".", "");
				consulta = str.replace("-","")
				//Realiza a consulta
				/*Realiza a consulta atrav&#233;s do toolsweb passando o cep como parametro
				  e informando que vamos consultar no tipo javascript

				$.getScript("http://www.toolsweb.com.br/webservice/clienteWebService.php?cep="+consulta+"&formato=javascript", function(){

						//unescape - Decodifica uma string codificada com o m&#233;todo escape.
						rua=unescape(resultadoCEP.logradouro)
						bairro=unescape(resultadoCEP.bairro)
						cidade=unescape(resultadoCEP.cidade)
						estado=unescape(resultadoCEP.estado)

						// preenche os campos
						$("#rua").val(rua)
						$("#bairro").val(bairro)
						$("#cidade").val(cidade)
						$("#estado").val(estado)

					});
			});
		});
*/ 		</script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
