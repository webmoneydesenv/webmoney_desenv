<?php header("Content-Type: text/html; charset=UTF-8",true); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Excluir Item</h4>
</div>
<br>
<form method="post" class="form-horizontal" name="formMnyCartao" action="?action=MnyMovimentoItem.processaFormularioExcluirFAProcedure">
    <div class="form-group">
        <label class="col-sm-2 control-label" style="text-align:left" for="De">F.A:</label>
        <div class="col-sm-3">
            <input class="form-control" type='text'  placeholder='F.A' name='fIdMnyMovimentoItem' required  value=''  />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" style="text-align:left" for="De">Motivo:</label>
        <div class="col-sm-6">
            <textarea class="form-control"   placeholder='Motivo' name='fMotivo'   value='' required></textarea>
        </div>
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary" >Excluir</button>
    </div>
</form>

