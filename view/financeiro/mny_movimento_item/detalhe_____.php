﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyMovimentoItem.preparaLista">Gerenciar Items</a> &gt; <strong><?php echo $sOP?> Item</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Item</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimentoItem" action="?action=MnyMovimentoItem.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyMovimentoItem" disabled>


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">- Codigo:</label>
					<div class="col-sm-10">
					<select name='fMovCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyMovimento){
							   foreach($voMnyMovimento as $oMnyMovimento){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getMovCodigo() == $oMnyMovimento->getMovCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyMovimento->getMovCodigo()?>'><?= $oMnyMovimento->getMovCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">- Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Item' name='fMovItem' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataVencto">- Data Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Data Vencimento' name='fMovDataVencto' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVencto() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataPrev">- Data Previsao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Data Previsao' name='fMovDataPrev' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrev() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">- Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Valor' name='fMovValor' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValor() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovJuros">- Juros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Juros' name='fMovJuros' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovJuros() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">- Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Pagar' name='fMovValorPagar' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPagar() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">- Forma de Pagamento:</label>
					<div class="col-sm-10">
					<select name='fFpgCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getFpgCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">- Tipo de Documento:</label>
					<div class="col-sm-10">
					<select name='fTipDocCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus">- Status:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Status' name='fMovStatus' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovRetencao">- Retencao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Retencao' name='fMovRetencao' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovRetencao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">- Data Inclusao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Data Inclusao' name='fMovDataInclusao' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataInclusao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParcela">- Parcela:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Parcela' name='fMovParcela' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovParcela() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutFinanceiro">- Quem Autorizou:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Quem Autorizou' name='fMovAutFinanceiro' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutFinanceiro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutGerencia">- Quem Autorizou:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Quem Autorizou' name='fMovAutGerencia' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutGerencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus1">___mov_status_1:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_status_1' name='fMovStatus1' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus1() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus2">___mov_status_2:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_status_2' name='fMovStatus2' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus2() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus3">___mov_status_3:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_status_3' name='fMovStatus3' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus3() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus4">___mov_status_4:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_status_4' name='fMovStatus4' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus4() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus5">___mov_status_5:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_status_5' name='fMovStatus5' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus5() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPago">- Valor Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='- Valor Pago' name='fMovValorPago' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPago() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutAuditoria">___mov_aut_auditoria:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_aut_auditoria' name='fMovAutAuditoria' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutAuditoria() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDevolucao">___mov_devolucao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='___mov_devolucao' name='fMovDevolucao' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDevolucao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='PIS' name='fMovPis' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovPis() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">COFINS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='COFINS' name='fMovConfins' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovConfins() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCsll">CSLL:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='CSLL' name='fMovCsll' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovCsll() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ISS' name='fMovIss' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIss() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IR' name='fMovIr' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIr() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIrrf">IRRF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='IRRF' name='fMovIrrf' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIrrf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='ICMS Aliq' name='fMovInss' value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovInss() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyMovimentoItem.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
