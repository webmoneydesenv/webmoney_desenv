<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];
 $voMnyMovimento = $_REQUEST['voMnyMovimento'];

$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];

$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Item - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyMovimentoItem.preparaLista">Gerenciar Items</a> &gt; <strong><?php echo $sOP?> Item</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Item</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimentoItem" action="?action=MnyMovimentoItem.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fMovCodigo" value="<?=(is_object($oMnyMovimentoItem)) ? $oMnyMovimentoItem->getMovCodigo() : ""?>" />
			<input type="hidden" name="fMovItem" value="<?=(is_object($oMnyMovimentoItem)) ? $oMnyMovimentoItem->getMovItem() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Item">


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">- Codigo:</label>
					<div class="col-sm-10">
					<select name='fMovCodigo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyMovimento){
							   foreach($voMnyMovimento as $oMnyMovimento){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getMovCodigo() == $oMnyMovimento->getMovCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyMovimento->getMovCodigo()?>'><?= $oMnyMovimento->getMovCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovItem">- Item:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovItem' placeholder='- Item' name='fMovItem'  required  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovItem() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataVencto">- Data Vencimento:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataVencto' placeholder='- Data Vencimento' name='fMovDataVencto'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataPrev">- Data Previsao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataPrev' placeholder='- Data Previsao' name='fMovDataPrev'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrevFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValor">- Valor:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValor' placeholder='- Valor' name='fMovValor'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovJuros">- Juros:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovJuros' placeholder='- Juros' name='fMovJuros'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovJurosFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPagar">- Pagar:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPagar' placeholder='- Pagar' name='fMovValorPagar'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPagarFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">- Forma de Pagamento:</label>
					<div class="col-sm-10">
					<select name='fFpgCodigo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getFpgCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">- Tipo de Documento:</label>
					<div class="col-sm-10">
					<select name='fTipDocCodigo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyPlanoContas->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getPlanoContasCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus">- Status:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus' placeholder='- Status' name='fMovStatus'  required  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovRetencao">- Retencao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovRetencao' placeholder='- Retencao' name='fMovRetencao'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovRetencaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDataInclusao">- Data Inclusao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDataInclusao' placeholder='- Data Inclusao' name='fMovDataInclusao'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataInclusaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovParcela">- Parcela:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovParcela' placeholder='- Parcela' name='fMovParcela'  required  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovParcela() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutFinanceiro">- Quem Autorizou:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovAutFinanceiro' placeholder='- Quem Autorizou' name='fMovAutFinanceiro'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutFinanceiro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutGerencia">- Quem Autorizou:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovAutGerencia' placeholder='- Quem Autorizou' name='fMovAutGerencia'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutGerencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus1">___mov_status_1:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus1' placeholder='___mov_status_1' name='fMovStatus1'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus1() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus2">___mov_status_2:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus2' placeholder='___mov_status_2' name='fMovStatus2'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus2() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus3">___mov_status_3:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus3' placeholder='___mov_status_3' name='fMovStatus3'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus3() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus4">___mov_status_4:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus4' placeholder='___mov_status_4' name='fMovStatus4'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus4() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovStatus5">___mov_status_5:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovStatus5' placeholder='___mov_status_5' name='fMovStatus5'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovStatus5() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovValorPago">- Valor Pago:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovValorPago' placeholder='- Valor Pago' name='fMovValorPago'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPagoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovAutAuditoria">___mov_aut_auditoria:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovAutAuditoria' placeholder='___mov_aut_auditoria' name='fMovAutAuditoria'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovAutAuditoria() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovDevolucao">___mov_devolucao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovDevolucao' placeholder='___mov_devolucao' name='fMovDevolucao'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDevolucaoFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovPis">PIS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovPis' placeholder='PIS' name='fMovPis'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovPisFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovConfins">COFINS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovConfins' placeholder='COFINS' name='fMovConfins'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovConfinsFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovCsll">CSLL:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovCsll' placeholder='CSLL' name='fMovCsll'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovCsllFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIss">ISS:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIss' placeholder='ISS' name='fMovIss'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIssFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIr">IR:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIr' placeholder='IR' name='fMovIr'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIrFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovIrrf">IRRF:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovIrrf' placeholder='IRRF' name='fMovIrrf'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovIrrfFormatado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MovInss">ICMS Aliq:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='MovInss' placeholder='ICMS Aliq' name='fMovInss'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovInssFormatado() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#MovDataVencto").mask("99/99/9999");
			$("#MovDataPrev").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovValorPago").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDevolucao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovPis").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovConfins").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovCsll").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIr").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovIrrf").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovInss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
