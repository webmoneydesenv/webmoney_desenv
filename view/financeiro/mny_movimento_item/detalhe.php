<?php
$oFachada = new FachadaFinanceiroBD();
$sOP = $_REQUEST['sOP'];
$oVMovimento  = $_REQUEST['oVMovimento'] ;
$voTramitacao = $_REQUEST['voTramitacao'];
$voTramitacaoStatus = $_REQUEST['voTramitacaoStatus'];
$voSysTipoProtocolo = $_REQUEST['voSysTipoProtocolo'];

$voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
if($_REQUEST['oMovContrato'])
    $oContratoPessoa = $_REQUEST['oMovContrato'];

$sSTatusWizardBreadcrumb =  $_REQUEST['sSTatusWizardBreadcrumb'];

$voSysProtocoloEncaminhamento = $_REQUEST['voSysProtocoloEncaminhamento'] ;

$voArquivoContrato = $_REQUEST['voArquivoContrato'];

$oMnySolicitacaoAporte = $_REQUEST['oMnySolicitacaoAporte'];

$voContratoAditivo = $_REQUEST['voContratoAditivo'];

$oMnyMovimentoDEVOLUCAO = $_REQUEST['oMnyMovimentoDEVOLUCAO'];

$voMovimentoItemAgrupado = $_REQUEST['voMovimentoItemAgrupado'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a Pagar - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

<style>
/* CSS used here will be applied after bootstrap.css */
.nav-wizard > li {
  float: left;
}
.nav-wizard > li > a {
  position: relative;
  background-color: #eeeeee;
}
.nav-wizard > li > a .badge {
  margin-left: 3px;
  color: #eeeeee;
  background-color: #073c8d;
}
.nav-wizard > li > a .badge.badge-step {
  color: #073c8d;
  border: 1px solid #073c8d;
  border-radius: 50%;
  padding: 3px 5px;
  background-color: transparent;
}
.nav-wizard > li.active > a .badge.badge-step {
  color: #FFFFFF;
  border: 1px solid #FFFFFF;
  border-radius: 50%;
  padding: 3px 5px;
  background-color: transparent;
}
.nav-wizard > li:not(:first-child) > a {
  padding-left: 34px;
}
.nav-wizard > li:not(:first-child) > a:before {
  width: 0px;
  height: 0px;
  border-top: 20px inset transparent;
  border-bottom: 20px inset transparent;
  border-left: 20px solid #ffffff;
  position: absolute;
  content: "";
  top: 0;
  left: 0;
}
.nav-wizard > li:not(:last-child) > a {
  margin-right: 4px;
}
.nav-wizard > li:not(:last-child) > a:after {
  width: 0px;
  height: 0px;
  border-top: 20px inset transparent;
  border-bottom: 20px inset transparent;
  border-left: 20px solid #eeeeee;
  position: absolute;
  content: "";
  top: 0;
  right: -20px;
  z-index: 2;
}
/*.nav-wizard > li:first-child > a {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}*/
/*.nav-wizard > li:last-child > a {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}*/
.nav-wizard > li.done:hover > a,
.nav-wizard > li:hover > a {
  background-color: #d5d5d5;
}
.nav-wizard > li.done:hover > a:before,
.nav-wizard > li:hover > a:before {
  border-right-color: #d5d5d5;
}
.nav-wizard > li.done:hover > a:after,
.nav-wizard > li:hover > a:after {
  border-left-color: #d5d5d5;
}
.nav-wizard > li.done > a {
  background-color: #e2e2e2;
}
.nav-wizard > li.done > a:before {
  border-right-color: #e2e2e2;
}
.nav-wizard > li.done > a:after {
  border-left-color: #e2e2e2;
}
.nav-wizard > li.active > a,
.nav-wizard > li.active > a:hover,
.nav-wizard > li.active > a:focus {
  color: #ffffff;
  background-color: #073c8d;
}
.nav-wizard > li.active > a:after {
  border-left-color: #073c8d;
}
.nav-wizard > li.active > a .badge {
  color: #073c8d;
  background-color: #ffffff;
}
.nav-wizard > li.disabled > a {
  color: #777777;
}
.nav-wizard > li.disabled > a:hover,
.nav-wizard > li.disabled > a:focus {
  color: #777777;
  text-decoration: none;
  background-color: #eeeeee;
  cursor: default;
}
.nav-wizard > li.disabled > a:before {
  border-right-color: #eeeeee;
}
.nav-wizard > li.disabled > a:after {
  border-left-color: #eeeeee;
}
.nav-wizard.nav-justified > li {
  float: none;
}
.nav-wizard.nav-justified > li > a {
  padding: 10px 15px;
}
@media (max-width: 768px) {
  .nav-wizard.nav-justified > li > a {
    /*border-radius: 4px;*/
    margin-right: 0;
  }
  .nav-wizard.nav-justified > li > a:before,
  .nav-wizard.nav-justified > li > a:after {
    border: none !important;
  }
}
  .aporte a {color: red;}
  .ui-dialog, .ui-widget, .ui-widget-content, .ui-corner-all, .ui-front, .ui-draggable, .ui-resizable{z-index: 1055 !important;}
</style>


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyMovimento.preparaLista">Gerenciar Movimentos</a> &gt; <strong><?php echo $sOP?> Movimento</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Contas a <?= $_REQUEST['sTipoLancamento'] ?></h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <div id="formulario" class="TabelaAdministracao">
        <? if($oVMovimento->getFinalizado() == 2){ ?>

            <div class="alert alert-danger" style="padding: 30px;">
                <div class="col-sm-5">&nbsp;</div>
                <div class="col-sm-4"><strong><span style="color:red;font-size:20px;">ESTORNO [<?="<a href='?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=".$_REQUEST['sTipoLancamento']."&nIdMnyMovimentoItem=".$oMnyMovimentoDEVOLUCAO->getMovCodigo()."||".$oMnyMovimentoDEVOLUCAO->getMovItem()."'>".$oMnyMovimentoDEVOLUCAO->getMovCodigo().".".$oMnyMovimentoDEVOLUCAO->getMovItem()."</a>"?>]</span></strong></div>
                <div class="col-sm-3">&nbsp;</div>
            </div>
        <? }else{ ?>
            <div class='form-group'>
               <!-- <div class="col-sm-12"><a  href='?action=MnyMovimentoItem.extornaItemMovimento&fa=<?= $_GET['nIdMnyMovimentoItem']?>' class="btn btn-warning" style='float:inline-end;'><icon class='glyphicon glyphicon-retweet'></icon>  Estorno</a>-->
            </div>
        <? }?>

          <div class="form-group">
            <div class="col-sm-4">
              <legend>Dados Gerais</legend>
              <div class="form-group">
                <div class="col-sm-12" style="text-align:left" ><strong>Tipo de Aceite: </strong><?=$oVMovimento->getAceiteDescricao() ?>  </div>
              </div>
              <br><br>
              <div class="form-group">
                <div class="col-sm-12" style="text-align:left"><strong>Competência: </strong><?=$oVMovimento->getCompetenciaFormatado() ?></div>
              </div>
              <br>
              <div class="form-group">
                <div class="col-sm-12" style="text-align:left"><strong>Pessoa: </strong><?=$oVMovimento->getNome()?></div>
              </div>
              <br>
              <div class="form-group">
                    <div class="col-sm-12" style="text-align:left" ><strong>Emissão:</strong><?= $oVMovimento->getMovDataEmissaoFormatado();?></div>
              </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Histórico: </strong><?= $oVMovimento->getMovObs()?></div>
                </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Documento: </strong><?= $oVMovimento->getMovDocumento()?></div>
                </div>
                <br>
                <div class="form-group">
	                <div class="col-sm-12" style="text-align:left"><strong>Valor Global:&nbsp;</strong><?= $oVMovimento->getMovValorGlobFormatado()?></div>
                </div>
                <br>
                </div>
                <div class="col-sm-4">
                <legend>Apropriações</legend>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Custo:&nbsp;</strong><?= $oVMovimento->getCustoCodigo() . " - " . $oVMovimento->getCustoDescricao() ?></div>
         	    </div>
                <br><br>
         		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>C.Negocio:&nbsp;</strong><?= $oVMovimento->getNegocioCodigo() . " - " . $oVMovimento->getNegocioDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Centro de Custo:&nbsp;</strong><?= $oVMovimento->getCentroCodigo() . " - " . $oVMovimento->getCentroDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= $oVMovimento->getUnidadeCodigo() . " - " . $oVMovimento->getUnidadeDescricao() ?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Conta:&nbsp;</strong><?= $oVMovimento->getContaContabil()?></div>
         	    </div>
                <br>
          		<div class="form-group">
					<div class="col-sm-12" style="text-align:left"><strong>Setor:&nbsp;</strong><?= $oVMovimento->getSetorCodigo() . " - " . $oVMovimento->getSetorDescricao() ?></div>
         	    </div>
          		</div>

            	</div>
				<div class="col-sm-4">
                <legend>Dados do Item</legend>
                 <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>F. Aceite:&nbsp;</strong><strong style='color: red; font-size: 16px;'><?= $oMnyMovimentoItem->getMovCodigo()?>.<?=$oMnyMovimentoItem->getMovItem()?></strong></div>
                  </div>
                <br>
                <br>
                 <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Vencimento:&nbsp;</strong><?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado() : ""?></div>
                  </div>
                <br>
                 <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Previsão:&nbsp;</strong><?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrevFormatado() : ""?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Forma de Pagamento:&nbsp;</strong><?=$oVMovimento->getFormaPagamentoDescricao()?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Tipo de Documento:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getTipoDocumentoDescricao() : ""?></div>
                  </div>
                <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getMovValorFormatado() : ""?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Juros:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getMovJurosFormatado() : ""?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Desconto:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getMovRetencaoFormatado() : ""?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Pagar:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getMovValorPagarFormatado() : ""?></div>
                  </div>
                  <br>
                  <div class="form-group">
                    <div class="col-sm-12" style="text-align:left"><strong>Pago:&nbsp;</strong><?= ($oVMovimento) ? $oVMovimento->getMovValorPagoFormatado() : ""?></div>
                  </div>
                  <br>

          		</div>

  </div>
         <? if($oContratoPessoa) { ?>

          <div class="form-group">
            <div class="col-sm-12">
               <legend>Dados Contrato</legend>
              <div class="form-group">
                 <div class="col-sm-4">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nº:</strong><strong style='color: red;'><?= ($oContratoPessoa) ? $oContratoPessoa->getNumero() : ""?></strong></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Nome: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getMnyPessoaGeral()->getNome() :  ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Status: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getSysStatus()->getDescStatus() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Descri&ccedil;&atilde;o: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getDescricao() : ""?></div></div>
				</div>
                <div class="col-sm-4">
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Vencimento: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getDataValidadeFormatado() : ""?></div></div>
					  <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Data do Contrato: </strong><?=  ($oContratoPessoa) ? $oContratoPessoa->getDataContratoFormatado() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Valor: </strong><?= ($oContratoPessoa) ? "R$".$oContratoPessoa->getValorContratoFormatado(): "" ?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Unidade: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getMnyPlanoContasUnidade()->getDescricao() : ""?></div></div>
                      <div class="form-group"><div class="col-sm-12" style="text-align:left" ><strong>Setor: </strong><?= ($oContratoPessoa) ? $oContratoPessoa->getMnyPlanoContasSetor()->getDescricao() : ""?></div></div>
				</div>
                 <div class="col-sm-4">
                     <? if($voArquivoContrato){?>
                         <? foreach($voArquivoContrato as $oArquivoContrato){?>
                                <div class="form-group">
                                    <div class="col-sm-12" style="text-align:left" >
                                        <div class="form-group">
                                            <div class="col-sm-12" style="text-align:left" >
                                                <a target='_blank' href='?action=SysArquivo.preparaArquivo&sOP=Visualizar&fCodArquivo=<?=$oArquivoContrato->getCodArquivo()?>'><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;<?= $oArquivoContrato->getCodTipo() ?></i></button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                         <? }?>
                     <? }?>
                  </div>
             </div>
              </div>
         </div>


        <? if($voContratoAditivo){?>
                <div class="form-group">
                    <div class="col-sm-12">
                       <legend>Aditivos de Contrato</legend>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                         <? $nI =1;?>
                         <? foreach($voContratoAditivo as $oContratoAditivo){ ?>
                         <div class="col-sm-12" style="background-color:#E0F0F9;padding-bottom:10px;">
                              <div class="col-sm-2" style="text-align:left" ><strong>Nº:</strong><strong style='color: red;'><?= $oContratoAditivo->getNumero() ?></strong></div>
                              <div class="col-sm-2" style="text-align:left" ><strong>Status: </strong><?= ($oContratoAditivo->getSysStatus()->getDescStatus()) ? $oContratoAditivo->getSysStatus()->getDescStatus() : 'Não Informado'  ?></div>
                              <div class="col-sm-2" style="text-align:left" ><strong>Motivo: </strong><?= $oContratoAditivo->getMotivoAditivo() ?></div>
                              <div class="col-sm-2" style="text-align:left" ><strong>Data: </strong><?= $oContratoAditivo->getDataAditivoFormatado() ?></div>
                              <div class="col-sm-1" style="text-align:left" ><strong>Validade: </strong><?=  $oContratoAditivo->getDataValidadeFormatado() ?></div>
                              <div class="col-sm-2" style="text-align:left" ><strong>Valor: </strong><?=  "R$".$oContratoAditivo->getValorAditivoFormatado() ?></div>
                              <div class="col-sm-1" style="text-align:left" ><a target='_blank' href='?action=MnyContratoAditivo.preparaArquivo&sOP=Visualizar&fAditivoCodigo=<?=$oContratoAditivo->getAditivoCodigo()?>'><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;Aditivo</i></button></a></div>

                        </div>
                            <? $nI ++;?>
                        <? } ?>
                     </div>

                      </div>


                 </div>
            <? } ?>


         <? } ?>
  <br>

         <? if($oMnySolicitacaoAporte){?>
                <div class="form-group">
                    <div class="col-sm-12">
                     <legend>Dados do Aporte</legend>
                            <div class="col-sm-12" style="background-color:#d2d2d2;">
                                  <div class="col-sm-2 aporte" style="text-align:left " ><strong>Nº:</strong><strong >
                                     <a  href='?action=MnySolicitacaoAporte.preparaFormulario&sOP=Detalhar&nIdMnySolicitacaoAporte=<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>' target='_blank' ><?= $oMnySolicitacaoAporte->getNumeroAporte()?></a>
                                      </strong></div>
                                  <div class="col-sm-2" style="text-align:left" ><strong>Data: </strong><?= $oMnySolicitacaoAporte->getDataSolicitacaoFormatado()?></div>
                                  <div class="col-sm-2" style="text-align:left" ><strong>Situação: </strong><?= ($oMnySolicitacaoAporte->getLiberado() == 0)? "<span style='color:#0881BD;font-weight:bold;size:18px;'>ABERTO</span>" : "<span style='color:red;font-weight:bold;size:18px;'>FECHADO</span>"?></div>
                                  <div class="col-sm-5" style="text-align:left" ><strong>Empresa: </strong><?= $oMnySolicitacaoAporte->getSolAlt()?></div>
                                <div class="col-sm-1" style="text-align:left" ><strong><?= ($oMnySolicitacaoAporte->getSolInc())? "<a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oMnySolicitacaoAporte->getSolInc()."'><button type=\"button\" class=\"btn btn-success  btn-sm\" ><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Pagamento</i></button></a>":"Ainda não foi inserido !"?></strong></div>

                            </div>
                    </div>
                </div>
         <? }?>
         <br>
         <? if($voMovimentoItemAgrupado){?>
                <div class="form-group">
                    <div class="col-sm-12">
                     <legend>Itens Agrupados</legend>
                            <div class="col-sm-12">
                            <? foreach($voMovimentoItemAgrupado as $oMovimentoItemLinha){ ?>
                                <div class='row' style="padding:4px">
                                    <div class="col-sm-12">
                                            <div class='col-sm-1'><a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=<?= $oMovimentoItemLinha->getMovCodigo()."||".$oMovimentoItemLinha->getMovItem()?>||1"><?= "FA ". $oMovimentoItemLinha->getMovCodigo().".". $oMovimentoItemLinha->getMovItem()?></a></div>
                                            <div class='col-sm-3'><?= $oMovimentoItemLinha->getMnyMovimento()->getMnyPlanoContasUnidade()->getDescricao()?></div>
                                            <div class='col-sm-3'><?= "R$".$oMovimentoItemLinha->getMnyMovimentoItem()->getMovValorParcelaFormatado()?></div>
                                    </div>
                                </div>
                                <hr style="margin-top:0px; margin-bottom:0px;padding-bottom:4px">

<?      }//foreach ?>
                            </div>
                    </div>
                </div>
         <? }?>
    <br> <br>
    <p>
		<!--Inicio Documentos-->
        <legend title="Documentos">Documentos</legend>
       <div class="form-group " >
            <div class="form-group" >
          <? $i = 0;
		     $j =0;

				if($voMnyContratoDocTipo){
					 foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){
/*
                        if(in_array($oMnyContratoDocTipo->getCodContratoDocTipo(),array(18,16,4))){

                              echo  $nCodContratoDocTipo = $oMnyContratoDocTipo->getCodContratoDocTipo();
                        }
*/
                ?>
                        <div class="col-sm-3" >
                            <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                           <? $voMnyContratoPessoaArquivoTipo = $oFachadaSys->recuperarTodosSysArquivoPorMovimentoPorTipoDocumento($oVMovimento->getMovCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());

						      ?>
                                <div class="col-sm-12">
                                <?
                                    if($voMnyContratoPessoaArquivoTipo){

                                        foreach($voMnyContratoPessoaArquivoTipo as $oMnyContratoPessoaArquivoTipo){
                                                 if(in_array($oMnyContratoDocTipo->getCodContratoDocTipo(),array(18,16,4))){
                                                            if($oMnyContratoPessoaArquivoTipo->getCodArquivo()){
                                                                $nCodArquivo = $oMnyContratoPessoaArquivoTipo->getCodArquivo();
                                                            }
                                                    }
                                                 if($oMnyContratoPessoa){
													  if(($oMnyContratoPessoaArquivoTipo->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
                                                            <input  type="file" class="filestyle" required data-buttonName="btn-primary"   data-iconName="glyphicon glyphicon-inbox" data-input="false"  name="arquivo[]" <?= ($oMnyContratoDocTipo->getObrigatorio() == 1) ? ' required="required"' : ""?>/><br>
                                                            <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                            <input type="hidden" name='fCodContratoArquivo[]' required value="<?= ($oMnyContratoPessoaArquivoTipo->getCodContratoArquivo()) ? $oMnyContratoPessoaArquivoTipo->getCodContratoArquivo() : ""?>">
												   <?

                                                     }
												 }

                                                  if($oMnyContratoPessoaArquivoTipo->getCodArquivo() && $sOP=='Detalhar'){
                                                        echo"<div class=\"col-sm-1 \" ><a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oMnyContratoPessoaArquivoTipo->getCodArquivo()."'><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Abrir</i></button></a> </div>"; ?>
                                                         <input type="hidden" name='fCodContratoArquivo[]' required value="<?= $oMnyContratoPessoaArquivoTipo->getCodArquivo()?>">
                                                         <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
                                                <? }
                                        }?>
                                <? }else{ ?>
                                    <div class="form-group">
                                        <div class='col-sm-12'>
                                            <span style='border-radius:20px;background-color:#C9302C;width:10%;border-radius:10px;'><i class="glyphicon glyphicon-remove" style="font-size: 12px;color: white;" ></i></span>
                                            Arquivo não encontrado!
                                        </div>
                                    </div>
                                <? } ?>
                                </div>
                              <? $i++ ?>
                              <? $j++?>
                        </div>

		  		  <? }
				}?>
                  <br>
		  </div>

       </div>
       <!--fim documentos-->
       <div class="col-sm-12">&nbsp;</div>
       <div class="col-sm-12">


      <div class="form-group">
           <legend>Hist&oacute;rico de Tramita&ccedil;&otilde;es</legend>
      </div>
       <? if($voTramitacao){ ?>
                <div class="row" style="background:#CCC;margin-right:0px; margin-left:0px;padding:10px">
                    <div class="col-sm-1"><strong>Detalhar</strong></div>
                    <div class="col-sm-1"><strong>DATA</strong></div>
                    <div class="col-sm-4"><strong>DE</strong></div>
                    <div class="col-sm-4"><strong>PARA</strong></div>
                    <div class="col-sm-2"><strong>QUEM</strong></div>
                </div>
      <!--id="tram" class="collapse"-->
            <? foreach($voTramitacao as $oTramitacao){
                $nUltimaPosicaoPara = $oTramitacao->getParaCodTipoProtocolo();
                $nUltimaPosicaoDe = $oTramitacao->getDeCodTipoProtocolo();
           ?>

                    <div class="row">
                        <?// if($oTramitacao->getDeCodTipoProtocolo() != $oTramitacao->getParaCodTipoProtocolo()){ ?>
                        <div class="form-group" style="padding:10px 0px">
                            <div class="col-sm-1" align="center"><a onclick="MostraEsconde2('Detalhe<?php echo $i?>');"><span class="glyphicon glyphicon-info-sign large" aria-hidden="true" style="font-size: 20px;"></span></a></div>
                            <div class="col-sm-1"><?php echo $oTramitacao->getDataFormatado()?></div>
                            <div class="col-sm-4"><?php echo $oTramitacao->getDeSysTipoProtocolo()->getDescricao() ?></div>
                            <div class="col-sm-4"><?php echo $oTramitacao->getParaSysTipoProtocolo()->getDescricao() ?></div>
                            <div class="col-sm-2"><?php echo $oTramitacao->getResponsavel() ?></div>
                        </div>

                    </div>
                    <div class="row" style="display:none;margin-bottom:10px;margin-top:10px; " id="Detalhe<?php echo $i;?>">
                    <div class="col-sm-1"><strong>Descrição:</strong></div>
                    <div class="col-sm-11"><textarea readonly class="form-control"><?php echo $oTramitacao->getDescricao()?></textarea></div>

                    </div>

            <hr style="margin-top:0px; margin-bottom:0px">
            <? $i++;
                } ?>
                <? if($oVMovimento->getFinalizado() == 1){ ?>
                    <? $oQuemFinalizaou =  end($voTramitacao);?>
                    <div class="col-sm-1" align="center">&nbsp;</div>
                    <div class="col-sm-9" align="center" style='background-color:#C9302C;color: white;font-weight: bold;'>Encerrada em: <?php echo $oQuemFinalizaou->getDataFormatado()?> por <?php echo $oQuemFinalizaou->getResponsavel() ?></div>
                <? } ?>
      <? }  else {?>
      	<div class="col-sm-12">N&atilde; h&aacute; tramita&ccedil;&otilde;es para este documento</div>
      <? } ?>
        <br><br>
        <!--wizard/breadcrumb-->
        <div class="col-sm-12">
            <ul class="nav nav-wizard">
                <? $i=1;
               if($voTramitacaoStatus){

                foreach($voTramitacaoStatus as $oTramitacaoStatus){?>
                    <li <?= ($sSTatusWizardBreadcrumb == $oTramitacaoStatus->getDe())? 'class="active" ' : ''?>><a href="#" ><span class="badge badge-step"><?=$oTramitacaoStatus->getOrdem()?></span> <?=$oTramitacaoStatus->getAtivo()?></a></li>
            <?  }
               }?>
                <li ><a href="#" <?= ($oVMovimento->getFinalizado() == 1)? "style='background-color:#C9302C;color: white;font-weight: bold;'" : "" ?>>Finalizado</a></li>
            </ul>
        </div>
    <?php if($sSTatusWizardBreadcrumb == $_SESSION['Perfil']['CodGrupoUsuario'] && $oVMovimento->getFinalizado() != 1){ ?>
       <div class="col-sm-12">
         <fieldset title="Dados da Tramitação">

       <form method="post" enctype="multipart/form-data"  class="form-horizontal" name="formMnyMovimentoItemProtocolo" action="?action=MnyMovimentoItemProtocolo.processaFormulario">
         <input type="hidden" name="sTipoLancamento" value="<?=$_REQUEST['sTipoLancamento']?>">
         <input type="hidden" name="sOP" value="Cadastrar" />
         <input type="hidden" name="fCodProtocoloMovimentoItem" value="" />
         <input type="hidden" name="fMovCodigo" value="<?=(is_object($oMnyMovimentoItemProtocolo)) ? $oMnyMovimentoItemProtocolo->getMovCodigo() : $oMnyMovimentoItem->getMovCodigo()?>" />
         <input type="hidden" name="fMovItem" value="<?=(is_object($oMnyMovimentoItemProtocolo)) ? $oMnyMovimentoItemProtocolo->getMovItem() : $oMnyMovimentoItem->getMovItem()?>" />
         <input type='hidden' name='fData' required value='<?=date('d/m/Y H:i:s')?>'/>
         <input type='hidden' name='fAtivo' value='1'/>
         <input type='hidden' name='fFinalizado' value='0'/>
         <input type="hidden" name='fConciliado' value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getConciliado() : 0 ?>">
         <legend>Tramita&ccedil;&atilde;o</legend>

 		<!-- Tramitação-->
        <?

            if(!($_SESSION['Perfil']['CodGrupoUsuario'] == 18 && $oMnyMovimentoItem->getNf() == 1 && ($sSTatusWizardBreadcrumb == 18)) && $nUltimaPosicaoDe != 12 ) {

           ?>


        <div class="form-group" id='divFinalizado'>
					<label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">De:</label>
					<div class="col-sm-2">
                        <input class="form-control" type='text' readonly  value="<?= $_SESSION['Perfil']['GrupoUsuario']?>">
                       <input type='hidden' name='fDeCodTipoProtocolo' value="<?= $_SESSION['Perfil']['CodGrupoUsuario']?>">

				</div>
					<label class="col-sm-1 control-label" style="text-align:left" for="SysTipoProtocolo">Para:</label>
					<div class="col-sm-2" id="divPara">
                         <select name='fParaCodTipoProtocolo'  class="form-control chosen"  required >
                            <option value=''>Selecione</option>
                            <? $sSelected = "";
                               if($voSysProtocoloEncaminhamento){
                                   foreach($voSysProtocoloEncaminhamento as $oSysProtocoloEncaminhamento){
                                       if($oMnyMovimentoItemProtocolo){
                                           $sSelected = ($oMnyMovimentoItemProtocolo->getDeCodTipoProtocolo() == $oSysTipoProtocolo->getCodGrupoUsuario()) ? "selected" : "";
                                       }

                            ?>

                                <option  <?= $sSelected?> value='<?= $oSysProtocoloEncaminhamento->getPara()?>'><?= $oSysProtocoloEncaminhamento->getAtivo()?></option>
                            <?
                                   }
/*
                                   if(($oMnyMovimentoItem->getCaixinha()==1) && ($_SESSION['Perfil']['CodGrupoUsuario']==7)){
                                echo "<option  value='11'>Conciliação</option>";
                                   }*/
                               }
                            ?>
                        </select>
                    </div>
					<label class="col-sm-2 control-label" style="text-align:left" for="Responsavel">Responsavel:</label>
					<div class="col-sm-4">
					<input class="form-control" type='text' name='fResponsavel' required readonly value='<?= $_SESSION['oUsuarioImoney']->getLogin()?>'/>
				</div>
                </div>



				<br>
				<div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Descricao">Descrição:</label>
					<div class="col-sm-11">
                    <textarea class="form-control" rows="3" placeholder='Descrição' name='fDescricao'></textarea>
				</div>
				</div>
				<br>
           <? }else{ ?>
          <input class="form-control" type='hidden' name='fResponsavel' required readonly value='<?= $_SESSION['oUsuarioImoney']->getLogin()?>'/>
           <?   if($_SESSION['Perfil']['CodGrupoUsuario']!=12){
                    switch($_SESSION['Perfil']['CodGrupoUsuario']){
                        case 10: ?>
                            <input class="btn btn-warning"  type="button" name="fBotao" value="Corrigir" data-toggle="modal" data-target="#modalCorrecao">
                       <?  break;
                        case 11:
                            echo  '<a href="?action=MnyConciliacao.preparaFormularioReConciliacao&nIdMnyMovimentoItem='.$oVMovimento->getMovCodigo().'||'.$oVMovimento->getMovItem().'"><input class="btn btn-warning"  type="button" name="fBotao" value="Corrigir"></a>';
                            //echo  '<input class="btn btn-warning"  type="button" name="fBotao" value="Corrigir">';
                        break;
                        case 18:
                             //echo '<a href="?action=MnyMovimento.preparaFormulario&sOP=AlterarReconciliacao&"><input class="btn btn-warning"  type="button" name="fBotao" value="Corrigir">';
                             echo '<input class="btn btn-warning"  type="button" name="fBotao" value="Corrigir" data-toggle="modal" data-target="#modalCorrecao">';
                        break;
                    }


           ?>


        <?      }
             }
           ?>

        <!-- Fim Tramitação -->
           <?php $aPermitido = array(7,12);
                if(in_array($_SESSION['Perfil']['CodGrupoUsuario'],$aPermitido)) { ?>
           <div class="row">
                <label class="col-sm-1 control-label" style="text-align:left" >C:</label>
                <div class="col-sm-1">
                    <input type="checkbox" class="form-control" name='fContabil' value="<?= $oMnyMovimentoItem->getContabil() ?>" <?= ($oMnyMovimentoItem->getContabil()==1)? "checked" : "" ?> >
                </div>
                <label class="col-sm-1 control-label" style="text-align:left" >Sem NF:</label>
                <div class="col-sm-1">
                    <input type="checkbox" class="form-control" name='fNf' value="<?= $oMnyMovimentoItem->getNf() ?>" <?= ($oMnyMovimentoItem->getNf()==1)? "checked" : "" ?> >
                </div>
           </div>
           <?php }
                 if($_SESSION['Perfil']['CodGrupoUsuario']==18 && $oMnyMovimentoItem->getNf() ==1 && ($sSTatusWizardBreadcrumb==18)){ ?>
            <div class="form-group">
                <div class="col-sm-12">
                        <label class="col-sm-1 control-label" style="text-align:left" >Nota Fiscal:</label>
                    <div class="col-sm-2">
                        <input type="file" data-buttonText="Anexar" required class="filestyle" data-iconName="glyphicon glyphicon-file" data-buttonName="btn-primary" style='width:50%;' data-input="true"  name='arquivo' />
                        <input type="hidden" name='fCodTipo' required value="29">
                        <? if($oContratoPessoa){ ?>
                        <input type="hidden" name='fContratoCodigo' required value="<?=$oContratoPessoa->getContratoCodigo()?>">
                        <? } ?>

                    </div>
                    <span style='color:red; font-weight:bold; size:12px;'> * Arquivo obrigatório</span>
                </div>
           </div>
            <?     }

           ?>

                </fieldset>
       <div class="form-group">
            <div class="col-sm-offset-5 col-sm-3">
            <button type="submit" class="btn btn-primary" >Tramitar</button>
            <? if($_SESSION['Perfil']['CodGrupoUsuario'] ==12){?>
            <button type="button" class="btn btn-danger" onclick="finalizaMovimento(<?= $oMnyMovimentoItem->getMovCodigo()?>,<?=$oMnyMovimentoItem->getMovItem()?>)">Encerrar Processo</button>
            <? } ?>
            </div>
   		</div>
        </form>
           <div class="modal fade" id="modalCorrecao" role="dialog">
                <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                          <? switch($_SESSION['Perfil']['CodGrupoUsuario']){
                                  case 10:
                                    include_once('view/includes/include_correcao_pagamento.php');
                                  break;
                                  case 18:
                                    include_once('view/includes/include_correcao_financeiro_central.php');
                                  break;
                             }?>
                      </div>
                </div>
            </div>
           <div id="divEnviar"></div>
 		</div>
           <?php } ?>
		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
           <script type="text/javascript">
            function finalizaMovimento(movimento,item){
                window.location.href = '?action=MnyMovimentoItem.finalizaMovimento&nMovCodigo='+movimento+'&nMovItem='+item;
                return false;
            }

            function submeteCorrecao(){

                document.getElementById('formCorrecao').submit();
            }

           </script>



       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
