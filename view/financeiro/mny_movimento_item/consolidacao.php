<?php
	$voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
?>
<link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

   <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
   <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

       <form method="post" class="form-horizontal" enctype="multipart/form-data" name="formMnyContratoPessoa" action="?action=MnyMovimentoItem.processaFormulario" enctype="multipart/form-data">
        <input type="hidden" name="sOP" value="Anexar" />
		  <div class="form-group">
          <?php foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
				<div>
                 <!-- <input id="uploadFile" placeholder="..." disabled="disabled" />
                     <label class="control-label col-sm-3" style="text-align:left" for="Anexo"><?//= $oMnyMovimentoDocTipo->getDescricao()?>:</label>
                        <div class="fileUpload btn btn-primary" col-sm-9>
                            <span>Selecionar Arquivo</span>
                            <input type="file" name="arquivo[]" class="upload" data-filename-placement="inside" />
                       </div>
                    </div>-->
                        <label class="control-label col-sm-3" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:</label>
                        <div class="input-group col-sm-7">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">
                                   <i class=" glyphicon glyphicon-folder-open"></i>&nbsp; Selecionar Arquivo <input  name="arquivo[]" type="file" multiple>
                                </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                        </div>
                    </div>
                    <p>
                    <p>
		  <? } ?>
          <br>
          <br>



                <div class="col-sm-offset-5 col-sm-2">
                <button type="submit" class="btn btn-primary" >Anexar</button>
                </div>
          </div>


       </form>


 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="js/bootstrap-filestyle.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataContrato").mask("99/99/9999");
				$("#DataValidade").mask("99/99/9999");
				$("#ValorContrato").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });

			 function voltar() {
				window.history.back()
			 }

 		</script>
