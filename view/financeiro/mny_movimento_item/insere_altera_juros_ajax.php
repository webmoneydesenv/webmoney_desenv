<?php header("Content-Type: text/html; charset=UTF-8",true);
 $oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];
 $oMnyAporteItem = $_REQUEST['oMnyAporteItem'];

?>
<form method="post" name="formMnyMovimentoItem" action="?action=MnyMovimentoItem.processaFormulario">
    <input type="hidden" name="sOP" value="AlterarJuros">
    <input type="hidden" name="fMovCodigo" value="<?=$oMnyMovimentoItem->getMovCodigo()?>">
    <input type="hidden" name="fMovItem" value="<?=$oMnyMovimentoItem->getMovItem()?>">
    <input type="hidden" name="fCodSolicitacaoAporte" value="<?=$_REQUEST['nCodSolicitacaoAporte']?>">
    <input type="hidden" name="nIdMnySolicitacaoAporte" value="<?=$oMnyAporteItem->getCodSolicitacaoAporte()?>">
    <div class="form-group">
        <h3 class="col-sm-12 control-label" style="text-align:left">F.A: <?=$oMnyMovimentoItem->getMovCodigo()?>.<?=$oMnyMovimentoItem->getMovItem()?></h3>
    </div>
    <div class="form-group">
        <h3 class="col-sm-12 control-label" style="text-align:left;color:red">Valor: <?=$oMnyMovimentoItem->getMovValorParcelaFormatado()?></h3>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" style="text-align:left">Juros:</label>
        <div class="col-sm-3">
            <input class="form-control" type='text'  placeholder='Juros' name='fMovJuros' id="juros"   value='<?=($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovJurosFormatado() : "" ?>'  />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" style="text-align:left" for="De">Desconto:</label>
        <div class="col-sm-3">
            <input class="form-control" type='text'  placeholder='Desconto' id="retencao" name='fMovRetencao' value='<?=($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovRetencaoFormatado() : "" ?>'  />
        </div>
    </div>
  <div class="form-group">
    <div class="col-sm-offset-5 col-sm-2">
      <button type="submit" class="btn btn-primary" >Alterar</button>
    </div>
  </div>
</form>
