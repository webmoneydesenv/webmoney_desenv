<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];
 $oMnyConciliacao = $_REQUEST['oMnyConciliacao'];
 $voSysCompetencia = $_REQUEST['voSysCompetencia'];

 if(!($oMnyMovimentoItem)){
	$oMnyMovimento = $_REQUEST['oMnyMovimento'] ;
  }
 $voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
 $voMnyFormaPagamento = $_REQUEST['voMnyFormaPagamento'];
 $voMnyTipoAceite = $_REQUEST['voMnyTipoAceite'];

 if($sOP == 'Cadastrar'){
	$oItem = $_REQUEST['AddItem'] ;
	$readOnly =  "";
 }else{
     if(($oMnyConciliacao) && ($oMnyConciliacao->getConsolidado()==1)){
	   $readOnly = " readonly='readonly' ";
     }
 }

$nContratoTipo = $_REQUEST['fContratoTipo'];
$sTipoLancamento = $_REQUEST['sTipoLancamento'];
$nIdMnyMovimentoItem = $_REQUEST['nIdMnyMovimentoItem'];
$nIdMnyMovimentoItem = explode("|",$nIdMnyMovimentoItem);
$nIdMnyMovimento = $_REQUEST['nIdMnyMovimento'];
?>

<form method="post" class="form-horizontal" name="formMnyMovimentoItem" action="?action=MnyMovimentoItem.processaFormulario">
  <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
  <input type="hidden" name="fMovCodigo" value="<?=(is_object($oMnyMovimentoItem)) ? $oMnyMovimentoItem->getMovCodigo() : $oMnyMovimento->getMovCodigo()?>" />
  <input type="hidden" name="fContabil" value="<?=(is_object($oMnyMovimentoItem))? $oMnyMovimentoItem->getContabil() : "1"?>" />
  <input type="hidden" name="SubtraiSaldo" value="1" />
  <input type="hidden" name="fContratoCodigo" value="<?= $_REQUEST['nCodContrato']?>" />
 <? if($sOP == 'Cadastrar'){?>
		<input type="hidden" name="fTipAceCodigo" value="<?= ($oItem)? $oItem[0]->getTipAceCodigo() : "" ?>">
         <input type="hidden" name="fCompetencia" value="<?= ($oItem) ? $oItem[0]->getCompetenciaFormatado() : ""?>">
 <? }?>
<? if($sOP =='Alterar'){ ?>
  <input type="hidden" name="fMovItem" value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovItem() : ""?>" />
  <input type="hidden" name="fTipAceCodigo" value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getTipAceCodigo() : ""?>">


  <? } ?>
  <input type='hidden' id='MovDataInclusao' name='fMovDataInclusao'  value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataInclusaoFormatado() : ""?>'/>
 <!--precisa para o get-->
  <input type='hidden' name='fContratoTipo'  value='<?= $nContratoTipo ?>'/>
  <input type='hidden' name='sTipoLancamento'  value='<?= $sTipoLancamento ?>'/>
  <input type='hidden' name='nIdMnyMovimentoItem'  value='<?= $nIdMnyMovimentoItem[0]?>'/>
  <input type='hidden' name='nIdMnyMovimento'  value='<?= $nIdMnyMovimento?>'/>

  <div id="formulario" class="TabelaAdministracao">
  <fieldset title="Dados do Item">
  <div class="form-group">
<? if($sOP =='Alterar'){ ?>
    <label class="col-sm-3 control-label" style="text-align:left" for="MnyMovimento">Código / Item:</label>
    <div class="col-sm-3">
      <h4>
        <?= $oMnyMovimentoItem->getMovCodigo()?>
        /
        <?=$oMnyMovimentoItem->getMovItem()?>
      </h4>
    </div>
	<div class="col-sm-6">
		<label  class="col-sm-6 control-label" style="text-align:left" for="MnyMovimento">Compet&ecirc;ncia:</label>
	 	<div class="col-sm-6">
    		<input type="text" class="form-control"  id="fCompetencia" name="fCompetencia" required value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getCompetenciaFormatado() : ""?>">
		</div>
	</div>
<? }else {?>
    <label class="col-sm-4 control-label" style="text-align:left" for="MnyMovimento">Código / Item:</label>
    <div class="col-sm-4">
    <h4><?= ($oMnyMovimento) ? $oMnyMovimento->getMovCodigo() : ""?> / <input type='text' placeholder='Parcela' name='fMovItem'  required  value='' maxlength="2" size="4"  />
    </h4></div>
	<div class="col-sm-4">
	<label class="control-label" style="text-align:left" for="MnyMovimento">Compet&ecirc;ncia:</label>
	<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getCompetenciaFormatado() : ""?>
	</div>

<? } ?>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovDataVencto">Vencimento:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovDataVencto' placeholder='Data Vencimento'   name='fMovDataVencto'  value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado() : ""?>' />
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovDataPrev">Previsão:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovDataPrev' placeholder='Data Previsão' name='fMovDataPrev'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrevFormatado() : ""?>' />
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-4 control-label" style="text-align:left" for="MnyPlanoContas">Forma de Pagamento:</label>
    <div class="col-sm-8">
      <select name='fFpgCodigo'  class="form-control chosen"  required >
        <option value=''>Selecione</option>
        <? $sSelected = "";
						   if($voMnyFormaPagamento){
							   foreach($voMnyFormaPagamento as $oMnyFormaPagamento){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getFpgCodigo() == $oMnyFormaPagamento->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
        <option  <?= $sSelected?> value='<?= $oMnyFormaPagamento->getPlanoContasCodigo()?>'>
        <?= $oMnyFormaPagamento->getDescricao()?>
        </option>
        <?
							   }
						   }
						?>
      </select>
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-4 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Documento:</label>
    <div class="col-sm-8">
      <select name='fTipDocCodigo'  class="form-control chosen"  required >
        <option value=''>Selecione</option>
        <? $sSelected = "";
						   if($voMnyTipoDoc){
							   foreach($voMnyTipoDoc as $oMnyTipoDoc){
								   if($oMnyMovimentoItem){
									   $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyTipoDoc->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
        <option  <?= $sSelected?> value='<?= $oMnyTipoDoc->getPlanoContasCodigo()?>'>
        <?= $oMnyTipoDoc->getDescricao()?>
        </option>
        <?
							   }
						   }
						?>
      </select>
    </div>
  </div>

  <br>
  <div class="form-group">
      <label class="col-sm-4 control-label" style="text-align:left" for="Tipo de Aceite">Tipo de Aceite:</label>
        <div class="col-sm-8">
          <select name='fTipAceCodigo'  class="form-control chosen"  required>
                    <option value=''>Selecione</option>
                <? $sSelected = "";
                   if($voMnyTipoAceite){
                       foreach($voMnyTipoAceite as $oMnyTipoAceite){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipAceCodigo() != $oMnyTipoAceite->getPlanoContasCodigo()) ? "" : "selected";
                            }						?>
        <option   <?= $sSelected?> value='<?= $oMnyTipoAceite->getPlanoContasCodigo()?>'><?= $oMnyTipoAceite->getCodigo()?> -
        <?= $oMnyTipoAceite->getDescricao()?>
        </option>
        <?
                       }
                   }
                ?>
      </select>
      </div>
      <br>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovValorParcela">Valor:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovValorParcela' placeholder='Valor' name='fMovValorParcela'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorParcelaFormatado() : ""?>' <?= $readOnly?>/>
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovJuros"> Juros:</label>
  <div class="col-sm-4">
    <input class="form-control" type='text' id='MovJuros' placeholder=' Juros' name='fMovJuros'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovJurosFormatado() : ""?>' <?= $readOnly?>/>
  </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovRetencao">Desconto:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovRetencao' placeholder='Desconto' name='fMovRetencao'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovRetencaoFormatado() : ""?>'  <?= $readOnly?>/>
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovValorDocTed">Doc/Ted</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovValorPagar' placeholder='Pagar' name='fMovValorTarifaBanco'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorTarifaBancoFormatado() : ""?>' <?= $readOnly?>/>
    </div>
  </div>

  <br>
  </div>
  <br>
  </fieldset>
  <div class="form-group">
    <div class="col-sm-offset-5 col-sm-2">
      <button type="submit" class="btn btn-primary" >
      <?=$sOP?>
      </button>
    </div>
  </div>
</form>
