<?php
 $voMnyMovimentoItem = $_REQUEST['voMnyMovimentoItem'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Items</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"> <strong><a href="?action=MnyMovimentoItem.preparaLista">Gerenciar Items</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Items</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnyMovimentoItem" id="formMnyMovimentoItem" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnyMovimentoItem" onChange="JavaScript: submeteForm('MnyMovimentoItem')">
   						<option value="" selected>A&ccedil;&otilde;es...</option>
   						<option value="?action=MnyMovimentoItem.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Item</option>
   						<option value="?action=MnyMovimentoItem.preparaFormulario&sOP=Alterar" lang="1">Alterar Item selecionado</option>
   						<option value="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Item selecionado</option>
   						<option value="?action=MnyMovimentoItem.processaFormulario&sOP=Excluir" lang="2">Excluir Item(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnyMovimentoItem)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnyMovimentoItem')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
   					<th class='Titulo'>- codigo</th>
					<th class='Titulo'>- item</th>
					<th class='Titulo'>- data vencimento</th>
					<th class='Titulo'>- data previsao</th>
					<th class='Titulo'>- valor</th>
					<th class='Titulo'>- juros</th>
					<th class='Titulo'>- pagar</th>
					<th class='Titulo'>- forma de pagamento</th>
					<th class='Titulo'>- tipo de documento</th>
					<th class='Titulo'>- status</th>
					<th class='Titulo'>- retencao</th>
					<th class='Titulo'>- data inclusao</th>
					<th class='Titulo'>- parcela</th>
					<th class='Titulo'>- quem autorizou</th>
					<th class='Titulo'>- quem autorizou</th>
					<th class='Titulo'>MovStatus1</th>
					<th class='Titulo'>MovStatus2</th>
					<th class='Titulo'>MovStatus3</th>
					<th class='Titulo'>MovStatus4</th>
					<th class='Titulo'>MovStatus5</th>
					<th class='Titulo'>- valor pago</th>
					<th class='Titulo'>MovAutAuditoria</th>
					<th class='Titulo'>MovDevolucao</th>
					<th class='Titulo'>Pis</th>
					<th class='Titulo'>Cofins</th>
					<th class='Titulo'>Csll</th>
					<th class='Titulo'>Iss</th>
					<th class='Titulo'>Ir</th>
					<th class='Titulo'>Irrf</th>
					<th class='Titulo'>Icms aliq</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnyMovimentoItem as $oMnyMovimentoItem){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnyMovimentoItem')" type="checkbox" value="<?=$oMnyMovimentoItem->getMovCodigo()?>||<?=$oMnyMovimentoItem->getMovItem()?>" name="fIdMnyMovimentoItem[]"/></td>
  					<td><?= $oMnyMovimentoItem->getMovCodigo()?></td>
					<td><?= $oMnyMovimentoItem->getMovItem()?></td>
					<td><?= $oMnyMovimentoItem->getMovDataVenctoFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovDataPrevFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovValorFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovJurosFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovValorPagarFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getFpgCodigo()?></td>
					<td><?= $oMnyMovimentoItem->getTipDocCodigo()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus()?></td>
					<td><?= $oMnyMovimentoItem->getMovRetencaoFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovDataInclusaoFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovParcela()?></td>
					<td><?= $oMnyMovimentoItem->getMovAutFinanceiro()?></td>
					<td><?= $oMnyMovimentoItem->getMovAutGerencia()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus1()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus2()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus3()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus4()?></td>
					<td><?= $oMnyMovimentoItem->getMovStatus5()?></td>
					<td><?= $oMnyMovimentoItem->getMovValorPagoFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovAutAuditoria()?></td>
					<td><?= $oMnyMovimentoItem->getMovDevolucaoFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovPisFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovConfinsFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovCsllFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovIssFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovIrFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovIrrfFormatado()?></td>
					<td><?= $oMnyMovimentoItem->getMovInssFormatado()?></td>

  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnyMovimentoItem)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyMovimentoItem');
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
