<?php
$sOP = $_REQUEST['sOP'];
$oVMovimento  = $_REQUEST['oVMovimento']



 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a Pagar - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyMovimento.preparaLista">Gerenciar Movimentos</a> &gt; <strong><?php echo $sOP?> Movimento</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Contas a <?=$_REQUEST['sTipoLancamento']?></h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimento" action="?action=MnyMovimento.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fMovCodigo" value="<?=(is_object($oMnyMovimento)) ? $oMnyMovimento->getMovCodigo() : ""?>" />
         <input type='hidden' name='fMovDataInclusao' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataInclusaoFormatado() : date('Y-m-d')?>'/>
         <input type='hidden' name='fMovInc' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovInc() : ""?>'/>
         <input type='hidden' name='fMovAlt'value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovAlt() : ""?>'/>
         <input type='hidden' name='fMovTipo' required  value='<?= ($oMnyMovimento) ? $oMnyMovimento->getMovTipo() : $_REQUEST['nTipo']; ?>'/>
         <input type='hidden' name='fEmpCodigo' required  value='<?= ($oMnyMovimento) ? $oMnyMovimento->getEmpCodigo() : /*$_SESSION['oEmpresa']->getEmpCodigo()*/""?>'/>
         <input type="hidden" name="fMovObs" value="'<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : "2"?>" />
         <input type='hidden' name='fAtivo' value='<?= ($oMnyMovimento) ? $oMnyMovimento->getAtivo() : "1"?>'/>
        <div id="formulario" class="TabelaAdministracao">
        <fieldset title="Dados Gerais" disabled>
          <div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais</legend>
           <form>
              <div class="form-group">
                <label class="col-sm-3 control-label" style="text-align:left" for="Tipo de Aceite">Tipo de Aceite:</label>
                <div class="col-sm-4">
                	<input class="form-control" type='text' id='' placeholder='Data de Emissão' name=''    value='<?=$oVMovimento->getAceiteDescricao() ?>'/>
                </div>
                <label class="col-sm-1 control-label" style="text-align:left" for="Competencia">Competência:</label>
                <div class="col-sm-4">
                	 <input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?=$oVMovimento->getCompetenciaFormatado() ?>'/>
                </div>
              </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Pessoa:</label>
                <div class="col-sm-10">
              		 <input class="form-control" type='text'  name='fPessoa'    value='<?=$oVMovimento->getNome() ?>'/>
 				</div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Data Emissão">Emissão:</label>
                <div class="col-sm-4">
                <input class="form-control" type='text' id='MovDataEmissao' placeholder='Data de Emissão' name='fMovDataEmissao'  required   value='<?= $oVMovimento->getMovDataEmissaoFormatado();?>'/>
				</div>
                <div id="divContrato">&nbsp;</div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Histórico">Histórico:</label>
                <div class="col-sm-10">
      			 <input class="form-control"  type='text' placeholder='Histórico' name='fMovObs' value='<?//= $oVMovimento->get()?>'/>
                </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                <div class="col-sm-10">
				<input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value=''/>
				</div>
                </div>
                <div class="form-group">
                <div id="divValorGlobal">
                <label class="col-sm-2 control-label" style="text-align:left" for="Parcelas">Valor Global:</label>
                <div class="col-sm-5">
                <input class="form-control" type='text' placeholder='Valor' id="fMovValorGlob" name='fMovValorGlob'   value='<?= $oVMovimento->getMovValorGlobFormatado()?>' />                </div>
                </div>
                </div>
                <br>
                </div>
                <div class="col-sm-6">
                <legend>Apropriações</legend>

				<? if ($voMnyCusto){ ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
            	<div class="col-sm-10">
				       <select name='fCusCodigo'  class="form-control chosen" required  >
                <option value=''>Selecione</option>
                <? $sSelected = "";
						   if($voMnyCusto){
							   foreach($voMnyCusto as $oMnyCusto){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
                <option  <?= $sSelected?> value='<?= $oMnyCusto->getPlanoContasCodigo()?>'>
                <?= $oMnyCusto->getDescricao()?>
                </option>
                <?
							   }
						   }
						?>
              </select>
		    	</div>
         	    </div>
          		<? } ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
           		<div class="col-sm-10">
                    <input class="form-control" type='text' id="fNegCodigo" name='fNegCodigo'   value='<?= $oVMovimento->getCentroDescricao()?>' />
                </div>
                </div>
                    <?		if($voMnyCentroCusto){ ?>
                         <div class="form-group">
<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroCusto">Centro de Custo:</label>
            <div class="col-sm-10">
             <input type='text' name="fCentroCusto" class='form-control' value="<?= $oVMovimento->getCentroCusto()?>">
            </div>
          </div>
					<? } ?>
                   <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                  <input type='text' class="form-control" name="fUnidade" value="<?=$oVMovimento->getUnidadeDescricao()?>">
				</div>
          		</div>
		        <div class="form-group">
        	    <label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
            	<div class="col-sm-10">
			 		<input type='text' class="form-control" value="<?= $oVMovimento->getPesgBcoConta()?>" >
                </div>
          		</div>
	           <div class="form-group">
     	       <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
        	   <div class="col-sm-10">
					<input type='text' class='form-control' name="fSetor" value='<?= $oVMovimento->getSetorDescricao()?>'>
				</div>
          		</div>
            	</div>
          		</div>
  </fieldset>
  </form>
   <fieldset disabled>
  <form>
  	 <legend>Dados do Item</legend>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimento">Código / Item:</label>
    <div class="col-sm-3">
	    <input type='text' name='fCodItem' class="form-control" value=" <?= $oMnyMovimentoItem->getMovCodigo()?>/<?=$oMnyMovimentoItem->getMovItem()?>">
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovDataVencto">Vencimento:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovDataVencto' placeholder='Data Vencimento' name='fMovDataVencto'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado() : ""?>'/>
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovDataPrev">Previsão:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovDataPrev' placeholder='Data Previsão' name='fMovDataPrev'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrevFormatado() : ""?>'/>
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Forma de Pagamento:</label>
    <div class="col-sm-8">
     	<input type='text' name="fFormaPagamento" class="form-control" value='<?= $oVMovimento->getFormaPagamentoDescricao()?>'>
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Documento:</label>
    <div class="col-sm-8">
      	<input type="text" class="form-control" value="<?=$oVMovimento->getTipoDocumentoDescricao()?>">
    </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovValor">Valor:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovValor' placeholder='Valor' name='fMovValor'  required   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorFormatado() : ""?>'/>
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovJuros"> Juros:</label>
  <div class="col-sm-4">
    <input class="form-control" type='text' id='MovJuros' placeholder=' Juros' name='fMovJuros'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovJurosFormatado() : ""?>'/>
  </div>
  </div>
  <br>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovRetencao">Desconto:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovRetencao' placeholder='Desconto' name='fMovRetencao'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovRetencaoFormatado() : ""?>'/>
    </div>
    <label class="col-sm-2 control-label" style="text-align:left" for="MovValorPagar">Pagar:</label>
    <div class="col-sm-4">
      <input class="form-control" type='text' id='MovValorPagar' placeholder='Pagar' name='fMovValorPagar'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPagarFormatado() : ""?>'/>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label" style="text-align:left" for="MovValorPago">Pago:</label>
    <div class="col-sm-10">
      <input class="form-control" type='text' id='MovValorPago' placeholder='Valor Pago' name='fMovValorPago'   value='<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovValorPagoFormatado() : ""?>'/>
    </div>
  </div>
  <br>
  </div>
  <br>
  </fieldset>
  </form>
  	  <? if($voTramitacao){ ?>
      <div class="form-group">
           <legend>Hist&oacute;rico de Tramita&ccedil;&otilde;es</legend>
      </div>
	  <br>
      		<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<th>De</th>
                	<th>Para</th>
                	<th>Data</th>
                	<th>Quem</th>
                	<th>Descri&ccedil;&atilde;o</th>
                </tr>
            <? foreach($voTramitacao as $oTramitacao){ ?>
            	<tr>
                	<td><?=$oTramitacao->getDeSysTipoProtocolo()->getDescricao()?></td>
                	<td><?=$oTramitacao->getParaSysTipoProtocolo()->getDescricao()?></td>
                	<td><?=$oTramitacao->getDataFormatado()?></td>
                	<td><?=$oTramitacao->getResponsavel()?></td>
                	<td><?=$oTramitacao->getDescricao()?></td>
            	</tr>
            <? } ?>
			</table>
      <? } else {?>
      	<div class="form-group">N&atilde; h&agrave; tramita&ccedil;&otilde;es para este documento</div>
      <? } ?>


 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovDataEmissao").mask("99/99/9999");
			$("#MovDataVencto").mask("99/99/9999");
			$("#fMovValorGlob").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovCsll").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIcmsAliq").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovValorGlobal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovPis").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovConfins").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIr").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovIrrf").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovInss").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovOutrosDesc").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#fMovOutros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});


			//$("#MovDataVencto").mask("99/99/9999");
			$("#MovDataPrev").mask("99/99/9999");
			$("#MovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorGlobal").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			$("#MovDataInclusao").mask("99/99/9999");
			$("#MovValorPago").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

				 });

			jQuery(document).ready(function(){
 				jQuery(".chosen").data("placeholder","Selecione").chosen();
 			});

		function verificaNulo(valor){
			if(valor == null || valor == undefined)
				return 0;
			else
				return valor;
		}

		function calculaValorLiquido(){
//			nValorTotal = document.formFormaPagamento.fValorTotal.value;
			nValor = verificaNulo(document.getElementById('fMovValor').value.replace(".","").replace(",","."));
			nValorPIS = verificaNulo(document.getElementById('fMovPis').value.replace(".","").replace(",","."));
			nValorCONFINS = verificaNulo(document.getElementById('fMovConfins').value.replace(".","").replace(",","."));
			nValorCSLL = verificaNulo(document.getElementById('fMovCsll').value.replace(".","").replace(",","."));
			nValorISS = verificaNulo(document.getElementById('fMovIss').value.replace(".","").replace(",","."));
			nValorIR = verificaNulo(document.getElementById('fMovIr').value.replace(".","").replace(",","."));
			nValorIRRF = verificaNulo(document.getElementById('fMovIrrf').value.replace(".","").replace(",","."));
			nValorINSS = verificaNulo(document.getElementById('fMovInss').value.replace(".","").replace(",","."));
			nValorOUTROS = verificaNulo(document.getElementById('fMovOutros').value.replace(".","").replace(",","."));
			nValorOUTROSDESC = verificaNulo(document.getElementById('fMovOutrosDesc').value.replace(".","").replace(",","."));


			nSoma = nValorPIS*1 + nValorCONFINS*1 + nValorCSLL*1 + nValorISS*1 + nValorIR*1 + nValorIRRF*1 + nValorINSS*1 + nValorOUTROS*1 + nValorOUTROSDESC*1 ;
			//alert (nSoma);
			nValorLiquido = (nValor - nSoma);

	  		nValorLiquidoFormatado = nValorLiquido.toFixed(2);
	   		nValorLiquidoFinal = nValorLiquidoFormatado.replace(".",",");

	  		document.getElementById('valorLiquido').innerHTML = "<strong style='color: red; font-size: 16px;'>" + nValorLiquidoFinal +"</strong><input type='hidden' name='fValorLiquido' id='fValorLiquido' value='"+ nValorLiquidoFormatado +"'>";
		}

		function preencheValorDocumento(nValor){
			document.getElementById('fMovValor').value = nValor;
		}

		function habilita() {
				document.getElementById("avancar").disabled = false;
			}


 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
