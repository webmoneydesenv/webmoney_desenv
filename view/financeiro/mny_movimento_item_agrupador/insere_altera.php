<?php
$sOP = $_REQUEST['sOP'];
//$oMnyMovimentoValorGlobal = $_REQUEST['oMnyMovimentoValorGlobal'];
//$oMnyMovimentoValorGlobalLiquido = $_REQUEST['oMnyMovimentoValorGlobalLiquido'];
//$sLink = "<a href='?action=MnyMovimento.preparaLista'>Gerenciar Movimentos</a>";

$sSomenteLeitura = $_REQUEST['sSomenteLeitura'];
$oMnyMovimento = $_REQUEST['oMnyMovimento'];
$oMnyMovimentoItem = $_REQUEST['oMnyMovimentoItem'];

$voMnyMovimentoItemAgrupado = $_REQUEST['voMnyMovimentoItemAgrupado'];

$voMnyCusto = $_REQUEST['voMnyCusto'];
$voMnyTipoDoc = $_REQUEST['voMnyTipoDoc'];
//$voMnyCentroCusto = $_REQUEST['voMnyCentroCusto'];
$voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
$voMnyUnidade = $_REQUEST['voMnyUnidade'];

$voMnyConta = $_REQUEST['voMnyConta'];
$voMnySetor = $_REQUEST['voMnySetor'];

$voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
$voMnyFormaPagamento = $_REQUEST['voMnyFormaPagamento'];

$sPagina = $_REQUEST['sPagina'];

$voMnyPessoa = $_REQUEST['voMnyPessoa'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Contas a Pagar </title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
     <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">
 <!-- InstanceBeginEditable name="head" -->
  <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a>
     <!-- InstanceBeginEditable name="Migalha" --> &gt; <strong>Movimento Agrupado <?php echo $sOP?> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->

       <h3 class="TituloPagina">Movimento Agrupado  - <?=$_REQUEST['sOP']?> <?=($oMnyMovimentoItemAgrupado) ? " -  &mdash; Nº ".$oMnyMovimentoItemAgrupado->getMovCodigo() . ".". $oMnyMovimentoItemAgrupado->getMovItem() : ""?></h3>

       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyMovimento" enctype="multipart/form-data" action="?action=MnyMovimentoItemAgrupador.processaFormulario" onSubmit="JavaScript: return validaForm(this);" >
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fMovCodigoAgrupador" value="<?=(is_object($oMnyMovimento)) ? $oMnyMovimento->getMovCodigo() : ""?>" />
         <input type="hidden" name="fMovItemAgrupador" value="<?=(is_object($oMnyMovimentoItem)) ? $oMnyMovimentoItem->getMovItem() : ""?>" />
         <input type="hidden" name="fMovItemAgrupador" value="<?=(is_object($oMnyMovimentoItem)) ? $oMnyMovimentoItem->getMovItem() : ""?>" />
         <input type='hidden' name='fMovDataInclusao' value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataInclusaoFormatado() : date('Y-m-d')?>"/>
         <input type='hidden' name='fMovDataEmissao' value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDataEmissaoFormatado() : date('Y-m-d')?>"/>
         <input type='hidden' name='fMovInc' value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovInc() : ""?>"/>
         <input type='hidden' name='fMovAlt'value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovAlt() : ""?>"/>
         <input type='hidden' name='fMovTipo' required  value="188"/>
         <input type='hidden' name='fEmpCodigo' required  value="<?= ($oMnyMovimento) ? $oMnyMovimento->getEmpCodigo() : $_SESSION['Perfil']['EmpCodigo']?>"/>
         <input type="hidden" name="fMovObs" value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : "2"?>" />
         <input type='hidden' name='fAtivo' value="<?= ($oMnyMovimento) ? $oMnyMovimento->getAtivo() : "1" ?>"/>
         <input type='hidden' name='fMovContrato' value="NULL"/>
           <input type="hidden" name='fTipAceCodigo'  value="169" >
           <input type='hidden'  name='fMovParcelas' required  value="1"/>
           <input type="hidden" name='fCenCodigo' value="156">




        <div id="formulario" class="TabelaAdministracao">

        <fieldset title="Dados Gerais">
          <div class="form-group">
            <div class="col-sm-6">
              <legend>Dados Gerais <?=($sOP=='Alterar') ? "<span style='color:red;font-size:20px;'>[".$oMnyMovimentoItem->getMovCodigo().".". $oMnyMovimentoItem->getMovItem()."]</span>": ""?></legend>
              <div class="form-group">

                    <label class="col-sm-2 control-label" style="text-align:left" for="Competencia">Comp:</label>
                    <div class="col-sm-2">
                    <input class="form-control" type='text' id='fCompetencia' placeholder='Competencia' name='fCompetencia'  required   value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getCompetenciaFormatado(): ""?>"/>
                    </div>


                <label class="col-sm-1 control-label" style="text-align:left" for="Contabil">Vencto:</label>
                <div class="col-sm-2"><input class="form-control"  id="MovDataVencto" type='text' placeholder='Vencimento' name='fMovDataVencto'  required   value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataVenctoFormatado(): ""?>" /></div>

                <label class="col-sm-1 control-label" style="text-align:left">Previs&atilde;o:</label>
                <div class="col-sm-2"><input class="form-control"   id="MovDataPrev" type='text' placeholder='Previsão' name='fMovDataPrev'  required value="<?= ($oMnyMovimentoItem) ? $oMnyMovimentoItem->getMovDataPrevFormatado(): ""?>" /></div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-2 control-label" style="text-align:left" for="Forma Pagamento">PGTO:</label>
                    <div class="col-sm-4">
                    <select name='fFpgCodigo'  class="form-control chosen"  required >
                        <option value="">Selecione</option>
                            <? $sSelected = "";
                               if($voMnyFormaPagamento){
                                   foreach($voMnyFormaPagamento as $oMnyFormaPagamento){
                                       if($oMnyMovimentoItem){
                                           $sSelected = ($oMnyMovimentoItem->getFpgCodigo() == $oMnyFormaPagamento->getPlanoContasCodigo()) ? "selected" : "";
                                        }						?>
                    <option  <?= $sSelected?> value="<?= $oMnyFormaPagamento->getPlanoContasCodigo()?>"><?= $oMnyFormaPagamento->getCodigo()?> -
                    <?= $oMnyFormaPagamento->getDescricao()?>
                    </option>
                    <?
                                   }
                               }?>
                  </select>

                   </div>
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPlanoContas">Tipo de Documento:</label>
					<div class="col-sm-4">
					<select name='fTipDocCodigo'  class="form-control chosen"  <?=($_REQUEST['sOP'] == 'Alterar')?  "" :  'required' ?>   >
			       <option value="">SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyTipoDoc){
                       foreach($voMnyTipoDoc as $oMnyTipoDoc){
                           if($oMnyMovimentoItem){
                               $sSelected = ($oMnyMovimentoItem->getTipDocCodigo() == $oMnyTipoDoc->getPlanoContasCodigo()) ? "selected" : "";
                           }
                ?>
                    <option  <?= $sSelected?> value="<?= $oMnyTipoDoc->getPlanoContasCodigo()?>"><?= $oMnyTipoDoc->getDescricao()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
               </div>
</div>
                 <div class="form-group">
                     <label class="col-sm-2 control-label" style="text-align:left" >Pessoa:</label>
                     <div class="col-sm-10">
                  <select name='fPesgCodigo' id='fPesgCodigo'  class="form-control chosen"  <?=($_REQUEST['sOP'] == 'Alterar')?  "readonly='readonly'" :  'required'  ?>  onchange="document.getElementById('campoPai').innerHTML=''" >
			       <option value="">SELECIONE</option>
                    <? $sSelected = "";
                   if($voMnyPessoa){
                       foreach($voMnyPessoa as $oMnyPessoa){
                           if($oMnyMovimento){
                               $sSelected = ($oMnyMovimento->getPesCodigo() == $oMnyPessoa->getPesgCodigo()) ? "selected" : "";
                           }
                ?>
                    <option  <?= $sSelected?> value="<?= $oMnyPessoa->getPesgCodigo()?>"><?= $oMnyPessoa->getNome()?>
                    </option>
                    <?
                       }
                   }
                ?>
                  </select>
                     </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Descri&ccedil;&atilde;o">Descri&ccedil;&atilde;o:</label>
                    <div class="col-sm-10">
                     <input class="form-control"  type='text' placeholder='Descrição' name='fMovObs' value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovObs() : ""?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                    <div class="col-sm-10">
                    <input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value="<?= ($oMnyMovimento) ? $oMnyMovimento->getMovDocumento() : ""?>"/>
                    </div>
                </div>

                </div>

                <div class="col-sm-6">
                <legend>Apropriações </legend>

				<? if ($voMnyCusto){ ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCusto">Custo:</label>
            	<div class="col-sm-10">
				       <select name='fCusCodigo'  class="form-control chosen" required    <?=$sSomenteLeitura?> >
                <option value="">Selecione</option>
                <? $sSelected = "";
						   if($voMnyCusto){
							   foreach($voMnyCusto as $oMnyCusto){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getCusCodigo() == $oMnyCusto->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                <option  <?= $sSelected?> value="<?= $oMnyCusto->getPlanoContasCodigo()?>"> <?= $oMnyCusto->getCodigo()?> - <?= $oMnyCusto->getDescricao()?></option>
                <?
							   }
						   }
						?>
              </select>
		    	</div>
         	    </div>
          		<? } ?>
          		<div class="form-group">
				<label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
           		<div class="col-sm-10">
				    <select name='fNegCodigo'  class="form-control chosen" required  <?=$sSomenteLeitura?>  >
                           <option value="">Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyCentroNegocio){
                                           foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                               if($oMnyMovimento){
                                                   $sSelected = ($oMnyMovimento->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                }elseif($oMnyContratoPessoa){
                                                    $sSelected = ($oMnyContratoPessoa->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                                }
                                    ?>
                            <option  <?= $sSelected?> value="<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>"><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?>
                            </option>
                            <?
                                           }
                                       }
                                    ?>
                          </select>
                    </div>
                    </div>
                   <div class="form-group">
                   <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                   <div class="col-sm-10">
                           <select name='fUniCodigo'  class="form-control chosen"  required  <?=$sSomenteLeitura?> >
                            <option value="">Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                           if($oMnyMovimento){
                                               $sSelected = ($oMnyMovimento->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                            }
                                ?>
                        <option  <?= $sSelected?> value="<?= $oMnyUnidade->getPlanoContasCodigo()?>"><?= $oMnyUnidade->getCodigo()?> - <?= $oMnyUnidade->getDescricao()?></option>
                        <?
                                       }
                                   }
                                ?>
                      </select>

				</div>
          		</div>

		        <div class="form-group" >
        	    <label class="col-sm-2 control-label" style="text-align:left" for="MnyConta">Conta:</label>
                <div class="col-sm-10"  id="divConta">
			 		<select  name='fConCodigo'  class="form-control chosen"  required <?=$sSomenteLeitura?> >
						    <option  value="">Selecione...</option>
                		<? $sSelected = "";
						   if($voMnyConta){
							   foreach($voMnyConta as $oMnyConta){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getConCodigo() == $oMnyConta->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                        <option  <?= $sSelected?> value="<?= $oMnyConta->getPlanoContasCodigo()?>"><?= $oMnyConta->getCodigo()?> -
                        <?= $oMnyConta->getDescricao()?>
                        </option>
                    	<?
                                   }
                               }
                            ?>

                  </select>
				</div>
          		</div>
	           <div class="form-group">
     	       <label class="col-sm-2 control-label" style="text-align:left" for="Setor">Setor:</label>
        	   <div class="col-sm-10" id="divConta">
					<select name='fSetCodigo'  class="form-control chosen"  required >
						    <option value="">Selecione</option>
                		<? $sSelected = "";
						   if($voMnySetor){
							   foreach($voMnySetor as $oMnySetor){
								   if($oMnyMovimento){
									   $sSelected = ($oMnyMovimento->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
                           			}elseif($oMnyContratoPessoa){
                               			$sSelected = ($oMnyContratoPessoa->getSetCodigo() == $oMnySetor->getPlanoContasCodigo()) ? "selected" : "";
									}
						?>
                            <option  <?= $sSelected?> value="<?= $oMnySetor->getPlanoContasCodigo()?>"><?= $oMnySetor->getCodigo()?> -
                            <?= $oMnySetor->getDescricao()?>
                            </option>
                            <?
							   }
						   }
						?>
              </select>
				</div>
          		</div>
            	</div>
                </div>


<fieldset >
    <div class="form-group">
	   <div class="col-sm-12">
           <legend> Movimentos Agrupados</legend>
            <div class="form-group">
     		 <label class="col-sm-2 control-label" style="text-align:left" for="MnyMovimentoItem">Movimento/Item:</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="fMovimentoItem" value="" id="fMovimentoITem"  ></div>
             <div class="col-sm-2"><button type='button' class='btn btn-success' onclick='addCampos()' value=''><i class='glyphicon glyphicon-plus'></i></button></div>
             </div>
           <br>
<?        if($voMnyMovimentoItemAgrupado){
        foreach($voMnyMovimentoItemAgrupado as $oMovimentoItemLinha){
           ?>
     <!-- <input type="hidden" name="fFA[]" value="<?= $oMovimentoItemLinha->getMovCodigo()?>||<?= $oMovimentoItemLinha->getMovItem()?>||<?//=$oMovimentoItemLinha->getValorLiquido()?>" >-->

		<div class='form-group'>
            <div class="col-sm-12">
                <div class='form-group' style="border-bottom:#DDD 1px solid;padding-bottom:2px;">
                    <div class='col-sm-1'><button  type='button' class='btn btn-danger removeButton' onclick="confirmacao('?action=MnyMovimentoItemAgrupador.processaFormulario&sOP=Excluir&fIdMnyMovimento=<?= $oMovimentoItemLinha->getMovCodigoAgrupador() .'||1||'. $oMovimentoItemLinha->getMovCodigo().'||'.$oMovimentoItemLinha->getMovItem()?>')" value='Apagar campo'><i class='glyphicon glyphicon-minus'></i></button></div>
                    <div class='col-sm-1'><?= $oMovimentoItemLinha->getMovCodigo()?>.<?= $oMovimentoItemLinha->getMovItem()?></div>
                    <div class='col-sm-3'><?= $oMovimentoItemLinha->getMnyMovimento()->getMnyPlanoContasUnidade()->getDescricao()?></div>
                    <div class='col-sm-3'><?= $oMovimentoItemLinha->getMnyMovimentoItem()->getMovValorParcelaFormatado()?></div>
                </div>
            </div>
        </div>

<?      }//foreach
        } //end if
     ?>
      <div class="col-sm-12" id="campoPai"></div>
     </div>
    </div>
    <? if($sOP== 'Alterar'){?>
    <div class="form-group">
        <div class="col-sm-5" align='left'><h3>Valor Fatura: <?=($oMnyMovimentoItem)? $oMnyMovimentoItem->getMovValorParcelaFormatado():""?></h3></div>
    </div>
    <? }  ?>
    </fieldset>
             <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2" id='botaoGravar'><button type="submit" id="submit_button" class="btn btn-primary" ><?=$sOP?></button></div>
   		</div>
       </form>


 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>

  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  	    <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
        <script src="js/modal-movimento-item.js"></script>

 		<script type="text/javascript" language="javascript">


		$('select[readonly=readonly] option:not(:selected)').prop('disabled', true);

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); });
			 jQuery(function($){

                $("#MovDataVencto").mask("99/99/9999");
                $("#MovDataPrev").mask("99/99/9999");
                $("#fCompetencia").mask("99/9999");
                $("#divMovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});


                $("#valorLiquidoExtenso").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#valorLiquidoParcelaExtenso").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

                $("#MovDataPrev").mask("99/99/9999");
                $("#divMovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#MovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#MovValorPagar").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#MovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#MovDataInclusao").mask("99/99/9999");
                $("#MovValorPago").maskMoney({symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
		  });

			jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen();});

		function verificaNulo(valor){
			if(valor == null || valor == undefined)
				return 0;
			else
				return valor;
		}

		function replace_all(string,encontrar,substituir){
		   while(string.indexOf(encontrar)>=0)
			string = string.replace(encontrar,substituir);
		   return string;
		}
/*
		function calculaValorLiquido(){
//			nValorTotal = document.formFormaPagamento.fValorTotal.value;
			nValor = replace_all(document.getElementById('divMovValor').value,'.','');
			nValor = replace_all(nValor,',','.');

			//nValor = verificaNulo(document.getElementById('fMovValor').value.replace(".","").replace(",","."));
			nValorPIS = verificaNulo(document.getElementById('fMovPis').value.replace(".","").replace(",","."));
			nValorCONFINS = verificaNulo(document.getElementById('fMovConfins').value.replace(".","").replace(",","."));
			nValorCSLL = verificaNulo(document.getElementById('fMovCsll').value.replace(".","").replace(",","."));
			nValorISS = verificaNulo(document.getElementById('fMovIss').value.replace(".","").replace(",","."));
			nValorIR = verificaNulo(document.getElementById('fMovIr').value.replace(".","").replace(",","."));
			nValorIRRF = verificaNulo(document.getElementById('fMovIrrf').value.replace(".","").replace(",","."));
			nValorINSS = verificaNulo(document.getElementById('fMovInss').value.replace(".","").replace(",","."));
			nValorOUTROS = verificaNulo(document.getElementById('fMovOutros').value.replace(".","").replace(",","."));
			nValorOUTROSDESC = verificaNulo(document.getElementById('fMovOutrosDesc').value.replace(".","").replace(",","."));
			nValorICMS = verificaNulo(document.getElementById('fMovIcmsAliq').value.replace(".","").replace(",","."));


			//nValorLiquidoItem = verificaNulo(document.getElementById('valorLiquidoExtenso').value.replace(".","").replace(",","."));
			nMovJuros = verificaNulo(document.getElementById('fMovJuros').value.replace(".","").replace(",","."));
			nMovRetencao = verificaNulo(document.getElementById('fMovRetencao').value.replace(".","").replace(",","."));
			nMovTarifaBanco = verificaNulo(document.getElementById('fMovTarifaBanco').value.replace(".","").replace(",","."));

			nSoma = nValorPIS*1 + nValorCONFINS*1 + nValorCSLL*1 + nValorISS*1 + nValorIR*1 + nValorIRRF*1 + nValorINSS*1 - nValorOUTROS*1 + nValorOUTROSDESC*1 ;//+ nValorICMS*1 ;

			nParcela = document.getElementById('MovParcelas').value;

			nValorGlobalLiquido = (nValor - nSoma) ;
			nValorGlobalLiquidoFormatado = nValorGlobalLiquido.toFixed(2) ;
			nValorGlobalLiquidoFinal = nValorGlobalLiquidoFormatado.replace(".",",") ;

			nValorLiquido = ((nValor - nSoma)/ nParcela) ;
	  		nValorLiquidoFormatado = nValorLiquido.toFixed(2);
			nValorLiquidoFinal = nValorLiquidoFormatado.replace(".",",") ;

			//verificar o valor que serra passado para a CTR
			//document.getElementById('valorLiquidoExtenso').innerHTML = "<input type='hidden' name='fMovValorLiquido' id='fMovValorLiquido' value='"+ nValorLiquidoFormatado +"'>";
	  		document.getElementById('valorLiquidoExtenso').value = nValorLiquidoFinal;
	  		document.getElementById('valorLiquido').value = nValorLiquidoFinal;

	  		document.getElementById('ValorGlobalLiquido').innerHTML = "<strong style='color: red; font-size: 16px;'>" +nValorGlobalLiquidoFinal+"</strong><input type='hidden' name='fMovValorParcela' id='fValorLiquido' value='"+ nValorGlobalLiquidoFormatado +"'>";

			SaldoContrato = document.elementById('SaldoContrato').value;
			//alert(nValorLiquidoFinal);

		}

		function calculaParcela(nQdtPArcela){
			var nParcela = nQdtParcela;
			nValorParcela = nValorLiquidoFinal / nParcela;
			alert(nValorParcela);
			return false;
	  		document.getElementById('valorLiquidoParcelaExtenso').innerHTML = "<strong style='color: red; font-size: 16px;'>" +nValorParcela+"</strong><input type='hidden' name='fMovValorLiquido' id='fValorLiquido' value='"+ nValorParcela +"'>";
		}
*/

		function calculaValorLiquidoItem(){
			nValorLiquidoItem = verificaNulo(document.getElementById('valorLiquidoExtenso').value.replace(".","").replace(",","."));
			nMovJuros = verificaNulo(document.getElementById('fMovJuros').value.replace(".","").replace(",","."));
			nMovRetencao = verificaNulo(document.getElementById('fMovRetencao').value.replace(".","").replace(",","."));
			nMovTarifaBanco = verificaNulo(document.getElementById('fMovTarifaBanco').value.replace(".","").replace(",","."));
			nSoma = nMovRetencao*1 + nMovTarifaBanco*1;

			nValorLiquidoItem = ((nValorLiquidoItem - nSoma) + nMovJuros*1);

			nValorLiquidoItemFormatado = nValorLiquidoItem.toFixed(2);
			nValorLiquidoFinal = nValorLiquidoItemFormatado.replace(".",",");
			//document.getElementById('valorLiquidoExtenso').value = nValorLiquidoItem;
	  		document.getElementById('valorLiquidoParcelaExtenso').innerHTML = "<strong style='color: red; font-size: 16px;'>" + nValorLiquidoFinal +"</strong><input type='hidden' name='fMovValorPagar' id='fValorLiquido' value='"+ nValorLiquidoItemFormatado +"'>";

		}




			function verificaMovimento(nMovimentoItem,movimentos){
				var cont = 0;
				var array_temp=[];
				for (var i = 0; i < movimentos.length; i++){
						if(movimentos[i] == nMovimentoItem){
							cont++;
						}
				 }
				 if(cont > 1){
					cont--;
					return false;

				 }else{
				   return true;
				 }

			 }
            function removeVerificaMovimento(id){
				array_remove_temp = [];
				for (var i = 0; i < movimentos.length; i++) {
						if( i == id){
							movimentos[i] = '';
						}
				 }
			 }
             var qtdeCampos = 0;
			 var cont=0;
			 nMovimentoItem=0;
			 var movimentos = [];

            function addCampos(){
				nMovimentoItem = document.getElementById('fMovimentoITem').value;
				nCodPessoa = document.getElementById('fPesgCodigo').value;


				movimentos[qtdeCampos]= nMovimentoItem;
				var rep=0;
				var objPai = document.getElementById("campoPai");
				//Criando o elemento DIV;
				var objFilho = document.createElement("div");
				//Definindo atributos ao objFilho:
				objFilho.setAttribute("id","filho"+qtdeCampos);

				//Inserindo o elemento no pai:
				objPai.appendChild(objFilho);
				//Escrevendo algo no filho recém-criado:

				document.getElementById("filho"+qtdeCampos).innerHTML="<div id='idMovimento_"+qtdeCampos+"_"+nMovimentoItem+"'></div>";
				if(!verificaMovimento(nMovimentoItem,movimentos)){
					rep=1;
				}

				recuperaConteudoDinamico('index.php','action=MnyMovimentoItemAgrupador.gravaLinha&rep='+rep+'&idCampo='+qtdeCampos+'&fMovimentoItem=' + nMovimentoItem+"&nCodPessoa="+nCodPessoa,'idMovimento_'+qtdeCampos+"_"+nMovimentoItem);
				qtdeCampos++;

			 }

			 function removerCampo(id){
				var objPai = document.getElementById("campoPai");
				var objFilho = document.getElementById("filho"+id);
				//Removendo o DIV com id específico do nó-pai:
				var removido = objPai.removeChild(objFilho);

				removeVerificaMovimento(id);
			 }

       </script>


       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>

  <div class="modal fade" id="Objeto" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Parcela</h4>
                </div>
                <div class="modal-title" id="conteudo">&nbsp;</div>
                <div class="modal-footer" id="rodape">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
          </div>
    </div>
</div>


