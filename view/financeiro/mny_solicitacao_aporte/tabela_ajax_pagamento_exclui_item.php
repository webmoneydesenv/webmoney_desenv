<?php
header("Content-Type: text/html; charset=UTF-8",true);

$voVMovimentoLiberado   = $_REQUEST['voVMovimentoLiberado'];
$oMnyContratoDocTipo = $_REQUEST['oMnyContratoDocTipo'];

$sReadOnly = $_REQUEST['Anexado'];


$aItemEstonado = $_REQUEST['aItemEstonado'];
$nCodUnidade = $_REQUEST['nCodUnidade'];

$sReadOnly=1;

if($voVMovimentoLiberado){?>

<table id="liberados" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
       <thead>
        <tr>
            <th class="col-sm-1">Estorno</th>
            <th class="col-sm-1">F.A</th>
            <th class="col-sm-7">Hist&oacute;rico</th>
            <th class="col-sm-1">Valor</th>
            <th class="col-sm-3" style='width:27%;'>Comprovante</th>
        </tr>
       </thead>
    <tbody>
        <? $nTotalLiberado=0;
            $i=0;
            foreach($voVMovimentoLiberado as $oVMovimentoLiberado){
                $nMovCodigoMovItem = explode(".",$oVMovimentoLiberado->getMovCodigo());



            ?>

                <tr <?= (in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado))? "style='background-color:rgb(246, 181, 181);'" : (($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 )? "style='background: antiquewhite;'" : "") ?>>
                    <td >
                          <? if($oVMovimentoLiberado->getAssinado() != 0 And $oVMovimentoLiberado->getAssinado() != 1){?>
                            <icon class='glyphicon glyphicon-usd' style='font-size: 50px;color:yellowgreen;' data-toggle="tooltip" title="Transferência do Aporte!"></icon>
                        <? }else{ ?>
                            <div class="col-sm-6" style="background: #097AC3;width: auto;border-radius: 10px;">
                                <input name='fFaEstorno[]' type="checkbox" value='<?= $nMovCodigoMovItem[0]."||".$nMovCodigoMovItem[1]."||".$oVMovimentoLiberado->getAtivo() ?>' data-toggle="tooltip" title="Estornar item do Aporte!">
                            </div>
                            <div class="col-sm-6">
                                <a href="#" onclick="recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.processaFormularioExcluiItenDoAporte&sOP=ExcluirItem&FA=<?= $nMovCodigoMovItem[0]."||".$nMovCodigoMovItem[1]?>&nUnidade=<?=$nCodUnidade?>&nIdMnySolicitacaoAporte=<?=$_REQUEST['nIdMnySolicitacaoAporte']?>','divTabela')">
                                    <icon class='glyphicon glyphicon-trash' style='font-size: 26px;color:#C9302C;' data-toggle="tooltip" title="Remover item do Aporte!"></icon>
                                </a>
                            </div>

                        <? } ?>
                    </td>
                    <td <?= (in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado))? "style='background-color:rgb(246, 181, 181);'" : "" ?>>
                        <input type="hidden" name="fFA[]" value="<?=$oVMovimentoLiberado->getMovCodigo()?>">
                        <input type="hidden" name="fMovCodigo[]" value="<?= $nMovCodigoMovItem[0] ?>">
                        <input type="hidden" name="fMovItem[]" value="<?= $nMovCodigoMovItem[1] ?>">
                        <a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?//= ($oVMovimentoLiberado->getMovTipo() == 188)? "Pagar" : "Receber"?>&nIdMnyMovimentoItem=<?=$nMovCodigoMovItem[0]?>||<?= $nMovCodigoMovItem[1] ?>" data-toggle="tooltip" title="Detalhar Movimento!" target="_blank"><?= $oVMovimentoLiberado->getMovCodigo()?></a>&nbsp;
                        <a href="relatorios/?rel=ficha_aceite_nova&mov=<?=$nMovCodigoMovItem[0]?>&item=<?=$nMovCodigoMovItem[1]?>" target="_blank" data-toggle="tooltip" title="Gerar Ficha de Aceite"><i class="glyphicon glyphicon-duplicate" style="font-size: 18px;"></i></a>
                        <? if(in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado)){?>
                        <span style="color:red;font-weight:bold;">ESTORNADO</span>
                        <? }elseif(!(($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 ))){
                            $nTotalLiberado += $oVMovimentoLiberado->getAtivo();
                        ?>
                            <!-- o link para estorno esta na conciliacao...
                            <a  href='?action=MnyMovimentoItem.extornaItemMovimento&nCodSolicitacao=<?= $oMnySolicitacaoAporte->getCodSolicitacao()?>&fa=<?=$nMovCodigoMovItem[0]?>||<?= $nMovCodigoMovItem[1] ?>' class="btn btn-warning" style='font-size: x-small;'><icon class='glyphicon glyphicon-retweet'></icon>  Estornar</a>
                            -->
                        <? } ?>
                    </td>
                    <td><?= $oVMovimentoLiberado->getAporteItem()?></td>
                    <td><?= number_format($oVMovimentoLiberado->getAtivo(),2,',','.')?></td>
                    <? if(!is_null($oVMovimentoLiberado->getCodSolicitacaoAporte())){?>
                        <td style='width:27%;'>
                            <a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=<?= $oVMovimentoLiberado->getCodSolicitacaoAporte() ?>' title="Visualizar Comprovante de Pagamento" ><button type="button" class="btn btn-success btn-sm" style="font-size:6px;"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button></a>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?= $oMnyContratoDocTipo->getTamanhoArquivo() ?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">
                                </div>
                            </div>
                            <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                            <input type="hidden" name='fCodArquivo[]' required value="<?=$oVMovimentoLiberado->getCodSolicitacaoAporte()?>">
                        </td>
                    <? }else{?>

                        <td  style='width:27%;'>
                            <? if(($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 )){?>
                               <button style=' opacity: 0.0;filter: alpha(opacity=0);' type="button" class="btn btn-success btn-sm" style="font-size:6px;"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">
                                    </div>
                                </div>
                                <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                <!--<input type="file" data-buttonText="Anexar" class="filestyle anexar" data-iconName="glyphicon glyphicon-file" data-buttonName="btn-primary" style='width:50%;' data-input="true"   id='arquivo' name='arquivo[]' />-->
                                <input type="hidden" name='fCodArquivo[]' required value="">
                            <? }else{
                                echo "
                                <div class='form-group'>
                                    <div class='col-sm-3'>
                                        <div style='display:none;'>
                                             <input type=\"file\" id=\"file$i\"  onblur=\"verificaUpload('file$i',$oMnyContratoDocTipo->getTamanhoArquivo())\"  data-buttonText=\"Atualizar\" class=\"\" style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" />
                                             <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'>(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)MB</span>
                                            <!--<input  type='file' data-buttonText='Anexar' class='filestyle anexar' data-iconName='glyphicon glyphicon-file' data-buttonName='btn-primary' style='width:50%;' data-input='true'   id='arquivo' name='arquivo[]' />-->
                                        </div>
                                    </div>
                                </div>
                                <input type='hidden' name='fCodArquivo[]' required value=''>";
                                echo "<span style='color:red;'>Aguardando comprovante da transferência</span>";
                               }?>
                        </td>


                    <? }?>

                </tr>
                <? $i++;
            }?>
    </tbody>
    <tfoot>
        <tr>
            <td ><input type="hidden" name="soma" id="somarealizado" value="0"/><div class="col-sm-4">- TOTAL LIBERADO PARA PAGAMENTO:</div><div class="col-sm-2 soma" id="totalrealizado"><span style="color:red;font-weight:bold;font-size: 250%;"><?= number_format($nTotalLiberado,2,',','.')?></span></div></td>
        </tr>

        <tr>
            <td >
                <a href="#" data-toggle="tooltip" title="Estornar Item do aporte!" onclick='confirmaEstorno(<?= $nMovCodigoMovItem[0].".".$nMovCodigoMovItem[1]?>);'>
                    <span style="font-size: 26px;color: #fffdfd;background: #12749b;border-radius: 10px;font-family: initial;font-weight: bold;padding: 4px;">Estornar Itens</span>
                </a>
            </td>
        </tr>

    </tfoot>
</table>
<? }?>
