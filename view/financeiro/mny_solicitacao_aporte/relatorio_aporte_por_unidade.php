<?php
 $voMnyUnidade =  $_REQUEST['voMnyUnidade'];
 $voResultado = $_REQUEST['voResultado'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Relatorio Aporte por Unidade</title>
 <!-- InstanceEndEditable -->
     <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
  <!-- <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css"> -->
 <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <!-- <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css"> -->
<!--
 <link rel="stylesheet" href="js/jquery/plugins/bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="js/jquery/plugins/bootstrap/css/jquery-ui.css">
-->
 <link rel="stylesheet" type="text/css" href="js/jquery/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css">
 <link rel="stylesheet" type="text/css" href="js/jquery/plugins/datatables/media/css/jquery.dataTables.css">
 <link rel="stylesheet" type="text/css" href="js/jquery/plugins/datatables/dataTables.bootstrap.css">

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/bootstrap-select-min.css">


<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<style>
    .show-tick {
        width:100% !important ;
    }
</style>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->


 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <strong>Relatório por Empresa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Relatório aporte por unidade</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnySolicitacaoAporte" id="formMnySolicitacaoAporte" class="formulario">
          <input type="hidden" name="sOP" value="RelEmp">
                        <div class="form-group">
                            <label class="col-sm-1 control-label" style="text-align:left;">Periodo</label>
                            <div class="col-sm-2">
                                <input type="text" id='dtInicio' name="dtInicio" class="form-control" value="<?= ($_REQUEST['dtInicio'])? $_REQUEST['dtInicio'] : "" ?>">
                            </div>
                            <label class="col-sm-1 control-label" style="text-align:left;font-weight:bold;font-size:16px;"> até </label>
                            <div class="col-sm-2">
                                <input type="text" id='dtFim' name='dtFim' class="form-control" value="<?= ($_REQUEST['dtFim'])? $_REQUEST['dtFim'] : "" ?>">
                            </div>

                       </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <br>
                    <div class="rows">
                        <div class="form-group">
                            <label class="col-sm-1 control-label" style="text-align:left;">Unidade</label>
                            <div class="col-sm-11">
                                <select class="selectpicker" name='fUniCodigo[]' id='fUniCodigo' multiple data-actions-box='true' >
                                    <? if($voMnyUnidade){
                                            $sSelected = "";
                                            foreach($voMnyUnidade as $oMnyUnidade){
                                    ?>
                                                <option  <?= (($_REQUEST['fUniCodigo']) && in_array($oMnyUnidade->getPlanoContasCodigo(),$_REQUEST['fUniCodigo'])) ? "selected" : "" ?> value="<?= $oMnyUnidade->getPlanoContasCodigo()?>" ><?= $oMnyUnidade->getDescricao()?></option>
                                        <? }
                                        }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="rows" style="margin-left: 600px;">
                        <!--<input type="submit" class="btn btn-success"  value="Gerar Relatorio">-->
                        <button type="submit" class="btn btn-success" >Gerar Relatorio</button>
                    </div>
         </form>
     <?php if(is_array( $voResultado)){?>
     <legend>&nbsp;</legend>
     <div class="col-sm-12">
   		  <table id="lista" class="table table-striped table-bordered display" align="left" width="100%" cellpadding="0" cellspacing="0">
   				<thead>
   				<tr>
   					<th class='Titulo'>F.A</th>
                    <th class='Titulo'>Empresa</th>
                    <th class='Titulo'>Aporte</th>
                    <th class='Titulo'>Vencimento</th>
                    <th class='Titulo'>Pago</th>
                    <th class='Titulo'>Desconto</th>
                    <th class='Titulo'>Juros</th>
                    <th class='Titulo'>Parcela Liquida</th>
                    <th class='Titulo'>Nome</th>
                    <th class='Titulo'>Descrição</th>
                    <th class='Titulo'>Unidade</th>
                    <th class='Titulo'>Conta</th>
                    <th class='Titulo'>Custo</th>
                    <th class='Titulo'>Para</th>

   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach( $voResultado as  $oResultado){ ?>
   				<tr>
  					<td><?= $oResultado->FA ?></td>
                    <td><?= $oResultado->emp_fantasia?></td>
                    <td><?= $oResultado->numero_aporte?></td>
                    <td><?= $oResultado->vencimento?></td>
                    <!--<td><?//= ($oResultado->pago != '-')?"<a href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oResultado->pago."' target='_blank'>SIM</a>" : "<span style='color:red;'>NÃO</span>" ?></td>-->
                    <td><?=  $oResultado->pago  ?></td>
                    <td><?= $oResultado->desconto ?></td>
                    <td><?= $oResultado->juros ?></td>
                    <td><?= $oResultado->valor_liquido ?></td>
                    <td><?= $oResultado->nome ?></td>
                    <td><?= $oResultado->descricao?></td>
                    <td><?= $oResultado->unidade?></td>
                    <td><?= $oResultado->conta?></td>
                    <td><?= $oResultado->custo?></td>
                    <td><?= $oResultado->tramitacao_para?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   					<td>Período: <?= $_REQUEST['dtInicio'] . " à " . $_REQUEST['dtFim'] ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <th></th>
                    <th></th>
                    <td><?//=$nTotalValorPagar?></td>
                    <td><?//=$nTotalJurosAcrescimo?></td>
                    <td><?//=$nTotalDesconto?></td>
                    <td><?//=$nTotalPagar?></td>
   				</tr>
   				</tfoot>

  			</table>
     </div>
     <?php }//if(count($voMnySolicitacaoAporte)){?>


 	    <!-- <script src="js/jquery/jquery.js"></script> -->
  		<!--<script src="js/bootstrap.min.js"></script>-->
        <!--<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>-->
  		<!--<script src="js/jquery/jquery-ui.js" type="text/javascript"></script> -->
        <!--<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script> -->
        <!--<script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script> -->
        <!--<script src="js/jquery/plugins/dataTables.colVis.js"></script> -->

        <script language="javascript" src="js/jquery/plugins/datatables/media/js/jquery-1.12.4.js"></script>
        <script src="js/jquery/plugins/jquery/jquery-ui.js" type="text/javascript"></script>
        <!--<script src="js/jquery/plugins/bootstrap/js/bootstrap.min.js"></script>          -->
        <script src="js/bootstrap.min.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/media/js/jquery.dataTables.js"></script>
       	<script language="javascript" src="js/jquery/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
        <script language="javascript" src="js/jquery/plugins/jquery/plugins/dataTables.bootstrap.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/extensions/Buttons/js/dataTables.buttons.js"></script>
		<script language="javascript" src="js/jquery/plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/media/js/jszip.min.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/media/js/pdfmake.min.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/media/js/vfs_fonts.js"></script>
        <script language="javascript" src="js/jquery/plugins/datatables/media/js/buttons.html5.min.js"></script><!-- extens�o / buttons - pdf exl -->
        <script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
        <script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
        <script src="js/producao.js"  type="text/javascript"></script>
         <script>
             jQuery(document).ready(function(){

             jQuery(".chosen").data("placeholder","Selecione").chosen(); });
			 jQuery(function($){
				$("#dtInicio").mask("99/99/9999");
				$("#dtFim").mask("99/99/9999");
                $('.selectpicker').selectpicker();
			 });


         </script>
    <!--
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
    </script>
    -->
            <script type="text/javascript" charset="utf-8">

        // jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })

           var asInitVals = new Array();
            $(document).ready(function() {

            var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 0 ?
                        data.replace( /[.]/g, ' .' ) :
                        data;
                }
            }
        }
    };


            $('#lista').DataTable( {

                	"scrollX": true,
    				dom: 'C<"clear">lfrtip',

                dom: 'lBfrtip', // Estava 'Bfrtip' e troquei por 'lBfrtip' para exibir a quantidade de linhas
                buttons: [

                 $.extend( true, {}, buttonCommon, {

                extend: 'excelHtml5',footer: true,
                columns: ':visible',
                customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row c[r^="A"]', sheet).attr( 's', '25' );
                $('row c[r^="B"]', sheet).attr( 's', '25' );
                $('row c[r^="C"]', sheet).attr( 's', '25' );
                $('row c[r^="D"]', sheet).attr( 's', '25' );
                $('row c[r^="E"]', sheet).attr( 's', '25' );
                $('row c[r^="F"]', sheet).attr( 's', '25' );
                $('row c[r^="G"]', sheet).attr( 's', '25' );
                $('row c[r^="H"]', sheet).attr( 's', '25' );
                $('row c[r^="I"]', sheet).attr( 's', '25' );
                $('row c[r^="J"]', sheet).attr( 's', '25' );
                $('row c[r^="K"]', sheet).attr( 's', '25' );
                $('row c[r^="L"]', sheet).attr( 's', '25' );
                $('row c[r^="M"]', sheet).attr( 's', '25' );
                $('row c[r^="N"]', sheet).attr( 's', '25' );
                $('row c[r^="O"]', sheet).attr( 's', '25' );
                $('row c[r^="P"]', sheet).attr( 's', '25' );
                $('row c[r^="Q"]', sheet).attr( 's', '25' );
                $('row c[r^="R"]', sheet).attr( 's', '25' );
                $('row:first c', sheet).attr( 's', '30' );
                $('row:last c', sheet).attr( 's', '30' );
                $('column:first c', sheet).attr( 's', '42' );
            }
} )
            ,
            {
                extend: 'pdfHtml5',footer: true,
                orientation: 'landscape',
                pageSize: 'A3',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'left',
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM4AAAAqCAYAAAD7ymRQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAC/sSURBVHhe7X0HeFRV/vY7SSYz6b2QSgldkKZIERSQRRQRQVdExV5B0VUsrIjuslgQsf8Re1nFgl1BEQVWRIoCIhBqCoH0PsnU5Ht/59xJZiZB4dvnex6//5M3ubn3nl5+9dxzb0zNBE4QNfZ8VDTsg8NVBZujCJWNebA569Bgz0NwSLBKU8fw6QO+Q6Q1Rd13oAP/G3Fcxqm3V6Ow6hsU1q5HdeMhlNt+Q0hwOMxB4QgyhSAohEdTMExBPBDEgoAmUxCZqhxn5SxGl/gx2Fu6EvWOY+iZPAVRljSj5LaodZSg0XkUDncNgk2hKK7fheTI3ggNiUZieG8EB4UaKf1R7yxBVcN+VNhyERfWBcHBEczXH6HBYUYKf9SxnprGA6izl6Cp2Yloazqs5jjEMG9IkBV2CgSTSfrTzB/5ywGSjPJHjRL/8Lq5mX/kl7fN/BPMH0tIjCRguc0oq9/B8TuGKsdB9qtUhRmJmdeNsJB4pEQPRHxEX0RbOql8v4fKunqsy81HbnE5HI1OmENDEBluxYicTAzukmGk+n3syC/G+gN5qKyqhznYBIvFgtO6pmNUr2zGBulEHThh+DHOkarNyC15D0UNG2F3lCGURBWiGCVIMYskNZlMaOJPMImgSVEVw4Qg5Mw7d5MDGTEjyTyLsHb/XORVr4anyQWnqw6RlnQkRvY10gahwVmMqf0+w/Mb0xAemsTQYGFBBJFRmpodaHRXo3/K1RjZdb5U5IeC6u/w6a7LEBaayjZKejfrtiHe2h9TT33PSOWPZT/2JPNbEWIys+fB8JB5qqk1Zw8vJLP+jI9+nUZGSlBppUdN6iy9Mohe84tqvyI1BjfBReLvhemDvpAQHCr/Cl/suQZRYRkUAroeg2ckOXtoYr1ueDwOuNheE0LQr9NVOCP7LpW/PTz39UbMWvw6kBTH8qQFbJmHpVXWIPfVheiRlqgTHgc784pw6tUPAJ2YLshgkiaPSEcceGkBuiXrPnfgxBFkc5Zh/cEHSLw5WL3/OhTbfuJUWhFj7QJrUAwnmpPfFMRJb6K2cVP6exAkBMMJbG4SpmGcQRlNPIQhbPajqvAq2yFYg+MRac5AgtIgESTUg0qD1fBcUreD82giQ2WQYFMRHpIACwnXQs0RFpKIGHM27J5KVVYgSmp3IcqajXBzIjVMFPPEISIkHc6mciOFP2rthWSyRKZJpsSNUXmswbHIjjsb5pBIMp+F5WUhKjST2jGDbcoiQ/Ce11EWhlkzEBMq1xmIZppIpo20Stoslhtl1AKllcMsqRy7OAqbcDWWISYLzxaE8lqEgtkUASv7KuVGhnbCvtL3seyHvqi2Fxml+GMXCR8pZJpIatIIC49wIJpHSjzuekcz7O/hX599D2SlAlERzGvlwXIieR0XrZi6AyePoBU/T0RB1VrEh3fhnKTS3JHJDVZSlTyhBBSFJs0aD3YWpCCvLAZBwaIxtIYRLhKNY+IMkAd4mFHm2KMKr6apYqKmMpmalKB0u4LIeCSiIDPPZjKSl+AkL9NIfbpQxYym4CAU1/6iUgTicOWXZIAw1QYp28SM0u5aRyEPzbi+EMZparZL0UwvGlRL/ihzpooXIUCVqtqgypP2SLskUsIkjdyoOKaTQ2lgwOWuRRO1qoaIDukL8xplKU3Fm2a20aR0jg6XMqRsM03S6PBkatBLdBEBcHoboSC5jbMlFGv2HITT7TbC2qLS1ogVG3cwLRmO/fHLz/Y7RPN04KQRdEr6DEUeYoZp4mEoz3JSFgGPuDA77n5tBP7x8kD8+7ue6JZUg/ioRlhDXcwnphv1Ec/K6iNF2B1VkpvE5ORfbdg47JKmniabrsPDCYuxiH0t1em6JXsT61M+Ac9ixtQ6CmjSNKp0vmh0UROJtuO1R2xGKYOHJSQOeaXf6kQ+KK75lX9pbkofpU0sX3yqfulXqHgdKsXotkg7pBlySFsEci20rsaL9NbMvkp0uCWFTELNrCDt4JhIHu+YsJ12dz0q63NR2ZCLBnc5i2QCabcacynRDKenHntK3tXF+MBqlrJVgeq+BWTGxup6fPjDTiOgLd7ZsI1ZmVfxi/zxlsGzx03rQOaoAyeLoDhLF86di/ROaWiMqRCEXGvJakJslAODupJQYxzYUWLBFU9PwOwXz8L3O7oizOzipFPKGmmFWELNkThWu4PnCF0utZAwTUhQAszmEDVnzfDQHKL5QIiWETktf1skvjHJYrbVB5gwpbafGcP6ZGGCZdHX1XUzzEwnv6Rxi07og4K6NQg1UUOxAumm9NHjaURCWA8Vr7IbZ6W95J43YqLWOgtQ2XgYNfZDNDMP8ziAGoeYm4dQxesGlxYUXkhW0WiKadgvNxrROXYkbh2Zj1uG5+Gcbs/B7qplG4RhmI59lv6EhcYit+xTVYYv1u/LI88L8xiN9EVMBO77+Gvjpi0e+XydNtFUrwXejrJShwdVZLwOnDyCQulLNMGpBZ/xo345qU2c1qz4Wly6dALeXptO0efhJDdjT1kI9hVb8EtePM0M6huVXgiFJTKfmXb+odLVtO2tOo6EY7Ek0DSy0VxzqjKaKbLFRxE4KI1FCtc7j6HBUcz7agpjkZLN9BmicKhilUrnRUHZZs0olKRa2Uh5qtlssRnFdVt5LxK6FY2OEkaKZmU4E4tplRI9SK3cCaTfUqQqh4da+KBW9ND5v3FoLgn+AG4edhA3yXn4Idykrg/i1uGFuLj/x6oMgWRTPh9HT64VZ7NAVxPNRAOZCcNxXq9laHBS84ig8KiUBJm5He1qVkwjZfpC7nmEhCD/SBl27CdzBeC7XQdwpKhUSxY/ptPtE1llNuvHCB04OahVtf/5IQex4V05vwbxSAyv4yIdePD907F5byztEdrRTg5yfSjjeHYAkyYcxvVn76ZGoB8jE6OIROd3NzdyvkTCc4rcFrg9tYxvQjD9GllgcPG+f6drMSD9BmwuWIKUyIGIsXalMRWOEttO/FiwkPnFW3AhMaIf/tLzWWmVwrf778Sxmi30xyzSARI5fRaGi4kn9dXZj+DyIT8i3EyHmqi3l+KjXy8ijVmZXq8KepodyIo5E6NzFqk0RTU/YfXeWyj143nHHgjnBDXB7Xbgr4NWI5Sa70RQULkOa/bfjvDQZNYig0Hdyro6RQ7AuJ7PGKkAm6scH+6cDLMxRuQ2aiYXwmlqXtT/I53IwKAHnsEvRWR8ZbLJ6AokkwG7AxP7dccXd11tBGicPv85bDlyzMgn8OY1UGvD8pmTcd2EkUZAW6zfcwgvrv4R3x8uRFEBy3LSRo2woHtWOs7qmo4rxpyBM9WS9vHR6HBi6ZcbcPBYOfaXV2FXSTmFq2ZYN32BvikJnItIjB3SF1eNGKjCTwRvbPgFq7b9hjUH8lF2mFYJxzAsIwl/6d4Zk09nWaNPM1ICz328Bqv3H+E8Sr0Uahz0ersLqeFWvHr7DJ0oANvZ33ve+IwKO0wNndgfDS7SYyQtKWGcV7YMUs8zTBR/EqlnEkiNbcDZf5sK0KeBIxhj+5fjtvG5iIy0o7QqgpqhifncpA2tbVQu/lFaSxkhIvXFNCJxe2iuUUOYQxlG9SaM07fTVRiUcbPkUnC46/DJjhmocR9AVGi6KoWGONObMX3wanUneGPLGWolTLVTCNzkYUe0CSY2lmisQamz0T9D+y+F1f9RxGwlUQYJ4zCLLIVP6P0y0qNPV2mKan7E17mzyGzxqhzVB/66m+2YMWgdiz0xyVxQuR7fHJiNCHOS7r0wBMvoFDUYY3ss1YmIw5XfYu2+OxFuTZZ5VKadiz5OcvRQjO/xpJFKY9DfnsAv1ZyDEBEPapT9IeaC04XqF+YjJlwWAYAKMkXitQ/QQY1hFjUyBiS/3PNcU483broEV5zVSmBe5BaUYPg/X0BlCU30+ChqLfZfEZ0eF+X8ejg3VTVISk3CoSVzESkE5oNKlp8wZQ6FrhWIpeChdlQ2ME1sXYgB8cGkD43UytUNWPf4HRjVX5vQ7WH74aMYeM8TLIL5ZHUwmOPibZuURbqEjZqbZda9tQiRVguFqRPR55PWMr3PzaR+GYM6vD7rClw5ZogO9oHpr3eLg6nb7B330ip8teAmJagRZ8khYcvqCgvjBDYpM4eVNobznqpF4AnCqVnVyEwsh5NSJ9xqg8Xs4PiJ+SP5JJFmGf2niSa0TZkoYpaFhDhpplNrSVo2xE0pHB92iiRsgYXaqN6Th4jQNFWSKpdNrHMW6gREuW0P2yeMwngOTDO1mGJcEof8mNh2MddqnQeNHGQSF80VsYlYnNe0k4WH1Mj+RgqBLkMejwjUQ04pD8GUkp/gQMUX2F/xmXF8qsLkWVIbMJt+riUVqV+WS80pz5lcdjQ6q7C39D18t38uwixJMkySguKB0sxDhk+7TgL8YCK9aahB9oHRWJlFEsar32/S98Tt//5CrbppppF83rzee4Km6+EKf/9M8O6mHeh1wwJUChGmUgOHkniEOL2Q7MLEFoaTacrcbkRNuxN784t1vAGXh/MtS+jUAmoJXdojPq6YjooRWYYcEiZxsTSbO6dg9L1P4ulP6Ju1gyc/+QYDb36YaSk44ykUAtsmTBnK8uLI7HHRiJp8G4qqaukWhGLJrZdp5pR4ySfnxDjMXPq6kbkVH23ZrRZPwHw6LQ/Sbd/e2ZhwWj/NOIlR/UlQbo6HTLiYOzLRJiRE0XGUwmWsw11YsrIn1f80/P3fQ/HllhwSuIkNos8iI8k04ueotDw73LUY0Ok6ZFCCuptdqLAd0M9vHPmoachDWf0eJEX0lOr9EGfpTaEhDCazIy3S55La7Sre5ipWVTSLBmCUxHuaG1Qa+ZVI2WlwrLZ1gSC/6jv6SjTr2C5JAhJxpCWb40BJ6AvWKYscSg6ohLJ8HoIf8x7BhoPzseHQAvxw8CH85+DDPB7Ct/vmkjikbh+ozMyp2k/wFCK7Ieq24+WtA/Dmz8NZ3uMIEw0vP0LYrKyRGrBv8gz1gDgQDWIe6YarewWR0EqwCBgZFYZHP92g7uw0J1b8yPEK8/aP8dRI0h9dkOTjQcY5Wuu/OFBGIpu+6GUgO9UgSEmvKieYR+ptCTPCRZNkJKP33Y/D6fYuyzNWD6LRbKNObz51Mq7V4ROfmYrbX3mfvqz/M7wfdh/CnctoxnZOM/IKmMetNR+q6nhN2vFCms9+ZFxPzUvcMWUsh4SMoISVUa+UQ+Z+VhZRfHDRE69oxmxJx1NBMbb+6w6J1oyTGX0G/ZAG0g0boSZcsQIq6i2YPJJOZy0rE0Q5labYVhiDZWs644J7JmHj3kxEWx2qywLJp6WcrBLF4+zuj+PKwRtwy4g85VRfc9p23DBsL+aMqkSEmCkBSAo/nYxAqSBt5SGMLKtzpfR7BAdLV1FYmRktbTRxomrRNf5C7VSLFiLTC7GX2fZyPLVDXmHbTSEXqsqSlkkfMuOGto69QCIYoJhf0ikGYB00Aa0hMTziEUZTz2KOVdtrrKEJTEVfzW1T2VtgtLlJ8sqh7kiDwWEcpyz1IFXKk3SqvYx0NdXjFAqZEV3mqSIC0aCIVWA0mPfxYfJQldPnZR5K8OKKSuSVVODrn3PhridDq9llnoZGXNqvi/KFdEcFup8hASboeU++SYLRCyYt9QkaOJblNQiT8SkuJyN6l7GN8sScoeaY9UrrQklbGOWJdXOQPokcZdXsj1CyxPnUFx2JRz/y1+jjnngVSE9klYryNcj4Q5MT8O2Dt2DNAzeiu5im0lZvWaQHeYa18EP9iGL7IhJ+oVczGm2PDMfsl1bqa+LtdVsZxThVhKThUd+Iu66YxHmn5iHU0KbHDodTtn+QaWQp1isJ6xvNuHPiDowcWMHBouqrIwO5mCWYDQ9j59Or8cirA6hJomCmI200Q9UZbLKguPZnI6QVsuImGk12EbSHvmlTmJ/az2ivtEdW50pqdFkltq3qXmwuIUkXzcEB6Vczj6xiyQ/bweZZzZEordtB86hRPQeSOmUkpI9Odz3SY4ar8rwQejCRINVinjEGAqMZBmT5WHw3iROm4lmNoD9UbsYJE4sgktRBikmYXwpjZWqspU4WYA6KxO5jb+Cz3TPIEz5EYcDcYooYLaFGuah/T0zonqmlrQpnPvoY936wGotXU/PEcr6kZtZppdUw/+LzAGEmBd1+0TjFda2ML37Rlp37tJXhC+a7YtggNH/4JBpeXIDmT57BcPEVGoQRpSwBz6x/+drNbF6r1mlpsxdNHkSbKcS+eVEdvz19L6LFJ2kRDgbolzy/frNxA3yzMxf2CmoVtrmlTpqn007vj02L5mBM3xyM7dcD+xbfhSxhHuV6GKDmXbTmB3XZo1MSJo4bSodaGN8oRyaCGnv2K3pR5vIlrwExXuFhoKYGj195gXFjTLsQs5sqVuhACFBYQLohf49UWjBvyk9Y9ehXuHf6r7hoMLnVSa5T88UMCTa882MXmqhiXsnyMHPyV3yDGscRKf6kEBfRVe13U74LW9LEARXHudqer9rkEAkvRBnkoZZxIyV6AE3GJM51NDWVsVDBH3NwFA5WrqbPVMJ07Jv8CFGyXBM1Ujq1rC/UQ1j+SPulTqmftxxT3rNcedYl9Ym28oD3cg0SiDTKF7zXZQkxGGPJ+yYTczU1wEktKEvherlfukLvhu0S0032zb26ebBK70WdrYECiOaUmmNjol1u5ND3uP2cEcrB1+BU0k9Y8dNv2HCY4652q7McSt8rRgxEdoqsFvpCTTYSxPcw8Om23wKYRjrjQUx4BN64xX9Xww8PzyLhsAxF8MahJQHW/nZApfGHtJ1pfPom6JORgp/+eSu1mZhl3jijrMZWBnx1LU1v+iwtkKQ0P9+foxeAfPHY5RQSNq/WYUIKHltR61asL+ZcDlTTrNMzoMJkAePZrzfisZXUTFGy8CThkp84VoGNS+7T1wa8oox+Th+Wof0cIVSRiLKPTDZ4VtSHodIehCHZpbjqnN+w9wmqNSelvgwCS6isNlNqigTVy8IiFIKDQ+mXtEqMk0FS+Cks2miLKteMOkceCumrqG0uSgILYTgRH659ghhLN7bHozSz5JMNlvXOI6iok+0/YjbpbTZC8PFW2XGtV5+8UPmMNMoXookmc9dEwrHSPAsjc8qrEuHmJFjJlHKdFn0aCT5gg6QMiTEWUqjWXh6VrnvSZHSOP5vlxaOq4RBpzsl6glWd0uoQmnOhFGJf7rlGl0U4SZhOpYWkz8YkM3ktCWPcgJ4wx1BzS7QC48PEkRXTmokkOTXSveePRrWsMimilbrkzIOm1Zc+RP7htl16BUylEfDscOGS09v6XYJL+tJHbfFpWvPsbjGFfOGtuy16pdOfkiVzEcS+6ZR/rTXH9sJjmrC8kDGLjoDpgtkw/eXG1mP8jbj0MTr7Mi46oT4swcgrpeUkCDHj9dtmiIrVcQo8M889H32j9/J5w51uDO3fA8O6Z+l7Ay0tSYroT2lKiS0/bLs0X60S8pwY1Uhzrh6xEQ5KKDt2HiWxyPKTlM1ThEXJYJ2PnRchJFEuag4xpU4WnePHk2CMB4FKKJg4rlH4ufBlzjU7pQilGXZ3FbrFTpIb9Ey5EA6am1K3jL8QbpVtPw7XfKM0qrRTPA7RZplxbZdfxfQTbaT7IAGiacSLcWNqv49wUb/3cWHf9zCl3weYdupnmNx3Bc7r/bJmEF/ILdsn46grlRVEN2JCszAsey7O7PowpvZfSZ/vEMOy4WoWDcqkrF5WCUOCwtRCQo1drySKmWZuWb6VUdVwK78AuJAmm/Y3VKMJSWPUTUe5f1YqutIHsKvFAd80+jCLY2/AIUUqJvYBtduQ7PZffWgO8zraPgg2obxe+5a6Di+812IRtAP1rEQ1QN9LKrbF4BvYlbUgVwFlpicBXdJ4pOujK68TqJn0JBrQ11pAaVw5ZihSEmINk07ijfRKcPiguAKb/jnbuGlFy6zHWrNJKLL1hjeqfJHSYjrZcf87Z2Hcwgtw00ujMHbhZFyyYAwdKg6OpKu24tzTiuB0yENQ5mGJ3qGRrTcl7fg5f4QktUzMMmTg5IpqU3YYN3iK2Hkzw3TbgoPCkRqrl7RTIgbQcjAWDRgnTCIbK0tqt6m8EibNdXqqkZM4ReXxhcQLkcuJSoL3ciVoYp6TYH42WNqs2qECZMKlxT6rPQYm9n1FaUaPmIWE0nYyfrwvo38mOFJZjYYamhXSKB+4DRt+7gWcizpjVVFBj70CHdpl11ykLsNEC9Fv0ARlHGyXu8XJJ1q0h08alusSH6QdSGwrjDuOoeJzBd8U3vLk5J9ToSXMm076w7PRLW3LCLzxBkRSqoNtFOaSa4kX6Sllest1eZCV5G+uqoWCo2XGnS+MMmgGP3vLpTooAC2MkxU/hgPUwHqkQmmvcrNpOphQaRMn3IkDZVbYZeNmoqENysJw/llH0C+jhMSlZlw51+JCi7Q2IwxH69ruG/sjpMXQzhc/wJAyhrtBx1pUtzAG2+ZxIyGsdTk73JJETW5hOqbk4KlHuTS3gtRqGsF86icolBqy7dupOo1KpvqtAtSksV8q8sSgWE/NmVd8MD+1mbEBxw/CNOYgOrJG+WqO1UPoEGopPcbSE8VNvvk9TehmEMGQrunolsn++DrDkpaEFBUbiTN6ddEhWhqoaw1eU7A41QqURpDZy1g+6Vh1vXKk2yKo0WEklT9GHsWM7Q1Yaxq/ZvjBpxyBTzHOlv550zBS2mons1fS5KqkcKngIedyOTNMTDE5y73SuP5IjY/GlHH0daUMBW/ZPEhz4TQVb500WsUEQmZEISG8OxyeKmZhJv7KfjJ5WS04xI1j5XSWajmoDbRDq2VwmcBmxh0zduOuyVvYXtk2IhWKE2xUzZKDQ0JRZqPd/H+BqOC+rIYaUMZHaEcKJdSeOF67SFiZcZS2PuiaMIGSmASniES3oUUD8Y84+WkRg0ms/k+4NViRjAYrFNLVFev85uOsALYHYU7mVuMh9Sr2ofCRB7OBkN3Qdk+Z0jQKrF89ElC3Okz8JePSAOMJmw8h3HneKFlFkEqNg4HUQg9Oah0fxQ86K+EtkG310WRnyPMR2vQ6oRFuCcXrm7T2C8TG/CJjEcIH1E6jexn+QEt9/jhOMCFjFBBr3OYoX84bJ2e2j37bmjtmoPmDJTyeaD0+9J4lnIfcf/mCzhqAlXfOVOXoenzKr67B5oW3G/dtIaTSAnF89VoP5SNPssJ0rDIcK+/+Bk/dvgkPzvgFK+d/z8Gh5GdcRV0QamxWNcRqYnglzrtWmRIahNqGtu/GnAj6Z02Fy9WoyvWWr4iSRQttybaanPgJOrGBeDK/Bw7ZgaP6rgeapMvM0i551pMSM0CStgPW0lIXayJ3yjW9APyU/zi2FDyNzflLsbngKX3OfwpbeGwueBI/H3lRlaBAwpVaRVs2G0vmzdQQ8p6TL+zOWqzcOY3tkucCRt2SkXCR+eWFOUGVSFC1vCut8UIzpRd/HT5QT76spuVzvGXfFsdp9sTWPWjh5hAEy9N5VYdREeen3O5Qy9CCa2Xrjc1rlhpp6LTvySvC9z/rd6y8eH7Vf3DkaLnYy7yTtDyMLL2z6Wu0wLfdGnpkAyFhbcNl/ARjTuunlp91JZKOZ/pF4/6xHJv2HpYkbVBWXY8Fb7bdbd4GkcZCl7dcOTc40DdT795vD36ME2/to1adZEVJZkaefov0rbYHIT26HqdkViA+sg4ZsexAWTTq6+RBnu6aIhd1wUop6mUZN5gmR62jdevLySBOzDBdlOqTNFRpNdlZzR8LTZzoMP/37TNiRlFL0lyTZ0qSV1GXaBzdH6fbhq4BzNYKabyA00oprLrBfPIw9UDFp9hb9i6P95Fb9h5yy3kufw975WDYzmPL4XDVGPmFSSU/x4BtlTaYgy3K1/t8z5X4dPdleHXLaXhj2xDStpMC21gBUznlTxMiaUqmReu9UyFqeVjiRaAZ4K3T1XqfEBmO5s+fR/Pq5Wj+ahnPL6L5vcXGhkaNcDJNjFWY19tPgSxGmGAN1emykuKQ1TVTmYIaevzkmdDZD7+AyY+/ikc++Raj5j+LW+WBYQLnX6UxDmrBId2zkC2veLfAtz4NLzP4Q8K8ZXnRen3/eWdSinhXwYy0MklJMRg27ymYZs7DsAeexWn3P43UWxfCNO1OJF9xDx76cA1eWqWf4RwXgUwjME7Hgx/jdE2cANnJqwrgr9j6MoQmSk6np5njYsLRqnA8c80m/Gf5u7h27M+0f0ViioJplYJy8pAAZEVKXh8tb/CXVieCpMi+zK+fcUh50hbpn1q1o//TKbrtypgQXJPUKf2X9ovmkAgKA1GCISHh6pXw48HIphUm84rCkhJka05wUIRaLpaPlQTT1As28cxwM8NFQ4jZpcqQPEqYSCH6XmxN6UW1rQB19qOwBkcjytpZCSgZX68vJ9NR7yxFTtLFHEu92uURh13FGYMrZ7cHA+VV6JOAGgvvEpW3LE6YnaZVeU3rtqHXr7qIDnMp47ykIWnZiZQEMv0B3PfBGmw4UqJXrlognSSqq/H2jdP0dQu87faBkfz48OZpTShv9943fSJ9lmreGW1S4HUiGZUCZtPREmwtKUOJgzQs22XSktnOWDzydesevuNDygso93fgxzjhoQkkVaciTpWdAyuSW9sQIkF5I4PNucwvC0cFtbpb7HRnJcNK9V40ex4qGg4wS7DWCpY0HKvaKKWdNDIizyADsjKhPuFK1ReaXM12JET5bxD1IjVqCM1sakSm1c9TGEjmbTI5EWPuRaLXjN4epJuqCqmKGWUUlPaQ0VAcoZLpdJLGGD559UH8J4EaIkknY+i9JiR9SDDNJVU/85FZxO+SnQRKM7L8ekcRsmJH4vSMW3QmosEh5TJewSiMaYN8n2mcALxvtbaBx02roNX5P+vU7lh6/SWAMEcLjPplZU6Wa9t7h6eoFEuuugQ9MgKXro02q7McMigq4HfgzeOPf105CYN6UPDJnjRViE/ZMvDib8khmlZPgro/uO+QTva78Jb3h41T8Bv9jNgRypwRBlGTTpLQCWTLvpuStQGNznKUNuxUhJAWOxQDOcln5zyCcd1eUi933ThsP24dcRiXDf4G0079GNMHrUW/9GtVKSeLRDKHep+fphdnWBGn6LF6RzH6JF6mEwWgW+JEOJuqSShu9aRfpLnks9Mn6pbsv5jgC5HGTEWms5MxGykQeE3tK68EuD0OpYk9jHPxXg6VRqVvoEkU5fN5KOo8ppUdAd4J1ZaijKFc8FDcIrGy968RNleJ+k7CmV0X4i+9/J1Y0RTKxxHzSeZVduxS43if45worBYzMmXHsCwqyAqVaDk5u2ScvJpI4/YpY/DW3KvIPNQ89SIdRYszvdCUOlTjVV7Zlo+jFVi36HbcceEoyd4C1XbVbqOtRvvldZQ2kK1DLe1g+dI/ts93F4Vg28LbcPf5Z2k/Trb8eDd1SpvUIRTLPGIuSPskDdt4tMJ/w6gfXKxXjYnc8I88o/TdLNoO6DP7t2z5pj40edIYEaSJxl2PRhKifHapR8o5SLQMRmaC/3aV/1eQ15Q3FyxFneMINVoFPaZI2uNRNHPSMab7YiOVP6obC+m4LyYxHlXL6/ImqewSiA3NxBmd72PeNCOlPypte+nkL6cvLFtQqGlEWIhkFxrhCCnNSyj9Q0JQK2EMlFcc5KMjwzrfq+LL63fjp4InqYUr1MdKhAG1+Ss5RSCxPaZQ5onhmPZCSnR/CqxR9M+GqvyB+PVQIWY8twL1JKTDJNCeWckIpn/z3pzL0LezrxP+x7j75ZX4ZM9B1DW6UFxRg54ZyUiwWPDW7dPR5TifiHrik+/w9o878MuBQp+FA4KO+ai+OZg+fCBu+ov/vj8vykmwp971BBqoHatLyikwgtCtcxrSoqKx/qGbjFR6fIfNexrFDQ3IP3xUCZuUTslIsoZi+6N3Ivg4b6n+8/1vsGLLTuzKLaBAkOeKxmSFmBFCP2tYRipG9+6CW84diU7yysJxcPr9S1HrdCM3X17Uc6J792w0su2Fyx8yUrRFG8ZZseM8Mukx2tpHcUrqlciOOwddE84xYjvQgQ4I2jDO5sInEWZOQr/Uy42QDnSgA4Fowzgd6EAH/hja9+9ABzpwUuhgnP8SDp/Vl7I620mvdnXg/090MM5/gfc2bod15EyYxl6L8Y+8jGReHyn9nWXPDvyvQYeP81/g1PuewoWD+2BkThbmrfwGs/8yQr1t2YH//fhTM86slz7El7sOIMJixrl9cvDYzNZ3vr3YkX8U059+G+FhsmubASbA6fZgaHYnLL+57bsUNzz/Lj7fewgJFisuHtIH8//qv3dtwfur8d7mX9XerlaY1Ef1zh3UC4unn2eEafy09zBufv1j9ElPwVuz2n8oK2j2NGPWyx/gh0NHYA0OxjQy3F0XjTNi/xiPfroWr63bhsiwUKhXP9jZRqcLn829Bl2NVwxqGhpxzsLlKKu14cM5l2NQN/1BecFnW3/Dza+sxLTTTsHSq/X7SAeOlWHcwmWIiwxnOdciQ33VxR+fbd2N2978GD0T4rBqfus38Hxx5VNv44e8Qjw3YxImDNFviz780Rq8tWEbshLisWbe9SosEH979WN8uy8PdocLo3MysEy9+2I8MDNw4wsrsDGvCBaz8cId57i+sRHvzrkSA7JadynMWv4BVu05hEmndseTM/3ft8q+cQGSkjlGtKLdzR70jo/DS7MvI135zrE/us9aiCbO0zu3X47Tu7b9H0R/SlOtpLoWpnNuwHPrt+JwSSV2FZbg8TU/wjT+BpTI+xU+aOCg7zlSgm1Hy7CNRLntQAF+3V+AzYeKjBStMF00B8t/3I5jpVXYVVSMBz9cg+wbHjRiNbYVFGPPvnyWVcQyy7HtGI9Dhdi9Px87eB2I295bjV+OlOLtVf/Brrz2d4LvzT+GoMmz8Pz6bdjBND/lHcHdK75C9PX+df8eCmvqsLeoFFuPsJ8HC9hPtokCoNbnfRqHx4MtBUeRV3AMdRwXXxyrsaGIY/nU5+vwyMdrVVit3Y78/GJsLyqDfFGzPTxETZpXVY/VFGBbZfd1O9hFBjxUUYNzFy1HrXy3jNhbUoX9nJNNbE8gnC43TFPmYMm3mzgeRcgtLsOLP+6EaeKtyCvz/87b7pJyls85YJ+kz9L33L35fv0WHCitxkHSwe52XkwrELqQMljXjkNH8e6u/YgcdyPHq/0+L1u7GQeKK3GovBovc17bw5+ScVJv/qf+rhcl6sLLzsWDUymZ5eMKSbFo8HkPRaDeV5H9SS4PFl81GU9fPw2Lb7wY90w+20ih8eb3W3W6UDPenD0DT10xiaLLhjUL/F+LvXnsUDxw1RT1tFk9hSYh3D15DMMuxI1nDjZSaci/0NjCiVQf5kuMxf0r/L9x7cWpC54DkuPUTudHr7gAd9Ckk60hdfWNuOL5d4xUv48Q2ZtmMiElLgpPXXcx/sn2P3jtVGSzXi/UZMqr0Oyn72vCArX7X/qfGIf7Xv8ExWUViJLd0hIWYlIP3QNhI3Fuyz2s96Zx3F78qv3NkuorPKIRYqLQY86jKsyiypX/VNd2b+CAe5ZAfdmThLvo0ol4WLS+rR6IjvJ5YU1D7/BuxogeXfAiNdLS66bhHzddgpyAnQ5m+UAi6/N9FbwFEkYr5Oqzh2KJeiOWDNOlE2ZTS7WHd9ZvAaLC5VNJeFHoph2osf4zYY334xHk9l8fvxP3TzoLC6aeg7K3H0Hz24+hS2rbj2N4EcLZN9OMcdjsuPQM//du9JdK5QJYQqmbEB6uXm7qHvDfzCYO6IWHp4zBLDKQ2utU34D5543CwxeOUWaOL9bvzEUz68qKiUF4eBg2HC5ke/yl2Kpte+CUV5ttDhQ+9jfMPe9MLLlyMp4UxmXj/71pF6pbPtv0ByAjp0ZF4rZxQzHv/NFYcOFYxPl8pUZRv2KAdrigBYwj83S643GOF9uqNotK+rZ5XvqeBMQyR8mrBiS+N7a2/0JbS1YyTwn7cvWz79BEE4aWCH9PoILWxJ7fDoo9jXXzrsW9FHAPUDDtf3YBmt99FD1S2/vvcia19UlYsJkMMLxrOtJavv3mhdQlL8H416fBOArcu88ZjjvGD8P5vXoIQeCIfMSwHaz7aRdMZNghNL9lz9qXv+QaMa340zFOyyY92t2nZOp9ZTNf/ADTn3kHox9ehntfC3gxyTtp4aGYw3Q3v/Au5j32Mo4GfNp16ggykow+fZVfSkpx+fL31WrYG/LxuXZgU6aOTIKJZk/7rw7f9f43anPg2jnTkR0ZiWqadmtp0vhiF80QEfexcZGIi440QoGxvbuqtjS5nCiznSDjUKLuoOljmnADTGdeiWlL3zQiDEhzW1zW4xCQbKSU/lBLDn5wmf7kkkraNv3bG7er8h6ddg5OocBykCm20IxtA29WKZea6bWtu7Dk6x9E7TDQO0Ea5fLSnHxhlH0Z0au7EQrkpLW/V06B2mRDbh6uZn/vePotPBbw1U0NaYQILf/6NBhHehry6HIk3LoQn+/aqzTQ2FNa6/fiuVXrlbaZ2qcz7hpPy4DtfGdD29f//3SMIx9mV32vs9F/0QT7KR3bNdv3Yv3+w/j5SHufHiIanHhw+rnKhLmbKj1evo3lA/nwdvO7i/HcZechxRqmtUlOJmY+82/UGXZ5+2hvIvTH+w6LxLKGYkdJNXp1pmkZEYan6Iv5ojudamGuavoYTh+b+qj8XxoxQyjxZbHghEAijqaGuXziaEygKTmudzcjwoBfUwPbzXsyTd+YaLx89WT1Pn6VaEelcdrC1kAz9Nf9CIqLQXmjg1YYTSsKgOdp/7cLapDx/bpjfE4X1c560fBeIeiDGGp6+eSUmNabDtAM9MIlG2GPA3cTTu+SgcevnYK/Xz0FN57dzleKpJr27E0FhrOfDfTDKkVIWSwYlJ2Ov9GaCcRL329TzJ8SF49GmS6aqW+t9p9TwZ+OccafkkOmISEnxSH7tkdQXFmDA0/MxcCe2WpgLO0RmUyQ3alMunk0qR4j84QbX+334mBpFUzn3oImDkrx83/HfVPO0VvOKVHc/nMbgPYj1/22D030UUR6Tp33FD76moNLxvmKGsf3ewBjB1CqyfeKaS/3nrsUNjLpD3sOY8Jjr1BLhuGMrhnIlBexTgQkti6JMXjzthn46v4b2t2VrEmnvTZLjEl93PAa2vozzxqozNDj9e/+D9aqr1s2sS+T6JP8sHW3ktortsp/tguElN2Mcpqkq2l+KaHUovn8kcr25/Sk6WcJxciHl3Ne5LO9lYi87iF0m/MY8soDzCcpmubSoMxU3HXeaPzj4vGYMvRUHecL3QTvnwAwjP7oG7dMx4CMZN42YY/6AGJbbKeAln4+9+EqXL3oRXkfQ72D9NWv+4wUGn86xhG8e/dM4FgZyp1OdLpuPhJn3odfZLs5Gccd4EOo+fGQUGMiYDr/Vpgm3gTT+OuRcp3/itUdr60kYVsx+/WPYLpgFhZ9/p2SoAkkjrjAb2kR6t1R0X5ytEMEf/+UKp02/UBOxM/LHsQX82+BiVLWRQbdQrPCi0g64K/RmZfvLRfV1iBy+lyMnP+Mkmqoq8V3x1mqDYR6t6WpGXZ5d+Q4kCTqXzXyCHzKoN4yZV+8q2evzbkKMfIBDNHqDA/s4brfaNdT0v/1tH7Y8oL07yYSnx2NVbVYJUzkA2UlsE7vezb7Hp8LyL8HkfoC3vURrPzbVYwvV6Zczk0Po8vND8HGuT20KxcHA1bhPFI2x+p/vvuJgo9zO5HjfOZMLJb584FH9VvqMwJ8IWW4XOqV7u/v5XgfK0djgx33ve3/j4cf++hbbbqWVWLH8w9gy3PzMKwT/RzROmJ6+uBPyTh/HTUEG5+4m05phnrfPTU7Bf+aOgYZ4tT6fgaVCKetnpaSiJ4clJ6ndkfPgX3QZWBvnN7H/xXpJTMn4yFqpDT5txUptKepBSbkZKDomb8bKfxhpTOcnpyAyE6JCArQcvkcWNEcQnhvXX8xBnZOw8RBvTB9SF8kd8vAM9/5rz7NPGsIfvufBZjQh6ZVYixM7NPNIwfAtWIprOo7AH8MWRRIYNu7+6yiBUJWt3LYt7i0RPXsyxcxERbE0U/p5vMp3JIn7kFOcjw6J8UjlJrXi0Jq+Vw68amMm3/h2eoTVBMH9sLFp52CpIwUfL3H/zsSmQmx6MRyuybquemenog1i25HHLVx9+S2zn6/zE6oeG8xLhYfIz4GQezTJQN6Yv+bizB2UG8jlUYaTUVpX0+Occ9BfdCL7UgZ3DvguwZAp4QoJLHvGW0WDTiXnZKRwjhRSjHUJndcOgGZ7Mej39JK8OHrtXsPICU1Uf1Xg/5ZndjvDMydNBpJrP/fO319V+D/AMk9+LLVdTtvAAAAAElFTkSuQmCC'
                    } );
                },
                exportOptions: {
                    columns: ':visible'
                }
            },
              {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
              extend: 'colvis',
              columns: ':gt(0)'
            },],
            "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [0],
              'bSearchable': false, "aTargets": [ 0 ]
            }],
            "order": [[1, "asc" ]]
            } );
            } );
        </script>

    <script>
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnySolicitacaoAporte');
  	 </script>




       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
