<?php
 $sOP = $_REQUEST['sOP'];
 $oMnySolicitacaoAporte = $_REQUEST['oMnySolicitacaoAporte'];
 $voSysEmpresa = $_REQUEST['voSysEmpresa'];
 $voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 $voVMovimentoLiberado = $_REQUEST['voVMovimentoLiberado'];
 $aItemEstonado = $_REQUEST['aItemEstonado'];
 $oMnyContratoDocTipo = $_REQUEST['oMnyContratoDocTipo'];


 $sReadOnly = $_REQUEST['Anexado'];



?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Solicita&ccedil;&atilde;o de Aporte - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
 <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
     <style>
         .input-group{width:50%;}
         .estorno{background-color:rgb(246, 181, 181); !important}
     </style>
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnySolicitacaoAporte.preparaLista">Gerenciar Solicita&ccedil;&atilde;o de Aporte</a> &gt; <strong><?php echo $sOP?> Solicita&ccedil;&atilde;o de Aporte</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP ?> Solicita&ccedil;&atilde;o de Aporte - <?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getNumeroAporte() : ""?></h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" enctype="multipart/form-data"   class="form-horizontal" name="formMnySolicitacaoAporte" action="?action=MnySolicitacaoAporte.processaFormulario">
         <input type="hidden" id='sOP' name="sOP" value="<?= $_REQUEST['sOP'] ?>" />
         <input type="hidden" name="fCodSolicitacao" id="CodSolicitacao" value="<?=(is_object($oMnySolicitacaoAporte)) ? $oMnySolicitacaoAporte->getCodSolicitacao() : ""?>" />
		 <input type='hidden' name='fSolAlt'   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getSolAlt() : ""?>'/>
		 <input type='hidden' name='fAtivo' value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getAtivo() : "1"?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Solicita&ccedil;&atilde;o">
          <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MnyPlanoContas">Unidade:</label>
				<div class="col-sm-7">
                    <select name='fUniCodigo'  id="Unidade" class="form-control chosen"  readonly  <?php echo $_REQUEST['sSomenteLeitura']?>  >
                        <option value=''>Selecione</option>
                            <? $sSelected = "";
                                       if($voMnyUnidade){
                                           foreach($voMnyUnidade as $oMnyUnidade){

                                               if($oMnySolicitacaoAporte){
                                                   $sSelected = ($_SESSION['Perfil']['CodUnidade'] == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";

                                                }
                                    ?>
                            <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> -
                            <?= $oMnyUnidade->getDescricao()?>
                            </option>
                            <?
                                   }
                               }
                            ?>
                    </select>
				</div>
                  <div class="col-sm-2">
                      <!--<input type="button" class="btn btn-success" value="Listar Movimentos" onclick="carregaTabela(document.getElementById('Unidade').value);">-->
                  </div>
				</div>

				<br>
             <div class="col-sm-12">
                <legend style="color:#01627F">Movimentos Liberados</legend>
                <br>
                 <div class="col-sm-12">
                     <? if( $_REQUEST['sOP'] != Pagamento){?>
                     <div class="alert alert-success">
                        <strong>Assinado!</strong> Aporte numero <?=($voVMovimentoLiberado) ? $voVMovimentoLiberado[0]->getMnySolicitacaoAporte()->getCodSolicitacao() : ""?>.
                     </div>
                    <? } ?>
                 </div>

                <div id="divTabela">
                <?php if($voVMovimentoLiberado){?>



                    <table id="liberados" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                           <thead>
                            <tr>
                                <th class="col-sm-1">Estorno</th>
                                <th class="col-sm-1">F.A</th>
                                <th class="col-sm-7">Hist&oacute;rico</th>
                                <th class="col-sm-1">Valor</th>
                                <th class="col-sm-3" style='width:27%;'>Comprovante</th>
                            </tr>
                           </thead>
                        <tbody>
                            <? $nTotalLiberado=0;
                                $i=0;
                                foreach($voVMovimentoLiberado as $oVMovimentoLiberado){
                                    $nMovCodigoMovItem = explode(".",$oVMovimentoLiberado->getMovCodigo());



                                ?>

                                    <tr <?= (in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado))? "style='background-color:rgb(246, 181, 181);'" : (($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 )? "style='background: antiquewhite;'" : "") ?>>
                                        <td >
                                              <? if($oVMovimentoLiberado->getAssinado() != 0 And $oVMovimentoLiberado->getAssinado() != 1){?>
                                                <icon class='glyphicon glyphicon-usd' style='font-size: 50px;color:yellowgreen;' data-toggle="tooltip" title="Transferência do Aporte!"></icon>
                                            <? }else{ ?>
                                                <div class="col-sm-6" style="background: #097AC3;width: auto;border-radius: 10px;">
                                                    <input name='fFaEstorno[]' type="checkbox" value='<?= $nMovCodigoMovItem[0]."||".$nMovCodigoMovItem[1]."||".$oVMovimentoLiberado->getAtivo() ?>' data-toggle="tooltip" title="Estornar item do Aporte!">
                                                </div>
                                            <!--
                                                <div class="col-sm-6">
                                                    <a href="#" onclick="recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.processaFormularioExcluiItenDoAporte&sOP=ExcluirItem&FA=<?= $nMovCodigoMovItem[0]."||".$nMovCodigoMovItem[1]?>&nUnidade=<?=$oMnyUnidade->getPlanoContasCodigo()?>&nIdMnySolicitacaoAporte=<?=$_REQUEST['nIdMnySolicitacaoAporte']?>','divTabela')">
                                                        <icon class='glyphicon glyphicon-trash' style='font-size: 26px;color:#C9302C;' data-toggle="tooltip" title="Remover item do Aporte!"></icon>
                                                    </a>
                                                </div>
                                            -->
                                            <? } ?>
                                        </td>
                                        <td <?= (in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado))? "style='background-color:rgb(246, 181, 181);'" : "" ?>>
                                            <input type="hidden" name="fFA[]" value="<?=$oVMovimentoLiberado->getMovCodigo()?>">
                                            <input type="hidden" name="fMovCodigo[]" value="<?= $nMovCodigoMovItem[0] ?>">
                                            <input type="hidden" name="fMovItem[]" value="<?= $nMovCodigoMovItem[1] ?>">
                                            <a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?//= ($oVMovimentoLiberado->getMovTipo() == 188)? "Pagar" : "Receber"?>&nIdMnyMovimentoItem=<?=$nMovCodigoMovItem[0]?>||<?= $nMovCodigoMovItem[1] ?>" data-toggle="tooltip" title="Detalhar Movimento!" target="_blank"><?= $oVMovimentoLiberado->getMovCodigo()?></a>&nbsp;
                                            <a href="relatorios/?rel=ficha_aceite_nova&mov=<?=$nMovCodigoMovItem[0]?>&item=<?=$nMovCodigoMovItem[1]?>" target="_blank" data-toggle="tooltip" title="Gerar Ficha de Aceite"><i class="glyphicon glyphicon-duplicate" style="font-size: 18px;"></i></a>
                                            <? if(in_array($oVMovimentoLiberado->getMovCodigo(),$aItemEstonado)){?>
                                            <span style="color:red;font-weight:bold;">ESTORNADO</span>
                                            <? }elseif(!(($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 ))){
                                                $nTotalLiberado += $oVMovimentoLiberado->getAtivo();
                                            ?>
                                                <!-- o link para estorno esta na conciliacao...
                                                <a  href='?action=MnyMovimentoItem.extornaItemMovimento&nCodSolicitacao=<?= $oMnySolicitacaoAporte->getCodSolicitacao()?>&fa=<?=$nMovCodigoMovItem[0]?>||<?= $nMovCodigoMovItem[1] ?>' class="btn btn-warning" style='font-size: x-small;'><icon class='glyphicon glyphicon-retweet'></icon>  Estornar</a>
                                                -->
                                            <? } ?>
                                        </td>
                                        <td><?= $oVMovimentoLiberado->getAporteItem()?></td>
                                        <td><?= number_format($oVMovimentoLiberado->getAtivo(),2,',','.')?></td>
                                        <? if(!is_null($oVMovimentoLiberado->getCodSolicitacaoAporte())){?>
                                            <td style='width:27%;'>
                                                <a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=<?= $oVMovimentoLiberado->getCodSolicitacaoAporte() ?>' title="Visualizar Comprovante de Pagamento" ><button type="button" class="btn btn-success btn-sm" style="font-size:6px;"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button></a>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?= $oMnyContratoDocTipo->getTamanhoArquivo() ?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">
                                                    </div>
                                                </div>
                                                <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                                <input type="hidden" name='fCodArquivo[]' required value="<?=$oVMovimentoLiberado->getCodSolicitacaoAporte()?>">
                                            </td>
                                        <? }else{?>

                                            <td  style='width:27%;'>
                                                <? if(($sReadOnly == 1) || ($oVMovimentoLiberado->getAssinado() != 0 && $oVMovimentoLiberado->getAssinado() != 1 )){?>
                                                   <button style=' opacity: 0.0;filter: alpha(opacity=0);' type="button" class="btn btn-success btn-sm" style="font-size:6px;"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <input type='file' id='file<?=$i?>'  onblur="verificaUpload('file<?=$i?>',<?=$oMnyContratoDocTipo->getTamanhoArquivo()?>)" style="background-color: #097AC3;border-radius: 5px;color:#FFFFFF;" data-iconName="glyphicon glyphicon-inbox" data-input="false" name="arquivo[]">
                                                        </div>
                                                    </div>
                                                    <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'><?=(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)?>MB</span>
                                                    <!--<input type="file" data-buttonText="Anexar" class="filestyle anexar" data-iconName="glyphicon glyphicon-file" data-buttonName="btn-primary" style='width:50%;' data-input="true"   id='arquivo' name='arquivo[]' />-->
                                                    <input type="hidden" name='fCodArquivo[]' required value="">
                                                <? }else{
                                                    echo "
                                                    <div class='form-group'>
                                                        <div class='col-sm-3'>
                                                            <div style='display:none;'>
                                                                 <input type=\"file\" id=\"file$i\"  onblur=\"verificaUpload('file$i',$oMnyContratoDocTipo->getTamanhoArquivo())\"  data-buttonText=\"Atualizar\" class=\"\" style='background-color: #097AC3;border-radius: 5px;color:#FFFFFF; ' data-iconName=\"glyphicon glyphicon-file\" data-buttonName=\"btn-primary\"  data-input=\"true\"  name=\"arquivo[]\" />
                                                                 <span style='background-color: #fb1e1e;border-radius: 5px; color: #fffbf5; font-weigth:bold;' data-toggle='tooltip' title='Tamnaho maximo permitido para este tipo de documento!'>(($oMnyContratoDocTipo->getTamanhoArquivo()/1024)/1024)MB</span>
                                                                <!--<input  type='file' data-buttonText='Anexar' class='filestyle anexar' data-iconName='glyphicon glyphicon-file' data-buttonName='btn-primary' style='width:50%;' data-input='true'   id='arquivo' name='arquivo[]' />-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' name='fCodArquivo[]' required value=''>";
                                                    echo "<span style='color:red;'>Aguardando comprovante da transferência</span>";
                                                   }?>
                                            </td>


                                        <? }?>

                                    </tr>
                                    <? $i++;
                                }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td ><input type="hidden" name="soma" id="somarealizado" value="0"/><div class="col-sm-4">- TOTAL LIBERADO PARA PAGAMENTO:</div><div class="col-sm-2 soma" id="totalrealizado"><span style="color:red;font-weight:bold;font-size: 250%;"><?= number_format($nTotalLiberado,2,',','.')?></span></div></td>
                            </tr>

                            <tr>
                                <td >
                                    <a href="#" data-toggle="tooltip" title="Estornar Item(s) do aporte!" onclick='confirmaOperacao(<?= $nMovCodigoMovItem[0].".".$nMovCodigoMovItem[1]?>,1);'>
                                        <span style="font-size: 26px;color: #fffdfd;background: #12749b;border-radius: 10px;font-family: initial;font-weight: bold;padding: 4px;">
                                           <icon class='glyphicon glyphicon-transfer' style='font-size: 26px;'  ></icon> Estornar Itens
                                        </span>
                                    </a>
                                    &nbsp;
                                    <!--
                                    <a href="#" data-toggle="tooltip" title="Abrir Aporte!" onclick='confirmaOperacao(<?= $nMovCodigoMovItem[0].".".$nMovCodigoMovItem[1]?>,2);'>
                                        <span style="font-size: 26px;color: #fffdfd;background: #12749b;border-radius: 10px;font-family: initial;font-weight: bold;padding: 4px;">
                                           <icon class='glyphicon glyphicon-folder-open' style='font-size: 26px;' ></icon> Abrir Aporte
                                        </span>
                                    </a>
                                     -->
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                <? }?>
             </div>
            </div>
			<br>
          </fieldset>
          <div class="form-group">
            <div class="col-sm-offset-5 col-sm-3">
                <button type="submit" class="btn btn-primary" >Gravar</button>
                <? if($sOP == 'Alterar'){?>
                    <a href="?action=MnySolicitacaoAporte.preparaFicha&sOP=Ficha&fCodSolicitacaoAporte=<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>" target="_blank"><button type="button" class="btn btn-warning" >Ficha</button></a>
                <? } ?>
            </div>
          </div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-filestyle.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
         <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">
            $('select[readonly=readonly] option:not(:selected)').prop('disabled', true);
			 jQuery(document).ready(function(){
             jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataSolicitacao").mask("99/99/9999");
				$("#Periodo1").mask("99/99/9999");
				$("#Periodo2").mask("99/99/9999");
			 });

             $(document).ready(function() {
                    oTable = $('#disponiveis').dataTable({
                    "scrollX": true,
                        dom: 'C<"clear">lfrtip',
                        "aoColumnDefs": [
                        {
                            "bSortable": false, "aTargets": [ 0 ],
                            "bSearchable": false, "aTargets": [ 0 ]
                        }
                    ]
                    });
                    oTable = $('#liberados').dataTable({
                    "scrollX": true,
                        dom: 'C<"clear">lfrtip',
                        "aoColumnDefs": [
                        {
                            "bSortable": false, "aTargets": [ 0 ],
                            "bSearchable": false, "aTargets": [ 0 ]
                        }
                    ]
                    });


                } );

			function carregaTabela(nCodUnidade){
                var unidade = nCodUnidade;
                var CodSolicitacao = document.getElementById('CodSolicitacao').value;
                recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.recuperarItensLiberadosParaPagamento&CodSolicitacaoAporte='+CodSolicitacao+'&nCodUnidade='+unidade,'divTabelaPagamento');
			}
		</script>
         <script>
             function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){
                var bValor = 1;
                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";},
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                            oDiv.innerHTML = data;
                            if(data == null){
                                bValor = 0;
                            }
                         },
                        complete:function(){
                            //$.fn.dataTable.ext.errMode = 'none'; //desabilitando WARNING sdatatabled...
                            if(bValor == 1){
                                var asInitVals = new Array();
                                $(document).ready(function() {
                                 oTable = $('#liberados').dataTable({
                                    "scrollX": true,
                                        dom: 'C<"clear">lfrtip',
                                        "aoColumnDefs": [
                                        {
                                            "bSortable": false, "aTargets": [ 0 ],
                                            "bSearchable": false, "aTargets": [ 0 ]
                                        }
                                    ]
                                    });


                                } );
                            }


                    }
                });
            }

            function calcTotais (campo){
                var valor_inicial = parseFloat(document.getElementById("SelecionadoDisponivel").value);
                   if(campo.checked){
                       var valor_final = valor_inicial + parseFloat(campo.getAttribute("val"));
                       valor_finalFormatado = valor_final.toFixed(2);
                       valor_finalFormatadoFinal = valor_finalFormatado.replace(".",",");
                       document.getElementById("SelecionadoDisponivel").value = valor_final;
                       document.getElementById("totaldisponivel").innerHTML = (valor_finalFormatadoFinal);

                       total_pagar = document.getElementById("totalPagar").value ;
                       saldo_pagar = total_pagar - valor_final;
                       saldo_pagar = saldo_pagar.toFixed(2);
                       saldo_pagarFormatado = saldo_pagar.replace(".",",");
                       document.getElementById("diferencadisponivel").innerHTML = saldo_pagarFormatado;

                   }else{

                       var valor_final = valor_inicial - parseFloat(campo.getAttribute("val"));
                       valor_finalFormatado = valor_final.toFixed(2);
                       valor_finalFormatadoFinal = valor_finalFormatado.replace(".",",");
                       document.getElementById("SelecionadoDisponivel").value = valor_final;
                       document.getElementById("totaldisponivel").innerHTML = (valor_finalFormatadoFinal);

                      total_pagar = document.getElementById("totalPagar").value ;
                       saldo_pagar = total_pagar - valor_final;
                       saldo_pagar = saldo_pagar.toFixed(2);
                       saldo_pagarFormatado = saldo_pagar.replace(".",",");
                       document.getElementById("diferencadisponivel").innerHTML = saldo_pagarFormatado;
                   }
            }
			 function confirmaOperacao(FA,op){
				$("#ModalAporte").on("shown.bs.modal", function () {
                    if(op==1){
                        document.getElementById('sOP').value="Estornar";
                        document.getElementById('msg').innerHTML = "Deseja mesmo estornar os itens selecionados do aporte ?";
                    }else{
                        document.getElementById('sOP').value="AbrirAporte";
                        document.getElementById('msg').innerHTML = "Deseja mesmo abir este aporte ?";
                    }
                    $(this).find('#ok').attr('href', "javascript:document.formMnySolicitacaoAporte.submit()");
				});
				$('#ModalAporte').modal('show');
			 }

         </script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd -->
<div id="ModalAporte" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Estorno</h4>
      </div>
      <div class="modal-body" id="msg"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalAporte').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
    </div>
    </div>
  </div>
</div>

     </html>
