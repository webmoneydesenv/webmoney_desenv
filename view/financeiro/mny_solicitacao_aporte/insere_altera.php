<?php
 $sOP = $_REQUEST['sOP'];
 $oMnySolicitacaoAporte = $_REQUEST['oMnySolicitacaoAporte'];
 $voSysEmpresa = $_REQUEST['voSysEmpresa'];
 $voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];
 $voMnyUnidade = $_REQUEST['voMnyUnidade'];
 $voMnyConta = $_REQUEST['voMnyConta'];
 $voMnyCentroNegocio = $_REQUEST['voMnyCentroNegocio'];
 $oMnyTransferencia = $_REQUEST['oMnyTransferencia'];
 $voMnyContaCorrenteOrigem = $_REQUEST['voMnyContaCorrenteOrigem'];
 $voMnyContaCorrenteDestino = $_REQUEST['voMnyContaCorrenteDestino'];
 $oMnyMovimentoOrigem = $_REQUEST['oMnyMovimentoOrigem'] ;
 $oMnyMovimentoDestino = $_REQUEST['oMnyMovimentoDestino'];


?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Solicita&ccedil;&atilde;o de Aporte - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
 <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnySolicitacaoAporte.preparaLista">Gerenciar Solicita&ccedil;&atilde;o de Aporte</a> &gt; <strong><?php echo $sOP?> Solicita&ccedil;&atilde;o de Aporte</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP ?> Solicita&ccedil;&atilde;o de Aporte</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post"   class="form-horizontal" name="formMnySolicitacaoAporte" action="?action=MnySolicitacaoAporte.processaFormulario">
         <input type="hidden" name="sOP" id="sOP" value="<?= $sOP ?>" />
         <input type="hidden" name="fCodSolicitacao" id="CodSolicitacao" value="<?=(is_object($oMnySolicitacaoAporte)) ? $oMnySolicitacaoAporte->getCodSolicitacao() : ""?>" />
         <!--<input type='hidden' name='fEmpCodigo'  value='<?//= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getEmpCodigo() : $_SESSION['Perfil']['EmpCodigo']?>'/>-->
		 <input type='hidden' name='fSolAlt'   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getSolAlt() : ""?>'/>
		 <input type='hidden' name='fNumeroAporte'   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getNumeroAporte() : ""?>'/>
		 <input type='hidden' name='fAtivo' value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getAtivo() : "1"?>'/>
		 <input type='hidden' name='fAporteItem'  value='<?= ($oMnyAporteItem) ? $oMnyAporteItem->getAporteItem() : ""?>'/>
		 <input type='hidden' id='ass' name='fAssinado'  value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getAssinado() : 0 ?>'/>
         <input  type='hidden' name='fMovObs' value='SOLICITAÇÃO DE APORTE'/>
         <input  type='hidden' name='fConCodigo' value='284'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Solicita&ccedil;&atilde;o">
          <div class="form-group">
              <div class="col-sm-6">
               <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="MnySolicitacaoAporte">Solicita&ccedil;&atilde;o:</label>
                    <div class="col-sm-4" id="divNumeroSolicitacao">
                        <?php if($oMnySolicitacaoAporte){
                            echo "<span style='color:black;font-weight:bold;font-size:18px;'>". $oMnySolicitacaoAporte->getNumeroAporte() ."</span>";
                            $sTipoCampo="hidden";
                        }?>
                      <input class="form-control" type='<?=($sTipoCampo) ? $sTipoCampo : "text"?>'  placeholder='Codigo Solicita&ccedil;&atilde;o' id='fNumeroSolicitacao' name='fNumeroSolicitacao'  required <?php echo $_REQUEST['sSomenteLeitura']?>   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getNumeroSolicitacao() : ""?>' readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
                    <div class="col-sm-8">
                            <select name='fNegCodigo'  class="form-control chosen" required   >
                       <option value=''>Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyCentroNegocio){
                                       foreach($voMnyCentroNegocio as $oMnyCentroNegocio){
                                           if($oMnyMovimentoOrigem){
                                               $sSelected = ($oMnyMovimentoOrigem->getNegCodigo() == $oMnyCentroNegocio->getPlanoContasCodigo()) ? "selected" : "";
                                           }
                                ?>
                        <option  <?= $sSelected?> value='<?= $oMnyCentroNegocio->getPlanoContasCodigo()?>'><?= $oMnyCentroNegocio->getCodigo()?> - <?= $oMnyCentroNegocio->getDescricao()?>
                        </option>
                        <?
                                       }
                                   }
                                ?>
                      </select>
                    </div>
                </div>

              </div>
              <div class="col-sm-6">
                   <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="DataSolicitacao">Data</label>
                        <div class="col-sm-4">
                                <input class="form-control" type='text' id='DataSolicitacao' placeholder='Data' required name='fDataSolicitacao'   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getDataSolicitacaoFormatado() : ""?>'/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type='text' id='MovDocumento' placeholder='Documento' name='fMovDocumento'  required   value='<?= ($oMnyMovimentoOrigem) ? $oMnyMovimentoOrigem->getMovDocumento() : ""?>'/>
                        </div>
                    </div>
              </div>

            </div>

             <!--Inicio Transferencia-->

                   <div class="form-group">
                        <div class="col-sm-6">
                     <legend>Origem</legend>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="text-align:left" for="Empresa">Empresa:</label>
                            <div class="col-sm-10">
                                <? if($_REQUEST['sOP'] == "Cadastrar"){?>
                                <span style="font-weight:bold;"><?= $_SESSION['oEmpresa']->getEmpFantasia();?></span>
                                <? }else{ ?>
                                   <span style="font-weight:bold;"><?= $oMnyMovimentoOrigem->getSysEmpresa()->getEmpFantasia();?></span>
                               <? } ?>
                            </div>
                        </div>

                       <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                           <div class="col-sm-10">
                           <select name='fUniCodigo' id="Unidade" class="form-control chosen" onChange="recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipoConta&fUniCodigo='+this.value,'divContaDe')"  required  <?= $_REQUEST['sSomenteLeitura']?>>
                            <option value=''>Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                               $sSelected = (($oMnySolicitacaoAporte) &&($oMnySolicitacaoAporte->getUnidadeAporte() == $oMnyUnidade->getPlanoContasCodigo()) && $_REQUEST['sOP'] == "Alterar") ? "selected" : "";
                                ?>
                        <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> -<?= $oMnyUnidade->getDescricao()?></option>
                        <?
                                       }
                                   }
                                ?>
                      </select>
                      </div>
                      </div>
                        <div class="form-group">
                      <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">De:</label>
                        <div class="col-sm-10">
                            <? //if($_REQUEST['sOP'] != "Alterar"){?>
                                <div id="divContaDe">
                                    <select name='fContaDe'  id='de' class="form-control chosen" required >
                                        <option value=''>Selecione</option>
                                        <?php foreach($voMnyContaCorrenteOrigem as $oMnyContaCorrente){
                                         if($oMnyTransferencia){
                                               $sSelected = ($oMnyTransferencia->getDe() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
                                              }
                                           ?>
                                            <option <?= $sSelected?>  value="<?php echo $oMnyContaCorrente->getCcrCodigo()?>"><?php echo $oMnyContaCorrente->getCcrConta()?> - <?= $oMnyContaCorrente->getBanCodigo()?></option>
                                            <? } ?>
                                    </select>
                                </div>
                            <? //}else{
                              //  echo '<span style="color:red;font-weight:bold;font-size:18px;">' .(($oMnyTransferencia)? $oMnyTransferencia->getMnyContaCorrenteOrigem()->getCcrContaFormatada() : "") . '</span>';
                            //} ?>

                        </div>
                    </div>
                   </div>
                   <div class="col-sm-6">
                       <legend>Destino</legend>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                      <div class="col-sm-10" id="divUnidadeDestino">
                           <select name='fUniCodigoPara'  class="form-control chosen"  required onchange="recuperaConteudoDinamico('index.php','action=SysEmpresa.carregaEmpresaPorUnidade&nCodUnidade='+this.value,'divEmpresa')"   <?= $_REQUEST['sSomenteLeitura']?>>
                           <!--<select name='fUniCodigoPara'  class="form-control chosen"  required onchange="carregaTabela(this.value,1)">-->
                           <!--<select name='fUniCodigoPara'  class="form-control chosen"  required >-->
                            <option value=''>Selecione</option>
                        <? $sSelected = "";
                                   if($voMnyUnidade){
                                       foreach($voMnyUnidade as $oMnyUnidade){
                                           if($oMnyMovimentoDestino){
                                               $sSelected = ($oMnyMovimentoDestino->getUniCodigo() == $oMnyUnidade->getPlanoContasCodigo()) ? "selected" : "";
                                           }
                                ?>
                        <option  <?= $sSelected?> value='<?= $oMnyUnidade->getPlanoContasCodigo()?>'><?= $oMnyUnidade->getCodigo()?> -
                        <?= $oMnyUnidade->getDescricao()?>
                        </option>
                        <?
                                       }
                                   }
                                ?>
                      </select>
                      </div>
                       </div>
                  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Empresa:</label>
                      <div id="divEmpresa">
                        <div class="col-sm-8" >
                            <select name='fEmpCodigoPara' id='fEmpCodigoPara' class="form-control chosen" required  <?= $_REQUEST['sSomenteLeitura']?>>
                                <option value=''>Selecione</option>
                                <?php foreach($voSysEmpresa as $oSysEmpresa){
                                        $sSelected = (($oMnyMovimentoDestino) && ($oMnyMovimentoDestino->getEmpCodigo() == $oSysEmpresa->getEmpCodigo())) ? " selected " : "";
                                ?>
                                    <option <?=$sSelected?> value="<?php echo $oSysEmpresa->getEmpCodigo()?>"><?php echo $oSysEmpresa->getEmpFantasia()?></option>
                                    <? } ?>
                            </select>
                        </div>
                       </div>

                    </div>


                      <div class="form-group">

                        <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Para:</label>
                        <div class="col-sm-10">
                            <? //if($_REQUEST['sOP'] != "Alterar"){?>
                                <div id="divContaPara">
                                    <select name='fContaPara' id='para' class="form-control chosen" required >
                                        <option value=''>Selecione</option>
                                        <?php foreach($voMnyContaCorrenteDestino as $oMnyContaCorrente){
                                                 if($oMnyTransferencia){
                                                   $sSelected = ($oMnyTransferencia->getPara() == $oMnyContaCorrente->getCcrCodigo()) ? "selected" : "";
                                                 }
                                           ?>
                                            <option <?= $sSelected?> value="<?php echo $oMnyContaCorrente->getCcrCodigo()?>"><?php echo $oMnyContaCorrente->getCcrConta()?> - <?= $oMnyContaCorrente->getBanCodigo()?></option>
                                            <? } ?>
                                    </select>
                                </div>
                            <? //}else{
                              //  echo '<span style="color:#087EC0;font-weight:bold;font-size:18px;">' .(($oMnyTransferencia)? $oMnyTransferencia->getMnyContaCorrenteDestino()->getCcrContaFormatada() : "") . '</span>';
                               // }?>
                        </div>
                    </div>
                </div>
                <p>
                <div id="divTabela">
                   <? if($sOP == "Alterar"){
                        include_once('view/financeiro/mny_solicitacao_aporte/tabela_ajax.php');
                      } ?>
                </div>
				<br>



          <div class="form-group">
          </fieldset>
     	<div class="col-sm-offset-5 col-sm-3">
            <button type="submit" id='btnSalvar' class="btn btn-primary" >Gravar</button>
           <!-- <button type="submit" id='btnSalvar' class="btn btn-primary">Gravar</button>-->
            <? if($sOP == 'Alterar' && ($oMnySolicitacaoAporte->getLiberado() == 1)){?>
                <a href="?action=MnySolicitacaoAporte.preparaFicha&sOP=Ficha&fCodSolicitacaoAporte=<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>" target="_blank"><button type="button" class="btn btn-warning" >Ficha</button></a>
            <? } ?>
            <? if($_REQUEST['sOP'] == 'Cadastrar'){?>
               <!-- <input type="button"  class="btn btn-success" value="Listar" onclick="carregaTabela(document.getElementById('Unidade').value,2);"> -->
            <? }?>

     	</div>
   		</div>
       </form>

        <div id="ModalAporte" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Estorno</h4>
              </div>
              <div class="modal-body" id="msg"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                <a href="" onClick="javascript:$('#ModalAporte').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>
            </div>
            </div>
          </div>
        </div>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
         <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">

            $('select[readonly=readonly] option:not(:selected)').prop('disabled', true);
			 jQuery(document).ready(function(){
             jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				$("#DataSolicitacao").mask("99/99/9999");

                $("#totaldisponivel").maskMoney(	{symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
                $("#fMovValor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#fMovJuros").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
                $("#fMovRetencao").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
			 });
             $(document).ready(function() {
                    oTable = $('#disponiveis').dataTable({
                    "scrollX": true,
                        dom: 'C<"clear">lfrtip',
                        "aoColumnDefs": [
                        {
                            "bSortable": false, "aTargets": [ 0 ],
                            "bSearchable": false, "aTargets": [ 0 ]
                        }
                    ]
                    });
                    oTable = $('#liberados').dataTable({
                    "scrollX": true,
                        dom: 'C<"clear">lfrtip',
                        "aoColumnDefs": [
                        {
                            "bSortable": false, "aTargets": [ 0 ],
                            "bSearchable": false, "aTargets": [ 0 ]
                        }
                    ]
                    });


                } );


			 function confirmaOperacao(FA,op){
				$("#ModalAporte").on("shown.bs.modal", function () {
                    if(op==1){
                        document.getElementById('sOP').value="Estornar";
                        document.getElementById('msg').innerHTML = "Deseja mesmo estornar os itens selecionados do aporte ?";
                    }else{
                        document.getElementById('sOP').value="AbrirAporte";
                        document.getElementById('msg').innerHTML = "Deseja mesmo abir este aporte ?";
                    }
                    $(this).find('#ok').attr('href', "javascript:document.formMnySolicitacaoAporte.submit()");
				});
				$('#ModalAporte').modal('show');
			 }

			function carregaTabela(nCodUnidade,n){

                var data = document.getElementById('DataSolicitacao').value;
                var empresa =document.getElementById('fEmpCodigoPara').value;
                var dataFormat = data.split('/');
                if(n==2){
                    recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.recuperarItensPagamento&nCodUnidade='+nCodUnidade+'&nNumeroSolicitacaoAporte='+nSolicitacao,'divTabela');
                }else{
                    setTimeout(function(){
                        recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.preparaProximoNumeroSolicitacao&fUniCodigo='+nCodUnidade+'&nAno='+dataFormat[2],'divNumeroSolicitacao');
                    }, 2000);
                    setTimeout(function(){
                        recuperaConteudoDinamico('index.php','action=MnyMovimento.carregaTipoConta&fUniCodigo1='+nCodUnidade,'divContaPara');
                    }, 3000);
                    setTimeout(function(){
                        recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.recuperarItensPagamento&nCodUnidade='+nCodUnidade+'&nNumeroSolicitacaoAporte='+document.getElementById('nSolicitacao').value,'divTabela');
                        //recuperaConteudoDinamico('index.php','action=MnySolicitacaoAporte.recuperarItensPagamento&nCodUnidade='+nCodUnidade,'divTabela');
                        //location.href();
                        //window.location.href = '?action=MnySolicitacaoAporte.recuperarItensPagamento&sOP=Cadastrar&nCodUnidade=200&nNumeroSolicitacaoAporte=999999';
                    }, 4000);

                }
			}


             function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){
                var bValor = 1;
                oDiv = document.getElementById(sIdDivInsert);
                sArquivo = sArquivo+'?'+sParametros;
                $.ajax({
                        dataType: "html",
                        type: "GET",
                        beforeSend: function(oXMLrequest){oDiv.innerHTML ="<div class='col-sm-12'><img src='imagens/ajax-loader-6.gif' title='img/ajax-loaders/ajax-loader-6.gif'></img> Carregando...</div> ";},
                        url: sArquivo,
                        error: function(oXMLRequest,sErrorType){
                                                alert(oXMLRequest.responseText);
                                                alert(oXMLRequest.status+' , '+sErrorType);
                                           },
                        success: function(data){
                            oDiv.innerHTML = data;
                            if(data == null){
                                bValor = 0;
                            }
                         },
                        complete:function(){
                            //$.fn.dataTable.ext.errMode = 'none'; //desabilitando WARNING sdatatabled...
                            if(bValor == 1){
                                var asInitVals = new Array();
                                $(document).ready(function() {
                                    oTable = $('#disponiveis').dataTable({
                                    "scrollX": true,
                                        dom: 'C<"clear">lfrtip'
                                    });
                                    oTable = $('#liberados').dataTable({
                                    "scrollX": true,
                                        dom: 'C<"clear">lfrtip'
                                    });


                                } );
                            }


                    }
                });
            }




            function calcTotais (campo){
                var valor_inicial = parseFloat(document.getElementById("SelecionadoDisponivel").value);
                   if(campo.checked){
                       var valor_final = valor_inicial + parseFloat(campo.getAttribute("val"));
                       valor_finalFormatado = valor_final.toFixed(2);
                       valor_finalFormatadoFinal = valor_finalFormatado.replace(".",",");
                       document.getElementById("SelecionadoDisponivel").value = valor_final;
                       document.getElementById("totaldisponivel").innerHTML = (valor_finalFormatadoFinal);

                       total_pagar = document.getElementById("totalPagar").value ;
                       saldo_pagar = total_pagar - valor_final;
                       saldo_pagar = saldo_pagar.toFixed(2);
                       saldo_pagarFormatado = saldo_pagar.replace(".",",");
                       document.getElementById("diferencadisponivel").innerHTML = saldo_pagarFormatado;

                   }else{

                       var valor_final = valor_inicial - parseFloat(campo.getAttribute("val"));
                       valor_finalFormatado = valor_final.toFixed(2);
                       valor_finalFormatadoFinal = valor_finalFormatado.replace(".",",");
                       document.getElementById("SelecionadoDisponivel").value = valor_final;
                       document.getElementById("totaldisponivel").innerHTML = (valor_finalFormatadoFinal);

                      total_pagar = document.getElementById("totalPagar").value ;
                       saldo_pagar = total_pagar - valor_final;
                       saldo_pagar = saldo_pagar.toFixed(2);
                       saldo_pagarFormatado = saldo_pagar.replace(".",",");
                       document.getElementById("diferencadisponivel").innerHTML = saldo_pagarFormatado;
                   }
            }

           function habilitaButton(){
                var check = document.getElementsByName('fMovCodigo[]');
                for (var i=0;i<check.length;i++){
                    if (check[i].checked == true){
                         document.getElementById('btnSalvar').disabled = false;
                           return false;
                    }  else {
                        document.getElementById('btnSalvar').disabled = true;
                    }
                }
            }


            function preparaJuros(sLink,n){
				$("#ModalJuros").on("shown.bs.modal", function () {
                    var CodSolicitacao = document.getElementById('CodSolicitacao').value;
					switch(n){
						case 1:
							$(this).find('#ok').attr('href', "javascript:document.formMnySolicitacaoAporte.submit()");
							document.getElementById('msg').innerHTML = "Deseja mesmo realizar esta opera&ccedil;&atilde;o ?";
						break;
						case 2:
							recuperaConteudoDinamico('index.php',sLink+'&nCodSolicitacao='+CodSolicitacao,'divJuros');
						break;
					}
				});
				$('#ModalJuros').modal('show');
			 }

           </script>

       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
 <!-- Modal -->
<div id="ModalJuros" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Juros </h4>
      </div>
      <div class="modal-body" id="divJuros"></div>
      <div class="modal-footer">
      <!--  <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
        <a href="" onClick="javascript:$('#ModalJuros').modal('hide')" class="btn btn-primary btn-ok" id="ok" >Sim</a>-->
    </div>
    </div>
  </div>
</div>
