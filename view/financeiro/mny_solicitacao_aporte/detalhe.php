<?php
 $sOP = $_REQUEST['sOP'];
 $oMnySolicitacaoAporte = $_REQUEST['oMnySolicitacaoAporte'];
 $voMnyAporteItem = $_REQUEST['voMnyAporteItem'];
 $oMnyMovimentoDestino = $_REQUEST['oMnyMovimentoDestino'];

/*  print_r(  $voMnyAporteItem);
  die();*/
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Solicita&ccedil;&atilde;o Aporte - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnySolicitacaoAporte.preparaLista">Gerenciar  Solicita&ccedil;&atilde;o Aporte</a> &gt; <strong><?php echo $sOP?>  Solicita&ccedil;&atilde;o Aporte</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Solicita&ccedil;&atilde;o Aporte</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">

<div class="form-group">
	 <div class="col-sm-6">
     <legend>Dados Gerais</legend>
         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Data Solicita&ccedil;&atilde;o:&nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getDataSolicitacaoFormatado() : ""?></div>
                        </div>
                        <br>
                        <br>
         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Unidade:&nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getMnyPlanoContasUnidade()->getDescricao() : ""?></div>
                        </div>
                        <br>

         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Periodo de :&nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getPeriodo1Formatado() : ""?>&nbsp;<strong> à &nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getPeriodo2Formatado() : ""?></div>
                        </div>
                        <br>
         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Incluido Por:&nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getSolInc() : ""?></div>
                        </div>
                        <br>

         <div class="form-group">
                             <div class="col-sm-12" style="text-align:left"><strong>Alterado Por:&nbsp;</strong><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getSolAlt() : ""?></div>
                        </div>
                        <br>
                        </div>

     </div>
      <div class="col-sm-6">
      <legend>Dados do Item</legend>
        <? if( $voMnyAporteItem ){ ?>
          <table class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Mov.Item</th>
                    <th>Hist&oacute;rico</th>
                    <th>Documento</th>
                    <th>Valor</th>
                </tr>
             </thead>
             <tbody>
                <tr>
                 <? foreach( $voMnyAporteItem as  $oMnyAporteItem ){?>
                    <tr>
                        <td><?= $oMnyAporteItem->getMovCodigo()?>.<?= $oMnyAporteItem->getMovItem()?></td>
                        <td><?= $oMnyAporteItem->getMovObs()?></td>
                        <td><?= $oMnyAporteItem->getMovDocumento()?></td>
                        <td><?= $oMnyAporteItem->getMovValor()?></td>
                    <tr>
                 <? } ?>
                <tr>
             </tbody>
        </table>
		<? }?>
      </div>
</div>


     </div>


         <br/>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnySolicitacaoAporte.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
