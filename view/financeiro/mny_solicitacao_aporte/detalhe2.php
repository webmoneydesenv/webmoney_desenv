<?php
 $sOP = $_REQUEST['sOP'];
 $oMnySolicitacaoAporte = $_REQUEST['oMnySolicitacaoAporte'];
 $voAporteItem = $_REQUEST['voMnyAporteItem'];
 $voAporteItemEstorno = $_REQUEST['voMnyAporteItemEstorno'];

 $oMnyTransferencia = $_REQUEST['oMnyTransferencia'];
 $oMnyMovimento = $_REQUEST['oMnyMovimentoOrigem'];
 $oMnyMovimentoDestino = $_REQUEST['oMnyMovimentoDestino'];
 $oMnyMovimentoOrigem = $_REQUEST['oMnyMovimentoOrigem'];

?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Solicita&ccedil;&atilde;o de Aporte - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
 <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
 <link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
 <link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnySolicitacaoAporte.preparaLista">Gerenciar Solicita&ccedil;&atilde;o de Aporte</a> &gt; <strong><?php echo $sOP?> Solicita&ccedil;&atilde;o de Aporte</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Detalhar Solicita&ccedil;&atilde;o de Aporte</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post"   class="form-horizontal" name="formMnySolicitacaoAporte" action="?action=MnySolicitacaoAporte.processaFormulario">
         <input type="hidden" name="sOP" value="<?= $sOP ?>" />
         <input type="hidden" name="fCodSolicitacao" id="CodSolicitacao" value="<?=(is_object($oMnySolicitacaoAporte)) ? $oMnySolicitacaoAporte->getCodSolicitacao() : ""?>" />
		 <input type='hidden' name='fSolAlt'   value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getSolAlt() : ""?>'/>
		 <input type='hidden' name='fNumeroSolicitacao'  value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getNumeroSolicitacao() : ""?>'/>
		 <input type='hidden' id='ass' name='fAssinado'  value='<?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getAtivo() : 0 ?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Solicita&ccedil;&atilde;o">
          <div class="form-group">
              <div class="col-sm-6">
               <div class="form-group">
                    <label class="col-sm-3 control-label" style="text-align:left" for="MnySolicitacaoAporte">N&deg; Solicita&ccedil;&atilde;o:</label>
                    <div class="col-sm-8"><span style='color:black;font-weight:bold;font-size:18px;'><?=$oMnySolicitacaoAporte->getNumeroAporteFormatado($oMnySolicitacaoAporte->getUnidadeAporte())?></span></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" style="text-align:left" for="MnyCentroNegocio">C.Negocio:</label>
                    <div class="col-sm-8"><?=($oMnyMovimentoDestino) ? $oMnyMovimentoDestino->getMnyPlanoContasNegocio()->getDescricao():""?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" style="text-align:left" >Status:</label>
                    <div class="col-sm-8"><?= ($oMnySolicitacaoAporte->getLiberado() == 0)? "<span style='color:#0881BD;font-weight:bold;size:18px;'>ABERTA</span>" : "<span style='color:red;font-weight:bold;size:18px;'>FECHADA</span>"?></div>
                </div>
              </div>
              <div class="col-sm-6">
                   <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="DataSolicitacao">Data</label>
                        <div class="col-sm-4"><?= ($oMnySolicitacaoAporte) ? $oMnySolicitacaoAporte->getDataSolicitacaoFormatado() : ""?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Histórico">Histórico:</label>
                        <div class="col-sm-8"><?= ($oMnyMovimentoDestino) ? $oMnyMovimentoDestino->getMovObs() : ""?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Documento">Documento:</label>
                        <div class="col-sm-8"><?= ($oMnyMovimentoDestino) ? $oMnyMovimentoDestino->getMovDocumento() : ""?>
                        </div>
                    </div>

              </div>

            </div>

             <!--Inicio Transferencia-->

                   <div class="form-group">
                        <div class="col-sm-6">
                     <legend>Origem</legend>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="text-align:left" for="Empresa">Empresa:</label>
                            <div class="col-sm-10">
                                <span style="font-weight:bold;"><?= ($oMnyMovimentoOrigem) ? $oMnyMovimentoOrigem->getSysEmpresa()->getEmpRazaoSocial():""?></span>
                            </div>
                        </div>

                       <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                           <div class="col-sm-10"><?=($oMnyMovimentoDestino) ? $oMnyMovimentoOrigem->getMnyPlanoContasUnidade()->getDescricao():"" ?> </div>
                      </div>
                        <div class="form-group">
                      <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">De:</label>
                        <div class="col-sm-10"><?=($oMnyTransferencia) ? $oMnyTransferencia->getMnyContaCorrenteOrigem()->getCcrContaFormatada():""; ?></div>
                        </div>
                    </div>

                   <div class="col-sm-6">
                       <legend>Destino</legend>
                  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Empresa:</label>
                        <div class="col-sm-10"><span style="font-weight:bold;"><?= ($oMnyMovimentoDestino) ? $oMnyMovimentoDestino->getSysEmpresa()->getEmpRazaoSocial():""?></span>

                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidade">Unidade:</label>
                      <div class="col-sm-10"><?=($oMnyMovimentoDestino) ? $oMnyMovimentoDestino->getMnyPlanoContasUnidade()->getDescricao():"" ?> </div>
                       </div>

                      <div class="form-group">

                        <label class="col-sm-2 control-label" style="text-align:left" for="Pessoa">Para:</label>
                        <div class="col-sm-10"><?=($oMnyTransferencia) ? $oMnyTransferencia->getMnyContaCorrenteDestino()->getCcrContaFormatada():""; ?>

                        </div>
                    </div>
                </div>
                <p>

              <div class="col-sm-12">
                <legend style="color:#01627F">Movimentos </legend>
                <br>

                <?php if($voAporteItem){?>
                    <table id="itens" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                           <thead>
                            <tr>
                                <th class="col-sm-1" rowspan="2">F.A</th>
                                <th class="col-sm-5" rowspan="2" >Favorecido</th>
                                <th class="col-sm-4" rowspan="2" >Hist&oacute;rico</th>
                                <th class="col-sm-1" rowspan="2">Parcela</th>
                                <th class="col-sm-1" rowspan="2">Valor</th>
                                <th class="col-sm-1" rowspan="2">Comprovante</th>
                                <th class="col-sm-1" colspan="4">Conciliação</th>
                                <!--
                                <th class="col-sm-3">Comprovante</th>-->
                            </tr>
                            <tr >

                                <th class="col-sm-1" >Data</th>
                                <th class="col-sm-1" > Conta</th>
                                <th class="col-sm-1" >Extrato</th>
                            </tr>
                           </thead>
                        <tbody>
                            <? $nTotalLiberado=0;
                                $i=0;
                                foreach($voAporteItem as $oAporteItem){
                            ?>
                                    <tr>
                                        <td><a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?//= ($oVMovimentoLiberado->getMovTipo() == 188)? "Pagar" : "Receber"?>&nIdMnyMovimentoItem=<?=$oAporteItem->mov_codigo?>||<?= $oAporteItem->mov_item ?>" data-toggle="tooltip" title="Detalhar Movimento!" target="_blank"><span style='font-weight:bold;'><?= $oAporteItem->FA?></span></a>&nbsp;</td>
                                        <td><?= $oAporteItem->nome?></td>
                                        <td><?= $oAporteItem->mov_obs?></td>
                                        <td><?= $oAporteItem->parcela?></td>
                                        <td><span style='color:#c9302c;font-weight:bold;'><?= number_format($oAporteItem->valor_liquido,2,",",".")?></span></td>
                                        <td><a href='?action=SysArquivo.preparaArquivo&fCodArquivo=<?= $oAporteItem->cod_arquivo ?>' target="_blank" data-toggle="tooltip" title="Visualizar Comprovante de Pagamento!"><input type="button" class="btn-primary" value="Abrir" style='border-radius: 10px;'></a></td>
                                        <td><?= $oAporteItem->data_conciliacao ?></td>
                                        <td><?= $oAporteItem->conta_conciliacao ?></td>
                                        <td>
                                            <? if($oAporteItem->extrato_bancario != '-'){?>
                                                <a href='<?= $oAporteItem->extrato_bancario ?>' target="_blank" data-toggle="tooltip" title="Visualizar Extrato Bancario!" ><input type="button" class="btn-primary" value="Abrir" style='border-radius: 10px;'></a>
                                            <? }else{
                                                echo "-";
                                            }?>
                                        </td>

                                        <? //if(!is_null($oAporteItem->getMovItem())){?>
                                        <!--
                                            <td>
                                                    <a target='_blank' href='<? //=$oVMovimentoLiberado->getMovItem()?>' title="Visualizar Comprovante de Pagamento"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button></a>
                                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="11">
                                            </td>
                                        -->
                                        <? //}?>
                                    </tr>
                                    <? $nTotalLiberado += $oAporteItem->valor_liquido;
                                    $i++;
                                }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"> <a href="?action=MnySolicitacaoAporte.preparaFicha&sOP=Ficha&fCodSolicitacaoAporte=<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>" title="Solicita&ccedil;&atilde;o Excel" target="_blank"><i class="glyphicon glyphicon-file" style='font-size: 26px;color:#66CC00'>Excel</i></a></td>
                                <td>TOTAL APORTE:</td>
                                <td colspan="5"><div class="col-sm-2 soma" id="totalrealizado"><span style="color:red;font-weight:bold;font-size: 150%;"><?= number_format($nTotalLiberado,2,',','.')?></span></div></td>
                            </tr>
                        </tfoot>
                    </table>
                <? }?>
            </div>
                </div>
				<br>
        <div class="form-group">
          <div class="col-sm-12">
              <?php if($voAporteItemEstorno){?>
                <legend style="color:#01627F">Movimentos Estornados</legend>
                <br>


                    <table id="itens" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
                           <thead>
                            <tr>
                                <th class="col-sm-1" rowspan="2">F.A</th>
                                <th class="col-sm-5" rowspan="2" >Favorecido</th>
                                <th class="col-sm-4" rowspan="2" >Hist&oacute;rico</th>
                                <th class="col-sm-1" rowspan="2">Parcela</th>
                                <!--<th class="col-sm-1" rowspan="2">Valor</th>-->

                                <th class="col-sm-1" colspan="4">Transferência</th>
                                <!--
                                <th class="col-sm-3">Comprovante</th>-->
                            </tr>
                            <tr >

                                <th class="col-sm-1" >Movimentos</th>
                                <th class="col-sm-1" > De</th>
                                <th class="col-sm-1" >Para</th>
                            </tr>
                           </thead>
                        <tbody>
                            <? $nTotalLiberado=0;
                                $i=0;
                                foreach($voAporteItemEstorno as $oAporteItemEstorno){
                            ?>
                                    <tr>
                                        <td><a href="?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=<?//= ($oVMovimentoLiberado->getMovTipo() == 188)? "Pagar" : "Receber"?>&nIdMnyMovimentoItem=<?=$oAporteItemEstorno->mov_codigo?>||<?= $oAporteItemEstorno->mov_item ?>" data-toggle="tooltip" title="Detalhar Movimento!" target="_blank"><span style='font-weight:bold;'><?= $oAporteItemEstorno->FA?></span></a>&nbsp;</td>
                                        <td><?= $oAporteItemEstorno->nome?></td>
                                        <td><?= $oAporteItemEstorno->mov_obs?></td>
                                        <td><?= $oAporteItemEstorno->parcela?></td>
                                        <!--<td><span style='color:#c9302c;font-weight:bold;'><?= number_format($oAporteItemEstorno->valor_liquido,2,",",".")?></span></td>-->

                                        <td></td>
                                        <td></td>
                                        <td>
                                            <? if($oAporteItemEstorno->extrato_bancario != '-'){?>
                                                <a href='<?= $oAporteItemEstorno->extrato_bancario ?>' target="_blank" data-toggle="tooltip" title="Visualizar Extrato Bancario!" ><input type="button" class="btn-primary" value="Abrir" style='border-radius: 10px;'></a>
                                            <? }else{
                                                echo "-";
                                            }?>
                                        </td>

                                        <? //if(!is_null($oAporteItem->getMovItem())){?>
                                        <!--
                                            <td>
                                                    <a target='_blank' href='<? //=$oVMovimentoLiberado->getMovItem()?>' title="Visualizar Comprovante de Pagamento"><button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-folder-open">&nbsp;Abrir</i></button></a>
                                                    <input type="hidden" name='fCodContratoDocTipo[]' required value="11">
                                            </td>
                                        -->
                                        <? //}?>
                                    </tr>
                                    <? $nTotalLiberado += $oAporteItemEstorno->valor_liquido;
                                    $i++;
                                }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">TOTAL ESTORNADO DO APORTE:&nbsp;<span style="color:red;font-weight:bold;font-size: 150%;"><?= number_format($voAporteItemEstorno[0]->total_estorno,2,',','.')?></span></td>
                                <!--<td colspan="1"><div class="col-sm-2 soma" id="totalrealizado"><span style="color:red;font-weight:bold;font-size: 150%;"><?//= number_format($voAporteItemEstorno[0]->total_estorno,2,',','.')?></span></div></td>-->
                            </tr>
                        </tfoot>
                    </table>
                <? }?>
            </div>
        </div>

        <div class="form-group">

            <div class="col-sm-offset-5 col-sm-3">
                <a href="?action=MnySolicitacaoAporte.preparaLista"><button type="button" class="btn btn-primary" >Voltar</button></a>

            </div>
   		</div>
    </fieldset>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
         <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
 		<script type="text/javascript" language="javascript">

             $(document).ready(function() {
                    oTable = $('#itens').dataTable();

                } );


		</script>


       <!-- InstanceEndEditable -->

   <!-- end .content -->
   </div>
</section>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
