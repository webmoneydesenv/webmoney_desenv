<?php

 $voMnySolicitacaoAporte = $_REQUEST['voMnySolicitacaoAporte'];
 unset($_SESSION['oMnyMov']);
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Lista De Solicita&ccedil;&atilde;o de Aporte</title>
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --> <strong><a href="?action=MnySolicitacaoAporte.preparaLista">Gerenciar Solicita&ccedil;&otilde;es de Aportes</a> </strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Gerenciar Solicita&ccedil;&atilde;o de Aporte</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
 <form method="post" action="" name="formMnySolicitacaoAporte" id="formMnySolicitacaoAporte" class="formulario">
   			<table width="100%" cellpadding="2" cellspacing="2" border="0">
   				<tr>
   					<td align="left"><select class="Acoes form-control" name="acoes" id="acoesMnySolicitacaoAporte" onChange="JavaScript: submeteForm('MnySolicitacaoAporte')">
   						<option value="" selected>A&ccedil;&otilde;es... </option>
   						<option value="?action=MnySolicitacaoAporte.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Solicita&ccedil;&atilde;o de Aporte</option>
   						<? if($_SESSION['Perfil']['CodGrupoUsuario'] == 10){?>
                            <option value="?action=MnySolicitacaoAporte.preparaFormulario&sOP=Pagamento" lang="1">Inserir Comprovantes de Pagamento</option>
                        <? } ?>
   						<option value="?action=MnySolicitacaoAporte.preparaFormulario&sOP=Alterar" lang="1">Alterar Solicita&ccedil;&atilde;o de Aporte selecionado</option>
   						<option value="?action=MnySolicitacaoAporte.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Solicita&ccedil;&atilde;o de Aporte selecionado</option>
   						<option value="?action=MnySolicitacaoAporte.processaFormulario&sOP=Excluir" lang="2">Excluir Solicita&ccedil;&atilde;o de Aporte(s) selecionado(s)</option>
   					</select></td>
                      <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
   				</tr>
   			</table><br/>
        <div class="form-group">
            <div class="col-sm-6">
                <?
                    if($_REQUEST['nSituacao']==1){
                        $nSituacao=0;
                        $sDescricao="Em Andamento";
                        $sLabel="primary";
                    }else{
                        $nSituacao=1;
                        $sDescricao="Finalizados";
                        $sLabel="danger";
                    }
                ?>
                <a href="?action=MnySolicitacaoAporte.preparaLista&nSituacao=<?=$nSituacao?>"><span class="label label-<?=$sLabel?>">Aportes <?=$sDescricao?></span></a>
            </div>
     </div>
   			<table id="lista" class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
   			<?php if(is_array($voMnySolicitacaoAporte)){?>
   				<thead>
   				<tr>
   					<th width="1%"><img onClick="javascript: marcarTodosCheckBoxFormulario('MnySolicitacaoAporte')" src="view/imagens/checkbox.gif" width="16" height="16"></th>
					<th class='Titulo'>&nbsp;</th>
   					<th class='Titulo'>Codigo</th>
					<th class='Titulo'>Data solicita&ccedil;&atilde;o</th>
					<th class='Titulo'>Empresa</th>
					<th class='Titulo'>Unidade</th>
					<th class='Titulo'>Situação</th>
					<th class='Titulo'>Valor</th>
   				</tr>
   				</thead>
   				<tbody>
                   <?php foreach($voMnySolicitacaoAporte as $oMnySolicitacaoAporte){ ?>
   				<tr>
  					<td ><input onClick="JavaScript: atualizaAcoes('MnySolicitacaoAporte')" type="checkbox" value="<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>" name="fIdMnySolicitacaoAporte[]"/></td>
  					<td>
                        <? if($oMnySolicitacaoAporte->getLiberado() == 1){?>
                            <a href="relatorios/?rel=aporte&cod_aporte=<?= $oMnySolicitacaoAporte->getCodSolicitacao()?>" target="_blank" data-toggle="tooltip" title="Solicita&ccedil;&atilde;o PDF"><i class="glyphicon glyphicon-duplicate" style="font-size: 20px;"></i></a>&nbsp;
                            <a  href="#"  onClick="geraFichaTransf(<?= $oMnySolicitacaoAporte->getSolInc() ?>, <?= $oMnySolicitacaoAporte->getAssinado()?>);" title="Gerar Ficha de Transferência"><i class="glyphicon glyphicon glyphicon-file" style="font-size: 20px;"></i></a>
                           [<a href='?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=<?=$oMnySolicitacaoAporte->getSolInc()?>||98' target="_blank"><?= $oMnySolicitacaoAporte->getSolInc().'.98 | '?> <a href='?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Receber&nIdMnyMovimentoItem=<?=$oMnySolicitacaoAporte->getAssinado()?>||99' target="_blank"><?= $oMnySolicitacaoAporte->getAssinado().'.99'?></a>]



                        <!-- <a href="?action=MnySolicitacaoAporte.preparaFicha&sOP=Ficha&fCodSolicitacaoAporte=<?=$oMnySolicitacaoAporte->getCodSolicitacao()?>" title="Solicita&ccedil;&atilde;o Excel" target="_blank"><i class="glyphicon glyphicon-file" style="font-size: 20px;"></i></a>-->

                        <? } ?>
					</td>
  					<td><?= $oMnySolicitacaoAporte->getNumeroAporte()?></td>
					<td><?= $oMnySolicitacaoAporte->getDataSolicitacaoFormatado()?></td>
					<td><?= $oMnySolicitacaoAporte->getAtivo()?></td>
					<td><?= $oMnySolicitacaoAporte->getSolAlt()?></td>
					<td align='center'><?= ($oMnySolicitacaoAporte->getLiberado() == 0)? "<span style='color:#337ab7;font-weight:bold;size:18px;'>ABERTA</span>" : "<span style='color:red;font-weight:bold;size:18px;'>FECHADA</span>"?></td>
					<!--<td align='center'><?//= ($oMnySolicitacaoAporte->getLiberado() == 1)? $oMnySolicitacaoAporte->getTransferenciaAporte()->getMnyMovimentoItemOrigem()->getMovValorParcelaFormatado() : "-"?></td>-->
					<td align='center'><?= $oMnySolicitacaoAporte->getNumeroSolicitacao() ?></td>
  				</tr>
  				<?php }?>
  				</tbody>
  				<tfoot>
  				<tr>
   					<td></td>
   				</tr>
   				</tfoot>
  			<?php }//if(count($voMnySolicitacaoAporte)){?>
  			</table>
                    </form>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
         <script src="js/producao.js" type="text/javascript"></script>
         <script type="text/javascript" charset="utf-8">
         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnySolicitacaoAporte');

        function geraFichaTransf(nMovCodigoOrigem, nMovCodigoDestino){
				window.open("http://200.98.201.88/webmoney/relatorios/?rel=transferencia&movimento="+nMovCodigoOrigem);
			setTimeout( function() {
				window.open("http://200.98.201.88/webmoney/relatorios/?rel=transferencia&movimento="+nMovCodigoDestino);
			}, 1000);

		}
  	 </script>

       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
