<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContaCaixa = $_REQUEST['oMnyContaCaixa'];
 $voMnyPlanoContas = $_REQUEST['voMnyPlanoContas'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Despesa de Caixa - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Gerenciar Despesa de Caixas</a> &gt;  <a href="index.php"><a href="?action=MnyConciliacao.preparaFormualrio&sOP=Cadastrar">Gerenciar Concilia&ccedil;&atilde;o</a>&gt;<strong><?php echo $sOP?> Despesa de Caixa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Despesa de Caixa</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyContaCaixa" action="?action=MnyContaCaixa.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodContaCaixa" value="<?=(is_object($oMnyContaCaixa)) ? $oMnyContaCaixa->getCodContaCaixa() : ""?>" />

        <input type='hidden' name='fInseridoPor'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getInseridoPor() : $_SESSION['oUsuarioImoney']->getLogin() ." || ". date('d/m/Y H:i')?>'/>
        <input type='hidden' name='fAlteradoPor'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getAlteradoPor() : $_SESSION['oUsuarioImoney']->getLogin() ." || ". date('d/m/Y H:i')?>'/>
        <input type='hidden' name='fAtivo' value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getAtivo() : "1"?>'/>

         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Despesa de Caixa">
 		 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MnyPlanoContas">Unidade:</label>
					<div class="col-sm-5">
					<select name='fCodUnidade'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPlanoContas){
							   foreach($voMnyPlanoContas as $oMnyPlanoContas){
								   if($oMnyContaCaixa){
									   $sSelected = ($oMnyContaCaixa->getCodUnidade() == $oMnyPlanoContas->getPlanoContasCodigo() || $oMnyPlanoContas->getPlanoContasCodigo() == $_REQUEST['fCodUnidade']) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPlanoContas->getPlanoContasCodigo()?>'><?= $oMnyPlanoContas->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                <label class="col-sm-2 control-label" style="text-align:left" for="Data">Data:</label>
                	<div class="col-sm-2">
                        <input  required class="form-control" type='text'   id='DataConta' placeholder='Data' name='fDataConta'   value='<?= ($_REQUEST['fDataConta']) ? $_REQUEST['fDataConta'] : ""?>'/>
                    </div>
                </div>
				<br>
                <div class="form-group">
                <label class="col-sm-2 control-label" style="text-align:left" for="CreditoDebito">Cr&eacute;dito/d&eacute;bito:</label>
                <label class="col-sm-2 control-label" style="text-align:left" for="Valor">Valor:</label>
                <label class="col-sm-4 control-label" style="text-align:left" for="Descri&ccedil;&atilde;o">Desdcri&ccedil;&atilde;o:</label>
                <label class="col-sm-4 control-label" style="text-align:left" for="Doc. Pendente">Doc. Pendente:</label>
				</div>
                 <div class="form-group">
                <div class="col-sm-2">
					<select name='fCreditoDebito'  class="form-control chosen"  >
						<option value=''>Selecione</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getCreditoDebito() == '-') ? " selected " : ""?> value="-">D&eacute;bito</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getCreditoDebito() == '+') ? " selected " : ""?> value='+'>Cr&eacute;dito</option>
					</select>

				</div>
                 <div class="col-sm-2">
                    <input  required class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getValorFormatado() : ""?>'/>
                 </div>
                 <div class="col-sm-4">
                         <input  required class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getDescricao() : ""?>'/>
                    </div>
				<div class="col-sm-4">
					<input type='checkbox' name='fDocPendente' value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getDocPendente() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodArquivo">Cod_arquivo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodArquivo' placeholder='Cod_arquivo' name='fCodArquivo'  onKeyPress="TodosNumero(event);" value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getCodArquivo() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
