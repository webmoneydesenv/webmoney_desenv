
 <?php
header("Content-Type: text/html; charset=UTF-8",true);
 $voMnyContaCaixa = $_REQUEST['voMnyContaCaixa'];
 $nSaldoConta = $_REQUEST['nSaldoConta'];
 $oMnySaldoDiarioCaixa = $_REQUEST['oMnySaldoDiarioCaixa'];
 if($voMnyContaCaixa && $oMnySaldoDiarioCaixa){
	($oMnySaldoDiarioCaixa->getData() == $voMnyContaCaixa[0]->getDataConta())? $disabled="disabled = disabled" : $disabled="";
 }
?>

<form method="post" class="form-horizontal" name="formMnyContaCaixa2" action="?action=MnyContaCaixa.processaFormulario">
<? if($voMnyContaCaixa){?>
<input type="hidden" name="sOP" value="Alterar" />
<input type="hidden" name="fInseridoPor" value="<?= $voMnyContaCaixa[0]->getInseridoPor()?>"/>
<input type="hidden" name="fAlteradoPor" value="<?= $_SESSION['oUsuarioImoney']->getLogin() . " || ". date('d/m/Y H:i')?>"/>
<input type="hidden" name="fDataConta" value="<?= $voMnyContaCaixa[0]->getDataContaFormatado()?>"/>
<input type="hidden" name="fCodUnidade" value="<?= $voMnyContaCaixa[0]->getCodUnidade()?>"/>


<table  class="table table-striped table-bordered" align="left" width="100%" cellpadding="0" cellspacing="0">
        <tr>
			<th class='Titulo'>A&ccedil;&atilde;o</th>
			<th class='Titulo'>Credito/Debito</th>
			<th class='Titulo'>Descri&ccedil;&atilde;o</th>
			<th class='Titulo'>Valor</th>
			<th class='Titulo'>Doc.Pendente</th>
        </tr>
 <? $SaldoDia = 0;
	$i = 0;
	foreach($voMnyContaCaixa as $oMnyContaCaixa){
		if($oMnyContaCaixa->getCreditoDebito() == '-')
			$SaldoDia -= $oMnyContaCaixa->getValor();
		else
			$SaldoDia += $oMnyContaCaixa->getValor();
		?>
        <tr>
            <td>
             <? if($disabled == ""){?>
                	<a href="#" onClick="confirmacao('?action=MnyContaCaixa.processaFormulario&sOP=Excluir&fIdMnyContaCaixa=<?= $oMnyContaCaixa->getCodContaCaixa()?>&req=1');" data-toggle="tooltip" title="Excluir!"><i class="glyphicon glyphicon-trash"></i></a>
             <? }else{?>
                	<a href="#" data-toggle="tooltip" title="Voc&ecirc; n&atilde;o pode exlcuir este item!"><i class="glyphicon glyphicon-trash"></i></a>
             <? }?>
                <a href="#" onclick="recuperaConteudoDinamico('index.php','action=MnyContaCaixa.exibeDetalhe&nCodContaCaixa=<?=$oMnyContaCaixa->getCodContaCaixa()?>','divDetalhe')" data-toggle="modal" data-target="#Detalhe"  ><i class="glyphicon glyphicon-eye-open"></i></a>
           </td>
            <td>
<fieldset <?=$disabled?> >
            <input type="hidden" name="fCodContaCaixa[]" value="<?= $oMnyContaCaixa->getCodContaCaixa()?>"/>
			  <select name='fCreditoDebito[]'  class="form-control chosen" required >
						<option value=''>Selecione</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getCreditoDebito() == '-') ? " selected " : ""?> value="-">D&eacute;bito</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getCreditoDebito() == '+') ? " selected " : ""?> value='+'>Cr&eacute;dito</option>
					</select>
			</td>
            <td><input  required class="form-control" type='text' id='Descricao' placeholder='Descricao' name='fDescricao[]'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getDescricao() : ""?>'/></td>
            <td><input  required class="form-control" type='text' id='Valor' placeholder='Valor' name='fValor[]'   value='<?= ($oMnyContaCaixa) ? $oMnyContaCaixa->getValorFormatado() : ""?>'/></td>
            <td><select name='fDocPendente[]'  class="form-control chosen"  required >
						<option value=''>Selecione</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getDocPendente() == '1') ? " selected " : ""?> value="1">Sim</option>
 					    <option  <?= ($oMnyContaCaixa && $oMnyContaCaixa->getDocPendente() == '0') ? " selected " : ""?> value='0'>N&atilde;o</option>
				</select>
           </td>
        </tr>
 <? $i++;
	} ?>
</table>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-5">
             <a href="?action=MnyContaCaixa.processaFormulario&sOP=FecharCaixa&fCod=<?=$SaldoDia?>&fCodUnidade=<?=$voMnyContaCaixa[0]->getCodUnidade()?>&fData=<?=$voMnyContaCaixa[0]->getDataConta()?>"   class="btn btn-primary btn-ok" id="ok">Fechar Caixa</a>
             <a href="javascript:document.formMnyContaCaixa2.submit();"   class="btn btn-primary btn-ok" id="ok">Alterar Movimentos</a>
        </div>
   		</div>
</fieldset>
	<div class="form-group col-sm-12">
		<div class="col-sm-12" align="left" >
			<label class="col-sm-2 control-label" style="text-align:left" >Mov. Dia:  <?= 'R$ ' . number_format($SaldoDia, 2, ',', '.'); ?></label>
		</div>
        <div class="col-sm-12" align="left" >
			<label class="col-sm-2 control-label" style="text-align:left" >Saldo Anterior:  <? //= 'R$ ' . number_format($nSaldoConta->getValor(), 2, ',', '.'); ?></label>
		</div>
        <div class="col-sm-12" align="left" >
			<label class="col-sm-12 control-label" style="text-align:left" >Saldo Final Dia:  <? //= 'R$ ' . number_format($nSaldoConta->getValor() - $total, 2, ',', '.'); ?></label>
		</div>
    </div>
<? }?>
</form>

<div class="modal fade" id="Detalhe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
           <h4>Detalhe Despesa Caixa</h4>
        </div>
        <div class="modal-body">
             <div class="modal-title"  id="divDetalhe">&nbsp;</div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Sair</button>
        </div>
    </div>
    </div>
</div>
