<?php
 $voUnidades = $_REQUEST['voUnidades'];
 $voMnyContaCaixa = $_REQUEST['voMnyContaCaixa'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>LISTA DE CAIXAS</title>

  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <link href="css/jquery-datatables.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/dataTables.colVis.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body <?=($_REQUEST['nTipo'] ? "onLoad='abrirPagina(\"action=MnyContaCaixa.preparaLista\",1)'" : "" )?> >
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
   <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContaCaixa.preparaLista">Gerenciar Despesa de Caixas</a> &gt;  <a href="index.php"><a href="?action=MnyConciliacao.preparaFormulario&sOP=Cadastrar">Gerenciar Concilia&ccedil;&atilde;o</a>&gt;<strong><?php echo $sOP?> Despesa de Caixa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina">Despesas de Caixa</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
        <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados de Despesa de Caixa">
        <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="MnyPlanoContas">Unidade:</label>
					<div class="col-sm-4">
					<select name='fCodUnidade' id="Unidade"  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voUnidades){
							   foreach($voUnidades as $oUnidade){
								   if($_REQUEST['nCodUnidade']){
									   $sSelected = ($_REQUEST['nCodUnidade'] == $oUnidade->getPlanoContasCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oUnidade->getPlanoContasCodigo()?>'><?= $oUnidade->getDescricao()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
                <label class="col-sm-1 control-label" style="text-align:left" for="Data">Data:</label>
                    <div class="col-sm-2">
                        <input required class="form-control" type='text' id='DataConta' placeholder='Data' name='fDataConta' value='<?= ($_REQUEST['fDataConta']) ? $_REQUEST['fDataConta'] : ""?>'/>
                    </div>
                    <div class="col-sm-4">
                        <a onClick="abrirPagina('action=MnyContaCaixa.preparaLista',1)" href="#"><button type="button" class="btn btn-primary" value=''>Listar Movimentos</button></a>
                        <a onClick="recuperaConteudoDinamico2('index.php','action=MnyContaCaixa.preparaFormulario&sOP=Cadastrar','divLinha')" href="#"><button type="button" class="btn btn-primary" value=''><i class="glyphicon glyphicon-plus"></i></button></a>
<!--
                       <a onClick="recuperaConteudoDinamico3('index.php','action=MnyContaCaixa.preparaLista&fCodUnidade=&fData=','divListaCaixa')" href="#"><button type="button" class="btn btn-primary" value=''>Listar Movimentos</button></a>
             	       <a onClick="abrirPagina('action=MnyContaCaixa.preparaFormulario&sOP=Cadastrar', 2)" href="#"><button type="button" class="btn btn-success" value=''><i class="glyphicon glyphicon-plus"></i></button></a>-->
     				</div>
				</div>
				<br>
                </fieldset>
                </div>
                <br>
			    <div id="divLinha">&nbsp;</div>
                <div id="divListaCaixa">&nbsp;</div>
 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
         <script language="javascript" src="js/jquery/plugins/dataTables.bootstrap.js"></script>
         <script src="js/jquery/plugins/dataTables.colVis.js"></script>
          		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">
			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			  jQuery(function($){
				$("#DataConta").mask("99/99/9999");
  			  });
 		</script>
         <script type="text/javascript" charset="utf-8">

 function recuperaConteudoDinamico2(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
					jQuery(function($){
					    document.getElementById('DataContaF').value = document.getElementById('DataConta').value;
					    document.getElementById('CodUnidadeF').value = document.getElementById('Unidade').value;
						$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					  });
    		}
	});
}


function recuperaConteudoDinamico3(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
				alert(oXMLRequest.responseText);
				alert(oXMLRequest.status+' , '+sErrorType);
		    },
			success: function(data){
				  oDiv.innerHTML = data;
			},
			complete: function() {
					jQuery(function($){
					//   document.getElementById('DataF').value = document.getElementById('DataConta').value;
						$("#Valor").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
					  });
    		}
	});
}



         var asInitVals = new Array();
        	$(document).ready(function() {
        		oTable = $('#lista').dataTable({
   				"scrollX": true,
    				dom: 'C<"clear">lfrtip',
    				colVis: {
              		  exclude: [ 0 ]
    				},
        			"aoColumnDefs": [
   					{
   						"bSortable": false, "aTargets": [ 0 ],
   						"bSearchable": false, "aTargets": [ 0 ]
   					}
   				]
    			});
   		} );
  		$(function() {
          window.prettyPrint && prettyPrint()
          $(document).on('mouseover', '.yamm .dropdown-menu', function(e) {
            e.stopPropagation()
          })
        });
 	  atualizaAcoes('MnyContaCaixa');


	function abrirPagina(sLink, tipo){

		var nUnidade = document.getElementById('Unidade').value;
		if(nUnidade==""){
			alert('Unidade não informada!');
			return false;
		}
		var dData = document.getElementById('DataConta').value;

		if(dData==""){
			alert('Data não informada!');
			return false;
		}else{
			if (dData.length == "10"){
				var vData = dData.split("/");
				var dDataBanco = vData[2] + '-' + vData[1] + '-' + vData[0];
				var sLinkCompleto = sLink + "&fCodUnidade="+nUnidade +"&fData="+dDataBanco;
				if(tipo == 1){
					recuperaConteudoDinamico3('index.php',sLinkCompleto,'divListaCaixa');
				}else{
					recuperaConteudoDinamico2('index.php',sLinkCompleto,'divLinha');
                    /*setTimeout( function() {
					    document.getElementById('DataConta').value = document.getElementById('DataConta').value;
						alert('chegou2');
						return false;
				    }, 1000);*/
				}
			}else{
				alert('Data inválida!');
				return false;
			}
		}



	}


  	 </script>


       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd -->





 </html>


