﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPessoaJuridica = $_REQUEST['oMnyPessoaJuridica'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Pessoa Jurídica - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPessoaJuridica.preparaLista">Gerenciar Pessoa Jurídicas</a> &gt; <strong><?php echo $sOP?> Pessoa Jurídica</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Pessoa Jurídica</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPessoaJuridica" action="?action=MnyPessoaJuridica.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyPessoaJuridica" disabled>


 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurCodigo">Pesjur_codigo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Pesjur_codigo' name='fPesjurCodigo' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurCodigo() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurRazao">Razao Social:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Razao Social' name='fPesjurRazao' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurRazao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurNome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome' name='fPesjurNome' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurCnpj">Cnpj:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Cnpj' name='fPesjurCnpj' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurCnpj() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurInsmun">Inscricao Municipal:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Inscricao Municipal' name='fPesjurInsmun' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurInsmun() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesjurInsest">Inscricao Estadual:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Inscricao Estadual' name='fPesjurInsest' value='<?= ($oMnyPessoaJuridica) ? $oMnyPessoaJuridica->getPesjurInsest() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaJuridica">Matriz:</label>
					<div class="col-sm-10">
					<select name='fCodMatriz'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPessoaJuridica){
							   foreach($voMnyPessoaJuridica as $oMnyPessoaJuridica){
								   if($oMnyPessoaJuridica){
									   $sSelected = ($oMnyPessoaJuridica->getCodMatriz() == $oMnyPessoaJuridica->getPesjurCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPessoaJuridica->getPesjurCodigo()?>'><?= $oMnyPessoaJuridica->getPesjurCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyPessoaJuridica.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
