<?php
header("Content-Type: text/html; text/css; charset=UTF-8",true);

$voVTramitacaoPendencias = $_REQUEST['voVTramitacaoPendencias'];
$voVTramitacaoRealizados = $_REQUEST['voVTramitacaoRealizados'];
?>

    <div class="form-group col-sm-8">
           <div class="alert alert-info cols-sm-8">
            <i class="glyphicon glyphicon-pushpin"></i><a href="#" data-toggle="collapse" data-target="#teste"><strong>Avisos <span class="badge"><?= $totalPendencias?></span></strong></a>
           </div>
           <div id="teste" class="collapse col-sm-12">
                 <div class="form-group">
                   <ul class="nav nav-list" >
                     <li class="nav-header"><strong>Pend&ecirc;ncias</strong></li>
                       <? if($voVTramitacaoPendencias ){?>
                           <? foreach($voVTramitacaoPendencias as $oVTramitacaoPendencias){ ?>
                                  <li class="active"><a href=".?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=<?=$oVTramitacaoPendencias->mov_codigo ?>||<?= $oVTramitacaoPendencias->mov_item?>&MovContrato=<?= $oVTramitacaoPendencias->mov_contrato?>&nTipoContrato=<?= $oVTramitacaoPendencias->tipo?>" target="_blank">
                                        Respons&aacute;vel : <?= $oVTramitacaoPendencias->de ?>
                                      | Movimento.Item : <?= $oVTramitacaoPendencias->mov_codigo.".".$oVTramitacaoPendencias->mov_item ?>
                                      | Descri&ccedil;&atilde;o : <?= $oVTramitacaoPendencias->tram_descricao ?>
                                      | Data : <?= date('d/m/Y', strtotime($oVTramitacaoPendencias->data)) ?></a>
                                  </li>
                            <? }?>
                       <? }?>
                      <li class="nav-header"><strong>Realizados</strong></li>
                      <? if($voVTramitacaoRealizados ){?>
                       <? foreach($voVTramitacaoRealizados as $oVTramitacaoRealizados){ ?>
                              <li class="active"><a href=".?action=MnyMovimentoItem.preparaFormulario&sOP=Detalhar&sTipoLancamento=Pagar&nIdMnyMovimentoItem=<?=$oVTramitacaoRealizados->mov_codigo ?>||<?= $oVTramitacaoRealizados->mov_item?>&MovContrato=<?= $oVTramitacaoRealizados->mov_contrato?>&nTipoContrato=<?= $oVTramitacaoRealizados->tipo?>" target="_blank">
                                    Respons&aacute;vel : <?= $oVTramitacaoRealizados->de ?>
                                  | Movimento.Item : <?= $oVTramitacaoRealizados->mov_codigo.".".$oVTramitacaoRealizados->mov_item ?>
                                  | Descri&ccedil;&atilde;o : <?= $oVTramitacaoRealizados->tram_descricao ?>
                                  | Data : <?= date('d/m/Y', strtotime($oVTramitacaoRealizados->data)) ?></a>
                              </li>
                        <? }?>
                   <? }?>
                   </ul>
                </div>
          </div>
     </div>





