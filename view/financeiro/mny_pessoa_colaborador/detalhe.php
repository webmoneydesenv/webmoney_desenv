﻿<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPessoaColaborador = $_REQUEST['oMnyPessoaColaborador'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Colaborador - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPessoaColaborador.preparaLista">Gerenciar Colaboradors</a> &gt; <strong><?php echo $sOP?> Colaborador</strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Colaborador</h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPessoaColaborador" action="?action=MnyPessoaColaborador.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do MnyPessoaColaborador" disabled>


 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyPessoaGeral">Pesc_codigo:</label>
					<div class="col-sm-10">
					<select name='fPescCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyPessoaGeral){
							   foreach($voMnyPessoaGeral as $oMnyPessoaGeral){
								   if($oMnyPessoaColaborador){
									   $sSelected = ($oMnyPessoaColaborador->getPescCodigo() == $oMnyPessoaGeral->getPesgCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyPessoaGeral->getPesgCodigo()?>'><?= $oMnyPessoaGeral->getPesgCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SysStatus">Status:</label>
					<div class="col-sm-10">
					<select name='fCodStatus'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysStatus){
							   foreach($voSysStatus as $oSysStatus){
								   if($oMnyPessoaColaborador){
									   $sSelected = ($oMnyPessoaColaborador->getCodStatus() == $oSysStatus->getCodStatus()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysStatus->getCodStatus()?>'><?= $oSysStatus->getCodStatus()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PescNome">Nome:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Nome' name='fPescNome' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getPescNome() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PescCpf">Cpf:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Cpf' name='fPescCpf' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getPescCpf() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PescMat">Matricula:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Matricula' name='fPescMat' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getPescMat() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyFuncao">Funcao:</label>
					<div class="col-sm-10">
					<select name='fFunCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyFuncao){
							   foreach($voMnyFuncao as $oMnyFuncao){
								   if($oMnyPessoaColaborador){
									   $sSelected = ($oMnyPessoaColaborador->getFunCodigo() == $oMnyFuncao->getFunCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyFuncao->getFunCodigo()?>'><?= $oMnyFuncao->getFunCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyUnidadePlanocontas">Unidade:</label>
					<div class="col-sm-10">
					<select name='fUniCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyUnidadePlanocontas){
							   foreach($voMnyUnidadePlanocontas as $oMnyUnidadePlanocontas){
								   if($oMnyPessoaColaborador){
									   $sSelected = ($oMnyPessoaColaborador->getUniCodigo() == $oMnyUnidadePlanocontas->getUniCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyUnidadePlanocontas->getUniCodigo()?>'><?= $oMnyUnidadePlanocontas->getUniCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PescSal">Salario:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Salario' name='fPescSal' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getPescSal() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnySetorPlanocontas">Setor:</label>
					<div class="col-sm-10">
					<select name='fSetCodigo'  class="form-control" >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnySetorPlanocontas){
							   foreach($voMnySetorPlanocontas as $oMnySetorPlanocontas){
								   if($oMnyPessoaColaborador){
									   $sSelected = ($oMnyPessoaColaborador->getSetCodigo() == $oMnySetorPlanocontas->getSetCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnySetorPlanocontas->getSetCodigo()?>'><?= $oMnySetorPlanocontas->getSetCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Admissao">Admissao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Admissao' name='fAdmissao' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getAdmissao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Demissao">Demissao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Demissao' name='fDemissao' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getDemissao() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Valext">Valor extenso:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Valor extenso' name='fValext' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getValext() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Motivo">Motivo:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' placeholder='Motivo' name='fMotivo' value='<?= ($oMnyPessoaColaborador) ? $oMnyPessoaColaborador->getMotivo() : ""?>'/>
				</div>
				</div>
				<br>


         </fieldset>
         <br/>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyPessoaColaborador.preparaLista">Voltar</a></div>
   		</div>
       </form>
 	   <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
