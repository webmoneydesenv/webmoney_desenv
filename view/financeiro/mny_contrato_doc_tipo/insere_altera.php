<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyContratoDocTipo = $_REQUEST['oMnyContratoDocTipo'];
 $voMnyContratoTipo = $_REQUEST['voMnyContratoTipo'];
 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Tipo de Documento do Contrato - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyContratoDocTipo.preparaLista">Gerenciar Tipo de Documento do Contratos</a> &gt; <strong><?php echo $sOP?> Tipo de Documento do Contrato</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Tipo de Documento do Contrato</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyContratoDocTipo" action="?action=MnyContratoDocTipo.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fCodContratoDocTipo" value="<?=(is_object($oMnyContratoDocTipo)) ? $oMnyContratoDocTipo->getCodContratoDocTipo() : ""?>" />
         <input class="form-control" type='hidden' id='Obrigatorio' placeholder='Item' name='fItem'   value='<?= ($oMnyContratoDocTipo) ? $oMnyContratoDocTipo->getItem() : ""?>'/>
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Tipo de Documento do Contrato">

 							<input type='hidden' name='fCodContratoDocTipo' value='<?= ($oMnyContratoDocTipo) ? $oMnyContratoDocTipo->getCodContratoDocTipo() : ""?>'/>

 <div class="form-group">
              <label class="col-sm-2 control-label" style="text-align:left" for="Obrigatorio">Tipo Contrato:</label>
					<div class="col-sm-4">
                	<select id='TipoContrato' name='fCodContratoTipo'  class="form-control chosen"  >
						<option >Selecione</option>
                        <? $sSelected = "";
					   if($voMnyContratoTipo){
						   foreach($voMnyContratoTipo as $oMnyContratoTipo){
							   if($oMnyContratoDocTipo){
								   $sSelected = ($oMnyContratoDocTipo->getCodContratoTipo() == $oMnyContratoTipo->getCodContratoTipo()) ? "selected" : "";
							   }
						  ?>
  							  <option  <?= $sSelected?> value='<?= $oMnyContratoTipo->getCodContratoTipo()?>'><?= $oMnyContratoTipo->getDescricao()?></option>

			<?				}
					}
					?>
<!--					<option  <?//= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getObrigatorio()==0) ? " selected " : ""?> value='0'>N&atilde;o</option>
-->                    </select>
				</div>
				</div>
				<br>
 <div class="form-group">
              <label class="col-sm-2 control-label" style="text-align:left" for="Obrigatorio">Obrigat&oacute;rio:</label>
					<div class="col-sm-4">
                <select id='fObrigatorio' name='fObrigatorio'  class="form-control chosen"  >
						<option >Selecione</option>
						<option  <?= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getObrigatorio()==1) ? " selected " : ""?> value='1'>Sim</option>
						<option  <?= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getObrigatorio()==0) ? " selected " : ""?> value='0'>N&atilde;o</option>
                    </select>
				</div>
				</div>
				<br>
 <div class="form-group">
              <label class="col-sm-2 control-label" style="text-align:left" for="Ação Inicial">A&ccedil;&atilde;o Inicial:</label>
					<div class="col-sm-4">
                <select id='fAcaoInicial' name='fAcaoInicial'  class="form-control chosen"  >
						<option >Selecione</option>
						<option  <?= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getAcaoInicial()==1) ? " selected " : ""?> value='1'>Sim</option>
						<option  <?= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getAcaoInicial()==0) ? " selected " : ""?> value='0'>N&atilde;o</option>
                    </select>
				</div>
				</div>
				<br>
 <div class="form-group">
              <label class="col-sm-2 control-label" style="text-align:left" for="Item">Multiplos Arquivos:</label>
					<div class="col-sm-4">
                <select id='fItem' name='fItem'  class="form-control chosen"  >
					<!--	<option>Selecione</option>
						<option  <? //= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getItem()==1) ? " selected " : ""?> value='1'>Sim</option> -->
						<option  <?= ($oMnyContratoDocTipo && $oMnyContratoDocTipo->getItem()==0) ? " selected " : ""?> value='0'>N&atilde;o</option>
                    </select>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyContratoDocTipo) ? $oMnyContratoDocTipo->getAtivo() : "1"?>'/>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="Descricao">Descri&ccedil;&atilde;o:</label>
					<div class="col-sm-10">
					<input class="form-control" type='text' id='Descricao' placeholder='Descrição' name='fDescricao'   value='<?= ($oMnyContratoDocTipo) ? $oMnyContratoDocTipo->getDescricao() : ""?>'/>
				</div>
				</div>
				<br>

         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
