<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyPessoaGeral = $_REQUEST['oMnyPessoaGeral'];

 $voMnyTipoPessoaPlanocontas = $_REQUEST['voMnyTipoPessoaPlanocontas'];

$voSysBanco = $_REQUEST['voSysBanco'];

$voSysContaTipoPlanocontas = $_REQUEST['voSysContaTipoPlanocontas'];


 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Pessoa - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
 <link href="css/chosen.min.css" rel="stylesheet" type="text/css">
 <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
     </header>
     <?php include_once("view/includes/menu.php")?>
   	<div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyPessoaGeral.preparaLista">Gerenciar Pessoas</a> &gt; <strong><?php echo $sOP?> Pessoa</strong><!-- InstanceEndEditable --></div>
 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?>Pessoa</h3>
       <!-- InstanceEndEditable --></h1>
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
       <form method="post" class="form-horizontal" name="formMnyPessoaGeral" action="?action=MnyPessoaGeral.processaFormulario">
         <input type="hidden" name="sOP" value="<?php echo $sOP?>" />
         <input type="hidden" name="fPesgCodigo" value="<?=(is_object($oMnyPessoaGeral)) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>" />
         <div id="formulario" class="TabelaAdministracao">
         <fieldset title="Dados do Pessoa">

 							<input type='hidden' name='fPesgCodigo' value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgCodigo() : ""?>'/>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="MnyTipoPessoaPlanocontas">Tipo:</label>
					<div class="col-sm-10">
					<select name='fTipCodigo'  class="form-control chosen"  required  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voMnyTipoPessoaPlanocontas){
							   foreach($voMnyTipoPessoaPlanocontas as $oMnyTipoPessoaPlanocontas){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getTipCodigo() == $oMnyTipoPessoaPlanocontas->getTipCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oMnyTipoPessoaPlanocontas->getTipCodigo()?>'><?= $oMnyTipoPessoaPlanocontas->getTipCodigo()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="CodStatus">Status:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='CodStatus' placeholder='Status' name='fCodStatus'  required  onKeyPress="TodosNumero(event);" value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getCodStatus() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndLogra">Logradouro:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgEndLogra' placeholder='Logradouro' name='fPesgEndLogra'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndLogra() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndBairro">- Bairro:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgEndBairro' placeholder='- Bairro' name='fPesgEndBairro'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndBairro() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEndCep">- CEP:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgEndCep' placeholder='- CEP' name='fPesgEndCep'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEndCep() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Cidade">- Cidade (Codigo IBGE):</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Cidade' placeholder='- Cidade (Codigo IBGE)' name='fCidade'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getCidade() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="Estado">Estado:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='Estado' placeholder='Estado' name='fEstado'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getEstado() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgEmail">- E-mail:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgEmail' placeholder='- E-mail' name='fPesgEmail'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgEmail() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesFones">- Fones:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesFones' placeholder='- Fones' name='fPesFones'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesFones() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgInc">- Inclusao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgInc' placeholder='- Inclusao' name='fPesgInc'  required   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgInc() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgAlt">- Alteracao:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgAlt' placeholder='- Alteracao' name='fPesgAlt'   value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgAlt() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SysBanco">- Codigo:</label>
					<div class="col-sm-10">
					<select name='fBcoCodigo'  class="form-control chosen"    >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysBanco){
							   foreach($voSysBanco as $oSysBanco){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getBcoCodigo() == $oSysBanco->getBcoCodigo()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysBanco->getBcoCodigo()?>'><?= $oSysBanco->getBcoNome()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-2 control-label" style="text-align:left" for="SysContaTipoPlanocontas">Tipocon_cod:</label>
					<div class="col-sm-10">
					<select name='fTipoconCod'  class="form-control chosen"  >
						<option value=''>Selecione</option>
						<? $sSelected = "";
						   if($voSysContaTipoPlanocontas){
							   foreach($voSysContaTipoPlanocontas as $oSysContaTipoPlanocontas){
								   if($oMnyPessoaGeral){
									   $sSelected = ($oMnyPessoaGeral->getTipoconCod() == $oSysContaTipoPlanocontas->getTipoconCod()) ? "selected" : "";
								   }
						?>
								   <option  <?= $sSelected?> value='<?= $oSysContaTipoPlanocontas->getTipoconCod()?>'><?= $oSysContaTipoPlanocontas->getTipoconCod()?></option>
						<?
							   }
						   }
						?>
					</select>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoAgencia">Agencia:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgBcoAgencia' placeholder='Agencia' name='fPesgBcoAgencia'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgBcoAgencia() : ""?>'/>
				</div>
				</div>
				<br>

 <div class="form-group">
					<label class="col-sm-1 control-label" style="text-align:left" for="PesgBcoConta">Conta:</label>
					<div class="col-sm-11">
					<input class="form-control" type='text' id='PesgBcoConta' placeholder='Conta' name='fPesgBcoConta'     value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getPesgBcoConta() : ""?>'/>
				</div>
				</div>
				<br>
					<input type='hidden' name='fAtivo' value='<?= ($oMnyPessoaGeral) ? $oMnyPessoaGeral->getAtivo() : "1"?>'/>
				</div>
				<br>


         </fieldset>
          <div class="form-group">
     	<div class="col-sm-offset-5 col-sm-2">
       	<button type="submit" class="btn btn-primary" ><?=$sOP?></button>
     	</div>
   		</div>
       </form>
 		<script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
  		<script language="javascript" src="js/jquery/plugins/jquery.maskedinput.js"></script>
 		<script language="javascript" src="js/jquery/plugins/jquery.maskMoney.js"></script>
 		<script language="javascript" src="js/jquery/plugins/chosen.jquery.min.js"></script>
  		<script src="js/producao.js" type="text/javascript"></script>

 		<script type="text/javascript" language="javascript">

			 jQuery(document).ready(function(){jQuery(".chosen").data("placeholder","Selecione").chosen(); })
			 jQuery(function($){
				 });
 		</script>
       <!-- InstanceEndEditable -->
     </section>
   <!-- end .content -->
   </div>
   <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
