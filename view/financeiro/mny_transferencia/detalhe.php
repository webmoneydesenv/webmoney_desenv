<?php
 $sOP = $_REQUEST['sOP'];
 $oMnyTransferencia = $_REQUEST['oMnyTransferencia'];

 $oMnyMovimentoItemOrigem = $oMnyTransferencia->getMnyMovimentoItemOrigem();
 $oMnyMovimentoOrigem = $oMnyTransferencia->getMnyMovimentoOrigem();
 $oMnyContaCorrenteOrigem = $oMnyTransferencia->getMnyContaCorrenteOrigem();
 $oMnySysBancoOrigem = $oMnyContaCorrenteOrigem->getSysBanco();
 $oMnySysEmpresaOrigem = $oMnyMovimentoOrigem->getSysEmpresa();

 $oMnyMovimentoItemDestino = $oMnyTransferencia->getMnyMovimentoItemDestino();
 $oMnyMovimentoDestino = $oMnyTransferencia->getMnyMovimentoDestino();
 $oMnyContaCorrenteDestino = $oMnyTransferencia->getMnyContaCorrenteDestino();
 $oMnySysEmpresaDestino = $oMnyMovimentoDestino->getSysEmpresa();
 $oMnySysBancoDestino = $oMnyContaCorrenteDestino->getSysBanco();
 $voMnyContratoDocTipo = $_REQUEST['voMnyContratoDocTipo'];
 $oMnyMovimentoDe = $_REQUEST['oMnyMovimentoDe'];



 ?>
 <!doctype html>
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <!-- InstanceBeginEditable name="doctitle" -->
 <title>Transfer&ecirc;ncia - <?php echo $sOP ?></title>
 <!-- InstanceEndEditable -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">                                             <link rel="apple-touch-icon" sizes="57x57" href="imagens/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="imagens/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="imagens/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="imagens/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="imagens/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="imagens/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="imagens/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="imagens/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="imagens/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="imagens/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="imagens/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="imagens/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="imagens/ico/favicon-16x16.png">
<link rel="manifest" href="imagens/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="imagens/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <!-- InstanceBeginEditable name="head" -->

 <!-- InstanceEndEditable -->
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 </head>
 <body>
 <div class="container">
   <header>
 	 <?php include_once("view/includes/topo.php")?>
   </header>
      <?php include_once("view/includes/menu.php")?>
   <div class="content">
       <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui:</strong> <a href="index.php">Home</a> &gt;
     <!-- InstanceBeginEditable name="Migalha" --><a href="index.php"><a href="?action=MnyTransferencia.preparaLista">Gerenciar Transfer&ecirc;ncias</a> &gt; <strong><?php echo $sOP?> Transfer&ecirc;ncia </strong><!-- InstanceEndEditable --></div>

 		<!-- InstanceBeginEditable name="titulo" -->
       <h3 class="TituloPagina"><?php echo $sOP?> Transfer&ecirc;ncia </h3>
       <!-- InstanceEndEditable -->
     <section>
 		<!-- InstanceBeginEditable name="conteudo" -->
         <div id="formulario" class="TabelaAdministracao">
    	<fieldset tile="Origem Destino">
        <div class="form-group col-sm-6">
        <legend>Origem</legend>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oMnySysEmpresaOrigem) ? $oMnySysEmpresaOrigem->getEmpFantasia() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Banco:&nbsp;</strong><?= ($oMnySysBancoOrigem) ? $oMnySysBancoOrigem->getBcoNome() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Ag&ecirc;ncia:&nbsp;</strong><?= ($oMnyContaCorrenteOrigem) ? $oMnyContaCorrenteOrigem->getCcrAgencia() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Conta:&nbsp;</strong><?= ($oMnyContaCorrenteOrigem) ? $oMnyContaCorrenteOrigem->getCcrConta() : ""?></div>
				</div>
		</div>
		<div class="form-group col-sm-6">
          <legend>Destino</legend>
				 <div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Empresa:&nbsp;</strong><?= ($oMnySysEmpresaDestino) ? $oMnySysEmpresaDestino->getEmpFantasia() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Banco:&nbsp;</strong><?= ($oMnySysBancoDestino) ? $oMnySysBancoDestino->getBcoNome() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Ag&ecirc;ncia:&nbsp;</strong><?= ($oMnyContaCorrenteDestino) ? $oMnyContaCorrenteDestino->getCcrAgencia() : ""?></div>
				</div>
 				<div class="form-group col-sm-12">
					 <div class="col-sm-12" style="text-align:left"><strong>Conta:&nbsp;</strong><?= ($oMnyContaCorrenteDestino) ? $oMnyContaCorrenteDestino->getCcrConta() : ""?></div>
				</div>
         </div>
         </fieldset>
         <div class="form-group ">
         <legend>Dados da Transfer&ecirc;ncia</legend>
             </div>
         <div class="form-group">
   				<div class="col-sm-12" style="background-color:#dbdbdb;">
					 <div class="col-sm-2" style="text-align:left"><strong>Compet&ecirc;ncia:&nbsp;</strong><?= ($oMnyMovimentoItemOrigem) ? $oMnyMovimentoItemOrigem->getCompetenciaFormatado() : ""?></div>
					 <div class="col-sm-3" style="text-align:left"><strong>Data da Transfer&ecirc;ncia:&nbsp;</strong><?= ($oMnyMovimentoOrigem) ? $oMnyMovimentoOrigem->getMovDataEmissaoFormatado() : ""?></div>
					 <div class="col-sm-3" style="text-align:left"><strong>N&ordm; Doc:&nbsp;</strong><?= ($oMnyMovimentoOrigem) ? $oMnyMovimentoOrigem->getMovDocumento() : ""?></div>
					 <div class="col-sm-2" style="text-align:left"><strong>Valor:&nbsp;</strong><?= ($oMnyMovimentoItemOrigem) ? $oMnyMovimentoItemOrigem->getMovValorParcelaFormatado() : ""?></div>
                </div>
         </div>
        <br><br>
         <div class="form-group">
                <div class="col-sm-12">
                 <a  href="#" onClick="geraFichaTransf(<?=$oMnyMovimentoOrigem->getMovCodigo()?>, <?=$oMnyMovimentoDestino->getMovCodigo()?>);" ><button type=\"button\" class=\"btn btn-success btn-sm\"><i class=\"glyphicon glyphicon-folder-open\">&nbsp;Gerar Ficha de Transfer&ecirc;cia</i></button></a>
                </div>
            </div>


      <legend title="Documentos">Documentos</legend>
  <? if($voMnyContratoDocTipo){ ?>

          <div class="form-group">
          <? foreach($voMnyContratoDocTipo as $oMnyContratoDocTipo){ ?>
                  <div>
                    <label class="control-label col-sm-12" style="text-align:left" for="Anexo"><?= $oMnyContratoDocTipo->getDescricao()?>:<br><br></label>
                      	<?  if($oMnyMovimentoDe)
                                    $voArquivos = $oFachadaSys->recuperarTodosSysArquivoPorMovimentoPorTipoDocumento($oMnyMovimentoDe->getMovCodigo(),$oMnyContratoDocTipo->getCodContratoDocTipo());
						?>
                      <div class="col-sm-12">
                        <? if($voArquivos){
                            foreach($voArquivos as $oArquivos){
                                if($oMnyContratoPessoa){
								    if(($oArquivos->getAtivo()==1) && ($oMnyContratoPessoaArquivoTipo->getItem() == 0)){ ?>
									   <input type="hidden" name='fCodContratoDocTipo[]' required value="<?= $oMnyContratoDocTipo->getCodContratoDocTipo()?>">
										<input type="hidden" name='fCodArquivo[]' required value="<?= ($oArquivos->getCodArquivo()) ? $oArquivos->getCodArquivo() : ""?>">
								 <? }
								}

                                if($oArquivos->getCodArquivo()){
                                            echo "
                                             <div class=\"col-sm-1 \" ><a target='_blank' href='?action=SysArquivo.preparaArquivo&fCodArquivo=".$oArquivos->getCodArquivo()."'><button type='button' class='btn btn-success btn-sm'><i class='glyphicon glyphicon-folder-open'>&nbsp;Abrir</i></button></a></div>";

                                     }

                            }
                       } ?>

                    <br>
		          </div>
        <? $i = 0;
		   $j =0;
            }

       }?>




   		 </div>
         </div>
          <div class="form-group">
 	    	<div class="col-sm-offset-5 col-sm-2"><a class="btn btn-primary" href="?action=MnyTransferencia.preparaLista">Voltar</a></div>
   		 </div>
 		</div>
       <!-- InstanceEndEditable --></h1>
     </section>
   <!-- end .content -->
   </div>

 	    <script src="js/jquery/jquery.js"></script>
  		<script src="js/bootstrap.min.js"></script>
         <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css"></script>
  		<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
     <script type="text/javascript" charset="utf-8">
        function geraFichaTransf(nMovCodigoOrigem, nMovCodigoDestino){
				window.open("http://200.98.201.88/webmoney/relatorios/?rel=transferencia&movimento="+nMovCodigoOrigem);
			setTimeout( function() {
				window.open("http://200.98.201.88/webmoney/relatorios/?rel=transferencia&movimento="+nMovCodigoDestino);
			}, 1000);

		}
     </script>
    <?php include_once("view/includes/mensagem.php")?>
   <footer>
 	<?php require_once("view/includes/rodape.php")?>
   </footer>
 <!-- end .container --></div>
 </body>
 <!-- InstanceEnd --></html>
