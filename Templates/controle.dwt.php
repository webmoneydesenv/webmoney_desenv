﻿<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title>Documento sem título</title>
<!-- TemplateEndEditable -->
<link href="css/style2.css" rel="stylesheet" type="text/css">
<link href="css/menu.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<script src="js/SpryMenuBar.js" type="text/javascript"></script>
<script src="js/producao.js" type="text/javascript"></script>
<script language="javascript" src="js/menu.js"></script>
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
 <?php include_once("controle/includes/mensagem.php")?>
<div class="container">
  <header>
	 <?php include_once("controle/includes/topo.php")?>
     <?php include_once("controle/includes/menu.php")?>  
  </header>
  	<div class="content">
      <div id="migalha" class="CelulaMigalhaLink"><strong>Você está aqui:</strong> <a href="index.php">Home</a> &gt; 
    <!-- TemplateBeginEditable name="Migalha" --><a href="?action=Basdespesaelemento.preparaLista">Gerenciar Basdespesaelementos</a> &gt; <strong><?php echo $sOP?> Basdespesaelemento</strong><!-- TemplateEndEditable --></div>
    <h1>
		<!-- TemplateBeginEditable name="titulo" -->Instruções<!-- TemplateEndEditable --></h1>
    <section>
		<!-- TemplateBeginEditable name="conteudo" -->Conteudo<!-- TemplateEndEditable --></h1>
    </section>
  <!-- end .content -->
  </div>
  <footer>
	<?php require_once("controle/includes/rodape.php")?>
  </footer>
<!-- end .container --></div>
</body>
</html>
