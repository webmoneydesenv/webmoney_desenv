<!DOCTYPE hmtl>
<html>
    <head>
        <meta charset="utf-8">
        <title>[Desenv --> Producao]</title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 80px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">T[Desenv --> Producao]</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Atualizar versão</a>
                </div>
            </div>
        </nav>

        <div class="container" role="main">
            <div class="alert alert-info">
                O script executou com sucesso. Verifique abaixo a saída desta execução.
                <a href="pull.php" class="btn btn-warning">Executar novamente</a>
            </div>
            <div class="well">
                <strong>Saída do comando:</strong><br>
                <?= nl2br(shell_exec("C:\wamp\atualizaVersao.bat"));?>
            </div>
        </div> <!-- /container -->
    </body>
</html>
