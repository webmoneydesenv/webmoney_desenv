﻿<!DOCTYPE hmtl>
<html>
    <head>
        <meta charset="utf-8">
        <title>git pull [Server_desenv]</title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 80px;
            }
        </style>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">git pull</a>
                </div>
            </div>
        </nav>

        <div class="container" role="main">
            <div class="alert alert-info">
                O script executou com sucesso. Verifique abaixo a saída desta execução.
                <a href="pull.php" class="btn btn-warning">Executar novamente</a>
            </div>
            <div class="well">
                <strong>Saída do comando (git pull):</strong><br>
                <?= nl2br(shell_exec("git pull https://webmoneydesenv:projetowebmoney@bitbucket.org/webmoneydesenv/webmoney_desenv.git"));?>
            </div>
        </div> <!-- /container -->
    </body>

</html>
