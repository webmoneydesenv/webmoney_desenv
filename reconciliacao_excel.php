
<?php  set_time_limit(3600); // 30 minutos ?>
<?php

 $sOP = $_REQUEST['sOP'];
 //$oAporte = $_REQUEST['oAporte'];
 $vaVReconciliacao = $_REQUEST['voVReconciliacao'];
 $oTotais = $_REQUEST['oTotais'];
 $i=1;

 $oUnidade = $_REQUEST['oUnidade'];
 $oMnyContaCorrente = $_REQUEST['oMnyContaCorrente'];

 if($vaVReconciliacao){
 $nSaldoAtual = array();
 $nSaldoAtual[0] =  $vaVReconciliacao[0]->getSaldoInicial;

// definimos o tipo de arquivo
header ('Cache-Control: no-cache, must-revalidate');
header ('Pragma: no-cache');
header('Content-Type: application/x-msexcel');
$sNomeArquivo = "RECONCILIACAO_". date('Y-m-d') . ".xls";
// Como será gravado o arquivo
header("Content-Disposition: attachment; filename=$sNomeArquivo"); ?>
<style type="text/css">
	.Table{
		background-color:#D6D6D6;
		font-weight:bold;
		text-transform:uppercase;
	}

.DATA1 {
mso-number-format:”dd\\/mm\\/yy\\;\\@”;
}

/*Data “Simples”*/

.DATA2 {
mso-number-format:”Short Date”;
}

/*Moeda Nacional*/

.MOEDARS {
mso-number-format:”_\\(\\[$R$ -416\\]* \\#\\,\\#\\#0\\.00_\\)\\;_\\(\\[$R$ -416\\]* \\\\\\(\\#\\,\\#\\#0\\.00\\\\\\)\\;_\\(\\[$R$ -416\\]* \\0022-\\0022??_\\)\\;_\\(\\@_\\)”;
}
/*Moeda Estrangeira (US$)*/

.MOEDAUS {
mso-number-format:”_\\(\\[$$ -416\\]* \\#\\,\\#\\#0\\.00_\\)\\;_\\(\\[$$ -416\\]* \\\\\\(\\#\\,\\#\\#0\\.00\\\\\\)\\;_\\(\\[$$ -416\\]* \\0022-\\0022??_\\)\\;_\\(\\@_\\)”;
}

/*Percentual*/

.PORCENTAGEM {
mso-number-format:Percent;
}

/*Forçar formatação como texto*/

.TEXTO{
 mso-number-format:"\@";/*force text*/

}

</style>

<table width="118%" border="1" cellpadding="2" cellspacing="2">
<tr>
  <th height="77" colspan="13" ><h2>RECONCILIA&Ccedil;&Atilde;O</h2></th>
  </tr>
<tr>
  <th height="77" colspan="11" ><h4>CONTA:<?=$vaVReconciliacao[0]->getContaFormatada;?> /  UNIDADE:<?=$vaVReconciliacao[0]->getUnidadeDescricao?></h4></th>
  <th colspan="2" >Período:
    <?=$_REQUEST['dtInicio']?> à
    <?=$_REQUEST['dtFim']?></th>
</tr>
<tr>
  <th colspan="9" ><h2>&nbsp;</h2></th>
  <th height="44" colspan="2" >SALDO INICIAL</th>
  <th colspan="2" class="MOEDARS"><?= (($vaVReconciliacao[0]->getSaldoInicial) ?   number_format($vaVReconciliacao[0]->getSaldoInicial, 2, ',', '.'): 0.00) ?></th>
</tr>
<tr height="70">
<th class="Table" >ITEM</th>
<th class="Table" >DATA PGTO</th>
<th class="Table" >F.A</th>
<th class="Table" >FORNECEDOR/CREDOR</th>
<th class="Table" >EMPRESA</th>
<th class="Table" >N&ordm;DOC</th>
<th class="Table" >PARC </th>
<th class="Table" >CENTRO DE CUSTO</th>
<th class="Table" >CONTA CONT&Aacute;BIL</th>
<th class="Table" >HIST&Oacute;RICO</th>
<th class="Table" >CREDITO</th>
<!--<th class="Table" >VALOR PRINCIPAL</th>-->
<!--<th class="Table" >J/D/R</th>-->
<th class="Table" >DEBITO</th>
<th class="Table" >SALDO</th>
</tr>

<?
$nTotalCredito = 0.00;
$nTotalDebito  = 0.00;
$nTotalJurosDescontosRetencoes = 0.00;
foreach($vaVReconciliacao as $oaVReconciliacao){
if($oaVReconciliacao->getMovTipo == 189){
  $nSaldoAtual[$i] = ($nSaldoAtual[$i - 1] + $oaVReconciliacao->getValorReceber );
  $nTotalCredito   = $nTotalCredito + $oaVReconciliacao->getValorReceber;
}else{
  $nSaldoAtual[$i] = ($nSaldoAtual[$i - 1] - $oaVReconciliacao->getValorPago );
  $nTotalDebito = $nTotalDebito + $oaVReconciliacao->getMovValorParcela;
}
$nTotalJurosDescontosRetencoes = $nTotalJurosDescontosRetencoes + $oaVReconciliacao->getJurosDescontosRetencoes;

?>

<tr>
  <td height="25">&nbsp;
    <?= $i++;?></td>
  <td style="text-align:left"><?= $oaVReconciliacao->getDataConciliacaoFormatada?></td>
  <td align='center'><?= $oaVReconciliacao->getFichaAceite?></td>
  <td style="text-align:left" ><?= $oaVReconciliacao->getFornecedor ?></td>
  <td style="text-align:right"><?= $oaVReconciliacao->getEmpresa ?></td>
  <td style="text-align:right;" class="TEXTO"><?= ($oaVReconciliacao->getDocumento) ? $oaVReconciliacao->getDocumento : "-"?></td>
  <td style="text-align:right;" class="TEXTO"><?=  $oaVReconciliacao->getParcela?>/<?=$oaVReconciliacao->getTotalParcela?></td>
  <td style="text-align:right"><?= ($oaVReconciliacao->getCentroDescricao == '-') ? "[ ". $oaVReconciliacao->getContaOrigem." ]" : $oaVReconciliacao->getCentroDescricao; ?></td>
  <td style="text-align:right"><?= $oaVReconciliacao->getContaDescricao ?></td>
  <td style="text-align:right"><?= $oaVReconciliacao->getHistorico?></td>
  <td style="text-align:right" class="MOEDARS"><?= ($oaVReconciliacao->getValorReceber) ? number_format($oaVReconciliacao->getValorReceber, 2, ',', '.') : "-"?></td>
  <td style="text-align:right" class="MOEDARS"><?= ($oaVReconciliacao->getValorPago) ? number_format($oaVReconciliacao->getValorPago, 2, ',', '.') : "-"?></td>
  <td style="text-align:right; <?=($nSaldoAtual[$i-1] <0) ? "color:red;" : ""?>" class="MOEDARS"><?=  number_format($nSaldoAtual[$i - 1], 2, ',', '.')  ?></td>
</tr>
<? }
$nValorFinal = $nTotalDebito + $nTotalJurosDescontosRetencoes;
 ?>
<tr height="30">
  <td colspan="9" align="right">&nbsp;</td>
  <td align="right" class="Table">TOTAL GERAL:</td>
  <td align="right" class="Table"><?= (($nTotalCredito) ? number_format($nTotalCredito, 2, ',', '.') : 0) ?></td>
  <td align="right" class="Table"><?= (($nValorFinal) ? number_format($nValorFinal, 2, ',', '.') : 0) ?></td>
  <td align="right" class="Table"><span class="MOEDARS">
    <?= (($nTotalCredito && $nValorFinal) ?   number_format(($nTotalCredito - $nValorFinal), 2, ',', '.'): 0.00) ?>
  </span></td>
</tr>
</table>
<? }else {
	$_SESSION['sMsg'] = "Não existem informações para esta Empresa(". $_SESSION['oEmpresa']->getEmpFantasia() ."), Unidade(". $oUnidade->getDescricao() ."), Conta(". $oMnyContaCorrente->getCcrContaFormatada() .") Per&iacute;odo de (".$_REQUEST['dtInicio'] ." &agrave; ". $_REQUEST['dtFim'].")";
	header ('Location:index.php');
}?>

</body>
</html>
