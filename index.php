
<?
 session_start();
 
 //1 - NECESSÁRIO PARA O LOG DO CAMINHO PERCORRIDO PELO USUÁRIO DENTRO DO SISTEMA
 //require_once("classes/aop/func.require_aop.php");
 /*
 // Loading compiled class or compiling and saving
 require_aop("classes/fachadaUsuario/BD/UsuarioBD.php", "classes/aop/definitionXML/aspect.xml");
 
 
 function aopCaminho($sDiretorio,$sXml){
 	if($handle = opendir($sDiretorio)) {
 		while (false !== ($file = readdir($handle))) {
 			if(is_file($sDiretorio."/".$file)){
 				if(substr($file,strlen($file)-6,6) == "TR.php" || substr($file,strlen($file)-6,6) == "BD.php")
 					require_aop($sDiretorio."/".$file, $sXml);
 			} elseif(is_dir($sDiretorio."/".$file)){
 				if($file != "." && $file != ".."){
 					aopCaminho($sDiretorio."/".$file,$sXml);
 				}
 			}
 		}
 
     }
 }*/
 //aopCaminho("classes/fachadaFinanceiro/controle/","classes/aop/definitionXML/caminho.xml");
 //aopCaminho("classes/fachadaFinanceiro/BD/","classes/aop/definitionXML/caminho.xml");
 //FIM 1
 
 //2 - NECESSÁRIO PARA CARREGAR AS CLASSES SEM REQUIRE
 function achaClasse($sNomeClasse, $sDiretorio){
    $sDiretorio =  ($sDiretorio)? $sDiretorio : "classes";
 	if($handle = opendir($sDiretorio)){
 		while (false !== ($file = readdir($handle))){

 			if(is_file($sDiretorio."/".$file)){
 				if($file == $sNomeClasse.".php"){

                    if($bInsertCache && (!$_SESSION['classe'][$sNomeClasse])){
                        $cache = fopen("classes/frontController/cache.php",'a+') or die("erro ao abrir o arquivo !");
                        fwrite($cache,"\n//Inserido em : ".date('d/m/Y H:i:s')."\n\$_SESSION['classe'][\"".$sNomeClasse."\"] = \"".$sDiretorio."\";\n");
                        fclose($cache);
                    }

 					require_once($sDiretorio."/".$sNomeClasse.".php");
 					break;
 				}
 			}elseif(is_dir($sDiretorio."/".$file)){
                $bInsertCache = true;
 				if($file != "." && $file != ".."){
 					achaClasse($sNomeClasse,$sDiretorio."/".$file);
 				}
 			}
 		}
 
     }
 }

 
 function __autoload($sNomeClasse){
 	include_once  ('classes/frontController/cache.php');
    $diretorio =  $_SESSION['classe'][$sNomeClasse];
 	achaClasse($sNomeClasse,$diretorio);

 }
 //FIM 2
  
  
  //INICIANDO A APLICAÇÃO
 $oFrontController = new FrontController();
 

 ?>
