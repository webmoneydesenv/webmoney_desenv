<?php

include 'Adl/Configuration.php';
include 'Adl/Config/Parser.php';
include 'Adl/Config/JasperServer.php';
include 'Adl/Integration/RequestJasper.php';

		$relatorio = $_REQUEST["relatorio"];
		$mov = $_REQUEST['mov'];
		$item = $_REQUEST['item'];
		$movimento = $_REQUEST['movimento'];
		$nCodAporte =$_REQUEST["cod_aporte"];
		$nCodUnidade = $_REQUEST['cod_unidade'];
		$nCodUnidade2 = $_REQUEST['unidade'];
		$nCodConta = $_REQUEST['conta'];
		$dData = $_REQUEST['data'];

		$dDataInicial = $_REQUEST['data_inicial'];
		$dDataFinal   = $_REQUEST['data_final'];
		$nEmpCodigo   = $_REQUEST['emp_codigo'];
		$nMovTipo     = $_REQUEST['mov_tipo'];
		$nPesgCodigo = $_REQUEST['pesg_codigo'];
		$unidade = $_REQUEST['unidade'];
		$nEmpCodigo = $_REQUEST['emp_codigo'];

	switch($relatorio)
	{
		case "/relatorio5/ficha_de_aceite":
			$relatorio= "/relatorio5/ficha_de_aceite";
			$param = array("mov" => $mov, "item" => $item);
		break;
		case "/ficha_de_aceite/ficha_de_aceite":
			$relatorio= "/ficha_de_aceite/ficha_de_aceite";
			$param = array("mov" => $mov, "item" => $item);
		break;
		case "/ficha_de_aceite/ficha_aceite_nova":
			$relatorio= "/ficha_de_aceite/ficha_aceite_nova";
			$param = array("mov" => $mov, "item" => $item);
		break;
		case "/ficha_aceite_nova_teste/ficha_aceite_nova_teste":
			$relatorio= "/ficha_aceite_nova_teste/ficha_aceite_nova_teste";
			$param = array("mov" => $mov, "item" => $item);
		break;
		case "/recibo/recibo":
			$relatorio= "/recibo/recibo";
			$param = array("movimento" => $movimento, "item"=>$item);
		break;
		case "/transferencia/transf":
			$relatorio= "/transferencia/transf";
			$param = array("movimento" => $movimento);
		break;
		case "/solicitacao_aporte/solicitacao_aporte":
			$relatorio= "/solicitacao_aporte/solicitacao_aporte";
			$param = array("cod_solicitacao_aporte" => $nCodAporte);
		break;
		case "/extrato_conciliacao/extrato_conciliacao":
			$relatorio= "/extrato_conciliacao/extrato_conciliacao";
			$param = array("cod_unidade" => $nCodUnidade,"conta" => $nCodConta, "data" => $dData , "emp_codigo" => $nEmpCodigo);
		break;
		case "/extrato_sm/extrato_sm":
			 $relatorio= "/extrato_sm/extrato_sm";
			 $param = array("cod_unidade" => $nCodUnidade,"conta" => $nCodConta, "data" => $dData, "emp_codigo" => $nEmpCodigo);
		break;
		case "/extrato/extrato":
			$relatorio= "/extrato/extrato";
			$param = array("cod_unidade" => $nCodUnidade, "conta" => $nCodConta, "data" => $dData );
		break;
		case "/pagas_pagar/pagar/pagar":
			$relatorio= "/pagas_pagar/pagar/pagar";
			$param = array("data_inicial" => $dDataInicial, "data_final" => $dDataFinal, "mov_tipo" => $nMovTipo, "emp_codigo" => $nEmpCodigo, "unidade" => $unidade);
		break;
		case "/pagas_pagar/pagas/pagas":
			$relatorio= "/pagas_pagar/pagas/pagas";
			$param = array("data_inicial" => $dDataInicial, "data_final" => $dDataFinal, "mov_tipo" => $nMovTipo, "emp_codigo" => $nEmpCodigo, "unidade" => $unidade );
		break;
		case "/credor_novo/credor_novo":
			$relatorio= "/credor_novo/credor_novo";
			$param = array("data_inicial" => $dDataInicial, "data_final" => $dDataFinal, "mov_tipo" => $nMovTipo, "emp_codigo" => $nEmpCodigo ,"pesg_codigo" => $nPesgCodigo, "uni_codigo" => $unidade );
		break;
	}

	try {

		$jasper = new Adl\Integration\RequestJasper();
		header('Content-type: application/pdf');
		header("Content-Disposition:inline; filename=relatorio.pdf");
		echo $jasper->run($relatorio,'pdf', $param);
		exit();
	} catch (\Exception $e) {
		echo $e->getMessage();
		die;
	}
?>
