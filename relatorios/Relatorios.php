<?Php
 class Relatorio{

		private $title;
		private $sRelatorio;

		public function __construct($sTipo){
			switch($sTipo){
				case "ficha_aceite_nova_old":								
					$this->title='Ficha de Aceite';			
					$this->sRelatorio = "/ficha_de_aceite/ficha_aceite_nova";
				break;
				case "ficha_aceite_nova":								
					$this->title='Ficha de Aceite';			
					$this->sRelatorio = "";
				break;
				case "ficha_aceite_nova_teste":								
					$this->title='Ficha de Aceite';			
					$this->sRelatorio = "";
				break;		
				case "Recibo":								
					$this->title='Recibo';			
					$this->sRelatorio = "";
				break;	
				case "Transferencia":								
					$this->title='Ficha de Transferencia';			
					$this->sRelatorio = "";
				break;
				case "Aporte":								
					$this->title='Solicita&ccedil;&atilde;o de Aporte';			
					$this->sRelatorio = "";
				break;
				case "ExtratoConciliacao":								
					$this->title='Extrato Concilia&ccedil;&atilde;o';			
					$this->sRelatorio = "	";
				break;
				case "ExtratoSm":								
					$this->title='Extrato Concilia&ccedil;&atilde;o';			
					$this->sRelatorio = "";
				break;
				case "Extrato":								
					$this->title='Extrato Concilia&ccedil;&atilde;o';			
					$this->sRelatorio = "";
				break;
				case "Pagar":								
					$this->title='Contas a Pagar';			
					$this->sRelatorio = "/pagas_pagar/pagar/pagar";
				break;
				case "Pagas":								
					$this->title='Contas  Pagas';			
					$this->sRelatorio = "";
				break;					
				case "Credor":								
					$this->title='Extrato Credor';			
					$this->sRelatorio = "";
				break;					
			}			
		}
		public function getTitle(){
			return $this->title;
		}
		public function getRelatorio(){
			return $this->sRelatorio; 	
		}
		
}

?>