function mainmenu(){
$(" #menulatnav ul ").css({display: "none"}); // Opera Fix
$(" #menulatnav li").hover(function(){
		$(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
		},function(){
		$(this).find('ul:first').css({visibility: "hidden"});
		});
}

function recuperaConteudoDinamico(sArquivo,sParametros,sIdDivInsert){

	oDiv = document.getElementById(sIdDivInsert);
    sArquivo = sArquivo+'?'+sParametros;
    $.ajax({
			dataType: "html",
			type: "GET",
			beforeSend: function(oXMLrequest){
                                        oDiv.innerHTML = "Aguarde... carregando...";
				    },
			url: sArquivo,
			error: function(oXMLRequest,sErrorType){
                                    alert(oXMLRequest.responseText);
                                    alert(oXMLRequest.status+' , '+sErrorType);
                               },
			success: function(data){

                                    oDiv.innerHTML = data;
                                 }
			});
}
function atualizaAcoes(sNome){
	oSelect = document.getElementById('acoes'+sNome);
	nChecados = retornaChecados(sNome);
	if(nChecados == 0){
			for(var i = 0; i < oSelect.length; i++){
				if(oSelect.options[i].lang == '1' || oSelect.options[i].lang == '2')
					oSelect.options[i].disabled = true;
				else
					oSelect.options[i].disabled = false;
			}
	}
	if(nChecados == 1){
			for(var i = 0; i < oSelect.length; i++){
				if(oSelect.options[i].lang == '0')
					oSelect.options[i].disabled = true;
				else
					oSelect.options[i].disabled = false;
			}
	}
	if(nChecados > 1){
			for(var i = 0; i < oSelect.length; i++){
				if(oSelect.options[i].lang == '0' || oSelect.options[i].lang == '1')
					oSelect.options[i].disabled = true;
				else
					oSelect.options[i].disabled = false;
			}
	}
        oSelect.options[0].selected = true;
}

function validaAcoes(sNome){
	oSelect = document.getElementById('acoes'+sNome);
	nChecados = retornaChecados(sNome);
	switch(oSelect.options[oSelect.selectedIndex].lang){
		case '0':
			if(nChecados > 0){
				alert("Nenhum registro deve estar marcado para realizar esta a��o!");
				oSelect.options[0].selected = true;
				return false;
			}
			return true;
		break;
		case '1':
			if(nChecados != 1 ){
				alert("Apenas um registro deve estar marcado para realizar esta a��o!");
				oSelect.options[0].selected = true;
				return false;
			}
			return true;
		break;
		case '2':
			if(nChecados < 1 ){
				alert("Pelo menos um registro deve estar marcado para realizar esta a��o!");
				oSelect.options[0].selected = true;
				return false;
			}
			return true;
		break;
	}
}


function submeteForm(sNome){
	if(validaAcoes(sNome)){
		oSelect = document.getElementById('acoes'+sNome);
		oForm = document.getElementById('form'+sNome);
		oForm.action = oSelect.value;

		if(oSelect.value.indexOf("sOP=Excluir") > 0){
			var nRegistros = "";
			camposMarcados = new Array();
			$("input[type=checkbox][name='fId"+sNome+"[]']:checked").each(function(){
				camposMarcados.push($(this).val());
			});
				nRegistros = camposMarcados.toString().split(',').join('____');
				sLink = oSelect.value + "&fId"+sNome+"=" + nRegistros;
				$("#confirm-delete").on("shown.bs.modal", function () {
					$(this).find('#ok').attr('href', sLink);
				});
				$('#confirm-delete').modal('show');

			return false;
		}else{
			oForm.submit();
			return true;
		}
	}

}

function retornaChecados(sNome){
	var nChecados = 0;
	oForm = document.getElementById('form'+sNome);
	for(var i = 1; i < oForm.length; i++){
		if(oForm.elements[i].type == 'checkbox'){
			if(oForm.elements[i].checked)
				nChecados++;
		}
	}
	return nChecados;
}


function validaForm(oForm){
	var sMsg = '';
	var bErroVazio = false;
	var bErroData = false;
	for(i = 0; i < oForm.length ; i++){
		switch(oForm.elements[i].lang){
			case 'vazio':
				if(!validaVazio(oForm.elements[i].value)){
					if(!bErroVazio){
						sMsg += '\n- Os campos em destaque s�o obrigat�rios';
						bErroVazio = true;
					}
					oForm.elements[i].style.backgroundColor = '#F89C8F';
				} else
					oForm.elements[i].style.backgroundColor = '#FFFFFF';
			break;
			case 'data':
				if(!validaData(oForm.elements[i].value)){
					if(!bErroData){
						sMsg += '\n- A data informada � inv�lida!';
						bErroData = true;
					}
					oForm.elements[i].style.backgroundColor = '#F89C8F';
				} else
					oForm.elements[i].style.backgroundColor = '#FFFFFF';
			break;
		}
	}
	if(bErroVazio || bErroData){
		alert(sMsg);
		//alerta(sMsg,document.formCategoria.fDescricao);
		return false;
	} else
		return true;
}

function validaVazio(sTexto){
	if(!sTexto)
		return false;
	return true;
}

function validaData(sTexto){
	if(!sTexto)
		return false;
	else {
		vTexto = sTexto.split('/');
		if(vTexto.length != 3)
			return false;
		else{
			if(vTexto[0] < 1 || vTexto[0] > 31)
				return false;
			else{
				if(vTexto[1] < 1 || vTexto[1] > 12)
					return false;
				else{
					if(vTexto[2].length != 4)
						return false;
					else
						return true;
				}
			}
		}
	}
}

function trocaNomeCampo(sNome,oCampo){
	if(oCampo.value == sNome)
		oCampo.value = '';
	else {
		if(oCampo.value == '')
			oCampo.value = sNome;
	}
}

function confirmaSenha(sSenha,sConfirmaSenha){
	if(document.getElementById(sSenha).value != document.getElementById(sConfirmaSenha).value){
		alert('Os campos Senha e Confirma Senha n�o conferem');
		return false;
	} else
		return true;
}

function valida_cpf(campo)
{
  campo = campo.replace('.','');
  campo = campo.replace('-','');
  var numeros, digitos, soma, i, resultado, digitos_iguais;
  digitos_iguais = 1;
  if (campo.length < 11)
        return false;
  for (i = 0; i < campo.length - 1; i++)
        if (campo.charAt(i) != campo.charAt(i + 1))
              {
              digitos_iguais = 0;
              break;
              }
  if (!digitos_iguais)
        {
        numeros = campo.substring(0,9);
        digitos = campo.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
              soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
              return false;
        numeros = campo.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
              soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
              return false;
        return true;
        }
  else
        return false;
}



function Verifica_campo_CPF(campo) {

var CPF = campo.value; // Recebe o valor digitado no campo
  CPF = CPF.replace('.','');
  CPF = CPF.replace('.','');
  CPF = CPF.replace('-','');

// Aqui come�a a checagem do CPF
var POSICAO, I, SOMA, DV, DV_INFORMADO;
var DIGITO = new Array(10);
DV_INFORMADO = CPF.substr(9, 2); // Retira os dois �ltimos d�gitos do n�mero informado

// Desemembra o n�mero do CPF na array DIGITO
for (I=0; I<=8; I++) {
  DIGITO[I] = CPF.substr( I, 1);
}

// Calcula o valor do 10� d�gito da verifica��o
POSICAO = 10;
SOMA = 0;
   for (I=0; I<=8; I++) {
      SOMA = SOMA + DIGITO[I] * POSICAO;
      POSICAO = POSICAO - 1;
   }
DIGITO[9] = SOMA % 11;
   if (DIGITO[9] < 2) {
        DIGITO[9] = 0;
}
   else{
       DIGITO[9] = 11 - DIGITO[9];
}

// Calcula o valor do 11� d�gito da verifica��o
POSICAO = 11;
SOMA = 0;
   for (I=0; I<=9; I++) {
      SOMA = SOMA + DIGITO[I] * POSICAO;
      POSICAO = POSICAO - 1;
   }
DIGITO[10] = SOMA % 11;
   if (DIGITO[10] < 2) {
        DIGITO[10] = 0;
   }
   else {
        DIGITO[10] = 11 - DIGITO[10];
   }

// Verifica se os valores dos d�gitos verificadores conferem
DV = DIGITO[9] * 10 + DIGITO[10];

//alert(DV_INFORMADO);

  if (DV != DV_INFORMADO) {

    alert('CPF inv�lido');
   campo.value = '';
      campo.focus();
      return false;
   }
}

//onblur="validacaoEmail(this)"
function validacaoEmail(field) {
	usuario = field.value.substring(0, field.value.indexOf("@"));
	dominio = field.value.substring(field.value.indexOf("@")+ 1, field.value.length);

	if (!((usuario.length >=1) &&
		(dominio.length >=3) &&
		(usuario.search("@")==-1) &&
		(dominio.search("@")==-1) &&
		(usuario.search(" ")==-1) &&
		(dominio.search(" ")==-1) &&
		(dominio.search(".")!=-1) &&
		(dominio.indexOf(".") >=1)&&
		(dominio.lastIndexOf(".") < dominio.length - 1))) {
		alert("E-mail invalido");
	}
}


function marcarTodosCheckBoxFormulario(sIdForm){
    nChecados = retornaChecados(sIdForm);
    bMarcado = true;
    if(nChecados >= 1)
        bMarcado = false;

    oForm = document.getElementById('form'+sIdForm);
    vElements = oForm.getElementsByTagName('INPUT');
    for(i = 0; i < vElements.length; i++){
        if(vElements[i].type == 'checkbox'){
            vElements[i].checked = bMarcado;
        }
    }

    atualizaAcoes(sIdForm);

}


function MostraEsconde(div){
		if(document.getElementById(div).style.display == "none"){
			document.getElementById(div).style.display = "block";
			recuperaConteudoDinamico("index.php","action=Ajax.preparaFormulario&sOP=CadastrarPessoaAjax",div);
		}else{
			document.getElementById(div).style.display = "none";
			document.getElementById(div).innerHTML = "";
		}
}
function MostraEsconde2(div){
		if(document.getElementById(div).style.display == "none"){
			document.getElementById(div).style.display = "block";
		}else if(document.getElementById(div).style.display == "block"){
			document.getElementById(div).style.display = "none";;
		}
}
// href="javascript: void(0);" onclick="MostraEscondeArvore('ul_transacao_1');"
function MostraEscondeArvore(div){
		if(document.getElementById(div).style.display == "none"){
			document.getElementById(div).style.display = "block";
                        sLink = div.replace('ul','a');
                        document.getElementById(sLink).innerHTML = "--";

		}else{
			document.getElementById(div).style.display = "none";
                        sLink = div.replace('ul','a');
                        document.getElementById(sLink).innerHTML = "+";
		}
}

// onKeyUp="sem_virgula(this);"
function sem_virgula(campo){
	var digits="ZXCVBNM�LKJHGFDSAQWERTYUIOP\|zxcvbnm<>.:;?/�asdfghjkl�^~}]�q/W?ertyuiop�`{[�'1���234567890-=!@#$%�&*()_+���"
	var campo_temp
	for (var i=0;i<campo.value.length;i++){
	  campo_temp=campo.value.substring(i,i+1)
	  if (digits.indexOf(campo_temp)==-1){
			campo.value = campo.value.substring(0,i);
			break;
	   }
	}
}

//onKeyPress="TodosNumero(event);"
function TodosNumero(e){
	var isNS4 = (navigator.appName=="Netscape")?1:0;
	var codigo=e.keyCode? e.keyCode : e.charCode
	var expReg = /^\d*$/;
	var caracter = String.fromCharCode(codigo);

	if ((codigo != 8) && (codigo != 46) && (codigo != 44) &&(codigo != 37) && (codigo != 39)&& (codigo != 9)&& (codigo != 127)) {
		if (!expReg.test(caracter)) {
			if (isNS4) {
				e.preventDefault();
			} else {
				e.returnValue = false;
			}
		}
	}
}

//onBlur='return validaData(this);'
function validaData(str) {
	if (str.value != "") {
		dia = (str.value.substring(0,2));
		mes = (str.value.substring(3,5));
		ano = (str.value.substring(6,10));

		cons = true;

		// verifica se foram digitados n�meros
		if (isNaN(dia) || isNaN(mes) || isNaN(ano)){
			alert("Preencha a data somente com numeros.");
			str.value = "";
			str.focus();
			return false;
		}

		// verifica o dia valido para cada mes
		if ((dia < 01)||(dia < 01 || dia > 30) &&
			(mes == 04 || mes == 06 ||
			 mes == 9 || mes == 11 ) ||
			 dia > 31) {
			cons = false;
		}

		// verifica se o mes e valido
		if (mes < 01 || mes > 12 ) {
			cons = false;
		}

		// verifica se e ano bissexto
		if (mes == 2 && ( dia < 01 || dia > 29 ||
		   ( dia > 28 &&
		   (parseInt(ano / 4) != ano / 4)))) {
			cons = false;
		}

		if (cons == false) {
			alert("A data inserida n�o � v�lida: " + str.value);
			str.value = "";
			str.focus();
			return false;
		}
	}
}


// onBlur="formataMoeda(this);"
function formataMoeda(campo) {
	var num = campo.value;

	//num = num.toString().replace(/\$|\,/g,"");
	num = num.toString().replace(".","");
	num = num.toString().replace(",",".");

	if(isNaN(num)) num = "0";

	cents = Math.floor((num*100+0.5)%100);
	num = Math.floor((num*100+0.5)/100).toString();

	if(cents < 10) cents = "0" + cents;

	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+"."+num.substring(num.length-(4*i+3));

	campo.value = num + "," + cents;
}


// onBlur="formataPercentual(this);"
function formataPercentual(campo) {
	var num = campo.value;
	var vTexto;

	//num = num.toString().replace(/\$|\,/g,"");
	//num = num.toString().replace(".","");
	num = num.toString().replace(",",".");

	if(isNaN(num)) num = "0";
	cents = Math.floor((num*100+0.5)%100);
	if(cents < 10) cents = "0" + cents;

	vTexto = num.split('.');
	campo.value = vTexto[0] + "," + cents;
}


// --------------------- ALERTA ---------------------------------------//

var nav = navigator.appName;
var quebra = "";

if (nav == "Microsoft Internet Explorer") {quebra = "<br>"}

document.write("<div id='alerta' style='border: 1px; background-color: #FFFFFF; position:absolute; top:0px; left:0px; z-index:10; visibility:hidden;'>")
document.write("<table id='backTabela' class='' cellpadding='0' cellspacing='0' width='280' >")
document.write("<tr>")
document.write("<td><span id='Obotao'></span></td>")
document.write("</tr>")
document.write("<tr>")
document.write("<td colspan='3' height='80' align='center'><font face='sans-serif' size='2' color='#FFFFFF'><b><span id='texto' class='alerta'></span></b>")
document.write("<br><br>" + quebra)
document.write("</td></tr>")
document.write("</table>")
document.write("</div>")

var largura = 280;
var altura = 135;
//

function alerta(txt,nTipo) {
	if(nTipo != ''){
		switch(nTipo){
			case "Erro":
				document.getElementById("Obotao").innerHTML="<input class='botao_image' type='image' id='botao' name='botao' src='imagens/btn_alerta.png' value='   OK   ' OnCLick='esconder_alerta()'>"
				document.getElementById("backTabela").style.backgroundImage = 'url(imagens/bg_erro.png)';
			break;
			case "Alerta":
				document.getElementById("Obotao").innerHTML="<input class='botao_image' type='image' id='botao' name='botao' src='imagens/btn_alerta.png' value='   OK   ' OnCLick='esconder_alerta()'>"
				document.getElementById("backTabela").style.backgroundImage = 'url(imagens/bg_alerta.png)';
			break;
			case "Sucesso":
				document.getElementById("Obotao").innerHTML="<input class='botao_image' type='image' id='botao' name='botao' src='imagens/btn_alerta.png' value='   OK   ' OnCLick='esconder_alerta()'>"
				document.getElementById("backTabela").style.backgroundImage = 'url(imagens/bg_sucesso.png)';
			break;
		}

		document.getElementById("backTabela").style.padding = '10px';
		document.getElementById("backTabela").style.border = '#000000 2px solid';

	}

	if (nav == "Netscape" ) {
		document.getElementById('botao').focus();
	}

	with(document.getElementById('alerta').style) {
		if (nav == "Microsoft Internet Explorer") {
			vLeft = ((document.body.clientWidth - largura) / 2);
			oTop = ((document.body.clientHeight - altura) / 2);
			vTop = (document.body.scrollTop + oTop);
		}

		if (nav == "Netscape") {
			vLeft = ((window.innerWidth - largura) / 2);
			oTop = ((window.innerHeight - altura) / 2);
			vTop = (window.pageYOffset + oTop);

		}

		 left = vLeft + 'px';
		 top = vTop + 'px';
		 position = 'absolute';
		 visibility = 'visible';

	}

	document.getElementById("texto").innerHTML=txt
	document.getElementById('botao').focus();
}

//
function esconder_alerta() {
	document.getElementById('alerta').style.visibility = "hidden"  ;
}

/* *********************************************************************************************************
 * FORMATA��O DE VALORES E DATAS EM INSER��O
 * EXEMPLO DE USO PARA VALOR = <input type="text" name="valor" value="" onKeyUp="this.value = mascara_global('[###.]###,##', this.value)" >
 * EXEMPLO DE USO PARA DATA = <input type="text" name="data" maxlength="10" value="" onKeyUp="this.value = mascara_global('##/##/####', this.value)" >

********************************************************************************************************* */
function mascara_global(mascara, valor){
	if(mascara == '###.###.###-##|##.###.###/####-##'){
		if(valor.length>14){
			return mascara_global('##.###.###/####-##', valor);
		}else{
			return mascara_global('###.###.###-##', valor);
		}
	}

	tvalor = "";
	ret = "";
	caracter = "#";
	separador = "|";
	mascara_utilizar = "";
	valor = removeEspacos(valor);
	if (valor == "")return valor;
	temp = mascara.split(separador);
	dif = 1000;

	valorm = valor;
	//tirando mascara do valor j� existente
	for (i=0;i<valor.length;i++){
		if (!isNaN(valor.substr(i,1))){
			tvalor = tvalor + valor.substr(i,1);
		}
	}

	valor = tvalor;

	//formatar mascara dinamica
	for (i = 0; i<temp.length;i++){
		mult = "";
		validar = 0;
		for (j=0;j<temp[i].length;j++){
			if (temp[i].substr(j,1) == "]"){
				temp[i] = temp[i].substr(j+1);
				break;
			}
			if (validar == 1)mult = mult + temp[i].substr(j,1);
			if (temp[i].substr(j,1) == "[")validar = 1;
		}
		for (j=0;j<valor.length;j++){
			temp[i] = mult + temp[i];
		}
	}

	//verificar qual mascara utilizar
	if (temp.length == 1){
		mascara_utilizar = temp[0];
		mascara_limpa = "";
		for (j=0;j<mascara_utilizar.length;j++){
			if (mascara_utilizar.substr(j,1) == caracter){
				mascara_limpa = mascara_limpa + caracter;
			}
		}
		tam = mascara_limpa.length;
	}else{
		//limpar caracteres diferente do caracter da m�scara
		for (i=0;i<temp.length;i++){
			mascara_limpa = "";
			for (j=0;j<temp[i].length;j++){
				if (temp[i].substr(j,1) == caracter){
					mascara_limpa = mascara_limpa + caracter;
				}
			}
			if (valor.length > mascara_limpa.length){
				if (dif > (valor.length - mascara_limpa.length)){
					dif = valor.length - mascara_limpa.length;
					mascara_utilizar = temp[i];
					tam = mascara_limpa.length;
				}
			}else if (valor.length < mascara_limpa.length){
				if (dif > (mascara_limpa.length - valor.length)){
					dif = mascara_limpa.length - valor.length;
					mascara_utilizar = temp[i];
					tam = mascara_limpa.length;
				}
			}else{
				mascara_utilizar = temp[i];
				tam = mascara_limpa.length;
				break;
			}
		}
	}

	//validar tamanho da mascara de acordo com o tamanho do valor
	if (valor.length > tam){
		valor = valor.substr(0,tam);
	}else if (valor.length < tam){
		masct = "";
		j = valor.length;
		for (i = mascara_utilizar.length-1;i>=0;i--){
			if (j == 0) break;
			if (mascara_utilizar.substr(i,1) == caracter){
				j--;
			}
			masct = mascara_utilizar.substr(i,1) + masct;
		}
		mascara_utilizar = masct;
	}

	//mascarar
	j = mascara_utilizar.length -1;
	for (i = valor.length - 1;i>=0;i--){
		if (mascara_utilizar.substr(j,1) != caracter){
			ret = mascara_utilizar.substr(j,1) + ret;
			j--;
		}
		ret = valor.substr(i,1) + ret;
		j--;
	}
	return ret;
}

function removeEspacos(valor){
var valorSemEspacos="";

var tamanho = valor.length;
	for (i = 0; i<30;i++){
		if(valor.substr(i,1)==" "){
		}else{
		valorSemEspacos = valorSemEspacos + valor.substr(i,1);
		}
	}
return valorSemEspacos;
}
