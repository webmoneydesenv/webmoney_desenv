<?php

require_once "class.AOP_XMLReader.php";
require_once "class.Advice.php";
require_once "class.Pointcut.php";
require_once "class.AutoPointcut.php";
require_once "class.CustomPointcut.php";


class Aspect
{
	var $_xmlFile;
	var $_initialized;
	var $_lastModified;
	var $_pointcuts;


	function Aspect($xmlFile, $lazyInit = true)
	{
		$this->_xmlFile = $xmlFile;
		$this->_initialized = false;
		$this->_lastModified = filemtime($this->_xmlFile);
		$this->_pointcuts = array();
		// Testing purposes
		$this->hash = md5(rand());

		// Do not initialize here (compiled version uses less resources)
		if ($lazyInit === false) {
	    	$this->init();
	    }
	}


	function & from($xmlFile, $lazyInit = true)
	{
		// This static method implements a modified version of Registry Pattern.
		// It adds the possibility to Aspects' Instances to be unique.
		// Prevents extra memory consumption of server.
		static $_instances = array();

		// Generate Xml Aspect Definition (XAD) Hash
		$hash = md5(realpath($xmlFile));

		if (!array_key_exists($hash, $_instances)) {
			$_instances[$hash] = & new Aspect($xmlFile, $lazyInit);
		}

		return $_instances[$hash];
	}


	function addPointcut(& $pointcut)
	{
		$this->_pointcuts[count($this->_pointcuts)] = & $pointcut;
	}


	function & getPointcut($i)
	{
		if ($this->_initialized === false) {
			$this->init();
		}

		if (array_key_exists($i, $this->_pointcuts)) {
			return $this->pointcuts[$i];
		}

		return null;
	}


	function & getPointcuts()
	{
		if ($this->_initialized === false) {
			$this->init();
		}

		return $this->_pointcuts;
	}


	function getXmlFile() { return $this->_xmlFile; }

	function getInitialized() { return $this->_initialized; }

	function getLastModified() { return $this->_lastModified; }


	function init()
	{
		// Checking if file exists
		if (!file_exists($this->_xmlFile)) {
			trigger_error(
				"<b>[Aspect Error]:</b> File <b>" . $this->_xmlFile . "</b> does not exist!",
				E_USER_ERROR
			);
		}

		// Get file contents
		$fContent = implode("", file($this->_xmlFile));

		// Parse XML file into an array map
		$XmlReader = AOP_XMLReader::fromString($fContent);

		// Solve Advice src directory reference. Changes actual working directory
		$oldWorkDir = getcwd();
		chdir(dirname(realpath($this->_xmlFile)));

		// Check for Root Element
		if (strtolower($XmlReader->getTag()) == "aspect") {
			foreach ($XmlReader->getChildNodes() as $k => $v) {
    	    	if ($v->getTag() == "pointcut") {
        			// Check for attributes
					$class = ($v->getAttribute("class") === null) ? "" : $v->getAttribute("class");
					$function = ($v->getAttribute("function") === null) ? "" : $v->getAttribute("function");
                    $nclass = ($v->getAttribute("nclass") === null) ? "" : $v->getAttribute("nclass");
					$nfunction = ($v->getAttribute("nfunction") === null) ? "" : $v->getAttribute("nfunction");

					// Implement some advice code creation via appending fragments
					$advice = & new Advice();

					foreach ($v->getChildNodes() as $c => $a) {
						// Append Advice fragment, depending from its source (CDATA or External File)
						if ($a->getTag() == "advice") {
							$asRequire = ($a->getAttribute("asrequire") === null) ? "" : trim($a->getAttribute("asrequire"));

							if ($asRequire == "true") {
								$adviceCode = " require \"". realpath(getcwd() . "/" . $a->getAttribute("src")) ."\"; ";
							} elseif ($asRequire == "" || $asRequire == "false") {
                                $adviceCode = implode("", file($a->getAttribute("src")));

								if (substr($adviceCode, 0, 2) == "<?") {
	                            	// Crop begin and end of archive
	                            	if (substr($adviceCode, 2, 3) == "php") {
										$adviceCode = substr($adviceCode, 5);
									} else {
	                                    $adviceCode = substr($adviceCode, 2);
									}
								} else {
									$adviceCode = "?>" . $adviceCode;
								}

								if (substr($adviceCode, -2, 2) == "?>") {
									$adviceCode = substr($adviceCode, 0, -2);
								} else {
									$adviceCode .= "<?php";
								}
							} else {
                                trigger_error(
									"<b>[Aspect Error]:</b> Undefined value <b>" . $asRequire . "</b> for advice tag!",
									E_USER_ERROR
								);
							}
						} else {
                        	$adviceCode = $a->getValue();
						}

						$advice->addData($adviceCode);
					}

					// Appending AutoPointcut or CustomPointcut to PointcutList
					if ($v->getAttribute("name") === null && $v->getAttribute("auto") !== null) {
                    	$this->addPointcut(new AutoPointcut($advice, $class, $function, $v->getAttribute("auto"), $nclass, $nfunction));
					} else if ($v->getAttribute("name") !== null && $v->getAttribute("auto") === null) {
                    	$this->addPointcut(new CustomPointcut($advice, $class, $function, $v->getAttribute("name"), $nclass, $nfunction));
					}
				}
			}
		}

		// Re-configure Work directory
		chdir($oldWorkDir);

		// Lazy load flag change
		$this->_initialized = true;
	}


	// Addon
	function & getAdviceFromCustomPointcut($className, $functionName, $pointcutName)
	{
		$advice = & new Advice();

		$pointcuts = & $this->getPointcuts();
		$l = count($pointcuts);

		for ($i = 0; $i < $l; $i++) {
			$pointcut = & $pointcuts[$i];

			if (is_a($pointcut, "CustomPointcut") &&
			    ($pointcut->hasClassName($className) || $pointcut->hasClassName("")) &&
				($pointcut->hasFunctionName($functionName) || $pointcut->hasFunctionName("")) &&
				$pointcut->hasName($pointcutName)) {
				if (!($pointcut->hasNotInClassName($className) || $pointcut->hasNotInFunctionName($functionName))) {
					$a = $pointcut->getAdvice();
					$advice->addData($a->getData());
				} else {
					$this->_checkForAttrMatching($pointcut, $className, $functionName);
				}
			}
		}

		return $advice;
	}


	function & getAdviceFromAutoPointcut($className, $functionName, $autoPointcut)
	{
		$advice = & new Advice();

		$pointcuts = & $this->getPointcuts();
		$l = count($pointcuts);

		for ($i = 0; $i < $l; $i++) {
			$pointcut = & $pointcuts[$i];

			if (is_a($pointcut, "AutoPointcut") &&
			    ($pointcut->hasClassName($className) || $pointcut->hasClassName("")) &&
				($pointcut->hasFunctionName($functionName) || $pointcut->hasFunctionName(""))) {
				if (!($pointcut->hasNotInClassName($className) || $pointcut->hasNotInFunctionName($functionName))) {
					if ($pointcut->hasAuto($autoPointcut)) {
						$a = $pointcut->getAdvice();
	                    $advice->addData($a->getData());
					}

					if ($pointcut->hasAuto("around")) {
						$a = $pointcut->getAdvice();

						eregi("(.*)[\t\s \n]proceed\(\);(.*)", $a->getData(), $data);

						//echo "<pre>Data: ".htmlentities($a->getData(), ENT_QUOTES)."\r\n\r\nPrintR: ".print_r($data, true)."</pre>";
						if ($autoPointcut == "before") {
							$data = (isset($data[1]) ? $data[1] : $a->getData());
						} elseif ($autoPointcut == "after") {
							$data = (isset($data[2]) ? $data[2] : $a->getData());
						}
						$advice->addData($data);
					}
				 } else {
				    $this->_checkForAttrMatching($pointcut, $className, $functionName);
				 }
			}
		}

		return $advice;
	}


	function _checkForAttrMatching(& $pointcut, $className, $functionName)
	{
    	// Need to check if there are a matching between class and nclass. Also, nfunction and function
		if ($pointcut->hasClassName($className) && $pointcut->hasNotInClassName($className)) {
			trigger_error(
				"<b>[Aspect Error]:</b> Cannot define a pointcut with the same class name [<b>".$className."</b>] in \"class\" and \"nclass\" attribute",
				E_USER_ERROR
			);
		} elseif ($pointcut->hasFunctionName($functionName) && $pointcut->hasNotInFunctionName($functionName)) {
			trigger_error(
				"<b>[Aspect Error]:</b> Cannot define a pointcut with the same function name [<b>".$functionName."</b>] in \"function\" and \"nfunction\" attribute",
				E_USER_ERROR
			);
		} // Does nothing if no match is found!
	}
}

?>
