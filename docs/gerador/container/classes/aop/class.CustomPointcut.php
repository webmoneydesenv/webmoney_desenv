<?php

require_once "class.Pointcut.php";


class CustomPointcut extends Pointcut
{
	var $name;


	function CustomPointcut($action, $class, $function, $name, $nclass, $nfunction)
	{
		Pointcut::Pointcut($action, $class, $function, $nclass, $nfunction);

		// Defining Name(s)
		$this->name = (is_array($name) ? $name : split(",[ ]*", $name));
	}


	function getName() { return $this->name; }

	function hasName($v) { return in_array($v, $this->name, true); }
}


?>
