<?php


class AOP_CodeParser
{
	var $init;
	var $length;

	var $code;
	var $tokens;

	var $index;
	var $invalidTokens;
	var $validTokens;


	function AOP_CodeParser($sCode)
	{
		$this->code = (trim(substr($sCode, 0, 2)) == "<?") ? $sCode : "<?php " . $sCode . " ?>";

		// Reference: http://bugs.php.net/bug.php?id=28391
		$this->tokens = token_get_all($this->code);

		//echo "<pre>" . print_r($this->tokens, true) . "</pre>";

		// PHP Tokenizer has different approaches here; sometimes it considers
		// < ? php as 3 itens, other items it just removes it. The same to ? >.
		// This piece of code tries to solve these differences between versions
		// of PHP Tokenizer Engine.

		// Initial OPENTAG
		$this->init = 0; // 0: considering PHP Tokenizer removed PHP Tag Open

		// Initial CLOSETAG
	    $this->length = count($this->tokens) - 1; // count() - 1: possible PHP Tag Close

        if (count($this->tokens) > 0) {
			// Correcting OPENTAG
			if (!is_array($this->tokens[0]) && $this->tokens[0] == "<") {
				// 3: PHP Tag Open - 0 => <, 1 => ?, 2 => php
				$this->init = 3;
			} elseif (is_array($this->tokens[0]) && token_name($this->tokens[0][0]) == "T_OPEN_TAG") {
				// 1: PHP Tag Open - 0 => < ? php
				$this->init = 1;
			}

            // Correcting CLOSETAG
			if (is_array($this->tokens[$this->length]) && token_name($this->tokens[$this->length][0]) != "T_CLOSE_TAG") {
				$this->length++; // Increase one (same as $l = count($tok);)
			}
		}

		$this->index = $this->init;

		// Initial Invalid Tokens (blank array means none)
		$this->invalidTokens = array();

		// Initial Valid Tokens (blank array means all)
		$this->validTokens = array();
	}


	function getToken($i)
	{
		if ($i >= 0 && $i < $this->length) {
			return $this->tokens[$i];
		}

		return null;
	}


	function getTokenName($i)
	{
		if ($i >= 0 && $i < $this->length) {
			$token = $this->tokens[$this->index];
			return (is_array($token) ? $token[1] : $token);
		}

		return null;
	}


	function setValidTokens($validTokens = array())
	{
		if (!is_array($validTokens)) {
        	$validTokens = array($validTokens);
		}

		$this->validTokens = $validTokens;
	}


	function getValidTokens()
	{
		return $this->validTokens;
	}


	function setInvalidTokens($invalidTokens = array())
	{
		if (!is_array($invalidTokens)) {
        	$invalidTokens = array($invalidTokens);
		}

		$this->invalidTokens = $invalidTokens;
	}


	function getInvalidTokens()
	{
		return $this->invalidTokens;
	}


	function setIndex($i)
	{
		// Only use this method if you are REALLY SURE of what you're doing
		$this->index = $i;
	}


	function getIndex()
	{
		return $this->index;
	}


	function getInit()
	{
		return $this->init;
	}


	function getLength()
	{
		return $this->length;
	}


	function getCode()
	{
		return $this->code;
	}


	function currentToken()
	{
		return $this->nextToken(false);
	}


	function currentTokenName()
	{
		return $this->nextTokenName(false);
	}


	function nextToken($increment = true)
	{
		if ($increment) {
			$this->increment();
		}

		return $this->getToken($this->index);
	}


	function nextTokenName($increment = true)
	{
    	if ($increment) {
			$this->increment();
		}

		return $this->getTokenName($this->index);
	}


	function previousToken($decrement = true)
	{
		if ($decrement) {
			$this->decrement();
		}

		return $this->getToken($this->index);
	}


    function previousTokenName($decrement = true)
	{
		if ($decrement) {
			$this->decrement();
		}

		return $this->getTokenName($this->index);
	}


	function reset()
	{
		$this->index = $this->init;
	}


	function increment()
	{
    	do {
			$this->index++;

			// PHP 4 Parser error prevention
			if (array_key_exists($this->index, $this->tokens)) {
				$token = $this->tokens[$this->index];
			} else {
				$this->index = $this->length;
			}
		} while($this->index < $this->length && ($this->isInvalidToken($token) || !$this->isValidToken($token)));
	}


	function decrement()
	{
        do {
			$this->index--;

			// PHP 4 Parser error prevention
			if (array_key_exists($this->index, $this->tokens)) {
				$token = $this->tokens[$this->index];
			} else {
				$this->index = -1;
			}
		} while($this->index >= 0 && ($this->isInvalidToken($token) || !$this->isValidToken($token)));
	}


	function isInvalidToken($token)
	{
		$tokenType = (is_array($token)) ? token_name((int) $token[0]) : $token;

		if (count($this->invalidTokens) < 1) {
			return false;
		}

		$return = array_search($tokenType, $this->invalidTokens);

		return ($return !== null && $return !== false);
	}


    function isValidToken($token)
	{
		$tokenType = (is_array($token)) ? token_name((int) $token[0]) : $token;

		if (count($this->validTokens) < 1) {
			return true;
		}

		$return = array_search($tokenType, $this->validTokens);

		return ($return !== null && $return !== false);
	}
}


?>
