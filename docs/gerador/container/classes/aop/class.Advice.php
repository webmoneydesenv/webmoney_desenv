<?php


class Advice
{
	var $data = array();


	function Advice()
	{
		$this->data = array();
	}


	function addData($code)
	{
		$this->data[count($this->data)] = ltrim(
			rtrim($code, "\r\n\x0B\t"), "\r\n\x0B"
		);
	}


	function getData($i = null)
	{
		if ($i !== null && array_key_exists($i, $this->data)) {
			return $this->data[$i];
		}

		return implode("", $this->data);
	}
}

?>
