<?php

require_once "class.AOP.php";
require_once "class.Weave.php";
require_once "class.Aspect.php";


function require_aop($filePath, $weave = "./")
{
	// Check if file to be compiled exists
	if (!file_exists($filePath)) {
       	trigger_error(
			"<b>[Aspect Error]:</b> File <b>" . $filePath . "</b> does not exist!",
			E_USER_ERROR
		);
	}

	// Correcting aspects typing
	if (!is_a($weave, "Weave")) {
		$weave = & new Weave($weave);
	}

	// Retrieving information
	$osSeparator = (stristr(getenv("OS"), "Win") ? "\\" : "/");
	$dir = ((AOP::CACHE() == "") ? dirname($filePath) : AOP::CACHE()) . $osSeparator;

	// Defining Last Modified Date
	$lastModified = filemtime($filePath);

	if ($lastModified < $weave->getLastModified()) {
		$lastModified = $weave->getLastModified();
	}

	// Compiled Class File - ".php" added to enable correctly Server File Type Handler
    $compiledClassFile = basename($filePath, "php") . md5($filePath) . ".php";

	// Checking if is necessary to compile file
	// First condition: No aspects defined
	// Second condition: Compiled file does not exist
	// Third condition: Compiled file is older than one (or more) of the aspects
	// Fourth condition: Programmer defined to recompile
	if (!$weave->hasAspects() ||
		!file_exists($dir . $compiledClassFile) ||
        filemtime($dir . $compiledClassFile) <= $lastModified ||
		AOP::RECOMPILE()) {
    	AOP::compile($filePath, $dir . $compiledClassFile, $weave);
	}

	// Check if the compiled file exists
	if (file_exists($dir . $compiledClassFile)) {
    	// Load the compiled file
		require_once $dir . $compiledClassFile;
	} elseif($weave->hasAspects() === false) {
		// Load original file (no aspects defined)
		require_once $filePath;
	} else {
		trigger_error(
			"<b>[Aspect Error]:</b> Compiled File <b>" . $dir . $compiledClassFile . "</b> [From Original: <b>" . $filePath . "</b>] could not be loaded!",
			E_USER_ERROR
		);
	}
}

?>
