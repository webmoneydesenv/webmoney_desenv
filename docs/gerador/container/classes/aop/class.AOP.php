<?php


require_once "class.AOP_CodeCruncher.php";
require_once "class.AOP_CodeCompiler.php";
require_once "class.Weave.php";



class AOP
{
	function COMPACT($v = null)
	{
		// Emulation of static properties in PHP4
		static $compact = true;

		if ($v !== null) {
			$compact = (boolean) $v;
		}

		return $compact;
	}


	function RECOMPILE($v = null)
	{
		// Emulation of static properties in PHP4
		static $recompile = false;

		if ($v !== null) {
			$recompile = (boolean) $v;
		}

		return $recompile;
	}


	function CACHE($v = null)
	{
		// Emulation of static properties in PHP4
		static $cache = "";

		if ($v !== null && is_dir($v)) {
			$cache = (string) $v;
		}

		return $cache;
	}


	function compile($filePath, $compiledFilePath, $weave)
	{
		// Get file content
		$fContent = implode("", file($filePath));

		$compiler = new AOP_CodeCompiler($fContent, $weave);
		$compiler->compile(AOP::COMPACT());

		// Saving compiled file
		if (!$fp = fopen($compiledFilePath, "w")) {
			trigger_error(
				"<b>[Aspect Error]:</b> File <b>" . $compiledFilePath . "</b> [From Original: <b>" . $filePath . "</b>] could not be created/loaded for write!",
				E_USER_ERROR
			);
		}

		if (!fwrite($fp, $compiler->getCode())) {
        	trigger_error(
				"<b>[Aspect Error]:</b> Could not write compiled data in file <b>" . $dir . $compiledFilePath . " [From Original: <b>" . $filePath . "</b>]</b>!",
				E_USER_ERROR
			);
		}

		fclose($fp);
	}
}

?>
