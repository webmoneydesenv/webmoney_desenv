<?php

require_once "class.AOP_XMLElement.php";


class AOP_XMLReader
{
	var $_parser;
	var $_curEl;
	var $documentElement;


	function AOP_XMLReader()
	{
		$this->_curEl = null;

		$this->_parser = xml_parser_create();

        xml_set_object($this->_parser, $this);
        xml_parser_set_option($this->_parser, XML_OPTION_CASE_FOLDING, 0);

        xml_set_element_handler($this->_parser, "_startElement", "_endElement");
        xml_set_character_data_handler($this->_parser, "_CDATAElement");
	}


	function & fromString($xmlContent)
	{
		$xmlReader = new AOP_XMLReader($xmlContent);
		return $xmlReader->_parse($xmlContent);
	}


	function & _parse($data)
    {
		if (!xml_parse($this->_parser, $data)) {
			$errMessage = xml_error_string(xml_get_error_code($this->_parser));
			$errLine = xml_get_current_line_number($this->_parser);

			xml_parser_free($this->_parser);

			die(sprintf("XML error: %s at line %d", $errMessage, $errLine));
		}

		xml_parser_free($this->_parser);

		return $this->documentElement;
	}


	function _startElement($parser, $tagName, $attrs)
	{
		if (strtolower($tagName) == "pointcut") {
			if (array_key_exists("name", $attrs) && array_key_exists("auto", $attrs)) {
				trigger_error(
					"<b>[Aspect Error]:</b> Pointcut Node '" . $NowObj[$Index]->getAttribute("name") . "' can not have 'name' and 'auto' attributes defined together!",
					E_USER_ERROR
				);
			} elseif (!array_key_exists("name", $attrs) && !array_key_exists("auto", $attrs)) {
				trigger_error(
					"<b>[Aspect Error]:</b> Pointcut Node does not have a 'name' or 'auto' defined attribute!",
					E_USER_ERROR
				);
			}
		}

		$el = & new AOP_XMLElement($tagName);
		$el->setAttributes($attrs);
		$el->setParentNode($this->_curEl);

		if ($this->_curEl !== null) {
			$this->_curEl->addChildNode($el);
		} else {
			$this->documentElement = & $el;
		}

		$this->_curEl = & $el;
	}


	function _endElement($parser, $tagName)
	{
		if ($this->_curEl !== null) {
			if ($this->_curEl->getTag() != $tagName) {
				trigger_error(
					"<b>[Aspect Error]:</b> XML Node '" . $tagName . "' is not in the right inheritance!",
					E_USER_ERROR
				);
			}

			$this->_curEl = & $this->_curEl->getParentNode();
		}
	}


	function _CDATAElement($parser, $data)
	{
		if (strlen(trim($data)) > 0) {
			$cdata = & new AOP_XMLElement("#text");
			$cdata->setValue($data);
			$cdata->setParentNode($this->_curEl);

			$this->_curEl->addChildNode($cdata);
	    }
	}
}

?>
