<?
class FrontController{

	public function __construct(){

                $action = (isset($_GET['action'])) ? $_GET['action'] : "";
                $vAction = explode(".",$action);

                if(is_object($_SESSION['oUsuarioLogado']) || $action == "Login.processaFormulario"){

                    if(count($vAction) != 2){
                            include_once('controle/index.php');
                    } else {
                        if($vAction[0] == "Site" || $vAction[0] == "Login")
                            $bPemissao = true;
                        else
                            $bPemissao = $this->verificarPermissao();

                        if($bPemissao){
                            $sClasse = $vAction[0];
                            $sMetodo = $vAction[1];
                            $oFactoryControle = new FactoryControle();
                            $oControle = $oFactoryControle->getObject($sClasse);
                            $oControle->$sMetodo();
                        } else {
                            $_SESSION['sMsg'] = "Voc� n�o tem permiss�o para acessar essa �rea!";
                            include_once('controle/index.php');
                        }
                    }
                } else {
                    include_once('controle/login/index.php');
                }
	}

        public function verificarPermissao(){
            $sOP = (!$_REQUEST['sOP']) ? "Visualizar" : $_REQUEST['sOP'];
            $oFachada = new FachadaPermissaoBD();
            $oTransacaoModulo = $oFachada->recuperarUmTransacaoModuloPorResponsavelOperacao($_REQUEST['action'], $sOP);
            //print_r($oTransacaoModulo);die();
            if(is_object($oTransacaoModulo)){
                if($oFachada->presentePermissao($oTransacaoModulo->getCodTransacaoModulo(), $_SESSION['oUsuarioLogado']->getCodGrupoUsuario()))
                        return true;
                return false;
            }
            return false;
        }
}
?>
