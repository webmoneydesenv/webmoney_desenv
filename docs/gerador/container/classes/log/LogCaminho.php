<?
class LogCaminho{

	public static function escreverCaminho($sMetodo){
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/'))
 			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/');
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/');
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")))
			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y"));
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")))
			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m"));
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")."/".date("d")))
			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")."/".date("d"));
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")."/".date("d")."/".session_id()))
			mkdir($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")."/".date("d")."/".session_id());

		$sCaminho = "[".date("m/d/Y H:i:s")."] ".$sMetodo.";\r\n";

		$fonte = fopen($_SERVER['DOCUMENT_ROOT'].'/#NOME_PROJETO#/logs/caminho/'.date("Y")."/".date("m")."/".date("d")."/".session_id().'/log_caminho.txt','a+');
		if(fwrite($fonte,$sCaminho."\r\n"))
			return true;
	}
}
?>
