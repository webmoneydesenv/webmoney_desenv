<?php
class Paginacao {

	# N�mero da P�gina atual da pagina��o
	var $nPaginaAtual;

	# N�mero do registro inicial a ser exibido
	var $nRegistroInicial;

	# N�mero de registros a serem exibidos por p�gina
	var $nRegistrosPorPagina;

	# Quantidade de P�ginas de acordo com o n�mero de registros por p�gina.
	var $nQuantidadePaginas;

	# Nome do script onde os resultados da consulta est�o sendo exibidos.
	var $sNomePaginaOrigem;

	# Nome da vari�vel que ser� passada pelo m�todo GET e conter� o n�mero da p�gina a ser exibida.
	var $sNomeVariavelDePaginas;

	# Construtor da Classe
	function __construct(){

	}

	public function inicializaPaginacao($nRegistrosPorPagina){
		if ($nRegistrosPorPagina == 0){
			die("O n�mero de registros por p�gina n�o pode ser zero!");
		} else {
			# Poderia ter sido declarado como constante.
			$this->setNomeVariavelDePaginas("pg");
			$this->setPaginaAtual();
			$this->setRegistrosPorPagina($nRegistrosPorPagina);
			$this->setRegistroInicial();
			$this->setNomePaginaOrigem($PHP_SELF);
		}
	}

	# Define o valor do atributo $nPaginaAtual.
	function setPaginaAtual(){
		$this->nPaginaAtual = ($_GET['pg']) ? $_GET['pg'] : 1;

	}

	# Define o valor do atributo $nRegistrosPorPagina
	function setRegistrosPorPagina($nRegistrosPorPagina){
		$this->nRegistrosPorPagina = $nRegistrosPorPagina;
	}

	# Define o valor do atributo $nRegistroInicial
	function setRegistroInicial(){
		$this->nRegistroInicial = ($this->nPaginaAtual - 1) + ($this->nRegistrosPorPagina * ($this->nPaginaAtual - 1));
		$this->nRegistroInicial = ($this->nRegistroInicial > 0) ? $this->nRegistroInicial - ($this->nPaginaAtual - 1) : 0;
		//($this->nRegistroInicial > 0) ? ($this->nRegistroInicial = $this->nRegistroInicial - 1) : ($this->nRegistroInicial = 0);

	}

	# Define o valor do atributo $nQuantidadeDePaginas.
	function setQuantidadeDePaginas($vResultado){
		$this->nQuantidadePaginas = (count($vResultado) > 0) ?  (ceil(count($vResultado)/$this->nRegistrosPorPagina)) : 1;
	}

	function getQuantidadeDePaginas() {
		return $this->nQuantidadePaginas;
	}


	# Define o valor do atributo $sNomeVariavelDePaginas.
	function setNomeVariavelDePaginas($sNomeVariavel){
		$this->sNomeVariavelDePaginas = $sNomeVariavel;
	}


	# Retorna a string que deve ser passada pelo m�todo GET para exibi��o de uma determinada p�gina.

	/*
	Esse m�todo verifica se a p�gina que est� exibindo os resultados da pagina��o foi chamada pelo
	m�todo POST ou GET. Depois varre o vetor referente aos par�metros passados � p�gina( por um dos
	m�todos mencionados) e monta uma string para ser passada pelo m�todo GET, com os par�metros
	necess�rios a realiza��o da consulta, ou seja, todos os par�metros passados a p�gina, com
	exce��o do n�mero da p�gina ( ou seja o par�metro "p").
	*/

	function retornaStringProLink(){
		global $_POST,$_GET;

		$bExecutaLoop = true;

		if (count($HTTP_POST_VARS) > 0){
		 	$sNomeVetorLoop = "HTTP_POST_VARS";
		} elseif(count($HTTP_GET_VARS) > 0) {
			$sNomeVetorLoop = "HTTP_GET_VARS";
		} else {
			$bExecutaLoop = false;
		}

		if ($bExecutaLoop){
			foreach($$sNomeVetorLoop as $sNomeVariavel => $sValorVariavel){
				if ($sNomeVariavel != $this->sNomeVariavelDePaginas){
					global $$sNomeVariavel;
					if (!is_array($$sNomeVariavel)){
						$sVariaveis .= $sNomeVariavel . "=" . $sValorVariavel . "&";
					}
				}
			}

			$sVariaveis = substr($sVariaveis,0,strlen($sVariaveis) - 1);
			$sVariaveis = "&" . $sVariaveis;
		}

		return $sVariaveis;
	}

	# Define o valor do atributo $sNomePaginaOrigem.

	function setNomePaginaOrigem($sNomePaginaOrigem){
		$vNomePaginaOrigem = explode("/",$sNomePaginaOrigem);
		$this->sNomePaginaOrigem = $vNomePaginaOrigem[count($vNomePaginaOrigem) - 1];
	}

    /*Monta a linha com o n�mero das p�ginas dispon�veis para a visualiza��o, cada um com um link para a p�gina
	  onde est�o sendo exibidos os resultados($sNomePaginaOrigem) e os par�metros necess�rios � realiza��o da
	  consulta. Sendo que a p�gina atual vir� apenas em negrito e sem link.*/
	function retornaLinhaDeLinks(){
		for($i = 1;$i <= $this->nQuantidadePaginas;$i++){
					if(!$_SERVER['QUERY_STRING']){
						$sParametros = "";
					}else{
						$sQuerystring = $_SERVER['QUERY_STRING'];
						$sRetirar =strchr($sQuerystring,'&pg=');
						$sParametros = str_replace($sRetirar,'',$sQuerystring);
						$sParametros .=  "&";
				}
		  			if ($i == $this->nPaginaAtual){
						$sLinhaPaginacao .= "<b>".$i."</b> &nbsp; ";
					} else {
						$sLinhaPaginacao .= "<a href=\"".$this->sNomePaginaOrigem."?". $sParametros .$this->sNomeVariavelDePaginas."=$i".$this->retornaStringProLink()."\">$i</a> &nbsp; ";
					}
		}//
		if ($this->nQuantidadePaginas > 1){
			return substr($sLinhaPaginacao,0,strlen($sLinhaPaginacao) - 1);
		} else {
			return "";
		}
	}


	# Realiza a pagina��o do vetor propriamente dita.
	function paginaResultado($vResultado){
	 	if (is_array($vResultado)){

			$this->setQuantidadeDePaginas($vResultado);

			if ($this->nRegistrosPorPagina > count($vResultado)){
				$this->nRegistrosPorPagina = count($vResultado);
			}
			return array_slice($vResultado,$this->nRegistroInicial,$this->nRegistrosPorPagina);
		} else {
			return false;
		}
	}
}

/*
-> Exemplo de Uso.

$oPaginacao = new Paginacao(10); //Passa-se como par�metro o n�mero de registros por p�gina desejados.
$vResultado = $oPaginacao->paginaResultado($vResultado);
foreach ($vResultado as $oResultado){
	print($oResultado->ID_RESULTADO); //Aqui viria a linha desejada, seja dentro de uma tabela, enfim.
}
print($oPaginacao->retornaLinhaDeLinks());// Imprime a linha com n�mero de p�ginas e links para as mesmas.

*/
?>
