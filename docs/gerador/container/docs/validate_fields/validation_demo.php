<?php
include("validation_class.php");
if (isset($_POST['submit'])) {
	$example = new Validate_fields;
	$example->check_4html = true;
	$example->add_text_field("Simple_text", $_POST['string'], "text", "y");
	$example->add_num_field("Number", $_POST['number'], "number", "n", 0, 5);
	$example->add_num_field("Number_with_decimals", $_POST['decimal'], "decimal", "n", 2);
	$example->add_date_field("Euro_date", $_POST['date'], "date", "eu", "n");
	$example->add_check_box("Check_box", "check", "checkbox");
	$example->add_link_field("Email_address", $_POST['email'], "email");
	$example->add_link_field("internet_link", $_POST['url'], "url", "n");
	if ($example->validation()) {
		$goto_after_mail = "thanks.htm"; // this is the page which is shown after the mail is submitted
		$from_mail = $_REQUEST['from_email'];
		$from_name = $_REQUEST['from_name']; // use both value's from your form
		$header = "From: \"$from_name\" <$from_mail>\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n";
		$subject = "Your webform posted at ".date("d-m-Y"); //your mailsubject incl. current date
		foreach ($_REQUEST as $key => $val) {
			if ($key != "from_email" && $key != "from_name") { //skip, this values are already in the header
				$body .= $key . " : " . $val . "\r\n";
			}
		}
		if (mail("you@yourmail.com", $subject, $body, $header)) {
			header("Location: ".$goto_after_mail);
		}
	} else {
		$error = $example->create_msg();
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Form field validation test application</title>
<style type="text/css">
<!--
body {
	font-family: Arial, Helvetica, sans-serif;
	text-align:center;
}
p {
	font-size: 14px;
	line-height: 20px;
}
label {
	font: 14px/20px Arial, Helvetica, sans-serif;
	margin-top:10px;
	float:left;
	width: 210px;
}
#main {
	width:640px;
	margin:0 auto;
	padding:10px;
	text-align:left;
	border: 1px solid #000000;
}
input {
	margin-top:10px;
}
-->
</style>
<script type="text/javascript">
<!--
if (opener.location.href == document.referrer) {
	window.resizeTo(700,680);
}
-->
</script>
</head>

<body>
<div id="main">
  <h1>Test the validation class...</h1>
  <p>I created this class to get an easy to use form field validation script. Use this class to validate your database inputs or mail forms. Of course there is much more to validate: postcodes, special strings, credit card number etc. In the first version is it (only) possible to validate simple text, numbers, (US)dates and e-mail addresses. Invalid form fields will be reported inside a detailed error message. If all form fields are valid you can use the boolean to submit your form.</p>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" style="padding-left:65px;">
    <p><b>Try it by yourself:</b></p>
	<label for="string">Simple text: (required, html-tags are not allowed)</label>
	<textarea name="string" cols="35" rows="3"><?php echo (isset($_POST['string'])) ? $_POST['string'] : ""; ?></textarea><br clear="all">
	<label for="number">An integer (max. digits = 5)</label>
	<input type="text" name="number" size="10" value="<?php echo (isset($_POST['number'])) ? $_POST['number'] : ""; ?>"><br clear="all">
	<label for="decimal">A number with 2 decimals</label>
	<input type="text" name="decimal" size="10" value="<?php echo (isset($_POST['decimal'])) ? $_POST['decimal'] : ""; ?>"><br clear="all">
	<label for="date">EU-date (format = dd-mm-yyyy)</label>
	<input type="text" name="date" size="10" value="<?php echo (isset($_POST['date'])) ? $_POST['date'] : ""; ?>"><br clear="all">
	<label for="check">A checkbox (required)</label>
	<input name="check" type="checkbox" id="check" value="y"<?php if (isset($_POST['check'])) echo " checked"; ?>><br clear="all">
	<label for="email">E-mail address (required)</label>
	<input type="text" name="email" size="25" value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : ""; ?>">
	<br clear="all">
	<label for="url">Hyperlink (ends with: filename, /folder/val/ or querystring) </label>
	<input type="text" name="url" size="45" value="<?php echo (isset($_POST['url'])) ? $_POST['url'] : "http://www.domain.com"; ?>">
	<br clear="all">
	<label for="submit">Test the class ->&gt;</label>
	<input type="submit" name="submit" value="Start">
  </form>
  <p style="color:#FF0000;padding-left:65px;"><?php echo (isset($error)) ? $error : "&nbsp;" ?></p>
</div>
<p style="margin-top:10px;">More classes and scripts on: <a href="http://www.finalwebsites.com/snippets.php" target="_blank">www.finalwebsites.com</a></p>
</body>
</html>
