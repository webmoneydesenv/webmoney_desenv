<?php
 /**
  * @author Auto-Generated
  * @package fachadaPermissao
  * @SGBD mysql
  * @tabela acesso_grupo_usuario
  */
 class GrupoUsuario{
 	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodGrupoUsuario;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
    private $oPermissao;


 	public function __construct(){

 	}

 	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}

        public function temPermissao($nCodTransacaoModulo){
            $oFachada = new FachadaPermissaoBD();
            $this->oPermissao = $oFachada->recuperarUmPermissao($nCodTransacaoModulo,$this->nCodGrupoUsuario);
            if($this->oPermissao)
               return $this->oPermissao;
            else
                return false;
        }

 }
 ?>
