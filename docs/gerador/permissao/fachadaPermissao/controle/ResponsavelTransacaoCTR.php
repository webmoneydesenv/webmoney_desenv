<?php
 class ResponsavelTransacaoCTR implements IControle{

 	public function ResponsavelTransacaoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

        $nCodTransacaoModulo = ($_POST['fCodTransacaoModulo']) ? $_POST['fCodTransacaoModulo'][0] : $_GET['nCodTransacaoModulo'];

 		$_REQUEST['oTransacaoModulo'] = $oFachada->recuperarUmTransacaoModulo($nCodTransacaoModulo);

		include_once("controle/responsavel_transacao/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

                $nCodTransacaoModulo = ($_POST['fCodTransacaoModulo']) ? $_POST['fCodTransacaoModulo'][0] : $_GET['nCodTransacaoModulo'];
                $_REQUEST['oTransacaoModulo'] = $oFachada->recuperarUmTransacaoModulo($nCodTransacaoModulo);

 		$oResponsavelTransacao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nCodResponsavelTransacao = ($_POST['fCodResponsavelTransacao'][0]) ? $_POST['fCodResponsavelTransacao'][0] : $_GET['nCodResponsavelTransacao'];

 			if($nCodResponsavelTransacao){
 				$vCodResponsavelTransacao = explode("||",$nCodResponsavelTransacao);
 				$oResponsavelTransacao = $oFachada->recuperarUmResponsavelTransacao($vCodResponsavelTransacao[0]);
                                $_REQUEST['oTransacaoModulo'] = $oResponsavelTransacao->getTransacaoModulo();
 			}
 		}

 		$_REQUEST['oResponsavelTransacao'] = ($_SESSION['oResponsavelTransacao']) ? $_SESSION['oResponsavelTransacao'] : $oResponsavelTransacao;
		$_SESSION['oResponsavelTransacao'] = array();

 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/responsavel_transacao/detalhe.php");
 		else
 			include_once("controle/responsavel_transacao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];

 		if($sOP != "Excluir"){
 			$oResponsavelTransacao = $oFachada->inicializarResponsavelTransacao($_POST['fCodResponsavelTransacao'],$_POST['fCodTransacaoModulo'],$_POST['fResponsavel'],$_POST['fOperacao']);
 			$_SESSION['oResponsavelTransacao'] = $oResponsavelTransacao;

 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;

 			$oValidate->add_number_field("Transa��o", $oResponsavelTransacao->getCodTransacaoModulo(), "number", "y");
			$oValidate->add_text_field("Responsavel", $oResponsavelTransacao->getResponsavel(), "text", "y");
			$oValidate->add_text_field("Operacao", $oResponsavelTransacao->getOperacao(), "text", "y");


 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=ResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nCodResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."";
 				header("Location: ".$sHeader);
 				die();
 			}
 		}

 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirResponsavelTransacao($oResponsavelTransacao)){
 					$_SESSION['oResponsavelTransacao'] = array();
 					$_SESSION['sMsg'] = "Respons�vel inserido com sucesso!";
 					$sHeader = "?bErro=0&action=ResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$oResponsavelTransacao->getCodTransacaoModulo();

 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel inserir o Respons�vel!";
 					$sHeader = "?bErro=1&action=ResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nCodTransacaoModulo=".$oResponsavelTransacao->getCodTransacaoModulo();
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarResponsavelTransacao($oResponsavelTransacao)){
 					$_SESSION['oResponsavelTransacao'] = array();
 					$_SESSION['sMsg'] = "Respons�vel alterado com sucesso!";
 					$sHeader = "?bErro=0&action=ResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$oResponsavelTransacao->getCodTransacaoModulo();

 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar o Respons�vel!";
 					$sHeader = "?bErro=1&action=ResponsavelTransacao.preparaFormulario&sOP=".$sOP."&nCodResponsavelTransacao=".$_POST['fCodResponsavelTransacao']."&nCodTransacaoModulo=".$oResponsavelTransacao->getCodTransacaoModulo();
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;

 				foreach($_POST['fCodResponsavelTransacao'] as $nCodResponsavelTransacao){
 					$vCodResponsavelTransacao = explode("||",$nCodResponsavelTransacao);
 					$bResultado &= $oFachada->excluirResponsavelTransacao($vCodResponsavelTransacao[0]);

 				}

 				if($bResultado){
 					$_SESSION['sMsg'] = "Respons�vel(is) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=ResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$_GET['nCodTransacaoModulo'];
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir o(s) Respons�vel(is)!";
 					$sHeader = "?bErro=1&action=ResponsavelTransacao.preparaLista&nCodTransacaoModulo=".$_GET['nCodTransacaoModulo'];
 				}
 			break;
 		}

 		header("Location: ".$sHeader);

 	}

 }


 ?>
