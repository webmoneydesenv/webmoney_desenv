<?php
 class PermissaoCTR implements IControle{

 	public function PermissaoCTR(){

 	}

 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();

 		$voPermissao = $oFachada->recuperarTodosPermissao();

        $nCodGrupoUsuario = ($_POST['fCodGrupoUsuario'][0]) ? $_POST['fCodGrupoUsuario'][0] : $_GET['nCodGrupoUsuario'];

        $_REQUEST['oGrupoUsuario'] = $oFachada->recuperarUmGrupoUsuario($nCodGrupoUsuario);

		$_REQUEST['voModulo'] = $oFachada->recuperarTodosModulo();

		include_once("controle/permissao/index.php");

 		exit();

 	}

 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

 		$oPermissao = false;

 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nCodPermissao = ($_POST['fCodPermissao'][0]) ? $_POST['fCodPermissao'][0] : $_GET['nCodPermissao'];

 			if($nCodPermissao){
 				$vCodPermissao = explode("||",$nCodPermissao);
 				$oPermissao = $oFachada->recuperarUmPermissao($vCodPermissao[0],$vCodPermissao[1]);
 			}
 		}

 		$_REQUEST['oPermissao'] = ($_SESSION['oPermissao']) ? $_SESSION['oPermissao'] : $oPermissao;
 		$_SESSION['oPermissao'] = array();

 		$_REQUEST['voGrupoUsuario'] = $oFachada->recuperarTodosGrupoUsuario();

		$_REQUEST['voTransacaoModulo'] = $oFachada->recuperarTodosTransacaoModulo();



 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/permissao/detalhe.php");
 		else
 			include_once("controle/permissao/insere_altera.php");

 		exit();

 	}

 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();

        $bResultado = true;
		$i=0;

        if($oFachada->excluirPermissaoPorGrupoUsuario($_POST['fCodGrupoUsuario'])){

		   foreach($_POST['fCodTransacaoModulo'] as $nCodTransacaoModulo){
                $oPermissao = $oFachada->inicializarPermissao($nCodTransacaoModulo, $_POST['fCodGrupoUsuario']);
                $bResultado &= $oFachada->inserirPermissao($oPermissao);
				$i++;
            }


        } else
            $bResultado = false;

        if($bResultado){
 			$_SESSION['oPermissao'] = array();
            $_SESSION['sMsg'] = "Permiss�o alterada com sucesso!";
        } else {
            $_SESSION['sMsg'] = "N�o foi poss�vel alterar a Permiss�o!";
        }

 		header("Location: index.php?action=Permissao.preparaLista&nCodGrupoUsuario=".$_POST['fCodGrupoUsuario']);

        die();

 	}

 }


 ?>
