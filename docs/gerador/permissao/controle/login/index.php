<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>Sistema de Controle de Escrit�rio de Arquitetura</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <!-- InstanceBeginEditable name="head" -->
  <link rel="shortcut icon" href="http://www.unitarquitetura.com.br/favicon.ico" />
 <!-- InstanceEndEditable -->
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}


 -->
 </style>
 <link href="css/controle.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="js/producao.js"></script>
 </head>

 <body>
 <? include("controle/includes/mensagem.php")?>
 <table class="TabelaPai" width="950" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
 			<td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> &gt; <strong>Login de Usu�rio</strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Titulo Pagina" -->
 			<h2 class="TituloPagina">Login de Usu�rio</h2>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			<? if($_SESSION['sMsg']){?>
 					<script language="javascript">
 						alert('<?=$_SESSION['sMsg']?>');
 					</script>
 			<?
 					$_SESSION['sMsg'] = '';
 			   }
 			?>
 			<? if(!$_SESSION['oUsuarioLogado']){?>
 			<form method="post" name="formLogin" action="?action=Login.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
 			<input type="hidden" name="sOP" value="Logon" />
 			<table width="100%" class="TabelaAdministracao">
 				<tr>
 					<th colspan="2">Dados do Usu�rio</th>
 				</tr>
 				<tr>
					<td class='Campo'>Login:</td>
					<td class='Conteudo'><input type='text' name='fLogin' lang='vazio' value=''/></td>
				</tr>
				<tr>
					<td class='Campo'>Senha:</td>
					<td class='Conteudo'><input type='password' name='fSenha' lang='vazio' value=''/></td>
				</tr>

 				<tr>
 					<td>&nbsp;</td>
 					<td><input type="submit" value="Logon" /></td>
 				</tr>
 			</table>
 			</form>
            <script>javascript:document.formLogin.fLogin.focus();</script>
<?
			} elseif(!$_SESSION['oEscritorio']){
				$voPessoaEscritorio = $_REQUEST['voPessoaEscritorio'];
?>
 			<form method="post" name="formLogin" action="?action=Login.processaFormulario" onSubmit="JavaScript: return validaForm(this);">
 			<input type="hidden" name="sOP" value="EscolherEscritorio" />
 			<table width="100%" class="TabelaAdministracao">
 				<tr>
 					<th colspan="2">Escolher Escrit�rio</th>
 				</tr>
 				<tr>
					<td class='Campo'>Escrit�rio:</td>
					<td class='Conteudo'><select lang="vazio" name="fCodEscritorio">
						<option value="">Selecione</option>
						<? if(is_array($voPessoaEscritorio)){
							 foreach($voPessoaEscritorio as $oEscritorio){?>
								<option value="<?=$oEscritorio->getCodEscritorio()?>"><?=$oEscritorio->getEscritorio()->getDescricao()?></option>
							<? }?>
						<? }?>
					</select></td>
				</tr>
				<tr>
 					<td>&nbsp;</td>
 					<td><input type="submit" value="Salvar" /></td>
 				</tr>
 			</table>
 			</form>
 			<? //}
			 }?>
			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>
