http://tecnobyte.com.br/dica9.html?pagina=dica9.html

http://www.devmedia.com.br/quick-tips-consultando-as-tabela-e-campos-no-firebird-atraves-de-tabelas-de-sistemas/15066



SELECIONANDO TABELAS E VIEWS POR BANCO 

Tabelas e views:
SELECT RDB$RELATION_NAME FROM RDB$RELATIONS;

Somente tabelas:
SELECT RDB$RELATION_NAME FROM RDB$RELATIONS WHERE RDB$VIEW_BLR IS NULL;

Somente views:
SELECT RDB$RELATION_NAME FROM RDB$RELATIONS WHERE NOT RDB$VIEW_BLR IS NULL;

Observa��o:

Para n�o incluir as tabelas e views de sistema, acrescente o filtro (RDB$SYSTEM_FLAG = 0 OR RDB$SYSTEM_FLAG IS NULL) na cl�usula WHERE. Exemplo:
SELECT RDB$RELATION_NAME FROM RDB$RELATIONS WHERE RDB$VIEW_BLR IS NULL AND (RDB$SYSTEM_FLAG = 0 OR RDB$SYSTEM_FLAG IS NULL); 



SELCIONANDO OS CAMPOS DA TABELA 

SELECT RDB$FIELD_NAME
FROM
  RDB$RELATION_CONSTRAINTS C,
  RDB$INDEX_SEGMENTS S
WHERE
  C.RDB$RELATION_NAME = 'NomeDaTabela' AND
  C.RDB$CONSTRAINT_TYPE = 'PRIMARY KEY' AND
  S.RDB$INDEX_NAME = C.RDB$INDEX_NAME
ORDER BY RDB$FIELD_POSITION


OP��O 02

Coincidentemente estava mechendo em algum parecido...

select rdb$field_name from rdb$relation_fields
where rdb$relation_name = 'ESTOQUE_AJUSTES'

nesse select pego todos os campos da tabela ESTOQUE_AJUSTES

Teste esse select aqui...

select
  r.rdb$relation_name,
  ( select max(i.rdb$statistics) || ' (' || count(*) || ')'
    from rdb$relation_fields rf
    where rf.rdb$relation_name = r.rdb$relation_name
  ) as "Max. IndexSel (# fields)"
from
  rdb$relations r
  join rdb$indices i on (i.rdb$relation_name = r.rdb$relation_name)
group by r.rdb$relation_name
having max(i.rdb$statistics) > 0
order by 2




-------------------------
CAMPOS CAHVE

Obter os campos da chave-prim�ria
Execute o comando SELECT abaixo para obter os nomes dos campos da chave-primaria de uma tabela do InterBase ou FireBird. SELECT RDB$FIELD_NAME FROM RDB$RELATION_CONSTRAINTS C, RDB$INDEX_SEGMENTS S WHERE C.RDB$RELATION_NAME = 'NomeDaTabela' AND C.RDB$CONSTRAINT_TYPE = 'PRIMARY KEY' AND S.RDB$INDEX_NAME = C.RDB$INDEX_NAME ORDER BY RDB$FIELD_POSITION