<?
session_start();

function copiaArquivo($sOrigem,$sDestino){
	$vDestino = explode("/",$sDestino);
	if(is_array($vDestino)){
		foreach($vDestino as $nIndice => $sDestinoCompleto){
			if($nIndice+1 < count($vDestino)){
				if(!is_dir($sDir.$sDestinoCompleto)){
					mkdir($sDir.$sDestinoCompleto);
				}
				$sDir .= $sDestinoCompleto."/";
			}
		}

		copy($sOrigem,$sDestino);
	}
	return false;
}

function copiaDiretorio($sDiretorioOrigem,$sDiretorioDestino){
	$bResultado = true;
	if($handle = opendir($sDiretorioOrigem)) {
		while (false !== ($file = readdir($handle))) {
			if(is_file($sDiretorioOrigem."/".$file)){
				$bResultado &= copiaArquivo($sDiretorioOrigem."/".$file,$sDiretorioDestino."/".$file);
			} elseif(is_dir($sDiretorioOrigem."/".$file)){
				if($file != "." && $file != ".."){
					copiaDiretorio($sDiretorioOrigem."/".$file,$sDiretorioDestino."/".$file);
				}

			}
		}

    }
}

function moveDiretorio($sDiretorioOrigem,$sDiretorioDestino){
	$bResultado = true;
	if($handle = opendir($sDiretorioOrigem)) {
		while (false !== ($file = readdir($handle))) {
			if(is_file($sDiretorioOrigem."/".$file)){
				$bResultado &= copiaArquivo($sDiretorioOrigem."/".$file,$sDiretorioDestino."/".$file);
			} elseif(is_dir($sDiretorioOrigem."/".$file)){
				if($file != "." && $file != ".."){
					copiaDiretorio($sDiretorioOrigem."/".$file,$sDiretorioDestino."/".$file);
				}

			}
		}

    }
}



function criaEstrutura_permissao(){
	$bResultado = true;
	$sSql =  "	CREATE TABLE IF NOT EXISTS acesso_modulo (cod_modulo int(11) NOT NULL AUTO_INCREMENT, descricao varchar(150) NOT NULL DEFAULT '', ativo smallint(1) NOT NULL DEFAULT '1',  PRIMARY KEY (cod_modulo)) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$sSql .= "	CREATE TABLE IF NOT EXISTS acesso_transacao_modulo (cod_transacao_modulo int(11) NOT NULL AUTO_INCREMENT, cod_modulo int(11) NOT NULL DEFAULT '0', descricao varchar(150) NOT NULL DEFAULT '', ativo smallint(1) NOT NULL DEFAULT '1', PRIMARY KEY (cod_transacao_modulo),  KEY acesso_transacao_modulo_ibfk_1 (cod_modulo),  CONSTRAINT acesso_transacao_modulo_ibfk_1 FOREIGN KEY (cod_modulo) REFERENCES acesso_modulo (cod_modulo)) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$sSql .= "	CREATE TABLE IF NOT EXISTS acesso_responsavel_transacao ( cod_responsavel_transacao int(11) NOT NULL AUTO_INCREMENT, cod_transacao_modulo int(11) NOT NULL DEFAULT '0', responsavel varchar(150) NOT NULL DEFAULT '',  operacao varchar(100) NOT NULL DEFAULT '',  PRIMARY KEY (cod_responsavel_transacao),  KEY acesso_responsavel_transacao_ibfk_1 (cod_transacao_modulo),  CONSTRAINT acesso_responsavel_transacao_ibfk_1 FOREIGN KEY (cod_transacao_modulo) REFERENCES acesso_transacao_modulo (cod_transacao_modulo)) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$sSql .= "	CREATE TABLE IF NOT EXISTS acesso_grupo_usuario (cod_grupo_usuario int(11) NOT NULL AUTO_INCREMENT, descricao varchar(150) NOT NULL DEFAULT '', ativo smallint(1) NOT NULL DEFAULT '1',  PRIMARY KEY (cod_grupo_usuario)) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$sSql .= "	CREATE TABLE IF NOT EXISTS acesso_usuario (cod_usuario int(11) NOT NULL AUTO_INCREMENT,  cod_grupo_usuario int(11) NOT NULL DEFAULT '0',  nome varchar(150) NOT NULL DEFAULT '',  login varchar(100) NOT NULL DEFAULT '',  senha varchar(255) NOT NULL DEFAULT '',  ativo smallint(1) NOT NULL DEFAULT '1',  PRIMARY KEY (cod_usuario),  KEY acesso_usuario_ibfk_1 (cod_grupo_usuario),  CONSTRAINT acesso_usuario_ibfk_1 FOREIGN KEY (cod_grupo_usuario) REFERENCES acesso_grupo_usuario (cod_grupo_usuario)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$sSql .= "	CREATE TABLE IF NOT EXISTS acesso_permissao (cod_transacao_modulo int(11) NOT NULL DEFAULT '0', cod_grupo_usuario int(11) NOT NULL DEFAULT '0', KEY cod_grupo_usuario (cod_grupo_usuario), CONSTRAINT acesso_permissao_ibfk_1 FOREIGN KEY (cod_grupo_usuario) REFERENCES acesso_grupo_usuario (cod_grupo_usuario)) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";

	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (1, 'M�dulo', 1);";
	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (2, 'Grupo de Usu�rios', 1);";
	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (3, 'Transa��es do M�dulo', 1);";
	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (4, 'Usuario', 1);";
	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (5, 'Respons�veis pela Transa��o', 1);";
	$sSql .= "		INSERT INTO acesso_modulo (cod_modulo, descricao, ativo) VALUES (6, 'Relat�rio', 1);";

	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (1, 1, 'Cadastrar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (2, 2, 'Visualizar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (3, 2, 'Cadastrar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (4, 2, 'Alterar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (5, 2, 'Detalhar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (6, 2, 'Excluir', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (7, 1, 'Visualizar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (8, 1, 'Alterar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (9, 1, 'Detalhar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (10, 1, 'Excluir', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (11, 3, 'Visualizar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (12, 3, 'Cadastrar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (13, 3, 'Alterar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (14, 3, 'Detalhar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (15, 3, 'Excluir', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (16, 2, 'Conceder Permiss�o', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (17, 5, 'Cadastrar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (18, 5, 'Visualizar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (19, 5, 'Alterar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (20, 5, 'Detalhar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (21, 5, 'Excluir', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (22, 4, 'Visualizar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (23, 4, 'Cadastrar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (24, 4, 'Alterar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (25, 4, 'Excluir', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (26, 4, 'Detalhar', 1);";
	$sSql .= "		INSERT INTO acesso_transacao_modulo (cod_transacao_modulo, cod_modulo, descricao, ativo) VALUES (27, 4, 'Alterar Senha', 1);";

	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (1, 2, 'GrupoUsuario.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES   (2, 3, 'GrupoUsuario.preparaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES   (3, 3, 'GrupoUsuario.processaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (4, 4, 'GrupoUsuario.preparaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (5, 4, 'GrupoUsuario.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (6, 5, 'GrupoUsuario.preparaFormulario', 'Detalhar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (7, 6, 'GrupoUsuario.processaFormulario', 'Excluir');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (8, 1, 'Modulo.preparaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (9, 1, 'Modulo.processaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (10, 10, 'Modulo.processaFormulario', 'Excluir');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (11, 7, 'Modulo.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (12, 8, 'Modulo.preparaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (13, 8, 'Modulo.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (14, 9, 'Modulo.preparaFormulario', 'Detalhar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (15, 11, 'TransacaoModulo.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (16, 12, 'TransacaoModulo.preparaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (17, 12, 'TransacaoModulo.processaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (18, 13, 'TransacaoModulo.preparaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (19, 13, 'TransacaoModulo.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (20, 14, 'TransacaoModulo.preparaFormulario', 'Detalhar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (21, 15, 'TransacaoModulo.processaFormulario', 'Excluir');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (22, 16, 'Permissao.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (23, 16, 'Permissao.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (74, 22, 'Usuario.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (75, 23, 'Usuario.preparaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (76, 23, 'Usuario.processaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (77, 24, 'Usuario.preparaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (78, 24, 'Usuario.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (79, 25, 'Usuario.processaFormulario', 'Excluir');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (80, 26, 'Usuario.preparaFormulario', 'Detalhar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (81, 17, 'ResponsavelTransacao.preparaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (82, 17, 'ResponsavelTransacao.processaFormulario', 'Cadastrar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (83, 18, 'ResponsavelTransacao.preparaLista', 'Visualizar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (84, 19, 'ResponsavelTransacao.preparaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (85, 19, 'ResponsavelTransacao.processaFormulario', 'Alterar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (86, 20, 'ResponsavelTransacao.preparaFormulario', 'Detalhar');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (87, 21, 'ResponsavelTransacao.processaFormulario', 'Excluir');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (88, 27, 'Usuario.preparaFormulario', 'AlterarSenha');";
	$sSql .= "		INSERT INTO acesso_responsavel_transacao (cod_responsavel_transacao, cod_transacao_modulo, responsavel, operacao) VALUES (89, 27, 'Usuario.processaFormulario', 'AlterarSenha');";

	$sSql .= "		INSERT INTO acesso_grupo_usuario (cod_grupo_usuario, descricao, ativo) VALUES (1, 'Desenvolvedor', 1);";
	$sSql .= "		INSERT INTO acesso_grupo_usuario (cod_grupo_usuario, descricao, ativo) VALUES (2, 'Administrador', 1);";
	$sSql .= "		INSERT INTO acesso_grupo_usuario (cod_grupo_usuario, descricao, ativo) VALUES (3, 'Usu�rio Comum', 1);";
	$sSql .= "		INSERT INTO acesso_grupo_usuario (cod_grupo_usuario, descricao, ativo) VALUES (4, 'Consulta', 1);";

	$sSql .= "		INSERT INTO acesso_usuario (cod_usuario, cod_grupo_usuario, nome, login, senha, ativo) VALUES (1, 1, 'Administrador do Sistema', 'admin', 'aa1bf4646de67fd9086cf6c79007026c', 1);";

	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (1, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (2, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (3, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (4, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (5, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (6, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (7, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (8, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (9, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (10, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (11, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (12, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (13, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (14, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (15, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (16, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (17, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (18, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (19, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (20, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (21, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (22, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (23, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (24, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (25, 1);";
	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (26, 1);";
 	$sSql .= "		INSERT INTO acesso_permissao (cod_transacao_modulo, cod_grupo_usuario) VALUES   (27, 1);";

	$oBanco = new BancoDeDados();

	if($oBanco->iniciaConexaoBanco()){
		$oBanco->executaSql($sSql);
		$oBanco->terminaConexaoBanco();
	}else {
		$_SESSION['sMsg'] = "N�o foi poss�vel conectar ao banco com os parametros informados!";
		header("Location: index.php");
		die();
	}


}


function criaEstrutura_log(){
	$bResultado = true;
	$sSql =  "CREATE TABLE IF NOT EXISTS acesso_log (cod_acesso int(11) NOT NULL AUTO_INCREMENT,usuario varchar(100) DEFAULT NULL,data datetime DEFAULT NULL,descricao varchar(255) DEFAULT NULL,sql varchar(255) DEFAULT NULL,ativo tinyint(1) DEFAULT NULL, PRIMARY KEY (cod_acesso)) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;";
	$oBanco = new BancoDeDados();

	if($oBanco->iniciaConexaoBanco()){
		$oBanco->executaSql($sSql);
		$oBanco->terminaConexaoBanco();
	}else {
		$_SESSION['sMsg'] = "N�o foi poss�vel conectar ao banco com os parametros informados!";
		header("Location: index.php");
		die();
	}


}



function deletaConteudoDiretorio($dirname){
        if ($dirHandle = opendir($dirname)){
            $old_cwd = getcwd();
            chdir($dirname);

            while ($file = readdir($dirHandle)){
                if ($file == '.' || $file == '..') continue;

                if (is_dir($file)){
                    if (!deletaConteudoDiretorio($file)) return false;
                }else{
                    if (!unlink($file)) return false;
                }
            }

            closedir($dirHandle);
            chdir($old_cwd);
            if(!strpos($dirname,"classes_geradas") && !strpos($dirname,"paginas_geradas")){
            	if (!rmdir($dirname)) return false;
            }

            return true;
        }else{
            return false;
        }
}

switch($_POST['fGeracao']){
	case 0:
		require_once('funcoes.php');
	break;
}

require_once("classes/class.BancoDeDados.php");

$bResultado = true;
$oBancoDeDados = new BancoDeDados();

$vTabelaFachada = $_POST['fTabela'];

foreach($vTabelaFachada as $oTabela){
	$vCampo = explode("||",$oTabela);
	$bResultado &= geraClasseSimples($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);
	if(!$_POST['fGeracao'] == 0)
		$bResultado &= geraClasseBD($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);
	$bResultado &= geraClasseCTR($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);
	$bResultado &= geraInsereAltera($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);
	if($_POST['fGeracao'] == 0)
		$bResultado &= geraDetalhe($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);
	$bResultado &= geraIndex($vCampo[0],$vCampo[1],$_POST['fNomeFachada']);

}

if($_POST['fGeracao'] == 2)
	$bResultado &= geraFrontController($vTabelaFachada,$_POST['fNomeFachada']);

$bResultado &= geraFachada($vTabelaFachada,$_POST['fNomeFachada']);
$bResultado &= geraMenu($vTabelaFachada,$_POST['fNomeFachada'],$_REQUEST['fPermissao']);

switch($_POST['fGeracao']){
	case 0:
		/*
		 * 0 = PHP
		 * ---- COPIANDO DIRET�RIOS ----
		 * 1� C�pia: Container
		 * 2� C�pia: P�ginas Geradas
		 * 3� C�pia: Classes Geradas
		 * 4� C�pia: Posicionando o arquivo "menu.php"
		 * ---- CONFIGURANDO ARQUIVOS ----
		 * 1� Arquivo: BancoDeDados.php
		 * 2� Arquivo: controle.css
		 * 3� Arquivo: index.php
		 * 4� Arquivo: LogCaminho.php
		 * 5� Arquivo: LogPersistencia.php
		 * ---- DELETANDO OS ARQUIVOS GERADOS ----
		 * 1� Grupo: P�ginas Geradas
		 * 2� Grupo: Classes Geradas
		 */

		 if($_REQUEST['fPermissao'] == 'S'){
			copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/permissao",$_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/classes")));

			$sDiretorioOrigem  = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/classes/controle"));
			$sDiretorioDestino = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/view"));
			copiaDiretorio($sDiretorioOrigem,$sDiretorioDestino);

			criaEstrutura_permissao();
			$sDiretorio = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']) . "/classes/frontController/");
  			rename($sDiretorio."FrontController.php", $sDiretorio."FrontController_sempermissao.php");
  			rename($sDiretorio."FrontController_permissao.php", $sDiretorio."FrontController.php");

			//copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/mvc_generator/permissao/controle",$_SERVER['DOCUMENT_ROOT']."/mvc_generator/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/controle")));
		 }


		 if($_REQUEST['fLog'] == 'S'){
			$sDiretorioOrigem  = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/log";
			$sDiretorioDestino = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/classes"));
			copiaDiretorio($sDiretorioOrigem,$sDiretorioDestino);

			$sDiretorioOrigem  = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/log/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/classes/controle"));
			$sDiretorioDestino = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'] . "/view"));
			copiaDiretorio($sDiretorioOrigem,$sDiretorioDestino);

			criaEstrutura_log();
			$sDiretorio = $_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']) . "/classes/frontController/");

  			rename($sDiretorio."Persistencia.php", $sDiretorio."Persistencia_semlog.php");
  			rename($sDiretorio."Persistencia_log.php", $sDiretorio."Persistencia.php");

		 }


		copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/container",$_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'])));



		moveDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/paginas_geradas",$_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/view/".$_POST['fNomeFachada']."/");
		deletaConteudoDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/paginas_geradas");

		moveDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/classes_geradas",$_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/classes");
		deletaConteudoDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/classes_geradas");

		if(copy($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/view/".$_POST['fNomeFachada']."/menu.php",$_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/view/includes/menu.php"))
			unlink($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/view/".$_POST['fNomeFachada']."/menu.php");

		//ALTERANDO A CLASSE DE PERSISTENCIA
		$vClasseBancoDeDados = file($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/classes/persistencia/Persistencia.php");
		unlink($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/classes/persistencia/Persistencia.php");


		$sClasseBancoDeDados = implode(" ",$vClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#SERVIDOR#",$_SESSION['sServidor'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#USUARIO#",$_SESSION['sUsuario'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#SENHA#",$_SESSION['sSenha'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#BANCO#",$_SESSION['sBanco'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#SISTEMA#",$_SESSION['sProjeto'],$sClasseBancoDeDados);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/classes/persistencia/Persistencia.php",'a+');
		fwrite($fonte,$sClasseBancoDeDados);
		fclose($fonte);

		//ALTERANDO CSS
		$vStyleCSS = file($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/css/style.css");
		unlink($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/css/style.css");
		$sStyleCSS = implode(" ",$vStyleCSS);
		$sStyleCSS = str_replace("#NOME_PROJETO#",str_replace(" ","",strtolower($_SESSION['sProjeto'])),$sStyleCSS);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/css/style.css",'a+');
		fwrite($fonte,$sStyleCSS);
		fclose($fonte);

		//ALTERANDO index.php
		$vIndex = file($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/index.php");
		unlink($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/index.php");
		$sIndex = implode(" ",$vIndex);
		$sIndex = str_replace("#NOME_FACHADA#",$_POST['fNomeFachada'],$sIndex);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/index.php",'a+');
		fwrite($fonte,$sIndex);
		fclose($fonte);

	break;
	case 2:
		/*
		 * 2 = JAVA
		 * ---- COPIANDO DIRET�RIOS ----
		 * 1� C�pia: Container
		 * 2� C�pia: P�ginas Geradas
		 * 3� C�pia: Classes Geradas
		 * 4� C�pia: Posicionando o arquivo "menu.php"
		 * ---- CONFIGURANDO ARQUIVOS ----
		 * 1� Arquivo: BancoDeDados.php
		 * 2� Arquivo: controle.css
		 * ---- DELETANDO OS ARQUIVOS GERADOS ----
		 * 1� Grupo: P�ginas Geradas
		 * 2� Grupo: Classes Geradas
		 */

		copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/containerJAVA",$_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto'])));

		copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/paginas_geradas",$_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/controle");
		deletaConteudoDiretorio($_SERVER['DOCUMENT_ROOT']."/webmoney/docs/gerador/paginas_geradas");

		copiaDiretorio($_SERVER['DOCUMENT_ROOT']."/gerador/classes_geradas",$_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/src/java");
		deletaConteudoDiretorio($_SERVER['DOCUMENT_ROOT']."/gerador/classes_geradas");

		if(copy($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/controle/menu.jsp",$_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/controle/includes/menu.jsp"))
			unlink($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/controle/menu.jsp");

		$vClasseBancoDeDados = file($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/src/java/bancodedados/BancoDeDados.java");
		unlink($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/src/java/bancodedados/BancoDeDados.java");
		$sClasseBancoDeDados = implode(" ",$vClasseBancoDeDados);

		//CONFIGURANDO NOME DO DRIVE E GETCONNECTION
		switch($_SESSION['sSGBD']){
			case "mssql":
				$sNomeDriver = "net.sourceforge.jtds.jdbc.Driver";
				$sGetConnection = "jtds:sqlserver";
			break;
			case "mysql":
				$sNomeDriver = "com.mysql.jdbc.Driver";
				$sGetConnection = "mysql";
			break;
			case "pgsql":
				$sNomeDriver = "org.postgresql.Driver";
				$sGetConnection = "postgresql";
			break;
		}
		$sClasseBancoDeDados = str_replace("#SGBD#",$sGetConnection,$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#NOME_DRIVER#",$sNomeDriver,$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#SERVIDOR#",$_SESSION['sServidor'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#USUARIO#",$_SESSION['sUsuario'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#SENHA#",$_SESSION['sSenha'],$sClasseBancoDeDados);
		$sClasseBancoDeDados = str_replace("#BANCO#",$_SESSION['sBanco'],$sClasseBancoDeDados);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/src/java/bancodedados/BancoDeDados.java",'a+');
		fwrite($fonte,$sClasseBancoDeDados);
		fclose($fonte);

		$vStyleCSS = file($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/css/style.css");
		unlink($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/css/style.css");
		$sStyleCSS = implode(" ",$vStyleCSS);
		$sStyleCSS = str_replace("#NOME_PROJETO#",str_replace(" ","",strtolower($_SESSION['sProjeto'])),$sStyleCSS);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT']."/gerador/projeto_gerado/".str_replace(" ","",strtolower($_SESSION['sProjeto']))."/web/css/style.css",'a+');
		fwrite($fonte,$sStyleCSS);
		fclose($fonte);
	break;
}

if($bResultado){
	$_SESSION['sMsg'] = "Classes geradas com sucesso";
	header("Location: index.php?logout=1");
} else {
	$_SESSION['sMsg'] = "N�o foi poss�vel gerar as classes";
	header("Location: index.php?logout=1");
}

?>
