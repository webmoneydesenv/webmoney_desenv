<? session_start();

require_once("classes/class.BancoDeDados.php");

if(!$_SESSION['sServidor']){
	$_SESSION['sProjeto'] = $_POST['fProjeto'];
	$_SESSION['sSGBD'] = $_POST['fSGBD'];
	$_SESSION['sServidor'] = $_POST['fServidor'];
	$_SESSION['sUsuario'] = $_POST['fUsuario'];
	$_SESSION['sSenha'] = $_POST['fSenha'];
	$_SESSION['sBanco'] = $_POST['fBanco'];
	$_SESSION['sEsquema'] = $_POST['fEsquema'];
}

$oBanco = new BancoDeDados();

if($oBanco->iniciaConexaoBanco())
	$oBanco->terminaConexaoBanco();
else {
	$_SESSION['sMsg'] = "N�o foi poss�vel conectar ao banco com os parametros informados!";
	header("Location: index.php");
	die();
}

$vTabela = $oBanco->retornaTabela();


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>MVC Generator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
body{
font:Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif;
size:10px;
}
<!--
.style1 {
	font-size: 36px;
	font-weight: bold;
	color: #1A8CFF;
}
.style2 {color: #FFFFFF}
.style4 {color: #000000}
.style5 {
	color: #FF2222;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="gera_classes.php">
  <table width="750" border="1" align="center" cellpadding="2" cellspacing="2">
    <tr>
      <td colspan="2"><div align="center" class="style1">GENERATOR WebMoney<br>
          <hr>
</div></td>
    </tr>
	<? if($_SESSION['sMsg']){?>
	<tr>
      <td colspan="2">&nbsp;</td>
    </tr>
	<tr>
      <td colspan="2"><div align="center" class="style5"><?=$_SESSION['sMsg']?></div></td>
    </tr>
	<? unset($_SESSION['sMsg']);
	   }?>
    <tr bgcolor="#5B5BFF">
      <td colspan="2"><div align="center" class="style2">Informe para quais tabelas voc� deseja gerar as classes !</div></td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Tabelas do Banco de dados:</div></td>
      <td><select name="fTabela[]" size="15" multiple>
	  <? foreach($vTabela as $oTabela){?>
	  <option value="<?=$oTabela['table_name'] . "||". $oTabela['comment']?>"><?=$oTabela['table_name']?></option>
	  <? }?>
	  </select></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFFF"><div align="right">Nome da Classe Fachada:</div></td>
      <td><input type="text" name="fNomeFachada" size="30"></td>
    </tr>
<!--
    <tr>
      <td bgcolor="#FFFFFF"><div align="right">Tipo de Classe:</div></td>
      <td><input type="radio" name="fGeracao" value="0" checked/>PHP <input type="radio" name="fGeracao" value="2"/>JAVA </td>
    </tr>
-->
    <tr>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td><input type="submit" name="Submit" value="Gerar Classes">
      <input type="hidden" name="fPermissao" value="<?php echo $_REQUEST['fPermissao']?>">
      <input type="hidden" name="fLog" value="<?php echo $_REQUEST['fLog']?>">
      </td>
    </tr>
  </table>
</form>
</body>
</html>
