<?
/*
Classe Responsável por todas as operações no Banco de Dados
*/
class BancoDeDados{

	private $sSGBD;
	private $sBanco;
	private $sServidor;
	private $sUsuario;
	private $sSenha;
	private $oPDO;

	public function __construct(){
		$this->sSGBD = $_SESSION['sSGBD'];
		$this->sBanco = $_SESSION['sBanco'];
		$this->sServidor = $_SESSION['sServidor'];
		$this->sUsuario = $_SESSION['sUsuario'];
		$this->sSenha = $_SESSION['sSenha'];
	}

	//Faz a conexão com o Banco de Dados
	public function iniciaConexaoBanco(){
		try{
			switch($this->sSGBD){
				case "mysql":

					$this->oPDO = new PDO($this->sSGBD.":dbname=".$this->sBanco.";charset=utf8;host=".$this->sServidor,$this->sUsuario,$this->sSenha);
					/*
					$this->oPDO = new PDO($this->sSGBD.':host='.$this->sServidor.'; dbname='.$this->sBanco, $this->sUsuario,$this->sSenha,
								array(
									PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
									PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
									PDO::ATTR_PERSISTENT => false,
									PDO::ATTR_EMULATE_PREPARES => false,
									PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
								)
					);
					*/
				break;
				case "sqlsrv":
					$this->oPDO = new PDO ("$this->sSGBD:Server=$this->sServidor;Database=$this->sBanco","$this->sUsuario","$this->sSenha");
				break;
				default:
					$this->oPDO = new PDO($this->sSGBD.":dbname=".$this->sBanco.";charset=utf8;host=".$this->sServidor,$this->sUsuario,$this->sSenha);
				break;
			}
		} catch (PDOException $e) {
			print("<pre>");print_r($e->getMessage());print("</pre>");
			die();
		}
		if($this->oPDO){
			return true;
		}
		return false;
	}

	//Desfaz a conexão com o Banco de Dados
	public function terminaConexaoBanco(){
		if (!$this->oPDO){
			return 0;
		}
		else{
			$this->oPDO = false;
			return 1;
		}
	}

	//Insere um registro no tabela mandada como parametro
	public function insereRegistroNoBanco($sTabela,$sCampos,$sValores){
		if($this->iniciaConexaoBanco()){
			$oStmt = $this->oPDO->query("INSERT INTO $sTabela($sCampos) VALUES ($sValores)");
			$nId = $this->oPDO->query->lastInsertId();
			$this->terminaConexaoBanco();
			if($nId)
				return $nId;
			else
				return $rsResultado;
		}
	}


	//Insere um registro no tabela mandada como parametro
	public function executaSql($sSql){
		if($this->iniciaConexaoBanco()){
			$oStmt = $this->oPDO->query($sSql);
			$this->terminaConexaoBanco();
			return $oStmt;
		}
	}


	//Recupera registro da tabela mandada com parametro *Variável $sComplemento
	//é opcional e se refere as clausulas adicionais como WHERE, ORDER BY, LIMIT
	//e etc.*
	public function recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento){
		if($this->iniciaConexaoBanco()){
			die("SELECT $sCampos FROM $sTabelas $sComplemento");
			$sSql = "SELECT $sCampos FROM $sTabelas $sComplemento";
			if ($oStmt = $this->oPDO->query($sSql)){
				while ($oReg = $oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}
				$this->terminaConexaoBanco();
				return $vObjeto;
			}
		}
	}

	//Atera o campo de um registro da tabela mandada como parametro
	public function alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("UPDATE $sTabela SET $sCampos $sComplemento");
			$oStmt = $this->oPDO->query("UPDATE $sTabela SET $sCampos $sComplemento");
			$this->terminaConexaoBanco();
			return $oStmt;
		}
	}

	//Exclui logicamente um registro da tabela
	public function excluiRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$oStmt = $this->oPDO->query("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$this->terminaConexaoBanco();
			return $oStmt;
		}
	}

	//Exclui fisicamente um registro da tabela
	public function excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			$oStmt = $this->oPDO->query("DELETE FROM $sTabela $sComplemento");
			$this->terminaConexaoBanco();
			return $oStmt;
		}
	}

	public function retornaCampo($sTabela){
		if($this->iniciaConexaoBanco()){
			$vCampo = array();
			switch($this->sSGBD){
				case "mysql":
//					$sSql = "DESCRIBE $sTabela";
					$sSql = "SELECT DISTINCT COLUMN_NAME as Field
							, DATA_TYPE as Type, IS_NULLABLE as 'Nulo'
							, COLUMN_KEY as 'Key'
							, EXTRA as Extra
							,IF(COLUMN_COMMENT='',concat(Upper(substr(COLUMN_NAME, 1,1)), lower(substr(COLUMN_NAME, 2,length(COLUMN_NAME)))),COLUMN_COMMENT) as Comentario

							FROM information_schema.COLUMNS
							WHERE TABLE_NAME = '$sTabela' AND TABLE_SCHEMA='$this->sBanco'";
				break;
				case "pgsql":
					$sSql = "SELECT
								c.attname as Field
								, tp.typname as Type
								, case when ct.contype = 'p' then 'PRI' else '' end as Key
								, case when max(seq.relname) <> '' then 'auto_increment' else '' end as Extra

							FROM
								pg_class t
								join pg_attribute c on (t.oid = c.attrelid)
								join pg_namespace s on (t.relnamespace = s.oid)
								join pg_type tp on (c.atttypid = tp.oid)
								left join pg_constraint ct on (t.oid = ct.conrelid and c.attnum = any(ct.conkey) and ct.contype = 'p')
								left join pg_depend dp on (dp.refobjid = t.oid and dp.refobjsubid = c.attnum)
								left join pg_class seq on (dp.objid = seq.oid and seq.relkind = 'S')

							WHERE
								s.nspname = '".$_SESSION['sEsquema']."'
								and t.relname = '$sTabela'
								and c.attnum > 0
								and c.attisdropped = false

							GROUP BY
								c.attname
								, tp.typname
								, case when ct.contype = 'p' then 'PRI' else '' end ";
				break;
				case "sqlsrv":
					$sSql = "Declare	@Schema varchar(15)='".$_SESSION['sEsquema']."'
							Declare	@Table  varchar(30)='".$sTabela."'
							Select	Distinct c.column_name As Field,sep.value Comentario,
								Case c.data_type When 'varchar' Then c.data_type+'('+Cast(max_length As varchar(6))+')'
										 When 'int'     Then c.data_type+'('+Cast(NUMERIC_PRECISION As varchar(6))+','+Cast(NUMERIC_SCALE As varchar(6))+')'
										 When 'float'   Then c.data_type+'('+Cast(NUMERIC_PRECISION As varchar(6))+')'
										 When 'decimal' Then c.data_type+'('+Cast(NUMERIC_PRECISION As varchar(6))+','+Cast(NUMERIC_SCALE As varchar(6))+')'
										 When 'numeric' Then c.data_type+'('+Cast(NUMERIC_PRECISION As varchar(6))+','+Cast(NUMERIC_SCALE As varchar(6))+')'
										 When 'real'    Then c.data_type+'('+Cast(NUMERIC_PRECISION As varchar(6))+','+Cast(NUMERIC_SCALE As varchar(6))+')'
										 Else c.data_type
								End As [Type],
								c.IS_NULLABLE As [Null],
								Case tc.constraint_type When 'PRIMARY KEY' Then 'PRI' When 'FOREIGN KEY' Then 'MUL' Else '' End As [Key],
								c.COLUMN_DEFAULT As [Default],
								Case sc.is_identity When '1' Then 'auto_increment' Else '' End As [Extra],
								c.ordinal_position
							From	INFORMATION_SCHEMA.TABLES t
								Inner Join INFORMATION_SCHEMA.COLUMNS c on (t.table_catalog=c.table_catalog And t.table_schema=c.table_schema And t.table_name=c.table_name)
								Left  Join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE cc on (cc.table_catalog=t.table_catalog And cc.table_schema=t.table_schema And cc.table_name=t.table_name And cc.column_name=c.column_name)
								Left  Join INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on (tc.table_catalog=t.table_catalog And tc.table_schema=t.table_schema And tc.table_name=t.table_name And tc.constraint_catalog=cc.constraint_catalog And tc.constraint_schema=cc.constraint_schema And tc.constraint_name=cc.constraint_name)
								Inner Join Sys.Tables st on (t.table_name=st.name)
								Inner Join Sys.Schemas ss on (st.schema_id=ss.schema_id)
								Inner Join Sys.Columns sc on (st.object_id=sc.object_id And sc.name=c.column_name)
								Left Join sys.extended_properties sep on st.object_id = sep.major_id And sc.column_id = sep.minor_id And sep.name = 'MS_Description'
							Where	t.table_schema=@Schema
								And t.table_name=@Table
								And ss.name=@Schema
							Order	By c.ordinal_position";
				break;
				default:
					$sSql = "DESCRIBE $sTabela";
				break;
			}
			//echo $sSql;
			//print_r($sSql);

			$oStmt = $this->oPDO->query($sSql);
			while($vResultado = $oStmt->fetch(PDO::FETCH_OBJ)){
				//print_r($vResultado);
//				die();
//			    var_dump($vResultado);
			/*	if($this->sSGBD == "pgsql")
					$vCampo[] = array("sNome"=>$vResultado['field'],"sTipo"=>strtolower($vResultado['type']),"sChave"=>$vResultado['key'],"sAuto"=>$vResultado['extra']);
				else
			*/		$vCampo[] = array("sNome"=>$vResultado->Field,"sTipo"=>strtolower($vResultado->Type),"sChave"=>$vResultado->Key,"sAuto"=>$vResultado->Extra,"sComentario"=>$vResultado->Comentario,"bNulo"=>$vResultado->Nulo);

			}
			//print_r($vCampo);


			return $vCampo;
		}
	}

	public function retornaTabela(){
		if($this->iniciaConexaoBanco()){
			$vTabela = array();
			switch($this->sSGBD){
				case "mysql":
//					$sSql = "SHOW TABLES";
					$sSql = "SELECT table_name, IF(table_comment='',table_name,table_comment) as comment FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$this->sBanco."'";

				break;
				case "sqlsrv":
					$sSql = "SELECT table_name FROM information_schema.tables WHERE table_type='BASE TABLE' AND table_schema = '".$_SESSION['sEsquema']."' ORDER BY table_name";
				break;
				case "pgsql":
					$sSql = "SELECT tablename FROM pg_tables WHERE schemaname = '".$_SESSION['sEsquema']."' ORDER BY tablename";
				break;
				default:
					$sSql = "SHOW TABLES";
				break;
			}

			$oStmt = $this->oPDO->query($sSql);
			while($vResultado = $oStmt->fetch())
				$vTabela[] = $vResultado;
			return $vTabela;
		}
	}

	public function retornaChavePrimaria($sTabela){
		if($this->iniciaConexaoBanco()){
			$vChavePrimaria = array();
			$vvCampo = $this->retornaCampo($sTabela);
			foreach($vvCampo as $vCampo){
				if($vCampo['sChave'] == "PRI")
					$vChavePrimaria[] = $vCampo;
			}
			return $vChavePrimaria;
		}
	}

	public function retornaChaveEstrangeira($sTabela){
		if($this->iniciaConexaoBanco()){
			switch($this->sSGBD){
				case "mysql":
					if($this->oPDO->query("USE information_schema")){
						$sSql = "SELECT
								C.*, T.CONSTRAINT_TYPE
							   FROM
								KEY_COLUMN_USAGE C, TABLE_CONSTRAINTS T
							   WHERE
								C.CONSTRAINT_SCHEMA = T.CONSTRAINT_SCHEMA AND
								C.CONSTRAINT_NAME = T.CONSTRAINT_NAME AND
								C.TABLE_NAME = T.TABLE_NAME AND
								C.TABLE_SCHEMA = T.TABLE_SCHEMA AND
								T.CONSTRAINT_TYPE = 'FOREIGN KEY' AND
								T.TABLE_SCHEMA = '".$this->sBanco."' AND
								T.TABLE_NAME = '$sTabela'
							  ";
					} else {
						return false;
					}
				break;
				case "sqlsrv":
					$sSql = "SELECT DISTINCT
								c.column_name AS COLUMN_NAME, cc_ref.column_name AS REFERENCED_COLUMN_NAME, cc_ref.table_name AS REFERENCED_TABLE_NAME, c.ordinal_position
							FROM
								information_schema.tables t
								join information_schema.columns c on (t.table_catalog = c.table_catalog and t.table_schema = c.table_schema and t.table_name = c.table_name)
								join information_schema.constraint_column_usage cc on (cc.table_catalog = t.table_catalog and cc.table_schema = t.table_schema and cc.table_name = t.table_name and cc.column_name = c.column_name)
								join information_schema.table_constraints tc on (tc.table_catalog = t.table_catalog and tc.table_schema = t.table_schema and tc.table_name = t.table_name and tc.constraint_catalog = cc.constraint_catalog and tc.constraint_schema = cc.constraint_schema and tc.constraint_name = cc.constraint_name)
								join information_schema.referential_constraints rc on (tc.constraint_catalog = rc.constraint_catalog and tc.constraint_schema = rc.constraint_schema and tc.constraint_name = rc.constraint_name)
								join information_schema.constraint_column_usage cc_ref on (cc_ref.constraint_catalog = rc.unique_constraint_catalog and cc_ref.constraint_schema = rc.unique_constraint_schema and cc_ref.constraint_name = rc.unique_constraint_name)

							WHERE
								t.table_schema = '".$_SESSION['sEsquema']."' and
								t.table_name = '$sTabela' and
								tc.constraint_type = 'FOREIGN KEY'

							ORDER BY
								c.ordinal_position";
				break;
				case "pgsql":
					$sSql = "SELECT
								c.attname as COLUMN_NAME
								, ft.relname as REFERENCED_TABLE_NAME
								, fc.attname as REFERENCED_COLUMN_NAME

							FROM
								pg_class t
								join pg_attribute c on (t.oid = c.attrelid)
								join pg_namespace s on (t.relnamespace = s.oid)
								join pg_constraint ct on (t.oid = ct.conrelid and c.attnum = any(ct.conkey) and ct.contype = 'f')
								join pg_class ft on (ct.confrelid = ft.oid)
								join pg_attribute fc on (ft.oid = fc.attrelid and fc.attnum = any(ct.confkey))

							WHERE
								s.nspname = '".$_SESSION['sEsquema']."'
								and t.relname = '$sTabela'
								and c.attnum > 0
								and c.attisdropped = false  ";
				break;
				default:
					if($this->oPDO->query("USE information_schema")){
						$sSql = "SELECT
								C.*, T.CONSTRAINT_TYPE
							   FROM
								KEY_COLUMN_USAGE C, TABLE_CONSTRAINTS T
							   WHERE
								C.CONSTRAINT_SCHEMA = T.CONSTRAINT_SCHEMA AND
								C.CONSTRAINT_NAME = T.CONSTRAINT_NAME AND
								C.TABLE_NAME = T.TABLE_NAME AND
								C.TABLE_SCHEMA = T.TABLE_SCHEMA AND
								T.CONSTRAINT_TYPE = 'FOREIGN KEY' AND
								T.TABLE_SCHEMA = '".$this->sBanco."' AND
								T.TABLE_NAME = '$sTabela'
							  ";
					} else {
						return false;
					}
				break;
			}
			$mChaveEstrangeira = array();

			$oStmt = $this->oPDO->query($sSql);

			while($vResultado = $oStmt->fetch()){
				if($this->sSGBD == "pgsql")
					$mChaveEstrangeira[] = array("sNome"=>$vResultado['column_name'],"sTabelaReferenciada"=>$vResultado['referenced_table_name'],"sCampoReferenciado"=>$vResultado['referenced_column_name']);
				else
					$mChaveEstrangeira[] = array("sNome"=>$vResultado['COLUMN_NAME'],"sTabelaReferenciada"=>$vResultado['REFERENCED_TABLE_NAME'],"sCampoReferenciado"=>$vResultado['REFERENCED_COLUMN_NAME']);
			}
			if(count($mChaveEstrangeira) > 0)
				return $mChaveEstrangeira;

			return false;

		}
	}

}
?>
