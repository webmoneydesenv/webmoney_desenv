
<?php
ini_set("max_execution_time", 0); // tempo ilimitado para realização da tarefa
date_default_timezone_set('America/Belem'); // define timezone para Belém-Pa

//--------------------------------------------------------------------------------------------------------------------------------
//--------------------------efetuando conexoes com adabas e GP e dBSepof(sqlserver)-----------------------------------------------

	$connAdabas = odbc_connect( 'adabas', 'slcdd', 's3p0f_est4@t' ) or die ("Não foi possível conectar-se ao servidor Adabas!!");
	//echo "Conexao com Adabas feita com sucesso!";
	//echo "<br />";

	$connSQL = odbc_connect( 'gp_ODBC', 'sa', 't3gucigalp@' ) or die ("Não foi possível conectar-se ao servidor SQL Server!!");
	//echo "Conexao com SQLserver feita com sucesso!";
	//echo "<br />";

	$conndbSepof = odbc_connect( 'dbSepof_ODBC', 'sa', 't3gucigalp@' ) or die ("Não foi possível conectar-se ao servidor SQL Server!!");;
	//echo "Conexao com SQLserver feita com sucesso!";
	//echo "<br />";

//--------------------------------------------------------------------------------------------------------------------------------
//--------------------------------Estabelecendo funcão para tratamento de erros em DATAS------------------------------------------
function data ($x){
	if (strlen($x)==8){
	$y= substr($x,0,4)."".substr($x,4,-2)."".substr($x,-2);
}
else{
	$y= substr($x,0,4)."".substr($x,4,-1)."0".substr($x,-1);
}return $y;
}
//----------------------
//dd-mm-aaaa
function dataContraria ($x){
	if (strlen($x)==8){
	$y= substr($x,-4)."".substr($x,2,2)."".substr($x,0,2);
	}
	else{
	$y= str_pad($x, 8, "0", STR_PAD_LEFT);
	$y= substr($y,-4)."".substr($y,2,2)."".substr($y,0,2);
	}return $y;
}
//---------------------
function dtaProg ($z, $w){
	if ($z >= "2008"){
			if ($w == "0125" or $w == "0000" or $w == "9999" or $w == "1297"){
				$y=$w;
			} else if($z >= "2016") {
				$y=$w + 3000;
			} else {
				$y=$w + 2000;
				}
				}
return $y;
}
//---------------------
function cdSituação ($z){
	if ($z==NULL){
	$y= 1;
}
else{
	$y=$z;
}return $y;
}
//----------------------
function funcStatus ($z){
	if ($z=='X'){
	$y= 0;
}
else{
	$y=1;
}return $y;
}
//-----------------------
function contaZero2 ($z){
	if (strlen($z)!=2){
	$y= str_pad($z, 2, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero3 ($z){
	if (strlen($z)!=3){
	$y= str_pad($z, 3, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero4 ($z){
	if (strlen($z)!=4){
	$y= str_pad($z, 4, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero5 ($z){
	if (strlen($z)!=5){
	$y= str_pad($z, 5, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero6 ($z){
	if (strlen($z)!=6){
	$y= str_pad($z, 6, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero9 ($z){
	if (strlen($z)!=9){
	$y= str_pad($z, 9, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//---------------------
function contaZero14 ($z){
	if (strlen($z)!=14){
	$y= str_pad($z, 14, "0", STR_PAD_LEFT);
}
else{
	$y=$z;
}return $y;
}
//-------------------------------------------Captura hora inicio do extrator------------------------------------------------------
$dataHoraINI = date("m/d/Y H:i:s");
//--------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------Começando alterações de registros(Extrator)-----------------------------------------------
//-----------------------------------------------Para tab EMPENHO----------------------------------------------------------------


//Select para empenho

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U35_EMPENHO WHERE N70U35_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U35_EMPENHO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U35_EMPENHO WHERE N70U35_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U35_EMPENHO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	   //deletando dados repetidos
	   $queryAux2 = ("Delete Empenho WHERE Emp_Unid_Orcamentaria = '".contaZero5($array[$i]['N70U35_UNIDADE_ORCAMENTARIA'])."'and Emp_Gestao='".$array[$i]['N70U35_GESTAO']."' and Emp_Ano='".$array[$i]['N70U35_ANO']."' and Emp_Numero='".$array[$i]['N70U35_NUM_EMPENHO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete AP70U35_EMPENHO!!");
	   }


//efetuando insert no sqlserver

foreach($array as $tabela) {
		$query2 = ("INSERT INTO Empenho (
		Emp_Unid_Orcamentaria,
		Emp_Gestao,
		Emp_Ano,
		Emp_Numero,
		Emp_Funcao,
		Emp_Sub_Funcao,
		Emp_Programa,
		Emp_Proj_Atividade,
		Emp_Nat_Despesa,
		Emp_Fonte,
		Emp_Data_Empenho,
		Emp_Data_Geracao,
		Emp_Valor,
		Emp_Descricao,
		Emp_Favorecido,
		Emp_Fonte_Nova,
		Emp_Fonte_Det,
		Emp_Programa_Aux,
		Emp_PI)
		VALUES
		 ('".contaZero5($tabela['N70U35_UNIDADE_ORCAMENTARIA'])."',
		 '".$tabela['N70U35_GESTAO']."',
		 '".$tabela['N70U35_ANO']."',
		 '".$tabela['N70U35_NUM_EMPENHO']."',
		 '".$tabela['N70U35_FUNCAO']."',
		 '".$tabela['N70U35_SUBFUNCAO']."',
		 '".dtaProg($tabela['N70U35_ANO'],$tabela['N70U35_PROGRAMA'])."',
		 '".$tabela['N70U35_PROJ_ATIVIDADE']."',
		 '".$tabela['N70U35_NATUREZA_DESPESA']."',
		 '".$tabela['N70U35_FONTE_NOVA']."',
		 '".dataContraria($tabela['N70U35_DATA_EMPENHO'])."',
		 '".date('Y-m-d  H:i:s')."',
		 '".$tabela['N70U35_VALOR_GERADO']."',
		 '".str_replace("'", " ",$tabela['N70U35_DESCRICAO'])."',
		 '".$tabela['N70U35_COD_CREDOR']."',
		 '".$tabela['N70U35_FONTE_NOVA']."',
		 '".$tabela['N70U35_FONTE_DET']."',
		 '".$tabela['N70U35_PROGRAMA']."',
		 '".$tabela['N70U35_IT_CO_PLANO_INTERNO']."')");
 		$insert = odbc_exec( $connSQL, $query2 )  or die ("Erro durante o insert EMPENHO!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U35_EMPENHO SET N70U35_FLAG_GERADO = 2 where ISN_AP70U35_EMPENHO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U35_EMPENHO']))  or die ("Erro durante o update AP70U35_EMPENHO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-------------------------------------------------Para tab Favorecido-----------------------------------------------------------
//select

		$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U47_FAVORECIDO where N70U47_FLAG_GERADO=1";
		$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U47_FAVORECIDO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U47_FAVORECIDO where N70U47_FLAG_GERADO=1 ";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U47_FAVORECIDO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos

	   $queryAux2 = ("Delete Favorecido WHERE fav_codigo = '".$array[$i]['N70U47_COD_CREDOR']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Favorecido!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Favorecido (
		fav_codigo,
		fav_nome,
		fav_endereco,
		fav_municipio,
		fav_uf,
		fav_cep,
		fav_idsiafem) VALUES (
		'".$tabela['N70U47_COD_CREDOR']."',
		'".str_replace("'", " ",$tabela['N70U47_NOME_CREDOR'])."',
		'".str_replace("'", " ",$tabela['N70U47_ENDER_CREDOR'])."',
		'".str_replace("'", " ",$tabela['N70U47_MUNICIPIO'])."',
		'".$tabela['N70U47_UF']."',
		'".$tabela['N70U47_CEP']."',
		'".$tabela['N70U47_IDENTIF_SIAFEM']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Favorecido!!");
}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U47_FAVORECIDO SET N70U47_FLAG_GERADO = 2 where ISN_AP70U47_FAVORECIDO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U47_FAVORECIDO'])) or die ("Erro durante o Update AP70U47_FAVORECIDO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------Para tab Pago---------------------------------------------------------(Pagamento)
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U37_PAGAMENTO WHERE N70U37_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U37_PAGAMENTO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U37_PAGAMENTO WHERE N70U37_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U37_PAGAMENTO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos
	   $queryAux2 = ("Delete Pago WHERE uo_codusu = '".contaZero5($array[$i]['N70U37_UNIDADE_ORCAMENTARIA'])."'and pag_gestao='".$array[$i]['N70U37_GESTAO']."' and pag_ano='".$array[$i]['N70U37_ANO']."' and pag_no_np='".$array[$i]['N70U37_NUM_NP']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Pago!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Pago (
		uo_codusu,
		pag_gestao,
		pag_ano,
		pag_no_np,
		pag_data,
		pag_data_reg,
		pag_valor,
		pag_descricao,
		fav_codigo) VALUES (
		'".contaZero5($tabela['N70U37_UNIDADE_ORCAMENTARIA'])."',
		'".$tabela['N70U37_GESTAO']."',
		'".$tabela['N70U37_ANO']."',
		'".$tabela['N70U37_NUM_NP']."',
		'".dataContraria($tabela['N70U37_DATA_PAGAMENTO'])."',
		'".date('Y-m-d  H:i:s')."',
		'".$tabela['N70U37_VALOR']."',
		'".str_replace("'", " ",$tabela['N70U37_DESCRICAO'])."',
		'".$tabela['N70U37_COD_CREDOR']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Pago!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U37_PAGAMENTO SET N70U37_FLAG_GERADO = 2 where ISN_AP70U37_PAGAMENTO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U37_PAGAMENTO'])) or die ("Erro durante o Update AP70U37_PAGAMENTO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-------------------------------------------------Para tab Liquidaçao_Det--------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U51_LIQUIDACAO_DET WHERE N70U51_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U51_LIQUIDACAO_DET!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U51_LIQUIDACAO_DET WHERE N70U51_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U51_LIQUIDACAO_DET!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos
	   $queryAux2 = ("Delete Liquidado_Det WHERE lidUnidOrcamentaria = '".contaZero5($array[$i]['N70U51_UNIDADE_ORCAMENTARIA'])."'and lidGestao='".$array[$i]['N70U51_GESTAO']."' and lidAno='".$array[$i]['N70U51_ANO']."' and lidNE='".$array[$i]['N70U51_NUM_NE']."' and lidNL='".$array[$i]['N70U51_NUM_NL']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Liquidacao_Det!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Liquidado_Det (
		lidUnidOrcamentaria,
		lidGestao,
		lidAno,
		lidNE,
		lidNL,
		lidNatDespesaDet,
		lidDataLiquidacao,
		lidDataProcessamento,
		lidValor,
		lidNatAntiga) VALUES (
		'".contaZero5($tabela['N70U51_UNIDADE_ORCAMENTARIA'])."',
		'".$tabela['N70U51_GESTAO']."',
		'".$tabela['N70U51_ANO']."',
		'".$tabela['N70U51_NUM_NE']."',
		'".$tabela['N70U51_NUM_NL']."',
		'".$tabela['N70U51_NAT_DESPESA_DET']."',
		'".data($tabela['N70U51_DATA_LIQUIDACAO'])."',
		'".date('Y-m-d  H:i:s')."',
		'".$tabela['N70U51_VALOR']."',
		'".$tabela['N70U51_NAT_DESPESA_DET_NOVA']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Liquidacao_Det!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U51_LIQUIDACAO_DET SET N70U51_FLAG_GERADO = 2 where ISN_AP70U51_LIQUIDACAO_DET = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U51_LIQUIDACAO_DET']))or die ("Erro durante o Update AP70U51_LIQUIDACAO_DET!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------------Para tab PAGO_EMPENHO-------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U48_PAGO_EMPENHO WHERE N70U48_FLAG_GERADO_U48=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U48_PAGO_EMPENHO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U48_PAGO_EMPENHO WHERE N70U48_FLAG_GERADO_U48=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U48_PAGO_EMPENHO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos

	   $queryAux2 = ("Delete PAGO_EMPENHO WHERE uo_codusu = '".contaZero5($array[$i]['N70U48_UNIDADE_ORCAMENTARIA_U48'])."'and pe_gestao='".$array[$i]['N70U48_GESTAO_U48']."' and pe_ano='".$array[$i]['N70U48_ANO_U48']."' and pe_no_np='".$array[$i]['N70U48_NOTA_PAGAMENTO_U48']."' and pe_no_ne='".$array[$i]['N70U48_NUM_EMPENHO_U48']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete PAGO_EMPENHO!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO PAGO_EMPENHO(
		uo_codusu,
		pe_gestao,
		pe_ano,
		pe_no_np,
		pe_no_ne,
		pe_valor) VALUES (
		'".contaZero5($tabela['N70U48_UNIDADE_ORCAMENTARIA_U48'])."',
		'".$tabela['N70U48_GESTAO_U48']."',
		'".$tabela['N70U48_ANO_U48']."',
		'".$tabela['N70U48_NOTA_PAGAMENTO_U48']."',
		'".$tabela['N70U48_NUM_EMPENHO_U48']."',
		'".$tabela['N70U48_VALOR_U48']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert PAGO_EMPENHO!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U48_PAGO_EMPENHO SET N70U48_FLAG_GERADO_U48 = 2 where ISN_AP70U48_PAGO_EMPENHO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U48_PAGO_EMPENHO'])) or die ("Erro durante o Update AP70U48_PAGO_EMPENHO!!");
     }
	}// fim if
	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------------Para tab Liquidado---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U36_LIQUIDACAO WHERE N70U36_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U36_LIQUIDACAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U36_LIQUIDACAO WHERE N70U36_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U36_LIQUIDACAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Liquidado WHERE Liq_Unid_Orcamentaria = '".contaZero5($array[$i]['N70U36_UNIDADE_ORCAMENTARIA'])."'and Liq_Gestao='".$array[$i]['N70U36_GESTAO']."' and Liq_Ano='".$array[$i]['N70U36_ANO']."' and Liq_NE='".$array[$i]['N70U36_NUM_ME']."' and Liq_NL='".$array[$i]['N70U36_NUM_NL']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Liquidado!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Liquidado (
		Liq_Unid_Orcamentaria,
		Liq_Gestao,
		Liq_Ano,
		Liq_NE,
		Liq_NL,
		Liq_Data_Liquidacao,
		Liq_Data_Geracao,
		Liq_Valor) VALUES
		('".contaZero5($tabela['N70U36_UNIDADE_ORCAMENTARIA'])."',
		'".$tabela['N70U36_GESTAO']."',
		'".$tabela['N70U36_ANO']."',
		'".$tabela['N70U36_NUM_ME']."',
		'".$tabela['N70U36_NUM_NL']."',
		'".data($tabela['N70U36_DATA_LIQUIDACAO'])."',
		'".date('Y-m-d  H:i:s')."',
		'".$tabela['N70U36_VALOR']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Liquidado!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U36_LIQUIDACAO SET N70U36_FLAG_GERADO = 2 where ISN_AP70U36_LIQUIDACAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U36_LIQUIDACAO'])) or die ("Erro durante o Update AP70U36_LIQUIDACAO!!");
     }
	}//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//-------------------------------------------------------Para tab PTRes-----------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U59_PTRES WHERE N70U59_FG_PTRES=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U59_PTRES!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U59_PTRES WHERE N70U59_FG_PTRES=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U59_PTRES!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete PTRes WHERE PT_CD_Resumido = '".$array[$i]['N70U59_CD_RESUM_PTRES']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete PTRes!");
	}


//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO PTRes (
		PT_UO,
		PT_CD_Completo,
		PT_CD_Resumido,
		PT_Funcao,
		PT_SubFuncao,
		PT_Programa,
		PT_ProjAtiv) VALUES
		('".contaZero5($tabela['N70U59_UNID_ORC_PTRES'])."',
		'".$tabela['N70U59_PROG_TRAB_PTRES']."',
		'".$tabela['N70U59_CD_RESUM_PTRES']."',
		'".$tabela['N70U59_FUNCAO_PTRES']."',
		'".$tabela['N70U59_SUB_FUNC_PTRES']."',
		'".$tabela['N70U59_PROGRAMA_PTRES']."',
		'".$tabela['N70U59_PROJ_ATIV_PTRES']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert PTRes!!");

}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U59_PTRES SET N70U59_FG_PTRES = 2 where ISN_AP70U59_PTRES = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U59_PTRES'])) or die ("Erro durante o Update AP70U59_PTRES!!");
     }
	}//fim if
	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------------Para tab Acao---------------------------------------------------------------
//-------Funcoes Relativas as tabAcao------
function prioridade ($z){
	if ($z==NULL){
	$y= 0;
}
else{
	$y=$z;
}return $y;
}
//-------------------------
function MudaNumerico ($z){
	if ($z == S){
	$y= 1;
} else if ($z == s){
	$y= 1;
}
else{
	$y=0;
}return $y;
}
//-------------------------
function estCod ($z){
	if ($z == 2){
	$y= 4;
} else if ($z == 1){
	$y= 6;
}
else{
	$y=1;
}return $y;
}

//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U12_ACAO WHERE N238U12_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U12_ACAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U12_ACAO WHERE N238U12_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U12_ACAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;



	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;


//deletando dados repetidos

	   $queryAux2 = ("Delete Acao WHERE CD_ACAO = '".contaZero9($array[$i]['N238U12_CD_ACAO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete acao!!");
	}

//tratamento para insersao dos dados na colunas FG_Material e FG_PTRes

	$queryAcao1 = "SELECT COUNT(*) as reg2 FROM PTRes WHERE PT_CD_Resumido= '".contaZero6($tabela['N238U12_GR_PTRES'])."'";
	$Acao1 = odbc_exec( $connSQL, $queryAcao1 );
	$Aux1 = odbc_fetch_array($Acao1);
		$valorPT = (int)$Aux1[reg2];

	if ($valor = 0 And contaZero6($tabela['N238U12_GR_PTRES']) > 0){
		$PTres= 1;
	}
		else{
		$PTres=2;
	}
//--------
	$queryAcao2 = "SELECT COUNT(*) as reg3 FROM Material WHERE CD_MATERIAL = '".contaZero6($tabela['N238U12_CD_MATERIAL'])."'";
	$Acao2 = odbc_exec( $connSQL, $queryAcao2 );
	$Aux2 = odbc_fetch_array($Acao2);
		$valorMat = (int)$Aux2[reg3];

	if ($valor = 0 And contaZero6($tabela['N238U12_CD_MATERIAL']) > 0){
		$material= 1;
	}
		else{
		$material=2;
	}

//efetuando insert no sqlserver
	if ($inteiro){
	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao (
		CD_ACAO,
		CD_TP_ACAO,
		AA_EXERCICIO,
		GR_PTRES,
		CD_DETALHE_ACAO,
		CD_CONECTIVO_OBJ,
		QUANTIDADE,
		CD_MATERIAL,
		CD_OBJETO,
		CD_CONECTIVO_PATRI,
		CD_PATRIMONIO,
		COMPLEMENTO,
		CD_MUNICIPIO_POLO,
		CUSTO_TOTAL,
		AA_MM_INICIO,
		AA_MM_FIM,
		VL_ACM_EMPENHO,
		VL_ACM_LIQUIDADO,
		CD_ORGAO,
		AA_NRECEBIMENTO,
		NR_NRECEBIMENTO,
		NR_ITEM_NR,
		DT_INCLUSAO,
		CD_USUARIO,
		CD_SITUACAO,
		DT_SITUACAO,
		FG_MARCA,
		CD_UNIDADE,
		FG_DETALHE,
		CD_UNID_ORCAM,
		est_codigo,
		da_especial,
		fg_especial,
		FG_MATERIAL,
		FG_PTRES,
		VL_CUSTO_ACUMULADO,
		sn_agenda_min,
		sn_terra_para,
		sn_ptp,
		nm_acao) VALUES (
		'".contaZero9($tabela['N238U12_CD_ACAO'])."',
		'".$tabela['N238U12_CD_TP_ACAO']."',
		'".$tabela['N238U12_AA_EXERCICIO']."',
		'".contaZero6($tabela['N238U12_GR_PTRES'])."',
		'".contaZero3($tabela['N238U12_CD_DETALHE_ACAO'])."',
		'".contaZero2($tabela['N238U12_CD_CONECTIVO_OBJ'])."',
		'".$tabela['N238U12_QUANTIDADE']."',
		'".contaZero6($tabela['N238U12_CD_MATERIAL'])."',
		'".contaZero4($tabela['N238U12_CD_OBJETO'])."',
		'".contaZero2($tabela['N238U12_CD_CONECTIVO_PATRI'])."',
		'".contaZero5($tabela['N238U12_CD_PATRIMONIO'])."',
		'".str_replace("'", " ",$tabela['N238U12_COMPLEMENTO'])."',
		'".contaZero3($tabela['N238U12_CD_MUNICIPIO_POLO'])."',
		Cast ('".$tabela['N238U12_CUSTO_TOTAL']."' as money),
		'".$tabela['N238U12_AA_MM_INICIO']."',
		'".$tabela['N238U12_AA_MM_FIM']."',
		Cast ('".$tabela['N238U12_VL_ACM_EMPENHO']."' as money),
		Cast ('".$tabela['N238U12_VL_ACM_LIQUIDADO']."' as money),
		'".contaZero5($tabela['N238U12_CD_ORGAO'])."',
		'".contaZero4($tabela['N238U12_AA_NRECEBIMENTO'])."',
		'".contaZero6($tabela['N238U12_NR_NRECEBIMENTO'])."',
		'".contaZero3($tabela['N238U12_NR_ITEM_NR'])."',
		'".date('Y-m-d  H:i:s')."',
		'".$tabela['N238U12_CD_USUARIO']."',
		'".$tabela['N238U12_CD_SITUACAO']."',
		'".data($tabela['N238U12_DT_SITUACAO'])."',
		'1',
		'".$tabela['N238U12_CD_UNIDADE']."',
		'".$tabela['N238U12_FG_DETALHE']."',
		'".contaZero5($tabela['N238U12_GR_UNID_ORCAMENTO'])."',
		'".estCod($tabela['N238U12_CD_TP_ACAO'])."',
		'".prioridade($tabela['N238U12_FG_PRIORIDADE'])."',
        '0',
		'".$material."',
		'".$PTres."',
        Cast ('".$tabela['N238U12_CUSTO_ACUMULADO']."' as money),
		'".MudaNumerico($tabela['N238U12_FG_AGENDA_MINIMA'])."',
		'".MudaNumerico($tabela['N238U12_FG_TERRA_DIREITOS'])."',
		'".MudaNumerico($tabela['N238U12_FG_ACAO_PTP'])."',
		'".str_replace("'", " ",$tabela['N238U12_NM_ACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert acao!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U12_ACAO SET N238U12_FG_MARCA = 2 where ISN_GP238U12_ACAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U12_ACAO'])) or die ("Erro durante o Update GP238U12_ACAO!!");
     }
	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//---------------------------------------------------Para tab AcaoMunicBenef------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U14_ACAO_MUNIC_BENEF WHERE N238U14_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U14_ACAO_MUNIC_BENEF!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U14_ACAO_MUNIC_BENEF WHERE N238U14_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U14_ACAO_MUNIC_BENEF!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Acao_Munic_Benef WHERE CD_ACAO = '".contaZero9($array[$i]['N238U14_CD_ACAO'])."'and CD_MUNICIPIO='".contaZero3($array[$i]['N238U14_CD_MUNICIPIO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Acao_Munic_Benef!!");
	}


//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao_Munic_Benef (
		CD_ACAO,
		CD_MUNICIPIO,
		QUANTIDADE,
		VL_LIQUIDADO,
		CD_SITUACAO,
		HH_CONTROLE,
		DT_CONTROLE) VALUES
		('".contaZero9($tabela['N238U14_CD_ACAO'])."',
		'".contaZero3($tabela['N238U14_CD_MUNICIPIO'])."',
		'".$tabela['N238U14_QUANTIDADE']."',
		'".$tabela['N238U14_VL_LIQUIDADO']."',
		'".$tabela['N238U14_CD_SITUACAO']."',
		Cast(Datepart(hour,getDate()) as varchar(2))+':'+Cast(Datepart(minute,getDate()) as varchar(2))+':'+
Cast(Datepart(second,getDate()) as varchar(2)),
		Cast(Datepart(day,getDate()) as varchar(2))+'/'+Cast(Datepart(month,getDate()) as varchar(2))+'/'+
Cast(Datepart(year,getDate()) as varchar(4)))");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Acao_Munic_Benef!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U14_ACAO_MUNIC_BENEF SET N238U14_FG_MARCA = 2 where ISN_GP238U14_ACAO_MUNIC_BENEF = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U14_ACAO_MUNIC_BENEF'])) or die ("Erro durante o Update GP238U14_ACAO_MUNIC_BENEF!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-------------------------------------------------Para tab acaoMunicipio---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U16_ACAO_MUNICIPIO WHERE N238U16_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U16_ACAO_MUNICIPIO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U16_ACAO_MUNICIPIO WHERE N238U16_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U16_ACAO_MUNICIPIO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Acao_Municipio WHERE CD_ACAO = '".contaZero9($array[$i]['N238U16_CD_ACAO'])."'and CD_MUNICIPIO='".contaZero3($array[$i]['N238U16_CD_MUNICIPIO'])."' and NR_NE='".$array[$i]['N238U16_NR_NE']."' and NR_NL='".$array[$i]['N238U16_NR_NL']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Acao_Municipio!!");
	}

//efetuando insert no sqlserver
	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao_Municipio (
		CD_ACAO,
		CD_MUNICIPIO,
		NR_NE,
		NR_NL,
		UO,
		GESTAO,
		ANO,
		NE,NL,
		VL_NL,
		HH_CONTROLE,
		DT_CONTROLE,
		CD_SITUACAO) VALUES (
		'".contaZero9($tabela['N238U16_CD_ACAO'])."',
		'".contaZero3($tabela['N238U16_CD_MUNICIPIO'])."',
		'".$tabela['N238U16_NR_NE']."',
		'".$tabela['N238U16_NR_NL']."',
		left('".$tabela['N238U16_NR_NL']."',2)+right(left('".$tabela['N238U16_NR_NL']."',6),3),
		right(left('".$tabela['N238U16_NR_NE']."',11),5),
		left(right('".$tabela['N238U16_NR_NE']."',11),4),
		right('".$tabela['N238U16_NR_NE']."',5),
		right('".$tabela['N238U16_NR_NL']."',5),
		'".$tabela['N238U16_VL_NL']."',
		Cast(Datepart(hour,getDate()) as varchar(2))+':'+Cast(Datepart(minute,getDate()) as varchar(2))+':'+
Cast(Datepart(second,getDate()) as varchar(2)),
Cast(Datepart(day,getDate()) as varchar(2))+'/'+Cast(Datepart(month,getDate()) as varchar(2))+'/'+
Cast(Datepart(year,getDate()) as varchar(4)),
		1)");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Acao_Municipio!!");
	}


//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U16_ACAO_MUNICIPIO SET N238U16_FG_MARCA = 2 where ISN_GP238U16_ACAO_MUNICIPIO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U16_ACAO_MUNICIPIO'])) or die ("Erro durante o Update GP238U16_ACAO_MUNICIPIO!!");
     }
	}//fim if


	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------------------Para tab Acao_NE--------------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U15_ACAO_NE WHERE N238U15_FG_MARCA=1 and N238U15_CD_ACAO is not null ";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U15_ACAO_NE!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U15_ACAO_NE WHERE N238U15_FG_MARCA=1 and N238U15_CD_ACAO is not null ";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U15_ACAO_NE!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Acao_NE WHERE CD_ACAO = '".contaZero9($array[$i]['N238U15_CD_ACAO'])."'and NR_NE='".$array[$i]['N238U15_NR_NE']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Acao_NE!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao_NE (
		CD_ACAO,
		NR_NE,
		NE_UO,
		NE_GESTAO,
		NE_ANO,
		NE_NO,
		DT_CONTROLE,
		CD_SITUACAO) VALUES (
		'".contaZero9($tabela['N238U15_CD_ACAO'])."',
		'".$tabela['N238U15_NR_NE']."',
		left('".$tabela['N238U15_NR_NE']."',2)+right(left('".$tabela['N238U15_NR_NE']."',6),3),
		right(left('".$tabela['N238U15_NR_NE']."',11),5),
		left(right('".$tabela['N238U15_NR_NE']."',11),4),
		right('".$tabela['N238U15_NR_NE']."',5),
		Cast(Datepart(day,getDate()) as varchar(2))+'/'+Cast(Datepart(month,getDate()) as varchar(2))+'/'+
Cast(Datepart(year,getDate()) as varchar(4)),
		'".cdSituação($tabela['N238U15_CD_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Acao_NE!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U15_ACAO_NE SET N238U15_FG_MARCA = 2 where ISN_GP238U15_ACAO_NE = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U15_ACAO_NE'])) or die ("Erro durante o Update GP238U15_ACAO_NE!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------Para tab Acao_Munic_Patrim-------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U13_ACAO_MUNIC_PATRIM WHERE N238U13_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U13_ACAO_MUNIC_PATRIM!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U13_ACAO_MUNIC_PATRIM WHERE N238U13_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U13_ACAO_MUNIC_PATRIM!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Acao_Munic_Patrim WHERE CD_ACAO = '".contaZero9($array[$i]['N238U13_CD_ACAO'])."'and CD_MUNICIPIO='".contaZero3($array[$i]['N238U13_CD_MUNICIPIO'])."' and CD_PATRIMONIO='".contaZero5($array[$i]['N238U13_CD_PATRIMONIO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Acao_Munic_Patrim!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao_Munic_Patrim (
		CD_ACAO,
		CD_MUNICIPIO,
		CD_PATRIMONIO,
		QUANTIDADE,
		VL_LIQUIDADO,
		CD_SITUACAO,
		DT_SITUACAO,
		HH_CONTROLE,
		DT_CONTROLE) VALUES (
		'".contaZero9($tabela['N238U13_CD_ACAO'])."',
		'".contaZero3($tabela['N238U13_CD_MUNICIPIO'])."',
		'".contaZero5($tabela['N238U13_CD_PATRIMONIO'])."',
		'".$tabela['N238U13_QUANTIDADE']."',
		'".$tabela['N238U13_VL_LIQUIDADO']."',
		'".cdSituação($tabela['N238U13_CD_SITUACAO'])."',
		'".data($tabela['N238U13_DT_SITUACAO'])."',
		Cast(Datepart(hour,getDate()) as varchar(2))+':'+Cast(Datepart(minute,getDate()) as varchar(2))+':'+
Cast(Datepart(second,getDate()) as varchar(2)),
		Cast(Datepart(day,getDate()) as varchar(2))+'/'+Cast(Datepart(month,getDate()) as varchar(2))+'/'+
Cast(Datepart(year,getDate()) as varchar(4)))");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Acao_Munic_Patrim!!");
}
//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U13_ACAO_MUNIC_PATRIM SET N238U13_FG_MARCA = 2 where ISN_GP238U13_ACAO_MUNIC_PATRIM = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U13_ACAO_MUNIC_PATRIM'])) or die ("Erro durante o Update GP238U13_ACAO_MUNIC_PATRIM!!");
     }
	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//------------------------------------------------Para tab Acao_Ptres-------------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U23_ACAO_PTRES WHERE N238U23_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U23_ACAO_PTRES!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U23_ACAO_PTRES WHERE N238U23_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U23_ACAO_PTRES!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Acao_Ptres WHERE CD_ACAO = '".contaZero9($array[$i]['N238U23_CD_ACAO'])."'and GR_PTRES='".contaZero6($array[$i]['N238U23_GR_PTRES'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Acao_Ptres!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Acao_Ptres (
		CD_ACAO,
		AA_EXERCICIO,
		GR_PTRES,
		CD_SITUACAO,
		DT_SITUACAO,
		HH_CONTROLE,
		DT_CONTROLE) VALUES (
'".contaZero9($tabela['N238U23_CD_ACAO'])."',
'".$tabela['N238U23_AA_EXERCICIO']."',
'".contaZero6($tabela['N238U23_GR_PTRES'])."',
'".$tabela['N238U23_CD_SITUACAO']."',
'".data($tabela['N238U23_DT_SITUACAO'])."',
Cast(Datepart(hour,getDate()) as varchar(2))+':'+Cast(Datepart(minute,getDate()) as varchar(2))+':'+
Cast(Datepart(second,getDate()) as varchar(2)),
Cast(Datepart(day,getDate()) as varchar(2))+'/'+Cast(Datepart(month,getDate()) as varchar(2))+'/'+
Cast(Datepart(year,getDate()) as varchar(4)) )");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Acao_Ptres!!");
	}


//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U23_ACAO_PTRES SET N238U23_FG_MARCA = 2 where ISN_GP238U23_ACAO_PTRES = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U23_ACAO_PTRES'])) or die ("Erro durante o Update GP238U23_ACAO_PTRES!!");
     }
	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-------------------------------------------------Para tab Detalhe_Acao---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U1_DETALHE_ACAO WHERE N238U1_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U1_DETALHE_ACAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U1_DETALHE_ACAO WHERE N238U1_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U1_DETALHE_ACAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Detalhe_Acao WHERE CD_DETALHE_ACAO = '".contaZero3($array[$i]['N238U1_CD_DETALHE_ACAO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Detalhe_Acao!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Detalhe_Acao (
		CD_DETALHE_ACAO,
		NM_DETALHE_ACAO,
		CD_TP_INVESTIMENTO,
		CD_SITUACAO,
		DT_SITUACAO) VALUES (
		'".contaZero3($tabela['N238U1_CD_DETALHE_ACAO'])."',
		'".$tabela['N238U1_NM_DETALHE_ACAO']."',
		'".$tabela['N238U1_CD_TP_ACAO']."',
		'".$tabela['N238U1_CD_SITUACAO']."',
		'".data($tabela['N238U1_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Detalhe_Acao!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U1_DETALHE_ACAO SET N238U1_FG_MARCA = 2 where ISN_GP238U1_DETALHE_ACAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U1_DETALHE_ACAO'])) or die ("Erro durante o Update GP238U1_DETALHE_ACAO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------------Para tab Grupo_Acao---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U8_GRUPO_ACAO WHERE N238U8_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U8_GRUPO_ACAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U8_GRUPO_ACAO WHERE N238U8_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U8_GRUPO_ACAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Grupo_Acao WHERE CD_GRUPO_ACAO = '".$array[$i]['N238U8_CD_GRUPO_ACAO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Grupo_Acao!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Grupo_Acao (
		CD_GRUPO_ACAO,
		NM_GRUPO_ACAO,
		CD_SITUACAO,
		DT_SITUACAO) VALUES
		('".$tabela['N238U8_CD_GRUPO_ACAO']."',
		'".$tabela['N238U8_NM_GRUPO_ACAO']."',
		'".$tabela['N238U8_CD_SITUACAO']."',
		'".data($tabela['N238U8_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Grupo_Acao!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U8_GRUPO_ACAO SET N238U8_FG_MARCA = 2 where ISN_GP238U8_GRUPO_ACAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U8_GRUPO_ACAO'])) or die ("Erro durante o Update GP238U8_GRUPO_ACAO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------Para tab Tipo_Patrimonio---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U7_TIPO_PATRIMONIO WHERE N238U7_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U7_TIPO_PATRIMONIO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U7_TIPO_PATRIMONIO WHERE N238U7_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U7_TIPO_PATRIMONIO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Tipo_Patrimonio WHERE CD_TP_PATRIMONIO = '".contaZero3($array[$i]['N238U7_CD_TP_PATRIMONIO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Tipo_Patrimonio!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Tipo_Patrimonio (
		CD_TP_PATRIMONIO,
		NM_TP_PATRIMONIO,
		CD_SITUACAO,
		DT_SITUACAO,
		CD_GRUPO_ACAO) VALUES (
		'".contaZero3($tabela['N238U7_CD_TP_PATRIMONIO'])."',
		'".str_replace("'", " ",$tabela['N238U7_NM_TP_PATRIMONIO'])."',
		'".$tabela['N238U7_CD_SITUACAO']."',
		'".data($tabela['N238U7_DT_SITUACAO'])."',
		'".$tabela['N238U7_CD_GRUPO_ACAO']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Tipo_Patrimonio!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U7_TIPO_PATRIMONIO SET N238U7_FG_MARCA = 2 where ISN_GP238U7_TIPO_PATRIMONIO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U7_TIPO_PATRIMONIO'])) or die ("Erro durante o Update GP238U7_TIPO_PATRIMONIO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------------Para tab Patrimonio---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U5_PATRIMONIO WHERE N238U5_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U5_PATRIMONIO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U5_PATRIMONIO WHERE N238U5_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U5_PATRIMONIO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Patrimonio WHERE CD_PATRIMONIO = '".contaZero5($array[$i]['N238U5_CD_PATRIMONIO'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Patrimonio!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Patrimonio (
		CD_PATRIMONIO,
		NM_PATRIMONIO,
		CD_SETOR,
		CD_MUNICIPIO_POLO,
		CD_TIPO_PAT,
		NM_LOCALIZACAO,
		TP_PROPRIO,
		CD_SITUACAO,
		DT_SITUACAO) VALUES (
		'".contaZero5($tabela['N238U5_CD_PATRIMONIO'])."',
		'".str_replace("'", " ",$tabela['N238U5_NM_PATRIMONIO'])."',
		'".$tabela['N238U5_CD_SETOR']."',
		'".contaZero5($tabela['N238U5_CD_MUNICIPIO_POLO'])."',
		'".$tabela['N238U5_CD_TIPO_PAT']."',
		'".$tabela['N238U5_NM_LOCALIZACAO']."',
		'".$tabela['N238U5_TP_PROPRIO']."',
		'".$tabela['N238U5_CD_SITUACAO']."',
		'".data($tabela['N238U5_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Patrimonio!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U5_PATRIMONIO SET N238U5_FG_MARCA = 2 where ISN_GP238U5_PATRIMONIO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U5_PATRIMONIO'])) or die ("Erro durante o Update GP238U5_PATRIMONIO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------------------Para tab Tipo_Acao-----------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U6_TIPO_ACAO WHERE N238U6_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U6_TIPO_ACAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U6_TIPO_ACAO WHERE N238U6_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U6_TIPO_ACAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Tipo_Acao WHERE CD_TP_ACAO = '".$array[$i]['N238U6_CD_TP_ACAO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Tipo_Acao!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Tipo_Acao (
		CD_TP_ACAO,
		NM_TP_ACAO,
		CD_SITUACAO,
		DT_SITUACAO) VALUES (
		'".$tabela['N238U6_CD_TP_ACAO']."',
		'".$tabela['N238U6_NM_TP_ACAO']."',
		'".$tabela['N238U6_CD_SITUACAO']."',
		'".data($tabela['N238U6_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Tipo_Acao!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U6_TIPO_ACAO SET N238U6_FG_MARCA = 2 where ISN_GP238U6_TIPO_ACAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U6_TIPO_ACAO'])) or die ("Erro durante o Update GP238U6_TIPO_ACAO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------------------Para tab Conectivo-----------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U2_CONECTIVO WHERE N238U2_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U2_CONECTIVO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U2_CONECTIVO WHERE N238U2_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U2_CONECTIVO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Conectivo WHERE CD_CONECTIVO = '".$array[$i]['N238U2_CD_CONECTIVO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete Conectivo!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Conectivo (
		CD_CONECTIVO,
		NM_CONECTIVO,
		CD_SITUACAO,
		DT_SITUACAO) VALUES (
		'".$tabela['N238U2_CD_CONECTIVO']."',
		'".$tabela['N238U2_NM_CONECTIVO']."',
		'".$tabela['N238U2_CD_SITUACAO']."',
		'".data($tabela['N238U2_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Conectivo!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U2_CONECTIVO SET N238U2_FG_MARCA = 2 where ISN_GP238U2_CONECTIVO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U2_CONECTIVO'])) or die ("Erro durante o Update GP238U2_CONECTIVO!!");
     }
	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------------Para tab SubFuncao---------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U27_TAB_SUB_FUNCAO WHERE N238U27_FG_SUB_FUNCAO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U27_TAB_SUB_FUNCAO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U27_TAB_SUB_FUNCAO WHERE N238U27_FG_SUB_FUNCAO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U27_TAB_SUB_FUNCAO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;
	}

//efetuando update no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO SubFuncao (
		sf_exercicio,
		sf_codigo,
		sf_descricao,
		sf_situacao,
		sf_codint) VALUES (
		'".$tabela['N238U27_EX_SUB_FUNCAO']."',
		'".contaZero3($tabela['N238U27_CD_SUB_FUNCAO'])."',
		'".str_replace("'", " ",$tabela['N238U27_DS_SUB_FUNCAO'])."',
		'".$tabela['N238U27_ST_SUB_FUNCAO']."',
		'".$tabela['N238U27_FG_SUB_FUNCAO']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert SubFuncao!!"); // o campo CD_SETOR via e dbSepof

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U27_TAB_SUB_FUNCAO SET N238U27_FG_SUB_FUNCAO = 2 where ISN_GP238U27_TAB_SUB_FUNCAO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U27_TAB_SUB_FUNCAO'])) or die ("Erro durante o Update GP238U27_TAB_SUB_FUNCAO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//------------------------------------------------------Para tab Objeto----------------------------------------------------------
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U3_OBJETO WHERE N238U3_FG_MARCA=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U3_OBJETO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U3_OBJETO WHERE N238U3_FG_MARCA=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U3_OBJETO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Objeto WHERE CD_OBJETO = '".$array[$i]['N238U3_CD_OBJETO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete objeto!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO Objeto (
		CD_OBJETO,
		NM_OBJETO,
		NM_OBJETO_PLURAL,
		CD_UNIDADE,
		CD_SITUACAO,
		DT_SITUACAO) VALUES (
		'".$tabela['N238U3_CD_OBJETO']."',
		'".str_replace("'", " ",$tabela['N238U3_NM_OBJETO'])."',
		'".str_replace("'", " ",$tabela['N238U3_NM_OBJETO_PLURAL'])."',
		'".$tabela['N238U3_CD_UNIDADE']."',
		'".$tabela['N238U3_CD_SITUACAO']."',
		'".data($tabela['N238U3_DT_SITUACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Objeto!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U3_OBJETO SET N238U3_FG_MARCA = 2 where ISN_GP238U3_OBJETO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U3_OBJETO'])) or die ("Erro durante o Update GP238U3_OBJETO!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//---------------------------------------------Para tab QuotasFinanceiras--------------------------------------------------------
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U9_QUOTAS_FINANCEIRAS WHERE N152U9_FLAG_FIN =1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP152U9_QUOTAS_FINANCEIRAS!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP152U9_QUOTAS_FINANCEIRAS WHERE N152U9_FLAG_FIN =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP152U9_QUOTAS_FINANCEIRAS!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete QuotasFinanceiras WHERE QUOTASFINANC_ANO = '".$array[$i]['N152U9_ANO_FIN']."'and QUOTASFINANC_ORGAO='".contaZero5($array[$i]['N152U9_ORGAO_FIN'])."' and QUOTASFINANC_FONTE='".contaZero4($array[$i]['N152U9_FONTE_FIN'])."' and QUOTASFINANC_GRUPO_DESPESA='".$array[$i]['N152U9_GRUPO_DESP_FIN']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete QuotasFinanceiras!!");
	   }

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO QuotasFinanceiras (
		QUOTASFINANC_ANO,
		QUOTASFINANC_ORGAO,
		QUOTASFINANC_FONTE,
		QUOTASFINANC_GRUPO_DESPESA,
		QUOTASFINANC_VALOR_LANC)
		VALUES (
		 '".$tabela['N152U9_ANO_FIN']."',
		 '".contaZero5($tabela['N152U9_ORGAO_FIN'])."',
		 '".contaZero4($tabela['N152U9_FONTE_FIN'])."',
		 '".$tabela['N152U9_GRUPO_DESP_FIN']."',
		 '".$tabela['N152U9_VALOR_LANC_FIN']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert QuotasFinanceiras!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U9_QUOTAS_FINANCEIRAS SET N152U9_FLAG_FIN = 2 where ISN_AP152U9_QUOTAS_FINANCEIRAS= ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U9_QUOTAS_FINANCEIRAS'])) or die ("Erro durante o Update AP152U9_QUOTAS_FINANCEIRAS!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------------Para tab QuotasOrcamentarias------------------------------------------------------
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U8_QUOTAS_ORCAMENTARIAS WHERE N152U8_FLAG_QT=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP152U8_QUOTAS_ORCAMENTARIAS!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP152U8_QUOTAS_ORCAMENTARIAS WHERE N152U8_FLAG_QT=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP152U8_QUOTAS_ORCAMENTARIAS!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete QuotasOrcamentarias WHERE QUOTAS_ANO = '".$array[$i]['N152U8_ANO_QUOTAS']."'and QUOTAS_UNID_ORCA='".contaZero5($array[$i]['N152U8_UNID_ORCA_QT'])."' and QUOTAS_GRUPO_DESPESA ='".$array[$i]['N152U8_GRUPO_DESP_QT']."' and QUOTAS_FONTE='".contaZero4($array[$i]['N152U8_FONTE_QT'])."'and QUOTAS_EVENTO='".$array[$i]['N152U8_EVENTO_QT']."'and QUOTAS_NL='".contaZero5($array[$i]['N152U8_NL_QT'])."'and QUOTAS_DATA_EMISSAO='".$array[$i]['N152U8_DATA_EMISSAO']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete QuotasOrcamentarias!!");
	   }

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO QuotasOrcamentarias (
		QUOTAS_ANO,
		QUOTAS_UNID_ORCA,
		QUOTAS_GRUPO_DESPESA,
		QUOTAS_FONTE,
		QUOTAS_EVENTO,
		QUOTAS_VALOR,
		QUOTAS_NL,
		QUOTAS_DATA_EMISSAO)
		VALUES
		 ('".$tabela['N152U8_ANO_QUOTAS']."',
		 '".contaZero5($tabela['N152U8_UNID_ORCA_QT'])."',
		 '".$tabela['N152U8_GRUPO_DESP_QT']."',
		 '".contaZero4($tabela['N152U8_FONTE_QT'])."',
		 '".$tabela['N152U8_EVENTO_QT']."',
		 '".$tabela['N152U8_VALOR_QT']."',
		 '".contaZero5($tabela['N152U8_NL_QT'])."',
		 '".data($tabela['N152U8_DATA_EMISSAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert QuotasOrcamentarias!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U8_QUOTAS_ORCAMENTARIAS SET N152U8_FLAG_QT = 2 where ISN_AP152U8_QUOTAS_ORCAMENTARIAS = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U8_QUOTAS_ORCAMENTARIA'])) or die ("Erro durante o Update AP152U8_QUOTAS_ORCAMENTARIAS!!");
     }
	 }//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//------------------------------------Para tab basPlanoInternoMovimento(dbSepof)---------------------------(MovimentoPlanoInterno)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U7_MOVIMENTO_PLANO_INTERNO WHERE N152U7_FLAG_MOV=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP152U7_MOVIMENTO_PLANO_INTERNO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP152U7_MOVIMENTO_PLANO_INTERNO WHERE N152U7_FLAG_MOV=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP152U7_MOVIMENTO_PLANO_INTERNO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete BasPlanoInternoMovimento WHERE PIMOV_ANO = '".$array[$i]['N152U7_ANO_MOV']."'and PIMOV_ORGAO='".contaZero6($array[$i]['N152U7_ORGAO'])."' and PIMOV_GESTAO ='".contaZero5($array[$i]['N152U7_GESTAO'])."' and PIMOV_CONTA_CORRENTE='".$array[$i]['N152U7_CONTA_CORRENTE']."'and PIMOV_DATA_LANCAMENTO='".$array[$i]['N152U7_DATA_LANCAMENTO']."'and PIMOV_EVENTO='".$array[$i]['N152U7_EVENTO']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 ) or die ("Erro durante o delete BasPlanoInternoMovimento!!");

	   }

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO BasPlanoInternoMovimento (
		PIMOV_ANO,
		PIMOV_ORGAO,
		PIMOV_GESTAO,
		PIMOV_CONTA_CORRENTE,
		PIMOV_DATA_LANCAMENTO,
		PIMOV_NUM_DOCUMENTO,
		PIMOV_EVENTO,
		PIMOV_VALOR_LANCAMENTO,
		PIMOV_PI,
		PIMOV_DECRETO)
		VALUES(
		 '".$tabela['N152U7_ANO_MOV']."',
		 '".contaZero6($tabela['N152U7_ORGAO'])."',
		 '".contaZero5($tabela['N152U7_GESTAO'])."',
		 '".$tabela['N152U7_CONTA_CORRENTE']."',
		 '".data($tabela['N152U7_DATA_LANCAMENTO'])."',
		 '".$tabela['N152U7_NUM_DOCUMENTO']."',
		 '".$tabela['N152U7_EVENTO']."',
		 '".$tabela['N152U7_VALOR_LANCAMENTO']."',
		 '".$tabela['N152U7_MOV_PI']."',
		 '".$tabela['N152U7_DECRETO']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 ) or die ("Erro durante o insert BasPlanoInternoMovimento!!");

	}

//Realizando Update da Flag

odbc_close ($connAdabas);

  $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U7_MOVIMENTO_PLANO_INTERNO SET N152U7_FLAG_MOV = 2 where ISN_AP152U7_MOVIMENTO_PLANO_INTERNO = ? ");


  	 for ($j=0; $j<$inteiro; $j++){
		$res = odbc_prepare ($connAdabas, $queryAux3);
		$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U7_MOVIMENTO_PLANO_INT']))  or die ("Erro durante o update AP152U7_MOVIMENTO_PLANO_INTERNO!!");
		}
		}//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//----------------------------------------Para tab basTabelaPlanoInterno(dbSepof)---------------------------------(PlanoInternoGP)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U6_TABELA_PLANO_INTERNO_GP WHERE N152U6_FLAG_TAB_PI=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP152U6_TABELA_PLANO_INTERNO_GP!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP152U6_TABELA_PLANO_INTERNO_GP WHERE N152U6_FLAG_TAB_PI =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP152U6_TABELA_PLANO_INTERNO_GP!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete basTabelaPlanoInterno WHERE TPI_COD = '".$array[$i]['N152U6_COD_PI']."'and TPI_ORGAO='".contaZero5($array[$i]['N152U6_ORGAO'])."' and TPI_ANO ='".$array[$i]['N152U6_ANO']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 ) or die ("Erro durante o delete basTabelaPlanoInterno!!");
	   }

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO basTabelaPlanoInterno (
		TPI_COD,
		TPI_NOME_DO_PI,
		TPI_NOME_ORC_1,
		TPI_ORGAO,
		TPI_UNID_ORCA,
		TPI_ANO)
		VALUES
		 ('".$tabela['N152U6_COD_PI']."',
		 '".str_replace("'", " ",$tabela['N152U6_NOME_DO_PI'])."',
		 '".str_replace("'", " ",$tabela['N152U6_NOME_ORC_1'])."',
		 '".contaZero5($tabela['N152U6_ORGAO'])."',
		 '".contaZero5($tabela['N152U6_UNID_ORCA'])."',
		 '".$tabela['N152U6_ANO']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 ) or die ("Erro durante o insert basTabelaPlanoInterno!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U6_TABELA_PLANO_INTERNO_GP SET N152U6_FLAG_TAB_PI = 2 where ISN_AP152U6_TABELA_PLANO_INTERNO_GP = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U6_TABELA_PLANO_INTERN'])) or die ("Erro durante o Update AP152U6_TABELA_PLANO_INTERNO_GP!!");
     }
	 }//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



///------------------------------------------------Para tab DescriçãoMaterias-------------------------------------------------(Grupo)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U34_DESCRICAO_MATERIAL where N238U34_FG = 1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select SIMA143U19_GRUPO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U34_DESCRICAO_MATERIAL WHERE N238U34_FG =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count SIMA143U19_GRUPO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete Material WHERE CD_MATERIAL ='".contaZero6($array[$i]['N238U34_CD_MATERIAL'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete DescriçãoMaterias!!");
	   }


	   foreach($array as $tabela) {
		$query2 = ("INSERT INTO Material (
		CD_MATERIAL,
		NM_MATERIAL,
		UN_MATERIAL,
		TP_GRUPO,
		CD_GRUPO,
		CD_CLASSE)
		VALUES
		 ('".contaZero6($tabela['N238U34_CD_MATERIAL'])."',
		 '".str_replace("'", " ",$tabela['N238U34_DESCR_MATERIAL'])."',
		 '".$tabela['N238U34_UNID_MATERIAL']."',
		 '".$tabela['N238U34_TP_GRUPO']."',
		 '".contaZero2($tabela['N238U34_CD_GRUPO'])."',
		 '".contaZero3($tabela['N238U34_CD_CLASSE'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert Material!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U34_DESCRICAO_MATERIAL SET N238U34_FG = 2 where ISN_GP238U34_DESCRICAO_MATERIAL = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_GP238U34_DESCRICAO_MATERIAL'])) or die ("Erro durante o Update SEPOF_GPPARA_PRD.dbo.GP238U34_DESCRICAO_MATERIAL!!");
     }
	 }//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//---------------------------------------------Para tab ClassedeMaterias--------------------------------------------------(Classe)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.SIMA143U3_CLASSE WHERE N143U3_FG_CARGA_GP=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select SIMA143U3_CLASSE!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.SIMA143U3_CLASSE WHERE N143U3_FG_CARGA_GP =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count SIMA143U3_CLASSE!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete CLASSE_MAT WHERE TP_GRUPO = '".$array[$i]['N143U3_TP_GRUPO']."'and CD_GRUPO='".contaZero2($array[$i]['N143U3_CD_GRUPO'])."' and CD_CLASSE ='".contaZero3($array[$i]['N143U3_CD_CLASSE'])."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete CLASSE_MAT!!");
	   }


	foreach($array as $tabela) {
		$query2 = ("INSERT INTO CLASSE_MAT (
		TP_GRUPO,
		CD_GRUPO,
		CD_CLASSE,
		NM_CLASSE)
		VALUES
		 ('".$tabela['N143U3_TP_GRUPO']."',
		 '".contaZero2($tabela['N143U3_CD_GRUPO'])."',
		 '".contaZero3($tabela['N143U3_CD_CLASSE'])."',
		 '".str_replace("'", " ",$tabela['N143U3_NM_CLASSE'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert CLASSE_MAT!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.SIMA143U3_CLASSE SET N143U3_FG_CARGA_GP = 2 where ISN_SIMA143U3_CLASSE = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_SIMA143U3_CLASSE'])) or die ("Erro durante o Update SIMA143U3_CLASSE!!");
     }
	 }//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//------------------------------------------------Para tab GrupodeMaterias-------------------------------------------------(Grupo)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.SIMA143U19_GRUPO WHERE N143U19_FG_CARGA_GP =1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select SIMA143U19_GRUPO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.SIMA143U19_GRUPO WHERE N143U19_FG_CARGA_GP =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count SIMA143U19_GRUPO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete GRUPO_MAT WHERE TP_GRUPO = '".$array[$i]['N143U3_TP_GRUPO']."'and CD_GRUPO='".contaZero2($array[$i]['N143U3_CD_GRUPO'])."' and CD_CLASSE ='".$array[$i]['N143U3_CD_CLASSE']."'");
	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete GRUPO_MAT!!");
	   }


	   foreach($array as $tabela) {
		$query2 = ("INSERT INTO GRUPO_MAT (
		TP_GRUPO,
		CD_GRUPO,
		NM_GRUPO)
		VALUES
		 ('".$tabela['N143U19_TP_GRUPO']."',
		 '".contaZero2($tabela['N143U19_CD_GRUPO'])."',
		 '".str_replace("'", " ",$tabela['N143U19_NM_GRUPO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert GRUPO_MAT!!");

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.SIMA143U19_GRUPO SET N143U19_FG_CARGA_GP = 2 where ISN_SIMA143U19_GRUPO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_SIMA143U19_GRUPO'])) or die ("Erro durante o Update SIMA143U19_GRUPO!!");
     }
	 }//fim do if

	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------Para tab pgpPlanoContas_Tradicao(dbSepof) da tab AP70U52_PLANO_CONTAS--------------------------------------------------------
//select para planodecontas

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U52_PLANO_CONTAS WHERE N70U52_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U52_PLANO_CONTAS!!");

	$querygp= "SELECT * FROM pgpPlanoContas_Tradicao";
	$selgp = odbc_exec( $conndbSepof, $querygp ) or die ("Erro durante o select pgpPlanoContas_Tradicao!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U52_PLANO_CONTAS WHERE N70U52_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U52_PLANO_CONTAS!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	$queryAuxgp= "select count (*) as reggp from pgpPlanoContas_Tradicao";
	$contagp = odbc_exec( $conndbSepof, $queryAuxgp ) or die ("Erro durante o select count pgpPlanoContas_Tradicao!!");
	$totalgp = odbc_fetch_array($contagp);

			$inteirogp = (int)$totalgp[reggp];

if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//update para dados onde o pcCodusu e ano são iguais e há apenas alteração da descricao

	   $queryAux2 = ("Update pgpPlanoContas_Tradicao set pcDsConta = '".str_replace("'", " ",$array[$i]['N70U52_DESC_CONTA'])."'  WHERE pcCodusu = '".$array[$i]['N70U52_COD_CONTA']."' and pcAno='".$array[$i]['N70U52_ANO']."'");
	   $update = odbc_exec( $conndbSepof, $queryAux2 )  or die ("Erro durante o Delete basPlanoContas!!");

	 }

	for ($i=0; $i<$inteirogp; $i++){
	$linhagp = odbc_fetch_array($selgp);
	$array_gp[] = $linhagp;
	}


	for ($i=0; $i<$inteiro; $i++){
		for ($j=0; $j<$inteirogp; $j++){
			if($array[$i]['N70U52_COD_CONTA'] == $array_gp[$j]['pcCodusu'] and $array[$i]['N70U52_ANO'] == $array_gp[$j]['pcAno']){
				unset($array[$i]);
			}
		}
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO pgpPlanoContas_Tradicao(
		pcCodusu,
		pcDsConta,
		pcAno,
		pcLancamento,
		pcStatus,
		pcEscrituracao) VALUES (
		'".$tabela['N70U52_COD_CONTA']."',
		'".str_replace("'", " ",$tabela['N70U52_DESC_CONTA'])."',
		'".$tabela['N70U52_ANO']."',
		'".$tabela['N70U52_LANC_NLSALDO']."',
		'".funcStatus($tabela['N70U52_STATUS'])."',
		'".$tabela['N70U52_ESCRITURACAO']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 )  or die ("Erro durante o insert basPlanoContas!!");
	}

}//fim if
	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------Para tab basPlanoContas(dbSepof)--------------------------------------------------------
//select para planodecontas

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U52_PLANO_CONTAS WHERE N70U52_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U52_PLANO_CONTAS!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U52_PLANO_CONTAS WHERE N70U52_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U52_PLANO_CONTAS!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos
	   $queryAux2 = ("Delete basPlanoContas WHERE pcCodusu = '".$array[$i]['N70U52_COD_CONTA']."'and pcAno='".$array[$i]['N70U52_ANO']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 )  or die ("Erro durante o Delete basPlanoContas!!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO basPlanoContas(
		pcCodusu,
		pcDsConta,
		pcAno,
		pcLancamento,
		pcStatus,
		pcEscrituracao) VALUES (
		'".$tabela['N70U52_COD_CONTA']."',
		'".str_replace("'", " ",$tabela['N70U52_DESC_CONTA'])."',
		'".$tabela['N70U52_ANO']."',
		'".$tabela['N70U52_LANC_NLSALDO']."',
		'".funcStatus($tabela['N70U52_STATUS'])."',
		'".$tabela['N70U52_ESCRITURACAO']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 )  or die ("Erro durante o insert basPlanoContas!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U52_PLANO_CONTAS SET N70U52_FLAG_GERADO = 2 where ISN_AP70U52_PLANO_CONTAS = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U52_PLANO_CONTAS']))  or die ("Erro durante o Update AP70U52_PLANO_CONTAS!!");
     }

	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------Para tab pgpPlanoContas_Tradicao(dbSepof) da tab PLANO_CONTAS_VELHO--------------------------------------------------------
//select para planodecontas

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U60_PLANO_CONTAS_VELHO WHERE N70U60_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U60_PLANO_CONTAS_VELHO!!");

	$querygp= "SELECT * FROM pgpPlanoContas_Tradicao";
	$selgp = odbc_exec( $conndbSepof, $querygp ) or die ("Erro durante o select pgpPlanoContas_Tradicao!!");


//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U60_PLANO_CONTAS_VELHO WHERE N70U60_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U60_PLANO_CONTAS_VELHO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	$queryAuxgp= "select count (*) as reggp from pgpPlanoContas_Tradicao";
	$contagp = odbc_exec( $conndbSepof, $queryAuxgp ) or die ("Erro durante o select count pgpPlanoContas_Tradicao!!");
	$totalgp = odbc_fetch_array($contagp);

			$inteirogp = (int)$totalgp[reggp];


if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//update para dados onde o pcCodusu e ano são iguais e há apenas alteração da descricao

	   $queryAux2 = ("Update pgpPlanoContas_Tradicao set pcDsConta = '".str_replace("'", " ",$array[$i]['N70U60_DESC_CONTA'])."'  WHERE pcCodusu = '".$array[$i]['N70U60_COD_CONTA']."' and pcAno='".$array[$i]['N70U60_ANO']."'");
	   $update = odbc_exec( $conndbSepof, $queryAux2 )  or die ("Erro durante o Delete basPlanoContas!!");

	}

	for ($i=0; $i<$inteirogp; $i++){
	$linhagp = odbc_fetch_array($selgp);
	$array_gp[] = $linhagp;
	}


	for ($i=0; $i<$inteiro; $i++){
		for ($j=0; $j<$inteirogp; $j++){
			if($array[$i]['N70U60_COD_CONTA'] == $array_gp[$j]['pcCodusu'] and $array[$i]['N70U60_ANO'] == $array_gp[$j]['pcAno']){
				unset($array[$i]);
			}
		}
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO pgpPlanoContas_Tradicao(
		pcCodusu,
		pcDsConta,
		pcAno,
		pcLancamento,
		pcStatus,
		pcEscrituracao) VALUES (
		'".$tabela['N70U60_COD_CONTA']."',
		'".str_replace("'", " ",$tabela['N70U60_DESC_CONTA'])."',
		'".$tabela['N70U60_ANO']."',
		'".$tabela['N70U60_LANC_NLSALDO']."',
		'".funcStatus($tabela['N70U60_STATUS'])."',
		'".$tabela['N70U60_ESCRITURACAO']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 )  or die ("Erro durante o insert basPlanoContas!!");
	}



}//fim if
	unset($array);
	unset($tabela);
	ob_clean();



//--------------------------------------Para tab basPlanoContasVelho(dbSepof)--------------------------------------------------------
//select para planodecontas

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U60_PLANO_CONTAS_VELHO WHERE N70U60_FLAG_GERADO=1";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U60_PLANO_CONTAS_VELHO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U60_PLANO_CONTAS_VELHO WHERE N70U60_FLAG_GERADO=1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U60_PLANO_CONTAS_VELHO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	//deletando dados repetidos
	   $queryAux2 = ("Delete basPlanoContas_Ant WHERE pcCodusu = '".$array[$i]['N70U60_COD_CONTA']."'and pcAno='".$array[$i]['N70U60_ANO']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 )  or die ("Erro durante o Delete basPlanoContas_Ant !!");
	}

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO basPlanoContas_Ant(
		pcCodusu,
		pcDsConta,
		pcAno,
		pcLancamento,
		pcStatus,
		pcEscrituracao,
		sup_ano_conta) VALUES (
		'".$tabela['N70U60_COD_CONTA']."',
		'".str_replace("'", " ",$tabela['N70U60_DESC_CONTA'])."',
		'".$tabela['N70U60_ANO']."',
		'".$tabela['N70U60_LANC_NLSALDO']."',
		'".funcStatus($tabela['N70U60_STATUS'])."',
		'".$tabela['N70U60_ESCRITURACAO']."',
		'".$tabela['N70U60_SUP_ANO_CONTA']."')");
 		$insert = odbc_exec( $conndbSepof, $query2 )  or die ("Erro durante o insert basPlanoContas_Ant !!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U60_PLANO_CONTAS_VELHO SET N70U60_FLAG_GERADO = 2 where ISN_AP70U60_PLANO_CONTAS_VELHO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U60_PLANO_CONTAS_VELHO']))  or die ("Erro durante o Update AP70U60_PLANO_CONTAS_VELHO!!");
     }

	}//fim if

	unset($array);
	unset($tabela);
	ob_clean();

//--------------------------------------------------Para tab DotacaoAtualizada----------------------------------(DOTACAO_ORCAMENT)
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT WHERE N70U44_FLAG_GERACAO=1 and N70U44_MES_DOTACAO=99 and N70U44_UNID_ORCAM<>96201 and N70U44_ANO_DOTACAO in ('2015','2016')";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "Select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT WHERE N70U44_FLAG_GERACAO=1 and N70U44_MES_DOTACAO=99 and N70U44_UNID_ORCAM<>96201 and N70U44_ANO_DOTACAO in ('2015','2016')";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;


	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	   $queryAux2 = ("Delete DotacaoAtualizada WHERE DTA_ANO = '".$array[$i]['N70U44_ANO_DOTACAO']."' and DTA_UNID_ORCAM='".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."' and DTA_PROG_TRAB = '".$array[$i]['N70U44_PROG_TRAB']."' and DTA_NAT_DESP='".$array[$i]['N70U44_NAT_DESP']."' and DTA_FONTE = '".$array[$i]['N70U44_FONTE_NOVA']."'");
	   	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete DotacaoAtualizada!!");


		$query2 = ("INSERT INTO DotacaoAtualizada (
		DTA_ANO,
		DTA_UNID_ORCAM,
		DTA_FUNCAO,
		DTA_SUBFUNCAO,
		DTA_PROGRAMA,
		DTA_PROJATIV,
		DTA_NAT_DESP,
		DTA_FONTE,
		DTA_PROG_TRAB,
		DTA_DATA_GERACAO,
		DTA_VL_DOT_INI,
		DTA_VL_DOT_ALT,
		DTA_VL_DOT_ESP,
		DTA_VL_DOT_EXT,
		DTA_VL_DOT_SUPL,
		DTA_VL_DOT_CANC,
		DTA_VL_DOT_AUTOR,
		DTA_VL_DOT_CONTING,
		DTA_VL_DOT_MOVTO_CRED,
		DTA_VL_DOT_CONTENC,
		DTA_VL_DOT_FIN,
		DTA_FONTE_NOVA,
		DTA_PROGRAMA_AUX) VALUES
		('".$array[$i]['N70U44_ANO_DOTACAO']."',
		'".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."',
		left('".$array[$i]['N70U44_PROG_TRAB']."',2),
		right(left('".$array[$i]['N70U44_PROG_TRAB']."',5),3),
		'".dtaProg($array[$i]['N70U44_ANO_DOTACAO'],substr($array[$i]['N70U44_PROG_TRAB'],5,-4))."',
		right('".$array[$i]['N70U44_PROG_TRAB']."',4),
		'".$array[$i]['N70U44_NAT_DESP']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		'".$array[$i]['N70U44_PROG_TRAB']."',
		'".data($array[$i]['N70U44_DATA_GERACAO'])."',
		'".$array[$i]['N70U44_VL_DOT_INI']."',
		'".$array[$i]['N70U44_VL_DOT_ALT']."',
		'".$array[$i]['N70U44_VL_DOT_ESP']."',
		'".$array[$i]['N70U44_VL_DOT_EXT']."',
		'".$array[$i]['N70U44_VL_DOT_SUPL']."',
		'".$array[$i]['N70U44_VL_DOT_CANC']."',
		'".$array[$i]['N70U44_VL_DOT_AUTOR']."',
		'".$array[$i]['N70U44_VL_DOT_CONTING']."',
		'".$array[$i]['N70U44_VL_DOT_MOVTO_CRED']."',
		'".$array[$i]['N70U44_VL_DOT_CONTENC']."',
		'".$array[$i]['N70U44_VL_DOT_FIN']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		left(right('".$array[$i]['N70U44_PROG_TRAB']."',8),4))");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert DotacaoAtualizada!!");


}//fim for


//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT SET N70U44_FLAG_GERACAO = 2 where ISN_AP70U44_DOTACAO_ORCAMENT = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U44_DOTACAO_ORCAMENT'])) or die ("Erro durante o Update AP70U44_DOTACAO_ORCAMENT!!");
     }
	}//fim if


	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------Para tab DotacaoAtualizadaMes----------------------------------(DOTACAO_ORCAMENT)
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT WHERE N70U44_FLAG_GERACAO=1 and N70U44_MES_DOTACAO<>99 and N70U44_UNID_ORCAM<>96201 and N70U44_ANO_DOTACAO in ('2015','2016','2017') {maxrows 1001}";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U44_DOTACAO_ORCAMENT!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT WHERE N70U44_FLAG_GERACAO=1 and N70U44_MES_DOTACAO<>99 and N70U44_UNID_ORCAM<>96201 and N70U44_ANO_DOTACAO in ('2015','2016',''2017)";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U44_DOTACAO_ORCAMENT!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];

if ($inteiro > 1000){
		$cont[]=1000;

	for ($i=0; $i<1000; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	   $queryAux2 = ("Delete DotacaoAtualizadaMes WHERE DAM_ANO = '".$array[$i]['N70U44_ANO_DOTACAO']."' and DAM_UNID_ORCAM='".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."' and DAM_PROG_TRAB = '".$array[$i]['N70U44_PROG_TRAB']."' and DAM_NAT_DESP='".$array[$i]['N70U44_NAT_DESP']."' and DAM_FONTE = '".$array[$i]['N70U44_FONTE_NOVA']."' and DAM_MES_DOTACAO = '".$array[$i]['N70U44_MES_DOTACAO']."' and DAM_FONTE_NOVA = '".$array[$i]['N70U44_FONTE_NOVA']."'");
	   	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o Delete DotacaoAtualizadaMes!!");

		$query2 = ("INSERT INTO DotacaoAtualizadaMes (
		DAM_ANO,
		DAM_UNID_ORCAM,
		DAM_FUNCAO,
		DAM_SUBFUNCAO,
		DAM_PROGRAMA,
		DAM_PROJATIV,
		DAM_NAT_DESP,
		DAM_FONTE,
		DAM_MES_DOTACAO,
		DAM_PROG_TRAB,
		DAM_DATA_GERACAO,
		DAM_VL_DOT_INI,
		DAM_VL_DOT_ALT,
		DAM_VL_DOT_ESP,
		DAM_VL_DOT_EXT,
		DAM_VL_DOT_SUPL,
		DAM_VL_DOT_CANC,
		DAM_VL_DOT_AUTOR,
		DAM_VL_DOT_CONTING,
		DAM_VL_DOT_MOVTO_CRED,
		DAM_VL_DOT_CONTENC,
		DAM_VL_DOT_FIN,
		DAM_FONTE_NOVA,
		DAM_PROGRAMA_AUX) VALUES
		('".$array[$i]['N70U44_ANO_DOTACAO']."',
		'".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."',
		left('".$array[$i]['N70U44_PROG_TRAB']."',2),
		right(left('".$array[$i]['N70U44_PROG_TRAB']."',5),3),
		'".dtaProg($array[$i]['N70U44_ANO_DOTACAO'],substr($array[$i]['N70U44_PROG_TRAB'],5,-4))."',
		right('".$array[$i]['N70U44_PROG_TRAB']."',4),
		'".$array[$i]['N70U44_NAT_DESP']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		'".$array[$i]['N70U44_MES_DOTACAO']."',
		'".$array[$i]['N70U44_PROG_TRAB']."',
		'".data($array[$i]['N70U44_DATA_GERACAO'])."',
		'".$array[$i]['N70U44_VL_DOT_INI']."',
		'".$array[$i]['N70U44_VL_DOT_ALT']."',
		'".$array[$i]['N70U44_VL_DOT_ESP']."',
		'".$array[$i]['N70U44_VL_DOT_EXT']."',
		'".$array[$i]['N70U44_VL_DOT_SUPL']."',
		'".$array[$i]['N70U44_VL_DOT_CANC']."',
		'".$array[$i]['N70U44_VL_DOT_AUTOR']."',
		'".$array[$i]['N70U44_VL_DOT_CONTING']."',
		'".$array[$i]['N70U44_VL_DOT_MOVTO_CRED']."',
		'".$array[$i]['N70U44_VL_DOT_CONTENC']."',
		'".$array[$i]['N70U44_VL_DOT_FIN']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		left(right('".$array[$i]['N70U44_PROG_TRAB']."',8),4))");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert DotacaoAtualizadaMes!!");

}
//Realizando Update da Flag

//var_dump($array);
   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT SET N70U44_FLAG_GERACAO = 2 where ISN_AP70U44_DOTACAO_ORCAMENT = ? ");

  	 for ($j=0; $j<1000; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U44_DOTACAO_ORCAMENT'])) or die ("Erro durante o update AP70U44_DOTACAO_ORCAMENT!!");
     }
}//fim if

//-------------------------------------------------------
	else {
		$cont[]=$inteiro;

	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

	   $queryAux2 = ("Delete DotacaoAtualizadaMes WHERE DAM_ANO = '".$array[$i]['N70U44_ANO_DOTACAO']."' and DAM_UNID_ORCAM='".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."' and DAM_PROG_TRAB = '".$array[$i]['N70U44_PROG_TRAB']."' and DAM_NAT_DESP='".$array[$i]['N70U44_NAT_DESP']."' and DAM_FONTE = '".$array[$i]['N70U44_FONTE_NOVA']."' and DAM_MES_DOTACAO = '".$array[$i]['N70U44_MES_DOTACAO']."' and DAM_FONTE_NOVA = '".$array[$i]['N70U44_FONTE_NOVA']."'");
	   	   $delete = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o Delete DotacaoAtualizadaMes!!");

		$query2 = ("INSERT INTO DotacaoAtualizadaMes (
		DAM_ANO,
		DAM_UNID_ORCAM,
		DAM_FUNCAO,
		DAM_SUBFUNCAO,
		DAM_PROGRAMA,
		DAM_PROJATIV,
		DAM_NAT_DESP,
		DAM_FONTE,
		DAM_MES_DOTACAO,
		DAM_PROG_TRAB,
		DAM_DATA_GERACAO,
		DAM_VL_DOT_INI,
		DAM_VL_DOT_ALT,
		DAM_VL_DOT_ESP,
		DAM_VL_DOT_EXT,
		DAM_VL_DOT_SUPL,
		DAM_VL_DOT_CANC,
		DAM_VL_DOT_AUTOR,
		DAM_VL_DOT_CONTING,
		DAM_VL_DOT_MOVTO_CRED,
		DAM_VL_DOT_CONTENC,
		DAM_VL_DOT_FIN,
		DAM_FONTE_NOVA,
		DAM_PROGRAMA_AUX) VALUES
		('".$array[$i]['N70U44_ANO_DOTACAO']."',
		'".contaZero5($array[$i]['N70U44_UNID_ORCAM'])."',
		left('".$array[$i]['N70U44_PROG_TRAB']."',2),
		right(left('".$array[$i]['N70U44_PROG_TRAB']."',5),3),
		'".dtaProg($array[$i]['N70U44_ANO_DOTACAO'],substr($array[$i]['N70U44_PROG_TRAB'],5,-4))."',
		right('".$array[$i]['N70U44_PROG_TRAB']."',4),
		'".$array[$i]['N70U44_NAT_DESP']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		'".$array[$i]['N70U44_MES_DOTACAO']."',
		'".$array[$i]['N70U44_PROG_TRAB']."',
		'".data($array[$i]['N70U44_DATA_GERACAO'])."',
		'".$array[$i]['N70U44_VL_DOT_INI']."',
		'".$array[$i]['N70U44_VL_DOT_ALT']."',
		'".$array[$i]['N70U44_VL_DOT_ESP']."',
		'".$array[$i]['N70U44_VL_DOT_EXT']."',
		'".$array[$i]['N70U44_VL_DOT_SUPL']."',
		'".$array[$i]['N70U44_VL_DOT_CANC']."',
		'".$array[$i]['N70U44_VL_DOT_AUTOR']."',
		'".$array[$i]['N70U44_VL_DOT_CONTING']."',
		'".$array[$i]['N70U44_VL_DOT_MOVTO_CRED']."',
		'".$array[$i]['N70U44_VL_DOT_CONTENC']."',
		'".$array[$i]['N70U44_VL_DOT_FIN']."',
		'".$array[$i]['N70U44_FONTE_NOVA']."',
		left(right('".$array[$i]['N70U44_PROG_TRAB']."',8),4))");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert DotacaoAtualizadaMes!!");

}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U44_DOTACAO_ORCAMENT SET N70U44_FLAG_GERACAO = 2 where ISN_AP70U44_DOTACAO_ORCAMENT = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP70U44_DOTACAO_ORCAMENT'])) or die ("Erro durante o update AP70U44_DOTACAO_ORCAMENT!!");
     }

} //fim else if


	unset($array);
	unset($tabela);
	ob_clean();



//-----------------------------------------------Para tab ELEMENTO_DESPESA--------------------------------------(TAB_NAT_DESPESA)
//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.GP238U31_TAB_NAT_DESPESA WHERE N238U31_FG_NAT_DESP=1 And Left(N238U31_CD_NAT_DESP,1) In('3','4','9')";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select GP238U31_TAB_NAT_DESPESA!!");

	$querygp= "SELECT * FROM ELEMENTO_DESPESA";
	$selgp = odbc_exec( $connSQL, $querygp ) or die ("Erro durante o select ELEMENTO_DESPESA!!");


//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.GP238U31_TAB_NAT_DESPESA WHERE N238U31_FG_NAT_DESP=1 And Left(N238U31_CD_NAT_DESP,1) In('3','4','9')";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count GP238U31_TAB_NAT_DESPESA!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	$queryAuxgp= "select count (*) as reggp from ELEMENTO_DESPESA";
	$contagp = odbc_exec( $connSQL, $queryAuxgp ) or die ("Erro durante o select count GP238U31_TAB_NAT_DESPESA!!");
	$totalgp = odbc_fetch_array($contagp);

			$inteirogp = (int)$totalgp[reggp];



 	if ($inteiro){
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array_adabas[] = $linha;
	$array_upadte[] = $linha;

	}

	for ($i=0; $i<$inteirogp; $i++){
	$linhagp = odbc_fetch_array($selgp);
	$array_gp[] = $linhagp;
	}

	for ($i=0; $i<$inteiro; $i++){
		for ($j=0; $j<$inteirogp; $j++){
			if($array_adabas[$i]['N238U31_CD_NAT_DESP'] == $array_gp[$j]['ed_codusu']){
				unset($array_adabas[$i]);
			}
		}
	}



//efetuando insert no sqlserver

	foreach($array_adabas as $tabela) {
		$query2 = ("INSERT INTO ELEMENTO_DESPESA (
		ed_exercicio,
		ed_codusu,
		ed_descricao,
		inativo) VALUES (
		'".$tabela['N238U31_EX_NAT_DESP']."',
		'".contaZero6($tabela['N238U31_CD_NAT_DESP'])."',
		'".str_replace("'", " ",$tabela['N238U31_DS_NAT_DESP'])."',
		'".$tabela['N238U31_ST_NAT_DESP']."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert ELEMENTO_DESPESA!!");
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.GP238U31_TAB_NAT_DESPESA SET N238U31_FG_NAT_DESP = 2 where ISN_GP238U31_TAB_NAT_DESPESA = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array_upadte[$j]['ISN_GP238U31_TAB_NAT_DESPESA'])) or die ("Erro durante o Update GP238U31_TAB_NAT_DESPESA!!");
     }
	 }//fim if
	unset($array);
	unset($tabela);
	ob_clean();



//---------------------------------------Para tab basPlanoInternoFinanc(dbSepof)------------------------------------(PlanoInterno)
//Select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO WHERE N152U5_FLAG_PI=1 {maxrows 1001}";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP152U5_PLANO_INTERNO!!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO WHERE N152U5_FLAG_PI =1";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP152U5_PLANO_INTERNO!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];

if ($inteiro > 1000){
		$cont[]=1000;

	for ($i=0; $i<1000; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete basPlanoInternoFinanc WHERE PI_ANO = '".$array[$i]['N152U5_ANO_PI']."' and PI_UNID_ORCA='".contaZero5($array[$i]['N152U5_UNID_ORCA_PI'])."' and PI_PROG_TRAB ='".contaZero4($array[$i]['N152U5_PROG_TRAB_PI'])."' and PI_PROJ_ATIV='".contaZero4($array[$i]['N152U5_PROJ_ATIV_PI'])."'and PI_NAT_DESP='".$array[$i]['N152U5_NAT_DESP_PI']."'and PI_SUB_ELEM='".$array[$i]['N152U5_SUB_ELEM_PI']."' and PI_FONTE='".contaZero4($array[$i]['N152U5_FONTE_PI'])."' and PI_COD='".$array[$i]['N152U5_COD_PI']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 ) or die ("Erro durante o delete basPlanoInternoFinanc!!");
	   }


	   foreach($array as $tabela) {
				$query2 = ("INSERT INTO basPlanoInternoFinanc (
		PI_ANO,
		PI_UNID_ORCA,
		PI_PROG_TRAB,
		PI_PROJ_ATIV,
		PI_NAT_DESP,
		PI_SUB_ELEM,
		PI_FONTE,
		PI_COD,
		PI_VAL_DOT_INICIAL,
		PI_VAL_CRED_A_EMP,
		PI_VAL_CRED_EMP,
		PI_VAL_LIQUID,
		PI_VAL_A_LIQUID,
		PI_VAL_PAGO,
		PI_VAL_A_PAGAR,
		PI_VAL_DOT_ATUAL,
		PI_ORGAO)
		VALUES
		 ('".$tabela['N152U5_ANO_PI']."',
		 '".contaZero5($tabela['N152U5_UNID_ORCA_PI'])."',
		 '".contaZero4($tabela['N152U5_PROG_TRAB_PI'])."',
		 '".contaZero4($tabela['N152U5_PROJ_ATIV_PI'])."',
		 '".$tabela['N152U5_NAT_DESP_PI']."',
		 '".$tabela['N152U5_SUB_ELEM_PI']."',
		 '".contaZero4($tabela['N152U5_FONTE_PI'])."',
		 '".$tabela['N152U5_COD_PI']."',
		 '".$tabela['N152U5_VAL_DOT_INICIAL']."',
		 '".$tabela['N152U5_VAL_CRED_A_EMP']."',
		 '".$tabela['N152U5_VAL_CRED_EMP']."',
		 '".$tabela['N152U5_VAL_LIQUID']."',
		 '".$tabela['N152U5_VAL_A_LIQUID']."',
		 '".$tabela['N152U5_VAL_PAGO']."',
		 '".$tabela['N152U5_VAL_A_PAGAR']."',
		 '".$tabela['N152U5_VAL_DOT_ATUAL']."',
		 '".contaZero5($tabela['N152U5_ORGAO'])."')");
 		$insert = odbc_exec( $conndbSepof, $query2 ) or $alt="1";

	}

	if($alt=="1"){

		$queryalt= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO WHERE N152U5_FLAG_PI=1 {maxrows 1}";
		$selalt = odbc_exec( $connAdabas, $queryalt ) or die ("Erro durante o select AP152U5_PLANO_INTERNO!!");

				$cont[]=1;

		for ($i=0; $i<1; $i++){
		$linhalt = odbc_fetch_array($selalt);
		$arrayalt[] = $linhalt;


		//deletando dados repetidos

	   $queryAux2 = ("Delete basPlanoInternoFinanc WHERE PI_ANO = '".$array[$i]['N152U5_ANO_PI']."' and PI_UNID_ORCA='".contaZero5($array[$i]['N152U5_UNID_ORCA_PI'])."' and PI_PROG_TRAB ='".contaZero4($array[$i]['N152U5_PROG_TRAB_PI'])."' and PI_PROJ_ATIV='".contaZero4($array[$i]['N152U5_PROJ_ATIV_PI'])."'and PI_NAT_DESP='".$array[$i]['N152U5_NAT_DESP_PI']."'and PI_SUB_ELEM='".$array[$i]['N152U5_SUB_ELEM_PI']."' and PI_FONTE='".contaZero4($array[$i]['N152U5_FONTE_PI'])."' and PI_COD='".$array[$i]['N152U5_COD_PI']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 ) or die ("Erro durante o delete basPlanoInternoFinanc!!");
	   }

	   foreach($arrayalt as $tabela) {
				$query2 = ("INSERT INTO basPlanoInternoFinanc (
		PI_ANO,
		PI_UNID_ORCA,
		PI_PROG_TRAB,
		PI_PROJ_ATIV,
		PI_NAT_DESP,
		PI_SUB_ELEM,
		PI_FONTE,
		PI_COD,
		PI_VAL_DOT_INICIAL,
		PI_VAL_CRED_A_EMP,
		PI_VAL_CRED_EMP,
		PI_VAL_LIQUID,
		PI_VAL_A_LIQUID,
		PI_VAL_PAGO,
		PI_VAL_A_PAGAR,
		PI_VAL_DOT_ATUAL,
		PI_ORGAO)
		VALUES
		 ('".$tabela['N152U5_ANO_PI']."',
		 '".contaZero5($tabela['N152U5_UNID_ORCA_PI'])."',
		 '".contaZero4($tabela['N152U5_PROG_TRAB_PI'])."',
		 '".contaZero4($tabela['N152U5_PROJ_ATIV_PI'])."',
		 '".$tabela['N152U5_NAT_DESP_PI']."',
		 '".$tabela['N152U5_SUB_ELEM_PI']."',
		 '".contaZero4($tabela['N152U5_FONTE_PI'])."',
		 '".$tabela['N152U5_COD_PI']."',
		 '".$tabela['N152U5_VAL_DOT_INICIAL']."',
		 '".$tabela['N152U5_VAL_CRED_A_EMP']."',
		 '".$tabela['N152U5_VAL_CRED_EMP']."',
		 '".$tabela['N152U5_VAL_LIQUID']."',
		 '".$tabela['N152U5_VAL_A_LIQUID']."',
		 '".$tabela['N152U5_VAL_PAGO']."',
		 '".$tabela['N152U5_VAL_A_PAGAR']."',
		 '".$tabela['N152U5_VAL_DOT_ATUAL']."',
		 '".contaZero5($tabela['N152U5_ORGAO'])."')");
 		$insert = odbc_exec( $conndbSepof, $query2 )  or die ("Erro durante o insert AP152U5_PLANO_INTERNO parte Alternativa!!");

	}
	//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO SET N152U5_FLAG_PI = 2 where ISN_AP152U5_PLANO_INTERNO = ? ");

  	 for ($j=0; $j<1; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$arrayalt[$j]['ISN_AP152U5_PLANO_INTERNO'])) or die ("Erro durante o Update AP152U5_PLANO_INTERNO!!");
     }

	unset($array);
	unset($arrayalt);
	unset($tabela);
	ob_clean();
	die('Insert Alternativo realizado c sucesso');

	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO SET N152U5_FLAG_PI = 2 where ISN_AP152U5_PLANO_INTERNO = ? ");

  	 for ($j=0; $j<1000; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U5_PLANO_INTERNO'])) or die ("Erro durante o Update AP152U5_PLANO_INTERNO!!");
     }

}//fim if

//-------------------------------------------------------
	else if($inteiro == 0) {
		$cont[]=0;

} //fim else if
//-------------------------------------------------------
 else {

 		$cont[]=$inteiro;
	for ($i=0; $i<$inteiro; $i++){
	$linha = odbc_fetch_array($sel);
	$array[] = $linha;

//deletando dados repetidos

	   $queryAux2 = ("Delete basPlanoInternoFinanc WHERE PI_ANO = '".$array[$i]['N152U5_ANO_PI']."' and PI_UNID_ORCA='".contaZero5($array[$i]['N152U5_UNID_ORCA_PI'])."' and PI_PROG_TRAB ='".contaZero4($array[$i]['N152U5_PROG_TRAB_PI'])."' and PI_PROJ_ATIV='".contaZero4($array[$i]['N152U5_PROJ_ATIV_PI'])."'and PI_NAT_DESP='".$array[$i]['N152U5_NAT_DESP_PI']."'and PI_SUB_ELEM='".$array[$i]['N152U5_SUB_ELEM_PI']."' and PI_FONTE='".contaZero4($array[$i]['N152U5_FONTE_PI'])."' and PI_COD='".$array[$i]['N152U5_COD_PI']."'");
	   $delete = odbc_exec( $conndbSepof, $queryAux2 ) or die ("Erro durante o delete basPlanoInternoFinanc!!");
	   }


	   foreach($array as $tabela) {
				$query2 = ("INSERT INTO basPlanoInternoFinanc (
		PI_ANO,
		PI_UNID_ORCA,
		PI_PROG_TRAB,
		PI_PROJ_ATIV,
		PI_NAT_DESP,
		PI_SUB_ELEM,
		PI_FONTE,
		PI_COD,
		PI_VAL_DOT_INICIAL,
		PI_VAL_CRED_A_EMP,
		PI_VAL_CRED_EMP,
		PI_VAL_LIQUID,
		PI_VAL_A_LIQUID,
		PI_VAL_PAGO,
		PI_VAL_A_PAGAR,
		PI_VAL_DOT_ATUAL,
		PI_ORGAO)
		VALUES
		 ('".$tabela['N152U5_ANO_PI']."',
		 '".contaZero5($tabela['N152U5_UNID_ORCA_PI'])."',
		 '".contaZero4($tabela['N152U5_PROG_TRAB_PI'])."',
		 '".contaZero4($tabela['N152U5_PROJ_ATIV_PI'])."',
		 '".$tabela['N152U5_NAT_DESP_PI']."',
		 '".$tabela['N152U5_SUB_ELEM_PI']."',
		 '".contaZero4($tabela['N152U5_FONTE_PI'])."',
		 '".$tabela['N152U5_COD_PI']."',
		 '".$tabela['N152U5_VAL_DOT_INICIAL']."',
		 '".$tabela['N152U5_VAL_CRED_A_EMP']."',
		 '".$tabela['N152U5_VAL_CRED_EMP']."',
		 '".$tabela['N152U5_VAL_LIQUID']."',
		 '".$tabela['N152U5_VAL_A_LIQUID']."',
		 '".$tabela['N152U5_VAL_PAGO']."',
		 '".$tabela['N152U5_VAL_A_PAGAR']."',
		 '".$tabela['N152U5_VAL_DOT_ATUAL']."',
		 '".contaZero5($tabela['N152U5_ORGAO'])."')");
 		$insert = odbc_exec( $conndbSepof, $query2 ) or die ("Erro durante o insert basPlanoInternoFinanc!!");

	}
//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP152U5_PLANO_INTERNO SET N152U5_FLAG_PI = 2 where ISN_AP152U5_PLANO_INTERNO = ? ");

  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$array[$j]['ISN_AP152U5_PLANO_INTERNO'])) or die ("Erro durante o Update AP152U5_PLANO_INTERNO!!");
     }
}//fim do else


	unset($array);
	unset($tabela);
	ob_clean();




//------------------------------------------------------Para tab UO--------------------------------------------------------------


//select

	$query= "SELECT * FROM SEPOF_GPPARA_PRD.dbo.AP70U58_UNID_ORC WHERE N70U58_FG_ORCAM=2";
	$sel = odbc_exec( $connAdabas, $query ) or die ("Erro durante o select AP70U58_UNID_ORC!!");

	$query= "select uo_codusu from UNIDADE_ORCAMENTARIA";
	$sel2 = odbc_exec( $connSQL, $query ) or die ("Erro durante o select !!");

//queryAux que conta quantos registros foram selecionados

	$queryAux= "select count (*) as reg from SEPOF_GPPARA_PRD.dbo.AP70U58_UNID_ORC WHERE N70U58_FG_ORCAM=2";
	$conta = odbc_exec( $connAdabas, $queryAux ) or die ("Erro durante o select count AP70U58_UNID_ORC!!");
	$total = odbc_fetch_array($conta);

			$inteiro = (int)$total[reg];
			$cont[]=$inteiro;

	$queryAux2= "select count (*) as reg2 from UNIDADE_ORCAMENTARIA";
	$conta2 = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o select count da Unidade Orcamentaria!!");
    $total2 = odbc_fetch_array($conta2);

			$inteiro2 = (int)$total2[reg2];

		if ($inteiro){
 		for ($i=0; $i<$inteiro; $i++){
			$linha = odbc_fetch_array($sel);
			$array[] = $linha;
			$arrayUpdate[]=$linha;

			 $queryAux2 = ("Update UNIDADE_ORCAMENTARIA set uo_descricao = '".str_replace("'", " ",$array[$i]['N70U58_NOME_UNID_ORCAM'])."' , uo_sigla = '".$array[$i]['N70U58_SIGLA_UNID_ORCAM']."' ,
	  		 uo_dataimpl= '".date('Y-m-d  H:i:s')."' WHERE uo_codusu = '".contaZero5($array[$i]['N70U58_UNID_ORCAM_58'])."'");
	   		 $update = odbc_exec( $connSQL, $queryAux2 ) or die ("Erro durante o delete UO!!");

			for ($j=0; $j<$inteiro2; $j++){
					$linha2 = odbc_fetch_array($sel2);
					$array2[] = $linha2;
							if ($array[$i]['N70U58_UNID_ORCAM_58']==$array2[$j]['uo_codusu'])
							{
								unset ($array[$i]) ;
							}
				        }//fimforj
			        }//fimfori

//efetuando insert no sqlserver

	foreach($array as $tabela) {
		$query2 = ("INSERT INTO UNIDADE_ORCAMENTARIA(
		uo_codusu,
		uo_descricao,
		uo_sigla,
		uo_dataimpl) VALUES (
		'".contaZero5($tabela['N70U58_UNID_ORCAM_58'])."',
		'".str_replace("'", " ",$tabela['N70U58_NOME_UNID_ORCAM'])."',
		'".$tabela['N70U58_SIGLA_UNID_ORCAM']."',
		'".dataContraria($tabela['N70U58_DATA_OPERACAO'])."')");
 		$insert = odbc_exec( $connSQL, $query2 ) or die ("Erro durante o insert UO!!"); //os campos org_codigo,aa_codigo,usu_codigo,inativo inseridos via gePpa e dbSepof
	}

//Realizando Update da Flag

   odbc_close ($connAdabas);

   $queryAux3= ("UPDATE SEPOF_GPPARA_PRD.dbo.AP70U58_UNID_ORC SET N70U58_FG_ORCAM = 2 where ISN_AP70U58_UNID_ORC = ? ");
  	 for ($j=0; $j<$inteiro; $j++){
			$res = odbc_prepare ($connAdabas, $queryAux3);
			$exc = odbc_execute($res, array((int)$arrayUpdate[$j]['ISN_AP70U58_UNID_ORC'])) or die ("Erro durante o Update AP70U58_UNID_ORC!!");
     }
	 }//fim if

	unset($array);
	unset($tabela);
	ob_clean();


//-------------------------------------------Captura hora final do extrator------------------------------------------------------
$dataHoraFIM = date("m/d/Y H:i:s");
//--------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------Gravando Log-------------------------------------------------------

	$query2 = ("INSERT INTO LOG_EXTRATOR(
		lex_inicio,
		lex_fim,
		lex_dados,
		lex_ok) VALUES (
		'".$dataHoraINI."',
		'".$dataHoraFIM."',
		'Foram ".$cont[0]." Registros de Empenho
		 Foram ".$cont[1]." Registros de Favorecido
		 Foram ".$cont[2]." Registros de Pago
		 Foram ".$cont[3]." Registros de Liquidacao_Det
		 Foram ".$cont[4]." Registros de Pago_Empenho
		 Foram ".$cont[5]." Registros de Liquidado
		 Foram ".$cont[6]." Registros de PTRes
		 Foram ".$cont[7]." Registros de Acao
		 Foram ".$cont[8]." Registros de AcaoMunicBenef
		 Foram ".$cont[9]." Registros de AcaoMunicipio
		 Foram ".$cont[10]." Registros de Acao_NE
		 Foram ".$cont[11]." Registros de Acao_Munic_Patrim
		 Foram ".$cont[12]." Registros de Acao_Ptres
		 Foram ".$cont[13]." Registros de Detalhe_Acao
		 Foram ".$cont[14]." Registros de Grupo_Acao
		 Foram ".$cont[15]." Registros de Tipo_Patrimonio
		 Foram ".$cont[16]." Registros de Patrimonio
		 Foram ".$cont[17]." Registros de Tipo_Acao
		 Foram ".$cont[18]." Registros de Conectivo
		 Foram ".$cont[19]." Registros de SubFuncao
		 Foram ".$cont[20]." Registros de Objeto
		 Foram ".$cont[21]." Registros de QuotasFinanceiras
		 Foram ".$cont[22]." Registros de QuotasOrcamentarias
		 Foram ".$cont[23]." Registros de basPlanoInternoMovimento
		 Foram ".$cont[24]." Registros de basTabelaPlanoInterno
		 Foram ".$cont[25]." Registros de Material
		 Foram ".$cont[26]." Registros de Classe_Mat
		 Foram ".$cont[27]." Registros de Grupo_Mat
		 Foram ".$cont[28]." Registros de pgpPlanoContas_Tradicao pela Plano_Contas
		 Foram ".$cont[29]." Registros de basPlanodeContas
		 Foram ".$cont[30]." Registros de pgpPlanoContas_Tradicao pela Plano_Contas_Velho
		 Foram ".$cont[31]." Registros de basPlanoContas_Ant
		 Foram ".$cont[32]." Registros de DotacaoAtualizada
		 Foram ".$cont[33]." Registros de DotacaoAtualizadaMES
		 Foram ".$cont[34]." Registros de Elemento_Despesa
		 Foram ".$cont[35]." Registros de basPlanoInternoFinanc extraidos do SIAFEM',
		 1)");
 		$insert = odbc_exec( $connSQL, $query2 );





//--------------------------------------------------------Fim das Conexões--------------------------------------------------------
	odbc_close ($connSQL);
	odbc_close ($connAdabas);
	odbc_close ($conndbSepof);
//--------------------------------------------------------------------------------------------------------------------------------

?>
